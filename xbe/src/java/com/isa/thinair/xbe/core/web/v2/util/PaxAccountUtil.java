package com.isa.thinair.xbe.core.web.v2.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang.time.DateFormatUtils;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargeAdjustmentType;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonOfflinePaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCashPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientLMSPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientOnAccountPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaxCreditPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientVoucherPaymentInfo;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;
import com.isa.thinair.webplatform.core.commons.DatabaseUtil;
import com.isa.thinair.xbe.api.dto.v2.CarrierwiseChargeAdjustments;
import com.isa.thinair.xbe.api.dto.v2.ChargesListTO;
import com.isa.thinair.xbe.api.dto.v2.CreditsListTO;
import com.isa.thinair.xbe.api.dto.v2.ExternalPaymentsTO;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountChargesTO;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountCreditTO;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountPaymentsTO;
import com.isa.thinair.xbe.api.dto.v2.PaxSortChargesTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentListTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.DateUtil;
import com.isa.thinair.xbe.core.web.facade.CardPaymentFacade;

public class PaxAccountUtil {

	/**
	 * Sets passenger charges information.
	 * @param agentCurrency
	 *            TODO
	 * @param isInfantPaymentSeparated
	 * 
	 * @return PaxAccountChargesTO
	 * @throws ParseException
	 */
	public static PaxAccountChargesTO getChargesDetails(LCCClientReservationPax passenger, String currency,
			List<LCCClientCarrierOndGroup> colOndGroups, boolean isGroupPnr, Collection<LCCClientReservationSegment> colsegs,
			String agentCurrency, String resStatus, Date resLastModDate, boolean isInfantPaymentSeparated) throws ParseException, ModuleException {
		PaxAccountChargesTO paxAccountChargesTO = new PaxAccountChargesTO();
		Collection<ChargesListTO> collection = new ArrayList<ChargesListTO>();
		Map<String, BigDecimal> airlinewiseTotals = new HashMap<String, BigDecimal>();

		if (passenger != null) {
			paxAccountChargesTO.setActualCredit(AccelAeroCalculator.formatAsDecimal(passenger.getTotalActualCredit()));
			paxAccountChargesTO.setBalance(AccelAeroCalculator.formatAsDecimal(passenger.getTotalAvailableBalance()));
			paxAccountChargesTO.setCurrency(currency);
			paxAccountChargesTO.setAgentCurrency(agentCurrency);
			Integer seqStartNo = new Integer(1);
			BigDecimal airLineTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
			// Builds airline wise charges map keys
			if (colOndGroups != null && !colOndGroups.isEmpty()) {
				for (LCCClientCarrierOndGroup carrierGroup : colOndGroups) {
					airlinewiseTotals.put(carrierGroup.getCarrierCode(), airLineTotal);
				}
			} else {
				airlinewiseTotals.put(AppSysParamsUtil.getDefaultCarrierCode(), airLineTotal);
			}

			// Adding adult pax charges
			collection.addAll(setPaxChargesAndFares(passenger, seqStartNo, airlinewiseTotals, isGroupPnr, agentCurrency,
					resLastModDate));
			// Adding all linked infant pax charges to the adult pax.
			if (!isInfantPaymentSeparated && passenger.getInfants() != null) {
				for (LCCClientReservationPax infantPax : passenger.getInfants()) {
					collection.addAll(setPaxChargesAndFares(infantPax, seqStartNo, airlinewiseTotals, isGroupPnr, agentCurrency,
							resLastModDate));
				}
			}
			paxAccountChargesTO.setTotalCharges(AccelAeroCalculator.formatAsDecimal(passenger.getTotalPrice()));
			collection = setSegmentTotals(new ArrayList<ChargesListTO>(collection), airlinewiseTotals, colsegs);
		}
		paxAccountChargesTO.setChargesList(collection);
		return paxAccountChargesTO;
	}

	/**
	 * Sets passenger payments information
	 * @param isGroupRefund 
	 * @return PaxAccountPaymentsTO
	 * @throws ParseException
	 */
	public static PaxAccountPaymentsTO getPaymentDetails(LCCClientReservationPax passenger, String resStatus,
			Map<String, String> mapAllAgentCodeDesc, boolean isGroupRefund) throws ModuleException {

		PaxAccountPaymentsTO paxAccountPaymentsTO = new PaxAccountPaymentsTO();
		ArrayList<PaymentListTO> collection = new ArrayList<PaymentListTO>();
		String strAgentName = null;

		if (passenger != null) {
			// Sets the summary data
			paxAccountPaymentsTO.setBalance(AccelAeroCalculator.formatAsDecimal(passenger.getTotalAvailableBalance()));
			paxAccountPaymentsTO.setTotalPayment(AccelAeroCalculator.formatAsDecimal(passenger.getTotalPaidAmount()));
			paxAccountPaymentsTO.setActualCredit(AccelAeroCalculator.formatAsDecimal(passenger.getTotalActualCredit()));

			// We can't make this common for all the payments.
			// Payments can be on any currency.. so let's implement that provision at once.
			paxAccountPaymentsTO.setCurrency(AppSysParamsUtil.getBaseCurrency());
			if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equalsIgnoreCase(resStatus)) {
				PaymentListTO paymentListTO = new PaymentListTO();
				paymentListTO.setDisplayMethod("No Display data");
				collection.add(paymentListTO);
			} else {
				
				Map<String, BigDecimal> paymentRefundMap = new HashMap<String, BigDecimal>();
				for (LCCClientPaymentInfo objPay : passenger.getLccClientPaymentHolder().getPayments()) {
					if (objPay.getPaymentTnxReference() != null) {
						BigDecimal refundTotal = paymentRefundMap.get(objPay.getPaymentTnxReference());
						if (refundTotal != null) {
							paymentRefundMap.put(objPay.getPaymentTnxReference(),
									AccelAeroCalculator.add(refundTotal, objPay.getTotalAmount()));
						} else {
							paymentRefundMap.put(objPay.getPaymentTnxReference(), objPay.getTotalAmount());
						}
					}
				}
				
				for (LCCClientPaymentInfo objPay : passenger.getLccClientPaymentHolder().getPayments()) {
					PaymentListTO paymentListTO = new PaymentListTO();
								
					if (isGroupRefund) {
						String paxName = BeanUtils.nullHandler(passenger.getTitle()) + " " + passenger.getFirstName()
								+ " " + passenger.getLastName();
						if (objPay.getTotalAmount().compareTo(BigDecimal.ZERO) < 0) {
							continue;
						} else {
							if (paymentRefundMap.get(objPay.getOriginalPayReference()) != null) {
								BigDecimal refundableAmount = AccelAeroCalculator.add(objPay.getTotalAmount(),
										paymentRefundMap.get(objPay.getOriginalPayReference()));
								if (BigDecimal.ZERO.compareTo(refundableAmount) < 0) {
									paymentListTO.setDisplayRefundableAmount(AccelAeroCalculator
											.formatAsDecimal(refundableAmount));
									paymentListTO.setDisplayPaxName(paxName);
								} else {
									continue;
								}

							} else {
								paymentListTO.setDisplayRefundableAmount(AccelAeroCalculator.formatAsDecimal(objPay
										.getTotalAmount()));
								paymentListTO.setDisplayPaxName(paxName);
							}
						}

					}
						
					
					String actualPaymentMethodAppend = "";
					if (objPay.getActualPaymentMethod() != null) {
						actualPaymentMethodAppend += "/"
								+ ModuleServiceLocator.getReservationBD().getActualPaymentMethodById(
										objPay.getActualPaymentMethod());
					}
					// Sets OnAcc Payments
					if (objPay instanceof LCCClientOnAccountPaymentInfo) {
						LCCClientOnAccountPaymentInfo payObject = (LCCClientOnAccountPaymentInfo) objPay;
						String paymentType = (payObject.getPaymentMethod().equals(Agent.PAYMENT_MODE_ONACCOUNT)
								? PaymentMethod.ACCO.getCode()
								: PaymentMethod.BSP.getCode());
						paymentListTO.setPaymentMethod(paymentType);

						strAgentName = mapAllAgentCodeDesc.get(payObject.getAgentName());
						if (strAgentName == null) {
							strAgentName = payObject.getAgentName();
						}
						// Disabling the actual dry agent display in the payment tab till the CR is approved
						if (payObject.getTotalAmount().compareTo(new BigDecimal(0)) >= 0) {

							if (!payObject.isDryPaymrtInfo()) {
								paymentListTO.setDisplayMethod(paymentType + "/" + strAgentName + actualPaymentMethodAppend);
							} else {
								// paymentListTO.setDisplayMethod("ONACC/"+payObject.getExternalCarrierCode() + "/" +
								// payObject.getExternalAgentCode());
								paymentListTO.setDisplayMethod(paymentType + "/" + strAgentName + actualPaymentMethodAppend);
							}
						} else {
							if (!payObject.isDryPaymrtInfo()) {
								paymentListTO.setDisplayMethod(paymentType + "_REFUND/" + payObject.getCarrierCode() + "/"
										+ strAgentName + actualPaymentMethodAppend);
							} else {
								// paymentListTO.setDisplayMethod("ONACC_REFUND/"+payObject.getExternalCarrierCode() +
								// "/" + payObject.getExternalAgentCode());
								paymentListTO.setDisplayMethod(paymentType + "_REFUND/" + payObject.getCarrierCode() + "/"
										+ strAgentName + actualPaymentMethodAppend);
							}
						}

						paymentListTO.setDisplayCarrierCode(payObject.getCarrierCode());
						String paidCurrency = payObject.getTotalAmountCurrencyCode();
						String paidAirLineCurrency = payObject.getPayCurrencyDTO().getPayCurrencyCode();
						paymentListTO.setHdnPaidAirLineCurrency(paidAirLineCurrency);
						paymentListTO.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()) + " "
								+ paidCurrency);
						paymentListTO.setDisplayPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getPayCurrencyAmount())
								+ " " + paidAirLineCurrency);
						if (payObject.getTxnDateTime() != null) {
							paymentListTO.setDisplayDate(DateFormatUtils.format(payObject.getTxnDateTime(), "dd-MM-yyyy HH:mm"));
							paymentListTO.setHdnPaydate(payObject.getTxnDateTime());
						}
						paymentListTO.setHdnAuthorizationId(paymentType + "$" + payObject.getAgentCode());
						paymentListTO.setHdnPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()));
						paymentListTO.setHdnPaidCurrency(paidCurrency);
						paymentListTO.setEnableRecieptPrint(ReservationBeanUtil.isRecieptGenerationEnable("OA") ? "Y" : "N");
						paymentListTO.setRecieptNumber(payObject.getRecieptNumber());
						paymentListTO.setDisplayNotes(payObject.getRemarks());
						// Sets Cash Payments
					} else if (objPay instanceof LCCClientCashPaymentInfo) {
						LCCClientCashPaymentInfo payObject = (LCCClientCashPaymentInfo) objPay;

						if (AppSysParamsUtil.showAgentInfoForCashPayments()) {
							strAgentName = mapAllAgentCodeDesc.get(payObject.getAgentName());
							if (strAgentName == null) {
								strAgentName = payObject.getAgentName();
							}
							strAgentName = "/" + strAgentName + " ";
						} else {
							strAgentName = "";
						}

						// hide zero cash payment in account tab
						if (AppSysParamsUtil.hideZeroCashPaymentRecordInXBE() && "Y".equals(payObject.getDummyPayment())
								&& payObject.getTotalAmount().compareTo(new BigDecimal(0)) == 0) {
							continue;
						}

						if (payObject.getTotalAmount().compareTo(new BigDecimal(0)) >= 0) {
							paymentListTO.setDisplayMethod("CASH_PAYMENT" + "/" + payObject.getCarrierCode() + strAgentName
									+ actualPaymentMethodAppend);
						} else {
							paymentListTO.setDisplayMethod("CASH_REFUND/" + payObject.getCarrierCode() + strAgentName
									+ actualPaymentMethodAppend);
						}

						paymentListTO.setDisplayCarrierCode(payObject.getCarrierCode());
						String paidCurrency = payObject.getTotalAmountCurrencyCode();
						String paidAirLineCurrency = payObject.getPayCurrencyDTO().getPayCurrencyCode();
						paymentListTO.setHdnPaidAirLineCurrency(paidAirLineCurrency);
						paymentListTO.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()) + " "
								+ paidCurrency);
						paymentListTO.setDisplayPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getPayCurrencyAmount())
								+ " " + paidAirLineCurrency);
						if (payObject.getTxnDateTime() != null) {
							paymentListTO.setDisplayDate(DateFormatUtils.format(payObject.getTxnDateTime(), "dd-MM-yyyy HH:mm"));
							paymentListTO.setHdnPaydate(payObject.getTxnDateTime());
						}
						paymentListTO.setHdnAuthorizationId("CASH");
						paymentListTO.setHdnPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()));
						paymentListTO.setHdnPaidCurrency(paidCurrency);
						paymentListTO.setEnableRecieptPrint(ReservationBeanUtil.isRecieptGenerationEnable("CA") ? "Y" : "N");
						paymentListTO.setRecieptNumber(payObject.getRecieptNumber());
						paymentListTO.setPaymentMethod(PaymentMethod.CASH.getCode());
						paymentListTO.setDisplayNotes(payObject.getRemarks());
						// Sets Credit card Payments
					} else if (objPay instanceof CommonCreditCardPaymentInfo) {
						CommonCreditCardPaymentInfo payObject = (CommonCreditCardPaymentInfo) objPay;
						String payMethod = null;

						if (payObject.getTotalAmount().compareTo(new BigDecimal(0)) >= 0) {
							payMethod = "CARD_PAYMENT_" + PaymentType.getPaymentTypeDesc(payObject.getType()) + "/"
									+ BeanUtils.nullHandler(payObject.getCardHoldersName() + actualPaymentMethodAppend);
						} else {
							payMethod = "CARD_REFUND_" + PaymentType.getPaymentTypeDesc(payObject.getType()) + "/"
									+ payObject.getCarrierCode() + "/"
									+ BeanUtils.nullHandler(payObject.getCardHoldersName() + actualPaymentMethodAppend);
						}
						paymentListTO.setDisplayMethod(payMethod);
						paymentListTO.setDisplayCarrierCode(payObject.getCarrierCode());
						String paidCurrency = payObject.getTotalAmountCurrencyCode();
						String paidAirLineCurrency = payObject.getPayCurrencyDTO().getPayCurrencyCode();
						paymentListTO.setHdnPaidAirLineCurrency(paidAirLineCurrency);
						paymentListTO.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()) + " "
								+ paidCurrency);
						paymentListTO.setDisplayPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getPayCurrencyAmount())
								+ " " + paidAirLineCurrency);
						if (payObject.getTxnDateTime() != null) {
							paymentListTO.setDisplayDate(DateUtil.formatDate(payObject.getTxnDateTime(), "dd-MM-yyyy HH:mm"));
							paymentListTO.setHdnPaydate(payObject.getTxnDateTime());
						}
						paymentListTO.setHdnAuthorizationId("CRED$" + payObject.getAuthorizationId());
						paymentListTO.setHdnPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()));
						paymentListTO.setHdnPaidCurrency(paidCurrency);
						paymentListTO.setPaymentMethod(PaymentMethod.CRED.getCode());
						paymentListTO.setDisplayNotes(payObject.getRemarks());

						if (payObject.getPaymentBrokerRefNo() != null) {
							paymentListTO.setPaymentBrokerRefNo(payObject.getPaymentBrokerRefNo().toString());
						}
						
						if (AppSysParamsUtil.getDefaultCarrierCode().equals(payObject.getCarrierCode())
								&& !AppSysParamsUtil.isLCCConnectivityEnabled()) {
							Date applicableExRateTimestamp = objPay.getTxnDateTime();
							ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(applicableExRateTimestamp);

							Object[] pgObj = CardPaymentFacade.getPaymentGatewayValues(objPay.getOriginalPayReference(),
									exchangeRateProxy, true);
							boolean supportOnlyFullRefund = Boolean.valueOf(((Properties) pgObj[4])
									.getProperty("supportOnlyFullRefund"));

							paymentListTO.setSupportOnlyFullRefund(supportOnlyFullRefund);
						}
					} else if (objPay instanceof LCCClientPaxCreditPaymentInfo) {
						LCCClientPaxCreditPaymentInfo payObject = (LCCClientPaxCreditPaymentInfo) objPay;
						String payMethod = payObject.getDescription();

						paymentListTO.setDisplayMethod(payMethod);
						paymentListTO.setDisplayCarrierCode(payObject.getCarrierCode());
						String paidCurrency = payObject.getTotalAmountCurrencyCode();
						String paidAirLineCurrency = payObject.getPayCurrencyDTO().getPayCurrencyCode();
						paymentListTO.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()) + " "
								+ paidCurrency);
						paymentListTO.setDisplayPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getPayCurrencyAmount())
								+ " " + paidAirLineCurrency);
						if (payObject.getTxnDateTime() != null) {
							paymentListTO.setDisplayDate(DateFormatUtils.format(payObject.getTxnDateTime(), "dd-MM-yyyy HH:mm"));
							paymentListTO.setHdnPaydate(payObject.getTxnDateTime());
						}
						paymentListTO.setHdnPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()));
						paymentListTO.setHdnPaidCurrency(paidCurrency);
						paymentListTO.setDisplayNotes(payObject.getRemarks());
					} else if (objPay instanceof LCCClientLMSPaymentInfo) {
						LCCClientLMSPaymentInfo payObject = (LCCClientLMSPaymentInfo) objPay;
						paymentListTO.setDisplayMethod("LOYALTY_PAYMENT /"
								+ BeanUtils.nullHandler(payObject.getLoyaltyMemberAccountId() + actualPaymentMethodAppend));
						paymentListTO.setDisplayCarrierCode(payObject.getCarrierCode());
						String paidCurrency = payObject.getTotalAmountCurrencyCode();
						String paidAirLineCurrency = payObject.getPayCurrencyDTO().getPayCurrencyCode();
						paymentListTO.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()) + " "
								+ paidCurrency);
						paymentListTO.setDisplayPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getPayCurrencyAmount())
								+ " " + paidAirLineCurrency);
						if (payObject.getTxnDateTime() != null) {
							paymentListTO.setDisplayDate(DateFormatUtils.format(payObject.getTxnDateTime(), "dd-MM-yyyy HH:mm"));
							paymentListTO.setHdnPaydate(payObject.getTxnDateTime());
						}
						paymentListTO.setHdnPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()));
						paymentListTO.setHdnPaidCurrency(paidCurrency);
						paymentListTO.setPaymentMethod(PaymentMethod.LMS.getCode());
						paymentListTO.setDisplayNotes(payObject.getRemarks());
					} else if (objPay instanceof LCCClientVoucherPaymentInfo) {
						LCCClientVoucherPaymentInfo payObject = (LCCClientVoucherPaymentInfo) objPay;
						paymentListTO.setDisplayMethod("VOUCHER_PAYMENT /"
								+ BeanUtils.nullHandler(payObject.getVoucherDTO().getVoucherId() + actualPaymentMethodAppend));
						paymentListTO.setDisplayCarrierCode(payObject.getCarrierCode());
						String paidCurrency = payObject.getTotalAmountCurrencyCode();
						String paidAirLineCurrency = payObject.getPayCurrencyDTO().getPayCurrencyCode();
						paymentListTO.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()) + " "
								+ paidCurrency);
						paymentListTO.setDisplayPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getPayCurrencyAmount())
								+ " " + paidAirLineCurrency);
						if (payObject.getTxnDateTime() != null) {
							paymentListTO.setDisplayDate(DateFormatUtils.format(payObject.getTxnDateTime(), "dd-MM-yyyy HH:mm"));
							paymentListTO.setHdnPaydate(payObject.getTxnDateTime());
						}
						paymentListTO.setHdnPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()));
						paymentListTO.setHdnPaidCurrency(paidCurrency);
						paymentListTO.setPaymentMethod(PaymentMethod.VOUCHER.getCode());
						paymentListTO.setDisplayNotes(payObject.getRemarks());
					} else if (objPay instanceof CommonOfflinePaymentInfo) {
						CommonOfflinePaymentInfo payObject = (CommonOfflinePaymentInfo) objPay;
						String payMethod = null;

						if (payObject.getTotalAmount().compareTo(new BigDecimal(0)) >= 0) {
							payMethod = "CARD_PAYMENT_"
									+ PaymentType.getPaymentTypeDesc(ReservationTnxNominalCode.CARD_PAYMENT_GENERIC.getCode())
									+ "/" + actualPaymentMethodAppend;
						} else {
							payMethod = "CARD_REFUND_"
									+ PaymentType.getPaymentTypeDesc(ReservationTnxNominalCode.REFUND_GENERIC.getCode()) + "/"
									+ payObject.getCarrierCode() + "/" + actualPaymentMethodAppend;
						}
						paymentListTO.setDisplayMethod(payMethod);
						paymentListTO.setDisplayCarrierCode(payObject.getCarrierCode());
						String paidCurrency = payObject.getTotalAmountCurrencyCode();
						String paidAirLineCurrency = payObject.getPayCurrencyDTO().getPayCurrencyCode();
						paymentListTO.setHdnPaidAirLineCurrency(paidAirLineCurrency);
						paymentListTO.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()) + " "
								+ paidCurrency);
						paymentListTO.setDisplayPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getPayCurrencyAmount())
								+ " " + paidAirLineCurrency);
						if (objPay.getPaymentTnxDateTime() != null) {
							paymentListTO.setDisplayDate(DateUtil.formatDate(objPay.getPaymentTnxDateTime(), "dd-MM-yyyy HH:mm"));
							paymentListTO.setHdnPaydate(objPay.getPaymentTnxDateTime());
						}
						paymentListTO.setHdnPayAmount(AccelAeroCalculator.formatAsDecimal(payObject.getTotalAmount()));
						paymentListTO.setHdnPaidCurrency(paidCurrency);
						paymentListTO.setPaymentMethod(PaymentMethod.CRED.getCode());
						paymentListTO.setDisplayNotes(payObject.getRemarks());

						if (AppSysParamsUtil.getDefaultCarrierCode().equals(payObject.getCarrierCode())
								&& !AppSysParamsUtil.isLCCConnectivityEnabled()) {
							Date applicableExRateTimestamp = objPay.getPaymentTnxDateTime();
							ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(applicableExRateTimestamp);

							Object[] pgObj = CardPaymentFacade.getPaymentGatewayValues(objPay.getPayReference(),
									exchangeRateProxy, true);
							boolean supportOnlyFullRefund = Boolean.valueOf(((Properties) pgObj[4])
									.getProperty("supportOnlyFullRefund"));

							paymentListTO.setSupportOnlyFullRefund(supportOnlyFullRefund);
						}
					}
					paymentListTO.setHdnPayRef(objPay.getOriginalPayReference());
					paymentListTO.setHdnCarrierVisePayments(objPay.getCarrierVisePayments());
					paymentListTO.setNonRefundable(objPay.isNonRefundable());
					paymentListTO.setHdnLccUniqueTnxId(objPay.getLccUniqueTnxId());
					collection.add(paymentListTO);
				} 
			}
		}
		paxAccountPaymentsTO.setPaymentList(sortPaxPayments(collection));

		return paxAccountPaymentsTO;
	}

	/**
	 * TODO
	 * 
	 * @return
	 * @throws ParseException
	 */
	public static Collection<ExternalPaymentsTO> getExternalPayments() throws ParseException {
		Collection<ExternalPaymentsTO> collection = new ArrayList<ExternalPaymentsTO>();

		// Zaki [Feb.11.2010] : At this stage we should not be putting any
		// dummy values as this will only delay what needs to be fixed now

		ExternalPaymentsTO externalPaymentsTO = new ExternalPaymentsTO();
		externalPaymentsTO.setDisplayAmount("-----");
		externalPaymentsTO.setDisplayChannel("-----");
		externalPaymentsTO.setDisplayStatus("-----");
		externalPaymentsTO.setDisplayTimeStamp("-----");
		collection.add(externalPaymentsTO);

		return collection;
	}

	public static Integer getPnrPaxID(String selectedPax, String carrierCode) throws ModuleException {
		String[] carrierWisePax = selectedPax.split(",");
		for (String carrierPax : carrierWisePax) {
			if (carrierPax.length() > 0) {
				String[] carrierPaxDetails = carrierPax.split("\\|");
				if (carrierPaxDetails[0].trim().equalsIgnoreCase(carrierCode)) {
					int pnrPaxIdStartIndex = carrierPaxDetails[1].lastIndexOf("$");
					Integer intPnrPaxId = null;
					pnrPaxIdStartIndex = pnrPaxIdStartIndex + 1;
					if (pnrPaxIdStartIndex > 0) {
						String pnrPaxId = carrierPaxDetails[1].trim().substring(pnrPaxIdStartIndex);
						intPnrPaxId = new Integer(pnrPaxId);
					}
					return intPnrPaxId;
				}
			}
		}
		throw new ModuleException("airreservations.arg.paxNoLongerExist");
	}

	/**
	 * Determines if the passenger represented by the given traveler ref id (format : 3O|A1$1765736) is of given
	 * carrier. (3O part of the example)
	 * 
	 * @param travellerRef
	 *            Traveler reference number representing the passenger
	 * @param carrier
	 *            Carrier code of the carrier which the passenger should belong to
	 * @return boolean value indicating whether the passenger belongs to the specific carrier. true if belongs false
	 *         otherwise
	 */
	public static boolean isPaxOfCarrier(String travellerRef, String carrier) {
		String paxCarrier = travellerRef.split(Pattern.quote("|"))[0].trim();
		return carrier.equalsIgnoreCase(paxCarrier);
	}

	/**
	 * @param travellerRefNo
	 * @return
	 * @throws ModuleException
	 */
	public static Integer getPnrPaxID(String travellerRefNo) throws ModuleException {

		int pnrPaxIdStartIndex = travellerRefNo.lastIndexOf("$");
		Integer intPnrPaxId = null;
		pnrPaxIdStartIndex = pnrPaxIdStartIndex + 1;
		if (pnrPaxIdStartIndex > 0) {
			String pnrPaxId = travellerRefNo.trim().substring(pnrPaxIdStartIndex);
			intPnrPaxId = Integer.valueOf(pnrPaxId);
		}
		return intPnrPaxId;
	}

	public static LCCClientReservationPax getSelectedPax(String travellerRefNo, Collection<LCCClientReservationPax> colPaxes) {
		LCCClientReservationPax passenger = null;
		if (colPaxes != null) {
			for (LCCClientReservationPax reservationPax : colPaxes) {
				if (reservationPax.getTravelerRefNumber().equals(travellerRefNo)) {
					passenger = reservationPax;
					break;
				}
			}
		}
		return passenger;
	}

	/**
	 * TODO
	 * 
	 * @param request
	 *            TODO
	 * 
	 * @return
	 * @throws ParseException
	 */
	public static PaxAccountCreditTO getCreditDetails(LCCClientReservationPax pax) throws ModuleException {

		Collection<CreditInfoDTO> colAvailableCredit = pax.getLccClientPaymentHolder().getCredits();
		PaxAccountCreditTO paxAccountCreditTO = new PaxAccountCreditTO();
		Collection<CreditsListTO> collection = new ArrayList<CreditsListTO>();
		BigDecimal avilCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal expiredCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal actualCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (CreditInfoDTO creditInfo : colAvailableCredit) {
			CreditsListTO creditsListTO = new CreditsListTO();

			String currencyCode = "";

			if (creditInfo.getCurrencyCode() != null) {
				currencyCode = creditInfo.getCurrencyCode();
			}

			// Amount in Operating carrier currency
			creditsListTO.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(creditInfo.getAmount()) + " " + currencyCode);

			BigDecimal mcCreditAmount = creditInfo.getMcAmount();
			if (mcCreditAmount == null) {
				mcCreditAmount = BigDecimal.ZERO;
			}
			creditsListTO.setMcCreditAmount(AccelAeroCalculator.formatAsDecimal(mcCreditAmount));
			creditsListTO.setDisplayExpiryDate(DateFormatUtils.format(creditInfo.getExpireDate(), "dd-MM-yyyy"));
			creditsListTO.setTnxStatus(creditInfo.getEnumStatus().toString());
			creditsListTO.setDisplayNotes(creditInfo.getUserNote());
			creditsListTO.setDisplayTxnID(String.valueOf(creditInfo.getTxnId()));
			creditsListTO.setCalendarDate(DateFormatUtils.format(creditInfo.getExpireDate(), "dd/MM/yyyy"));
			creditsListTO.setCarrierCode(creditInfo.getCarrierCode());
			if (CreditInfoDTO.status.AVAILABLE.equals(creditInfo.getEnumStatus())) {
				avilCredit = AccelAeroCalculator.add(avilCredit, mcCreditAmount);
				if(!creditInfo.isNonRefundableCredit()){
					actualCredit = AccelAeroCalculator.add(actualCredit, mcCreditAmount);
				}
			} else {
				expiredCredit = AccelAeroCalculator.add(expiredCredit, mcCreditAmount);
			}
			creditsListTO.setDisplayCreditID(String.valueOf(creditInfo.getCreditId()));
			creditsListTO.setLccUniqueTnxId(creditInfo.getLccUniqueTnxId());
			collection.add(creditsListTO);
		}
		paxAccountCreditTO.setCurrency(AppSysParamsUtil.getBaseCurrency());
		paxAccountCreditTO.setDisplayAvailableCredit(AccelAeroCalculator.formatAsDecimal(avilCredit));
		paxAccountCreditTO.setDisplayExpiredCredit(AccelAeroCalculator.formatAsDecimal(expiredCredit));
		paxAccountCreditTO.setDisplayActualCredit(AccelAeroCalculator.formatAsDecimal(actualCredit));		
		paxAccountCreditTO.setCredits(collection);

		return paxAccountCreditTO;
	}

	private static Collection<ChargesListTO> setPaxChargesAndFares(LCCClientReservationPax passenger, Integer sequenceStartNo,
			Map<String, BigDecimal> airLineWiseTotals, boolean isGroupPnr, String agentCurrency, Date exchangeRateApplicableDate) throws ModuleException {
		Collection<ChargesListTO> collection = new ArrayList<ChargesListTO>();
		Map chargeRateIdDescMap = DatabaseUtil.getChargeRateIdDescMap();
		PnrPaxChargeHolder chargeHolder = new PnrPaxChargeHolder();

		if (passenger != null) {
			int sequence = sequenceStartNo.intValue();
			// Set fares
			List<FareTO> fareTOs = passenger.getFares();
			Collections.sort(fareTOs);
			for (FareTO fare : fareTOs) {
				ChargesListTO chargesListTOFare = new ChargesListTO();
				chargesListTOFare.setDisplaySegmentID(String.valueOf(sequence));
				chargesListTOFare.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(fare.getAmount()));
				chargesListTOFare.setAgentCurrencyAmount(getAgentCurrencyAmount(fare.getAmount(),
						(exchangeRateApplicableDate != null ? exchangeRateApplicableDate : fare.getApplicableDate()),
						agentCurrency));
				chargesListTOFare.setDisplaySegment(fare.getSegmentCode());
				chargesListTOFare.setDisplayDescription(fare.getDescription());
				chargesListTOFare.setDisplayCarrierCode(fare.getCarrierCode());
				/** Builds airLine wise charges */
				BigDecimal airLineTotal = airLineWiseTotals.get(fare.getCarrierCode());
				airLineTotal = AccelAeroCalculator.add(airLineTotal, fare.getAmount());
				airLineWiseTotals.put(fare.getCarrierCode(), airLineTotal);

				if (fare.getApplicableDate() != null) {
					chargesListTOFare.setDisplayDate(DateFormatUtils.format(fare.getApplicableDate(), "dd-MM-yyyy HH:mm"));
				}
				if (fare.getDepartureDate() != null) {
					chargesListTOFare
							.setDisplayDepartureDate(DateFormatUtils.format(fare.getDepartureDate(), "dd-MM-yyyy HH:mm"));
				}
				collection.add(chargesListTOFare);
				chargeHolder.addCharge(fare);
				sequence++;
			}
			// Add Pax Fees
			List<FeeTO> feeTOs = passenger.getFees();
			Collections.sort(feeTOs);
			for (FeeTO fee : feeTOs) {
				ChargesListTO chargesListTO = new ChargesListTO();
				chargesListTO.setDisplaySegmentID(String.valueOf(sequence));
				chargesListTO.setDisplaySegment(fee.getSegmentCode());
				chargesListTO.setDisplayDescription(fee.getFeeName());
				if (!isGroupPnr) {
					if (fee.getChargeRateId() != null) {
						chargesListTO.setDisplayDescription((String) chargeRateIdDescMap.get(fee.getChargeRateId()));
					}
				}

				chargesListTO.setDisplayCarrierCode(fee.getCarrierCode());
				chargesListTO.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(fee.getAmount()));
				chargesListTO.setAgentCurrencyAmount(getAgentCurrencyAmount(fee.getAmount(), fee.getApplicableTime(),
						agentCurrency));

				/** Builds airLine wise charges */
				BigDecimal airLineTotal = airLineWiseTotals.get(fee.getCarrierCode());
				airLineTotal = AccelAeroCalculator.add(airLineTotal, fee.getAmount());
				airLineWiseTotals.put(fee.getCarrierCode(), airLineTotal);
				// JIRA 6929 - If the reporting group code is available, set it as the description
				if (fee.getReportingChargeGroupCode() != null) {
					chargesListTO.setDisplayDescription(fee.getReportingChargeGroupCode());
				}
				if (fee.getApplicableTime() != null) {
					chargesListTO.setDisplayDate(DateFormatUtils.format(fee.getApplicableTime(), "dd-MM-yyyy HH:mm"));
				}
				if (fee.getDepartureDate() != null) {
					chargesListTO.setDisplayDepartureDate(DateFormatUtils.format(fee.getDepartureDate(), "dd-MM-yyyy HH:mm"));
				}
				collection.add(chargesListTO);
				chargeHolder.addCharge(fee);
				sequence++;
			}
			// Add passenger Taxes
			List<TaxTO> taxTOs = passenger.getTaxes();
			Collections.sort(taxTOs);
			for (TaxTO tax : taxTOs) {
				ChargesListTO chargesListTO = new ChargesListTO();
				chargesListTO.setDisplaySegmentID(String.valueOf(sequence));
				chargesListTO.setDisplaySegment(tax.getSegmentCode());
				chargesListTO.setDisplayDescription(tax.getTaxName());
				if (!isGroupPnr) {
					if (tax.getChargeRateId() != null) {
						chargesListTO.setDisplayDescription((String) chargeRateIdDescMap.get(tax.getChargeRateId()));
					}
				}
				chargesListTO.setDisplayCarrierCode(tax.getCarrierCode());
				chargesListTO.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(tax.getAmount()));
				chargesListTO.setAgentCurrencyAmount(getAgentCurrencyAmount(tax.getAmount(), tax.getApplicableTime(),
						agentCurrency));
				/** Builds airLine wise charges */
				BigDecimal airLineTotal = airLineWiseTotals.get(tax.getCarrierCode());
				airLineTotal = AccelAeroCalculator.add(airLineTotal, tax.getAmount());
				airLineWiseTotals.put(tax.getCarrierCode(), airLineTotal);
				// JIRA 6929 - If the reporting group code is available, set it as the description
				if (tax.getReportingChargeGroupCode() != null) {
					chargesListTO.setDisplayDescription(tax.getReportingChargeGroupCode());
				}
				if (tax.getApplicableTime() != null) {
					chargesListTO.setDisplayDate(DateFormatUtils.format(tax.getApplicableTime(), "dd-MM-yyyy HH:mm"));
				}
				if (tax.getDepartureDate() != null) {
					chargesListTO.setDisplayDepartureDate(DateFormatUtils.format(tax.getDepartureDate(), "dd-MM-yyyy HH:mm"));
				}
				collection.add(chargesListTO);
				chargeHolder.addCharge(tax);
				sequence++;
			}
			// Add passenger surcharges
			List<SurchargeTO> surchargeTOs = passenger.getSurcharges();
			Collections.sort(surchargeTOs);
			for (SurchargeTO surcharge : surchargeTOs) {
				ChargesListTO chargesListTO = new ChargesListTO();
				chargesListTO.setDisplaySegmentID(String.valueOf(sequence));
				chargesListTO.setDisplaySegment(surcharge.getSegmentCode());
				chargesListTO.setDisplayDescription(surcharge.getSurchargeName());
				if (!isGroupPnr) {
					if (surcharge.getChargeRateId() != null) {
						chargesListTO.setDisplayDescription((String) chargeRateIdDescMap.get(surcharge.getChargeRateId()));
					}
				}

				chargesListTO.setDisplayCarrierCode(surcharge.getCarrierCode());
				chargesListTO.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(surcharge.getAmount()));
				chargesListTO.setAgentCurrencyAmount(getAgentCurrencyAmount(surcharge.getAmount(), surcharge.getApplicableTime(),
						agentCurrency));

				/** Builds airLine wise charges */
				BigDecimal airLineTotal = airLineWiseTotals.get(surcharge.getCarrierCode());
				airLineTotal = AccelAeroCalculator.add(airLineTotal, surcharge.getAmount());
				airLineWiseTotals.put(surcharge.getCarrierCode(), airLineTotal);
				// JIRA 6929 - If the reporting group code is available, set it as the description
				if (surcharge.getReportingChargeGroupCode() != null) {
					chargesListTO.setDisplayDescription(surcharge.getReportingChargeGroupCode());
				}
				if (surcharge.getApplicableTime() != null) {
					chargesListTO.setDisplayDate(DateFormatUtils.format(surcharge.getApplicableTime(), "dd-MM-yyyy HH:mm"));
				}
				if (surcharge.getDepartureDate() != null) {
					chargesListTO
							.setDisplayDepartureDate(DateFormatUtils.format(surcharge.getDepartureDate(), "dd-MM-yyyy HH:mm"));
				}
				collection.add(chargesListTO);
				chargeHolder.addCharge(surcharge);
				sequence++;
			}
			sequenceStartNo = new Integer(sequence);
		}
		return collection;
	}

	private static String getAgentCurrencyAmount(BigDecimal amt, Date exchangeRateDate, String agentCurrency)
			throws ModuleException {
		CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(exchangeRateDate).getCurrencyExchangeRate(
				agentCurrency, ApplicationEngine.XBE);
		BigDecimal convertedAmount = AccelAeroRounderPolicy.convertAndRound(amt,
				currencyExchangeRate.getMultiplyingExchangeRate(), new BigDecimal("0.01"), new BigDecimal("0.009"));
		return AccelAeroCalculator.formatAsDecimal(convertedAmount);
	}

	private static Collection<ChargesListTO> setSegmentTotals(ArrayList<ChargesListTO> chargesList,
			Map<String, BigDecimal> airlinewiseTotals, Collection<LCCClientReservationSegment> colsegs) {

		Map<String, ArrayList<ChargesListTO>> segmentWiseChargesMap = new HashMap<String, ArrayList<ChargesListTO>>();
		if (chargesList != null) {
			for (ChargesListTO chargesTO : chargesList) {
				if (!segmentWiseChargesMap.containsKey(chargesTO.getDisplaySegment())) {
					segmentWiseChargesMap.put(chargesTO.getDisplaySegment(), new ArrayList<ChargesListTO>());
				}
			}
		}
		boolean foundSegmentEntry = false;
		for (String segCodeKey : segmentWiseChargesMap.keySet()) {
			ArrayList<ChargesListTO> segmentChargesList = new ArrayList<ChargesListTO>();
			BigDecimal segTotal = new BigDecimal(0.00);
			for (int i = 0; i < chargesList.size(); i++) {
				ChargesListTO chargeTO = chargesList.get(i);
				if (segCodeKey.equalsIgnoreCase(chargeTO.getDisplaySegment())) {
					segTotal = AccelAeroCalculator.add(segTotal, new BigDecimal(chargeTO.getDisplayAmount()));
					foundSegmentEntry = true;
					segmentChargesList.add(chargeTO);
				}
			}
			if (foundSegmentEntry) { // If a matching entry is found & entire list is traversed add it to the map
				ChargesListTO totalElement = new ChargesListTO();
				totalElement.setDisplayDescription("Total (" + segCodeKey + ")");
				totalElement.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(segTotal));
				segmentChargesList.add(totalElement);
				segmentWiseChargesMap.get(segCodeKey).addAll(segmentChargesList);// Add existing list to the specific
																					// map entry
				segmentChargesList = new ArrayList<ChargesListTO>(); // Reset collection
				segTotal = new BigDecimal(0.00); // Reset total
				foundSegmentEntry = false;
			}
		}

		Collection<ChargesListTO> allCharges = new ArrayList<ChargesListTO>();
		allCharges = sortSegmentWiseCharges(segmentWiseChargesMap, colsegs);
		allCharges.addAll(getAirLineWiseCharges(airlinewiseTotals));
		return allCharges;

	}

	/**
	 * Sorts the charges segments according to the departure time of the corresponding segment.
	 * 
	 * @param segmentWiseChargesMap
	 * @param reservation
	 * @return
	 */
	private static Collection<ChargesListTO> sortSegmentWiseCharges(Map<String, ArrayList<ChargesListTO>> segmentWiseChargesMap,
			Collection<LCCClientReservationSegment> colsegs) {

		Set<LCCClientReservationSegment> setSegs = new HashSet<LCCClientReservationSegment>();
		Collection<PaxSortChargesTO> sortDtos = new ArrayList<PaxSortChargesTO>();
		PaxSortChargesTO paxSortChargesTO = null;

		for (LCCClientReservationSegment seg : colsegs) {
			setSegs.add(seg);
		}
		LCCClientReservationSegment[] segArray = SortUtil.sort(setSegs);
		Collection<ChargesListTO> chargesList = new ArrayList<ChargesListTO>();
		int segLength = segArray.length;
		String[] selsegs = new String[segLength + 5];
		int slSeq = 0;
		for (int i = 0; i < segLength; i++) {
			LCCClientReservationSegment resSegment = segArray[i];
			selsegs[slSeq] = resSegment.getSegmentCode();
			slSeq++;

		}
		// ----------------------------------------

		for (String segmentCode : segmentWiseChargesMap.keySet()) {
			paxSortChargesTO = new PaxSortChargesTO();
			paxSortChargesTO.setSegmentCode(segmentCode);
			paxSortChargesTO.setChargeList(segmentWiseChargesMap.get(segmentCode));
			for (int i = 0; i < slSeq; i++) {

				if (selsegs[i].equalsIgnoreCase(segmentCode)) {
					paxSortChargesTO.setSequence(i);

				} else if (segmentCode.split("/").length > 2 && segmentCode.startsWith(selsegs[i])) {
					String newSegcode = getSegmentCodeFromChargeApplicableSegments(segmentCode, selsegs[i], selsegs, i);
					if (!newSegcode.equals("")) {
						paxSortChargesTO.setSequence(i);
					}
				}
			}
			sortDtos.add(paxSortChargesTO);
		}

		int size = sortDtos.size();
		PaxSortChargesTO[] arrayToSort = sortDtos.toArray(new PaxSortChargesTO[size]);
		Arrays.sort(arrayToSort, new Comparator<PaxSortChargesTO>() {
			@Override
			public int compare(PaxSortChargesTO rs1, PaxSortChargesTO rs2) {
				return rs1.getSequence().compareTo(rs2.getSequence());
			}
		});

		for (int al = 0; al < arrayToSort.length; al++) {
			chargesList.addAll(arrayToSort[al].getChargeList());
		}

		return chargesList;
	}

	/**
	 * Gets the flight segment code from charge applicable segment codes
	 * 
	 * @param segCodes
	 * @return
	 */
	private static String getSegmentCodeFromChargeApplicableSegments(String matchinCode, String arrSegcode, String[] segArray,
			int j) {
		if (segArray[j + 1] == null)
			return "";
		arrSegcode = arrSegcode + segArray[j + 1].substring(segArray[j + 1].indexOf("/"));
		if (matchinCode.equalsIgnoreCase(arrSegcode)) {
			return matchinCode;
		} else {
			return getSegmentCodeFromChargeApplicableSegments(matchinCode, arrSegcode, segArray, j + 1);
		}
	}

	/**
	 * This method will create ChargesLIstTO element for each carrier code with the total amount for the corresponding
	 * carrier.
	 * 
	 * @param airlinewiseTotals
	 * @return
	 */
	private static Collection<ChargesListTO> getAirLineWiseCharges(Map<String, BigDecimal> airlinewiseTotals) {
		Collection<ChargesListTO> airLineCharges = new ArrayList<ChargesListTO>();
		for (String airLine : airlinewiseTotals.keySet()) {
			ChargesListTO chargesListTO = new ChargesListTO();
			chargesListTO.setDisplayCarrierCode(airLine);
			chargesListTO.setDisplayDescription("Airline Total(" + airLine + ")");
			chargesListTO.setDisplayAmount(AccelAeroCalculator.formatAsDecimal(airlinewiseTotals.get(airLine)));
			airLineCharges.add(chargesListTO);
		}

		return airLineCharges;
	}

	private static List<PaymentListTO> sortPaxPayments(List<PaymentListTO> payList) {
		Collections.sort(payList, new Comparator<PaymentListTO>() {
			@Override
			public int compare(PaymentListTO list1, PaymentListTO list2) {
				return (list1.getHdnPaydate().compareTo(list2.getHdnPaydate()));
			}
		});
		return payList;
	}

	/**
	 * Returns a collection of {@link ChargeAdjustmentType} collection for each carrier in the reservation.
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<CarrierwiseChargeAdjustments> getChargeAdjustmentTypes(boolean isGroupPnr, Set<String> carrierCodes,
			BasicTrackInfo trackInfo) throws ModuleException {

		Collection<CarrierwiseChargeAdjustments> carrierWiseChargeAdjustmentTypes = new ArrayList<CarrierwiseChargeAdjustments>();

		if (isGroupPnr) {
			Set<String> otherCarrierCodes = new HashSet<String>();

			for (String carrierCode : carrierCodes) {
				if (AppSysParamsUtil.getDefaultCarrierCode().equals(carrierCode)) {
					carrierWiseChargeAdjustmentTypes.add(getOwnCarrierChargeAdjustmentTypes());
				} else {
					otherCarrierCodes.add(carrierCode);
				}
			}

			Map<String, Collection<ChargeAdjustmentTypeDTO>> carrierwiseChargeAdjustmentsMap = ModuleServiceLocator
					.getAirproxyReservationBD().getCarrierWiseChargeAdjustmentTypes(otherCarrierCodes, trackInfo);

			for (String carrierCode : carrierwiseChargeAdjustmentsMap.keySet()) {
				CarrierwiseChargeAdjustments otherCarrierAdjustmentTypes = new CarrierwiseChargeAdjustments();
				otherCarrierAdjustmentTypes.setCarrierCode(carrierCode);
				otherCarrierAdjustmentTypes.getChargeAdjustmentTypes().addAll(carrierwiseChargeAdjustmentsMap.get(carrierCode));
				carrierWiseChargeAdjustmentTypes.add(otherCarrierAdjustmentTypes);
			}

		} else {
			carrierWiseChargeAdjustmentTypes.add(getOwnCarrierChargeAdjustmentTypes());
		}

		return carrierWiseChargeAdjustmentTypes;
	}

	/**
	 * @return
	 * @throws ModuleException
	 */
	private static CarrierwiseChargeAdjustments getOwnCarrierChargeAdjustmentTypes() throws ModuleException {

		Collection<ChargeAdjustmentTypeDTO> chargeAdjustmentTypes = CommonsServices.getGlobalConfig().getChargeAdjustmentTypes();

		CarrierwiseChargeAdjustments ownCarrierAdjustmentTypes = new CarrierwiseChargeAdjustments();
		ownCarrierAdjustmentTypes.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		ownCarrierAdjustmentTypes.getChargeAdjustmentTypes().addAll(chargeAdjustmentTypes);

		return ownCarrierAdjustmentTypes;
	}
}
