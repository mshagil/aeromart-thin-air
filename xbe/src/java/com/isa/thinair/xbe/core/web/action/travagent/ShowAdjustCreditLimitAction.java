package com.isa.thinair.xbe.core.web.action.travagent;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.travagent.TravelAgentRequestHandler;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.TravAgent.CREDIT_LIMIT_GRID),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.TravAgent.CREDIT_LIMIT_GRID),
		@Result(name = S2Constants.Result.CREDIT_LIMIT, value = S2Constants.Jsp.TravAgent.CREDIT_LIMIT_GRID) })
public class ShowAdjustCreditLimitAction extends BaseRequestAwareAction {

	public String execute() {
		return TravelAgentRequestHandler.execute(request);
	}
}