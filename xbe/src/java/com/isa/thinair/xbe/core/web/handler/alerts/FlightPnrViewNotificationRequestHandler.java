package com.isa.thinair.xbe.core.web.handler.alerts;

import com.isa.thinair.airreservation.api.dto.ItineraryLayoutModesDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airschedules.api.dto.FlightAlertInfoDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.utils.AlertUtil;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.alerting.api.dto.FlightNotificationDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrNotificationDTO;
import com.isa.thinair.alerting.api.model.FlightNotification;
import com.isa.thinair.alerting.core.bl.NotifyReservations;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.alerts.FlightPnrViewNotificationHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

/**
 * Created by hasika on 6/11/18.
 */
public class FlightPnrViewNotificationRequestHandler extends BasicRH {

    private static Log log = LogFactory.getLog(FlightPnrNotificationRequestHandler.class);

    // private static final String PARAM_VERSION = "hdnVersion";
    private static final String PARAM_MODE = "hdnMode";
    private static final String PARAM_ACTION = "hdnAction";
    private static final String PARAM_SMS_PNR_SEGMENT_LIST = "hdnSmsPnrSegList";
    private static final String PARAM_EMAIL_PNR_SEGMENT_LIST = "hdnEmailPnrSegList";
    private static final String PARAM_FLIGHT_ID = "hdnFlightId";
    private static final String PARAM_FLIGHT_NUMBER = "txtFlightNo";
    private static final String PARAM_DEP_DATE = "hdnDepDate";
    private static final String PARAM_CHANNEL = "selNotifyChannel";
    private static final String PARAM_CODE = "hdnNtCode";
    private static final String PARAM_MSG = "hdnNtMessage";
    private static final String PARAM_FIELD_INFO = "hdnFields";
    private static final String PARAM_PNR = "txtPnr";
    private static final String PARAM_FLIGHT_PNR_DTO = "flightPnrDetailDTO";
    private static final String PARAM_NEW_DEP_DATE = "txtNewDepDate";
    private static final String PARAM_NEW_DEP_TIME = "txtDepTime1";
    private static final String PARAM_GEN_LANGUAGE = "selGenLanguage";
    private static final String PARAM_ANCI_REPROTECT_STATUS = "selAnciReprotectStatus";

    private static final String REQ_NOTIFICATION_CODE_LIST = "reqNotificationCodeList";
    private static final String REQ_NOTIFICATION_MSG_LIST = "reqNotificationMsgList";
    private static final String REQ_PREFERRED_LANGUAGE_LIST = "reqPrefLanguageList";
    private static final String REQ_FINAL_NOTIFICATION_MSG = "txtFinalMessage";

    private static final String ACTION_VIEWMSG = "VIEWMSG";
    private static final String ACTION_VIEWSUMMARY = "VIEWSUMMARY";
    private static final String ACTION_VIEWDETAILS = "VIEWDETAILS";
    private static final String ACTION_EDITMESSAGE = "EDITMSG";
    private static final String ACTION_UPDATE_CONTACT_DATA = "UPDATE_CONTACT_DATA";
	private static final String ACTION_VIEW_MESSAGE_LIST = "VIEWMESSAGELIST";

    private static final int SMS_MESSAGE_ALLOWED_LENGTH = 160;

    private static final String SMS_ONLY = "1";
    private static final String EMAIL_ONLY = "2";
    private static final String SMS_AND_EMAIL = "3";

    private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";
    private static final String DATE_FORMAT_HHMM = "HH:mm";

    private static final String SAMPLE_PNR = "XXXXXXXX";
    private static final String PNR_PLACEHOLDER = "\\$pnr";
    private static final String STR_ITINERARY = "chkItinerary";

    // helpers
    private static LocalZuluTimeAdder timeAdder;

    /**
     *
     * @param request
     * @return
     */
    public static String execute(HttpServletRequest request) {
        String forward = S2Constants.Result.SUCCESS;
        timeAdder = new LocalZuluTimeAdder(ModuleServiceLocator.getAirportServiceBD());
        String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
        UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

        String strAction = null;

        boolean blnSms = false;
        boolean blnEmail = false;

        boolean notificationSent = false;
        try {
            setAirportComboList(request);
            strAction = request.getParameter(PARAM_ACTION);
            if (strAction != null) {
                if (strAction.equalsIgnoreCase(WebConstants.ACTION_LOAD)) {
                    setDisplayData(request);
                    request.setAttribute(WebConstants.PARAM_ALERT_INFO, "SEARCHCALLED");
                } else if (strAction.equalsIgnoreCase(WebConstants.ACTION_SEND)
                        || strAction.equalsIgnoreCase(WebConstants.ACTION_GENERATE) || strAction.equalsIgnoreCase(ACTION_VIEWMSG)) {
                    checkPrivilege(request, WebConstants.PRIV_NOTIFICATION_SEND);
                    String strMode = request.getParameter(PARAM_MODE);
                    String strChannel = request.getParameter(PARAM_CHANNEL);
                    String strFlightId = request.getParameter(PARAM_FLIGHT_ID);
                    String strSmsPnrSegList = request.getParameter(PARAM_SMS_PNR_SEGMENT_LIST);
                    String strEmailPnrSegList = request.getParameter(PARAM_EMAIL_PNR_SEGMENT_LIST);
                    String strDepDate = request.getParameter(PARAM_DEP_DATE);
                    String strCode = request.getParameter(PARAM_CODE);
                    String msgContent = request.getParameter(PARAM_MSG);
                    String strFltNum = request.getParameter(PARAM_FLIGHT_NUMBER);
                    String genLang = request.getParameter(PARAM_GEN_LANGUAGE);
                    String anciReprotectStatus = request.getParameter(PARAM_ANCI_REPROTECT_STATUS);

                    String depDate = (strDepDate.trim().equalsIgnoreCase("")) ? null : strDepDate;
                    Date fltDepDateLocal = null;
                    try {
                        if (depDate != null)
                            fltDepDateLocal = CalendarUtil.getParsedTime(depDate, DATE_FORMAT_DDMMYYYY);
                    } catch (ParseException e) {
                        log.error("execute() Invalid Date - Parse Exception", e);
                    }

                    int flightId = -1;
                    if (strFlightId != null && !"".equals(strFlightId))
                        flightId = Integer.parseInt(strFlightId);
                    else
                        flightId = ModuleServiceLocator.getAlertingBD().getFlightID(strFltNum.toUpperCase(), fltDepDateLocal);

                    Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(flightId);
                    flight = timeAdder.addLocalTimeDetailsToFlight(flight);

                    ArrayList<String> smsPnrSegList = new ArrayList<String>();
                    ArrayList<String> emailPnrSegList = new ArrayList<String>();

                    ArrayList<FlightPnrNotificationDTO> colFlightPnrDTO = new ArrayList<FlightPnrNotificationDTO>();

                    if (strSmsPnrSegList != null && !"".equals(strSmsPnrSegList)) {
                        String[] arrSms = strSmsPnrSegList.split(",");
                        if (arrSms != null) {
                            for (int k = 0; k < arrSms.length; k++)
                                smsPnrSegList.add(arrSms[k]);
                        }
                    }

                    if (strEmailPnrSegList != null && !"".equals(strEmailPnrSegList)) {
                        String[] arrEmail = strEmailPnrSegList.split(",");
                        if (arrEmail != null) {
                            for (int k = 0; k < arrEmail.length; k++)
                                emailPnrSegList.add(arrEmail[k]);
                        }
                    }

                    if (strChannel != null && !strChannel.equals("")) {
                        if (strChannel.equals(SMS_ONLY)) {
                            blnSms = true;
                            blnEmail = false;
                        } else if (strChannel.equals(EMAIL_ONLY)) {
                            blnSms = false;
                            blnEmail = true;
                        } else if (strChannel.equals(SMS_AND_EMAIL)) {
                            blnSms = true;
                            blnEmail = true;
                        }

                        // HashMap hmPnrDetails = (HashMap) request.getSession().getAttribute("flightPnrDetailDTO");
                        // The below code is called again in-order to avoid setting the objects in session, due to
                        // memory issue
                        FlightPnrViewNotificationHTMLGenerator htmlGen = new FlightPnrViewNotificationHTMLGenerator();
                        htmlGen.getPnrRowHtml(request);
                        HashMap<Integer, FlightPnrNotificationDTO> hmPnrDetails = (HashMap<Integer, FlightPnrNotificationDTO>) request
                                .getAttribute(PARAM_FLIGHT_PNR_DTO);

                        // Setting the selected PNRs for sending sms/email
                        if (hmPnrDetails != null) {
                            for (int i = 0; i < smsPnrSegList.size(); i++) {
                                Integer key = Integer.valueOf((String) smsPnrSegList.get(i));
                                if (hmPnrDetails.containsKey(key)) {
                                    FlightPnrNotificationDTO flightPnrDTO = (FlightPnrNotificationDTO) hmPnrDetails.get(key);
                                    flightPnrDTO.setSendSms(blnSms);

                                    if (emailPnrSegList.contains(smsPnrSegList.get(i)))
                                        flightPnrDTO.setSendEmail(blnEmail);
                                    colFlightPnrDTO.add(flightPnrDTO);
                                }

                            }

                            for (int i = 0; i < emailPnrSegList.size(); i++) {
                                Integer key = Integer.valueOf((String) emailPnrSegList.get(i));
                                if (hmPnrDetails.containsKey(key) && !smsPnrSegList.contains((String) emailPnrSegList.get(i))) {
                                    FlightPnrNotificationDTO flightPnrDTO = (FlightPnrNotificationDTO) hmPnrDetails.get(key);
                                    flightPnrDTO.setSendEmail(blnEmail);
                                    colFlightPnrDTO.add(flightPnrDTO);
                                }

                            }

                        }

                        FlightNotificationDTO fltDTO = new FlightNotificationDTO();
                        fltDTO.setNotifyByEmail(blnEmail);
                        fltDTO.setNotifyBySms(blnSms);
                        fltDTO.setPreferredLanguage(genLang);

                        if (strMode.equals(WebConstants.ACTION_CANCEL_FLIGHT)) {
                            FlightAlertInfoDTO oldFlightAlertInfoDTO = AlertUtil.copyFlightDetailsToFlightAlertInfoDTO(flight);
                            fltDTO.setOldFlightAlertInfoDTO(oldFlightAlertInfoDTO);
                            fltDTO.setNewFlightAlertInfoDTO(null);
                            fltDTO.setAlertDetails(AlertUtil.prepareFlightSegmentAlertDto(oldFlightAlertInfoDTO, null));
                            fltDTO.setAction(strMode);
                        } else if (strMode.equals(WebConstants.ACTION_REPROTECT_FLIGHT)) {
                            FlightAlertInfoDTO oldFlightAlertInfoDTO = AlertUtil.copyFlightDetailsToFlightAlertInfoDTO(flight);
                            fltDTO.setOldFlightAlertInfoDTO(oldFlightAlertInfoDTO);
                            fltDTO.setNewFlightAlertInfoDTO(null);
                            oldFlightAlertInfoDTO.setAction(strMode);
                            fltDTO.setAlertDetails(AlertUtil.prepareFlightSegmentAlertDto(oldFlightAlertInfoDTO, null));
                            fltDTO.setAction(strMode);
                        } else if (strMode.equals(WebConstants.ACTION_UPDATE_FLIGHT)) {

                            String strNewDepDate = request.getParameter(PARAM_NEW_DEP_DATE);
                            Date dtNewDepDate = null;

                            String strNewDepTime = request.getParameter(PARAM_NEW_DEP_TIME);
                            Date dtNewDepTime = null;

                            try {
                                if (strNewDepDate != null && !"".equals(strNewDepDate))
                                    dtNewDepDate = CalendarUtil.getParsedTime(strNewDepDate, DATE_FORMAT_DDMMYYYY);
                                if (strNewDepTime != null && !"".equals(strNewDepTime))
                                    dtNewDepTime = CalendarUtil.getParsedTime(strNewDepTime, DATE_FORMAT_HHMM);
                            } catch (ParseException e) {
                                log.error("execute() Invalid Date - Parse Exception", e);

                            }

                            Flight oldFlight = FlightUtil.getCopyOfFlight(flight);
                            oldFlight = timeAdder.addLocalTimeDetailsToFlight(oldFlight);

                            Flight newFlight = FlightUtil.getCopyOfFlight(flight);
                            newFlight = timeAdder.addLocalTimeDetailsToFlight(newFlight);

                            Date oldDepartureDate = CalendarUtil.getTimeAddedDate(oldFlight.getDepartureDateLocal(), LegUtil
                                    .getFirstFlightLeg(oldFlight.getFlightLegs()).getEstDepartureTimeLocal());

                            if (dtNewDepDate != null && dtNewDepTime != null) {
                                Date newDepartureDate = CalendarUtil.getTimeAddedDate(dtNewDepDate,
                                        CalendarUtil.getOnlyTime(dtNewDepTime));

                                newFlight.setDepartureDateLocal(newDepartureDate);
                                FlightLeg newLeg = LegUtil.getFirstFlightLeg(newFlight.getFlightLegs());
                                newLeg.setEstDepartureTimeLocal(CalendarUtil.getOnlyTime(dtNewDepTime));

                                long timeDiff = CalendarUtil.getTimeDifferenceInMillis(
                                        CalendarUtil.getOnlyTime(oldDepartureDate), CalendarUtil.getOnlyTime(newDepartureDate));
                                Date newArrTime = new Date(newLeg.getEstArrivalTimeLocal().getTime() + timeDiff);
                                newLeg.setEstArrivalTimeLocal(CalendarUtil.getOnlyTime(newArrTime));
                            }

                            newFlight = timeAdder.addZuluTimeDetailsforFlight(newFlight);

                            FlightAlertInfoDTO oldFlightAlertInfoDTO = AlertUtil.copyFlightDetailsToFlightAlertInfoDTO(oldFlight);
                            FlightAlertInfoDTO newFlightAlertInfoDTO = AlertUtil.copyFlightDetailsToFlightAlertInfoDTO(newFlight);
                            fltDTO.setOldFlightAlertInfoDTO(oldFlightAlertInfoDTO);
                            fltDTO.setNewFlightAlertInfoDTO(newFlightAlertInfoDTO);

                            fltDTO.setAlertDetails(AlertUtil.prepareFlightSegmentAlertDto(oldFlightAlertInfoDTO,
                                    newFlightAlertInfoDTO));
                            fltDTO.setAction(strMode);
                        }

                        if (strAction.equalsIgnoreCase(WebConstants.ACTION_GENERATE)) {
                            forward = S2Constants.Result.GET_XBE_DATA;
                            // NotifyReservations.setMessageDesc(fltDTO);

                            FlightPnrNotificationDTO noteDto = null;
                            if (colFlightPnrDTO != null) {
                                if (colFlightPnrDTO.size() == 1) {
                                    Iterator<FlightPnrNotificationDTO> iter = colFlightPnrDTO.iterator();
                                    noteDto = (FlightPnrNotificationDTO) iter.next();
                                }
                            }

                            fltDTO.setSmsMessageDesc(NotifyReservations.getGeneratedNotificationMessage(fltDTO, noteDto, null));
                            String code = "";
                            if (strMode.equals(WebConstants.ACTION_CANCEL_FLIGHT))
                                code = generateNotificationCode(flight.getFlightNumber(), strDepDate, "CNX");
                            else if (strMode.equals(WebConstants.ACTION_UPDATE_FLIGHT)) {
                                if (fltDTO.getAlertDetails().keySet().size() == 0) {
                                    code = "ERR-DATETIME";
                                } else {
                                    code = generateNotificationCode(flight.getFlightNumber(), strDepDate, "CHG");
                                }
                            } else if (strMode.equals(WebConstants.ACTION_REPROTECT_FLIGHT))
                                code = generateNotificationCode(flight.getFlightNumber(), strDepDate, "RPT");
                            request.setAttribute(WebConstants.REQ_RESPONSE_STRING, code + "###" + fltDTO.getSmsMessageDesc());
                            // request.setAttribute(WebConstants.REQ_RESPONSE_STRING, "{'code':'" + code +
                            // "','message':'" + fltDTO.getSmsMessageDesc() + "'}");

                        } else if (strAction.equalsIgnoreCase(ACTION_VIEWMSG)) {
							/*
							 * if(msgContent != null && !"".equals(msgContent)){ fltDTO.setSmsMessageDesc(msgContent); }
							 *
							 * FlightPnrNotificationDTO noteDto = null; if(colFlightPnrDTO != null){
							 * if(colFlightPnrDTO.size() == 1) { Iterator<FlightPnrNotificationDTO> iter =
							 * colFlightPnrDTO.iterator(); noteDto = (FlightPnrNotificationDTO) iter.next(); } } String
							 * msg = NotifyReservations.getGeneratedNotificationMessage(fltDTO, noteDto);
							 */

                            String msg = "";

                            if (msgContent != null && !"".equals(msgContent)) {
                                msg = msgContent;
                            } else {

                                FlightPnrNotificationDTO noteDto = null;
                                if (colFlightPnrDTO != null) {
                                    if (colFlightPnrDTO.size() == 1) {
                                        Iterator<FlightPnrNotificationDTO> iter = colFlightPnrDTO.iterator();
                                        noteDto = (FlightPnrNotificationDTO) iter.next();
                                    }
                                }

								msg = NotifyReservations.getGeneratedNotificationMessage(fltDTO, noteDto, null);
                            }

                            if (msg == null) {
                                msg = "";
                            } else {
                                if (fltDTO.isNotifyBySms()) {
                                    String tempMsg = msg.replaceAll(PNR_PLACEHOLDER, SAMPLE_PNR);
                                    if (tempMsg.length() >= SMS_MESSAGE_ALLOWED_LENGTH) {
                                        tempMsg = tempMsg.substring(0, SMS_MESSAGE_ALLOWED_LENGTH);
                                    }

                                    msg = tempMsg.replaceAll(SAMPLE_PNR, PNR_PLACEHOLDER);
                                }
                            }
                            request.setAttribute(REQ_FINAL_NOTIFICATION_MSG, msg);
                        } else if (strAction.equalsIgnoreCase(WebConstants.ACTION_SEND)) {
                            if (msgContent != null && !"".equals(msgContent)) {

                                if (fltDTO.isNotifyBySms()) {
                                    String unescapedHtml = StringEscapeUtils.unescapeHtml(msgContent);
                                    if(msgContent.equals(unescapedHtml) && msgContent.length() >= SMS_MESSAGE_ALLOWED_LENGTH)
                                        msgContent = msgContent.substring(0, SMS_MESSAGE_ALLOWED_LENGTH);
                                }

                                fltDTO.setSmsMessageDesc(msgContent);
                                fltDTO.setCurrentNotificationMsg(msgContent);
                            }

                            if (strCode != null && !"".equals(strCode)) {
                                fltDTO.setCurrentNotificationCode(strCode);
                            } else {
                                String code = "";
                                if (strMode.equals(WebConstants.ACTION_CANCEL_FLIGHT)) {
                                    code = generateNotificationCode(flight.getFlightNumber(), strDepDate, "CNX");
                                } else if (strMode.equals(WebConstants.ACTION_UPDATE_FLIGHT)) {
                                    code = generateNotificationCode(flight.getFlightNumber(), strDepDate, "CHG");
                                } else if (strMode.equals(WebConstants.ACTION_REPROTECT_FLIGHT)) {
                                    code = generateNotificationCode(flight.getFlightNumber(), strDepDate, "RPT");
                                }
                                fltDTO.setCurrentNotificationCode(code);
                            }

							String from = ModuleServiceLocator.getAirportBD().getCachedAirport(fltDTO.getOldFlightAlertInfoDTO().getOriginAptCode()).getAirportName();
							String to = ModuleServiceLocator.getAirportBD().getCachedAirport(fltDTO.getOldFlightAlertInfoDTO().getDestinationAptCode()).getAirportName();

							fltDTO.setDepartureAirport(from);
							fltDTO.setArrivalAirport(to);

                            ModuleServiceLocator.getAlertingBD().notifyReservations(flightId, fltDTO, colFlightPnrDTO,
                                    userPrincipal);
                            notificationSent = true;
                            if (request.getParameter(STR_ITINERARY) != null) {
                                if (request.getParameter(STR_ITINERARY).equals(WebConstants.ITINERARY)) {
                                    if (colFlightPnrDTO != null) {
                                        ReservationBD resBd = ModuleServiceLocator.getReservationBD();
                                        Iterator<FlightPnrNotificationDTO> iter = colFlightPnrDTO.iterator();
                                        while (iter.hasNext()) {
                                            FlightPnrNotificationDTO noteDto = (FlightPnrNotificationDTO) iter.next();
                                            String pnr = noteDto.getPnr();
                                            ItineraryLayoutModesDTO itineraryLayoutModesDTO = new ItineraryLayoutModesDTO();
                                            itineraryLayoutModesDTO.setPnr(pnr);
                                            itineraryLayoutModesDTO.setLocale(new Locale("en"));
                                            itineraryLayoutModesDTO.setIncludePassengerPrices(false);
                                            resBd.emailItinerary(itineraryLayoutModesDTO, null, null, false);
                                        }
                                    }
                                }
                            }
                            if (notificationSent) {
                                saveMessage(request, XBEConfig.getServerMessage(WebConstants.KEY_NOTIFICATIONS_SEND_SUCCESS, userLanguage, null),
                                        MSG_TYPE_CONFIRMATION);
                                request.setAttribute(WebConstants.MSG_SUCCESS, "1");
                            }
                        }
                    }

                    if (!strAction.equalsIgnoreCase(WebConstants.ACTION_GENERATE)) {
                        setDisplayData(request);
                    }
                    request.setAttribute(PARAM_FLIGHT_ID, String.valueOf(flightId));
                } else if (strAction.equals(ACTION_EDITMESSAGE)) {
                    checkPrivilege(request, WebConstants.PRIV_NOTIFICATION_EDIT);
                    request.setAttribute(WebConstants.REQ_RESPONSE_STRING, strAction);
                    forward = S2Constants.Result.GET_XBE_DATA;
                } else if (strAction.equalsIgnoreCase(ACTION_VIEWSUMMARY) || strAction.equalsIgnoreCase(ACTION_VIEWDETAILS)) {
                    checkPrivilege(request, WebConstants.PRIV_NOTIFICATION_VIEW);
                    setDisplayData(request);
                    if (strAction.equalsIgnoreCase(ACTION_VIEWDETAILS))
                        forward = S2Constants.Result.GET_XBE_DATA;
                } else if(strAction.equals(ACTION_UPDATE_CONTACT_DATA)){
                    updateContactDetailsNotification(request);
				} else if (strAction.equals(ACTION_VIEW_MESSAGE_LIST)) {
					setDisplayDataForMessageList(request);
					forward = S2Constants.Result.GET_XBE_DATA;
				}

            }

        } catch (ModuleException exception) {
            log.error("Exception in FlightPnrNotificationRequestHandler.execute [orignal module=" + exception.getModuleDesc()
                    + "]", exception);
            String strErrorMessage = exception.getMessageString();
            saveMessage(request, strErrorMessage, WebConstants.MSG_ERROR);

        } catch (Exception ex) {
            forward = S2Constants.Result.ERROR;
            log.error("FlightPnrNotificationRequestHandler execute()", ex);
            request.setAttribute(WebConstants.REQ_RESPONSE_STRING, "ERROR;An Error Occured");
        }

        Map<String, String> mapLang = AppSysParamsUtil.getIBESupportedLanguages();
        Set colKey = mapLang.keySet();
        StringBuffer langStr = new StringBuffer();
        for (Iterator iter = colKey.iterator(); iter.hasNext();) {
            String key = (String) iter.next();
            String langName = mapLang.get(key);
            langStr.append("<option value='");
            langStr.append(key);
            langStr.append("'");

            langStr.append(">");
            langStr.append(langName);
            langStr.append("</option>");
        }

        request.setAttribute(REQ_PREFERRED_LANGUAGE_LIST, langStr);

        return forward;
    }

    /**
     * set data to show in the jsp file
     *
     * @param request
     */
    private static void setDisplayData(HttpServletRequest request) throws ModuleException {

        String strAction = request.getParameter(PARAM_ACTION);
        if (strAction.equalsIgnoreCase(ACTION_VIEWSUMMARY))
            setNotificationSummaryRowHtml(request);
        else if (strAction.equalsIgnoreCase(ACTION_VIEWDETAILS)) {
            setNotificationSummaryRowHtml(request);
            setNotificationDetailRowHtml(request);
        } else {
            setPnrRowHtml(request);
        }

        int flightId = -1;
        String strFlightId = request.getAttribute(PARAM_FLIGHT_ID) == null ?
                request.getParameter(PARAM_FLIGHT_ID) :
                (String) request.getAttribute(PARAM_FLIGHT_ID);



        if (strFlightId == null || "".equals(strFlightId) || "-1".equals(strFlightId)) {
            strFlightId = "-1";
        }

        StringBuilder sb = new StringBuilder();
        StringBuilder sbData = new StringBuilder();
        sbData.append("var arrNCodes = new Array();");

        for(String sFlightId : strFlightId.split(",")) {
            String strPnr = request.getParameter(PARAM_PNR);
            if (strPnr == null || "".equals(strPnr)) {
                strPnr = null;
            }


            if (!"".equals(strFlightId))
                flightId = Integer.parseInt(sFlightId.trim());
            else
                flightId = -1;

            ArrayList aList = ModuleServiceLocator.getAlertingBD().getAllNotificationsForFlight(flightId, strPnr);
            if (aList != null) {
                for (int j = 0; j < aList.size(); j++) {
                    FlightNotification objFn = (FlightNotification) aList.get(j);
                    sb.append("<option value='").append(objFn.getFlightNotificationId()).append("'>").append(objFn.getNotificationCode()).append("</option>");
                    sbData.append("arrNCodes [").append(j).append("] = new Array();");
                    sbData.append("arrNCodes [").append(j).append("][1] = ' ").append(objFn.getFlightNotificationId()).append("';");

                    String msg = "";
                    if (objFn.getMessage() != null) {
                        StringBuilder strOut = new StringBuilder();
                        String aux;
                        try {
                            BufferedReader br = new BufferedReader(objFn.getMessage().getCharacterStream());
                            while ((aux = br.readLine()) != null) {
                                strOut.append(aux);
                            }
                        } catch (IOException e) {
                            log.error("IOException in reading Clob message", e);
                        } catch (SQLException se) {
                            log.error("SQL Exception in reading Clob message", se);
                        }

                        msg = strOut.toString();
                    }

                    if (objFn.getLanguage() != null && (!objFn.getLanguage().equals(Locale.ENGLISH.toString()))) {
                        msg = StringUtil.getUnicode(msg);
                    }
                    msg = StringUtil.escapeStr(msg, "'", "\\\\'");
                    msg = StringUtil.escapeStr(msg, "\n|\r|\r\n", " ");
                    sbData.append("arrNCodes [").append(j).append("][2] = ' ").append(msg).append("';");
                }
            }
        }

        request.setAttribute(PARAM_FIELD_INFO, request.getParameter(PARAM_FIELD_INFO));
        request.setAttribute(REQ_NOTIFICATION_CODE_LIST, sb.toString());
        request.setAttribute(REQ_NOTIFICATION_MSG_LIST, sbData.toString());

    }

    /**
     * sets the html rows for PNR list into request variable
     *
     * @param request
     */
    private static void setPnrRowHtml(HttpServletRequest request) {
        try {
            FlightPnrViewNotificationHTMLGenerator htmlGen = new FlightPnrViewNotificationHTMLGenerator();
            String htmlRows = htmlGen.getPnrRowHtml(request);
            request.setAttribute(WebConstants.REQ_HTML_ROWS, htmlRows);
        } catch (ModuleException moduleException) {
            log.error("FlightPnrNotificationRequestHandler.setPnrRowHtml() failed - ", moduleException);
            saveMessage(request, moduleException.getMessageString(), MSG_TYPE_ERROR);
        }
    }

    /**
     * sets the html rows for Notification summary list into request variable
     *
     * @param request
     */
    private static void setNotificationSummaryRowHtml(HttpServletRequest request) {
        try {
            FlightPnrViewNotificationHTMLGenerator htmlGen = new FlightPnrViewNotificationHTMLGenerator();
            String htmlRows = htmlGen.getNotificationSummaryRowHtml(request);
            request.setAttribute(WebConstants.REQ_HTML_ROWS, htmlRows);
        } catch (ModuleException moduleException) {
            log.error("FlightPnrNotificationRequestHandler.setNotificationSummaryRowHtml() failed - ", moduleException);
            saveMessage(request, moduleException.getMessageString(), MSG_TYPE_ERROR);
        }
    }

    /**
     * sets the html rows for Notification summary list into request variable
     *
     * @param request
     */
    private static void setNotificationDetailRowHtml(HttpServletRequest request) {
        try {
            FlightPnrViewNotificationHTMLGenerator htmlGen = new FlightPnrViewNotificationHTMLGenerator();
            String htmlRows = htmlGen.getNotificationDetailRowHtml(request);
            request.setAttribute(WebConstants.REQ_RESPONSE_STRING, htmlRows);
        } catch (ModuleException moduleException) {
            log.error("FlightPnrNotificationRequestHandler.setNotificationDetailRowHtml() failed - ", moduleException);
            saveMessage(request, moduleException.getMessageString(), MSG_TYPE_ERROR);
        }
    }

    private static String generateNotificationCode(String fltNumber, String depDate, String action) throws ModuleException {
        StringBuffer sb = new StringBuffer();
        if (fltNumber == null || depDate == null || action == null)
            return null;
        sb.append(fltNumber).append("_").append(depDate.replace("/", "")).append("_").append(action);
        String nextCode = ModuleServiceLocator.getAlertingBD().getNextNotificationCode(sb.toString());
        return nextCode;
    }

    /**
     *
     * @param request
     * @throws ModuleException
     */
    private static void setAirportComboList(HttpServletRequest request) throws ModuleException {
        String strStation = SelectListGenerator.createActiveAirportCodeList();
        request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
    }

    /**
     * update the pax contact details for Notification
     * @param request
     */
    private static void updateContactDetailsNotification(HttpServletRequest request) {
        FlightPnrViewNotificationHTMLGenerator htmlGen = new FlightPnrViewNotificationHTMLGenerator();
        htmlGen.updateReservationContactData(request);
    }

	private static void setDisplayDataForMessageList(HttpServletRequest request) {
		try {
			FlightPnrViewNotificationHTMLGenerator htmlGen = new FlightPnrViewNotificationHTMLGenerator();
			String htmlRows = htmlGen.getMessageListRowHtml(request);
			request.setAttribute(WebConstants.REQ_RESPONSE_STRING, htmlRows);
		} catch (ModuleException moduleException) {
			log.error("FlightPnrNotificationRequestHandler.setDisplayDataForMessageList() failed - ", moduleException);
			saveMessage(request, moduleException.getMessageString(), MSG_TYPE_ERROR);
		}
	}
}
