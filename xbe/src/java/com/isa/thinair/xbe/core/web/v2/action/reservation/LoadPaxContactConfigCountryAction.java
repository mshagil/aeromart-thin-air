package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.v2.util.PassengerUtil;

/**
 * Load contact configuration for the countries
 * 
 * @author Chamara
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadPaxContactConfigCountryAction extends BaseRequestAwareAction {
	private static final Log log = LogFactory.getLog(LoadPaxContactConfigCountryAction.class);

	private String system;

	private String origin;

	private String destination;

	private String paxType;

	private Map<String, Boolean> countryPaxConfigAD = new HashMap<String, Boolean>();

	private Map<String, Boolean> countryPaxConfigCH = new HashMap<String, Boolean>();

	private Map<String, Boolean> countryPaxConfigIN = new HashMap<String, Boolean>();

	private Map<String, Boolean> countryAutoCheckinPaxConfigAD = new HashMap<String, Boolean>();

	private Map<String, Boolean> countryAutoCheckinPaxConfigCH = new HashMap<String, Boolean>();

	private Map<String, Boolean> countryAutoCheckinPaxConfigIN = new HashMap<String, Boolean>();

	private boolean success;

	public String execute() {

		try {
			if (BeanUtils.nullHandler(origin).length() > 0 && BeanUtils.nullHandler(destination).length() > 0) {

				List<String> originDestinations = new ArrayList<String>();
				String[] originList = origin.split(",");
				String[] destinationList = destination.split(",");
				for (int i = 0; i < originList.length; i++) {
					originDestinations.add(originList[i].trim());
					originDestinations.add(destinationList[i].trim());
				}				

				// loading country wise configs for above origin and destination
				Map<String, List<PaxCountryConfigDTO>> countryWiseConfigs = ModuleServiceLocator.getAirproxyPassengerBD()
						.loadPaxCountryConfig(system, AppIndicatorEnum.APP_XBE.toString(), originDestinations, getTrackInfo());

				if (countryWiseConfigs != null && countryWiseConfigs.size() > 0) {
					// generating pax category type configs according to the most restricted rule
					PassengerUtil.populateCountryWisePaxConfigs(countryWiseConfigs, countryPaxConfigAD, countryPaxConfigCH,
							countryPaxConfigIN, paxType);
					if (countryPaxConfigAD.size() > 0 && countryPaxConfigCH.size() > 0 && countryPaxConfigIN.size() > 0) {
						success = true;
					}
					// generating pax category type configs for Autocheckin ancillary
					if (AppSysParamsUtil.isAutomaticCheckinEnabled()) {
						PassengerUtil.populateCountryWisePaxConfigs(countryWiseConfigs, countryAutoCheckinPaxConfigAD,
								countryAutoCheckinPaxConfigCH, countryAutoCheckinPaxConfigIN, paxType);

						if (countryAutoCheckinPaxConfigAD.size() > 0 && countryAutoCheckinPaxConfigCH.size() > 0
								&& countryAutoCheckinPaxConfigIN.size() > 0) {
							success = true;
						}
					}
				}
			}
		} catch (ModuleException e) {
			success = false;
			log.error(e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public Map<String, Boolean> getCountryPaxConfigAD() {
		return countryPaxConfigAD;
	}

	public void setCountryPaxConfigAD(Map<String, Boolean> countryPaxConfigAD) {
		this.countryPaxConfigAD = countryPaxConfigAD;
	}

	public Map<String, Boolean> getCountryPaxConfigCH() {
		return countryPaxConfigCH;
	}

	public void setCountryPaxConfigCH(Map<String, Boolean> countryPaxConfigCH) {
		this.countryPaxConfigCH = countryPaxConfigCH;
	}

	public Map<String, Boolean> getCountryPaxConfigIN() {
		return countryPaxConfigIN;
	}

	public void setCountryPaxConfigIN(Map<String, Boolean> countryPaxConfigIN) {
		this.countryPaxConfigIN = countryPaxConfigIN;
	}

	/**
	 * @return the countryAutoCheckinPaxConfigAD
	 */
	public Map<String, Boolean> getCountryAutoCheckinPaxConfigAD() {
		return countryAutoCheckinPaxConfigAD;
	}

	/**
	 * @param countryAutoCheckinPaxConfigAD
	 *            the countryAutoCheckinPaxConfigAD to set
	 */
	public void setCountryAutoCheckinPaxConfigAD(Map<String, Boolean> countryAutoCheckinPaxConfigAD) {
		this.countryAutoCheckinPaxConfigAD = countryAutoCheckinPaxConfigAD;
	}

	/**
	 * @return the countryAutoCheckinPaxConfigCH
	 */
	public Map<String, Boolean> getCountryAutoCheckinPaxConfigCH() {
		return countryAutoCheckinPaxConfigCH;
	}

	/**
	 * @param countryAutoCheckinPaxConfigCH
	 *            the countryAutoCheckinPaxConfigCH to set
	 */
	public void setCountryAutoCheckinPaxConfigCH(Map<String, Boolean> countryAutoCheckinPaxConfigCH) {
		this.countryAutoCheckinPaxConfigCH = countryAutoCheckinPaxConfigCH;
	}

	/**
	 * @return the countryAutoCheckinPaxConfigIN
	 */
	public Map<String, Boolean> getCountryAutoCheckinPaxConfigIN() {
		return countryAutoCheckinPaxConfigIN;
	}

	/**
	 * @param countryAutoCheckinPaxConfigIN
	 *            the countryAutoCheckinPaxConfigIN to set
	 */
	public void setCountryAutoCheckinPaxConfigIN(Map<String, Boolean> countryAutoCheckinPaxConfigIN) {
		this.countryAutoCheckinPaxConfigIN = countryAutoCheckinPaxConfigIN;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
}
