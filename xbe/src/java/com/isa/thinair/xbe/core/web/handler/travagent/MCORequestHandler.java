package com.isa.thinair.xbe.core.web.handler.travagent;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airpricing.api.model.MCOServices;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class MCORequestHandler extends BasicRH {

	public static Log log = LogFactory.getLog(MCORequestHandler.class);

	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SUCCESS;
		try {
			displayData(request);
		} catch (ModuleException e) {
			log.error(" MCORequestHandler displayData FAILED : " + e);
		}
		return forward;
	}

	public static void displayData(HttpServletRequest request) throws ModuleException {
		setMcoServices(request);
		setAgentCurrency(request);
	}

	public static void setMcoServices(HttpServletRequest request) throws ModuleException {
		Collection<MCOServices> mcoServices = ModuleServiceLocator.getChargeBD().getMCOServices();
		StringBuffer sb = new StringBuffer();
		if (!mcoServices.isEmpty()) {
			for (MCOServices service : mcoServices) {
				sb.append("<option value='" + service.getMcoServiceId() + "'>" + service.getServiceName() + "</option>");
			}
		}
		request.setAttribute(WebConstants.REQ_MCO_SERVICES, sb.toString());
	}

	public static void setAgentCurrency(HttpServletRequest request) throws ModuleException {
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
		String userAgentCode = user.getAgentCode();
		String strCurrencyComboList = "";
		StringBuilder ssb = new StringBuilder();
		String strcur = SelectListGenerator.getAgentCurrency(userAgentCode);
		ssb.append("<option value='" + strcur + "'>" + strcur + "</option>");
		strCurrencyComboList = ssb.toString();
		request.setAttribute(WebConstants.REQ_HTML_CURRENCY_COMBO, strCurrencyComboList);
	}

}
