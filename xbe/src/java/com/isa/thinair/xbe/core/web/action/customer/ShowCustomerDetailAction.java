package com.isa.thinair.xbe.core.web.action.customer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.dto.UserRegConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.api.dto.v2.CustomerRegPrivillagesDTO;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.generator.customer.CustomerHTMLGenerator;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowCustomerDetailAction extends BaseRequestAwareAction {

	private boolean loyaltyManagmentEnabled;

	private String errorList;

	Map<String, UserRegConfigDTO> configList;

	private CustomerRegPrivillagesDTO regPrivilages;

	public String execute() {

		List<UserRegConfigDTO> customerConfigList = XBEModuleUtils.getGlobalConfig().getUserRegConfig();
		configList = generateUserRegConfigMap(customerConfigList);
		CustomerHTMLGenerator customerErrors = new CustomerHTMLGenerator();
		regPrivilages = new CustomerRegPrivillagesDTO(this.request);
		try {
			setErrorList(customerErrors.getClientErrors());
		} catch (ModuleException e) {
			e.printStackTrace();
		}

		if (AppSysParamsUtil.isLMSEnabled()) { // sashrika
			setLoyaltyManagmentEnabled(AppSysParamsUtil.isLMSEnabled());
		} else {
			setLoyaltyManagmentEnabled(false);
		}

		return "success";
	}

	/**
	 * @return the loyaltyManagmentEnabled
	 */
	public boolean isLoyaltyManagmentEnabled() {
		return loyaltyManagmentEnabled;
	}

	/**
	 * @param loyaltyManagmentEnabled
	 *            the loyaltyManagmentEnabled to set
	 */
	public void setLoyaltyManagmentEnabled(boolean loyaltyManagmentEnabled) {
		this.loyaltyManagmentEnabled = loyaltyManagmentEnabled;
	}

	/**
	 * @return the errorList
	 */
	public String getErrorList() {
		return errorList;
	}

	/**
	 * @param errorList
	 *            the errorList to set
	 */
	public void setErrorList(String errorList) {
		this.errorList = errorList;
	}

	private Map<String, UserRegConfigDTO> generateUserRegConfigMap(List<UserRegConfigDTO> configList) {
		Map<String, UserRegConfigDTO> userConfigDTOList = new HashMap<String, UserRegConfigDTO>();
		Iterator<UserRegConfigDTO> iterator = configList.iterator();
		while (iterator.hasNext()) {
			UserRegConfigDTO regConfigDTO = (UserRegConfigDTO) iterator.next();
			userConfigDTOList.put(regConfigDTO.getFieldName() + regConfigDTO.getGroupId(), regConfigDTO);
		}
		return userConfigDTOList;
	}

	/**
	 * @return the configList
	 */
	public Map<String, UserRegConfigDTO> getConfigList() {
		return configList;
	}

	/**
	 * @param configList
	 *            the configList to set
	 */
	public void setConfigList(Map<String, UserRegConfigDTO> configList) {
		this.configList = configList;
	}

	public CustomerRegPrivillagesDTO getRegPrivilages() {
		return regPrivilages;
	}

	public void setRegPrivilages(CustomerRegPrivillagesDTO regPrivilages) {
		this.regPrivilages = regPrivilages;
	}

}
