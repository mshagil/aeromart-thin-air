package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.alerting.api.dto.PnrRequestStatusDTO;
import com.isa.thinair.alerting.api.dto.RequestHistoryDTO;
import com.isa.thinair.alerting.api.model.RequestHistory;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * POJO to handle the History for a given Request
 * 
 * @author Sashrika Waidyarathna
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class RequestHistoryPnrAction {
	private static Log log = LogFactory.getLog(ViewQueueRequestStatusesAction.class);
	private List<Map<String,Object>> row;
	private int requestId;
	private int page;
	private int total;
	private int records;
	
		
	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public List<Map<String, Object>> getRow() {
		return row;
	}

	public void setRow(List<Map<String, Object>> row) {
		this.row = row;
	}

	public String execute() {
		return S2Constants.Result.SUCCESS;
	}
	
	public String loadHistoryForRequest() {
		Page<RequestHistory> historyForRequest=null;
		try {
			historyForRequest = ModuleServiceLocator.getAirproxyAlertingBD().getHistoryForRequest((this.page - 1) * 5, 5, requestId);
		} catch (Exception e) {
			e.printStackTrace();  // handle exception
		}
		total = historyForRequest.getTotalNoOfRecords() / 5;
		records = historyForRequest.getTotalNoOfRecords();
		int mod = historyForRequest.getTotalNoOfRecords() % 5;

		if (mod > 0) {
			this.total = this.total + 1;
		}
		setRow(getRequestDTOList((List<RequestHistory>) historyForRequest
				.getPageData()));		
		return S2Constants.Result.SUCCESS;
	}
	
	public ArrayList<Map<String, Object>> getRequestDTOList(
			List<RequestHistory> requestHistoryPageData) {
		ArrayList<Map<String, Object>> requestHistoryMap = new ArrayList<Map<String, Object>>();
		RequestHistory requestHistoryDTO = null;
		Iterator<RequestHistory> iterator = requestHistoryPageData.iterator();
		while (iterator.hasNext()) {
			try{
			requestHistoryDTO = iterator.next();
			}catch(Exception e){
				e.printStackTrace();
			}
			Map<String, Object> row = new HashMap<String, Object>();
			row.put("requestHistoryDTO", requestHistoryDTO);
			requestHistoryMap.add(row);
		}
		return requestHistoryMap;
	}
}
