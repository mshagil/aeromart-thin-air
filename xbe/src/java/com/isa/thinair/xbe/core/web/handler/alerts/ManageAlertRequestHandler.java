package com.isa.thinair.xbe.core.web.handler.alerts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.alerts.AlertHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class ManageAlertRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(ManageAlertRequestHandler.class);

	private static final String ALERT_ID_SEPERATOR = "-";

	/**
	 * 
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SUCCESS;
		String alertInfo = null;
		String[] arrAlertInfo = null;
		try {
			alertInfo = request.getParameter(WebConstants.PARAM_ALERT_INFO);
			if (alertInfo != null) {
				arrAlertInfo = alertInfo.split(",");
				if ((arrAlertInfo != null) && (arrAlertInfo.length != 0) && (arrAlertInfo[0] != null)) {
					if (arrAlertInfo[0].trim().equalsIgnoreCase(WebConstants.ACTION_DELETE)) {
						checkPrivilege(request, WebConstants.PRIV_ALERT);
						deleteData(request);
					}
					setDisplayData(request);
					request.setAttribute(WebConstants.PARAM_ALERT_INFO, "SEARCHCALLED");

				}
			}
			setOnlineAirportHtml(request);
			setInterlineCarrierCodeHtml(request);

			String strHtml = SelectListGenerator.createFlightStatus();
			request.setAttribute(WebConstants.SES_HTML_STATUS_DATA, strHtml);

		} catch (Exception ex) {
			forward = S2Constants.Result.ERROR;
			log.error("ManageAlertRequestHandler execute()", ex);
		}
		return forward;
	}

	/**
	 * 
	 * @param request
	 */
	private static void deleteData(HttpServletRequest request) {
		String selectedAlerts = request.getParameter(WebConstants.PARAM_SELECTED_ALERTS);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);

		try {
			if ((selectedAlerts != null) && (selectedAlerts.trim().length() != 0)) {

				String alert[] = selectedAlerts.split(",");

				if (alert != null) {
					Map<String, Collection<Integer>> alertIdMap = new HashMap<String, Collection<Integer>>();
					for (int i = 0; i < alert.length; i++) {
						String alertIdArr[] = alert[i].split(ALERT_ID_SEPERATOR);

						Collection<Integer> alertIDCol;
						if (alertIdMap.get(alertIdArr[1]) != null) {
							alertIDCol = alertIdMap.get(alertIdArr[1]);
						} else {
							alertIDCol = new ArrayList<Integer>();
							alertIdMap.put(alertIdArr[1], alertIDCol);
						}
						alertIDCol.add(new Integer(alertIdArr[0]));
					}

					// ModuleServiceLocator.getResSegmentBD().clearAlerts(new ArrayList<Integer>());
					ModuleServiceLocator.getAirproxySegmentBD().clearAlerts(alertIdMap, TrackInfoUtil.getBasicTrackInfo(request));
					saveMessage(request, XBEConfig.getServerMessage("alerts.deletion.success", userLanguage, null), MSG_TYPE_CONFIRMATION);
				}

			}
		} catch (ModuleException moduleException) {
			log.error("ManageAlertRequestHandler deleteData()", moduleException);
			saveMessage(request, moduleException.getMessageString(), MSG_TYPE_ERROR);
		}
	}

	/**
	 * set data to show in the jsp file
	 * 
	 * @param request
	 */
	private static void setDisplayData(HttpServletRequest request) {
		setAlertRowHtml(request);
	}

	/**
	 * sets the html rows for alert list into request variable
	 * 
	 * @param request
	 */
	private static void setAlertRowHtml(HttpServletRequest request) {
		try {
			AlertHTMLGenerator htmlGen = new AlertHTMLGenerator();
			String htmlRows = htmlGen.getAlertRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, htmlRows);
		} catch (ModuleException moduleException) {
			log.error("ManageAlertRequestHandler.setAlertRowHtml() failed - ", moduleException);
			saveMessage(request, moduleException.getMessageString(), MSG_TYPE_ERROR);
		}
	}

	/**
	 * Sets Online & Active Airport List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setOnlineAirportHtml(HttpServletRequest request) throws ModuleException {

		Map<String, String> airports = null;
		if (AppSysParamsUtil.isLCCConnectivityEnabled()
				&& BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_VIEW_ANY_AIRLINE_ALERT)) {

			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

			if (userPrincipal.getCarrierCodes() != null) {
				airports = SelectListGenerator.createAirportsWithoutSubstations(true, null, userPrincipal.getCarrierCodes());
			} else {
				airports = SelectListGenerator.createAirportsWithoutSubstations(false, null, null);
			}

		} else {
			airports = SelectListGenerator.createAirportsWithoutSubstations(false, null, null);
		}

		StringBuffer sb = new StringBuffer();
		List<String> list = new ArrayList<String>(airports.keySet());
		Collections.sort(list);
		for (String airportCode : list) {
			sb.append("<option value='" + airportCode + "'>" + airportCode + "</option>");
		}

		request.getSession().setAttribute(WebConstants.SES_HTML_AIRPORT_DATA, sb.toString());
	}

	// TODO- we must find a unified way of getting the interlined set of carriers.
	private static void setInterlineCarrierCodeHtml(HttpServletRequest request) {
		String strHtml = null;

		if (AppSysParamsUtil.isLCCConnectivityEnabled()
				&& BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_VIEW_ANY_AIRLINE_ALERT)) {

			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

			if (userPrincipal.getCarrierCodes() != null) {
				strHtml = SelectListGenerator.createInterlineCarrierSelectListOptions(userPrincipal.getCarrierCodes());
			} else {

				strHtml = SelectListGenerator.createInterlineCarrierSelectListOptions(null);
			}

		} else {
			strHtml = SelectListGenerator.createInterlineCarrierSelectListOptions(null);
		}

		request.setAttribute(WebConstants.REQ_CARRIER_CODE_LIST_OPTIONS, strHtml);
	}
}
