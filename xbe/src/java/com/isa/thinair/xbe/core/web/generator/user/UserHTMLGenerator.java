package com.isa.thinair.xbe.core.web.generator.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.model.UserSearchDTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class UserHTMLGenerator {

	private static String clientErrors;
	private static final String PARAM_RECNO = "hdnRecNo";
	private static final String PARAM_SEARCHDATA = "hdnSearchData";
	private static final String PARAM_GRIDDATA = "hdnGridData";
	private static final String PARAM_SEARCHID = "txtLoginIdSearch";
	private static final String PARAM_SEARCHAIRLINE = "hdnSearchAirline";
	private static final String PARAM_SEARCHNAME = "txtLastNameSearch";
	private static final String PARAM_SEARCHAIRPORT = "selAirportSearch";
	private static final String PARAM_SEARCHAGENT = "selTravelAgentSearch";
	private static final String PARAM_SEARCHSTATUS = "selSearchStatus";
	private Properties props;

	/**
	 * Construct HG Using Property file Values
	 * 
	 * @param props
	 *            the Properties file containg Request values
	 */
	public UserHTMLGenerator(Properties props) {
		this.props = props;
	}

	/**
	 * Gets the User Grid Row Array For the Search
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getUserRowHtml(HttpServletRequest request, String agentCode, int adminLevel, int normalLevel)
			throws ModuleException {

		Collection<User> colRoles = null;
		Page page = null;
		int recNo = 0;
		int totRec = 0;
		String strSearchData = props.getProperty(PARAM_SEARCHDATA);
		String strGridData = props.getProperty(PARAM_GRIDDATA);
		String strSrchAgentCode = props.getProperty(PARAM_SEARCHAGENT).split(",")[0];

		if (!props.getProperty(PARAM_RECNO).equals("")) {
			recNo = Integer.parseInt(props.getProperty(PARAM_RECNO));
			recNo = recNo - 1;
		}
		request.setAttribute("recordNo", new Integer(recNo + 1).toString());
		request.setAttribute("reqSearchData", strSearchData);
		request.setAttribute("reqGridData", strGridData);
		UserSearchDTO userSearchDTO = new UserSearchDTO();

		if (!props.getProperty(PARAM_SEARCHID).trim().equals("")
				&& props.getProperty(PARAM_SEARCHAIRLINE).trim().toUpperCase().equals("ALL")) {
			userSearchDTO.setUserID("%" + props.getProperty(PARAM_SEARCHID).toUpperCase().trim() + "%");
		} else if (!props.getProperty(PARAM_SEARCHID).trim().equals("")
				&& !props.getProperty(PARAM_SEARCHAIRLINE).trim().toUpperCase().equals("ALL")) {
			userSearchDTO.setUserID(props.getProperty(PARAM_SEARCHAIRLINE).trim().toUpperCase() + "%"
					+ props.getProperty(PARAM_SEARCHID).toUpperCase().trim() + "%");
		} else if (props.getProperty(PARAM_SEARCHID).trim().equals("")
				&& !props.getProperty(PARAM_SEARCHAIRLINE).trim().toUpperCase().equals("ALL")) {
			userSearchDTO.setUserID(props.getProperty(PARAM_SEARCHAIRLINE).trim().toUpperCase() + "%");
		}

		if (!props.getProperty(PARAM_SEARCHNAME).trim().equals("")) {
			userSearchDTO.setDisplayName("%" + props.getProperty(PARAM_SEARCHNAME) + "%");
		}

		if (!props.getProperty(PARAM_SEARCHAIRPORT).trim().equals("")
				&& !props.getProperty(PARAM_SEARCHAIRPORT).trim().toUpperCase().equals("ALL")) {
			userSearchDTO.setStationCode(props.getProperty(PARAM_SEARCHAIRPORT));
		}

		if (!props.getProperty(PARAM_SEARCHSTATUS).trim().equals("")
				&& !props.getProperty(PARAM_SEARCHSTATUS).trim().toUpperCase().equals("ALL")) {
			userSearchDTO.setStatus(props.getProperty(PARAM_SEARCHSTATUS));
		}

		if (!strSrchAgentCode.trim().equals("") && !strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
			userSearchDTO.setTravelAgentCode(strSrchAgentCode);
			userSearchDTO.setAccTravelAgentCode(strSrchAgentCode);
		} else {
			userSearchDTO.setAccTravelAgentCode(agentCode);
		}

		if (adminLevel == 1 && normalLevel == 1) { // level 1
			userSearchDTO.setAccessLevel(0);
		} else if (adminLevel == 1 && normalLevel == 2) {
			userSearchDTO.setAccessLevel(1);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (adminLevel == 1 && normalLevel == 3) {
			userSearchDTO.setAccessLevel(2);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (adminLevel == 1 && normalLevel == 4) {
			userSearchDTO.setAccessLevel(3);

		} else if (adminLevel == 1 && normalLevel == 0) {
			userSearchDTO.setAccessLevel(4);
		} else if (normalLevel == 1 && adminLevel == 2) {
			userSearchDTO.setAccessLevel(5);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (normalLevel == 1 && adminLevel == 3) {
			userSearchDTO.setAccessLevel(6);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (normalLevel == 1 && adminLevel == 4) {
			userSearchDTO.setAccessLevel(7);

		} else if (normalLevel == 1 && adminLevel == 0) {
			userSearchDTO.setAccessLevel(8);
		} else if (normalLevel == 2 && adminLevel == 2) {
			userSearchDTO.setAccessLevel(9);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (adminLevel == 2 && normalLevel == 3) {
			userSearchDTO.setAccessLevel(10);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (adminLevel == 2 && normalLevel == 4) {
			userSearchDTO.setAccessLevel(11);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (adminLevel == 2 && normalLevel == 0) {
			userSearchDTO.setAccessLevel(12);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (adminLevel == 3 && normalLevel == 2) {
			userSearchDTO.setAccessLevel(13);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (adminLevel == 3 && normalLevel == 3) {
			userSearchDTO.setAccessLevel(14);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (adminLevel == 3 && normalLevel == 4) {
			userSearchDTO.setAccessLevel(15);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (adminLevel == 3 && normalLevel == 0) {
			userSearchDTO.setAccessLevel(16);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (adminLevel == 4 && normalLevel == 2) {
			userSearchDTO.setAccessLevel(17);

		} else if (adminLevel == 4 && normalLevel == 3) {
			userSearchDTO.setAccessLevel(18);
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}

		} else if (adminLevel == 4 && normalLevel == 4) {
			userSearchDTO.setAccessLevel(19);

		} else if (adminLevel == 4 && normalLevel == 0) {
			userSearchDTO.setAccessLevel(20);
			userSearchDTO.setTravelAgentCode(agentCode);

		} else if (adminLevel == 0 && normalLevel == 2) {
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}
			userSearchDTO.setAccessLevel(21);

		} else if (adminLevel == 0 && normalLevel == 3) {
			if (strSrchAgentCode.trim().toUpperCase().equals("ALL")) {
				Collection colAgent = new ArrayList();
				colAgent.add(agentCode);
				userSearchDTO.setReportingAgentCodes(colAgent);
			}
			userSearchDTO.setAccessLevel(22);

		} else if (adminLevel == 0 && normalLevel == 4) {
			userSearchDTO.setAccessLevel(23);
			userSearchDTO.setTravelAgentCode(agentCode);

		}
		if (adminLevel > 0 || normalLevel > 0) {
			page = ModuleServiceLocator.getSecurityBD().searchUsersWithAirlineSelection(userSearchDTO, recNo, 20);
		}

		if (page != null) {
			colRoles = page.getPageData();
			totRec = page.getTotalNoOfRecords();
		}
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, "'" + new Integer(totRec).toString() + "';");
		return createUserRowHtml(colRoles);

	}

	/**
	 * Creates the Grid array from a Collecion of Users
	 * 
	 * @param users
	 *            the collection of Users
	 * @return String the Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static String createUserRowHtml(Collection<User> users) throws ModuleException {

		StringBuffer sb = new StringBuffer("var userData = new Array();");
		User user = null;
		Role role = null;
		int tempInt = 0;

		if (users != null) {
			Iterator<User> userIter = users.iterator();
			while (userIter.hasNext()) {

				user = (User) userIter.next();
				String strAssign = "";
				Set<Role> setRole = user.getRoles();
				Iterator<Role> iter = setRole.iterator();

				while (iter.hasNext()) {
					role = (Role) iter.next();
					if (user.isEditable())
						strAssign += role.getRoleId() + ",";
				}
				if (!("".equals(strAssign))) {
					strAssign = strAssign.substring(0, strAssign.lastIndexOf(","));
				}
				sb.append("userData[" + tempInt + "] = new Array();");
				sb.append("userData[" + tempInt + "][0] = '" + tempInt + "';");
				sb.append("userData[" + tempInt + "][1] = '" + user.getUserId() + "';");
				sb.append("userData[" + tempInt + "][2] = '" + user.getDisplayName() + "';");
				sb.append("userData[" + tempInt + "][3] = '&nbsp';");
				if (user.getEmailId() != null) {
					sb.append("userData[" + tempInt + "][5] = '" + user.getEmailId() + "';");
				} else {
					sb.append("userData[" + tempInt + "][5] = '&nbsp';");
				}
				Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(user.getAgentCode());
				if (agent != null) {
					sb.append("userData[" + tempInt + "][6] = '" + agent.getAgentDisplayName() + "';");
					sb.append("userData[" + tempInt + "][13] = '" + agent.getAgentCode() + "," + agent.getAgentTypeCode() + "';");
				} else {
					sb.append("userData[" + tempInt + "][6] = '&nbsp';");
					sb.append("userData[" + tempInt + "][13] = '&nbsp';");
				}
				sb.append("userData[" + tempInt + "][7] = '"
						+ (user.getStatus().equals(User.STATUS_ACTIVE) ? "Active" : "In-Active") + "';");

				sb.append("userData[" + tempInt + "][10] = '" + user.getVersion() + "';");
				sb.append("userData[" + tempInt + "][11] = 'PASS1234';"); // not passing the password
				sb.append("userData[" + tempInt + "][12] = '" + strAssign + "';");
				if (user.isEditable())
					sb.append("userData[" + tempInt + "][14] = 'Y';");
				else
					sb.append("userData[" + tempInt + "][14] = 'N';");

				if (user.getCarriers() != null) // remove "[" //remove "]" //remove white spaces
					sb.append("userData[" + tempInt + "][16] = '"
							+ user.getCarriers().toString().replace("[", "").replace("]", "").replace(" ", "") + "';");
				else
					sb.append("userData[" + tempInt + "][16] = '';");
				if (user.getDefaultCarrierCode() != null)
					sb.append("userData[" + tempInt + "][17] = '" + user.getDefaultCarrierCode() + "';");
				else
					sb.append("userData[" + tempInt + "][17] = '';");
				sb.append("userData[" + tempInt + "][18] = '" + user.getAdminUserCreateStatus() + "';");
				sb.append("userData[" + tempInt + "][19] = '" + user.getNormalUserCreateStatus() + "';");
				sb.append("userData[" + tempInt + "][20] = '" + user.getServiceChannel() + "';");
				sb.append("userData[" + tempInt + "][21] = '" + user.getThemeCode() + "';");
				sb.append("userData[" + tempInt + "][22] = '" + StringUtil.getNotNullString(user.getFfpNumber()) + "';");
				sb.append("userData[" + tempInt + "][23] = '" + StringUtil.getNotNullString(user.getMyIDCarrierCode()) + "';");
				sb.append("userData[" + tempInt + "][24] = '" + ((user.getMaxAdultAllowed() == null) ? "" : user.getMaxAdultAllowed()) + "';");
				tempInt++;
			}
		}

		return sb.toString();
	}

	/**
	 * Gets the Client Validations For the User Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.user.form.id.required", "userIdRqrd");
			moduleErrs.setProperty("um.user.form.id.invalid", "userIdInvalid");
			moduleErrs.setProperty("um.user.form.nochanges", "ffpnochanges");
			moduleErrs.setProperty("um.user.form.search.field", "userSearchField");
			moduleErrs.setProperty("um.user.form.password.required", "userPasswordRqrd");
			moduleErrs.setProperty("um.user.form.password.cmbinvalid", "cmbinvalid");
			moduleErrs.setProperty("um.user.form.confpassword.required", "userConfPasswordRqrd");
			moduleErrs.setProperty("um.user.form.password.not.confirmed", "userPasswordNotConf");
			moduleErrs.setProperty("um.user.form.display.name.required", "userDisplayNameRqrd");
			moduleErrs.setProperty("um.user.form.display.name.invalid", "userDisplayNameInvalid");
			moduleErrs.setProperty("um.user.form.first.name.required", "userFirstNameRqrd");
			moduleErrs.setProperty("um.user.form.last.name.required", "userLastNameRqrd");
			moduleErrs.setProperty("um.user.form.email.required", "userEmailRqrd");
			moduleErrs.setProperty("um.user.form.emali.format", "useremailFormat");
			moduleErrs.setProperty("um.user.form.domain.required", "userDomainRqrd");
			moduleErrs.setProperty("um.user.form.airport.required", "userAirportRqrd");
			moduleErrs.setProperty("um.user.form.station.required", "userStationRqrd");
			moduleErrs.setProperty("um.user.form.password.length", "userPasswordlength");
			moduleErrs.setProperty("um.user.form.password.max.length", "userMaxPasswordlength");
			moduleErrs.setProperty("um.user.form.confpassword.length", "userConfPasswordlength");
			moduleErrs.setProperty("um.user.form.station.inactive", "userStationINA");
			moduleErrs.setProperty("um.user.form.agent.inactive", "userAgentINA");
			moduleErrs.setProperty("um.user.form.agent.required", "userAgentRqrd");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteConf");
			moduleErrs.setProperty("um.airadmin.form.carier.null", "carierNull");
			moduleErrs.setProperty("um.airadmin.form.defcarier.null", "defcarierNull");
			moduleErrs.setProperty("um.airadmin.form.defcarier.assignNotmatch", "assignNotmatch");
			moduleErrs.setProperty("um.airadmin.reset.confirmation", "resetConfirm");
			moduleErrs.setProperty("um.airadmin.reset.userVerifypass", "userVerifypass");
			moduleErrs.setProperty("um.user.form.servicechannel.required", "userServiceChRqrd");
			moduleErrs.setProperty("um.user.form.dry.userlevel.required", "dryUserLevelRqrd");
			moduleErrs.setProperty("um.user.form.role.cannotRemove", "excRoleRemove");
			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	/**
	 * Private method to generate a key.
	 * 
	 * @param toEncrypt
	 *            text to encrypt.
	 * @return the key.
	 * @throws ModuleException
	 */
	/*
	 * private static String getDecryptedString(String toDecrypt) throws ModuleException{ String key = null;
	 * CryptoServiceBD cryptoServiceBD = (CryptoServiceBD) getCryptoServiceBD();
	 * 
	 * key = cryptoServiceBD.decrypt(toDecrypt);
	 * 
	 * return key; }
	 */

	/**
	 * Private method to get CryptoServiceBD reference.
	 * 
	 * @return CryptoServiceBD
	 */
	/*
	 * getCryptoServiceBD() {
	 * 
	 * CryptoServiceBD cryptoService = (CryptoServiceBD) AirSecurityUtil.lookupServiceBD("crypto","crypto.service");
	 * 
	 * return cryptoService; }
	 */

	/**
	 * Gets the User Grid Row Array For the Search
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getDryUserEnable(HttpServletRequest request) throws ModuleException {

		StringBuffer sb = new StringBuffer("var strDryUserEnable = '");

		String defCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		String dryUserEnable = ModuleServiceLocator.getSecurityBD().getDryUserEnable(defCarrierCode);
		sb.append(dryUserEnable + "';");
		return sb.toString();
	}
}