package com.isa.thinair.xbe.core.web.v2.action.system;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airmaster.api.model.DashboardMessage;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.DashboardMessageTO;
import com.isa.thinair.webplatform.api.dto.DashbrdUserDataRetrivalTO;
import com.isa.thinair.webplatform.core.util.LookupUtils;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * Dashboard messages listing
 * 
 * @author Baladewa
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadDBMessagesAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(LoadDBMessagesAction.class);

	private boolean success = true;
	private String messageTxt = "";
	private boolean unread;
	private Collection<DashboardMessageTO> dashboardMessageList;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		String userId = userPrincipal.getUserId();

		log.debug("Dashboard content User Id :" + userId);
		DashbrdUserDataRetrivalTO dataRetrivalTO = new DashbrdUserDataRetrivalTO();
		dataRetrivalTO.setUserID(userId);
		dataRetrivalTO.setAgentCode(userPrincipal.getAgentCode());
		dataRetrivalTO.setAgentType(userPrincipal.getAgentTypeCode());
		dataRetrivalTO.setPosCode(userPrincipal.getAgentStation());
		try {
			Map<Integer, Collection<DashboardMessageTO>> mapDashboardMessages = LookupUtils.getWebServiceDAO()
					.getDashboardContent(new Date(), dataRetrivalTO);
			dashboardMessageList = new ArrayList<DashboardMessageTO>();
			if (mapDashboardMessages != null) {
				
				for(Integer priority :mapDashboardMessages.keySet()){
					dashboardMessageList.addAll(mapDashboardMessages.get(priority));
				}
			}
			boolean isNewMessageExist = ModuleServiceLocator.getSecurityBD().isNewDashBoardMsgsExists(userId);
			this.unread = DashboardMessage.NEW_MESSAGE.equals((isNewMessageExist) ? "Y" : "N");
		} catch (Exception e) {
			success = false;
			log.error(messageTxt, e);
			dashboardMessageList = null;
		}
		return forward;
	}

	public String updateDBMessageExistStatus() {
		String forward = S2Constants.Result.SUCCESS;
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		String userId = userPrincipal.getUserId();

		try {
			ModuleServiceLocator.getSecurityBD().updateUserNewMessageStatus(userId, DashboardMessage.NO_NEW_MESSAGE);
		} catch (Exception e) {
			success = false;
			log.error(messageTxt, e);
		}

		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public boolean isUnread() {
		return unread;
	}

	public void setUnread(boolean unread) {
		this.unread = unread;
	}

	public Collection<DashboardMessageTO> getDashboardMessageList() {
		return dashboardMessageList;
	}

	public void setDashboardMessageList(Collection<DashboardMessageTO> dashboardMessageList) {
		this.dashboardMessageList = dashboardMessageList;
	}

}
