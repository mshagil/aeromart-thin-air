package com.isa.thinair.xbe.core.web.v2.action.travagent;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.v2.generator.IncentiveSchemeHG;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.TravAgent.V2_CREDIT_MANAGEMENT),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class LoadCreditManagementAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(LoadCreditManagementAction.class);
	
	
	
	private BigDecimal totalCreditLimit;
	
	private BigDecimal fixedCreditLimit;
	
	private BigDecimal sharedCreditLimit;
	

	public String execute() {

//		try {
//			if (AppSysParamsUtil.isTravelAgentIncentiveAnabled()) {
//				String schemeList = IncentiveSchemeHG.createIncentiveSchemeList(request);
//				String agentMulitiSelList = IncentiveSchemeHG.setAgentsMultiSelect();
//				request.setAttribute("reqSchemeList", schemeList);
//				request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, agentMulitiSelList);
//				request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, IncentiveSchemeHG.getClientErrors(request));
//			} else {
//				return S2Constants.Result.ERROR;
//			}
//
//		} catch (ModuleException me) {
//			log.error("Error in LoadCreditManagementAction" + me.getExceptionCode());
//		}
		return S2Constants.Result.SUCCESS;
	}
	
	
	public String saveCredit(){
		
		//Get agent code - update credit values with given
		return S2Constants.Result.SUCCESS;
	}


	/**
	 * @return the totalCreditLimit
	 */
	public BigDecimal getTotalCreditLimit() {
		return totalCreditLimit;
	}


	/**
	 * @param totalCreditLimit the totalCreditLimit to set
	 */
	public void setTotalCreditLimit(BigDecimal totalCreditLimit) {
		this.totalCreditLimit = totalCreditLimit;
	}


	/**
	 * @return the fixedCreditLimit
	 */
	public BigDecimal getFixedCreditLimit() {
		return fixedCreditLimit;
	}


	/**
	 * @param fixedCreditLimit the fixedCreditLimit to set
	 */
	public void setFixedCreditLimit(BigDecimal fixedCreditLimit) {
		this.fixedCreditLimit = fixedCreditLimit;
	}


	/**
	 * @return the sharedCreditLimit
	 */
	public BigDecimal getSharedCreditLimit() {
		return sharedCreditLimit;
	}


	/**
	 * @param sharedCreditLimit the sharedCreditLimit to set
	 */
	public void setSharedCreditLimit(BigDecimal sharedCreditLimit) {
		this.sharedCreditLimit = sharedCreditLimit;
	}
	
}
