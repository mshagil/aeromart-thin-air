package com.isa.thinair.xbe.core.web.action.travagent;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.util.CollectionTypeEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.xbe.api.dto.v2.ReservationPostPaymentDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Payment.CCTOPUP_PGW_FEEDBACK),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class HandleCCTopupEPGWResponseAction extends BaseRequestAwareAction {

	private static final String SAVESUCCESS = "SAVESUCCESS";
	private static final String SAVEFAILED = "SAVEFAILED";
	private static Log log = LogFactory.getLog(HandleCCTopupEPGWResponseAction.class);

	public String execute() {

		ReservationPostPaymentDTO postPayData = (ReservationPostPaymentDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.POST_PAY_DATA);
		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
		String strAdjAmount = (String) request.getSession().getAttribute(S2Constants.Session_Data.SELF_TOPUP_AMOUNT);
		String strRemarks = (String) request.getSession().getAttribute(S2Constants.Session_Data.SELF_TOPUP_REMARKS);
		strRemarks += "--added by TOPUPSCREEN";
		String strCurrency = (String) request.getSession().getAttribute(S2Constants.Session_Data.SELF_TOPUP_SEL_CURR);
		String strCarrier = AppSysParamsUtil.getDefaultCarrierCode();
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
		String strAgentCode = user.getAgentCode();
		List<Integer> colTnxIds = new ArrayList<Integer>();
		Integer tempPayId = null;
		boolean topupSaveStatus = false;

		if (postPayData != null) {
			IPGResponseDTO externalPGWResponseDTO = postPayData.getIpgResponseDTO();
			Map<String, String> receiptyMap;
			try {
				externalPGWResponseDTO.setRequestTimsStamp(ModuleServiceLocator.getReservationBD().getPaymentRequestTime(
						postPayData.getIpgResponseDTO().getTemporyPaymentId()));
			} catch (ModuleException e) {
				log.error("Error in retrieving paymentRequestTime : " + e);
			}

			externalPGWResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
			externalPGWResponseDTO.setPaymentBrokerRefNo(postPayData.getPaymentBrokerRefNo());

			try {
				IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil
						.validateAndPrepareIPGConfigurationParamsDTO(postPayData.getIpgId(), postPayData.getCurrencyCode());
				receiptyMap = getReceiptMap(request);
				externalPGWResponseDTO = ModuleServiceLocator.getPaymentBrokerBD().getReponseData(receiptyMap,
						externalPGWResponseDTO, ipgIdentificationParamsDTO);
				request.getSession().setAttribute(S2Constants.Session_Data.EXTERNAL_PAYMENT_GATEWAY_RESPONSE,
						externalPGWResponseDTO);

				tempPayId = postPayData.getIpgResponseDTO().getTemporyPaymentId();
				colTnxIds.add(tempPayId);
				if (externalPGWResponseDTO.isSuccess()) {
					ModuleServiceLocator.getReservationBD().updateTempPaymentEntry(colTnxIds,
							ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS, null, null, null, null);
					if (!strAdjAmount.equals("")) {
						amount = AccelAeroCalculator.getTwoScaledBigDecimalFromString(strAdjAmount);
					}
					topupSaveStatus = ModuleServiceLocator.getTravelAgentFinanceBD().recordselfTopup(strAgentCode, amount, strRemarks,
							CollectionTypeEnum.CREDITCARD, strCurrency, strCarrier, tempPayId.toString());
					if(topupSaveStatus){
						ModuleServiceLocator.getReservationBD().updateTempPaymentEntry(colTnxIds,
								ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, null, null, null, null);
						sendNotificationEmail(request);
					}
					log.debug("Agent topup completed successfully. Agent : " + strAgentCode);
					request.getSession().setAttribute(S2Constants.Session_Data.SELF_TOPUP_MSGTXT, SAVESUCCESS);

				} else {
					ModuleServiceLocator.getReservationBD().updateTempPaymentEntry(colTnxIds,
							ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE, null, null, null, null);
					request.getSession().setAttribute(S2Constants.Session_Data.SELF_TOPUP_MSGTXT, SAVEFAILED);
				}
				request.getSession().removeAttribute(S2Constants.Session_Data.SELF_TOPUP_AMOUNT);
				request.getSession().removeAttribute(S2Constants.Session_Data.SELF_TOPUP_REMARKS);
				request.getSession().removeAttribute(S2Constants.Session_Data.SELF_TOPUP_SEL_CURR);

			} catch (NumberFormatException e) {
				log.error("Error in retrieving paymentResponseData : " + e);
			} catch (ModuleException e) {
				log.error("Error in retrieving paymentResponseData : " + e);
			} catch (Exception e) {
				log.error("Error in retrieving paymentResponseData : " + e);
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	private static Map<String, String> getReceiptMap(HttpServletRequest request) {

		Map<String, String> fields = new LinkedHashMap<>();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements();) {
			String fieldName = enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);

			if (fieldValue != null && fieldValue.length() > 0) {
				fields.put(fieldName, fieldValue);
			}
		}
		fields.put(PaymentConstants.IPG_SESSION_ID, request.getSession().getId());
		return fields;
	}
	
	private static void sendNotificationEmail(HttpServletRequest request) throws ModuleException {

		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
		String strAdjAmount = (String) request.getSession().getAttribute(S2Constants.Session_Data.SELF_TOPUP_AMOUNT);
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
		String strAgentCode = user.getAgentCode();
		if (!strAdjAmount.equals("")) {
			amount = AccelAeroCalculator.getTwoScaledBigDecimalFromString(strAdjAmount);
		}

		try {
			log.info("sending agent topup email notification-start: " + amount.toString() + " and " + strAgentCode);
			ModuleServiceLocator.getTravelAgentFinanceBD().agentTopupEmailNotification(amount.toString(), strAgentCode);
			log.info("sending agent topup email notification-end: " + amount.toString() + " and " + strAgentCode);
		} catch (Exception emailSendException) {
			log.error("Error during send topup email : " + emailSendException);
		}
	}
}
