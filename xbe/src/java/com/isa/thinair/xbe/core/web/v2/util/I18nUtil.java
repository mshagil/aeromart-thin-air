package com.isa.thinair.xbe.core.web.v2.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.commons.lang.StringEscapeUtils;

import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

/**
 * 
 * @author rajitha
 *
 */
public class I18nUtil {

	private static Set<String> lanKeySet;

	private static Map<String, ResourceBundle> translationData = new HashMap<String, ResourceBundle>();

	private static Map<String, ResourceBundle> clientMessages = new HashMap<String, ResourceBundle>();

	private static Map<String, ResourceBundle> serverMessages = new HashMap<String, ResourceBundle>();

	private static Map<String, ResourceBundle> menuMessages = new HashMap<String, ResourceBundle>();

	private static Map<String, ResourceBundle> loginMessages = new HashMap<String, ResourceBundle>();

	private static Map<String, Map<String, ResourceBundle>> allMessageMap = new HashMap<String, Map<String, ResourceBundle>>();

	static {

		loadResourceBundles();
	}

	private static void loadResourceBundles() {
		Map<String, String> xbeSuppotedLanguagesMap = AppSysParamsUtil.getXBESupportedLanguages();
		lanKeySet = xbeSuppotedLanguagesMap.keySet();

		for (Iterator<String> iterator = lanKeySet.iterator(); iterator.hasNext();) {
			String lan = (String) iterator.next();
			Locale locale = new Locale(lan);

			translationData.put(lan, ResourceBundle.getBundle(XbeI18nKeys.XBE_TRANSLATION_DATA, locale));
			menuMessages.put(lan, ResourceBundle.getBundle(XbeI18nKeys.XBE_MENU_DATA, locale));
			loginMessages.put(lan, ResourceBundle.getBundle(XbeI18nKeys.XBE_LOGIN_DATA, locale));
			clientMessages.put(lan, ResourceBundle.getBundle(XbeI18nKeys.XBE_CLIENT_MESSAGES_DATA, locale));
			serverMessages.put(lan, ResourceBundle.getBundle(XbeI18nKeys.XBE_SERVER_MESSAGES_DATA, locale));
		}

		allMessageMap.put(XbeI18nKeys.TRANSLATION, translationData);
		allMessageMap.put(XbeI18nKeys.CLIENT_MESSAGES, clientMessages);
		allMessageMap.put(XbeI18nKeys.SERVER_MESSAGES, serverMessages);

	}


	public static String getMessage(String messageCode, String languge, String messageType, String errorMsg) {

		Map<String, ResourceBundle> messageMap = allMessageMap.get(messageType);

		if (languge == null || languge.isEmpty()) {
			languge = WebConstants.DEFAULT_LANGUAGE;
		}

		String message = "";
		if (messageCode != null && !messageCode.equals("")) {

			try {
				ResourceBundle rb = messageMap.get(languge);
				message = rb.getString(messageCode);
			} catch (Exception ex) {
				try {
					
					if (errorMsg != null && !errorMsg.equals("")){
						message = errorMsg;
					}else{
						message = MessagesUtil.getMessage(messageCode);
					}
					
				} catch (Exception commonEx) {
					message = messageMap.get(languge).getString(XbeI18nKeys.EMPTY_MESSAGE);
				}
			}

		}

		return message;
	}

	public static HashMap<String, String> getXbeMesseges(String languge, String... pageIds) {
		HashMap<String, String> pageMesage = new HashMap<String, String>();
		ResourceBundle rb = translationData.get(languge);
		if (rb != null) {
			for (String key : rb.keySet()) {
				for (String pageID : pageIds) {
					if (key.startsWith(pageID + "_")) {
						pageMesage.put(key.split(pageID + "_")[1], rb.getString(key));
						break;
					}
				}
			}
		}

		return pageMesage;
	}

	public static HashMap<String, String> getMenuData(String languge) {
		HashMap<String, String> menuData = new HashMap<String, String>();
		ResourceBundle rb = menuMessages.get(languge);
		if (rb != null) {
			for (String key : rb.keySet()) {

				menuData.put(key, StringEscapeUtils.escapeJavaScript(rb.getString(key)));
			}
		}

		return menuData;
	}

	public static HashMap<String, HashMap<String, String>> getLoginData() {
		HashMap<String, HashMap<String, String>> loginData = new HashMap<String, HashMap<String, String>>();

		for (Iterator<String> iterator = lanKeySet.iterator(); iterator.hasNext();) {

			HashMap<String, String> languageData = new HashMap<String, String>();
			String lan = (String) iterator.next();
			ResourceBundle rb = loginMessages.get(lan);
			if (rb != null) {
				for (String key : rb.keySet()) {

					languageData.put(key, rb.getString(key));
				}

				loginData.put(lan, languageData);
			}

		}

		return loginData;
	}

}


