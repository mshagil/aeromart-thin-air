package com.isa.thinair.xbe.core.web.v2.action;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.BaggageRatesDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the Load Fare Quote
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class BaggageRatesAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(BaggageRatesAction.class);

	private boolean success = true;

	private String messageTxt;

	private FlightSearchDTO fareQuoteParams;

	private BaggageRatesDTO baggageRatesDTO;

	private ArrayList<String> outFlightRPHList;

	private ArrayList<String> retFlightRPHList;

	private String groupPNR;

	private boolean addSegment = false;

	private boolean modifyBooking = false;

	public String execute() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			boolean viewFlightsWithLesserSeats = BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_RES_ALLFLIGHTS);
			boolean isAllowFlightSearchAfterCutOffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_RESERVATION_AFTER_CUT_OFF_TIME);
			boolean isAllowOverBookAfterCutOffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_OVERBOOK_RESERVATIONS_AFTER_CUT_OFF_TIME);
			boolean isAllowOverBookBeforeCutOffTime = BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_OVER_BOOKING);
			String pnr = request.getParameter("pnr");

			FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, outFlightRPHList,
					retFlightRPHList);


			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

			flightPriceRQ.getAvailPreferences().setAllowFlightSearchAfterCutOffTime(isAllowFlightSearchAfterCutOffTime);
			flightPriceRQ.getAvailPreferences().setAllowDoOverbookBeforeCutOffTime(isAllowOverBookBeforeCutOffTime);
			flightPriceRQ.getAvailPreferences().setAllowDoOverbookAfterCutOffTime(isAllowOverBookAfterCutOffTime);

			flightPriceRQ.setTransactionIdentifier(resInfo.getTransactionId());
			flightPriceRQ.getAvailPreferences().setRestrictionLevel(AvailabilityConvertUtil
					.getAvailabilityRestrictionLevel(viewFlightsWithLesserSeats, fareQuoteParams.getBookingType()));
			flightPriceRQ.getAvailPreferences().setQuoteFares(true);

			if (pnr != null && !"".equals(pnr)) {
				flightPriceRQ.getAvailPreferences().setModifyBooking(true);
				flightPriceRQ.getAvailPreferences().setAddSegment(addSegment);
				ReservationBeanUtil.setExistingSegInfo(flightPriceRQ, request.getParameter("resSegments"),
						request.getParameter("modifySegment"));
			}

			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			if (isGroupPNR) {
				flightPriceRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
			}

			if (log.isDebugEnabled()) {
				if (flightPriceRQ.getTransactionIdentifier() != null) {
					log.debug("Fare quote" + flightPriceRQ.getTransactionIdentifier());
				}
			}

			flightPriceRQ.getAvailPreferences().setTravelAgentCode(fareQuoteParams.getTravelAgentCode());

			if (ReservationUtil.isCharterAgent(request)) {
				flightPriceRQ.getAvailPreferences().setFixedFareAgent(true);
			}
			if (flightPriceRQ.getTravelPreferences().isOpenReturnConfirm()) {
				ReservationUtil.setOpenReturnConfirmInfo(flightPriceRQ, pnr, getTrackInfo());
			}

			ReservationApiUtils.replaceOnDInformationForFareQuoteAfterCityBaseSearch(flightPriceRQ);

			FlightPriceRS flightPriceRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().quoteFlightPrice(flightPriceRQ,
					getTrackInfo());

			this.baggageRatesDTO = ModuleServiceLocator.getAirproxyAncillaryBD().getBaggageRates(getTrackInfo(),
					flightPriceRS, fareQuoteParams.getClassOfService(), fareQuoteParams.getClassOfService(),
					flightPriceRQ.getAvailPreferences().getSearchSystem().toString(), ApplicationEngine.XBE,
					userLanguage);

		} catch (ModuleException me) {
			log.error(me.getMessage(), me);
			success = false;
			messageTxt = BasicRH.getErrorMessage(me, userLanguage);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
		}

		return S2Constants.Result.SUCCESS;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	@JSON(serialize = false)
	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	@JSON(serialize = false)
	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isAddSegment() {
		return addSegment;
	}

	public void setAddSegment(boolean addSegment) {
		this.addSegment = addSegment;
	}

	public boolean isModifyBooking() {
		return modifyBooking;
	}

	public void setModifyBooking(boolean modifyBooking) {
		this.modifyBooking = modifyBooking;
	}

	public BaggageRatesDTO getBaggageRatesDTO() {
		return baggageRatesDTO;
	}

	public void setBaggageRatesDTO(BaggageRatesDTO baggageRatesDTO) {
		this.baggageRatesDTO = baggageRatesDTO;
	}
}