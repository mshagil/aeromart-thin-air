package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;
import com.isa.thinair.webplatform.api.dto.ReservationConstants;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Add Segment Payment Confirm
 * 
 * @author Pradeep Karunanayaka
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AddSegmentPaymentConfirmAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AddSegmentPaymentConfirmAction.class);

	private boolean success = true;

	private String messageTxt;

	private PaymentTO payment;

	private CreditCardTO creditInfo;

	private String reservationResponse;

	private String agentBalance;

	private String ownerAgentCode;

	private String pnr;

	private String groupPNR;

	private String hdnSelCurrency;

	private boolean convertedToGroupReservation;

	private String resContactInfo;

	private boolean actualPayment;

	private BigDecimal totalAmount;

	private GroupBookingRequestTO groupBookingRequest;

	private String groupBookingReference;

	private boolean isOhdResExpired;

	private String agentBalanceInAgentCurrency;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			BookingShoppingCart bookingShopingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			if (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
					&& resInfo.getExistingReleaseTimestamp().before(CalendarUtil.getCurrentZuluDateTime())
					&& AppSysParamsUtil.isCancelExpiredOhdResEnable()) {
				this.success = false;
				this.messageTxt = "Your on hold reservation has expired. Please reload";
				this.isOhdResExpired = true;
				return S2Constants.Result.SUCCESS;
			}

			// validate data
			this.validate(bookingShopingCart);

			// set tracking info
			TrackInfoDTO trackInfoDTO = this.getTrackInfo();

			boolean isGroupPnr = ReservationUtil.isGroupPnr(groupPNR);
			if (!isGroupPnr) {
				groupPNR = pnr;
			}

			Integer ipgId = null;
			if (request.getParameter(WebConstants.REQ_HDN_IPG_ID) != null
					&& !"".equals(request.getParameter(WebConstants.REQ_HDN_IPG_ID))) {
				ipgId = new Integer(request.getParameter(WebConstants.REQ_HDN_IPG_ID));
			}
			long requestID = -1;
			if (getGroupBookingReference() != null && !getGroupBookingReference().equals("")) {
				requestID = Long.parseLong(getGroupBookingReference());
				if (requestID > 0) {
					if (totalAmount != null) {
						payment.setAmount(totalAmount.toString());
					} else {
						payment.setAmount(payment.getCcAmount());
					}
				}
			}
			// transform the contact info
			CommonReservationContactInfo contactInfo = ReservationUtil.transformJsonContactInfo(resContactInfo);

			Map<String, String> mapPrivileges = (Map<String, String>) request.getSession().getAttribute(
					WebConstants.SES_PRIVILEGE_IDS);

			LCCClientResAlterModesTO lccClientResAlterQueryModesTO = (LCCClientResAlterModesTO) BookingUtil
					.getAlterModesToAddSeg(bookingShopingCart, payment, creditInfo, ipgId, ownerAgentCode,
							request.getParameter("paxPayments"), isGroupPnr, contactInfo, getHdnSelCurrency(),
							RequestHandlerUtil.getAgentCode(request), RequestHandlerUtil.getAgentStationCode(request),
							mapPrivileges, actualPayment, requestID);

			if (payment.getMode().equals("CONFIRM") || payment.getMode().equals("FORCE")) {
				lccClientResAlterQueryModesTO
						.setPaymentTypes(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS);
			} else {
				lccClientResAlterQueryModesTO
						.setPaymentTypes(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT);
			}

			LCCClientReservation lccReservation = ModuleServiceLocator.getAirproxySegmentBD().addSegment(
					lccClientResAlterQueryModesTO, getClientInfoDTO(),
					AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT), trackInfoDTO);
			if (lccReservation.isUseOtherCarrierPaxCredit()) {
				ReservationUtil.reconcileDummyCarrierReservation(groupPNR, trackInfoDTO);
				// set the group pnr and flag so the reservation will be loaded as a interline after the payments
				convertedToGroupReservation = true;
				groupPNR = pnr;
			} else if (lccClientResAlterQueryModesTO.isGroupPnr()) {
				convertedToGroupReservation = true;
			}

			// updated agent credit
			agentBalance = CommonUtil.getLoggedInAgentBalance(request);
			agentBalanceInAgentCurrency = CommonUtil.getLoggedInAgentBalanceInAgentCurrency(request);
			// clearing the block seats after modification success.
			request.getSession().setAttribute(S2Constants.Session_Data.LCC_SESSION_BLOCK_IDS, null);
		} catch (Exception e) {
			success = false;
			reservationResponse = "";
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public String groupBookingValidate() {
		try {
			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			long requestID = Long.parseLong(getGroupBookingReference());
			BookingShoppingCart bookingShopingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			Collection<ReservationPaxTO> pax = bookingShopingCart.getPaxList();
			int adultCount = 0, childCount = 0, infantCount = 0;
			for (ReservationPaxTO resPax : pax) {
				if (resPax.getPaxType().equals("AD")) {
					adultCount++;
				} else if (resPax.getPaxType().equals("CH")) {
					childCount++;
				} else {
					infantCount++;
				}
			}
			String paxCountStr = adultCount + "|" + childCount + "|" + infantCount;
			ArrayList<String> flightList = new ArrayList<String>();
			for (LCCClientReservationSegment reservationSeg : bookingShopingCart.getReservationAlterModesTO()
					.getExistingAllSegs()) {
				flightList.add(reservationSeg.getFlightSegmentRefNumber());
			}

			setGroupBookingRequest(ModuleServiceLocator.getGroupBookingBD().validateModifyResGroupBookingRequest(requestID,
					getTotalAmount(), paxCountStr, flightList, user.getAgentCode()));

			// If the agreed fare is set as a percentage then calculate it accordingly
			if (groupBookingRequest != null && groupBookingRequest.isAgreedFarePercentage()) {
				groupBookingRequest.setMinPaymentRequired(groupBookingRequest.getAgreedFare().divide(new BigDecimal("100"))
						.multiply(groupBookingRequest.getMinPaymentRequired()));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return S2Constants.Result.SUCCESS;
	}

	/**
	 * @param bookingShopingCart
	 * @return
	 */
	private void validate(BookingShoppingCart bookingShopingCart) throws ModuleException {

		PaymentMethod paymentMethod = payment.getPaymentMethod();
		if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {
			LCCClientResAlterQueryModesTO lCCClientResAlterQueryModesTO = bookingShopingCart.getReservationAlterModesTO();

			if ((ReservationUtil.getNoOfCnfSegments(lCCClientResAlterQueryModesTO.getExistingAllSegs()) + ReservationUtil
					.getNoOfCnfSegments(lCCClientResAlterQueryModesTO.getLccClientSegmentAssembler().getSegmentsMap().values())) > ReservationConstants.MAX_NO_OF_SEGMENTS_FOR_BSP_PAYMENT) {
				XBERuntimeException ex = new XBERuntimeException("bsp.payment.segment.count.exceeded");
				List<String> messageParameters = new ArrayList<String>();
				messageParameters.add(ReservationConstants.MAX_NO_OF_SEGMENTS_FOR_BSP_PAYMENT + "");
				ex.setMessageParameters(messageParameters);
				throw ex;
			}
		}
	}

	@Override
	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = super.getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		trackInfoDTO.setPaymentAgent(payment != null ? payment.getAgent() : ((UserPrincipal) request.getUserPrincipal())
				.getAgentCode());
		return trackInfoDTO;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public PaymentTO getPayment() {
		return payment;
	}

	public void setPayment(PaymentTO payment) {
		this.payment = payment;
	}

	public CreditCardTO getCreditInfo() {
		return creditInfo;
	}

	public void setCreditInfo(CreditCardTO creditInfo) {
		this.creditInfo = creditInfo;
	}

	public String getReservationResponse() {
		return reservationResponse;
	}

	public void setReservationResponse(String reservationResponse) {
		this.reservationResponse = reservationResponse;
	}

	public String getAgentBalance() {
		return agentBalance;
	}

	/**
	 * @return the hdnSelCurrency
	 */
	public String getHdnSelCurrency() {
		return hdnSelCurrency;
	}

	/**
	 * @param hdnSelCurrency
	 *            the hdnSelCurrency to set
	 */
	public void setHdnSelCurrency(String hdnSelCurrency) {
		this.hdnSelCurrency = hdnSelCurrency;
	}

	public void setOwnerAgentCode(String ownerAgentCode) {
		if (ownerAgentCode != null && !"".equals(ownerAgentCode.trim()) && !"undefined".equals(ownerAgentCode.trim())
				&& !"null".equals(ownerAgentCode.trim())) {
			this.ownerAgentCode = ownerAgentCode;
		}
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	/**
	 * @return the groupPNR
	 */
	public String getGroupPNR() {
		return groupPNR;
	}

	/**
	 * @return the convertedToGroupReservation
	 */
	public boolean isConvertedToGroupReservation() {
		return convertedToGroupReservation;
	}

	/**
	 * @param convertedToGroupReservation
	 *            the convertedToGroupReservation to set
	 */
	public void setConvertedToGroupReservation(boolean convertedToGroupReservation) {
		this.convertedToGroupReservation = convertedToGroupReservation;
	}

	public void setResContactInfo(String resContactInfo) {
		this.resContactInfo = resContactInfo;
	}

	public void setActualPayment(boolean actualPayment) {
		this.actualPayment = actualPayment;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public GroupBookingRequestTO getGroupBookingRequest() {
		return groupBookingRequest;
	}

	public void setGroupBookingRequest(GroupBookingRequestTO groupBookingRequest) {
		this.groupBookingRequest = groupBookingRequest;
	}

	public String getGroupBookingReference() {
		return groupBookingReference;
	}

	public void setGroupBookingReference(String groupBookingReference) {
		this.groupBookingReference = groupBookingReference;
	}

	public boolean isOhdResExpired() {
		return isOhdResExpired;
	}

	public void setOhdResExpired(boolean isOhdResExpired) {
		this.isOhdResExpired = isOhdResExpired;
	}
	
	public String getAgentBalanceInAgentCurrency() {
		return agentBalanceInAgentCurrency;
	}

	public void setAgentBalanceInAgentCurrency(String agentBalanceInAgentCurrency) {
		this.agentBalanceInAgentCurrency = agentBalanceInAgentCurrency;
	}

}
