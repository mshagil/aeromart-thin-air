package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.HashMap;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.v2.util.I18nUtil;


@Namespace(S2Constants.Namespace.PUBLIC)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = ""),
	@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class MakeReservationLanguageAction extends BaseRequestAwareAction {

	private HashMap<String, String> languageData;

	public String execute() throws JSONException {

		String[] pagesIDs = { "buttons","mkeRes","FindRes","reports" };
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		setLanguageData(I18nUtil.getXbeMesseges(userLanguage, pagesIDs));

		return S2Constants.Result.SUCCESS;

	}

	public HashMap<String, String> getLanguageData() {
		return languageData;
	}

	public void setLanguageData(HashMap<String, String> languageData) {
		this.languageData = languageData;
	}

}
