package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.CodeshareUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaymentPassengerTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentSummaryTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the Passenger Tab
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "", params = { "excludeProperties",
		"currencyExchangeRate\\.currency, countryCurrExchangeRate\\.currency" })
public class PaymentTabAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(PaymentTabAction.class);
	
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private boolean success = true;

	private String messageTxt;

	private Collection<PaymentPassengerTO> passengerPayment;

	private PaymentSummaryTO paymentSummaryTO;

	private FlightSearchDTO searchParams;

	private String selCurrency;

	private String airportMessage;

	private String airportCardPaymentMessages;

	private String selectedFlightList;

	private CurrencyExchangeRate currencyExchangeRate;

	private CurrencyExchangeRate countryCurrExchangeRate;

	private boolean blnPrintItinerary = true;
	
	private boolean csPnr = false;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			bookingShoppingCart.setUsedPaxCredit(BigDecimal.ZERO);

			// update handling fee according selected fare type
			bookingShoppingCart.setTotalExternalChangeAmount(EXTERNAL_CHARGES.HANDLING_CHARGE);
			//bookingShoppingCart.setTotalExternalChangeAmount(EXTERNAL_CHARGES.CREDIT_CARD);

			List<FlightSegmentTO> flightSegmentTOs = ReservationUtil
					.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
			// set flight segments - need to calculate the cc charges
			// bookingShoppingCart.setFlightSegmentTOs(flightSegmentTOs);
			
			ReservationDiscountDTO resDiscountDTO = null;
			if (bookingShoppingCart.getPriceInfoTO() != null) {
				FlightAvailRQ flightAvailRQ = ReservationBeanUtil.createFlightAvailRQ(searchParams);
				XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
						S2Constants.Session_Data.XBE_SES_RESDATA);

				resDiscountDTO = ReservationUtil.calculateDiscountForReservation(bookingShoppingCart,
						bookingShoppingCart.getPaxList(), flightAvailRQ, getTrackInfo(), false,
						bookingShoppingCart.getFareDiscount(), bookingShoppingCart.getPriceInfoTO(), resInfo.getTransactionId());
				bookingShoppingCart.setReservationDiscountDTO(resDiscountDTO);				
			}

			passengerPayment = PaymentUtil.createPaymentPaxList(bookingShoppingCart, resDiscountDTO, RequestHandlerUtil.getAgentCode(request));
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			paymentSummaryTO = PaymentUtil.createPaymentSummary(bookingShoppingCart, selCurrency, exchangeRateProxy, "0.00", RequestHandlerUtil.getAgentCode(request));
			currencyExchangeRate = bookingShoppingCart.getCurrencyExchangeRate();

			this.countryCurrExchangeRate = PaymentUtil.getBSPCountryCurrencyExchgRate(request);
			
			if(this.countryCurrExchangeRate != null) {
				paymentSummaryTO.setCountryCurrencyCode(countryCurrExchangeRate.getCurrency().getCurrencyCode());
			}

			// LOAD Airport Messages
			this.airportCardPaymentMessages = AirportMessageDisplayUtil.getAirportMessagesForFlightSegmentWise(flightSegmentTOs,
					ReservationInternalConstants.AirportMessageSalesChannel.XBE,
					ReservationInternalConstants.AirportMessageStages.CARD_PAYMENT, null);
			this.setBlnPrintItinerary();
			csPnr = CodeshareUtil.isCodeshareMCPnr(flightSegmentTOs);
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error("Generic exception caught.", e);
		}

		return S2Constants.Result.SUCCESS;
	}
	
	private boolean isCodesharePnr(List<FlightSegmentTO> flightSegmentTOs) {
		Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (gdsStatusMap != null && !gdsStatusMap.isEmpty() && flightSegmentTO.getOperatingAirline() != null) {
				for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
					if (gdsStatusTO != null && gdsStatusTO.isCodeShareCarrier()
							&& flightSegmentTO.getOperatingAirline().equals(gdsStatusTO.getCarrierCode())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public Collection<PaymentPassengerTO> getPassengerPayment() {
		return passengerPayment;
	}

	public PaymentSummaryTO getPaymentSummaryTO() {
		return paymentSummaryTO;
	}

	public boolean isBlnPrintItinerary() {
		return blnPrintItinerary;
	}

	public void setBlnPrintItinerary() {
		if (BasicRH.hasPrivilege(request, PriviledgeConstants.ACCEPT_PARTIAL_PAYMNETS)
				&& !BasicRH.hasPrivilege(request, PriviledgeConstants.PRINT_ITENERARY_FOR_PARTIAL_PAYMNETS)) {
			blnPrintItinerary = false;
		}
	}

	public void setSelCurrency(String selCurrency) {
		this.selCurrency = selCurrency;
	}

	/**
	 * @return the searchParams
	 */
	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	/**
	 * @param searchParams
	 *            the searchParams to set
	 */
	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	/**
	 * @return the airportMessage
	 */
	public String getAirportMessage() {
		return airportMessage;
	}

	public String getAirportCardPaymentMessages() {
		return airportCardPaymentMessages;
	}

	public CurrencyExchangeRate getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	public void setCurrencyExchangeRate(CurrencyExchangeRate currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public CurrencyExchangeRate getCountryCurrExchangeRate() {
		return countryCurrExchangeRate;
	}

	public void setCountryCurrExchangeRate(CurrencyExchangeRate countryCurrExchangeRate) {
		this.countryCurrExchangeRate = countryCurrExchangeRate;
	}

	/**
	 * @param selectedFlightList
	 *            the selectedFlightList to set
	 */
	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	public boolean isCsPnr() {
		return csPnr;
	}

	public void setCsPnr(boolean csPnr) {
		this.csPnr = csPnr;
	}

}
