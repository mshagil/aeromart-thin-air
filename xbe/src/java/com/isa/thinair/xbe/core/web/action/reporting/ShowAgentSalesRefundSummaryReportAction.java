package com.isa.thinair.xbe.core.web.action.reporting;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.reporting.AgentSalesRefundReportRH;

/**
 * Action class for Agent Sales,Refund & Summary Report
 * 
 * @author M.Rikaz
 * @since 09/12/2011
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.AGENT_SALES_REFUND_SUMMARY_REPORT),
		@Result(name = S2Constants.Result.RES_SUCCESS, value = S2Constants.Jsp.Reporting.AGENT_SALE_REF_RPT_RESULT) })
public class ShowAgentSalesRefundSummaryReportAction extends BaseRequestResponseAwareAction {

	public String execute() {
		return AgentSalesRefundReportRH.execute(request, response);
	}

}
