package com.isa.thinair.xbe.core.config;

import java.util.Map;
import java.util.Properties;

import com.isa.thinair.platform.api.DefaultModuleConfig;

public class XBEModuleConfig extends DefaultModuleConfig {

	private String defaultDateFormat;

	private String port;

	private String sslPort;

	private String nonSecureProtocol;

	private String secureProtocol;

	private String auditEnable;

	private Map supportedLanguages;

	private Map nonEncryptedURLs;

	private Properties emailProblemTo;

	private boolean loadExtPayDetailsInACC;

	private String defaultPaymentBroker;

	private boolean v2Enable;

	private Map<String, String> adultPaxDetailsFormat;

	private Map<String, String> infantPaxDetailsFormat;

	private boolean hideGDSOnHoldOption;

	private boolean uniqueReceiptNumber;

	private boolean overridePrivForNameChange;

	private String sessionKeepAliveIntervalInMinutes = "-1";

	private boolean useSecureLogin = true;

	public XBEModuleConfig() {
		super();
	}

	public String getDefaultDateFormat() {
		return defaultDateFormat;
	}

	public void setDefaultDateFormat(String defaultDateFormat) {
		this.defaultDateFormat = defaultDateFormat;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getSslPort() {
		return sslPort;
	}

	public void setSslPort(String sslPort) {
		this.sslPort = sslPort;
	}

	public String getNonSecureProtocol() {
		return nonSecureProtocol;
	}

	public void setNonSecureProtocol(String nonSecureProtocol) {
		this.nonSecureProtocol = nonSecureProtocol;
	}

	public String getSecureProtocol() {
		return secureProtocol;
	}

	public void setSecureProtocol(String secureProtocol) {
		this.secureProtocol = secureProtocol;
	}

	public String getAuditEnable() {
		return auditEnable;
	}

	public void setAuditEnable(String auditEnable) {
		this.auditEnable = auditEnable;
	}

	public Map getSupportedLanguages() {
		return supportedLanguages;
	}

	public void setSupportedLanguages(Map supportedLanguages) {
		this.supportedLanguages = supportedLanguages;
	}

	public Map getNonEncryptedURLs() {
		return nonEncryptedURLs;
	}

	public void setNonEncryptedURLs(Map secureURLs) {
		this.nonEncryptedURLs = secureURLs;
	}

	/**
	 * @return Returns the emailPnlAdlErrorTo.
	 */
	public Properties getEmailProblemTo() {
		return emailProblemTo;
	}

	/**
	 * @param emailPnlAdlErrorTo
	 *            The emailPnlAdlErrorTo to set.
	 */
	public void setEmailProblemTo(Properties emailProblemTo) {
		this.emailProblemTo = emailProblemTo;
	}

	public boolean isLoadExtPayDetailsInACC() {
		return loadExtPayDetailsInACC;
	}

	public void setLoadExtPayDetailsInACC(boolean loadExtPayDetailsInACC) {
		this.loadExtPayDetailsInACC = loadExtPayDetailsInACC;
	}

	/**
	 * @return Returns the defaultPaymentBroker.
	 */
	public String getDefaultPaymentBroker() {
		return defaultPaymentBroker;
	}

	/**
	 * @param defaultPaymentBroker
	 *            The defaultPaymentBroker to set.
	 */
	public void setDefaultPaymentBroker(String defaultPaymentBroker) {
		this.defaultPaymentBroker = defaultPaymentBroker;
	}

	public boolean isV2Enable() {
		return v2Enable;
	}

	public void setV2Enable(boolean v2Enable) {
		this.v2Enable = v2Enable;
	}

	public void setAdultPaxDetailsFormat(Map<String, String> adultPaxDetailsFormat) {
		this.adultPaxDetailsFormat = adultPaxDetailsFormat;
	}

	/**
	 * Returns the format defined in xbe-config.mod.xml file for Adult Passenger parsing form a csv file.
	 */
	public Map<String, String> getAdultPaxDetailsFormat() {
		return adultPaxDetailsFormat;
	}

	public void setInfantPaxDetailsFormat(Map<String, String> infantPaxDetailsFormat) {
		this.infantPaxDetailsFormat = infantPaxDetailsFormat;
	}

	/**
	 * Returns the format defined in xbe-config.mod.xml file for Infant Passenger parsing form a csv file.
	 */
	public Map<String, String> getInfantPaxDetailsFormat() {
		return infantPaxDetailsFormat;
	}

	public boolean isHideGDSOnHoldOption() {
		return hideGDSOnHoldOption;
	}

	public void setHideGDSOnHoldOption(boolean hideGDSOnHoldOption) {
		this.hideGDSOnHoldOption = hideGDSOnHoldOption;
	}

	/**
	 * @return the uniqueReceiptNumber
	 */
	public boolean isUniqueReceiptNumber() {
		return uniqueReceiptNumber;
	}

	/**
	 * @param uniqueReceiptNumber
	 *            the uniqueReceiptNumber to set
	 */
	public void setUniqueReceiptNumber(boolean uniqueReceiptNumber) {
		this.uniqueReceiptNumber = uniqueReceiptNumber;
	}

	/**
	 * Overrides the Name Change Privilege
	 * 
	 * @return the overridePrivForNameChange
	 */
	public boolean isOverridePrivForNameChange() {
		return overridePrivForNameChange;
	}

	/**
	 * @param overridePrivForNameChange
	 *            the overridePrivForNameChange to set
	 */
	public void setOverridePrivForNameChange(boolean overridePrivForNameChange) {
		this.overridePrivForNameChange = overridePrivForNameChange;
	}

	public String getSessionKeepAliveIntervalInMinutes() {
		return sessionKeepAliveIntervalInMinutes;
	}

	public void setSessionKeepAliveIntervalInMinutes(String sessionKeepAliveIntervalInMinutes) {
		this.sessionKeepAliveIntervalInMinutes = sessionKeepAliveIntervalInMinutes;
	}

	public void setUseSecureLogin(boolean useSecureLogin) {
		this.useSecureLogin = useSecureLogin;
	}

	public boolean isUseSecureLogin() {
		return useSecureLogin;
	}

}
