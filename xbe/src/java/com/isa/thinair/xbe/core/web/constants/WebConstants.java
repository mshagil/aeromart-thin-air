package com.isa.thinair.xbe.core.web.constants;

public class WebConstants {
	
	public static final String XBE_LOGIN_CAPTCHA = "isCaptchaValidated";

	public static final String DEFAULT_LANGUAGE = "en";
	
	// Master file Record Status
	public static final String RECORD_STATUS_INACTIVE = "INA";

	public static final String RECORD_STATUS_ACTIVE = "ACT";

	// Agent Types
	public static final String AGENT_TYPE_TRAVEL_AGENT = "TA";

	public static final String AGENT_TYPE_GSA = "GSA";

	// User message types
	public static final String MSG_TYPE = "reqMsgType";

	public static final String MSG_SUCCESS = "Confirmation";

	public static final String MSG_ERROR = "Error";

	// payment methods
	public static final String PAYMENT_TYPE_CASH = "CASH";

	public static final String PAYMENT_TYPE_ONACCOUNT = "ONACCOUNT";

	public static final String PAYMENT_TYPE_CREDITCARD = "CREDITCARD";

	public static final String PARAM_TXT_ADDRESS = "selAddress";

	// Key values

	public static final String KEY_SAVE_SUCCESS = "cc.airadmin.save.success";

	public static final String KEY_DELETE_SUCCESS = "cc.airadmin.delete.success";

	public static final String KEY_REFUND_SUCCESS = "paxaccount.refund.success";

	public static final String KEY_REFUND_FAILED = "paxaccount.refund.failed";

	public static final String KEY_EXT_UPDATE_FAILED = "account.ext.update.failed";

	public static final String KEY_AUTHORIZATION_FAILED = "authorization.failed";

	public static final String KEY_UNAUTHORIZED_OPERATION = "unauthorized.operation";

	public static final String KEY_BLOCK_SEATS_FAILED = "reservation.block.seats.failed";

	public static final String KEY_SENDNEW_PNL_SUCCESS = "cc.sendnew.pnl.success";

	public static final String KEY_SENDNEW_ADL_SUCCESS = "cc.sendnew.adl.success";

	public static final String KEY_RESEND_PNL_SUCCESS = "cc.resend.pnl.success";

	public static final String KEY_RESEND_ADL_SUCCESS = "cc.resend.adl.success";
	
	public static final String KEY_SENDNEW_PAL_SUCCESS = "cc.sendnew.pal.success";

	public static final String KEY_SENDNEW_CAL_SUCCESS = "cc.sendnew.cal.success";

	public static final String KEY_RESEND_PAL_SUCCESS = "cc.resend.pal.success";

	public static final String KEY_RESEND_CAL_SUCCESS = "cc.resend.cal.success";
	
	public static final String ERR_UNAUTHORIZED_ACCESS = "err.unauthorized.access";

	public static final String KEY_GENERATE_SUCCESS = "cc.airadmin.generate.success";

	public static final String KEY_TRANSFER_SUCCESS = "cc.airadmin.transfer.success";

	public static final String KEY_ADD_SUCCESS = "cc.airadmin.add.success";

	public static final String KEY_SEND_EMAIL_SUCCESS = "cc.airadmin.send.emails.success";

	public static final String KEY_UPDATE_SUCCESS = "cc.airadmin.update.success";

	public static final String KEY_RESET_SUCCESS = "cc.airadmin.reset.success";

	public static final String KEY_FFP_EDIT_SUCCESS = "cc.airadmin.edit.ffp.success";

	public static final String KEY_RESET_NOTAUTHORIZED = "cc.airadmin.user.notAuthorized";

	public static final String KEY_FFP_EIDT_NOTAUTHORIZED = "cc.airadmin.user.notAuthorized";

	public static final String KEY_MAX_ADMINUSERS = "cc.airadmin.user.maxUsers";

	public static final String KEY_NOTIFICATIONS_SEND_SUCCESS = "notifications.send.success";

	public static final String KEY_RECEIPT_NUMBER_EXISTS = "cc.receiptnumber.exists";
	
	public static final String KEY_DUPLICATE_RECORD_EXIST = "err.duplicate.record.exist";

	public static final String KEY_VOUCHER_ID_INVALID = "error.voucher.redeem.invalid";

	public static final String KEY_VOUCHER_PAX_NAME_INVALID = "error.voucher.pax.name.invalid";

	public static final String KEY_VOUCHER_REDEEM_SUCCESS = "success.voucher.redeem";

	public static final String KEY_VOUCHER_MOBILE_NUMBER_INVALID = "error.voucher.mobile.number.invalid";

	public static final String KEY_VOUCHER_OTP_CODE_INVALID = "error.voucher.otp.code.invalid";

	public static final String KEY_VOUCHER_USED_IN_SESSION = "error.voucher.already.used.for.reservation";

	public static final String KEY_VOUCHER_VALID_PERIOD_INVALID = "error.voucher.valid.period.invalid";

	// request attributes
	
	public static final String REQ_LANGUAGE = "prefLangunage";

	public static final String REQ_CLIENT_MESSAGES = "reqClientErrors";

	public static final String REQ_PARAMS = "reqParams";

	public static final String REQ_ERROR_SERVER_MESSAGES = "reqServerErrors";

	public static final String REQ_ERROR_REDIRECT = "reqErrorRedirect";

	public static final String REQ_ERROR_FRAME = "reqErrorFrame";

	public static final String REQ_MESSAGE = "reqMessage";

	protected final static String REQ_SESSION_BLOCK_SEAT_IDS = "seatBlockIDs";

	public static final String REQ_HTML_LOGIN_UID = "logUID";

	public static final String REQ_HTML_AGENT_CODE = "agentCode";

	public static final String REQ_HTML_AGENT_NAME = "agentName";

	public static final String REQ_HTML_MENU_DATA = "menuList";

	public static final String REQ_HTML_GSA_LIST = "reqGSAList";

	public static final String REQ_HTML_USER_LIST = "reqAgentUserList";

	public static final String REQ_HTML_DETAILS = "reqHtmlDetails";

	public static final String REQ_HTML_AGENTTYPE_LIST = "reqAgentTypeList";
	
	public static final String REQ_HTML_CMP_PAY_AGENTSTATUS_LIST = "reqCmpPayAgentStatusList";

	public static final String REQ_HTML_FLOWN_PASSENGER_LIST = "reqFlownPassengerList";

	public static final String REP_RPT_SHOW_PAY = "reqSowPay";

	public static final String REP_MOD_DETAILS_ENABLED = "reqModDetailsEnabled";

	public static final String REQ_HTML_AIRPORT_OWNER_LIST = "airportOwnerList";

	public static final String REQ_HTML_AIRPORT_LIST = "airportList";

	public static final String REQ_HTML_AIRPORT_LIST_V2 = "airportListV2";

	public static final String REQ_CURRENCY_EXCHANGE_LIST = "currencyExchangeList";

	public static final String REQ_HTML_CURRENCY_LIST = "currencyList";

	public static final String REQ_HTML_TITLE_LIST = "titleList";

	public static final String REQ_HTML_TITLE_LIST_WITH_CHILD = "titleListWithChild";

	public static final String REQ_HTML_DEFAULT_PAX_TYPES_LIST = "defaultPaxTypeList";

	public static final String REQ_HTML_TITLE_VISIBLE_LIST = "titleVisibleList";

	public static final String REQ_HTML_INFANT_TITLE_LIST = "intantTitleList";

	public static final String REQ_HTML_SALES_COUNTRY_LIST = "coutnryList";

	public static final String REQ_HTML_NATIONALITY_LIST = "nationalityList";
	
	public static final String PAX_TITLES_VISIBLE = "paxTitlesVisible";

	public static final String REQ_HTML_SSRCODE_LIST = "ssrCodes";

	public static final String REQ_HTML_CARD_TYPE_LIST = "cardType";

	public static final String REQ_HTML_TERMS_N_CONDITIONS = "termsConditions";

	public static final String REQ_HTML_SALES_POINT_LIST = "reqSalesPointList";

	public static final String REQ_HTML_SALES_POINT_WITH_DRY_LIST = "reqSalesPointWithDryList";

	public static final String REQ_HTML_GDS_MSGTYPE_LIST = "gdsMsgTypeList";

	public static final String REQ_HTML_GDS_NAMES_LIST = "gdsNamesList";

	public static final String REQ_HTML_GDS_PROCESS_STATUS_LIST = "gdsProcessStatusList";

	public static final String REQ_HTML_ROWS = "reqHtmlRows";

	public static final String REQ_TOTAL_RECORDS = "totalNoOfRecords";

	public static final String REQ_HTML_PAYMENTTYPE_LIST = "reqPaymentType";

	public static final String REQ_HTML_CUSTOM_CHARGE_ADJUSTMENT_CHARGE_CODES_LIST = "reqCustomChargeAdjustmentCodesType";

	public static final String REQ_HTML_PAYMENTCARRIER_LIST = "reqPaymentCarrierList";

	public static final String REQ_HTML_PAYMENTCARRIER_ENABLE = "enableAdjustmentCarrier";

	public static final String REQ_PRIVILEGES = "reqPrivileges";

	public static final String REQ_SHOW_E_TICKET = "ekt";

	public static final String REQ_SHOW_E_TICKET_PAX = "showPaxETicket";

	public static final String REQ_HTML_REQUEST = "reqRequest";

	public static final String REQ_RESPONSE_STRING = "reqResponseString";

	public static final String REQ_HTML_REQUEST_DATA = "reqRequestData";

	public static final String REQ_HTML_ACTION_LIST = "ClearAlertActions";

	public static final String REQ_TERRITORY = "reqterritory";

	public static final String REQ_INOICE_SUMM = "reqInvoiceSummData";

	public static final String REQ_PAX_STATUS_LIST = "reqPaxStatusList";
	
	public static final String REQ_PFS_STATUS_LIST = "reqPfsStatusList";

	public static final String REQ_CHARGE_CODE_LIST = "reqChargeCodeList";

	public static final String REQ_CATEGORY_LIST = "reqCategoryList";

	public static final String REQ_GROUP_LIST = "reqGroupList";

	public static final String REQ_IS_EXACT_MATCH_EXISTS = "isExactMatchExists";

	public static final String REQ_IS_DUPLICATE_EXISTS = "isDuplicateAgentAmountCombinationExists";
	
	public static final String REQ_GST_MESSAGE_ENABLED_AIRPORTS = "gstMessageEnabledAirports";
	
	public static final String REQ_GST_MESSAGE = "gstMessage";
	
	public static final String REQ_PREV_TA_CREDIT_REVERSE= "enableTACreditReverse";
	
	public static final String REQ_FORCE_CONFIRMED_PAY_TYPES = "reqForceConfirmedPayStatuType";

	// Credit card Payment for MakeBooking - End

	public static final String REQ_JS_ARRAY = "reqJSArray";

	public static final String REQ_HTML_ALL_TRAVEL_AGENT_LIST = "reqAllTravelAgentsHtml";

	public static final String REQ_HTML_ALL_TRANSFER_TRAVEL_AGENT_LIST = "reqAllTransferTravelAgentsHtml";

	public static final String REQ_HTML_OWN_TRAVEL_AGENT_LIST = "reqOwnTravelAgentsHtml";

	public static final String REQ_HTML_ALL_IPGS_LIST = "reqAllIpgsHtml";
	
	public static final String REQ_HTML_ALL_OFFLINE_IPGS_LIST = "reqAllOfflineIpgsHtml";

	public static final String REQ_HTML_MODIFCATION_ENABLED_OFFLINE_IPGS_LIST = "reqModificationEnabledOfflineIpgsHtml";

	public static final String REQ_HTML_E_TICKET_STATUS = "reqEticketStatusHtml";

	public static final String REQ_HTML_SEGMENT_STATUS = "reqSegmentStatusHtml";

	public static final String REQ_HTML_ITINERARY_LANG_LIST = "reqItnLangList";

	public static final String REQ_HTML_TIMEIN = "reqTimeIn";

	public static final String REQ_HTML_DEFAULT_OPERATIONTYPE = "reqDefaultOperationType";

	public static final String REQ_HTML_DEFAULT_FLIGHTSTATUS = "reqFlightDefStatus";

	public static final String REQ_HTML_FLIGHT_DATA = "reqFlightData";

	public static final String REQ_OPERATION_MODE = "reqOperationMode";

	public static final String REQ_DEFAULT_AIRLINE_CODE = "reqDefaultAirlineCode";

	public static final String REQ_ALLOW_CARRIER_PREFIX = "reqIsAllowCarrierPrefix";

	public static final String REQ_IS_REMOTE_USER = "reqIsRemoteUser";

	public static final String REQ_IS_EXCEPTION = "reqIsExceptionOccured";

	public static final String REQ_HTML_SITA_DATA = "reqSITAData";

	public static final String REQ_HTML_DEFAULT_MAIL_SERVER = "reqDefaultMailServer";

	public static final String REQ_HTML_MAILSERVER_LIST = "reqMailServerList";

	public static final String REQ_OPERATION_UI_MODE = "reqOperationUIMode";

	public static final String REQ_HTML_FORM_DATA = "reqFormData";

	public static final String REQ_FORM_FIELD_VALUES = "reqFormFieldValues";

	public static final String REQ_HTML_GDS_MSG_DATA = "reqGDSMessageData";

	public static final String REQ_HTML_PNLADL_TEXT_DATA = "reqPNLADLTextData";
	
	public static final String REQ_STATION_LIST = "reqStationList";

	public static final String REQ_JOURNEY_TYPE_LIST = "reqJourneyType";
	
	public static final String REQ_RESERVATION_FLOW = "reqReservationFlow";

	public static final String REQ_HTML_COUNTRY_LIST = "reqCountryList";

	public static final String REQ_PAYMENT_GATEWAY_ERROR = "pay.gateway.error.";

	public static final String REQ_PAYMENT_GATEWAY_ERROR_DEFAULT = "pay.error.default";

	public static final String REQ_CONN_LIMIT = "sesConnectionLimit";

	public static final String REQ_MIN_CONN_LIMIT = "sesMinConnectionLimit";

	public static final String REQ_HTML_STANDBY_LIST = "standByList";

	public static final String REQ_HTML_SEARCH_OPTION_LIST = "arrSearchOptions";

	public static final String REQ_HTML_RESERVATION_SEARCH_OPTION_LIST = "arrReservationSearchOptions";

	public static final String REQ_HTML_THRUCHK_DATA = "reqThruCheckData";

	public static final String REQ_HTML_DEFAULT_CRRIERCODE = "reqcrriercode";

	public static final String REQ_LANGUAGE_HTML = "reqLanguageHtml";

	public static final String REQ_HTML_CUS_TYPE_LIST = "reqCusTypeHtml";

	public static final String REQ_PAX_CATEGORY_FOID_LIST = "reqPaxCategoryFOIDList";

	public static final String REQ_HTML_FARE_CAT_LIST = "reqFareCatList";

	public static final String REQ_HTML_FARE_CAT_LIST_ALL = "reqFareCatListAll";

	public static final String REQ_RECIEPT_BODY_HTML = "reqRecieptBodyHtml";

	public static final String REQ_HTML_CARRIER_CODE_LIST = "carrierCodeList";

	public static final String REQ_HTML_COUNTRY_PHONE_LIST = "countryPhoneList";

	public static final String REQ_HTML_COUNTRY_PHONE_JSON = "countryPhoneJson";

	public static final String REQ_HTML_CC_TYPES_FOR_PGW = "ccTypeForPgw";

	public static final String REQ_HTML_ACTUAL_PAYMENT_METHOD_DATA = "reqAcutalPaymentMethodData";

	public static final String REQ_BASE_CURRENCY = "reqBaseCurrency";

	public static final String REQ_ACCOUNT_CODE = "reqAccountCode";

	public static final String REQ_CARRIER_AGENT = "reqCarrierAgent";

	public static final String REQ_HTML_COMMITION_TYPE_LIST = "reqCommTypeList";
	
	public static final String REQ_AGENT_LEVEL_ETICKETS ="reqAgntLevelEtickets";

	public static final String REQ_CURRENT_YEAR = "reqCurrentYear";

	public static final String REQ_SHOW_TOTAL = "reqShowTotal";

	public static final String REQ_INVOICEYEAR = "reqBasePrevious";

	public static final String REQ_INVOICEPERIOD = "reqInvPeriods";

	public static final String REQ_IS_WEEKLYINVOICE_ENABLED = "isWeeklyInvoiceEnabled";

	public static final String REQ_HTML_INVOIVEGEN_DETAILS = "reqinvoiceGenDetails";

	public static final String REQ_AGENT_CODE = "reqAgentCode";

	public static final String REQ_AGENT_NAME = "reqAgentName";

	public static final String REQ_HTML_PAYMENTPURPOSE_LIST = "reqPaymentPurpose";

	public static final String REQ_HTML_INVOICE_ARRAY = "reqInvoiceArray";

	public static final String REQ_ADJUST_PAYROWS = "reqAdjustedPayment";

	public static final String REQ_ACCOUNT_NO = "reqAccountNo";

	public static final String REQ_ACCOUNT_NAME = "reqAccountName";

	public static final String REQ_BANK_NAME = "reqBankName";

	public static final String REQ_BANK_ADDRESS = "reqBankAddress";

	public static final String REQ_SWIFT_CODE = "reqSwiftCode";
	
	public static final String REQ_BANK_IBAN_CODE = "reqBankIbanCode";
	
	public static final String REQ_BANK_BENEFICIARY_NAME = "reqBankBeneficiaryName";

	public static final String REQ_PAYMENT_TERMS = "reqPaymentTerms";

	public static final String REQ_HTML_AGENT_LIST = "reqAgentList";

	public static final String REQ_HTML_SERVICE_CHANNELS = "reqServiceChannels";

	public static final String REQ_HTML_DRY_USER_ENABLE = "reqDryUserEnable";

	public static final String REQ_CURRENT_MONTH = "reqCurrentMonth";

	public static final String REQ_SET_PRIVILEGE = "reqsetPrivi";

	public static final String REQ_SEARCH_CURR = "reqsearchcurr";

	public static final String REQ_CARRIER_SELECT_LIST = "reqCarrierList";

	public static final String REQ_AIRLINE_SELECT_LIST = "reqAirlineList";

	public static final String REQ_NO_OF_CARRIERS = "reqNoofCarriers";

	public static final String REQ_HTML_USER_ROLE_DATA = "reqUserRoleHtmlData";

	public static final String REQ_HTML_USER_DATA = "reqUserData";

	public static final String REQ_HTML_AGENTSTATUS_LIST = "reqAgentStatusList";

	public static final String REQ_HTML_ALLOW_LOCAL_AGENT = "reqAllowLocalAgent";

	public static final String REQ_HTML_USER_AGENT_TYPE = "reqUserAgentType";

	public static final String REQ_HTML_ACTUAL_USER_AGENT_TYPE = "reqActualUserAgentType";

	public static final String REQ_HTML_AIRLINE_USER_AGENT_TYPE = "reqAirlineAgentType";

	public static final String REQ_HTML_ALLOW_LOCAL_CR = "reqAllowLocalCr";

	public static final String REQ_HTML_USER_CURR = "reqUserCurr";

	public static final String REQ_TRANSACTION_DISPLAYSTATUS = "reqDisplayTransactionStatus";

	public static final String REQ_TRANSACTION_STATUS = "reqTransactionStatus";

	public static final String REQ_AGENT_HANDLING_FEE_ENABLE = "reqAgentHandlingFee";

	public static final String REQ_CREDIT_HISTORY_ROWS = "reqCreditHistoryRow";

	public static final String REQ_HTML_AGENT_COMBO = "reqAgentCombo";

	public static final String REQ_HTML_GSA_COMBO = "reqGSACombo";

	public static final String REQ_HTML_AIRPORT_COMBO = "reqAirportCombo";

	public static final String REQ_HTML_TERRITORY_COMBO = "reqTerritoryCombo";

	public static final String REQ_HTML_OWN_TERRITORY_COMBO = "reqOwnTerritoryCombo";

	public static final String REQ_HTML_AIRPORT_TERRITORY_LIST = "reqStatTerList";

	public static final String REQ_HTML_CURRENCY_COMBO = "reqCurrencyCombo";

	public static final String REQ_HTML_STATION_COUNTRY = "reqStationCountry";

	public static final String REQ_HTML_STATION_CURRENCY = "reqStationCurrency";

	public static final String REQ_HTML_USER_THEME_LIST = "reqUserThemes";

	public static final String REQ_AGENT_TYPE_CREDIT_APPLICABLE = "reqAgentTypeCreditApplicable";

	public static final String REQ_POPUPMESSAGE = "popupmessage";

	public static final String REQ_TOTAL_STRING = "totalString";

	public static final String REQ_HTML_COUNTTRY_CURR = "Country Currency";

	public static final String REQ_HANDLING_CHARGES = "reqHandlingCharge";

	public static final String REQ_AGENT_PAYMENT_OPTIONS = "reqAgentPaymentOptions";

	public static final String REQ_AGENT_CURR_DET = "reqAgCurrDet";

	public static final String REQ_HDN_IPG_ID = "hdnIPGId";
	
	public static final String REQ_REASON_FOR_ALLOW = "reasonForAllowBlPax";

	public static final String REQ_PRIV_TA_PAY_SETTLE_ANY = "hasPriviGSA";
	
	public static final String REQ_PRIV_TA_PAY_VIEW_ONLY = "hasPriviForViewPayOnly";

	public static final String REQ_SEARCH_IF_GSA_USER = "searchGSA";

	public static final String REQ_IF_GSA_NAME_CODE = "gNameNCode";

	public static final String REQ_HTML_PWD_RESETED = "userPwdReseted";

	public static final String REQ_HTML_SHOW_RESET_MSG = "pwdResetMsg";

	public static final String REQ_HTML_PWD_EXPIRED = "userPwdExpired";

	public static final String REQ_HTML_USR_NOTES = "reqUserNotes";

	public static final String REQ_SERVICE_CATEGORY_LIST = "reqServiceCategories";

	public static final String REQ_HTML_CHECKIN_NOTES = "reqCheckinNotes";

	public static final String REQ_FLT_NUMBER = "flightNumber";

	public static final String REQ_FLT_ID = "fltId";

	public static final String REQ_FLT_DATE_TIME = "flightDateTime";

	public static final String REQ_FLT_STATUS = "flightStatus";

	public static final String REQ_FLT_CHECKIN_STATUS = "flightCheckinStatus";

	public static final String REQ_AIRPORT_CODE = "airportCode";

	public static final String REQ_MODEL_CAPACITIES = "modleCapacities";

	public static final String REQ_USERPRINCIPLE_CARRIER = "carrier";

	public static final String REQ_TRAVEL_AGENT_INCENTIVE_ENABLED = "taIncentiveEnabled";

	public static final String REQ_TRAVEL_AGENT_PAY_REF_ENABLED = "payRefEnabled";

	public static final String REQ_CARRIER_CODE_LIST_OPTIONS = "carrierCodeListOptions";

	public static final String REQ_CHARGE_APPLIES_TO_LIST = "reqChargeAppliesToList";

	public static final String REQ_GSA_LIST = "reqGSAList";

	public static final String REQ_ALL_REP_AGENT_LISTS = "allSubAgentTypes";

	public static final String REQ_MULTIPLE_GSAS_FOR_A_STATION = "reqMultipleGSAsForAStationEnabled";
	
	public static final String REQ_GSA_V2 = "reqGSAStructureV2Enabled";

	public static final String REQ_CHARGE_FAREBASIS_TO_LIST = "reqChargeFareBasisToList";

	public static final String REQ_BOOKING_CATEGORY_LIST = "reqBookingCategoryList";

	public static final String REQ_ALL_BOOKING_CATEGORY_LIST = "reqAllBookingCategoryList";

	public static final String REQ_ITINERARY_FARE_MASK_OPTIONS = "reqItineraryFareMaskOptions";

	public static final String REQ_DASHBOARD_PREF_JSON = "jsonDashboardPref";

	public static final String REQ_AGENT_MAINTAIN_BSP_CRED_LIM_ENABLED = "maintainAgentBspCredLmt";

	public static final String REQ_LOGICAL_CABIN_CLASS_ENABLED = "reqLogicalCCEnabled";

	public static final String REQ_HTML_ACTIVE_AIRPORT_SELECT_LIST = "reqActiveViaList";

	public static final String REQ_HTML_ACTIVE_STATION_SELECT_LIST = "reqActStationList";

	public static final String REQ_AUTO_SEAT_ASSIGNMENT_ENABLED = "reqAutoSeatAssignmentEnabled";

	public static final String REQ_AUTO_SEAT_ASSIGNMENT_START_ROW = "reqAutoSeatAssignmentStartingRow";

	public static final String REQ_ALL_COUPON_STATUS_LIST = "reqAllCouponStatusList";

	public static final String REQ_ALL_PAX_STATUS_LIST = "reqAllPaxStatusList";

	public static final String REQ_AGENT_FAREMASK_STATUS = "reqAgentFareMaskStatus";

	public static final String REQ_CABINCLASS_RANKING_MAP = "reqCabinClassRankingMap";

	public static final String REQ_SHOW_FARE_MASK_OPTION = "reqShowFareMaskOption";

	public static final String REQ_SHOW_CHARTER_AGENT_OPTION = "reqShowCharterAgentOption";
	
	public static final String REQ_NIC_MANDATORY_FOR_DOMESTIC_SEGMENTS = "reqIsRequestNICForReservationsHavingDomesticSegments";

	public static final String REQ_IS_PREVENT_ADDING_DUPLICATE_RECORDS = "reqIsPreventDuplicateRecord";

	public static final String REQ_BOOKING_TYPE_LIST = "reqBookingType";
	
	public static final String REQ_AGENT_USER_LIST = "reqAgentUser";
	
	public static final String REQ_COUNTRY_STATE_MAP = "reqCountryStateMap";

	public static final String REQ_VISIBLE_TERRITORIES_STATIONS = "reqVisibleTerritoriesStations";
	
	public static final String REQ_AGENT_CREDIT_SUMMARY= "reqAgentCreditSummary";
	
	public static final String REQ_SHOW_AGENT_CREDIT_SUMMARY= "reqShowCreditSummary";	
	
	public static final String REQ_AGENT_TAX_REG_NO = "reqAgentTaxRegNo";
	
	public static final String REQ_HTML_STN_DATA = "reqStnHtmlData";
	
	public static final String REQ_HTML_AGENT_WISE_ONDS = "agentWiseOndMap";
	
	public static final String REQ_AGENT_ROUTE_SELECTIONS ="reqAgntRouteSelection";
	
	// response attributes
	public static final String RES_DEFAULT_CARRIER_NAME = "reqCarrName";

	public static final String RES_DEFAULT_AIRLINE_ID_CODE = "defaultAirlineId";

	// session attributes
	public static final String SES_HTML_AIRPORT_DATA = "sesHtmlAirportData";

	public static final String SES_HTML_COS_DATA = "sesHtmlCosData";

	public static final String REQ_HTML_COS_WITH_LOGICAL_DATA = "reqHtmlCosWithLogicalData";

	public static final String SES_CURRENT_USER = "sesCurrentUser";

	public static final String SES_PRIVILEGE_IDS = "sesPrivilegeIds";

	public static final String SES_HTML_YEAR_DATA = "sesHtmlYearData";

	public static final String SES_RESERVATION = "sesReservation";

	public static final String SES_HTML_OPERATIONTYPE_LIST_DATA = "sesOperationType";

	public static final String SES_HTML_STATUS_DATA = "sesStatus";

	public static final String SES_HTML_CHECKIN_STATUS_DATA = "sesCheckinStatus";

	public static final String SES_EXTERNAL_CHARGES_DATA = "sesExternalChargesData";

	// public static final String SES_MAP_ALL_TRAVELS = "sesAllTravelAgents";

	public static final String SES_COS = "reqCOS";

	public static final String SES_PAX_LOAD = "reqLoad";

	// System Parameters
	public static final String PARAM_SYSTEM_DETAILS = "sysParam";

	public static final String PARAM_NOTE = "txtaNote";

	public static final String PARAM_PNR_PAX_ID = "pnrPaxId";

	// Parameters
	public static final String PARAM_SES_AGENT_BALANCE = "sesAgentBalance";

	public static final String PARAM_SES_AGENT_BALANCE_IN_AGENT_CURRENCY = "sesAgentBalanceInAgentCurrency";
	
	public static final String PARAM_SES_SHOW_AGENT_BALANCE_IN_AGENT_CURRENCY = "sesShowAgentBalanceInAgentCurrency";
	
	public static final String PARAM_SELECTED_ALERTS = "hdnSelectedAlerts";

	public static final String PARAM_ALERT_INFO = "hdnAlertInfo";

	public static final String PARAM_PNR_NO = "pnrNo";

	public static final String PARAM_HDN_COMMANDS = "hdnCommands";

	public static final String PARAM_RAD_PAY_METHOD = "radPayMethod";

	public static final String PARAM_SEL_CARD_TYPE = "selCardType";

	public static final String PARAM_TXT_HOLDER_NAME = "txtHolderName";

	public static final String PARAM_TXT_SEC_CODE = "txtSecCode";

	public static final String PARAM_TXT_CARD_NO = "txtCardNo";

	public static final String PARAM_SEL_AGENT = "selAgent";

	public static final String PARAM_TXT_REFUND = "txtRefund";

	public static final String PARAM_FLIGHT_SEG_ID = "flightSegId";

	public static final String PARAM_FORCE_CONFIRM = "forceConfirm";

	public static final String PARAM_TXT_EXPIRY_DATE = "txtExpiryDate";

	public static final String PARAM_HDN_PAY_ID = "hdnPayId";

	public static final String PARAM_PAY_CURR_AMOUNT = "payCurrAmt";

	public static final String PARAM_BASE_PAY_AMOUNT = "basePayAmt";

	public static final String PARAM_HDN_PNRPAX_IDS = "hdnPNRPaxIds";

	public static final String PARAM_CCD_ID = "ccdId";

	public static final String PARAM_HDN_CHK_INDIVIDUAL = "hdnChkIndi";

	public static final String FLIGHT_SEGMENT_IDS = "flightSegIds";

	// Actions for ajax and modes
	public static final String ACTION_SEARCH = "search";

	public static final String ACTION_VIEW = "VIEW";

	public static final String ACTION_VIEW_SUMMARY = "VIEWSUMMARY";

	public static final String ACTION_VIEW_REQ_XML = "VIEW_REQ_XML";

	public static final String ACTION_VIEW_RES_XML = "VIEW_RES_XML";

	public static final String ACTION_DELETE = "delete";

	public static final String ACTION_REMOVE_PAX = "REMOVEPAX";

	public static final String ACTION_RES_CANCEL = "RESCANCEL";

	public static final String ACTION_RES_VOID = "RESVOID";

	public static final String ACTION_MODIFY_SEGMENT = "MODIFYSEGMENT";

	public static final String ACTION_CANCEL_SEGMENT = "CANCELSEGMENT";

	public static final String ACTION_NEWPNL = "NEWPNL";

	public static final String ACTION_NEWADL = "NEWADL";

	public static final String ACTION_RESENDPNL = "RESENDPNL";

	public static final String ACTION_RESENDADL = "RESENDADL";

	public static final String ACTION_REPRINT_PNL = "PNLREPRINT";

	public static final String ACTION_REPRINT_ADL = "ADLREPRINT";

	public static final String ACTION_PRINT_NEW_PNL = "NEWPNLPRINT";

	public static final String ACTION_PRINT_NEW_ADL = "NEWADLPRINT";
	
	public static final String ACTION_NEWPAL = "NEWPAL";

	public static final String ACTION_NEWCAL = "NEWCAL";

	public static final String ACTION_RESENDPAL = "RESENDPAL";

	public static final String ACTION_RESENDCAL = "RESENDCAL";

	public static final String ACTION_REPRINT_PAL = "PALREPRINT";

	public static final String ACTION_REPRINT_CAL = "CALREPRINT";

	public static final String ACTION_PRINT_NEW_PAL = "NEWPALPRINT";

	public static final String ACTION_PRINT_NEW_CAL = "NEWCALPRINT";

	public static final String ACTION_PRINT = "PRINT";

	public static final String ACTION_EDIT = "EDIT";

	public static final String ACTION_SAVE = "SAVE";

	public static final String ACTION_ADD = "ADD";

	public static final String ACTION_GENERATE = "GENERATE";

	public static final String ACTION_TRANSFER = "TRANSFER";

	public static final String ACTION_EMAIL = "EMAIL";

	public static final String ACTION_RESET = "RESET";

	public static final String ACTION_EDIT_FFP = "EDIT_FFP";

	public static final String ACTION_LOAD = "LOAD";

	public static final String ACTION_SEND = "SEND";

	public static final String ACTION_CANCEL_FLIGHT = "CANCEL";

	public static final String ACTION_UPDATE_FLIGHT = "UPDATE";

	public static final String ACTION_REPROTECT_FLIGHT = "REPROTECT";

	public static final String ITINERARY = "ITINERARY";
	
	public static final String ACTION_DETAIL = "DETAIL";

	// Commands
	public static final String COMMAND_REFUND = "REFUND";

	public static final Object COMMAND_CHANGE_PASSWORD = "CHANGEPASSWORD";

	public static final Object COMMAND_GET_PASSWORD = "GETPASSWORD";

	// privileges
	public static final String PRIV_ALERT = "xbe.alert";

	public static final String PRIV_SYS_SECURITY_USER = "sys.security.user";

	public static final String PRIV_SYS_SECURITY_USER_ADD = "sys.security.user.add";

	public static final String PRIV_SYS_SECURITY_USER_EDIT = "sys.security.user.edit";

	public static final String PRIV_SYS_SECURITY_USER_DELETE = "sys.security.user.delete";

	public static final String PRIV_SYS_SECURITY_USER_ADDNOTE = "sys.security.user.addnote";

	public static final String PRIV_SYS_SECURITY_USER_MYIDTRAVEL = "sys.security.user.myidtravel";
	
	public static final String PRIV_SYS_SECURITY_USER_AUDIT = "sys.security.user.audit";
	
	public static final String PRIV_SYS_SECURITY_ROLE_SERVCH_VIEW = "sys.security.role.servicechannel.view";

	public static final String PRIV_SYS_SECURITY_ROLE_SERVCH_EDIT = "sys.security.role.servicechannel.edit";

	public static final String PRIV_TA_MAINT = "ta.maint";

	public static final String PRIV_TA_MAINT_CREDIT = "ta.maint.credit";

	public static final String PRIV_TA_MAINT_HANDLING_FEE = "ta.maint.handlingfee";

	public static final String PRIV_TA_MAINT_DELETE = "ta.maint.delete";
	
	public static final String PRIV_SYS_SECURITY_USER_FFP = "sys.security.user.ffp";
	
	public static final String PRIV_TA_MAINT_SEARCH_ANY = "ta.maint.search.any";


	// Permission to enable editing of Agent status
	public static final String TA_MAINT_STATUS_EDIT = "ta.maint.status.edit";

	public static final String PRIV_TA_MAINT_CREDIT_UPDATE = "ta.maint.credit.update";

	public static final String PRIV_TA_MAINT_ADD_ANY = "ta.maint.add.any";

	public static final String PRIV_TA_MAINT_ADD_GSA = "ta.maint.add.gsa";

	public static final String PRIV_TA_MAINT_ADD_TRAVEL_AGENT = "ta.maint.add.travel.agent";
	
	public static final String PRIV_TA_MAINT_ADD_SUB_AGENTS = "ta.maint.add.sub.agent";

	public static final String PRIV_TA_INVOICE_GENERATE = "ta.invoice.generate";

	public static final String PRIV_TA_INVOICE_TRANSFER = "ta.invoice.transfer";

	public static final String PRIV_TA_INVOICE_EMAIL = "ta.invoice.email";

	public static final String PRIV_TA_PAY_SETTLE_INVOICE = "ta.pay.settle.invoice";

	public static final String PRIV_TA_PAY_SETTLE_PAYMENT = "ta.pay.settle.payment";

	public static final String PRIV_TA_PAY_SETTLE_PAYMENT_ANY = "ta.pay.settle.payment.any";

	public static final String PRIV_TA_PAY_SETTLE_PAYMENT_GSA = "ta.pay.settle.payment.gsa";
	
	public static final String PRIV_TA_PAY_SETTLE_CC_PAYMENT = "ta.pay.settle.cc.payment";

	public static final String PRIV_TA_PAY_REVERSE_CREDIT = "ta.pay.reverse.credit";

	public static final String PRIV_TA_PAY_SETTLE_ANY = "ta.pay.settle.any";

	public static final String PRIV_TA_PAY_SETTLE_GSA = "ta.pay.settle.gsa";
	
	public static final String PRIV_TA_PAY_VIEW_PAYMENT = "ta.pay.view.payment";

	public static final String PRIV_TA_INVOICE_VIEW_ANY = "ta.invoice.any";

	public static final String PRIV_TA_INVOICE_VIEW_GSA = "ta.invoice.gsa";

	public static final String PRIV_TA_MAINT_CREDIT_ADD = "ta.maint.creidt.add";

	public static final String PRIV_TA_MAINT_REL_BLK_AGENT = "ta.maint.gsarlsblk";
	
	public static final String PRIV_TA_MAINT_ASSIGN_VISIBLE_STATIONS = "ta.maint.assign.visible.stations";
	
	public static final String PRIV_TA_MAINT_ASSIGN_VISIBLE_TERRITORIES = "ta.maint.assign.visible.territories";
	
	public static final String PRIV_TA_MAINT_ROUTE_SELECTION = "ta.maint.route.selection";

	public static final String PRIV_NOTIFICATION_SEND = "xbe.notification.send";

	public static final String PRIV_NOTIFICATION_VIEW = "xbe.notification.view";

	public static final String PRIV_NOTIFICATION_EDIT = "xbe.notification.edit";

	public static final String PRIV_CHECKIN_FLIGHT_AGENT = "xbe.checkin.flight.agent";

	public static final String REQ_DAY_OFFSET = "reqDayOffSet";

	public static final String CHECKIN_PASSENGER_DATA = "reqCheckinPassengerData";

	public static final String REQ_MANIFEST_MODE = "hdnMode";
	public static final String REQ_MANIFEST_FLIGHT_NO = "hdnFlightNo";
	public static final String REQ_MANIFEST_FLIGHT_ROUTE = "hdnFltRoute";
	public static final String REQ_MANIFEST_FLIGHT_DATE = "hdnFltDate";
	public static final String REQ_MANIFEST_FLIGHT_ID = "hdnFlightID";
	public static final String REQ_MANIFEST_CONNECTION_MODE = "hdnConnMode";
	public static final String REQ_MANIFEST_VIEW_OPTION = "hdnView";
	public static final String REQ_MANIFEST_SORT_OPTION = "hdnSort";
	public static final String REQ_MANIFEST_COS = "hdnCos";
	public static final String REQ_MANIFEST_COS_TYPE = "hdnCosType";
	public static final String REQ_PASSPORT_STATUS = "hdnPassportStatus";
	public static final String REQ_DOCS_STATUS = "hdnDOCSInfoStatus";
	public static final String REQ_MANIFEST_EMAIL_ID = "hdnEmailID";
	public static final String REQ_MANIFEST_SUMMARY_MODE = "hdnSummaryMode";
	public static final String REQ_MANIFEST_SUMMARY_POSITION = "hdnSummaryPosition";
	public static final String REQ_MANIFEST_INWARD_CONN_LIMIT = "hdnInConnLimit";
	public static final String REQ_MANIFEST_INWARD_CONN_ORIGIN = "hdnInOrigin";
	public static final String REQ_MANIFEST_INWARD_CONN_BOARDING_AIRPORT = "hdnInBoard";
	public static final String REQ_MANIFEST_INWARD_CONN_FLIGHT = "hdnInFlightNo";
	public static final String REQ_MANIFEST_OUTWARD_CONN_LIMIT = "hdnOutConnLimit";
	public static final String REQ_MANIFEST_OUTWARD_CONN_DEST = "hdnOutDest";
	public static final String REQ_MANIFEST_OUTWARD_CONN_BOARDING_AIRPORT = "hdnOutBoard";
	public static final String REQ_MANIFEST_OUTWARD_CONN_FLIGHT = "hdnOutFlightNo";
	public static final String MANIFEST_HTML_RESPONSE = "reqManifestHtml";
	// Parameter name indicating whether the user has privilege to change Agent status.
	public static final String REQ_HAS_STATUS_CHANGE_PRIVILEGE = "hasStatusEditPrivilege";

	// strings needed for reports
	public static final String REP_HDN_LIVE = "hdnLive";

	public static final String REP_SET_LIVE = "reqHdnLive";

	public static final String REP_RPT_START_DAY = "reqStartDay";

	public static final String REP_LIVE = "LIVE";

	public static final String REP_OFFLINE = "OFFLINE";

	public static final String REPORT_HTML = "HTML";

	public static final String REPORT_PDF = "PDF";
	public static final String REPORT_EXCEL = "EXCEL";
	public static final String REPORT_CSV = "CSV";

	public static final String REPORT_REF = "WebReport1";

	public static final String REP_CARRIER_NAME = "CARRIER";

	public static final String REQ_HTML_COS_LIST = "reqCOSList";

	public static final String REQ_HTML_AGENT_CITY_LIST = "reqAgentCityList";

	// Value constants
	public static final String APP_CODE = "xbe";

	public static final String LOCALTIME = "LOCAL";

	public static final String ZULUTIME = "ZULU";

	public static final String HTML_STR_INFANT = "/INF";

	public static final String DASHBOARD_CONTENT = "dashboardContent";

	public static final String DASHBOARD_ENABLED = "dashboardEnabled";

	public static final String TRANS_DEBIT = "DR";

	public static final String TRANS_CREDIT = "CR";

	public static final String VAL_CHECKBOX_ON = "on";

	public static final String VAL_CHECKBOX_TRUE = "true";

	// need to check constants
	public static final String APP_CARRIER_NAME = "appCarrierName";

	public static final String APP_CARRIER_CODE = "appCarrierCode";

	public static final String APP_RELEASE_VERSION = "appAccelAeroRelVersion";

	public static final String APP_RELEASE_VERSION_URL_KEY = "relversion";

	public static final String APP_MAP_NON_ENCRYPTED_URLS = "appMapSecureURLs";

	// User notes Types
	public static final String USER_NOTES_FOR_USER_MAINTENANCE = "USERMAINT";

	public static final String USER_NOTES_FOR_MANUAL_CHECKIN = "CHKNOTE";

	public static final String REQ_HTML_RETURN_VALIDITY = "reqRetValidity";

	public static final String REQ_HTML_BC_TYPE = "reqBCType";

	// payment status
	public static final String DUP_NAMES_IN_FLIGHT_SEGMENT = "error.duplicate.names";

	public static final String ANCI_BLOCKING_FAILED = "error.blocking.failed";

	public static final String NO_OUTBOUND_FOUND = "error.outbound.notfound";

	public static final String REPORT_FORMAT_OPTIONS = "rptFormatOption";

	public static final String REQ_HTML_FLIGHT_TYPES = "reqFlightTypes";

	public static final String REQ_HTML_FARE_DISCOUNT_TYPES = "reqFareDiscountTypes";

	public static final String DUPLICATE_PASSPORT_FOR_SEGMENT = "duplicate.passport";

	public static final String PAX_PASSPORT_EXPIRE = "passport.expire";

	public static final String BOOKING_CLASS_CODES = "reqBookingClassCodes";

	public static final String REQ_HTML_FARERULE_MULTI_LIST = "reqFareRuleMultiList";

	public static final String REQ_HTML_FARE_VISIBILITY_MULTI_LIST = "reqFareVisibilityMultiList";

	public static final String REQ_FLIGHT_STATUS = "reqFlightStatus";

	public static final String REQ_FLIGHT_NO_LIST = "reqFlightNoList";

	public static final String REQ_HTML_BOOKINGCODES_DESC = "reqBookingCodesDesc";

	public static final String REQ_HTML_BOOKING_CLASS_TYPES_LIST = "reqBookingClassTypeList";

	public static final String REQ_HTML_BOOKINGCODES_TYPES_LIST = "reqBCTypeList";

	public static final String REQ_HTML_FARECLASS_SELECT_LIST = "reqFareClassList";

	public static final String REQ_HTML_ALLOCATION_TYPES_LIST = "reqAllocationTypeList";

	public static final String REQ_HTML_BOOKINGCODES_LIST = "reqBookingCodesList";

	public static final String REQ_HTML_PAX_STATUS_LIST = "reqPaxStatusList";

	public static final String DEMO_DISPLAY = "demoDispay";

	public static final String DASHBOARD_TOOLS = "dbToolDispay";
	
	public static final String DASHBOARD_ANCI = "dbAnciDispay";

	public static final String DASHBOARD_MSG_SYNC = "msgSyncEnable";

	public static final String DASHBOARD_MSG_SYNC_TIME = "msgSyncTime";

	public static final String REQ_HTML_BASIS_LIST = "reqBasisList";
	
	public static final String REP_CMP_PAY_STATIONS_ENABLED= "reqStationsFilterForCompanyPayRpt";

	// Checkin PFS details
	public final static String PAX_TITLE = "selTitle";
	public final static String PAX_TYPE = "selPaxType";
	public final static String PAX_FIRST_NAME = "txtFirstName";
	public final static String PAX_LAST_NAME = "txtLastName";
	public final static String PAX_STATUS = "selAddStatus";
	public final static String PAX_DESTINATION = "selDest";
	public final static String PAX_COS = "selCC";
	public final static String PAX_CATEGORY = "selPaxCat";
	public final static String PFS_HDN_MODE = "hdnMode";
	public static final String REQ_PAXCAT_LIST = "reqPaxCatList";
	public static final String REQ_HTML_CABINCLASS_LIST = "reqCabinClassList";
	public static final String REQ_PAX_TITLES = "reqPaxTitles";
	public static final String REQ_PAX_TYPE = "reqPaxType";
	public static final String REQ_SEGMENT_ID = "hdnFlightSegId";
	public static final String REQ_FLIGHT_ID = "hdnFltId";
	public static final String ERROR_SYSTEM_ERROR = "cc_common";
	public static final String REQ_VIEW_ONLY = "reqViewOnly";
	public static final String REQ_PAX_PNR = "hdnPaxPnr";
	public static final String REQ_PAX_PNR_TYPE = "hdnPaxType";
	public static final String XBE_ANALYTIC_JS = "xbeAnalyticJs";
	public static final String REQ_SSR_CODE_WITH_CATEGORY = "reqSsrAndCategoryList";
	public static final String REQ_BL_RULES_DATA_ELEMENTS = "reqBlRulesDataElementList";

	public static final String REQ_HTML_SSR_CODE_LIST = "reqSSRCodeList";

	// view invoice report
	public static final String REQ_VIEW_REPORT_STATUS = "reqViewReportStatus";

	// agent transaction report
	public static final String REQ_AGENT_TRANSACTION_SEARCH_FULL = "priviSearchFullAllowed";

	public static final String TICKET_PRINT_EMAIL_PAX_WISE_FORMAT = "printIndItinerary";

	public static final String REQ_HTML_REPORT_SCHEDULER_APP_PARAM = "reqReportSchedularAppParam";

	// Flight manifest window optins
	public static final String REQ_HTML_SHOW_DOCS_FLITER = "reqShowDOCSFilter";

	public static final String REQ_HTML_SHOW_PSPT_FLITER = "reqShowPSPTFilter";
	
	public static final String REQ_HAS_MANUALLY_SEND_PNL = "reqHasManuallySendPnl";
	
	public static final String REQ_HAS_MANUALLY_SEND_PAL = "reqHasManuallySendPal";

	public static final String REQ_SESSION_KEEP_ALIVE_IN_MINS = "reqSessionKeepAliveInMins";

	public static final String REQ_HUB_COUNTRIES = "reqHubCountries";

	public static final String REQ_SELECTED_LANGUAGE = "reqSelectedLangunage";

	 public static final String REQ_FFP_NUMBER = "reqFFPNumber";
	 
	public static final String REQ_MCO_SERVICES = "reqMCOService";
	//Request Management window options
	public static final String REQ_HTML_QUEUE_PAGES = "reqQueuePages";
	public static final String REQ_HTML_QUEUES = "reqQueues";
	public static final String REQ_HTML_REQUEST_STATUS = "requestStatus";
	public static final String REQ_HTML_QUEUES_FOR_PAGE = "reqQueuesForPage";

	public static final String REQ_HTML_PGW_LIST = "reqPGWType";
	
	//for invoice entity request
	public static final String REQ_ENITITY = "reqEntityList";
	
	public static final String AGENT_CC_TOPUP = "agentCCTopup";
	
	public static final String REQ_HANDLING_FEE_APPLY_OPS = "reqHandlingFeeApplyOperations";
	public static final String REQ_HANDLING_FEE_OP_STATUS = "reqHandlingFeeApplyOpStatus";
	public static final String OTP_GENERATION_SUCCESS = "success.otp.generation";

	// view reporting agent pnr
	public static final String REQ_LOAD_REP_AGENT_PNR_ERROR = "loadReportingAgentReservationError";

	// actual Payment method capture

	public interface actualPayment {
		public static final String PARAM_EXTERNAL_REF = "extRef";
		public static final String PARAM_EXTERNAL_REF_TYPE = "extRefType";
	}

	public interface ReportFormatType {
		public static final String EXCEL_FORMAT = ".xls";
		public static final String PDF_FORMAT = ".pdf";
		public static final String CSV_FORMAT = ".csv";
	}
	
	public static final String REQ_ENTITIES = "reqEntities";

}
