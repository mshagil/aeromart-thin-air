/**
 * 
 */
package com.isa.thinair.xbe.core.exception;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;

/**
 * @author Sudheera
 * 
 */
public class XBERuntimeException extends ModuleRuntimeException {

	/**
     * 
     */
	private static final long serialVersionUID = 5416075585137991746L;

	/**
	 * @param cause
	 * @param exceptionCode
	 * @param moduleCode
	 */
	public XBERuntimeException(Throwable cause, String exceptionCode, String moduleCode) {
		super(cause, exceptionCode, moduleCode);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param exceptionCode
	 */
	public XBERuntimeException(String exceptionCode) {
		super(exceptionCode);
		// TODO Auto-generated constructor stub
	}

}
