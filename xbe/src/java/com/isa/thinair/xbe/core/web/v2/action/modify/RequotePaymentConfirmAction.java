package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCCPaymentMetaInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.utils.AuthorizationsUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.ReservationPaxUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;
import com.isa.thinair.webplatform.core.util.ReservationCommonUtil;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaxCreditFETO;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.api.dto.v2.PnrCreditFETO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBEException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class RequotePaymentConfirmAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(RequotePaymentConfirmAction.class);

	private boolean success = true;

	private String messageTxt;

	private FlightSearchDTO fareQuoteParams;

	private String balanceQueryData;

	private String paxPayments;

	private PaymentTO payment;

	private CreditCardTO creditInfo;

	private String hdnSelCurrency;

	private String agentBalance;

	private boolean actualPayment;

	private BigDecimal totalAmount;

	private GroupBookingRequestTO groupBookingRequest;

	private boolean convertedToGroupReservation;

	private String groupBookingReference;

	private String resContactInfo;

	private String paxNameDetails;

	private boolean isOhdResExpired;

	private String reasonForAllowBlPax;

	private String agentBalanceInAgentCurrency;

	public String execute() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			// XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
			// S2Constants.Session_Data.XBE_SES_RESDATA);
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);
			IPGResponseDTO externalPGWResponseDTO = (IPGResponseDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.EXTERNAL_PAYMENT_GATEWAY_RESPONSE);
			if (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
					&& resInfo.getExistingReleaseTimestamp().before(CalendarUtil.getCurrentZuluDateTime())
					&& AppSysParamsUtil.isCancelExpiredOhdResEnable()) {
				this.success = false;
				this.messageTxt = "Your on hold reservation has expired. Please reload";
				this.isOhdResExpired = true;
				return S2Constants.Result.SUCCESS;
			}

			// String strTxnIdntifier = resInfo.getTransactionId();
			FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, AppIndicatorEnum.APP_XBE);

			RequoteModifyRQ requoteModifyRQ = JSONFETOParser.parse(RequoteModifyRQ.class, balanceQueryData);

			CommonReservationContactInfo contactInfo = ReservationUtil.transformJsonContactInfo(resContactInfo);

			QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO(),
					flightPriceRQ, bookingShoppingCart.getFareDiscount());
			requoteModifyRQ.setFareInfo(fareInfo);
			requoteModifyRQ.setActualPayment(actualPayment);
			requoteModifyRQ.setContactInfo(contactInfo);
			requoteModifyRQ.setReasonForAllowBlPax(reasonForAllowBlPax);

			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = ReservationUtil.getPassengerExtChgMap(bookingShoppingCart);
			addAdminFeeForNonPayment(paxExtChgMap, bookingShoppingCart, resInfo.isInfantPaymentSeparated());
			requoteModifyRQ.setPaxExtChgMap(paxExtChgMap);

			Integer ipgId = null;
			if (payment.getPaymentMethod() == PaymentMethod.CRED || payment.getPaymentMethod() == PaymentMethod.OFFLINE) {
				ipgId = new Integer(request.getParameter(WebConstants.REQ_HDN_IPG_ID));
			}

			if (bookingShoppingCart.getPayByVoucherInfo() != null) {
				requoteModifyRQ.setPayByVoucherInfo(bookingShoppingCart.getPayByVoucherInfo());
			}

			requoteModifyRQ.setReservationStatus(resInfo.getReservationStatus());
			requoteModifyRQ.setLccPassengerPayments(getPassengerPayments(bookingShoppingCart,
					requoteModifyRQ.getReservationStatus(), ipgId, resInfo.isInfantPaymentSeparated(), externalPGWResponseDTO));
			requoteModifyRQ.setExternalChargesMap(ReservationUtil.getExternalChargesMap(bookingShoppingCart));
			// requoteModifyRQ.setOwnerAgentCode(ownerAgentCode);
			requoteModifyRQ.setLastCurrencyCode(hdnSelCurrency);
			if (bookingShoppingCart.getPriceInfoTO() != null) {
				requoteModifyRQ.setLastFareQuoteDate(bookingShoppingCart.getPriceInfoTO().getLastFareQuotedDate());
				requoteModifyRQ.setFQWithinValidity(bookingShoppingCart.getPriceInfoTO().isFQWithinValidity());
				fareInfo.setDiscountedFareDetails(bookingShoppingCart.getFareDiscount());
			}

			TrackInfoDTO trackInfoDTO = getTrackInfo();
			trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
			trackInfoDTO.setPaymentAgent(
					payment != null ? payment.getAgent() : ((UserPrincipal) request.getUserPrincipal()).getAgentCode());
			BookingUtil.populateInsurance(bookingShoppingCart.getInsuranceQuotes(), requoteModifyRQ,
					fareQuoteParams.getAdultCount() + fareQuoteParams.getChildCount(), trackInfoDTO);

			requoteModifyRQ.setLccTemporaryTnxMap(BookingUtil.getTemporyPaymentMap(externalPGWResponseDTO));
			requoteModifyRQ.setRequoteSegmentMap(ReservationBeanUtil.getReQuoteResSegmentMap(fareQuoteParams.getOndList()));

			Map<Integer, Integer> oldFareIdByFltSegIdMap = new HashMap<Integer, Integer>();

			requoteModifyRQ.setOndFareTypeByFareIdMap(
					ReservationBeanUtil.getONDFareTypeByFareIdMap(fareQuoteParams.getOndList(), oldFareIdByFltSegIdMap));

			requoteModifyRQ.setOldFareIdByFltSegIdMap(oldFareIdByFltSegIdMap);
			if (paxNameDetails != null) {
				requoteModifyRQ.setNameChangedPaxMap(ReservationBeanUtil.getNameChangedPaxMap(paxNameDetails));
				boolean skipDuplicateNameCheck = BasicRH.hasPrivilege(request,
						PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_DUPLI_NAME_SKIP);
				boolean dupNameCheck = AppSysParamsUtil.isDuplicateNameCheckEnabled() && !skipDuplicateNameCheck;
				requoteModifyRQ.setDuplicateNameCheck(dupNameCheck);
			}

			@SuppressWarnings("unchecked")
			Map<String, String> mapPrivileges = (Map<String, String>) request.getSession()
					.getAttribute(WebConstants.SES_PRIVILEGE_IDS);

			if (payment.getMode().equals("CONFIRM") || payment.getMode().equals("FORCE")) {

				if (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
						|| resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
					UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
					@SuppressWarnings("unchecked")
					List<FlightSegmentTO> openFltSegments = JSONFETOParser.parseCollection(List.class, FlightSegmentTO.class,
							request.getParameter("openSegments"));

					for (FlightSegmentTO flightSegmentTO : openFltSegments) {
						ReservationBeanUtil.setClassOfServiceInfo(flightSegmentTO, fareQuoteParams);
						ReservationBeanUtil.setFlightStatus(flightSegmentTO, fareQuoteParams.getOndList());
					}

					Date releaseTimestamp = BookingUtil.getReleaseTimestamp(openFltSegments, mapPrivileges,
							userPrincipal.getAgentCode(), bookingShoppingCart.getPriceInfoTO().getFareTypeTO(), null,
							resInfo.getExistingReleaseTimestamp());

					if (releaseTimestamp == null && ((resInfo.getReservationStatus()
							.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD))
							|| (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)
									&& AppSysParamsUtil.isAutoCancellationEnabled()))) {
						throw new XBEException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
					}
					requoteModifyRQ.setReleaseTimeStampZulu(releaseTimestamp);
				}

				requoteModifyRQ
						.setPaymentType(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS);
			} else {
				requoteModifyRQ.setReleaseTimeStampZulu(null);
				requoteModifyRQ.setPaymentType(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT);
			}

			// Add auto cancellation information
			if (AppSysParamsUtil.isAutoCancellationEnabled()
					&& (payment.getMode().equals("CONFIRM") || payment.getMode().equals("FORCE"))
					&& resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
				requoteModifyRQ.setAutoCancellationEnabled(true);
				requoteModifyRQ.setHasBufferTimeAutoCnxPrivilege(AuthorizationsUtil.hasPrivilege(mapPrivileges.keySet(),
						PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALT_RES_AUTO_CNX));
			}
			requoteModifyRQ.setInfantPaymentSeparated(resInfo.isInfantPaymentSeparated());
			if (flightPriceRQ.getAvailPreferences().getSearchSystem().equals(SYSTEM.INT)) {
				requoteModifyRQ.setSystem(SYSTEM.INT);
				if (requoteModifyRQ.getGroupPnr() == null || requoteModifyRQ.getGroupPnr().equals("")) {
					requoteModifyRQ.setGroupPnr(requoteModifyRQ.getPnr());
				}
				groupBookingReference = requoteModifyRQ.getGroupPnr();
				convertedToGroupReservation = true;
				requoteModifyRQ
						.setCarrierWiseFlightRPHMap(ReservationCommonUtil.getCarrierWiseRPHs(fareQuoteParams.getOndList()));
				requoteModifyRQ.setVersion(ReservationCommonUtil.getCorrectInterlineVersion(requoteModifyRQ.getVersion()));
				requoteModifyRQ.setTransactionIdentifier(resInfo.getTransactionId());
				requoteModifyRQ.setFlightRefWiseOndBaggageGroup(bookingShoppingCart.getFlightRefWiseOndBaggageGroup());
			}

			ModuleServiceLocator.getAirproxyReservationBD().requoteModifySegments(requoteModifyRQ, trackInfoDTO);

			// updated agent credit
			agentBalance = CommonUtil.getLoggedInAgentBalance(request);
			agentBalanceInAgentCurrency = CommonUtil.getLoggedInAgentBalanceInAgentCurrency(request);
			boolean isForceConfirmed = payment.getMode().equals("FORCE");

			// send medical ssr email
			SSRServicesUtil.sendMedicalSsrEmail(requoteModifyRQ.getPnr(),
					ReservationPaxUtil.toLccClientReservationPax(bookingShoppingCart.getPaxList()),
					requoteModifyRQ.getPaxExtChgMap(), contactInfo, isForceConfirmed);

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error("Generic exception caught.", e);
		} finally {
			request.getSession().setAttribute(S2Constants.Session_Data.EXTERNAL_PAYMENT_GATEWAY_RESPONSE, null);
		}

		return S2Constants.Result.SUCCESS;
	}

	private void addAdminFeeForNonPayment(Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap,
			BookingShoppingCart bookingShoppingCart, boolean infantPaymentSeparated) {

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

		if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
				&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, userPrincipal.getAgentCode())
				&& (payment.getMode().equals("CONFIRM") || payment.getMode().equals("FORCE"))) {
			ReservationBalanceTO reservationBalance = bookingShoppingCart.getReservationBalance();
			int noOfAdults = 0;
			int noOfInfantWithPayment = 0;

			for (LCCClientPassengerSummaryTO paxSummaryTo : reservationBalance.getPassengerSummaryList()) {
				// BigDecimal paxAmountDue = ReservationBeanUtil.getAmountDueWhenOnlyInfantHasAmountDue(paxSummaryTo,
				// reservationBalance.getPassengerSummaryList(), infantPaymentSeparated);
				BigDecimal paxAmountDue = paxSummaryTo.getTotalAmountDue();
				if (paxAmountDue.compareTo((BigDecimal.ZERO)) > 0) {
					if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {
						noOfAdults++;
					} else if (infantPaymentSeparated && paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {
						noOfInfantWithPayment++;
					}
				}

			}
			ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, true);
			externalChargesMediator.setCalculateJNTaxForCCCharge(true);
			LinkedList perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(noOfAdults,
					noOfInfantWithPayment);

			for (LCCClientPassengerSummaryTO paxSummary : reservationBalance.getPassengerSummaryList()) {
				if (infantPaymentSeparated || !paxSummary.getPaxType().equals(PaxTypeTO.INFANT)) {
					BigDecimal paxAmountDue = paxSummary.getTotalAmountDue();
					if (paxAmountDue.compareTo((BigDecimal.ZERO)) > 0) {

						Integer paxSeq = PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber());
						if (perPaxExternalCharges.size() > 0) {
							List<LCCClientExternalChgDTO> extCharges = splitForFlights(
									BookingUtil.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop()),
									getNewFlightRPHs());
							if (paxExtChgMap.containsKey(paxSeq)) {
								paxExtChgMap.get(paxSeq).addAll(extCharges);
							} else {
								paxExtChgMap.put(paxSeq, extCharges);
							}
						}
					}
				}
			}
		}
	}

	private List<LCCClientExternalChgDTO> splitForFlights(List<LCCClientExternalChgDTO> converToProxyExtCharges,
			List<String> newFlightRPHs) {
		List<LCCClientExternalChgDTO> newCharges = new ArrayList<>();
		for (LCCClientExternalChgDTO chg : converToProxyExtCharges) {
			if (newFlightRPHs.size() == 1) {
				chg.setFlightRefNumber(newFlightRPHs.get(0));
				newCharges.add(chg);
			} else if (newFlightRPHs.size() > 0) {
				BigDecimal[] amounts = AccelAeroCalculator.roundAndSplit(chg.getAmount(), newFlightRPHs.size());
				int i = 0;
				for (String rph : newFlightRPHs) {
					LCCClientExternalChgDTO newChg = chg.clone();
					newChg.setAmount(amounts[i++]);
					newChg.setFlightRefNumber(rph);
					newCharges.add(newChg);
				}
			}
		}
		return newCharges;
	}

	public List<String> getNewFlightRPHs() {
		List<String> newFlights = new ArrayList<>();
		if (fareQuoteParams.getOndList() != null && fareQuoteParams.getOndList().size() > 0) {
			Iterator<ONDSearchDTO> ondSearchItr = fareQuoteParams.getOndList().iterator();
			while (ondSearchItr.hasNext()) {
				ONDSearchDTO ondSearchDTO = ondSearchItr.next();
				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(ondSearchDTO.getStatus())
						&& !ondSearchDTO.isFlownOnd()) {
					newFlights.addAll(ondSearchDTO.getFlightRPHList());
				}

			}

		}
		return newFlights;
	}

	public String groupBookingValidate() {
		try {
			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			long requestID = -1;
			if (getGroupBookingReference() != null && !getGroupBookingReference().equals("")) {
				requestID = Long.parseLong(getGroupBookingReference());
			}
			BookingShoppingCart bookingShopingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			Collection<ReservationPaxTO> pax = bookingShopingCart.getPaxList();
			int adultCount = 0, childCount = 0, infantCount = 0;
			for (ReservationPaxTO resPax : pax) {
				if (resPax.getPaxType().equals("AD")) {
					adultCount++;
				} else if (resPax.getPaxType().equals("CH")) {
					childCount++;
				} else {
					infantCount++;
				}
			}
			String paxCountStr = adultCount + "|" + childCount + "|" + infantCount;
			ArrayList<String> flightList = new ArrayList<String>();
			for (LCCClientReservationSegment reservationSeg : bookingShopingCart.getReservationAlterModesTO()
					.getExistingAllSegs()) {
				flightList.add(reservationSeg.getFlightSegmentRefNumber());
			}

			setGroupBookingRequest(ModuleServiceLocator.getGroupBookingBD().validateModifyResGroupBookingRequest(requestID,
					getTotalAmount(), paxCountStr, flightList, user.getAgentCode()));

			// If the agreed fare is set as a percentage then calculate it accordingly
			if (groupBookingRequest != null && groupBookingRequest.isAgreedFarePercentage()) {
				groupBookingRequest.setMinPaymentRequired(groupBookingRequest.getAgreedFare().divide(new BigDecimal("100"))
						.multiply(groupBookingRequest.getMinPaymentRequired()));
			}

		} catch (Exception e) {
			log.error("Group booking validate error", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	@SuppressWarnings("unchecked")
	private Map<String, LCCClientPaymentAssembler> getPassengerPayments(BookingShoppingCart bookingShoppingCart,
			String reservationStatus, Integer ipgId, boolean infantPaymentSeparated, IPGResponseDTO externalPGWResponseDTO)
			throws ModuleException {

		Map<String, LCCClientPaymentAssembler> paymentMap = new HashMap<String, LCCClientPaymentAssembler>();

		ReservationBalanceTO reservationBalance = bookingShoppingCart.getReservationBalance();
		Collection<ReservationPaxTO> paxList = bookingShoppingCart.getPaxList();

		PaymentMethod paymentMethod = payment.getPaymentMethod();
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

		boolean isBSPPayment = paymentMethod.getCode().equals(PaymentMethod.BSP.getCode());

		Map<String, PaxCreditFETO> paxCreditMap = new HashMap<String, PaxCreditFETO>();
		if (paxPayments != null) {
			List<PaxCreditFETO> paxCreditList = null;
			paxCreditList = JSONFETOParser.parseCollection(ArrayList.class, PaxCreditFETO.class, paxPayments);
			if (paxCreditList == null) {
				paxCreditList = new ArrayList<PaxCreditFETO>();
			}
			for (PaxCreditFETO pax : paxCreditList) {
				paxCreditMap.put(pax.getDisplayTravelerRefNo(), pax);
			}
		}

		int noOfAdults = 0;
		int noOfInfantWithPayment = 0;

		for (LCCClientPassengerSummaryTO paxSummaryTo : reservationBalance.getPassengerSummaryList()) {
			BigDecimal paxAmountDue = paxSummaryTo.getTotalAmountDue();
			if (paxAmountDue.compareTo((BigDecimal.ZERO)) > 0) {
				if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {
					noOfAdults++;
				} else if (infantPaymentSeparated && paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {
					noOfInfantWithPayment++;
				}
			}

		}

		Map<Integer, ReservationPaxTO> resPaxMap = new HashMap<Integer, ReservationPaxTO>();
		for (ReservationPaxTO resPax : paxList) {
			resPaxMap.put(resPax.getSeqNumber(), resPax);
		}

		Map<String, BigDecimal> agentAmountMap = new ConcurrentHashMap<String, BigDecimal>();
		if (BookingUtil.isDoubleAgentPay(payment)) {
			agentAmountMap.put(payment.getAgent(), new BigDecimal(payment.getAgentOneAmount()));
			agentAmountMap.put(payment.getAgentTwo(), new BigDecimal(payment.getAgentTwoAmount()));
		}

		ExternalChargesMediator externalChargesMediator;
		if (new BigDecimal(payment.getAmount()).compareTo(BigDecimal.ZERO) <= 0
				|| (payment.getMode().equals("CONFIRM") || payment.getMode().equals("FORCE"))) {

			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, userPrincipal.getAgentCode())) {
				externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, true);
			} else {
				externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, false);
			}

		} else {
			externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart,
					paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())
							|| (AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
									.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, userPrincipal.getAgentCode())));
			if (isBSPPayment) {
				externalChargesMediator.setBSPPayment(true);
			}
		}
		externalChargesMediator.setCalculateJNTaxForCCCharge(true);

		ReservationUtil.getApplicableServiceTax(bookingShoppingCart, EXTERNAL_CHARGES.JN_OTHER);

		LinkedList perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(noOfAdults,
				noOfInfantWithPayment);

		PayCurrencyDTO cardPayCurrencyDTO = null;
		PayCurrencyDTO onAccPayCurrencyDTO = null;
		PayCurrencyDTO bspPayCurrencyDTO = null;
		PayCurrencyDTO cashPayCurrencyDTO = null;
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;
		Date paymentTimestamp = new Date(); // Timestamp for the payment transaction
		Date selectedCurrencyTimestamp = bookingShoppingCart.getSelectedCurrencyTimestamp();
		if (selectedCurrencyTimestamp == null) {
			selectedCurrencyTimestamp = paymentTimestamp;
		}
		BigDecimal totalPaxCredit = BigDecimal.ZERO;
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		OUTER: for (LCCClientPassengerSummaryTO paxSummary : reservationBalance.getPassengerSummaryList()) {
			if (infantPaymentSeparated || !paxSummary.getPaxType().equals(PaxTypeTO.INFANT)) {
				BigDecimal paxAmountDue = paxSummary.getTotalAmountDue();
				if (paxAmountDue.compareTo((BigDecimal.ZERO)) > 0) {
					BigDecimal amountDueWithExt = AccelAeroCalculator.add(paxSummary.getTotalCreditAmount().negate(),
							paxAmountDue);
					LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();
					ReservationPaxTO resPax = resPaxMap.get(PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber()));

					BigDecimal amountWithoutExt = amountDueWithExt;
					List<LCCClientExternalChgDTO> extCharges = new ArrayList<>();
					if (perPaxExternalCharges.size() > 0) {
						extCharges = BookingUtil
								.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop());
					}
					if (resPax != null) {
						extCharges.addAll(resPax.getExternalCharges());
					}

					if (extCharges.size() > 0) {
						paymentAssembler.getPerPaxExternalCharges().addAll(extCharges);

						List<EXTERNAL_CHARGES> totalServiceTax = new ArrayList<EXTERNAL_CHARGES>();
						totalServiceTax.add(EXTERNAL_CHARGES.SERVICE_TAX);
						BigDecimal extChgTotalServiceTax = BookingUtil.getTotalExtChargeSelected(extCharges, totalServiceTax);
						amountDueWithExt = AccelAeroCalculator.add(amountDueWithExt, extChgTotalServiceTax);

						List<EXTERNAL_CHARGES> notYetAppliedExtCharges = new ArrayList<EXTERNAL_CHARGES>();
						notYetAppliedExtCharges.add(EXTERNAL_CHARGES.BSP_FEE);
						BigDecimal extChgTotal = BookingUtil.getTotalExtChargeExcludingSelected(extCharges,
								notYetAppliedExtCharges);
						amountWithoutExt = AccelAeroCalculator.subtract(amountDueWithExt, extChgTotal);
					}
					boolean creditFlag = false;

					if (!(payment.getMode().equals("CONFIRM") || payment.getMode().equals("FORCE"))) {

						PaxCreditFETO paxCreditFeto = paxCreditMap.get(paxSummary.getTravelerRefNumber());
						if (paxCreditFeto != null) {
							if (paxCreditFeto.hasCreditPayment()) {
								for (PnrCreditFETO pnrCreditFeto : paxCreditFeto.getPaxCreditInfo()) {
									BigDecimal amount = pnrCreditFeto.getUsedAmount();
									amountWithoutExt = AccelAeroCalculator.add(amountWithoutExt, amount.negate());
									amountDueWithExt = AccelAeroCalculator.add(amountDueWithExt, amount.negate());
									creditFlag = true;
								}

							}
						}

						if (bookingShoppingCart.getPayByVoucherInfo() != null) {
							for (VoucherDTO voucherDTO : bookingShoppingCart.getPayByVoucherInfo().getVoucherDTOList()) {
								BigDecimal voucherAmount = new BigDecimal(voucherDTO.getAmountInBase());
								BigDecimal voucherRedeemAmount = new BigDecimal(voucherDTO.getRedeemdAmount());
								BigDecimal remainingVoucherAmount = AccelAeroCalculator.subtract(voucherAmount,
										voucherRedeemAmount);
								BigDecimal totalAmountWithExt = amountWithoutExt;
								if (!paymentAssembler.isExternalChargesConsumed()) {
									totalAmountWithExt = AccelAeroCalculator.add(amountWithoutExt,
											paymentAssembler.calculateTotalExternalChargesAmount());
								}
								if (voucherRedeemAmount.compareTo(voucherAmount) < 0) {
									VoucherDTO voucher = new VoucherDTO();
									voucher.setAmountInBase(voucherDTO.getAmountInBase());
									voucher.setVoucherId(voucherDTO.getVoucherId());
									if (remainingVoucherAmount.compareTo(totalAmountWithExt) >= 0) {
										voucherDTO.setRedeemdAmount(
												AccelAeroCalculator.add(totalAmountWithExt, voucherRedeemAmount).toString());
										voucher.setRedeemdAmount(amountWithoutExt.toString());
										paymentAssembler.addVoucherPayment(voucher, cardPayCurrencyDTO, paymentTimestamp);
										amountWithoutExt = AccelAeroCalculator.getDefaultBigDecimalZero();
										paymentMap.put(paxSummary.getTravelerRefNumber(), paymentAssembler);
										continue OUTER;
									} else {
										voucherDTO.setRedeemdAmount(
												AccelAeroCalculator.add(remainingVoucherAmount, voucherRedeemAmount).toString());
										if (!paymentAssembler.isExternalChargesConsumed()) {
											BigDecimal consumedPayAmount = AccelAeroCalculator.subtract(remainingVoucherAmount,
													paymentAssembler.calculateTotalExternalChargesAmount());
											voucher.setRedeemdAmount(consumedPayAmount.toString());
											amountWithoutExt = AccelAeroCalculator.subtract(amountWithoutExt, consumedPayAmount);
										} else {
											voucher.setRedeemdAmount(remainingVoucherAmount.toString());
											amountWithoutExt = AccelAeroCalculator.subtract(amountWithoutExt,
													remainingVoucherAmount);
										}
										paymentAssembler.addVoucherPayment(voucher, cardPayCurrencyDTO, paymentTimestamp);
									}
								}
							}
						}

						if (PaymentUtil.isValidPaymentExist(creditFlag, amountDueWithExt, reservationStatus)) {
							if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
								if (cashPayCurrencyDTO == null) {
									cashPayCurrencyDTO = PaymentUtil.getPaymentCurrencyForCashPay(hdnSelCurrency,
											exchangeRateProxy);
								}
								paymentAssembler.addCashPayment(amountWithoutExt, cashPayCurrencyDTO, paymentTimestamp,
										payment.getPaymentReference(), payment.getActualPaymentMethod(), null, null);

							} else if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {

								if (onAccPayCurrencyDTO == null) {
									String loggedInAgencyCode = RequestHandlerUtil.getAgentCode(request);
									onAccPayCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(hdnSelCurrency, loggedInAgencyCode,
											payment.getAgent(), exchangeRateProxy);
								}

								// Double Agent Changes -----------------
								if (BookingUtil.isDoubleAgentPay(payment)) {
									if (agentAmountMap.get(payment.getAgent()).compareTo(amountDueWithExt) >= 0) {
										paymentAssembler.addAgentCreditPayment(payment.getAgent(), amountWithoutExt,
												onAccPayCurrencyDTO, exchangeRateProxy.getExecutionDate(),
												payment.getPaymentReference(), payment.getActualPaymentMethod(),
												Agent.PAYMENT_MODE_ONACCOUNT, null);
										agentAmountMap.put(payment.getAgent(), AccelAeroCalculator
												.add(agentAmountMap.get(payment.getAgent()), amountDueWithExt.negate()));
									} else {
										BigDecimal agentTwoAmount = AccelAeroCalculator.add(amountWithoutExt,
												(agentAmountMap.get(payment.getAgent())).negate());
										paymentAssembler.addAgentCreditPayment(payment.getAgentTwo(), agentTwoAmount,
												onAccPayCurrencyDTO, exchangeRateProxy.getExecutionDate(),
												payment.getPaymentReference(), payment.getActualPaymentMethod(),
												Agent.PAYMENT_MODE_ONACCOUNT, null);

										if (agentAmountMap.get(payment.getAgent()).compareTo(BigDecimal.ZERO) > 0) {
											paymentAssembler.addAgentCreditPayment(payment.getAgent(),
													agentAmountMap.get(payment.getAgent()), onAccPayCurrencyDTO,
													exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
													payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_ONACCOUNT, null);
											agentAmountMap.put(payment.getAgent(), BigDecimal.ZERO);
										}
									}

									// -----------------------------------------------------------------------
								} else {

									paymentAssembler.addAgentCreditPayment(payment.getAgent(), amountWithoutExt,
											onAccPayCurrencyDTO, paymentTimestamp, payment.getPaymentReference(),
											payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_ONACCOUNT, null);
								}

							} else if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {

								if (bspPayCurrencyDTO == null) {
									String loggedInAgencyCode = RequestHandlerUtil.getAgentCode(request);
									// Override the selected currency with Agent country currency code for BSP
									String bspCurrency = ModuleServiceLocator.getCommonServiceBD()
											.getCurrencyCodeForStation(RequestHandlerUtil.getAgentStationCode(request));
									bspPayCurrencyDTO = PaymentUtil.getBSPPayCurrencyDTO(bspCurrency, loggedInAgencyCode,
											payment.getAgent(), exchangeRateProxy);
								}
								paymentAssembler.addAgentCreditPayment(payment.getAgent(), amountWithoutExt, bspPayCurrencyDTO,
										paymentTimestamp, payment.getPaymentReference(), payment.getActualPaymentMethod(),
										Agent.PAYMENT_MODE_BSP, null);

							} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {

								if (cardPayCurrencyDTO == null) {
									cardPayCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(hdnSelCurrency, ipgId,
											exchangeRateProxy, false);
									ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
											cardPayCurrencyDTO.getPayCurrencyCode());
								}

								if (externalPGWResponseDTO != null
										&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_EXTERNAL
												.equals(externalPGWResponseDTO.getBrokerType())) {
									LCCClientCCPaymentMetaInfo lccClientCCPaymentMetaInfo = new LCCClientCCPaymentMetaInfo();
									lccClientCCPaymentMetaInfo.setTemporyPaymentId(externalPGWResponseDTO.getTemporyPaymentId());
									lccClientCCPaymentMetaInfo.setPaymentBrokerId(externalPGWResponseDTO.getPaymentBrokerRefNo());
									paymentAssembler.addExternalCardPayment(PaymentType.CARD_GENERIC.getTypeValue(), "",
											amountWithoutExt, AppIndicatorEnum.APP_XBE, TnxModeEnum.SECURE_3D,
											lccClientCCPaymentMetaInfo, ipgIdentificationParamsDTO, cardPayCurrencyDTO,
											new Date(), null, null, false,
											externalPGWResponseDTO.getTemporyPaymentId().toString());
								} else {
									paymentAssembler.addInternalCardPayment(
											new Integer(creditInfo.getCardType().trim()).intValue(),
											creditInfo.getExpiryDateYYMMFormat().trim(), creditInfo.getCardNo().trim(),
											creditInfo.getCardHoldersName().trim(), creditInfo.getAddress(),
											creditInfo.getCardCVV().trim(), amountWithoutExt, AppIndicatorEnum.APP_XBE,
											TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO, cardPayCurrencyDTO,
											paymentTimestamp, creditInfo.getCardHoldersName(), payment.getPaymentReference(),
											payment.getActualPaymentMethod(), null, null);
								}

							} else if (paymentMethod.getCode().equals(PaymentMethod.OFFLINE.getCode())) {
								if (cardPayCurrencyDTO == null) {
									cardPayCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(hdnSelCurrency, ipgId,
											exchangeRateProxy, true);
									ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
											cardPayCurrencyDTO.getPayCurrencyCode());
									ipgIdentificationParamsDTO
											.setFQIPGConfigurationName(ipgId + "_" + cardPayCurrencyDTO.getPayCurrencyCode());
									ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);
								}
								paymentAssembler.addOfflineCardPayment(amountWithoutExt, TnxModeEnum.MAIL_TP_ORDER,
										ipgIdentificationParamsDTO, payment.getPaymentReference(),
										payment.getActualPaymentMethod(), cardPayCurrencyDTO);
							}
						}

						if (paxCreditFeto != null) {
							if (paxCreditFeto.hasCreditPayment()) {
								for (PnrCreditFETO pnrCreditFeto : paxCreditFeto.getPaxCreditInfo()) {
									String carrierCode = pnrCreditFeto.getCarrier();
									Integer debitPaxRefNo = pnrCreditFeto.getPaxCreditId();
									BigDecimal amount = pnrCreditFeto.getUsedAmount();
									BigDecimal residingCarrierAmount = pnrCreditFeto.getResidingCarrierAmount();
									String pnr = pnrCreditFeto.getPnr();
									paymentAssembler.addPaxCreditPayment(carrierCode, debitPaxRefNo, amount, paymentTimestamp,
											residingCarrierAmount, null, null,
											PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber()), null, pnr, null);
									totalPaxCredit = AccelAeroCalculator.add(totalPaxCredit, amount);
								}
							}
						}
					}

					if (PaxTypeTO.ADULT.equals(paxSummary.getPaxType()) && paxSummary.hasInfant() && !infantPaymentSeparated) {
						paymentAssembler.setPaxType(PaxTypeTO.PARENT);
					} else {
						paymentAssembler.setPaxType(paxSummary.getPaxType());
					}
					paymentMap.put(paxSummary.getTravelerRefNumber(), paymentAssembler);
				} else {
					ReservationPaxTO resPax = resPaxMap.get(PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber()));

					List<LCCClientExternalChgDTO> extCharges = new ArrayList<>();
					if (resPax != null) {
						extCharges.addAll(resPax.getExternalCharges());
					}
					if (extCharges != null && extCharges.size() > 0) {
						BigDecimal extChgTotal = BookingUtil.getTotalExtCharge(extCharges);
						BigDecimal amountWithoutExt = AccelAeroCalculator.subtract(AccelAeroCalculator.getDefaultBigDecimalZero(),
								extChgTotal);

						LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();
						paymentAssembler.getPerPaxExternalCharges().addAll(extCharges);

						if (cashPayCurrencyDTO == null) {
							cashPayCurrencyDTO = PaymentUtil.getPaymentCurrencyForCashPay(AppSysParamsUtil.getBaseCurrency(),
									exchangeRateProxy);
						}
						paymentAssembler.addCashPayment(amountWithoutExt, cashPayCurrencyDTO, paymentTimestamp, null, null, null,
								null);

						if (PaxTypeTO.ADULT.equals(paxSummary.getPaxType()) && paxSummary.hasInfant()
								&& !infantPaymentSeparated) {
							paymentAssembler.setPaxType(PaxTypeTO.PARENT);
						} else {
							paymentAssembler.setPaxType(paxSummary.getPaxType());
						}
						paymentMap.put(paxSummary.getTravelerRefNumber(), paymentAssembler);
					}

				}
			}
		}

		return paymentMap;
	}

	private boolean isInfantPaxCredit(String travelerRefNoStr) {
		if (!StringUtil.isNullOrEmpty(travelerRefNoStr)) {
			String travelerRefNoArray[] = travelerRefNoStr.split(",");
			if (travelerRefNoArray != null) {
				for (int i = 0; i < travelerRefNoArray.length; i++) {
					String refNo = travelerRefNoArray[i];
					if (!StringUtil.isNullOrEmpty(refNo)) {
						String travelerRefNo = refNo.split("\\|")[1];
						if (travelerRefNo.startsWith("I")) {
							return true;
						}
					}
				}
			}

		}

		return false;
	}

	public void setHdnSelCurrency(String hdnSelCurrency) {
		this.hdnSelCurrency = hdnSelCurrency;
	}

	public PaymentTO getPayment() {
		return payment;
	}

	public void setPayment(PaymentTO payment) {
		this.payment = payment;
	}

	public CreditCardTO getCreditInfo() {
		return creditInfo;
	}

	public void setCreditInfo(CreditCardTO creditInfo) {
		this.creditInfo = creditInfo;
	}

	public void setPaxPayments(String paxPayments) {
		this.paxPayments = paxPayments;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setBalanceQueryData(String balanceQueryData) {
		this.balanceQueryData = balanceQueryData;
	}

	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public String getAgentBalance() {
		return agentBalance;
	}

	public boolean isActualPayment() {
		return actualPayment;
	}

	public void setActualPayment(boolean actualPayment) {
		this.actualPayment = actualPayment;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public GroupBookingRequestTO getGroupBookingRequest() {
		return groupBookingRequest;
	}

	public void setGroupBookingRequest(GroupBookingRequestTO groupBookingRequest) {
		this.groupBookingRequest = groupBookingRequest;
	}

	public String getGroupBookingReference() {
		return groupBookingReference;
	}

	public void setGroupBookingReference(String groupBookingReference) {
		this.groupBookingReference = groupBookingReference;
	}

	public boolean isConvertedToGroupReservation() {
		return convertedToGroupReservation;
	}

	public String getResContactInfo() {
		return resContactInfo;
	}

	public void setResContactInfo(String resContactInfo) {
		this.resContactInfo = resContactInfo;
	}

	public String getPaxNameDetails() {
		return paxNameDetails;
	}

	public void setPaxNameDetails(String paxNameDetails) {
		this.paxNameDetails = paxNameDetails;
	}

	public boolean isOhdResExpired() {
		return isOhdResExpired;
	}

	public void setOhdResExpired(boolean isOhdResExpired) {
		this.isOhdResExpired = isOhdResExpired;
	}

	public String getReasonForAllowBlPax() {
		return reasonForAllowBlPax;
	}

	public void setReasonForAllowBlPax(String reasonForAllowBlPax) {
		this.reasonForAllowBlPax = reasonForAllowBlPax;
	}

	public String getAgentBalanceInAgentCurrency() {
		return agentBalanceInAgentCurrency;
	}

	public void setAgentBalanceInAgentCurrency(String agentBalanceInAgentCurrency) {
		this.agentBalanceInAgentCurrency = agentBalanceInAgentCurrency;
	}
}
