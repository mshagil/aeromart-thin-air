package com.isa.thinair.xbe.core.web.v2.action.voucher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.generator.voucher.VoucherHTMLGenerator;

/**
 * @author chethiya
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowVoucherDetailAction extends BaseRequestAwareAction {

	Log log = LogFactory.getLog(ShowVoucherDetailAction.class);

	private String errorList;

	private String currencyList;

	private String baseCurrency;

	public String execute() {
		try {
			setErrorList(VoucherHTMLGenerator.getVoucherClientErrors());
			setCurrencyList(VoucherHTMLGenerator.getCurrencyList(request));
			setBaseCurrency(AppSysParamsUtil.getBaseCurrency());
		} catch (Exception e) {
			log.error("ShowVoucherDetailAction ==> execute()", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String getErrorList() {
		return errorList;
	}

	public void setErrorList(String errorList) {
		this.errorList = errorList;
	}

	public String getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(String currencyList) {
		this.currencyList = currencyList;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

}
