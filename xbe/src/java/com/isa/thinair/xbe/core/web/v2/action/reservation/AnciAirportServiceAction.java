package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.FltSegBuilder;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Action class for Airport Services
 * 
 * @author rumesh
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AnciAirportServiceAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(AnciAirportServiceAction.class);

	private boolean success = true;

	private String messageTxt;

	private FlightSearchDTO searchParams;

	private String selectedFlightList;

	private boolean modifyAncillary;

	private boolean modifySegment;

	private String resPaxInfo;

	private String modifyingSegments;

	private String resSegments;

	private List<LCCFlightSegmentAirportServiceDTO> airportServicesList;

	private boolean isRequote = false;

	private String jsonOnds;

	private String pnr;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			String strTxnIdntifier = null;
			SYSTEM system = null;
			List<FlightSegmentTO> selflightSegmentTOs = null;
			List<BundledFareDTO> bundledFareDTOs = null;

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			if (modifyAncillary) {
				FltSegBuilder fltSegBuilder = new FltSegBuilder(selectedFlightList);
				system = fltSegBuilder.getSystem();
				selflightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
				WebplatformUtil.updateOndSequence(jsonOnds, selflightSegmentTOs);
				bundledFareDTOs = resInfo.getBundledFareDTOs();
			} else {
				strTxnIdntifier = resInfo.getTransactionId();
				system = resInfo.getSelectedSystem();
				if (isRequote) {
					selflightSegmentTOs = ReservationUtil.populateFlightSegmentList(selectedFlightList, searchParams);
				} else {
					selflightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
				}

				BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
						S2Constants.Session_Data.LCC_SES_BOOKING_CART);
				bundledFareDTOs = bookingShoppingCart.getPriceInfoTO().getFareTypeTO().getOndBundledFareDTOs();
			}

			List<FlightSegmentTO> flightSegmentTOs = mergeFlightSegmentList(selflightSegmentTOs);

			if (modifySegment) {
				SegmentUtil.setFltSegSequence(flightSegmentTOs, null);
			} else {
				SortUtil.sortFlightSegByDepDate(flightSegmentTOs);
			}

			Map<String, Integer> serviceCount = null;

			if (resPaxInfo != null && !resPaxInfo.equals("")) {
				Collection<LCCClientReservationPax> colpaxs = ReservationUtil.transformJsonPassengers(resPaxInfo);
				serviceCount = SSRServicesUtil.extractAiportServiceCountFromJsonObj(colpaxs);
			} else {
				serviceCount = new HashMap<String, Integer>();
			}

			Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> aiportWiseAvailability = ModuleServiceLocator
					.getAirproxyAncillaryBD().getAvailableAiportServices(flightSegmentTOs, AppIndicatorEnum.APP_XBE,
							SSRCategory.ID_AIRPORT_SERVICE, system, Locale.ENGLISH.getLanguage(), strTxnIdntifier,
							getTrackInfo(), serviceCount, modifyAncillary, bundledFareDTOs, pnr);
			List<LCCFlightSegmentAirportServiceDTO> apsList = populateLCCAiportServicesList(aiportWiseAvailability);

			filterServicesForSelectedSegments(apsList, selflightSegmentTOs);
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error("Aiport Service Request Error", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	private void filterServicesForSelectedSegments(List<LCCFlightSegmentAirportServiceDTO> apsList,
			List<FlightSegmentTO> selflightSegmentTOs) {
		airportServicesList = new ArrayList<LCCFlightSegmentAirportServiceDTO>();

		if (apsList != null) {
			for (LCCFlightSegmentAirportServiceDTO lccFlightSegmentAirportServiceDTO : apsList) {
				for (FlightSegmentTO flightSegmentTO : selflightSegmentTOs) {
					String[] fltRefArr = flightSegmentTO.getFlightRefNumber().split("-");
					String[] apFltRefArr = lccFlightSegmentAirportServiceDTO.getFlightSegmentTO().getFlightRefNumber().split("-");
					if (fltRefArr != null && fltRefArr.length > 0 && apFltRefArr != null && apFltRefArr.length > 0
							&& fltRefArr[0].equals(apFltRefArr[0])) {
						airportServicesList.add(lccFlightSegmentAirportServiceDTO);
					}
				}
			}
		}
	}

	private List<FlightSegmentTO> mergeFlightSegmentList(List<FlightSegmentTO> selectedFltSegList) throws Exception {
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		flightSegmentTOs.addAll(selectedFltSegList);

		Collection<LCCClientReservationSegment> colAllsegs = null;
		if (resSegments != null && !"".equals(resSegments)) {
			colAllsegs = ModifyReservationJSONUtil.transformJsonSegments(resSegments);
		}

		if (colAllsegs != null) {
			colAllsegs = removeCancelledSegments(colAllsegs);
		}

		Collection<LCCClientReservationSegment> colModSegs = null;
		if (modifySegment && modifyingSegments != null && !"".equals(modifyingSegments)) {
			ReservationProcessParams rParm = new ReservationProcessParams(request, null, null, true, false, null, null, false, false);
			colModSegs = ReservationUtil.getModifyingSegments(colAllsegs, modifyingSegments, rParm, true);
		}

		if (colAllsegs != null) {
			// if modify segment, then remove modifying segments from all segment list
			if (colModSegs != null) {
				colAllsegs.removeAll(colModSegs);
			}

			flightSegmentTOs.addAll(ReservationBeanUtil.convertReservationSegmentsToFlightSegmentTOs(colAllsegs));
		}

		return flightSegmentTOs;
	}

	private List<LCCFlightSegmentAirportServiceDTO> populateLCCAiportServicesList(
			Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> aiportWiseAvailability) {
		List<LCCFlightSegmentAirportServiceDTO> lccAvailabilityList = new ArrayList<LCCFlightSegmentAirportServiceDTO>();

		for (Entry<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> entry : aiportWiseAvailability.entrySet()) {
			AirportServiceKeyTO keyTO = entry.getKey();

			LCCFlightSegmentAirportServiceDTO airportServiceDTO = entry.getValue();
			airportServiceDTO.setFlightSegmentTO(airportServiceDTO.getFlightSegmentTO().clone());
			airportServiceDTO.getFlightSegmentTO().setAirportCode(keyTO.getAirport());
			airportServiceDTO.getFlightSegmentTO().setAirportType(keyTO.getAirportType());
			lccAvailabilityList.add(airportServiceDTO);
		}

		SortUtil.sortAirportServicesByFltDeparture(lccAvailabilityList);

		return lccAvailabilityList;
	}

	private List<LCCClientReservationSegment> removeCancelledSegments(Collection<LCCClientReservationSegment> colAllsegs) {
		List<LCCClientReservationSegment> segs = new ArrayList<LCCClientReservationSegment>();
		for (LCCClientReservationSegment s : colAllsegs) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(s.getStatus())) {
				segs.add(s);
			}
		}
		return segs;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	@JSON(serialize = false)
	public String getSelectedFlightList() {
		return selectedFlightList;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	@JSON(serialize = false)
	public boolean isModifyAncillary() {
		return modifyAncillary;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	public List<LCCFlightSegmentAirportServiceDTO> getAirportServicesList() {
		return airportServicesList;
	}

	public void setAirportServicesList(List<LCCFlightSegmentAirportServiceDTO> airportServicesList) {
		this.airportServicesList = airportServicesList;
	}

	@JSON(serialize = false)
	public String getResPaxInfo() {
		return resPaxInfo;
	}

	public void setResPaxInfo(String resPaxInfo) {
		this.resPaxInfo = resPaxInfo;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public void setModifyingSegments(String modifyingSegments) {
		this.modifyingSegments = modifyingSegments;
	}

	public void setResSegments(String resSegments) {
		this.resSegments = resSegments;
	}

	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}

	public void setJsonOnds(String jsonOnds) {
		this.jsonOnds = jsonOnds;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
