package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO.RouteTypes;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.HandlingFeeCalculationUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingTO;
import com.isa.thinair.xbe.api.dto.v2.ConfirmUpdateOverrideChargesTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the Load Profile
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class CancelReservationAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(CancelReservationAction.class);

	private boolean success = true;

	private String messageTxt;

	private BookingTO bookingInfo;

	private String groupPNR;

	private String adultCnxCharge;

	private String childCnxCharge;

	private String infantCnxCharge;

	private String routeType;

	private String userNote;

	private ConfirmUpdateOverrideChargesTO adultOverideCharges;

	private ConfirmUpdateOverrideChargesTO childOverideCharges;

	private ConfirmUpdateOverrideChargesTO infantOverideCharges;

	private String voidReservation;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		try {

			String pnr = bookingInfo.getPNR();
			if (!isGroupPNR) {
				groupPNR = pnr;
			}
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));

			ReservationProcessParams rparam = new ReservationProcessParams(request, resInfo != null
					? resInfo.getReservationStatus()
					: null, colsegs, resInfo.isModifiableReservation(), resInfo.isGdsReservationModAllowed(),
					resInfo.getLccPromotionInfoTO(), null, false, false);
			if (isGroupPNR) {
				rparam.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo.getReservationStatus(), colsegs, resInfo.isModifiableReservation(),
						resInfo.getLccPromotionInfoTO());
			}
			if (rparam.isCancelReservation()) {
				String version = bookingInfo.getVersion();
				userNote = request.getParameter("userNote");
				
				LCCClientResAlterModesTO lccClientResAlterModesTO = LCCClientResAlterModesTO.composeCancelReservationRequest(
						groupPNR, null, null, null, version, userNote);
				lccClientResAlterModesTO.setGroupPnr(isGroupPNR);

				// this.adultOverideCharges = tmpAdultOverideCharges;

				if (this.adultOverideCharges != null) {
					lccClientResAlterModesTO.setAdultCustomChargeTO(this.createPNRDetailTO(adultOverideCharges));
				}
				if (this.infantOverideCharges != null) {
					lccClientResAlterModesTO.setInfantCustomChargeTO(this.createPNRDetailTO(infantOverideCharges));
				}
				if (this.childOverideCharges != null) {
					lccClientResAlterModesTO.setChildCustomChargeTO(this.createPNRDetailTO(childOverideCharges));
				}

				// Default cnx charge state no route value will be set
				if (this.routeType != null && !this.routeType.equals("")) {
					BigDecimal customAdultCharge = null;
					BigDecimal customChildCharge = null;
					BigDecimal customInfantCharge = null;
					if (adultCnxCharge != null && !adultCnxCharge.equals("") && !adultCnxCharge.equals("undefined")) {
						customAdultCharge = new BigDecimal(adultCnxCharge);
						if (this.adultOverideCharges == null
								|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
										this.adultOverideCharges)) {
							lccClientResAlterModesTO.setSendingAbosoluteCharge(true);
						} // if not modifying using percentage
					}

					if (childCnxCharge != null && !childCnxCharge.equals("") && !childCnxCharge.equals("undefined")) {
						customChildCharge = new BigDecimal(childCnxCharge);
						if (this.childOverideCharges == null
								|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
										this.childOverideCharges)) {
							lccClientResAlterModesTO.setSendingAbosoluteCharge(true);
						}
					}

					if (infantCnxCharge != null && !infantCnxCharge.equals("") && !infantCnxCharge.equals("undefined")) {
						customInfantCharge = new BigDecimal(infantCnxCharge);
						if (this.infantOverideCharges == null
								|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
										this.infantOverideCharges)) {
							lccClientResAlterModesTO.setSendingAbosoluteCharge(true);
						}
					}

					lccClientResAlterModesTO.setCustomAdultCharge(customAdultCharge);
					lccClientResAlterModesTO.setCustomChildCharge(customChildCharge);
					lccClientResAlterModesTO.setCustomInfantCharge(customInfantCharge);
				}

							
				LCCClientSegmentAssembler lccClientSegmentAssembler = new LCCClientSegmentAssembler();
				lccClientSegmentAssembler.setPassengerExtChargeMap(HandlingFeeCalculationUtil.composeExternalChargesByPaxSequence(
						pnr, colsegs, null, CommonsConstants.ModifyOperation.CANCEL_PNR.getOperation()));
				lccClientResAlterModesTO.setLccClientSegmentAssembler(lccClientSegmentAssembler);	
				
				// when it comes to actually CNX the reservation we can allways assume the rout type as total as the
				// value
				// in the override box will represent the total override charge even if the initial criteria is a
				// per_ond
				lccClientResAlterModesTO.setRouteType(RouteTypes.TOTAL_ROUTE.getRouteTypes());

				ModuleServiceLocator.getAirproxyReservationBD().cancelReservation(lccClientResAlterModesTO, getClientInfoDTO(),
						getTrackInfo(), new Boolean(this.voidReservation), false);

			} else {
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private PnrChargeDetailTO createPNRDetailTO(ConfirmUpdateOverrideChargesTO chargesTO) {
		PnrChargeDetailTO chargeDetailTO = new PnrChargeDetailTO();
		chargeDetailTO.setCancellationChargeType(chargesTO.getChargeType());
		if (PlatformUtiltiies.isNotEmptyString(chargesTO.getMin())) {
			chargeDetailTO.setMinCancellationAmount(new BigDecimal(chargesTO.getMin()));
		}
		if (PlatformUtiltiies.isNotEmptyString(chargesTO.getMax())) {
			chargeDetailTO.setMaximumCancellationAmount(new BigDecimal(chargesTO.getMax()));
		}

		if (PlatformUtiltiies.isNotEmptyString(chargesTO.getPrecentageVal())) {
			chargeDetailTO.setChargePercentage(new BigDecimal(chargesTO.getPrecentageVal()));
		}
		return chargeDetailTO;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @return the bookingInfo
	 */
	public BookingTO getBookingInfo() {
		return bookingInfo;
	}

	/**
	 * @param bookingInfo
	 *            the bookingInfo to set
	 */
	public void setBookingInfo(BookingTO bookingInfo) {
		this.bookingInfo = bookingInfo;
	}

	public void setAdultCnxCharge(String adultCnxCharge) {
		this.adultCnxCharge = adultCnxCharge;
	}

	public void setChildCnxCharge(String childCnxCharge) {
		this.childCnxCharge = childCnxCharge;
	}

	public void setInfantCnxCharge(String infantCnxCharge) {
		this.infantCnxCharge = infantCnxCharge;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public ConfirmUpdateOverrideChargesTO getAdultOverideCharges() {
		return adultOverideCharges;
	}

	public void setAdultOverideCharges(ConfirmUpdateOverrideChargesTO adultOverideCharges) {
		this.adultOverideCharges = adultOverideCharges;
	}

	public ConfirmUpdateOverrideChargesTO getChildOverideCharges() {
		return childOverideCharges;
	}

	public void setChildOverideCharges(ConfirmUpdateOverrideChargesTO childOverideCharges) {
		this.childOverideCharges = childOverideCharges;
	}

	public ConfirmUpdateOverrideChargesTO getInfantOverideCharges() {
		return infantOverideCharges;
	}

	public void setInfantOverideCharges(ConfirmUpdateOverrideChargesTO infantOverideCharges) {
		this.infantOverideCharges = infantOverideCharges;
	}

	public String getRouteType() {
		return routeType;
	}

	public void setRouteType(String routeType) {
		this.routeType = routeType;
	}

	public String getAdultCnxCharge() {
		return adultCnxCharge;
	}

	public String getChildCnxCharge() {
		return childCnxCharge;
	}

	public String getInfantCnxCharge() {
		return infantCnxCharge;
	}

	public String getVoidReservation() {
		return voidReservation;
	}

	public void setVoidReservation(String voidReservation) {
		this.voidReservation = voidReservation;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = super.getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		trackInfoDTO.setPaymentAgent(((UserPrincipal) request.getUserPrincipal()).getAgentCode());
		return trackInfoDTO;
	}
}