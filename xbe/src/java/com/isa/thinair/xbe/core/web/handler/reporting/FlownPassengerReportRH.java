package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.constants.WebConstants.ReportFormatType;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class FlownPassengerReportRH extends BasicRH {
	private static Log log = LogFactory.getLog(FlownPassengerReportRH.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;

		
		
		try {
			setDisplayData(request);
		} catch (Exception e) {
			log.error("FlownPassengerListReportRH SETDISPLAYDATA() FAILED " + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				return null;
			} else {
				log.error("FlownPassengerReportRH setReportView not selected");
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		}
		return forward;

	}

	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}
	
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		String id = "UC_REPM_099";
		String reportTemplate = "";

		String viewMode = request.getParameter("hdnMode");
		if (("VIEW").equals(viewMode)) {
			reportTemplate = "FlownPassengerReport.jasper";
		}

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");

		//String loggedUserAgentCode = RequestHandlerUtil.getAgentCode(request);

		String reportType = request.getParameter("radReportOption");
		String reportNumFormat = request.getParameter("radRptNumFormat");
		String flightNumber = request.getParameter("txtFlightNumber");

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria searchCriteria = new ReportsSearchCriteria();
			Map<String, Object> parameters = new HashMap<String, Object>();
			if (isNotEmptyOrNull(toDate)) {
				searchCriteria.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}
			if (isNotEmptyOrNull(toDate)) {
				searchCriteria.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + "  23:59:59");
			}

			if (isNotEmptyOrNull(flightNumber)) {
				searchCriteria.setFlightNumber(flightNumber);
			}

			searchCriteria.setInterlineCode(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE));

			ResultSet resultSet = null;

			resultSet = ModuleServiceLocator.getDataExtractionBD().getFlownPassengerData(searchCriteria);

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			
			// To provide Report Format Options
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			String strLogo = AppSysParamsUtil.getReportLogo(false);
			String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true);
			String reportNameStr = "FlownPassengerReport";
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			if (reportType.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
						reportsRootDir, response);

			} else if (reportType.trim().equals("PDF")) {
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

			} else if (reportType.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportNameStr));
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);

			} else if (reportType.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=FlownPassengerReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}

		} catch (Exception e) {
			log.error("setReportView() method is failed :" + e.getMessage());

		}

	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}

}
