package com.isa.thinair.xbe.core.web.v2.action.voucher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ReprotectedPaxDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.promotion.api.to.ReprotectedPaxSearchrequest;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.generator.voucher.VoucherHTMLGenerator;

/**
 * @author chanaka
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowReprotectedPaxDetailAction extends BaseRequestAwareAction{
	private static Log log = LogFactory.getLog(ShowReprotectedPaxDetailAction.class);
	
	private static final int PAGE_LENGTH = 20;

	private int page;
	
	private String flightNum;

	private int records;

	private int total;
	
	private ArrayList<Map<String, ReprotectedPaxDTO>> reProtectedPaxList;
	
	private ArrayList<ReprotectedPaxDTO> selectedPaxList;
	
	private String amount;
	
	private String validFrom;
	
	private String validTo;
	
	private String remarks;
	
	private ReprotectedPaxSearchrequest reprotectedPaxSearchrequest;

	private String errorList;
	
	private String issueType;
	
	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		
		if (AppSysParamsUtil.isVoucherEnabled()){
			VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
			reprotectedPaxSearchrequest.setFlightnumber(flightNum);
			
			try {
				int start = (page - 1) * 20;
				Page<ReprotectedPaxDTO> rePaxPage = voucherDelegate.searchReprotectedPax(start, PAGE_LENGTH, reprotectedPaxSearchrequest);
				setReProtectedPaxList(getReprotectedList((List<ReprotectedPaxDTO>) rePaxPage.getPageData()));
				this.setRecords(rePaxPage.getTotalNoOfRecords());
				this.total = rePaxPage.getTotalNoOfRecordsInSystem() / 20;
				int mod = rePaxPage.getTotalNoOfRecordsInSystem() % 20;
				if (mod > 0) {
					this.total = this.total + 1;
				}
			} catch (ModuleException e) {
				log.error("ShowReprotectedPaxDetailAction ==> ", e);
				forward = S2Constants.Result.ERROR;
			}
		}

		return forward;
	}
	
	public String issueVoucher() {

		String forward = S2Constants.Result.SUCCESS;
		if (AppSysParamsUtil.isVoucherEnabled()){
			UserPrincipal userPrincipal = (UserPrincipal) this.request.getUserPrincipal();		
			try {
				VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
				ArrayList<VoucherDTO> voucherDTOs = getVoucherDTOs(selectedPaxList, amount, validFrom, validTo, remarks,
						userPrincipal);
				voucherDelegate.issueVouchersForReprotectedPax(selectedPaxList, voucherDTOs, issueType);

			} catch (Exception me) {
				log.error("ShowReprotectedPaxDetailAction ==> execute() issue voucher failed", me);
				forward = S2Constants.Result.ERROR;
			}
			
		}
		return forward;	
	}
	
	public String returnErrorList() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			setErrorList(VoucherHTMLGenerator.getRePaxErrors());
		} catch (ModuleException e) {
			log.error("ShowReprotectedPaxDetailAction ==> execute()", e);
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}
	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the reProtectedPaxList
	 */
	public ArrayList<Map<String, ReprotectedPaxDTO>> getReProtectedPaxList() {
		return reProtectedPaxList;
	}

	/**
	 * @param reProtectedPaxList the reProtectedPaxList to set
	 */
	public void setReProtectedPaxList(ArrayList<Map<String, ReprotectedPaxDTO>> reProtectedPaxList) {
		this.reProtectedPaxList = reProtectedPaxList;
	}

	/**
	 * @return the pageLength
	 */
	public static int getPageLength() {
		return PAGE_LENGTH;
	}	

	/**
	 * @return the flightNum
	 */
	public String getFlightNum() {
		return flightNum;
	}
	/**
	 * @param flightNum the flightNum to set
	 */
	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}
	/**
	 * @return the reprotectedPaxSearchrequest
	 */
	public ReprotectedPaxSearchrequest getReprotectedPaxSearchrequest() {
		return reprotectedPaxSearchrequest;
	}
	/**
	 * @param reprotectedPaxSearchrequest the reprotectedPaxSearchrequest to set
	 */
	public void setReprotectedPaxSearchrequest(ReprotectedPaxSearchrequest reprotectedPaxSearchrequest) {
		this.reprotectedPaxSearchrequest = reprotectedPaxSearchrequest;
	}	
	
	/**
	 * @return the selectedPaxList
	 */
	public ArrayList<ReprotectedPaxDTO> getSelectedPaxList() {
		return selectedPaxList;
	}

	/**
	 * @param selectedPaxList the selectedPaxList to set
	 */
	public void setSelectedPaxList(ArrayList<ReprotectedPaxDTO> selectedPaxList) {
		this.selectedPaxList = selectedPaxList;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the validFrom
	 */
	public String getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}


	/**
	 * @return the errorList
	 */
	public String getErrorList() {
		return errorList;
	}

	/**
	 * @param errorList the errorList to set
	 */
	public void setErrorList(String errorList) {
		this.errorList = errorList;
	}
	
	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	/**
	 * @return the issueType
	 */
	public String getIssueType() {
		return issueType;
	}

	/**
	 * @param issueType the issueType to set
	 */
	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	/**
	 * @param rePaxList
	 * @return
	 */
	private static ArrayList<Map<String, ReprotectedPaxDTO>> getReprotectedList(List<ReprotectedPaxDTO> rePaxList) {

		ArrayList<Map<String, ReprotectedPaxDTO>> peprotectedPaxList = new ArrayList<Map<String, ReprotectedPaxDTO>>();
		ReprotectedPaxDTO rePaxDTO = null;
		if (rePaxList.size() > 0) {
			Collections.sort(rePaxList, new Comparator<ReprotectedPaxDTO>() {
				@Override
				public int compare(final ReprotectedPaxDTO object1, final ReprotectedPaxDTO object2) {
					return object1.getReprotectedPaxid().compareTo(object2.getReprotectedPaxid());
				}
			});
		}
		Iterator<ReprotectedPaxDTO> iterator = rePaxList.iterator();
		while (iterator.hasNext()) {
			rePaxDTO = iterator.next();
			Map<String, ReprotectedPaxDTO> row = new HashMap<String, ReprotectedPaxDTO>();
			row.put("rePaxDTO", rePaxDTO);
			peprotectedPaxList.add(row);
		}

		return peprotectedPaxList;
	}

	private ArrayList<VoucherDTO> getVoucherDTOs(ArrayList<ReprotectedPaxDTO> paxList, String amount, String validFrom,
			String validTo, String remarks,UserPrincipal userPrincipal) {
		ArrayList<VoucherDTO> voucherDTOList = new ArrayList<VoucherDTO>();
		for (ReprotectedPaxDTO rePax : paxList) {
			VoucherDTO voucherDTO = new VoucherDTO();
			voucherDTO.setValidFrom(validFrom);
			voucherDTO.setValidTo(validTo);
			voucherDTO.setRemarks(remarks);
			voucherDTO.setIssuedUserId(userPrincipal.getUserId());
			voucherDTO.setPaxFirstName(rePax.getPaxFirstName());
			voucherDTO.setPaxLastName(rePax.getPaxlastName());
			voucherDTO.setEmail(rePax.getPaxEmail());
			voucherDTO.setMobileNumber(rePax.getMobileNumber());
			voucherDTO.setReprotectedPaxid(rePax.getReprotectedPaxid());
			voucherDTO.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
			if (issueType.equals(AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes())) {
				voucherDTO.setAmountInBase(amount);
				voucherDTO.setAmountInLocal(amount);
			} else if (issueType.equals(AirPricingCustomConstants.ChargeTypes.PERCENTAGE_OF_FARE_PF.getChargeTypes())) {
				voucherDTO.setPercentage(amount);
			}			
			voucherDTOList.add(voucherDTO);
		}
		return voucherDTOList;

	}
	
}
