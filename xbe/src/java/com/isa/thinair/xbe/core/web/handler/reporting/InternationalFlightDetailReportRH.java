package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class InternationalFlightDetailReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(InternationalFlightDetailReportRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	/**
	 * @param request
	 * @param response
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;
		setAttribInRequest(request, "displayAgencyMode", "-1");
		setAttribInRequest(request, "currentAgentCode", "");

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("InternationalFlightDetailReportRH setReportView Success");
				return null;
			} else {
				log.error("InternationalFlightDetailReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("HandlingFeeReportRH setReportView Failed " + e.getMessageString(), e);
		}

		return forward;
	}

	/**
	 * @param request
	 * @param response
	 * @throws ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		String id = "UC_REPM_094";
		String reportTemplate = "InternationalFlightDetailsReport.jasper";

		String fromDate = request.getParameter("txtFromDate");
		String originStation = request.getParameter("selOriginStation");
		String departureStation = request.getParameter("selDestinationStation");
		String agents;
		if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			agents = (String) getAttribInRequest(request, "currentAgentCode");
		} else {
			agents = request.getParameter("hdnAgents");
		}

		String reportType = request.getParameter("radReportOption");
		String reportNumFormat = request.getParameter("radRptNumFormat");
		String internationalFlightType = request.getParameter("flightType");

		ArrayList<String> selectedAgents = new ArrayList<String>();
		if (!agents.trim().isEmpty()) {
			String agentList[] = agents.split(",");
			for (String agent : agentList) {
				selectedAgents.add(agent);
			}
		}

		try {
			ReportsSearchCriteria searchCriteria = new ReportsSearchCriteria();
			Map<String, Object> parameters = new HashMap<String, Object>();

			if (fromDate != null && !fromDate.equals("")) {
				searchCriteria.setDateRangeFrom(fromDate);
			}
			/*
			 * if (toDate != null && !toDate.equals("")) {
			 * searchCriteria.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + "  23:59:59"); }
			 */

			searchCriteria.setOriginAirport(originStation);
			searchCriteria.setDestinationAirport(departureStation);
			searchCriteria.setAgents(selectedAgents);
			searchCriteria.setJourneyType(internationalFlightType);
			searchCriteria.setReportOption("DETAIL");

			ResultSet resultSet = null;
			ResultSet sumerryResultSet = null;

			resultSet = ModuleServiceLocator.getDataExtractionBD().getPassengerInternationalFlightDetailsData(searchCriteria);

			searchCriteria.setReportOption("SUMMARY");
			sumerryResultSet = ModuleServiceLocator.getDataExtractionBD()
					.getPassengerInternationalFlightDetailsData(searchCriteria);
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			String summary = getSummary(sumerryResultSet);

			parameters.put("SUMMARY", summary);
			parameters.put("FROM_DATE", fromDate);
			parameters.put("ID", id);
			parameters.put("INT_FLIGHT_TYPE", internationalFlightType.equals("ARRIVAL") ? "ARRIVAL" : "DEPARTURE");
			parameters.put("FROM", originStation);
			parameters.put("TO", departureStation);
			// To provide Report Format Options
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			String strLogo = AppSysParamsUtil.getReportLogo(true);
			String reportsRootDir = "../images/" + strLogo;
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			if (reportType.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
						reportsRootDir, response);

			} else if (reportType.trim().equals("PDF")) {
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=InternationalFlightDetailReport.pdf");
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

			} else if (reportType.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=InternationalFlightDetailReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);

			} else if (reportType.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=InternationalFlightDetailReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}

		} catch (Exception e) {
			log.error("setReportView() method is failed :" + e.getMessage(), e);

		}

	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setDisplayAgencyMode(request);
		setStationComboList(request);
		setJourneyTypeComboList(request);
		setAgentTypes(request);
		setClientErrors(request);
		setGSAMultiSelectList(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setStationComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createStationList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	private static void setJourneyTypeComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createJourneyTypeList();
		request.setAttribute(WebConstants.REQ_JOURNEY_TYPE_LIST, strStation);
	}

	private static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	private static void setDisplayAgencyMode(HttpServletRequest request) throws ModuleException {
		String strCrrAgentType = AppSysParamsUtil.getCarrierAgent();
		String userId = request.getUserPrincipal().getName();
		Agent currentAgent = null;

		if (userId != null && !userId.equals("")) {
			currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
					ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
		}

		if (currentAgent != null && currentAgent.getAgentTypeCode().equals(strCrrAgentType)) {
			setAttribInRequest(request, "displayAgencyMode", "1");
		} else if (currentAgent != null && currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
			setAttribInRequest(request, "displayAgencyMode", "2");
		} else {
			setAttribInRequest(request, "displayAgencyMode", "0");
		}
	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";
		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			blnWithTAs = true;
		}
		if (getAttribInRequest(request, "displayAgencyMode").equals("-1")) {
			// No privilege to access the reports
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			// No Agent Population Controls or Agents list box are displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("1")) {
			// Both Agent Population Controls and Agents list box are displayed
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			// Only Agents list box is displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
				if (currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					// Agent is a GSA
					strList = ReportsHTMLGenerator.createTAsOfGSAMultiSelect(currentAgent.getAgentCode(), true);
				} else {
					// Agent is not a GSA
					// Only select the current agent
					setAttribInRequest(request, "displayAgencyMode", "0");
				}
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS,
				"displayAgencyMode = " + getAttribInRequest(request, "displayAgencyMode") + ";");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	private static String getSummary(ResultSet resultSet) {

		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		StringBuilder sb = new StringBuilder();
		sb.append("<html>");
		sb.append("<table>");
		sb.append("<tr><th>Intl Flight, Segment, Pax</th></tr>");
		sb.append("<tr></tr>");
		try {
			while (resultSet.next()) {
				String seg = resultSet.getString("segment_code");
				String internationalFlight = resultSet.getString("intFlit");
				int paxCount = resultSet.getInt("count");

				sb.append("<tr>");
				sb.append("<td>");
				sb.append(internationalFlight + " ," + seg + " ," + paxCount);
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("<tr></tr>");
			}

		} catch (SQLException e) {
		}
		sb.append("<tr></tr>").append("<tr></tr>").append("<tr></tr>");
		sb.append("<tr>").append(strCarrier).append("</tr>");
		sb.append("<tr>*** End of Report ***</tr>");
		sb.append("<tr></tr>").append("<tr></tr>");
		sb.append("</table>");
		sb.append("</html>");

		return sb.toString();
	}

}
