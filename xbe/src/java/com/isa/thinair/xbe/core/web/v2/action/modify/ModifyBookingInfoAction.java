package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPaxNamesTranslation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.AdultPaxTO;
import com.isa.thinair.xbe.api.dto.v2.InfantPaxTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Action to handle modifying an interline reservation's booking information
 * 
 * @author Zaki Saimeh
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ModifyBookingInfoAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ModifyBookingInfoAction.class);

	private String pnr;
	private boolean success = true;
	private String messageTxt;
	private String reservationResponse;
	private String resPax;

	private List<AdultPaxTO> adultPaxTOList;
	private List<InfantPaxTO> infantPaxTOList;
	private String userNotes;
	private String userNoteType;
	private String groupPNR;
	private String version;

	private String foidIds = "";
	private String foidCode = "";
	private Integer[] segmentList;
	private String returnData;

	private String selectedBookingCategory;

	private String itineraryFareMaskOption;

	private String hdnItineraryFareMask;

	private String endorsementTxt;

	private boolean applyNameChangeCharge;
	
	private String reasonForAllowBlPax;

	private String selectedOriginCountry;

	public String execute() {

		String strForward = S2Constants.Result.SUCCESS;
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			if (!isGroupPNR) {
				groupPNR = pnr;
			}

			LCCClientReservation oldLccReservation = populateOldLccReservation();

			LCCClientReservation lccClientReservation = new LCCClientReservation();
			lccClientReservation.setPNR(groupPNR);
			lccClientReservation.setLastUserNote(userNotes);
			lccClientReservation.setUserNoteType(userNoteType);

			List<LCCClientReservationPax> paxAdults = ReservationUtil.populatePaxAdults(adultPaxTOList);

			if (paxAdults != null) {
				for (LCCClientReservationPax lccClientReservationPax : paxAdults) {
					lccClientReservation.addPassenger(lccClientReservationPax);
				}
			}
			
			if(!BasicRH.hasPrivilege(request,PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_NAME_CHANGE)){
				checkNameChange(oldLccReservation);
			}

			if (infantPaxTOList != null) {
				for (InfantPaxTO infantPaxTO : infantPaxTOList) {
					LCCClientReservationPax infantLccPax = new LCCClientReservationPax();
					infantLccPax.setTravelerRefNumber(infantPaxTO.getDisplayPaxTravelReference());
					infantLccPax.setPaxSequence(PaxTypeUtils.getPaxSeq(infantPaxTO.getDisplayPaxTravelReference()));
					infantLccPax.setPaxType(ReservationInternalConstants.PassengerType.INFANT);
					infantLccPax.setFirstName(infantPaxTO.getDisplayInfantFirstName());
					infantLccPax.setLastName(infantPaxTO.getDisplayInfantLastName());
					infantLccPax.setTitle(infantPaxTO.getDisplayInfantTitle());
					if (infantPaxTO.getDisplayInfantNationality() != null
							&& !infantPaxTO.getDisplayInfantNationality().equals("")) {
						infantLccPax.setNationalityCode(Integer.valueOf(infantPaxTO.getDisplayInfantNationality()));
					}
					String infantDOB = infantPaxTO.getDisplayInfantDOB();
					infantLccPax.setDateOfBirth(infantDOB != null && infantDOB.trim().length() > 0 ? BookingUtil
							.stringToDate(infantDOB) : null);
					String adultSeq = infantPaxTO.getDisplayInfantTravellingWith();
					for (LCCClientReservationPax adults : lccClientReservation.getPassengers()) {
						if (adults.getPaxSequence().intValue() == new Integer(adultSeq).intValue()) {
							infantLccPax.setParent(adults);
							adults.getInfants().add(infantLccPax);
							break;
						}
					}

					LCCClientReservationAdditionalPax addnInfo = new LCCClientReservationAdditionalPax();

					String foidNum = "";
					Date foidExpiry = null;
					String foidPlace = "";
					String arrivalIntlFlightNo = "";
					String intlFlightArrivalDate = "";
					String intlFlightArrivalTime = "";
					String departureIntlFlightNo = "";
					String intlFlightDepartureDate = "";
					String intlFlightDepartureTime = "";
					Date internationalFlightArrivalDate = null;
					Date internationalFlightDepartureDate = null;
					String pnrPaxGroupId = "";
					String nationalIDNo = "";
					
					if (infantPaxTO.getDisplayNationalIDNo() != null) {
						nationalIDNo = infantPaxTO.getDisplayNationalIDNo();
					}
					
					if (infantPaxTO.getDisplayPnrPaxCatFOIDNumber() != null
							&& !infantPaxTO.getDisplayPnrPaxCatFOIDNumber().equals("null")
							&& !infantPaxTO.getDisplayPnrPaxCatFOIDNumber().equals("undefined")
							&& !infantPaxTO.getDisplayPnrPaxCatFOIDNumber().equals("")) {
						foidNum = infantPaxTO.getDisplayPnrPaxCatFOIDNumber();
					}

					if (infantPaxTO.getDisplayPnrPaxCatFOIDExpiry() != null
							&& !infantPaxTO.getDisplayPnrPaxCatFOIDExpiry().equals("null")
							&& !infantPaxTO.getDisplayPnrPaxCatFOIDExpiry().equals("undefined")
							&& !infantPaxTO.getDisplayPnrPaxCatFOIDExpiry().equals("")) {
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						foidExpiry = sdf.parse(infantPaxTO.getDisplayPnrPaxCatFOIDExpiry());
					}

					if (infantPaxTO.getDisplayPnrPaxCatFOIDPlace() != null
							&& !infantPaxTO.getDisplayPnrPaxCatFOIDPlace().equals("null")
							&& !infantPaxTO.getDisplayPnrPaxCatFOIDPlace().equals("undefined")
							&& !infantPaxTO.getDisplayPnrPaxCatFOIDPlace().equals("")) {
						foidPlace = infantPaxTO.getDisplayPnrPaxCatFOIDPlace();
					}
					if (infantPaxTO.getDisplaypnrPaxArrivalFlightNumber() != null
							&& !infantPaxTO.getDisplaypnrPaxArrivalFlightNumber().equals("null")
							&& !infantPaxTO.getDisplaypnrPaxArrivalFlightNumber().equals("")) {
						arrivalIntlFlightNo = infantPaxTO.getDisplaypnrPaxArrivalFlightNumber();
					}
					
					if (infantPaxTO.getDisplaypnrPaxFltArrivalDate() != null
							&& !infantPaxTO.getDisplaypnrPaxFltArrivalDate().equals("null")
							&& !infantPaxTO.getDisplaypnrPaxFltArrivalDate().equals("")) {
						intlFlightArrivalDate = infantPaxTO.getDisplaypnrPaxFltArrivalDate();
					}
					
					if (infantPaxTO.getDisplaypnrPaxArrivalTime() != null && !infantPaxTO.getDisplaypnrPaxArrivalTime().equals("null")
							&& !infantPaxTO.getDisplaypnrPaxArrivalTime().equals("")) {
						intlFlightArrivalTime = infantPaxTO.getDisplaypnrPaxArrivalTime();
					}
					
					if (infantPaxTO.getDisplaypnrPaxDepartureFlightNumber() != null
							&& !infantPaxTO.getDisplaypnrPaxDepartureFlightNumber().equals("null")
							&& !infantPaxTO.getDisplaypnrPaxDepartureFlightNumber().equals("")) {
						departureIntlFlightNo = infantPaxTO.getDisplaypnrPaxDepartureFlightNumber();
					}
					
					if (infantPaxTO.getDisplaypnrPaxFltDepartureDate() != null
							&& !infantPaxTO.getDisplaypnrPaxFltDepartureDate().equals("null")
							&& !infantPaxTO.getDisplaypnrPaxFltDepartureDate().equals("")) {
						intlFlightDepartureDate = infantPaxTO.getDisplaypnrPaxFltDepartureDate();
					}
					
					if (infantPaxTO.getDisplaypnrPaxDepartureTime() != null && !infantPaxTO.getDisplaypnrPaxDepartureTime().equals("null")
							&& !infantPaxTO.getDisplaypnrPaxDepartureTime().equals("")) {
						intlFlightDepartureTime = infantPaxTO.getDisplaypnrPaxDepartureTime();
					}
					
					if (infantPaxTO.getDisplayPnrPaxGroupId() != null
							&& !infantPaxTO.getDisplayPnrPaxGroupId().equals("null")
							&& !infantPaxTO.getDisplayPnrPaxGroupId().equals("undefined")
							&& !infantPaxTO.getDisplayPnrPaxGroupId().equals("")) {
						pnrPaxGroupId = infantPaxTO.getDisplayPnrPaxGroupId();
					}
					
					internationalFlightArrivalDate = getTimeStampObj(intlFlightArrivalDate, intlFlightArrivalTime);
					internationalFlightDepartureDate = getTimeStampObj(intlFlightDepartureDate,intlFlightDepartureTime);
					addnInfo.setPassportNo(foidNum);
					addnInfo.setPassportExpiry(foidExpiry);
					addnInfo.setPassportIssuedCntry(foidPlace);
					addnInfo.setArrivalIntlFltNo(arrivalIntlFlightNo);
					addnInfo.setIntlFltArrivalDate(internationalFlightArrivalDate);
					addnInfo.setDepartureIntlFltNo(departureIntlFlightNo);
					addnInfo.setIntlFltDepartureDate(internationalFlightDepartureDate);
					addnInfo.setPnrPaxGroupId(pnrPaxGroupId);
					addnInfo.setNationalIDNo(nationalIDNo);

					if (infantPaxTO.getDisplayPnrPaxPlaceOfBirth() != null
							&& !infantPaxTO.getDisplayPnrPaxPlaceOfBirth().equals("null")
							&& !infantPaxTO.getDisplayPnrPaxPlaceOfBirth().equals("undefined")
							&& !infantPaxTO.getDisplayPnrPaxPlaceOfBirth().equals("")) {
						addnInfo.setPlaceOfBirth(infantPaxTO.getDisplayPnrPaxPlaceOfBirth());
					}
					addnInfo.setTravelDocumentType(infantPaxTO.getDisplayTravelDocType());
					
					if (infantPaxTO.getDisplayVisaDocNumber() != null
							&& !infantPaxTO.getDisplayVisaDocNumber().equals("null")
							&& !infantPaxTO.getDisplayVisaDocNumber().equals("undefined")
							&& !infantPaxTO.getDisplayVisaDocNumber().equals("")) {
						addnInfo.setVisaDocNumber(infantPaxTO.getDisplayVisaDocNumber());
					}
					if (infantPaxTO.getDisplayVisaDocPlaceOfIssue() != null
							&& !infantPaxTO.getDisplayVisaDocPlaceOfIssue().equals("null")
							&& !infantPaxTO.getDisplayVisaDocPlaceOfIssue().equals("undefined")
							&& !infantPaxTO.getDisplayVisaDocPlaceOfIssue().equals("")) {
						addnInfo.setVisaDocPlaceOfIssue(infantPaxTO.getDisplayVisaDocPlaceOfIssue());
					}

					if (infantPaxTO.getDisplayVisaDocIssueDate() != null
							&& !infantPaxTO.getDisplayVisaDocIssueDate().equals("null")
							&& !infantPaxTO.getDisplayVisaDocIssueDate().equals("undefined")
							&& !infantPaxTO.getDisplayVisaDocIssueDate().equals("")) {
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						addnInfo.setVisaDocIssueDate(sdf.parse(infantPaxTO.getDisplayVisaDocIssueDate()));
					}

					if (infantPaxTO.getDisplayVisaApplicableCountry() != null
							&& !infantPaxTO.getDisplayVisaApplicableCountry().equals("null")
							&& !infantPaxTO.getDisplayVisaApplicableCountry().equals("undefined")
							&& !infantPaxTO.getDisplayVisaApplicableCountry().equals("")) {
						addnInfo.setVisaApplicableCountry(infantPaxTO.getDisplayVisaApplicableCountry());
					}
					infantLccPax.setLccClientAdditionPax(addnInfo);
					String firstNameOl = infantPaxTO.getDisplayInfantFirstNameOl();
					String lastNameOl = infantPaxTO.getDisplayInfantLastNameOl();
					String translationLanguage = infantPaxTO.getDisplayNameTranslationLanguage();
					String infantTitleOl = infantPaxTO.getDisplayInfantTitleOl();
					if (firstNameOl != null && lastNameOl != null && !firstNameOl.isEmpty() && !lastNameOl.isEmpty() && !translationLanguage.isEmpty()) {
						LCCClientReservationPaxNamesTranslation paxNameTranslation = new LCCClientReservationPaxNamesTranslation();
						paxNameTranslation.setFirstNameOl(firstNameOl);
						paxNameTranslation.setLastNameOl(lastNameOl);
						paxNameTranslation.setLanguageCode(translationLanguage);
						paxNameTranslation.setTitleOl(infantTitleOl);
						infantLccPax.setLccClientPaxNameTranslations(paxNameTranslation);
					}
					lccClientReservation.addPassenger(infantLccPax);

				}
			}

			// Correct Traveler References
			for (LCCClientReservationPax lccClientReservationPax : lccClientReservation.getPassengers()) {
				if (ReservationInternalConstants.PassengerType.INFANT.equals(lccClientReservationPax.getPaxType())) {
					lccClientReservationPax.setTravelerRefNumber(PaxTypeUtils
							.fixLccInfantTravelerReference(lccClientReservationPax));
				}
			}
			lccClientReservation.setVersion(version);

			if (foidIds.length() > 0 && foidIds != null && segmentList.length > 0) {
				returnData = ReservationUtil.getUniqueFoidData(foidIds, foidCode, segmentList, log, pnr);
				if (returnData != "true") {
					throw new XBERuntimeException(WebConstants.DUPLICATE_PASSPORT_FOR_SEGMENT);
				}
			}
			if ((BeanUtils.nullHandler(endorsementTxt).length() > 0) && (BeanUtils.nullHandler(endorsementTxt).length() < 500)) {
				lccClientReservation.setEndorsementNote(endorsementTxt);
			} else {
				if (BeanUtils.nullHandler(endorsementTxt).length() == 0) {
					lccClientReservation.setEndorsementNote(null);
				} else {
					lccClientReservation.setEndorsementNote(endorsementTxt.substring(0, 500));
				}

			}
			// update booking status
			lccClientReservation.setBookingCategory(selectedBookingCategory);

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
					? resInfo.getReservationStatus()
					: null, null, resInfo.isModifiableReservation(), false, resInfo.getLccPromotionInfoTO(), null, false, false);

			if (!rpParams.isItineraryFareMaskModifyPrivilege()) {
				itineraryFareMaskOption = oldLccReservation.getItineraryFareMask();
			}

			lccClientReservation.setItineraryFareMask(itineraryFareMaskOption);
			lccClientReservation.setOriginCountryOfCall(selectedOriginCountry);
			boolean skipDuplicateNameCheck = BasicRH.hasPrivilege(request,
					PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_DUPLI_NAME_SKIP);

			lccClientReservation.setReasonForAllowBlPax(reasonForAllowBlPax);
			// Update passenger information
			ModuleServiceLocator.getAirproxyReservationBD().modifyPassengerInfo(lccClientReservation, oldLccReservation,
					isGroupPNR, getClientInfoDTO(), AppIndicatorEnum.APP_XBE.toString(), skipDuplicateNameCheck,
					applyNameChangeCharge, rpParams.getAllowedSubOperations(), getTrackInfo());

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return strForward;
	}

	private LCCClientReservation populateOldLccReservation() throws Exception {
		LCCClientReservation lccClientReservation = new LCCClientReservation();
		lccClientReservation.setPNR(groupPNR);
		lccClientReservation.setLastUserNote(userNotes);
		lccClientReservation.setVersion(version);
		lccClientReservation.setItineraryFareMask(hdnItineraryFareMask);
		Collection<LCCClientReservationPax> colpaxs = ReservationUtil.transformJsonPassengers(resPax);
		for (LCCClientReservationPax lccClientReservationPax : colpaxs) {
			lccClientReservation.addPassenger(lccClientReservationPax);
		}
		return lccClientReservation;
	}
	
	private static Date getTimeStampObj(String strDate, String strTime) {
		try {
			Calendar validDate =null;
			Date dtReturn = null;
			if (!strDate.equals("")) {
				int date = Integer.parseInt(strDate.substring(0, 2));
				int month = Integer.parseInt(strDate.substring(3, 5));
				int year = Integer.parseInt(strDate.substring(6, 10));
				
				validDate = new GregorianCalendar(year, month - 1, date);
				
			}
			if (!strTime.equals("") && validDate!= null) {
				Date tmpDate = validDate.getTime();
				Calendar tmpCalendar = Calendar.getInstance();
				tmpCalendar.setTime(tmpDate);
				int hours = Integer.parseInt(strTime.substring(0, 2));
				int minutes = Integer.parseInt(strTime.substring(3, 5));
				tmpCalendar.add(Calendar.HOUR, hours);
				tmpCalendar.add(Calendar.MINUTE, minutes);
				validDate = tmpCalendar;
			}
			if (validDate != null) {
				dtReturn = validDate.getTime();
			}
			return dtReturn;
			
		} catch (Exception e) {
			return null;
		}
	}
	
	private void checkNameChange(LCCClientReservation oldLccReservation) throws ModuleException {
		XBEReservationInfoDTO sessionResInfo = (XBEReservationInfoDTO) request.getSession()
				.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

		boolean isOnhold = ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(sessionResInfo.getReservationStatus());
		// TODO : Handle other flows
		for(LCCClientReservationPax oldReservationPax: oldLccReservation.getPassengers()){
			for(AdultPaxTO adultPaxTO : adultPaxTOList){
				if(adultPaxTO.getDisplayPaxTravelReference().equals(oldReservationPax.getTravelerRefNumber())){
					String paxName = getFullName(adultPaxTO.getDisplayAdultTitle(),adultPaxTO.getDisplayAdultFirstName(),adultPaxTO.getDisplayAdultLastName());
					String paxOldName = getFullName(oldReservationPax.getTitle(),oldReservationPax.getFirstName(),oldReservationPax.getLastName());
					if(paxOldName.contains(com.isa.thinair.webplatform.api.util.ReservationUtil.TBA_USER) && BasicRH.hasPrivilege(request,PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_TBA_NAME_CHANGE) ){
						break;
					}else if(paxName.equals(paxOldName)){
						break;
					}else if (!isOnhold){
						throw new ModuleException("airreservation.modify.reservation.unauthorized.nameChange");
					}
				}	
			}
			if (infantPaxTOList != null) {
				for (InfantPaxTO infantPaxTO : infantPaxTOList) {
					if(infantPaxTO.getDisplayPaxTravelReference().equals(oldReservationPax.getTravelerRefNumber())){
						String paxName = getFullName(infantPaxTO.getDisplayInfantTitle(),infantPaxTO.getDisplayInfantFirstName(),infantPaxTO.getDisplayInfantLastName());
						String paxOldName = getFullName(oldReservationPax.getTitle(),oldReservationPax.getFirstName(),oldReservationPax.getLastName());
						if(paxOldName.contains(com.isa.thinair.webplatform.api.util.ReservationUtil.TBA_USER) && BasicRH.hasPrivilege(request,PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_TBA_NAME_CHANGE) ){
							break;
						}else if(paxName.equals(paxOldName)){
							break;
						}else if (!isOnhold){
							throw new ModuleException("airreservation.modify.reservation.unauthorized.nameChange");
						}
					}	
				}
			}
		}
	}
		
	private String getFullName(String title, String firstName, String lastName) {
		
		String fullName = BeanUtils.nullHandler(title) + ReservationInternalConstants.DLIM
				+ BeanUtils.nullHandler(firstName) + ReservationInternalConstants.DLIM
				+ BeanUtils.nullHandler(lastName);
		return fullName.toUpperCase();
		
	}


	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the messageTxt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @param messageTxt
	 *            the messageTxt to set
	 */
	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	/**
	 * @return the reservationResponse
	 */
	public String getReservationResponse() {
		return reservationResponse;
	}

	/**
	 * @param reservationResponse
	 *            the reservationResponse to set
	 */
	public void setReservationResponse(String reservationResponse) {
		this.reservationResponse = reservationResponse;
	}

	/**
	 * @return the resPax
	 */
	public String getResPax() {
		return resPax;
	}

	/**
	 * @param resPax
	 *            the resPax to set
	 */
	public void setResPax(String resPax) {
		this.resPax = resPax;
	}

	/**
	 * @return the adultPaxTOList
	 */
	public List<AdultPaxTO> getAdultPaxTOList() {
		return adultPaxTOList;
	}

	/**
	 * @param adultPaxTOList
	 *            the adultPaxTOList to set
	 */
	public void setAdultPaxTOList(List<AdultPaxTO> adultPaxTOList) {
		this.adultPaxTOList = adultPaxTOList;
	}

	/**
	 * @return the infantPaxTOList
	 */
	public List<InfantPaxTO> getInfantPaxTOList() {
		return infantPaxTOList;
	}

	/**
	 * @param infantPaxTOList
	 *            the infantPaxTOList to set
	 */
	public void setInfantPaxTOList(List<InfantPaxTO> infantPaxTOList) {
		this.infantPaxTOList = infantPaxTOList;
	}

	/**
	 * @return the userNotes
	 */
	public String getUserNotes() {
		return userNotes;
	}

	/**
	 * @param userNotes
	 *            the userNotes to set
	 */
	public void setUserNotes(String userNotes) {
		this.userNotes = userNotes;
	}

	/**
	 * @return the userNoteType
	 */
	public String getUserNoteType() {
		return userNoteType;
	}

	/**
	 * @param userNoteType the userNoteType to set
	 */
	public void setUserNoteType(String userNoteType) {
		this.userNoteType = userNoteType;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getReturnData() {
		return returnData;
	}

	public void setReturnData(String returnData) {
		this.returnData = returnData;
	}

	public String getFoidIds() {
		return foidIds;
	}

	public void setFoidIds(String foidIds) {
		this.foidIds = foidIds;
	}

	public String getFoidCode() {
		return foidCode;
	}

	public void setFoidCode(String foidCode) {
		this.foidCode = foidCode;
	}

	public Integer[] getSegmentList() {
		return segmentList;
	}

	public void setSegmentList(Integer[] segmentList) {
		this.segmentList = segmentList;
	}

	public String getEndorsementTxt() {
		return endorsementTxt;
	}

	public void setEndorsementTxt(String endorsementTxt) {
		this.endorsementTxt = endorsementTxt;
	}

	public void setSelectedBookingCategory(String selectedBookingCategory) {
		this.selectedBookingCategory = selectedBookingCategory;
	}

	public void setItineraryFareMaskOption(String itineraryFareMaskOption) {
		this.itineraryFareMaskOption = itineraryFareMaskOption;
	}

	public String getHdnItineraryFareMask() {
		return hdnItineraryFareMask;
	}

	public void setHdnItineraryFareMask(String hdnItineraryFareMask) {
		this.hdnItineraryFareMask = hdnItineraryFareMask;
	}

	public boolean isApplyNameChangeCharge() {
		return applyNameChangeCharge;
	}

	public void setApplyNameChangeCharge(boolean applyNameChangeCharge) {
		this.applyNameChangeCharge = applyNameChangeCharge;
	}

	public String getReasonForAllowBlPax() {
		return reasonForAllowBlPax;
	}

	public void setReasonForAllowBlPax(String reasonForAllowBlPax) {
		this.reasonForAllowBlPax = reasonForAllowBlPax;
	}
	
	public void setSelectedOriginCountry(String selectedOriginCountry) {
		this.selectedOriginCountry = selectedOriginCountry;
	}

}