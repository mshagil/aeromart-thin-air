package com.isa.thinair.xbe.core.web.handler.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reservation.ReservationHG;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class LoadDataRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(LoadDataRequestHandler.class);

	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SUCCESS;
		String strReturnData = "";
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			strReturnData += storeExternalChargesInfo(request);
			String strID = request.getParameter("id");
			String[] arrCommands = strID.split(",");
			boolean blnShowCredit = true;
			for (String arrCommand : arrCommands) {
				if (arrCommand.equals("TIMEOUTRESET")) {
					strReturnData += setTimeOutRest(request);
				}
			}
			if (blnShowCredit) {
				strReturnData += "top.intAgtBalance = '" + ReservationHG.getUsersAvailableCredit(request) + "';";
				strReturnData += "top.showAgentCreditInAgentCurrency = 'true';";
				strReturnData += "top.agentAvailableCreditInAgentCurrency= '" + ReservationHG.getUserAvailableCreditInUserCurrency(request, AppSysParamsUtil.getBaseCurrency()) + "';";
			}
			
		} catch (XBERuntimeException xre) {
			forward = S2Constants.Result.ERROR;
			strReturnData = "strSYSError = '" + XBEConfig.getServerMessage(xre.getExceptionCode(), userLanguage, null) + "'";
			log.error("LoadDataRequestHandler.execute", xre);
		} catch (ModuleException me) {
			forward = S2Constants.Result.ERROR;
			strReturnData = "strSYSError = '" + me.getMessageString() + "'";
			log.error("LoadDataRequestHandler.execute", me);
		} catch (Exception ex) {
			forward = S2Constants.Result.ERROR;
			strReturnData = "strSYSError = '" + ex.getMessage() + "'";
			log.error("LoadDataRequestHandler.execute", ex);
		}

		request.setAttribute("returnData", strReturnData);
		return forward;
	}

	/**
	 * Stores the external charges information
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static String storeExternalChargesInfo(HttpServletRequest request) throws ModuleException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.SEAT_MAP);
		// Haider
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.MEAL);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.FLEXI_CHARGES);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.PROMOTION_REWARD);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN);

		Map externalChargesMap = ModuleServiceLocator.getReservationBD()
				.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null);
		request.getSession().setAttribute(WebConstants.SES_EXTERNAL_CHARGES_DATA, externalChargesMap);

		BigDecimal agentHandlingCharge = ((ExternalChgDTO) externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE)).getAmount();

		return "top.strAgentHandlingCharge = '" + agentHandlingCharge.toString() + "';";
	}

	private static String setTimeOutRest(HttpServletRequest request) throws ModuleException {
		return "";
	}

}
