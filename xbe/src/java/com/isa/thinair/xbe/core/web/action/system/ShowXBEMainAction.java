package com.isa.thinair.xbe.core.web.action.system;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.system.ShowMenuRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.System.MENU),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT),
		@Result(name = S2Constants.Actions.SHOW_PASSWORD_CHANGE_ACTION, value = S2Constants.Jsp.System.PASSWORD_CHANGE) })
public class ShowXBEMainAction extends BaseRequestAwareAction {

	public String execute() {
		return ShowMenuRH.execute(request);
	}
}
