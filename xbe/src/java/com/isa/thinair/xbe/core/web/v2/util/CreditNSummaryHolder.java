package com.isa.thinair.xbe.core.web.v2.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.xbe.api.dto.v2.PaxSummaryCreditFETO;
import com.isa.thinair.xbe.api.dto.v2.PnrCreditFETO;

public class CreditNSummaryHolder {

	private Map<Integer, PaxSummaryCreditFETO> paxCreditMap;

	public CreditNSummaryHolder(List<PaxSummaryCreditFETO> paxCreditList) {
		if (paxCreditList != null) {
			paxCreditMap = new HashMap<>();
			for (PaxSummaryCreditFETO paxFETO : paxCreditList) {
				paxCreditMap.put(paxFETO.getDisplayPaxID(), paxFETO);
			}
		}
	}

	private Map<Integer, PaxSummaryCreditFETO> getPaxCreditMap() {
		if (paxCreditMap != null) {
			return paxCreditMap;
		}
		return new HashMap<>();
	}

	public boolean hasPaxWithPayments() {
		for (PaxSummaryCreditFETO feto : getPaxCreditMap().values()) {
			if (feto.isHasPayment()) {
				return true;
			}
		}
		return false;
	}

	public boolean isPaxFullyPaidByCredit(Integer paxSequence) {
		if (getPaxCreditMap().containsKey(paxSequence)) {
			PaxSummaryCreditFETO pax = getPaxCreditMap().get(paxSequence);
			return pax.isFullyPaidByCredit();
		}
		return false;
	}

	public int getTotalPaxWithPayments() {
		int paxCount = 0;
		for (PaxSummaryCreditFETO feto : getPaxCreditMap().values()) {
			if (feto.isHasPayment()) {
				paxCount++;
			}
		}
		return paxCount;
	}

	public boolean hasPayment(Integer paxSequence) {
		if (getPaxCreditMap().containsKey(paxSequence)) {
			PaxSummaryCreditFETO pax = getPaxCreditMap().get(paxSequence);
			return pax.isHasPayment();
		}
		return false;
	}

	public List<PnrCreditFETO> getPaxCreditInfo(Integer paxSequence) {
		if (getPaxCreditMap().containsKey(paxSequence)) {
			PaxSummaryCreditFETO pax = getPaxCreditMap().get(paxSequence);
			return pax.getPaxCreditInfo();
		}
		return null;
	}

	public int getTotalPaxWithoutFullCreditPayment(String... paxTypes) {
		int paxCount = 0;
		for (String paxType : paxTypes) {
			for (PaxSummaryCreditFETO feto : getPaxCreditMap().values()) {
				if (paxType.equals(feto.getPaxType()) && !feto.isFullyPaidByCredit()) {
					paxCount++;
				}
			}
		}
		return paxCount;
	}

}
