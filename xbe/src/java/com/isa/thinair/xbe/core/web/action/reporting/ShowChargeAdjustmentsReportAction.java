package com.isa.thinair.xbe.core.web.action.reporting;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.reporting.ChargeAdjustmentReportRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.CHARGE_ADJUSTMENTS_REPORT)
public class ShowChargeAdjustmentsReportAction extends BaseRequestResponseAwareAction {
	public String execute() {
		return ChargeAdjustmentReportRH.execute(request, response);
	}
}
