package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class LoyaltyPointsReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(LoyaltyPointsReportRH.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				return null;
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("LoyaltyPointsRH setReportView Failed " + e.getMessageString());
			forward = S2Constants.Result.ERROR;
		} catch (Exception e) {
			log.error("Error in LoyaltyPointsRH execute()" + e.getMessage());
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}

	/**
	 * Sets the initial data for the report.
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * Sets client error messages
	 * 
	 * @param request
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Gets the report data and sets to the corresponding view option.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		ResultSet resultSet = null;

		String reportTemplateCre = "LoyaltyPointsReport.jasper";
		String reportTemplateUse = "LoyaltyPointsUsageReport.jasper";

		String id = "UC_REPM_070";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../images/" + AppSysParamsUtil.getReportLogo(true); 
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

		String value = request.getParameter("radReportOption");
		String accountNo = request.getParameter("txtAccountNo");
		String acccountType = request.getParameter("radAccountType");
		String expDays = request.getParameter("txtExpDays");
		String reportType = request.getParameter("radCreditSumm");
		String usageFromDate = request.getParameter("txtUsegeFrom");
		String usageToDate = request.getParameter("txtUsegeTo");

		if (isNotEmptyOrNull(reportType) && reportType.equals("CRE_SUM")) {
			Date currDate = new Date();
			Calendar cal = Calendar.getInstance();
			String fromDate = CalendarUtil.getSQLToDateInputString(currDate);
			Date toDate = null;
			String strToDate = null;
			if (isNotEmptyOrNull(expDays)) {
				Integer days = new Integer(expDays);
				cal.add(GregorianCalendar.DATE, days);
				toDate = cal.getTime();
				GregorianCalendar calendar = new GregorianCalendar();
				calendar.setTime(toDate);
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mi:ss");
				String formattedDate = dateFormat.format(calendar.getTime());
				strToDate = "'" + formattedDate + "'" + "," + "'dd-mon-yyyy HH24:mi:ss'";
			}
			search.setReportType(reportType);
			if (isNotEmptyOrNull(accountNo)) {
				search.setAccountNumber(accountNo);

			} else {
				if (isNotEmptyOrNull(acccountType) && acccountType.equals("CREDIT")) {
					search.setByCredit(true);
					if (isNotEmptyOrNull(expDays)) {
						search.setDateRangeFrom(fromDate);
						search.setDateRangeTo(strToDate);
					}
				}
			}
		} else {
			if (isNotEmptyOrNull(accountNo) && isNotEmptyOrNull(usageFromDate) && isNotEmptyOrNull(usageToDate)) {
				String fromDate = "'" + usageFromDate + " 00:00:00 ','dd/mm/yyyy HH24:mi:ss' ";
				String toDate = "'" + usageToDate + " 23:59:59 ','dd/mm/yyyy HH24:mi:ss' ";
				search.setReportType(reportType);
				search.setAccountNumber(accountNo);
				search.setDateRangeFrom(fromDate);
				search.setDateRangeTo(toDate);
			}

		}
		if (isNotEmptyOrNull(reportType) && reportType.equals("CRE_SUM")) {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplateCre));
		} else {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplateUse));
		}

		resultSet = ModuleServiceLocator.getDataExtractionBD().getLoyaltyPointsData(search);

		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("ACC_NO", accountNo);
		parameters.put("ID", id);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

		// To provide Report Format Options
		String reportNumFormat = request.getParameter("radRptNumFormat");
		ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

		if (value.trim().equals("HTML")) {
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
					response);
		} else if (value.trim().equals("PDF")) {
			response.reset();
			response.addHeader("Content-Disposition", "filename=LoyaltyPointsReport.pdf");
			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
		} else if (value.trim().equals("EXCEL")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=LoyaltyPointsReport.xls");
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
		} else if (value.trim().equals("CSV")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=LoyaltyPointsReport.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
		}

	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}

}
