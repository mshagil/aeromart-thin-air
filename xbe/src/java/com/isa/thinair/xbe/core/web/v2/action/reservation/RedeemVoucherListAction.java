package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.dto.VoucherRedeemResponse;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.service.AirproxyVoucherBD;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherRedeemRequest;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.model.Voucher;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * @author chethiya
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class RedeemVoucherListAction extends BaseRequestAwareAction {

	private Log log = LogFactory.getLog(RedeemVoucherListAction.class);

	private VoucherRedeemRequest voucherRedeemRequest;

	private VoucherRedeemResponse voucherRedeemResponse;

	private String selCurrencyCode;

	private boolean canceledVouchersAvailable = false;

	private boolean disableRedeem = false;

	private String message;

	private boolean onLoad;

	private boolean success = true;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		
		if (AppSysParamsUtil.isVoucherEnabled()) {
			VoucherRedeemResponse voucherRedeemResponse = null;
			AirproxyVoucherBD airproxyVoucherBD = ModuleServiceLocator.getAirproxyVoucherBD();
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			
			if (bookingShoppingCart.getPayByVoucherInfo() == null
					|| (bookingShoppingCart.getPayByVoucherInfo() != null && bookingShoppingCart.getPayByVoucherInfo()
							.getUnsuccessfulAttempts() < 2)) {

				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(new Date());
				try {
					BigDecimal balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
					
					if (AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
							.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, getTrackInfo().getOriginAgentCode())) {
						if (bookingShoppingCart.getReservationBalance() == null) {
							balanceToPay = bookingShoppingCart.getTotalPayableIncludingCCCharges();
						} else {
							balanceToPay = bookingShoppingCart.getTotalPayable();
						}
					} else if (bookingShoppingCart.getTotalPayable() != null
							&& bookingShoppingCart.getTotalPayable().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
						balanceToPay = bookingShoppingCart.getTotalPayable();
					}
					
					List<LCCClientReservationPax> passengerList = null;
					if (balanceToPay.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
						//Balance payment flow
						balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
						Collection<LCCClientReservationPax> colPaxes = ReservationUtil.transformJsonPassengers(request
								.getParameter("resPax"));
						Set<LCCClientReservationPax> passengers = new HashSet<LCCClientReservationPax>(colPaxes);

						passengerList = new ArrayList<LCCClientReservationPax>(passengers);
						for (LCCClientReservationPax pax : passengerList) {
							if (pax.getTotalAvailableBalance().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
								balanceToPay = AccelAeroCalculator.add(balanceToPay, pax.getTotalAvailableBalance());
							}
						}
						if (bookingShoppingCart.getTotalPayable() != null
								&& bookingShoppingCart.getTotalPayable()
										.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) < 0) {
							balanceToPay = AccelAeroCalculator.add(balanceToPay, bookingShoppingCart.getTotalPayable());
						}
					}					
					
					CurrencyExchangeRate currencyExchangeRate = null;
					if ((bookingShoppingCart.getSelectedCurrencyTimestamp() == null || bookingShoppingCart
							.getCurrencyExchangeRate() == null) && selCurrencyCode != null) {
						currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrencyCode, ApplicationEngine.XBE);
						bookingShoppingCart.setCurrencyExchangeRate(currencyExchangeRate);
					} else {
						currencyExchangeRate = bookingShoppingCart.getCurrencyExchangeRate();
					}
					Currency currency = currencyExchangeRate.getCurrency();
					if (StringUtil.isNullOrEmpty(selCurrencyCode)) {
						selCurrencyCode = currency.getCurrencyCode();
					}
					
					if (bookingShoppingCart.getPayByVoucherInfo() != null) {
						if (balanceToPay.compareTo(
								new BigDecimal(bookingShoppingCart.getPayByVoucherInfo().getVouchersTotal())) > -1) {
							balanceToPay = AccelAeroCalculator.subtract(AccelAeroCalculator.scaleValueDefault(balanceToPay),
									AccelAeroCalculator.scaleValueDefault(new BigDecimal(bookingShoppingCart
											.getPayByVoucherInfo().getVouchersTotal())));
						} else {
							balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
						}
					}

					String voucherId = null;
					if (bookingShoppingCart.getPayByVoucherInfo() != null) {
						if (onLoad) {
							voucherRedeemRequest = new VoucherRedeemRequest();
							voucherRedeemRequest.setBlockedVoucherIDList(bookingShoppingCart.getPayByVoucherInfo().getVoucherIDList());
							voucherRedeemRequest.setVoucherIDList(bookingShoppingCart.getPayByVoucherInfo().getVoucherIDList());

							PayByVoucherInfo payByVoucherInfo = bookingShoppingCart.getPayByVoucherInfo();
							voucherRedeemResponse = new VoucherRedeemResponse();
							voucherRedeemResponse.setRedeemVoucherList(payByVoucherInfo.getVoucherDTOList());
							voucherRedeemResponse.setRedeemedTotal(String.valueOf(payByVoucherInfo.getRedeemedTotal()));
							voucherRedeemResponse.setVouchersTotal(payByVoucherInfo.getVouchersTotal());
							voucherRedeemResponse.setVoucherPaymentOption(payByVoucherInfo.getVoucherPaymentOption());
						} else {
							for (String voucherID : voucherRedeemRequest.getVoucherIDList()) {
								if (bookingShoppingCart.getPayByVoucherInfo().getVoucherIDList().contains(voucherID)) {
									ModuleException moduleException = new ModuleException(
											WebConstants.KEY_VOUCHER_USED_IN_SESSION);
									throw moduleException;
								}
							}
							voucherRedeemRequest.setBlockedVoucherIDList(bookingShoppingCart.getPayByVoucherInfo()
									.getVoucherIDList());
						}
					}


					if (AppSysParamsUtil.isVoucherNameValidationEnabled()) {
						ArrayList<String> paxNameList = new ArrayList<String>();
						if (!bookingShoppingCart.getPaxList().isEmpty()) {
							for (ReservationPaxTO reservationPax : bookingShoppingCart.getPaxList()) {
								paxNameList.add(reservationPax.getFirstName() + " " + reservationPax.getLastName());
							}
							String strInfants = request.getParameter("paxInfants");
							if (strInfants!= null && strInfants.length() > 0) {
								JSONArray jsonArray = (JSONArray) new JSONParser().parse(strInfants);
								Iterator<?> iterator = jsonArray.iterator();
								while (iterator.hasNext()) {
									JSONObject jObject = (JSONObject) iterator.next();
									if (jObject.get("displayInfantFirstName").toString() != null && !""
											.equals(jObject.get("displayInfantFirstName").toString().trim())
											&& jObject.get("displayInfantLastName").toString() != null &&
											!"".equals(jObject.get("displayInfantLastName").toString().trim())) {
										paxNameList.add(jObject.get("displayInfantFirstName").toString() + " " + jObject
												.get("displayInfantLastName").toString());
									}
								}
							}
						} else if (passengerList != null) {
							for (LCCClientReservationPax reservationPax : passengerList) {
								paxNameList.add(reservationPax.getFirstName() + " " + reservationPax.getLastName());
							}
						}
						voucherRedeemRequest.setPaxNameList(paxNameList);
					}

					VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
					Voucher voucher = voucherDelegate.getVoucher(voucherRedeemRequest.getVoucherIDList().get(0));
					
					if (voucher != null) {
						if (!balanceToPay.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
							voucherRedeemRequest.setBalanceToPay(balanceToPay.toString());
							PayByVoucherInfo payByVoucherInfo = new PayByVoucherInfo();
	
							if (voucher.getTemplateId() != null && AppSysParamsUtil.isOTPEnabled()
									&& !request.getParameterMap().containsKey("otp") && !onLoad) {

								voucherRedeemResponse = airproxyVoucherBD
										.getVoucherRedeemResponse(voucherRedeemRequest);
								if (voucherRedeemResponse != null) {
									if (voucherRedeemResponse.getOtp() != null || !voucherRedeemResponse.getOtp()
											.isEmpty()) {
										String otpAndVoucherID = (voucherRedeemResponse.getOtp() + "-"
												+ voucherRedeemRequest.getVoucherIDList().get(0)).trim();
										request.getSession().setAttribute("otpAndVoucherID", otpAndVoucherID);
										setMessage(XBEConfig
												.getServerMessage(WebConstants.OTP_GENERATION_SUCCESS, userLanguage, null));
										log.info("VOUCHERID: " + voucher.getVoucherId() + "is to be redeemed. Please enter OTP: "
												+ voucherRedeemResponse.getOtp());										
									}
								} else {
									log.error("Voucher Redeem failed.");
									ModuleException moduleException = new ModuleException(WebConstants.KEY_VOUCHER_ID_INVALID);
									moduleException.setCustumValidateMessage(voucherRedeemRequest.getVoucherIDList().get(0));
									throw moduleException;
								}	
							} else {
								Boolean otpAvailable = false;
								if (voucher.getTemplateId()!=null && AppSysParamsUtil.isOTPEnabled()) {
									String otp = null;
									Map map = request.getParameterMap();
									for (Object key : map.keySet()) {
										String keyStr = (String) key;
										String[] value = (String[]) map.get(keyStr);
										if (keyStr.equals("voucherRedeemRequest.voucherIDList[0]")) {
											voucherId = Arrays.toString(value).replace("[", "").replace("]", "").trim();
										} else {
											if (keyStr.equals("otp")) {
												otp = Arrays.toString(value).replace("[", "").replace("]", "").trim();
											}
										}
									}
	
									otpAvailable = (otp + "-" + voucherId).trim().equalsIgnoreCase(
											request.getSession().getAttribute("otpAndVoucherID").toString());
								}
	
								if (voucher.getTemplateId()!=null && AppSysParamsUtil.isOTPEnabled() && otpAvailable) {
									voucherRedeemRequest.setOTP(request.getParameterMap().get("otp").toString());
								} 
								
								if (!onLoad) {
									voucherRedeemResponse = airproxyVoucherBD.getVoucherRedeemResponse(voucherRedeemRequest);
								}
								setVoucherRedeemResponse(voucherRedeemResponse);
								if (!StringUtil.isNullOrEmpty(voucherRedeemResponse.getOtp())) {
									String otpAndVoucherID = (voucherRedeemResponse.getOtp() + "-" + voucherRedeemRequest
											.getVoucherIDList().get(0)).trim();
									request.getSession().setAttribute("otpAndVoucherID", otpAndVoucherID);
									setMessage(
											XBEConfig.getServerMessage(WebConstants.OTP_GENERATION_SUCCESS, userLanguage, null));
								}
								if (!otpAvailable && voucherRedeemResponse.getVouchersTotal() == null) {
									log.error("Invalid OTP code.");
									ModuleException moduleException = new ModuleException(
											WebConstants.KEY_VOUCHER_OTP_CODE_INVALID);
									throw moduleException;
								}
								BigDecimal voucherTotal = new BigDecimal(voucherRedeemResponse.getVouchersTotal());
	
								if (bookingShoppingCart.getPayByVoucherInfo() != null
										&& bookingShoppingCart.getPayByVoucherInfo().getRedeemedTotal() != null && !onLoad) {
									voucherTotal = bookingShoppingCart.getPayByVoucherInfo().getRedeemedTotal().add(voucherTotal);
									voucherRedeemResponse.setRedeemedTotal(voucherTotal.toString());
								}
								
								if (!onLoad) {
									if (balanceToPay.compareTo(new BigDecimal(voucherRedeemResponse.getVouchersTotal())) > -1) {

										BigDecimal updatedBalanceToPay =AccelAeroCalculator.subtract(AccelAeroCalculator.scaleValueDefault(balanceToPay),
												AccelAeroCalculator.scaleValueDefault(new BigDecimal(
														voucherRedeemResponse.getVouchersTotal()))) ;
										voucherRedeemResponse.setBalanceToPaySelectedCurrency(AccelAeroCalculator.multiplyDefaultScale(
												currencyExchangeRate.getMultiplyingExchangeRate(), updatedBalanceToPay).toString());
										voucherRedeemResponse.setBalTotalPay(updatedBalanceToPay.toString());
									} else {
										voucherRedeemResponse.setBalTotalPay(AccelAeroCalculator.getDefaultBigDecimalZero()
												.toString());
										voucherRedeemResponse.setBalanceToPaySelectedCurrency(AccelAeroCalculator
												.getDefaultBigDecimalZero().toString());
									}

									bookingShoppingCart.getRedeemedVoucherList().add(
											voucherRedeemResponse.getRedeemVoucherList().get(0));
									if (bookingShoppingCart.getPayByVoucherInfo() != null
											&& bookingShoppingCart.getPayByVoucherInfo().getVouchersTotal() != null) {
										voucherRedeemResponse.setVouchersTotal(AccelAeroCalculator.add(
												new BigDecimal(bookingShoppingCart.getPayByVoucherInfo().getVouchersTotal()),
												new BigDecimal(voucherRedeemResponse.getVouchersTotal())).toString());
									}
								} else {
									voucherRedeemResponse.setBalTotalPay(balanceToPay.toString());
								}
									
								payByVoucherInfo.setVoucherDTOList((ArrayList<VoucherDTO>) bookingShoppingCart.getRedeemedVoucherList());
								payByVoucherInfo.setRedeemedTotal(new BigDecimal(voucherRedeemResponse.getRedeemedTotal()));
								payByVoucherInfo.setVouchersTotal(voucherRedeemResponse.getVouchersTotal());
								payByVoucherInfo.setVoucherPaymentOption(voucherRedeemResponse.getVoucherPaymentOption());
								payByVoucherInfo.setBalTotalPay(voucherRedeemResponse.getBalTotalPay());
								bookingShoppingCart.setPayByVoucherInfo(payByVoucherInfo);
								setMessage(XBEConfig
										.getServerMessage(WebConstants.KEY_VOUCHER_REDEEM_SUCCESS, userLanguage, null));
							}
						} else {
							throw new Exception("balance.to.pay.invalid");
						}
					} else {
						log.error("Invalid Voucher ID.");
						ModuleException moduleException = new ModuleException(WebConstants.KEY_VOUCHER_ID_INVALID);
						moduleException.setCustumValidateMessage(voucherRedeemRequest.getVoucherIDList().get(0));
						throw moduleException;
					}
				} catch (ModuleException me) {
					log.error("RedeemVoucherListAction ==> execute()", me);
					if (me.getExceptionCode().equals(WebConstants.KEY_VOUCHER_ID_INVALID)) {
						bookingShoppingCart
								.setInvalidVoucherRedeemAttempts(bookingShoppingCart.getInvalidVoucherRedeemAttempts() + 1);
						setMessage(XBEConfig.getServerMessage(me.getExceptionCode(), userLanguage, null) + " "
								+ me.getCustumValidateMessage());
					} else if (me.getExceptionCode().equals(WebConstants.KEY_VOUCHER_VALID_PERIOD_INVALID)) {
						setMessage(XBEConfig.getServerMessage(me.getExceptionCode(), userLanguage, null));
					} else if (me.getExceptionCode().equals(WebConstants.KEY_VOUCHER_PAX_NAME_INVALID) || me
							.getExceptionCode().equals(WebConstants.KEY_VOUCHER_MOBILE_NUMBER_INVALID)) {
						bookingShoppingCart.setInvalidVoucherRedeemAttempts(
								bookingShoppingCart.getInvalidVoucherRedeemAttempts() + 1);
						setMessage(XBEConfig.getServerMessage(me.getExceptionCode(), userLanguage, null));
					} else {
						setMessage(XBEConfig.getServerMessage(me.getExceptionCode(), userLanguage, null));
					}
					if (bookingShoppingCart.getInvalidVoucherRedeemAttempts() >= 2) {
						disableRedeem = true;
					}
					success = false;
				} catch (Exception ex) {
					log.error("RedeemVoucherListAction ==> execute()", ex);
					setMessage(BasicRH.getErrorMessage(ex, userLanguage));
					success = false;
				}
			}
		}
		return forward;

	}

	public String unblockRedeemedVouchers() {
		String forward = S2Constants.Result.SUCCESS;
		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
				S2Constants.Session_Data.LCC_SES_BOOKING_CART);
		if (bookingShoppingCart.getPayByVoucherInfo() != null) {
			VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
			try {
				voucherDelegate.unblockRedeemedVouchers(bookingShoppingCart.getPayByVoucherInfo().getVoucherIDList());
			} catch (ModuleException ex) {
				log.error("RedeemVoucherListAction ==> unblockRedeemedVouchers()", ex);
			}
		}
		return forward;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the voucherRedeemRequest
	 */
	public VoucherRedeemRequest getVoucherRedeemRequest() {
		return voucherRedeemRequest;
	}

	/**
	 * @param voucherRedeemRequest
	 *            the voucherRedeemRequest to set
	 */
	public void setVoucherRedeemRequest(VoucherRedeemRequest voucherRedeemRequest) {
		this.voucherRedeemRequest = voucherRedeemRequest;
	}

	/**
	 * @return the voucherRedeemResponse
	 */
	public VoucherRedeemResponse getVoucherRedeemResponse() {
		return voucherRedeemResponse;
	}

	/**
	 * @param voucherRedeemResponse
	 *            the voucherRedeemResponse to set
	 */
	public void setVoucherRedeemResponse(VoucherRedeemResponse voucherRedeemResponse) {
		this.voucherRedeemResponse = voucherRedeemResponse;
	}

	/**
	 * @return the selCurrencyCode
	 */
	public String getSelCurrencyCode() {
		return selCurrencyCode;
	}

	/**
	 * @param selCurrencyCode
	 *            the selCurrencyCode to set
	 */
	public void setSelCurrencyCode(String selCurrencyCode) {
		this.selCurrencyCode = selCurrencyCode;
	}

	/**
	 * @return the canceledVouchersAvailable
	 */
	public boolean isCanceledVouchersAvailable() {
		return canceledVouchersAvailable;
	}

	/**
	 * @param canceledVouchersAvailable
	 *            the canceledVouchersAvailable to set
	 */
	public void setCanceledVouchersAvailable(boolean canceledVouchersAvailable) {
		this.canceledVouchersAvailable = canceledVouchersAvailable;
	}

	/**
	 * @return the onLoad
	 */
	public boolean isOnLoad() {
		return onLoad;
	}

	/**
	 * @param onLoad
	 *            the onLoad to set
	 */
	public void setOnLoad(boolean onLoad) {
		this.onLoad = onLoad;
	}

	/**
	 * @return the disableRedeem
	 */
	public boolean isDisableRedeem() {
		return disableRedeem;
	}

	/**
	 * @param disableRedeem
	 *            the disableRedeem to set
	 */
	public void setDisableRedeem(boolean disableRedeem) {
		this.disableRedeem = disableRedeem;
	}

}