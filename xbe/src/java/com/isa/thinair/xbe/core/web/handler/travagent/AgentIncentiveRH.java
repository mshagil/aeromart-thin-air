package com.isa.thinair.xbe.core.web.handler.travagent;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentIncentive;
import com.isa.thinair.airtravelagents.api.model.AgentIncentiveSlab;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.DateUtil;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public final class AgentIncentiveRH extends BasicRH {
	
	private static final String PARAM_MODE = "hdnMode";

	/**
	 * Main Execute Method for Travel Agent & Credit History
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SUCCESS;

		String strMode = request.getParameter(PARAM_MODE);
		if (strMode != null && strMode.equals("SAVE")) {
			try {
				boolean isAllAgentsSaved = saveAgentIncentive(request);
				if (!isAllAgentsSaved) {
					saveMessage(request, "Successfully Updated. Some expired schemes were not saved", WebConstants.MSG_SUCCESS);
					// TODO: Display exact schemes
				} else {
					saveMessage(request, "Successfully Updated.", WebConstants.MSG_SUCCESS);
				}
			} catch (ModuleException me) {
				forward = S2Constants.Result.ERROR;
				saveMessage(request, "Update Fail.", WebConstants.MSG_ERROR);
			} catch (Exception e) {
				forward = S2Constants.Result.ERROR;
				saveMessage(request, "Update Fail.", WebConstants.MSG_ERROR);
			}
		}

		try {
			setDisplayData(request);
		} catch (ModuleException me) {
			forward = S2Constants.Result.ERROR;
			saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
		} catch (Exception e) {
			forward = S2Constants.Result.ERROR;
			saveMessage(request, "Update Fail.", WebConstants.MSG_ERROR);
		}
		return forward;
	}

	private static boolean saveAgentIncentive(HttpServletRequest request) throws ModuleException {
		String selectedIncentives = StringUtil.getNotNullString(request.getParameter("hdnSelctdIncentive"));
		String agentCode = StringUtil.getNotNullString(request.getParameter("hdnAgentCode"));
		Collection<Integer> setIncenID = new HashSet<Integer>();
		String[] arrIncenID = selectedIncentives.split(",");

		for (String incenID : arrIncenID) {
			setIncenID.add(new Integer(incenID));
		}

		return ModuleServiceLocator.getTravelAgentBD().saveAgentIncentive(setIncenID, agentCode);

	}

	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		String incentiveMS = createIncentiveMultiSelect();
		request.setAttribute("reqIncentiveLst", incentiveMS);
		setSelectedIncentivesForAgent(request);
	}

	@SuppressWarnings("unchecked")
	private static void setSelectedIncentivesForAgent(HttpServletRequest request) throws ModuleException {
		String agentCode = StringUtil.getNotNullString(request.getParameter("hdnAgentCode"));
		if (agentCode != null && !agentCode.equals("")) {
			Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(agentCode);
			Set<Integer> existSchemes = agent.getSchemeIDs();
			StringBuilder sb = new StringBuilder();
			if (existSchemes != null && !existSchemes.isEmpty()) {
				sb.append("curentSchemes = '");
				for (Iterator iterator = existSchemes.iterator(); iterator.hasNext();) {
					Integer integer = (Integer) iterator.next();
					sb.append(integer);
					sb.append("'");
					if (iterator.hasNext()) {
						sb.append(",");
					}
				}
				request.setAttribute("reqSelectedIncentives", sb.toString());
			}
		}
	}

	private static String createIncentiveMultiSelect() throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Collection colIncentive = null;
		String strIncenHtml = "";
		final String DATE_FORMAT = "dd/MM/yyyy";

		// strIncenHtml += "var ls = new Listbox('lstSchemes', 'lstAssignedSchemes', 'spnIncentive');";

		colIncentive = ModuleServiceLocator.getTravelAgentBD().createIncentivCodes();

		Iterator ite = (Iterator) colIncentive.iterator();
		sb.append("var schemes = new Array(); ");

		int intc = 0;
		Integer schemeId = null;

		while (ite.hasNext()) {

			AgentIncentive incen = (AgentIncentive) ite.next();
			schemeId = incen.getAgentIncSchemeID();
			Set slbSet = incen.getAgentIncentiveSlabs();

			// Populate the Group Array
			sb.append("schemes[" + intc + "] = new Array('" + incen.getSchemeName() + "','" + incen.getAgentIncSchemeID() + "');");
			sb.append("schemes[" + ++intc + "] = new Array(' ");
			sb.append(DateUtil.formatDate(incen.getEffectiveFrom(), DATE_FORMAT) + " - ");
			sb.append(DateUtil.formatDate(incen.getEffectiveTo()));
			sb.append("','" + incen.getAgentIncSchemeID() + "');");
			if (slbSet != null && !slbSet.isEmpty()) {
				Iterator<AgentIncentiveSlab> iter = slbSet.iterator();
				int cnt = 1;
				while (iter.hasNext()) {
					String slabLbl = "SLAB " + cnt;
					AgentIncentiveSlab slab = iter.next();
					sb.append("schemes[" + ++intc + "] = new Array('");
					sb.append(slabLbl + " " + slab.getRateStartValue());
					sb.append(" - " + slab.getRateEndValue());
					sb.append(" " + slab.getPersentage());
					sb.append(" %','" + incen.getAgentIncSchemeID() + "');");
					cnt++;
				}
			}
			intc++;
		}
		return sb.toString();
	}

}
