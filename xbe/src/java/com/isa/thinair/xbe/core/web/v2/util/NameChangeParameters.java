package com.isa.thinair.xbe.core.web.v2.util;

public class NameChangeParameters {
	public boolean isBeforeBufferTime;
	public boolean isWithinBufferTime;
	public boolean isCreditUtilizedBooking;
	public boolean isFlownSegmentExists;
}