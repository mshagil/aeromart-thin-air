package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.lang.Double;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.aeromart.services.endpoint.adaptor.impls.booking.FlightSegmentAdaptor;
import com.isa.aeromart.services.endpoint.adaptor.utils.AdaptorUtils;
import com.isa.aeromart.services.endpoint.dto.ancillary.integrate.AncillaryIntegratePassengerTO;
import com.isa.aeromart.services.endpoint.dto.session.BookingTransaction;
import com.isa.aeromart.services.endpoint.dto.session.SessionFlightSegment;
import com.isa.aeromart.services.endpoint.dto.session.impl.modification.ReservationInfo;
import com.isa.aeromart.services.endpoint.dto.session.store.PaymentBaseSessionStore;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airpricing.api.dto.CnxModPenServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeAdustment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxCnxModPenChargeForServiceTaxDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForNonTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.utils.converters.ServiceTaxConverterUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ChargeAdjustmentTypeUtil;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountChargesTO;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountCreditTO;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountPaymentsTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.PaxAccountUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;


/**
 * POJO to handle the Load Profile
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class PaxAccountChargesConfirmAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(PaxAccountChargesConfirmAction.class);

	private boolean success = true;

	private String messageTxt;

	private String selAirlineOnd;

	private String txtUN;

	private Boolean selChargeType;

	private String txtAdjustment;

	private String selPax;

	private String groupPNR;

	private String pnr;

	private String version;

	private PaxAccountChargesTO paxAccountChargesTO;

	private PaxAccountPaymentsTO paxAccountPaymentsTO;

	private PaxAccountCreditTO paxAccountCreditTO;

	private String status;

	/** The selected charge adjustment type id. */
	private String selAdjustmentType;
	
	private String serviceTaxMessage;

	@SuppressWarnings("unchecked")
	public String execute() {
		
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			List<LCCClientCarrierOndGroup> colONDs = ReservationUtil.transformJsonONDs(request.getParameter("resONDs"));
			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));
			if (!isGroupPNR) {
				groupPNR = pnr;
			}

			// For multiple seperated with comma ","
			List<String> selectedAirLineONDs = Arrays.asList(selAirlineOnd.split(","));

			// For multiple seperated with comma ","
			List<String> selectedPassengers = Arrays.asList(selPax.split(","));

			String strCurrency = AppSysParamsUtil.getBaseCurrency();
			UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
			String agentCurrency = up.getAgentCurrencyCode();
			List<LCCClientChargeAdustment> chargeAdjustments = new ArrayList<LCCClientChargeAdustment>();
			LCCClientReservationPax pax = null;
			Map<Integer, List<LCCClientExternalChgDTO>> handlingFeeByPax = new HashMap<>();
			boolean applyHandlingFee = false;
			ExternalChgDTO handlingChgDTO = null;			
			BigDecimal totalHandlingFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			
			if (Double.parseDouble(txtAdjustment) > 0) {
				handlingChgDTO = getHandlingFeeExternalChgDTO();
				if (handlingChgDTO != null && handlingChgDTO.getAmount().doubleValue() > 0) {
					totalHandlingFeeAmount = handlingChgDTO.getAmount();
					applyHandlingFee = true;
				}
			}

			
			int handlingDivision = selectedAirLineONDs.size() * getUniquePaxSequence(selectedPassengers).size();
			BigDecimal paxWiseHandlingFee[] = AccelAeroCalculator.roundAndSplit(totalHandlingFeeAmount, handlingDivision);			
			
			int paxCount = 0;
			for (LCCClientCarrierOndGroup carrierONDGroup : colONDs) {
				if (selectedAirLineONDs.contains(carrierONDGroup.getCarrierOndGroupRPH())) {
					for (String passenger : selectedPassengers) {
						if (PaxAccountUtil.isPaxOfCarrier(passenger, carrierONDGroup.getCarrierCode())) {
							LCCClientChargeAdustment chargeAdjustment = new LCCClientChargeAdustment();
							chargeAdjustment.setAmount(new BigDecimal(txtAdjustment));
							chargeAdjustment.setCarrierCode(carrierONDGroup.getCarrierCode());
							chargeAdjustment.setCarrierOndGroupRPH(carrierONDGroup.getCarrierOndGroupRPH());
							chargeAdjustment.setSegmentCode(carrierONDGroup.getSegmentCode());
							chargeAdjustment.setRefundable(selChargeType);
							chargeAdjustment.setTravelerRefNumber(passenger);
							chargeAdjustment.setUserNote(txtUN);
							chargeAdjustment.setAdjustmentTypeId(new Integer(selAdjustmentType));
							chargeAdjustments.add(chargeAdjustment);
							
							if (applyHandlingFee) {
								int paxSequence = PaxTypeUtils.getPaxSeq(passenger);
								LCCClientExternalChgDTO handlingChg = new LCCClientExternalChgDTO();
								handlingChg.setAmount(paxWiseHandlingFee[paxCount]);
								handlingChg.setExternalCharges(EXTERNAL_CHARGES.HANDLING_CHARGE);
								handlingChg.setCode(handlingChgDTO.getChargeCode());
								handlingChg
										.setFlightRefNumber(getFlightRefNumber(colsegs, carrierONDGroup.getCarrierPnrSegIds()));
								handlingChg.setCarrierCode(carrierONDGroup.getCarrierCode());
								handlingChg.setOperationMode(CommonsConstants.ModifyOperation.CHARGE_ADJ.getOperation());

								if (handlingFeeByPax.get(paxSequence) == null) {
									handlingFeeByPax.put(paxSequence, new ArrayList<>());
								}

								handlingFeeByPax.get(paxSequence).add(handlingChg);
							}
							paxCount++;
						}
					}
				}
			}
			
			ServiceTaxQuoteForTicketingRevenueResTO serviceTaxQuoteRS = null;
			
			if(Double.parseDouble(txtAdjustment) > 0){
				serviceTaxQuoteRS = calculateServiceTax(isGroupPNR);
			}
			
			LCCClientReservation reservation = ModuleServiceLocator.getAirproxyReservationBD().adjustGroupCharge(groupPNR,
					version, chargeAdjustments, isGroupPNR, getClientInfoDTO(), getTrackInfoForXBE(), serviceTaxQuoteRS, handlingFeeByPax);
			version = reservation.getVersion();

			/*
			 * Only load the following details if this is not a group adjustment as these are only relevant for single
			 * pax adjustments
			 */
			if (chargeAdjustments.size() == 1) {
				pax = PaxAccountUtil.getSelectedPax(selPax, reservation.getPassengers());
				paxAccountChargesTO = PaxAccountUtil.getChargesDetails(pax, strCurrency, colONDs, isGroupPNR, colsegs,
						agentCurrency, status, reservation.getLastModificationTimestamp(), Boolean.parseBoolean(request.getParameter("isInfantPaymentSeparated")));
				Map<String, String> mapAllAgentCodeDesc = XBEModuleUtils.getGlobalConfig().getAgentsByAgentCode();
				paxAccountPaymentsTO = PaxAccountUtil.getPaymentDetails(pax, status, mapAllAgentCodeDesc, false);
				paxAccountCreditTO = PaxAccountUtil.getCreditDetails(pax);
			}

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String chargeAmountWithServiceTax(){
		
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		
		try {
		
			BigDecimal totalServiceTax = AccelAeroCalculator.getDefaultBigDecimalZero();
			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			BigDecimal totalChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal chargeAmount = new BigDecimal(txtAdjustment);
			BigDecimal totalHandlingFeeAmount = getHandlingFeeAmount();
			
			ServiceTaxQuoteForTicketingRevenueResTO serviceTaxQuoteRS = calculateServiceTax(isGroupPNR);
			
			if(serviceTaxQuoteRS == null){
				serviceTaxMessage = XBEConfig.getClientMessage("cc_resmod_modify_charge_adjestment_service_tax_not_applicable", userLanguage);
				return S2Constants.Result.SUCCESS;
			}
	
			if (serviceTaxQuoteRS.getPaxWiseServiceTaxes() != null
					&& !serviceTaxQuoteRS.getPaxWiseServiceTaxes().isEmpty()) {
				for(List<ServiceTaxTO> serviceTaxTOList : serviceTaxQuoteRS.getPaxWiseServiceTaxes().values()){
					for(ServiceTaxTO serviceTaxTO : serviceTaxTOList){
						totalServiceTax = AccelAeroCalculator.add(totalServiceTax, serviceTaxTO.getAmount());
					}
				}
			}
						
			List<String> selectedPassengers = Arrays.asList(selPax.split(","));
			List<String> selectedAirLineONDs = Arrays.asList(selAirlineOnd.split(","));

			serviceTaxMessage = XBEConfig.getClientMessage("cc_resmod_modify_charge_adjestment", userLanguage);
			
			if(selectedPassengers.size() > 1){
				totalChargeAmount = AccelAeroCalculator.add(totalChargeAmount,
						AccelAeroCalculator.multiply(chargeAmount, new BigDecimal(selectedPassengers.size())));
			}
			
			if(selectedAirLineONDs.size() > 1){
				totalChargeAmount = AccelAeroCalculator.add(totalChargeAmount,
						AccelAeroCalculator.multiply(chargeAmount, new BigDecimal(selectedAirLineONDs.size())));
			}
			
			if(selectedAirLineONDs.size() == 1 && selectedPassengers.size() == 1){
				totalChargeAmount = chargeAmount;
			}
			
			serviceTaxMessage = serviceTaxMessage + " " + AccelAeroCalculator.add(totalServiceTax,totalChargeAmount,totalHandlingFeeAmount)
					+ " " + ((UserPrincipal) request.getUserPrincipal()).getAgentCurrencyCode();

		}catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}
		
		return S2Constants.Result.SUCCESS;
	}
	
	private ServiceTaxQuoteForTicketingRevenueResTO calculateServiceTax(boolean isGroupPNR) throws ModuleException,Exception {
		
		Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS;
		LCCClientReservation reservation = ReservationUtil.loadMinimumReservation(pnr,isGroupPNR,getTrackInfo());
		List<LCCClientCarrierOndGroup> colONDs = ReservationUtil.transformJsonONDs(request.getParameter("resONDs"));
		String carrierCode = "";
		
		if(!isServiceTaxApplicable(reservation.getSegments(), isGroupPNR)){
			return null;
		}

		// Constructing ServiceTax request
		ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = createServiceTaxRequest(isGroupPNR, reservation);

		// For multiple seperated with comma ","
		List<String> selectedAirLineONDs = Arrays.asList(selAirlineOnd.split(","));

		for (LCCClientCarrierOndGroup carrierONDGroup : colONDs) {
			if (selectedAirLineONDs.contains(carrierONDGroup.getCarrierOndGroupRPH())) {
				carrierCode = carrierONDGroup.getCarrierCode();
				break;
			}
		}
	
		serviceTaxQuoteRS = ModuleServiceLocator
				.getAirproxyReservationBD().quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, getTrackInfo());
		
		return serviceTaxQuoteRS.get(carrierCode);
		
	}
	
	
	private ServiceTaxQuoteCriteriaForTktDTO createServiceTaxRequest(boolean isGroupPNR, LCCClientReservation reservation) throws ModuleException,Exception {
		
		Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();
		
		List<LCCClientCarrierOndGroup> colONDs = ReservationUtil.transformJsonONDs(request.getParameter("resONDs"));
		Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
				.getParameter("resSegments"));
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		
		BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
		baseAvailRQ.getAvailPreferences().setAppIndicator(ApplicationEngine.XBE);
		if(isGroupPNR){
			baseAvailRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
		}else{
			baseAvailRQ.getAvailPreferences().setSearchSystem(SYSTEM.AA);
		}
		
		List<FlightSegmentTO> flightSegments = ServiceTaxConverterUtil.adaptFlightSegments(colsegs);
	
		List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
				
				
		// For multiple seperated with comma ","
		List<String> selectedAirLineONDs = Arrays.asList(selAirlineOnd.split(","));

		// For multiple seperated with comma ","
		List<String> selectedPassengers = Arrays.asList(selPax.split(","));

		for (LCCClientCarrierOndGroup carrierONDGroup : colONDs) {
			if (selectedAirLineONDs.contains(carrierONDGroup.getCarrierOndGroupRPH())) {
				
				for(LCCClientReservationSegment flightSegment : colsegs){

					if(carrierONDGroup.getCarrierPnrSegIds().contains(Integer.parseInt(flightSegment.getBookingFlightSegmentRefNumber()))){
						for (String passenger : selectedPassengers) {
							if (PaxAccountUtil.isPaxOfCarrier(passenger, carrierONDGroup.getCarrierCode())) {
								
								LCCClientExternalChgDTO externalChargeDTO = new LCCClientExternalChgDTO();
								LCCClientReservationPax pax = PaxAccountUtil.getSelectedPax(passenger, reservation.getPassengers());
									
								externalChargeDTO.setAmount(new BigDecimal(txtAdjustment));
								externalChargeDTO.setCarrierCode(carrierONDGroup.getCarrierCode());
									
								if (selChargeType) {
									externalChargeDTO.setExternalCharges(EXTERNAL_CHARGES.REF_ADJ);
								} else {
									externalChargeDTO.setExternalCharges(EXTERNAL_CHARGES.NON_REF_ADJ);
								}
								
								externalChargeDTO.setFlightRefNumber(flightSegment.getFlightSegmentRefNumber());
								externalChargeDTO.setSegmentCode(flightSegment.getSegmentCode());
								externalChargeDTO.setSegmentSequence(flightSegment.getSegmentSeq());
								
								if(paxWiseExternalCharges.get(pax.getPaxSequence()) == null){
									paxWiseExternalCharges.put(pax.getPaxSequence(), new ArrayList<LCCClientExternalChgDTO>());
									paxWiseExternalCharges.get(pax.getPaxSequence()).add(externalChargeDTO);
								}else{
									paxWiseExternalCharges.get(pax.getPaxSequence()).add(externalChargeDTO);
								}
								
								paxWisePaxTypes.put(pax.getPaxSequence(), pax.getPaxType());
								
							}
						}
						break;
					}
					
				}
				
				
			}
		}
		
		String taxRegNumber = reservation.getContactInfo().getTaxRegNo();
		
		ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
		
		serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
		serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(null);
		serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegments);
		
		serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(resInfo.getTransactionId());
		serviceTaxQuoteCriteriaDTO.setPaxState(reservation.getContactInfo().getState());
		serviceTaxQuoteCriteriaDTO.setPaxCountryCode(reservation.getContactInfo().getCountryCode());
		serviceTaxQuoteCriteriaDTO.setPaxTaxRegistrationNo(taxRegNumber);

		serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
		serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
		serviceTaxQuoteCriteriaDTO.setCalculateOnlyForExternalCharges(true);
		serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered((taxRegNumber != null && !taxRegNumber.isEmpty()) ? true : false);
		
		return serviceTaxQuoteCriteriaDTO;
		
	}
	
	private boolean isServiceTaxApplicable(Set<LCCClientReservationSegment> resSegments, boolean isGroupPNR){
		
		if(resSegments != null){
			
			String originCountry = "";
			LCCClientReservationSegment firstSegment = ReservationUtil.getFirstFlightSeg(resSegments);
			String[] countryCodes = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
			String originCode = "";
			
			if (firstSegment != null && firstSegment.getSegmentCode() != null && firstSegment.getSegmentCode().length() > 2) {
				originCode = firstSegment.getSegmentCode().split("/")[0];
			}
			
			if(originCode.isEmpty()){
				return false;
			}
			
			if (isGroupPNR){
				try {
					Map<String, CachedAirportDTO> mapAirports = ModuleServiceLocator.getAirportBD()
							.getCachedAllAirportMap(new ArrayList<>(Arrays.asList(originCode)));
					originCountry = mapAirports.get(originCode) != null ? mapAirports.get(originCode).getCountryCode() : "";

				} catch (ModuleException e) {
					log.error("ERROR in loading country code for the origin airport", e);
					return false;
				}
			}else{
					Object[] airportInfo = XBEModuleUtils.getGlobalConfig().retrieveAirportInfo(firstSegment.getSegmentCode().split("/")[0]);
					if (airportInfo != null && airportInfo.length > 2) {
						originCountry = (String) airportInfo[3];
					}
			}
			
			for(String countryCode : countryCodes){
				if(originCountry.equals(countryCode)){
					return true;
				}
			}

		}

		return false;
		
	}
	
	private ExternalChgDTO getHandlingFeeExternalChgDTO() throws ModuleException {
		int operationMode = CommonsConstants.ModifyOperation.CHARGE_ADJ.getOperation();
		return ModuleServiceLocator.getReservationBD().getHandlingFeeForModification(operationMode);

	}

	private BigDecimal getHandlingFeeAmount() throws ModuleException {
		BigDecimal totalHandlingFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		ExternalChgDTO handlingChgDTO = getHandlingFeeExternalChgDTO();
		if (handlingChgDTO != null && handlingChgDTO.getAmount().doubleValue() > 0) {
			totalHandlingFeeAmount = handlingChgDTO.getAmount();
		}
		return totalHandlingFeeAmount;

	}

	private String getFlightRefNumber(Collection<LCCClientReservationSegment> colsegs, Collection<Integer> carrierPnrSegIds) {

		if (colsegs != null && !colsegs.isEmpty() && carrierPnrSegIds != null && !carrierPnrSegIds.isEmpty()) {
			for (LCCClientReservationSegment flightSegment : colsegs) {

				if (carrierPnrSegIds.contains(Integer.parseInt(flightSegment.getBookingFlightSegmentRefNumber()))) {
					return flightSegment.getFlightSegmentRefNumber();
				}

			}
		}

		return null;
	}

	public String chargeAmountWithHandlingFee() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);

		try {

			BigDecimal totalHandlingFeeAmount = getHandlingFeeAmount();
			BigDecimal totalChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal chargeAmount = new BigDecimal(txtAdjustment);

			List<String> selectedPassengers = Arrays.asList(selPax.split(","));
			List<String> selectedAirLineONDs = Arrays.asList(selAirlineOnd.split(","));

			serviceTaxMessage = XBEConfig.getClientMessage("cc_resmod_modify_charge_adj_with_handling_fee", userLanguage);

			int paxCount = getUniquePaxSequence(selectedPassengers).size();

			if (paxCount > 1) {
				totalChargeAmount = AccelAeroCalculator.add(totalChargeAmount,
						AccelAeroCalculator.multiply(chargeAmount, new BigDecimal(paxCount)));
			}

			if (selectedAirLineONDs.size() > 1) {
				totalChargeAmount = AccelAeroCalculator.add(totalChargeAmount,
						AccelAeroCalculator.multiply(chargeAmount, new BigDecimal(selectedAirLineONDs.size())));
			}

			if (selectedAirLineONDs.size() == 1 && paxCount == 1) {
				totalChargeAmount = chargeAmount;
			}

			serviceTaxMessage = serviceTaxMessage + " " + AccelAeroCalculator.add(totalHandlingFeeAmount, totalChargeAmount) + " "
					+ ((UserPrincipal) request.getUserPrincipal()).getAgentCurrencyCode();

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}
	
	
	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setSelAirlineOnd(String selAirlineOnd) {
		this.selAirlineOnd = selAirlineOnd;
	}

	public void setTxtUN(String txtUN) {
		this.txtUN = txtUN;
	}

	public void setSelChargeType(Boolean selChargeType) {
		this.selChargeType = selChargeType;
	}

	public void setTxtAdjustment(String txtAdjustment) {
		this.txtAdjustment = txtAdjustment;
	}

	public void setSelPax(String selPax) {
		this.selPax = selPax;
	}

	public PaxAccountChargesTO getPaxAccountChargesTO() {
		return paxAccountChargesTO;
	}

	public String getSelPax() {
		return selPax;
	}

	public PaxAccountPaymentsTO getPaxAccountPaymentsTO() {
		return paxAccountPaymentsTO;
	}

	public void setPaxAccountPaymentsTO(PaxAccountPaymentsTO paxAccountPaymentsTO) {
		this.paxAccountPaymentsTO = paxAccountPaymentsTO;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public PaxAccountCreditTO getPaxAccountCreditTO() {
		return paxAccountCreditTO;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSelAdjustmentType() {
		return selAdjustmentType;
	}

	public void setSelAdjustmentType(String selAdjustmentType) {
		this.selAdjustmentType = selAdjustmentType;
	}

	public String getServiceTaxMessage() {
		return serviceTaxMessage;
	}
	
	protected TrackInfoDTO getTrackInfoForXBE() {
		TrackInfoDTO trackInfoDTO = getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		return trackInfoDTO;
	}
	
	private Collection<Integer> getUniquePaxSequence(List<String> selectedPassengers) {
		Set<Integer> uniquePaxSeq = new HashSet<>();
		if (selectedPassengers != null && !selectedPassengers.isEmpty()) {
			for (String passenger : selectedPassengers) {
				if (!StringUtil.isNullOrEmpty(passenger)) {
					uniquePaxSeq.add(PaxTypeUtils.getPaxSeq(passenger));
				}
			}
		}
		return uniquePaxSeq;

	}
}
