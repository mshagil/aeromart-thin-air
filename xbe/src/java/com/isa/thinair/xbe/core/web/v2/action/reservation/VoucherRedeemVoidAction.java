package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;


import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.dto.VoucherRedeemResponse;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * @author chethiya
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class VoucherRedeemVoidAction extends BaseRequestAwareAction {

	private Log log = LogFactory.getLog(VoucherRedeemVoidAction.class);

	private VoucherRedeemResponse voidRedeemResponse;

	private String selCurrencyCode;

	private String message;

	private boolean success = true;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		
		if (AppSysParamsUtil.isVoucherEnabled()) {
			voidRedeemResponse = new VoucherRedeemResponse();
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(new Date());
			try {
				BigDecimal balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
						.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, getTrackInfo().getOriginAgentCode())) {
					if (bookingShoppingCart.getReservationBalance() == null) {
						balanceToPay = bookingShoppingCart.getTotalPayableIncludingCCCharges();
					} else {
						balanceToPay = bookingShoppingCart.getTotalPayable();
					}
				} else {
					balanceToPay = bookingShoppingCart.getTotalPayable();
				}

				List<LCCClientReservationPax> passengerList = null;

				if (balanceToPay.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
					balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
					Collection<LCCClientReservationPax> colPaxes = ReservationUtil
							.transformJsonPassengers(request.getParameter("resPax"));
					Set<LCCClientReservationPax> passengers = new HashSet<LCCClientReservationPax>(colPaxes);

					passengerList = new ArrayList<LCCClientReservationPax>(passengers);
					for (LCCClientReservationPax pax : passengerList) {
						if (pax.getTotalAvailableBalance()
								.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
							balanceToPay = AccelAeroCalculator.add(balanceToPay, pax.getTotalAvailableBalance());
						}
					}
				}

				CurrencyExchangeRate currencyExchangeRate = null;

				if ((bookingShoppingCart.getSelectedCurrencyTimestamp() == null
						|| bookingShoppingCart.getCurrencyExchangeRate() == null) && selCurrencyCode != null) {
					currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrencyCode,
							ApplicationEngine.XBE);
					bookingShoppingCart.setCurrencyExchangeRate(currencyExchangeRate);
				} else {
					currencyExchangeRate = bookingShoppingCart.getCurrencyExchangeRate();
				}

				Currency currency = currencyExchangeRate.getCurrency();
				balanceToPay = AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
						balanceToPay, currency.getBoundryValue(), currency.getBreakPoint());

				if (StringUtil.isNullOrEmpty(selCurrencyCode)) {
					selCurrencyCode = currency.getCurrencyCode();
				}

				VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
				if (bookingShoppingCart.getPayByVoucherInfo() != null) {
					voucherDelegate
							.unblockRedeemedVouchers(bookingShoppingCart.getPayByVoucherInfo().getVoucherIDList());
				}

				voidRedeemResponse.setVouchersTotal(String.valueOf(AccelAeroCalculator.getDefaultBigDecimalZero()));
				voidRedeemResponse.setRedeemedTotal(String.valueOf(AccelAeroCalculator.getDefaultBigDecimalZero()));
				voidRedeemResponse.setBalTotalPay(String.valueOf(balanceToPay));
				bookingShoppingCart.setPayByVoucherInfo(null);
				bookingShoppingCart.getRedeemedVoucherList().clear();

			} catch (ModuleException me) {
				log.error("VoucherRedeemVoidAction ==> execute()", me);
				setMessage(BasicRH.getErrorMessage(me, userLanguage));
				success = false;
			} catch (Exception ex) {
				log.error("VoucherRedeemVoidAction ==> execute()", ex);
				setMessage(BasicRH.getErrorMessage(ex, userLanguage));
				success = false;
			}

		}
		return forward;

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getSelCurrencyCode() {
		return selCurrencyCode;
	}

	public void setSelCurrencyCode(String selCurrencyCode) {
		this.selCurrencyCode = selCurrencyCode;
	}

	public VoucherRedeemResponse getVoidRedeemResponse() {
		return voidRedeemResponse;
	}

	public void setVoidRedeemResponse(VoucherRedeemResponse voidRedeemResponse) {
		this.voidRedeemResponse = voidRedeemResponse;
	}

}