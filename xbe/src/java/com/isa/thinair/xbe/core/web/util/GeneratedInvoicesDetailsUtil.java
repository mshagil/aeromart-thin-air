/**
 * 
 */
package com.isa.thinair.xbe.core.web.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceGenerationStatusDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

/**
 * Utility class to get invoice details
 * 
 * @author Noshani Perera
 * 
 */
public class GeneratedInvoicesDetailsUtil {

	private static final String strSMDateFormat = "dd/MM/yyyy";

	public static String getInvoiceDetails(HttpServletRequest request) throws ModuleException {

		InvoiceGenerationStatusDTO invGeneratedStsDto = new InvoiceGenerationStatusDTO();
		Page page = null;
		Collection colInvoices = null;
		int recNo = 0;
		int totRec = 0;
		String strHdnMode = request.getParameter("hdnMode");

		SearchInvoicesDTO searchInvoicesDTO = getSearchInvoiceDTO(request);

		if (request.getParameter("hdnRecNo") != null && !("".equals(request.getParameter("hdnRecNo")))) {
			recNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}

		if (strHdnMode != null && !strHdnMode.equals("AGENTS")) {

			invGeneratedStsDto = ModuleServiceLocator.getInvoicingBD().getGeneratedInvoicesDetails(searchInvoicesDTO, recNo, 20);

			if (invGeneratedStsDto != null) {
				page = invGeneratedStsDto.getInvoices();

			}

			if (page != null) {
				colInvoices = page.getPageData();
				totRec = page.getTotalNoOfRecords();

			}
		}
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, new Integer(totRec).toString());
		return createInvoiceRowHtml(colInvoices);
	}

	public static String getTotalInvoiceDetails(HttpServletRequest request) throws ModuleException {

		InvoiceGenerationStatusDTO invGeneratedStsDto = new InvoiceGenerationStatusDTO();
		Page page = null;
		Collection colInvoices = null;
		int recNo = 0;
		int totRec = 0;
		String strHdnMode = request.getParameter("hdnMode");

		SearchInvoicesDTO searchInvoicesDTO = getSearchInvoiceDTO(request);

		if (request.getParameter("hdnRecNo") != null && !("".equals(request.getParameter("hdnRecNo")))) {
			recNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}

		if (strHdnMode != null && !strHdnMode.equals("AGENTS")) {

			invGeneratedStsDto = ModuleServiceLocator.getInvoicingBD().getGeneratedInvoicesDetails(searchInvoicesDTO, recNo, 20);

			if (invGeneratedStsDto != null) {
				page = invGeneratedStsDto.getInvoices();

			}
			if (page != null) {

				totRec = page.getTotalNoOfRecords();

			}

			invGeneratedStsDto = ModuleServiceLocator.getInvoicingBD().getGeneratedInvoicesDetails(searchInvoicesDTO, recNo,
					totRec);
			if (invGeneratedStsDto != null) {
				page = invGeneratedStsDto.getInvoices();
			}
			if (page != null) {
				colInvoices = page.getPageData();

			}
		}
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, new Integer(totRec).toString());
		return createTotalInvoiceRowHtml(colInvoices);
	}

	private static SearchInvoicesDTO getSearchInvoiceDTO(HttpServletRequest request) {

		String strHdnMode = request.getParameter("hdnMode");
		String strYear = request.getParameter("selYear1");
		String strMonth = request.getParameter("selMonth1");
		String strBilingPeriod = request.getParameter("selBPStart");
		String strAgent = request.getParameter("hdnAgents");
		String strZero = request.getParameter("chkZero1");
		String strAgentType = request.getParameter("selAgencies");

		String strYearTo = request.getParameter("selYear2");
		String strMonthTo = request.getParameter("selMonth2");
		String strBilingPeriodTo = request.getParameter("selBPEnd");

		int recNo = 0;
		int totRec = 0;

		SearchInvoicesDTO srcinvDto = new SearchInvoicesDTO();

		Collection<String> agentcol = new ArrayList<String>();

		if (request.getParameter("hdnRecNo") != null && !("".equals(request.getParameter("hdnRecNo")))) {
			recNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}

		if (strHdnMode != null && !strHdnMode.equals("AGENTS")) {

			if (strYear != null && !strYear.trim().equals("")) {
				srcinvDto.setYear(Integer.parseInt(strYear));
			}
			if (strYearTo != null && !strYearTo.trim().equals("")) {
				srcinvDto.setYearTo(Integer.parseInt(strYearTo));
			}
			if (strMonth != null && !strMonth.trim().equals("")) {
				srcinvDto.setMonth(Integer.parseInt(strMonth));
			}
			if (strMonthTo != null && !strMonthTo.trim().equals("")) {
				srcinvDto.setMonthTo(Integer.parseInt(strMonthTo));
			}
			if (strBilingPeriod != null) {
				srcinvDto.setInvoicePeriod(Integer.parseInt(strBilingPeriod));
			}
			if (strBilingPeriodTo != null) {
				srcinvDto.setInvoicePeriodTo(Integer.parseInt(strBilingPeriodTo));
			}
			if (strAgent != null && !strAgent.equals("")) {
				String[] agentList = strAgent.split(",");
				for (int j = 0; j < agentList.length; j++) {
					agentcol.add(agentList[j]);
				}

				srcinvDto.setAgentCodes(agentcol);
			} else if (strAgentType != null && strAgentType.equals("All")) {
				srcinvDto.setAgentCodes(null);

			}

			if (strZero != null && strZero.equals("ZERO")) {
				srcinvDto.setFilterZeroInvoices(true);
			}
		}

		return srcinvDto;
	}

	/**
	 * Create the Grid Array from a Collection of Invoices
	 * 
	 * @param invoices
	 *            the Collection of GeneratedInvoiceDTOs
	 * @return String the Created Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static String createInvoiceRowHtml(Collection invoices) throws ModuleException {
		StringBuffer sb = new StringBuffer("var invoiceData = new Array();");
		GeneratedInvoiceDTO genInvDTO = null;
		if (invoices != null) {
			List list = (List) invoices;
			Object[] listArr = list.toArray();

			for (int i = 0; i < listArr.length; i++) {
				genInvDTO = (GeneratedInvoiceDTO) listArr[i];
				sb.append("invoiceData[" + i + "] = new Array();");
				sb.append("invoiceData[" + i + "][0] = '" + i + "';");
				sb.append("invoiceData[" + i + "][1] = '" + genInvDTO.getAgentCode() + "';");
				sb.append("invoiceData[" + i + "][2] = '" + genInvDTO.getAgentName() + "';");
				sb.append("invoiceData[" + i + "][3] = '" + genInvDTO.getInvoiceNumber() + "';");
				sb.append("invoiceData[" + i + "][4] = '" + genInvDTO.getBillingPeriod() + "';");
				sb.append("invoiceData[" + i + "][5] = '" + genInvDTO.getInvoiceAmount() + "';");
				sb.append("invoiceData[" + i + "][6] = '" + formatDate(genInvDTO.getInvoiceDate(), strSMDateFormat) + "';");
				sb.append("invoiceData[" + i + "][7] = '" + getTransferStatus(genInvDTO.getTransferedStatus()) + "';");
				sb.append("invoiceData[" + i + "][8] = '" + genInvDTO.getSettledAmount() + "';");
				BigDecimal balance = AccelAeroCalculator.subtract(genInvDTO.getInvoiceAmount(), genInvDTO.getSettledAmount());
				sb.append("invoiceData[" + i + "][9] = '" + balance + "';");
				sb.append("invoiceData[" + i + "][10] = '';");
				sb.append("invoiceData[" + i + "][11] = '" + genInvDTO.getBillingEmailAddress() + "';");

			}
		}
		return sb.toString();
	}

	/**
	 * Create the Grid Array from a Collection of Invoices
	 * 
	 * @param invoices
	 *            the Collection of GeneratedInvoiceDTOs
	 * @return String the Created Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static String createTotalInvoiceRowHtml(Collection invoices) throws ModuleException {
		StringBuffer sb = new StringBuffer("var totalInvoiceData = new Array();");
		GeneratedInvoiceDTO genInvDTO = null;
		if (invoices != null) {
			List list = (List) invoices;
			Object[] listArr = list.toArray();

			for (int i = 0; i < listArr.length; i++) {
				genInvDTO = (GeneratedInvoiceDTO) listArr[i];
				sb.append("totalInvoiceData[" + i + "] = new Array();");
				sb.append("totalInvoiceData[" + i + "][0] = '" + i + "';");
				sb.append("totalInvoiceData[" + i + "][1] = '" + genInvDTO.getAgentCode() + "';");
				sb.append("totalInvoiceData[" + i + "][2] = '" + genInvDTO.getAgentName() + "';");
				sb.append("totalInvoiceData[" + i + "][3] = '" + genInvDTO.getInvoiceNumber() + "';");
				sb.append("totalInvoiceData[" + i + "][4] = '" + genInvDTO.getBillingPeriod() + "';");
				sb.append("totalInvoiceData[" + i + "][5] = '" + genInvDTO.getInvoiceAmount() + "';");
				sb.append("totalInvoiceData[" + i + "][6] = '" + formatDate(genInvDTO.getInvoiceDate(), strSMDateFormat) + "';");
				sb.append("totalInvoiceData[" + i + "][7] = '" + getTransferStatus(genInvDTO.getTransferedStatus()) + "';");
				sb.append("totalInvoiceData[" + i + "][8] = '" + genInvDTO.getSettledAmount() + "';");
				BigDecimal balance = AccelAeroCalculator.subtract(genInvDTO.getInvoiceAmount(), genInvDTO.getSettledAmount());
				sb.append("totalInvoiceData[" + i + "][9] = '" + balance + "';");
				sb.append("totalInvoiceData[" + i + "][10] = '';");
				sb.append("totalInvoiceData[" + i + "][11] = '" + genInvDTO.getBillingEmailAddress() + "';");

			}
		}
		return sb.toString();
	}

	/**
	 * Formats the Date to a Given Format
	 * 
	 * @param utilDate
	 *            the Date
	 * @param fmt
	 *            the String Format
	 * @return String the Formatted date
	 */
	private static String formatDate(Date utilDate, String fmt) {
		String strDate = "";
		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(fmt);
			strDate = sdFmt.format(utilDate);
		}
		return strDate;
	}

	/**
	 * Gets the Trasfering Status
	 * 
	 * @param str
	 *            the string
	 * @return String the Status
	 */
	private static String getTransferStatus(String str) {
		if (str.equals("P")) {
			return "InPrgress";
		}
		return (str.equals("Y")) ? "Transfered" : "Not Transfered";
	}

}
