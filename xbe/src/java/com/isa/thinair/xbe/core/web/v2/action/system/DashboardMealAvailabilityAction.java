package com.isa.thinair.xbe.core.web.v2.action.system;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentMealsDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealResponceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class DashboardMealAvailabilityAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(DashboardMealAvailabilityAction.class);

	private String flightNumber;
	private String classOfService;
	private String logicalCabinClass;
	private String departureDate;
	private LCCMealResponceDTO lccMealResponceDTO;
	private List<LCCMealDTO> meals = new ArrayList<LCCMealDTO>();
	private boolean success = true;
	private String messageTxt;
	
	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			String strTxnIdntifier = null;
			SYSTEM system = null;
			List<FlightSegmentTO> flightSegmentTOs = null;
			Map<String, Set<String>> fltRefWiseSelectedMeal = null;
			List<BundledFareDTO> bundledFareDTOs = null;
			FlightSegement flightSegment = null;
            flightNumber = flightNumber.toUpperCase();
			String carrierCode = flightNumber.substring(0, 2);

			if (ModuleServiceLocator.getGlobalConfig().getOwnAirlineCarrierCodes().contains(carrierCode)) {
				system = SYSTEM.AA;
				flightSegment = ModuleServiceLocator.getFlightServiceBD().getFlightSegmentForLocalDate(flightNumber,
						parseToDate(departureDate));
			} else if (ModuleServiceLocator.getGlobalConfig().getInterlineCarriers().contains(carrierCode)) {
				system = SYSTEM.INT;
				flightSegment = AirproxyModuleUtils.getLCCFlightServiceBD().getFlightSegment(flightNumber,
						parseToDate(departureDate));
				if(flightSegment.getFltSegId() == 0){
					throw new Exception("um.dashboard.meal.flight.does.not.exist");	
				}
			} else {
				throw new Exception("um.dashboard.meal.data.invalid");
			}

			if (flightSegment != null) {

				FlightSegmentTO flightSegmentTO = new FlightSegmentTO(flightSegment);
				flightSegmentTO.setLogicalCabinClassCode(logicalCabinClass);
				flightSegmentTO.setCabinClassCode(classOfService);
				flightSegmentTO.setOperatingAirline(flightNumber.substring(0, 2));
				flightSegmentTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTO));
				flightSegmentTOs = new ArrayList<FlightSegmentTO>();
				flightSegmentTOs.add(flightSegmentTO);
				String pnr = null;
				List<LCCMealRequestDTO> lccMealRequestDTOs = composeMealRequest(flightSegmentTOs, classOfService, strTxnIdntifier);
				lccMealResponceDTO = ModuleServiceLocator.getAirproxyAncillaryBD().getAvailableMeals(lccMealRequestDTOs,
						TrackInfoUtil.getBasicTrackInfo(request), fltRefWiseSelectedMeal, bundledFareDTOs, system,
						strTxnIdntifier, ApplicationEngine.XBE, pnr);

				for (LCCFlightSegmentMealsDTO lccFlightSegmentMealsDTO : lccMealResponceDTO.getFlightSegmentMeals()) {
					meals.addAll(lccFlightSegmentMealsDTO.getMeals());
				}

				if (meals.isEmpty()) {
					throw new Exception("um.dashboard.meal.not.avail");
				}

			} else {
				throw new Exception("um.dashboard.meal.flight.does.not.exist");
			}

		} catch (Exception e) {
			success = false;
			if ("um.dashboard.meal.data.invalid".equals(e.getMessage())) {
				messageTxt = XBEConfig.getClientMessage(e.getMessage(), userLanguage);
			} else if ("um.dashboard.meal.flight.does.not.exist".equals(e.getMessage())) {
				messageTxt = XBEConfig.getClientMessage(e.getMessage(), userLanguage);
			} else if ("um.dashboard.meal.not.avail".equals(e.getMessage())) {
				messageTxt = XBEConfig.getClientMessage(e.getMessage(), userLanguage);
			} else {
				messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			}
			log.error(messageTxt, e);
		}
		return S2Constants.Result.SUCCESS;
	}

	private List<LCCMealRequestDTO> composeMealRequest(List<FlightSegmentTO> flightSegTO, String cabinClass, String txnId) {
		List<LCCMealRequestDTO> mL = new ArrayList<LCCMealRequestDTO>();
		for (FlightSegmentTO fst : flightSegTO) {
			LCCMealRequestDTO mrd = new LCCMealRequestDTO();
			mrd.setTransactionIdentifier(txnId);
			if (cabinClass != null) {
				mrd.setCabinClass(cabinClass);
			}
			mrd.getFlightSegment().add(fst);
			mL.add(mrd);
		}
		return mL;
	}

	private Date parseToDate(String strDate) throws Exception {
		Date date = null;
		SimpleDateFormat dateFormat = null;

		if (strDate.indexOf('-') != -1)
			dateFormat = new SimpleDateFormat("dd-MM-yy");
		if (strDate.indexOf('/') != -1)
			dateFormat = new SimpleDateFormat("dd/MM/yy");
		try {
			date = dateFormat.parse(strDate);
		} catch (Exception e) {
			log.error(e);
			throw new Exception("Invalid Departure Date");
		}

		return date;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getClassOfService() {
		return classOfService;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public List<LCCMealDTO> getMeals() {
		return meals;
	}

	public void setMeals(List<LCCMealDTO> meals) {
		this.meals = meals;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

}
