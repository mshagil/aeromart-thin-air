package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SubStationUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.SystemParams;
import com.isa.thinair.xbe.api.dto.v2.ModifySegmentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.v2.util.AirportDDListGenerator;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reservation.V2_MODIFY_SEGMENT),
		@Result(name = S2Constants.Result.REQUOTE, value = S2Constants.Jsp.Reservation.MODIFY_RES_REQUOTE),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ModifySegmentAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ModifySegmentAction.class);

	public String execute() {
		try {

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));
			ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo.getReservationStatus(), colsegs,
					resInfo.isModifiableReservation(), false, resInfo.getLccPromotionInfoTO(), null, false, false);
			if (resInfo.getInterlineModificationParams() != null && resInfo.getInterlineModificationParams().size() > 0) {
				rpParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo.getReservationStatus(), colsegs, resInfo.isModifiableReservation(),
						resInfo.getLccPromotionInfoTO());
			}
			request.setAttribute("initialParams", JSONUtil.serialize(rpParams));
			Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getModifyingSegments(colsegs,
					request.getParameter("fltSegment"), rpParams, true);
			String depSegment = null;
			String arrSegment = null;
			Date departureDate = null;
			Date arrivalDate = null;
			Date confirmBeforeDate = null;
			Date departureDateZulu = null;
			String strCabinclass = null;
			String strCabinclassORTN = null;
			Date firstDepartureDate = null;
			Date lastArrivalDate = null;
			Date firstDepartureDateZulu = null;
			Date lastArrivalDateZulu = null;
			Date OpenReturnTravelExpiry = null;

			for (LCCClientReservationSegment seg : colsegs) {
				for (LCCClientReservationSegment intModSeg : colModSegs) {
					if (intModSeg.getBookingFlightSegmentRefNumber().equals(seg.getBookingFlightSegmentRefNumber())) {
						if (departureDateZulu == null || departureDateZulu.after(seg.getDepartureDateZulu())) {
							departureDate = seg.getDepartureDate();
							depSegment = seg.getSegmentCode();
							strCabinclass = seg.getCabinClassCode();
							departureDateZulu = seg.getDepartureDateZulu();
						}
						if (arrivalDate == null || arrivalDate.compareTo(seg.getArrivalDateZulu()) <= 0) {
							arrivalDate = seg.getArrivalDateZulu();
							arrSegment = seg.getSegmentCode();
						}
						// set confirm before date
						if (seg.getModifyTillBufferDateTime() != null
								&& (confirmBeforeDate == null || confirmBeforeDate.after(seg.getModifyTillBufferDateTime()))) {
							confirmBeforeDate = seg.getModifyTillBufferDateTime();
						}
					} else {
						if (request.getParameter("pgMode").equals("confirmOprt")
								&& seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
							strCabinclassORTN = seg.getCabinClassCode();
						}
					}
				}

				if (seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					if (firstDepartureDateZulu == null || firstDepartureDateZulu.after(seg.getDepartureDateZulu())) {
						firstDepartureDate = seg.getDepartureDate();
						firstDepartureDateZulu = seg.getDepartureDateZulu();
					}
					if (lastArrivalDateZulu == null || lastArrivalDateZulu.before(seg.getArrivalDateZulu())) {
						lastArrivalDate = seg.getArrivalDate();
						lastArrivalDateZulu = seg.getArrivalDateZulu();
					}
					if (seg.isOpenReturnSegment()) {
						SimpleDateFormat simpleDate = new SimpleDateFormat("dd-MM-yyyy");
						OpenReturnTravelExpiry = simpleDate.parse(seg.getOpenReturnTravelExpiry().split(" ")[0]);
					}
					if (seg.isOpenReturnSegment()) {
						SimpleDateFormat simpleDate = new SimpleDateFormat("dd-MM-yyyy");
						OpenReturnTravelExpiry = simpleDate.parse(seg.getOpenReturnTravelExpiry().split(" ")[0]);
					}
				}
			}

			Date ticketExpireDate = captureTicketExpiryDate(colsegs, colModSegs);
			ticketExpireDate = ticketExpireDate != null ? ticketExpireDate : confirmBeforeDate;

			String depAirport = depSegment.split("/")[0];
			String[] arrSegmentCodes = arrSegment.split("/");
			String arrAirport = arrSegmentCodes[arrSegmentCodes.length - 1];
			String strFirstDepdate = DateFormatUtils.format(firstDepartureDate, "yyyy-MM-dd HH:mm:ss.S");
			String strLastArrdate = DateFormatUtils.format(lastArrivalDate, "yyyy-MM-dd HH:mm:ss.S");
			String strDepdate = DateFormatUtils.format(departureDate, "dd/MM/yyyy");
			if (request.getParameter("pgMode").equals("confirmOprt")) {
				strDepdate = DateFormatUtils.format(OpenReturnTravelExpiry, "dd/MM/yyyy");
				strLastArrdate = DateFormatUtils.format(OpenReturnTravelExpiry, "yyyy-MM-dd HH:mm:ss.S");
			}
			boolean isModifySurfaceSegment = AddModifySurfaceSegmentValidations.isModifyingSurfaceSegment(depAirport, arrAirport);

			String strConfirmBeforeDate = null;
			if (confirmBeforeDate != null) {
				strConfirmBeforeDate = DateFormatUtils.format(confirmBeforeDate, "yyyy-MM-dd HH:mm:ss.S");
			}

			ModifySegmentTO modifySegmets = new ModifySegmentTO();
			modifySegmets.setRequote(AppSysParamsUtil.isRequoteEnabled());
			modifySegmets.setDepartureDate(strDepdate);
			modifySegmets.setFrom(depAirport);
			modifySegmets.setTo(arrAirport);
			modifySegmets.setModifyingSegments(request.getParameter("fltSegment"));
			modifySegmets.setMode(request.getParameter("pgMode"));
			modifySegmets.setPNR(request.getParameter("pnrNo"));
			modifySegmets.setGroupPNR(request.getParameter("groupPNRNo"));

			if (request.getParameter("pgMode").equals("confirmOprt")) {
				modifySegmets.setBookingclass(strCabinclassORTN);
			} else {
				modifySegmets.setBookingclass(strCabinclass);
			}

			modifySegmets.setBookingType(request.getParameter("bookingType"));
			modifySegmets.setNoOfAdults(new Integer(request.getParameter("resNoOfAdults")));
			modifySegmets.setNoOfInfants(new Integer(request.getParameter("resNoOfInfants")));
			modifySegmets.setNoOfChildren(new Integer(request.getParameter("resNoOfchildren")));
			modifySegmets.setVersion(request.getParameter("version"));
			modifySegmets.setOwnerAgentCode(request.getParameter("resOwnerAgent"));
			modifySegmets.setFirstDepature(strFirstDepdate);
			modifySegmets.setLastArrival(strLastArrdate);
			modifySegmets.setStatus(request.getParameter("modifyStatus"));
			modifySegmets.setModifyingSurfaceSegment(isModifySurfaceSegment);
			// modifySegmets.setSelectedCurrency(selectedCurrency);
			/* segment modification */
			modifySegmets.setIsAllowModifyByDate(new Boolean(request.getParameter("allowModifyDate")));
			modifySegmets.setIsAllowModifyByRoute(new Boolean(request.getParameter("allowModifyRoute")));

			modifySegmets.setConfirmBeforeDate(strConfirmBeforeDate);

			modifySegmets.setTicketExpiryEnabled(new Boolean(request.getParameter("ticketExpiryEnabled")));
			modifySegmets.setTicketExpiryDate(request.getParameter("ticketExpiryDate"));
			modifySegmets.setTicketValidFromDate(request.getParameter("ticketValidFrom"));

			request.setAttribute("resHasInsurance", request.getParameter("resHasInsurance"));
			if (request.getParameter("pgMode").equals("confirmOprt")) {
				request.setAttribute("modifySegs", JSONUtil.serialize(modifySegmets));
			} else if (AppSysParamsUtil.isRequoteEnabled()) {
				request.setAttribute("reservationInfo", JSONUtil.serialize(modifySegmets));
			} else {
				request.setAttribute("modifySegs", JSONUtil.serialize(modifySegmets));
			}

			request.setAttribute("resSegments", request.getParameter("resSegments"));
			request.setAttribute("resPaxs", request.getParameter("resPax").replaceAll("'", "\\\\'"));
			request.setAttribute("resContactInfo", request.getParameter("resContactInfo"));
			request.setAttribute("resPaySummary", request.getParameter("resPaySummary"));
			request.setAttribute("reqPayAgents", ReservationUtil.getPaymentAgentOptions(request));
			request.setAttribute("flexiAlertInfo", request.getParameter("flexiAlertInfo"));
			request.setAttribute("modifyFlexiBooking", request.getParameter("modifyFlexiBooking"));
			request.setAttribute("serviceTaxLabel", AppSysParamsUtil.getServiceTaxLabel());

			SystemParams sysParams = new SystemParams();
			sysParams.setMinimumTransitionTimeInMillis(AppSysParamsUtil.getMinimumReturnTransitTimeInMillis());
			sysParams.setDefaultLanguage(XBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_LANGUAGE));
			sysParams.setDynamicOndListPopulationEnabled(AppSysParamsUtil.isDynamicOndListPopulationEnabled());
			sysParams.setUserDefineTransitTimeEnabled(AppSysParamsUtil.isEnableUserDefineTransitTime());

			request.setAttribute("systemParams", JSONUtil.serialize(sysParams));
			request.setAttribute("carrierCode", AppSysParamsUtil.getDefaultCarrierCode());
			request.setAttribute("allowedGroundStations",
					this.prepareGroundSegmentData(depAirport, arrAirport, isModifySurfaceSegment));
			request.setAttribute("excludableCharges", JSONUtil.serialize(ReservationUtil.getExcludableChargesMap()));

			if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
				request.setAttribute("sysOndSource", AppSysParamsUtil.getDynamicOndListUrl());
			}

		} catch (Exception e) {
			log.error("ERROR in LCC Modify", e);
			return S2Constants.Result.ERROR;
		}
		if (request.getParameter("pgMode").equals("confirmOprt")) {
			return S2Constants.Result.SUCCESS;
		} else if (AppSysParamsUtil.isRequoteEnabled()) {
			return S2Constants.Result.REQUOTE;
		} else {
			return S2Constants.Result.SUCCESS;
		}
	}

	private Date captureTicketExpiryDate(Collection<LCCClientReservationSegment> colsegs,
			Collection<LCCClientReservationSegment> colModSegs) {

		if (AppSysParamsUtil.isTicketValidityEnabled()) {
			Collection<Date> validites = new ArrayList<Date>();
			for (LCCClientReservationSegment seg : colsegs) {
				if (seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					if (seg.getTicketValidTill() != null) {
						validites.add(seg.getTicketValidTill());
					}
				}
			}

			return BeanUtils.getFirstElement(validites);
		}

		return null;
	}

	private String prepareGroundSegmentData(String origin, String destination, boolean isModifySurfaceSegment)
			throws JSONException, ModuleException {
		StringBuilder newGroundStations = new StringBuilder();

		String strOnd = origin + "/" + destination;
		List[] arrList = new List[] { null, null };
		if (isModifySurfaceSegment) {
			arrList = SubStationUtil.getGroundSegmentModifyCombo(strOnd);
		}
		newGroundStations.append(AirportDDListGenerator.prepareAirportsHtml(request, arrList[0], "arrGroundSegmentParentFrom",
				isModifySurfaceSegment));
		newGroundStations.append(AirportDDListGenerator.prepareAirportsHtml(request, arrList[1], "arrGroundSegmentParentTo",
				isModifySurfaceSegment));

		return newGroundStations.toString();
	}

}
