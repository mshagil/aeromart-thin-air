package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.auditor.api.dto.PassengerTicketCouponAuditDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketChangeOfStatusRQ;
import com.isa.thinair.gdsservices.api.model.GdsEvent;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.ETicketSegTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.PaxAccountUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the ETicket Mask Details
 * 
 * @author M.Rikaz
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadPaxETicketMaskAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(LoadPaxAccountDetailsAction.class);

	private boolean success = true;

	private String messageTxt;

	private String pnrPaxId;

	private String travelerRefNum;

	private String passengerName;

	private String remark;

	private String pnr;

	private Collection<ETicketSegTO> passengerTicketCoupons;
	
	private Collection<LccClientPassengerEticketInfoTO> modifyingPassengerTicketCoupons;

	private Collection<LccClientPassengerEticketInfoTO> updatedPassengerTicketCoupons;

	private ETicketSegTO modifiedCoupon;
	
	private boolean enableCpnCtrl;
	
	private String groupPnr;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			@SuppressWarnings("unchecked")
			Collection<LccClientPassengerEticketInfoTO> colPassenegrTktCoupons = JSONFETOParser.parseCollection(Collection.class,
					LccClientPassengerEticketInfoTO.class, request.getParameter("ticketCoupons"));
			if (PaxAccountUtil.getPnrPaxID(travelerRefNum) != null) {
				this.pnrPaxId = PaxAccountUtil.getPnrPaxID(travelerRefNum).toString();
			}
			this.passengerTicketCoupons = buildPassengerEticketDetail(colPassenegrTktCoupons,
					request.getParameter("eTicketStatus"), request.getParameter("segmentStatus"));

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public String saveModifiedCoupon() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			
			@SuppressWarnings("unchecked")
			Collection<LccClientPassengerEticketInfoTO> colPassenegrTktCoupons = JSONFETOParser.parseCollection(Collection.class,
					LccClientPassengerEticketInfoTO.class, request.getParameter("ticketCoupons"));

			ReservationProcessParams rparam = new ReservationProcessParams(request, null, null, true, true, null, null, false, false);
			LccClientPassengerEticketInfoTO paxEticketInfo = this.getModifiedPassengerCoupon(colPassenegrTktCoupons);
			PassengerTicketCouponAuditDTO couponAuditDetail = this.getCouponAuditDetail();
			PassengerCouponUpdateRQ passengerCoupon = new PassengerCouponUpdateRQ();
			passengerCoupon.setCouponAudit(couponAuditDetail);
			passengerCoupon.setCarrierCode(paxEticketInfo.getCarrierCode());
			passengerCoupon.setEticketId(paxEticketInfo.getEticketId());
			passengerCoupon.setEticketStatus(modifiedCoupon.getPaxETicketStatus());
			passengerCoupon.setPaxStatus(modifiedCoupon.getPaxStatus());
			
			if (AppSysParamsUtil.isGroundServiceEnabled()) {
				Map<String, String> busAirCarrierCodes = ModuleServiceLocator.getFlightServiceBD().getBusAirCarrierCodes();

				if (busAirCarrierCodes != null && busAirCarrierCodes.size() > 0
						&& busAirCarrierCodes.containsKey(passengerCoupon.getCarrierCode())) {
					passengerCoupon.setCarrierCode(busAirCarrierCodes.get(passengerCoupon.getCarrierCode()));
				}
			}

			ModuleServiceLocator.getAirproxyPassengerBD().updatePassengerCoupon(passengerCoupon, this.getTrackInfo(),
					rparam.isAllowExceptionalEticketCouponModifications());

			this.updateCouponDetails(colPassenegrTktCoupons);

			this.updatedPassengerTicketCoupons = colPassenegrTktCoupons;

		} catch (ModuleException e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}
		return S2Constants.Result.SUCCESS;

	}
	
	public String saveGroupModifiedCoupon() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		ReservationProcessParams rparam = new ReservationProcessParams(request, null, null, true, true, null, null, false, false);

		LccClientPassengerEticketInfoTO updatedPassengerEticketInfoTO = null;
		updatedPassengerTicketCoupons = new ArrayList<LccClientPassengerEticketInfoTO>();

		List<PassengerCouponUpdateRQ> groupPaxCouponUpdateRQ = new ArrayList<>();

		// create passengerCoupon list
		if (modifyingPassengerTicketCoupons != null && !modifyingPassengerTicketCoupons.isEmpty()
				&& ((modifiedCoupon.getPaxETicketStatus() != null && !modifiedCoupon.getPaxETicketStatus().isEmpty())
						|| (modifiedCoupon.getPaxStatus() != null && !modifiedCoupon.getPaxStatus().isEmpty()))) {
			try {
				for (LccClientPassengerEticketInfoTO lccClientPassengerEticketInfoTO : modifyingPassengerTicketCoupons) {
					PassengerTicketCouponAuditDTO couponAuditDetail = new PassengerTicketCouponAuditDTO();
					couponAuditDetail.seteTicketId(lccClientPassengerEticketInfoTO.getEticketId());
					couponAuditDetail.setPassengerName(lccClientPassengerEticketInfoTO.getPassengerName());
					couponAuditDetail.setPnr(pnr);
					couponAuditDetail.setPnrPaxId(
							PaxAccountUtil.getPnrPaxID(lccClientPassengerEticketInfoTO.getTravelerRefNumber()).toString());
					couponAuditDetail.setRemark(remark);

					PassengerCouponUpdateRQ passengerCoupon = new PassengerCouponUpdateRQ();
					passengerCoupon.setCouponAudit(couponAuditDetail);
					passengerCoupon.setCarrierCode(lccClientPassengerEticketInfoTO.getCarrierCode());
					passengerCoupon.setEticketId(lccClientPassengerEticketInfoTO.getEticketId());
					passengerCoupon.setEticketStatus(
							(modifiedCoupon.getPaxETicketStatus() != null && !modifiedCoupon.getPaxETicketStatus().isEmpty())
									? modifiedCoupon.getPaxETicketStatus()
									: lccClientPassengerEticketInfoTO.getPaxETicketStatus());
					passengerCoupon
							.setPaxStatus((modifiedCoupon.getPaxStatus() != null && !modifiedCoupon.getPaxStatus().isEmpty())
									? modifiedCoupon.getPaxStatus() : lccClientPassengerEticketInfoTO.getPaxStatus());

					if (AppSysParamsUtil.isGroundServiceEnabled()) {
						Map<String, String> busAirCarrierCodes = ModuleServiceLocator.getFlightServiceBD()
								.getBusAirCarrierCodes();

						if (busAirCarrierCodes != null && busAirCarrierCodes.size() > 0
								&& busAirCarrierCodes.containsKey(passengerCoupon.getCarrierCode())) {
							passengerCoupon.setCarrierCode(busAirCarrierCodes.get(passengerCoupon.getCarrierCode()));
						}
					}

					// ModuleServiceLocator.getAirproxyPassengerBD().updatePassengerCoupon(passengerCoupon,
					// this.getTrackInfo(),
					// rparam.isAllowExceptionalEticketCouponModifications());

					updatedPassengerEticketInfoTO = lccClientPassengerEticketInfoTO;
					updatedPassengerEticketInfoTO.setPaxETicketStatus(modifiedCoupon.getPaxETicketStatus());
					if (modifiedCoupon.getPaxETicketStatus() != null && !modifiedCoupon.getPaxETicketStatus().isEmpty()) {
						updatedPassengerEticketInfoTO.setPaxETicketStatus(modifiedCoupon.getPaxETicketStatus());

					}
					if (modifiedCoupon.getPaxStatus() != null && !modifiedCoupon.getPaxStatus().isEmpty()) {
						updatedPassengerEticketInfoTO.setPaxStatus(modifiedCoupon.getPaxStatus());
					}
					updatedPassengerTicketCoupons.add(updatedPassengerEticketInfoTO);

					groupPaxCouponUpdateRQ.add(passengerCoupon);
				}

				ModuleServiceLocator.getAirproxyPassengerBD().updateGroupPassengerCoupon(groupPaxCouponUpdateRQ,
						this.getTrackInfo(), rparam.isAllowExceptionalEticketCouponModifications(), ReservationUtil.isGroupPnr(getGroupPnr()));

				success = true;
				messageTxt = BasicRH.getErrorMessage(new ModuleException("airreservations.eticket.success.all"), userLanguage);

			} catch (ModuleException e) {
				success = false;
				messageTxt = BasicRH.getErrorMessage(e, userLanguage);
				log.error(messageTxt, e);
			}

		} else {
			success = false;
			messageTxt = BasicRH.getErrorMessage(new ModuleException("airreservations.eticket.invalid.empty"), userLanguage);
		}

		return S2Constants.Result.SUCCESS;
	}

	/**
	 * @param coupons
	 * @throws ModuleException 
	 */
	private void updateCouponDetails(Collection<LccClientPassengerEticketInfoTO> coupons) throws ModuleException {
		
		 Map<Integer, String> couponStatus = new HashMap<Integer, String>();
		 
		for (LccClientPassengerEticketInfoTO passengerCoupon : coupons) {
			if (passengerCoupon.getPpfsId().equals(modifiedCoupon.getPpfsId())
					&& passengerCoupon.getCarrierCode().equals(modifiedCoupon.getCarrierCode())) {
				if (passengerCoupon.getEticketId().intValue() == modifiedCoupon.getEticketId().intValue()) {
					// set updated coupon status
					passengerCoupon.setPaxETicketStatus(modifiedCoupon.getPaxETicketStatus());
				}
				passengerCoupon.setPaxStatus(modifiedCoupon.getPaxStatus());
				
				if(ReservationInternalConstants.PfsPaxStatus.FLOWN.equals(modifiedCoupon.getPaxStatus())){
					couponStatus.put(modifiedCoupon.getPpfsId(), null);
				}
			}
		}
		if (!couponStatus.isEmpty()) {
			TicketChangeOfStatusRQ ticketChangeOfStatusRQ = new TicketChangeOfStatusRQ();
			ticketChangeOfStatusRQ.setOverrideStatus(false);
			ticketChangeOfStatusRQ.setRequestType(GdsEvent.Request.CHANGE_OF_STATUS_TO_FINAL);
			ticketChangeOfStatusRQ.setCouponStatus(couponStatus);
			ReservationModuleUtils.getGDSNotifyBD().changeCouponStatus(ticketChangeOfStatusRQ);
		}
	}

	/**
	 * @return
	 * @throws ModuleException
	 */
	private PassengerTicketCouponAuditDTO getCouponAuditDetail() throws ModuleException {
		PassengerTicketCouponAuditDTO couponAuditDetail = new PassengerTicketCouponAuditDTO();

		couponAuditDetail.setPnr(pnr);
		couponAuditDetail.setPnrPaxId(PaxAccountUtil.getPnrPaxID(travelerRefNum).toString());
		couponAuditDetail.setPassengerName(passengerName);
		couponAuditDetail.setRemark(remark);

		return couponAuditDetail;
	}

	/**
	 * @param coupons
	 */
	private LccClientPassengerEticketInfoTO getModifiedPassengerCoupon(Collection<LccClientPassengerEticketInfoTO> coupons) {

		for (LccClientPassengerEticketInfoTO coupon : coupons) {
			if (coupon.getEticketId().intValue() == modifiedCoupon.getEticketId().intValue()
					&& coupon.getCarrierCode().equals(modifiedCoupon.getCarrierCode())) {
				return coupon;
			}
		}
		return null;
	}

	/**
	 * @param passenger
	 * @return
	 */
	private Collection<ETicketSegTO> buildPassengerEticketDetail(Collection<LccClientPassengerEticketInfoTO> coupons,
			String eticketStatus, String segmentStatus) throws ModuleException {

		List<ETicketSegTO> eTckMaskColl = new ArrayList<ETicketSegTO>();
		coupons = removeInternallyExchangedETs(coupons);

		for (LccClientPassengerEticketInfoTO lccPaxEticket : coupons) {
			if (lccPaxEticket.getTravelerRefNumber().equals(this.getTravelerRefNum())) {
				// filter by segment status
				if ((!segmentStatus.isEmpty() && lccPaxEticket.getSegmentStatus().equals(segmentStatus))
						|| segmentStatus.isEmpty()) {

					// filter by e ticket status
					if ((!eticketStatus.isEmpty() && lccPaxEticket.getPaxETicketStatus().equals(eticketStatus))
							|| eticketStatus.isEmpty()) {
						ETicketSegTO eTicketMaskSegTO = new ETicketSegTO();
						eTicketMaskSegTO.setEticketId(lccPaxEticket.getEticketId());
						eTicketMaskSegTO.setPpfsId(lccPaxEticket.getPpfsId());
						eTicketMaskSegTO.setSegmentCode(lccPaxEticket.getSegmentCode());
						eTicketMaskSegTO.setSegmentStatus(lccPaxEticket.getSegmentStatus());
						eTicketMaskSegTO.setFlightNo(lccPaxEticket.getFlightNo());
						eTicketMaskSegTO.setCarrierCode(lccPaxEticket.getCarrierCode());
						eTicketMaskSegTO.setDepartureDateTime(lccPaxEticket.getDepartureDate());
						eTicketMaskSegTO.setDepartureTime(DateUtil.formatDate(lccPaxEticket.getDepartureDate(),
								"dd/MM/yyyy HH:mm"));
						eTicketMaskSegTO.setPaxETicketStatus(lccPaxEticket.getPaxETicketStatus());
						eTicketMaskSegTO.setPaxStatus(lccPaxEticket.getPaxStatus());
						if (lccPaxEticket.getExternalPaxETicketNo() != null
								&& lccPaxEticket.getExternalPaxETicketNo().length() > 0
								&& lccPaxEticket.getExternalCouponNo() != null) {
							eTicketMaskSegTO.setPaxETicketNo(lccPaxEticket.getExternalPaxETicketNo());
							eTicketMaskSegTO.setCouponNo(String.valueOf(lccPaxEticket.getExternalCouponNo()));
							if (!StringUtil.isNullOrEmpty(lccPaxEticket.getExternalCouponStatus())) {
								eTicketMaskSegTO.setPaxETicketStatus(lccPaxEticket.getExternalCouponStatus());
							}
							if (!StringUtil.isNullOrEmpty(lccPaxEticket.getExternalCouponControl())) {
								eTicketMaskSegTO.setCouponControl(lccPaxEticket.getExternalCouponControl());
								this.enableCpnCtrl = true;
							}
						} else {
							eTicketMaskSegTO.setPaxETicketNo(lccPaxEticket.getPaxETicketNo());
							eTicketMaskSegTO.setCouponNo(String.valueOf(lccPaxEticket.getCouponNo()));
						}

						eTckMaskColl.add(eTicketMaskSegTO);
					}
				}
			}
		}
		Collections.sort(eTckMaskColl);
		return eTckMaskColl;
	}
	
	private List<LccClientPassengerEticketInfoTO> removeInternallyExchangedETs(Collection<LccClientPassengerEticketInfoTO> eTckMaskColl) {
		List<LccClientPassengerEticketInfoTO> etSegsWithoutInternallyExchangedETs = new ArrayList<LccClientPassengerEticketInfoTO>();
		List<Integer> exchangedEticketIds = new ArrayList<Integer>();
		for (LccClientPassengerEticketInfoTO etSeg : eTckMaskColl) {
			for (LccClientPassengerEticketInfoTO et : eTckMaskColl) {
				if (!etSeg.getEticketId().equals(et.getEticketId()) && etSeg.getPaxETicketNo().equals(et.getPaxETicketNo())
						&& etSeg.getCouponNo().equals(et.getCouponNo())) {

					if (etSeg.getPaxETicketStatus().equals(EticketStatus.EXCHANGED.code())
							&& (!etSeg.getPaxETicketStatus().equals(EticketStatus.EXCHANGED.code()) || !exchangedEticketIds
									.contains(et.getEticketId()))) {
						exchangedEticketIds.add(etSeg.getEticketId());
					}
				}
			}
		}
		for (LccClientPassengerEticketInfoTO etSeg : eTckMaskColl) {
			if (!exchangedEticketIds.contains(etSeg.getEticketId())) {
				etSegsWithoutInternallyExchangedETs.add(etSeg);
			}
		}
		return etSegsWithoutInternallyExchangedETs;
	}

	public Collection<ETicketSegTO> getPassengerTicketCoupons() {
		return passengerTicketCoupons;
	}

	public void setPassengerTicketCoupons(Collection<ETicketSegTO> passengerTicketCoupons) {
		this.passengerTicketCoupons = passengerTicketCoupons;
	}

	public Collection<LccClientPassengerEticketInfoTO> getUpdatedPassengerTicketCoupons() {
		return updatedPassengerTicketCoupons;
	}

	public void setUpdatedPassengerTicketCoupons(Collection<LccClientPassengerEticketInfoTO> updatedPassengerTicketCoupons) {
		this.updatedPassengerTicketCoupons = updatedPassengerTicketCoupons;
	}

	public ETicketSegTO getModifiedCoupon() {
		return modifiedCoupon;
	}

	public void setModifiedCoupon(ETicketSegTO modifiedCoupon) {
		this.modifiedCoupon = modifiedCoupon;
	}

	public String getTravelerRefNum() {
		return travelerRefNum;
	}

	public void setTravelerRefNum(String travelerRefNum) {
		this.travelerRefNum = travelerRefNum;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(String pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public boolean isEnableCpnCtrl() {
		return enableCpnCtrl;
	}

	public void setEnableCpnCtrl(boolean enableCpnCtrl) {
		this.enableCpnCtrl = enableCpnCtrl;
	}

	public Collection<LccClientPassengerEticketInfoTO> getModifyingPassengerTicketCoupons() {
		return modifyingPassengerTicketCoupons;
	}

	public void setModifyingPassengerTicketCoupons(Collection<LccClientPassengerEticketInfoTO> modifyingPassengerTicketCoupons) {
		this.modifyingPassengerTicketCoupons = modifyingPassengerTicketCoupons;
	}

	public String getGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(String groupPnr) {
		this.groupPnr = groupPnr;
	}	
		
}
