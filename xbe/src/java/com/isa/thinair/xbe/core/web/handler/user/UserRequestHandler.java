package com.isa.thinair.xbe.core.web.handler.user;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.user.UserHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class UserRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(UserRequestHandler.class);

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_LOGIN_ID = "txtLoginId";
	private static final String PARAM_PASSWORD = "pwdPassword";
	private static final String PARAM_PREVPASSWORD = "hdnPassword";
	private static final String PARAM_FIRST_NAME = "txtFirstName"; // Display
	// Name
	private static final String PARAM_EMAIL = "txtEmail";
	private static final String PARAM_TRAVEL_AGENT = "selTravelAgent";
	private static final String PARAM_ASSIGN_ROLES = "hdnRoleValues";
	private static final String PARAM_STATUS = "chkStatus";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_EDITABLE = "hdnEditable";
	private static final String PARAM_HDNACTION = "hdnAction";
	private static final String PARAM_RECNO = "hdnRecNo";
	private static final String PARAM_SEARCHDATA = "hdnSearchData";
	private static final String PARAM_GRIDDATA = "hdnGridData";
	private static final String PARAM_SEARCHID = "txtLoginIdSearch";
	private static final String PARAM_SEARCHAIRLINE = "hdnSearchAirline";
	private static final String PARAM_SEARCHNAME = "txtLastNameSearch";
	private static final String PARAM_SEARCHAIRPORT = "selAirportSearch";
	private static final String PARAM_SEARCHAGENT = "selTravelAgentSearch";
	private static final String PARAM_CARRIER_CODES = "hdnCarrierCode";
	private static final String PARAM_DEF_CARRIER = "selDefCarrier";
	private static final String PARAM_SERVICE_CHANNEL = "selServiceChannel";
	private static final String PARAM_USER_THEME = "selTheme";
	private static final String PARAM_FFP_NUMBER = "txtFFPNumber";
	private static final String PARAM_APPLY_ROLE_CHANGE = "chkRoleChange";
	private static final String PARAM_MYID_CARRIER = "txtMyIDCarrier";

	private static final String PARAM_ADMIN_REPORTING = "chkAUReporting";
	private static final String PARAM_ADMIN_OWN = "chkAUOwner";
	private static final String PARAM_ADMIN_ANY = "chkAUAny";
	private static final String PARAM_NORM_REPORTING = "chkOUReporting";
	private static final String PARAM_NORM_OWN = "chkOUOwner";
	private static final String PARAM_NORM_ANY = "chkOUAny";

	private static final String PARAM_DRY_USER_ENABLE = "hdnDryUserEnable";

	private static final String PARAM_SERV_CH_JS = "reqServChJS";
	private static final String PARAM_SEARCHSTATUS = "selSearchStatus";
	private static final String PARAM_MAX_ADULT_ALLOWED = "maxAdultAllowed";

	private static XBEConfig xbeConfig = new XBEConfig();
	
	private static boolean isGSAV2Enabled = AppSysParamsUtil.isGSAStructureVersion2Enabled();

	/**
	 * Method to Check Agents Validity
	 *
	 * @param userId    the User Id
	 * @param agentCode the Agent Code
	 * @return int Validity 0- Success 1- The changed Station inactive 2- The changed Agent inactive
	 * @throws ModuleException the ModuleException
	 */
	private static int checkValidStationAgent(String userId, String agentCode, String strAssignedRoleIDs,
			HttpServletRequest request) throws ModuleException {

		User user = null;
		Agent agent = null;

		if (userId != null)
			user = ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId);
		if (agentCode != null)
			agent = ModuleServiceLocator.getTravelAgentBD().getAgent(agentCode);

		if (agent == null)
			return 2;

		if (AppSysParamsUtil.isGSAStructureVersion2Enabled() && user != null
				&& !AppSysParamsUtil.getCarrierAgent().equals(((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode())
				&& !agentCode.equals(user.getAgentCode())) {
			return 3;
		}

		if (agent.getStatus().equals(Agent.STATUS_INACTIVE)) {
			if (user == null)
				return 2;
			else {
				Agent agentofUser = ModuleServiceLocator.getTravelAgentBD().getAgent(user.getAgentCode());
				if (agentofUser == null)
					return 2;
				if (!agentofUser.getAgentCode().equals(agent.getAgentCode()))
					return 2;
			}
		}
		return 0;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	/**
	 * Main Execute Method for User Action
	 *
	 * @param request the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter("hdnMode");
		String strHdnAction = request.getParameter("hdnAction");
		String forward = S2Constants.Result.SUCCESS;
		String defaultAirlineCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode();
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);

		String strFormData = "var saveSuccess = 0; ";
		strFormData += "var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		if (strHdnAction != null && strHdnAction.equals("LANDING")) {
			setRequestHtml(request);
			forward = S2Constants.Result.SHOW_LANDING;
		} else if (strHdnMode != null && strHdnMode.equals("CLOSE")) {
			setClose(request);
			forward = S2Constants.Result.SHOW_DUMMY;
		} else {
			StringBuffer scBuff = new StringBuffer();
			if (hasPrivilege(request, WebConstants.PRIV_SYS_SECURITY_ROLE_SERVCH_VIEW)) {
				scBuff.append("isServCh = true;");
			}
			if (hasPrivilege(request, WebConstants.PRIV_SYS_SECURITY_ROLE_SERVCH_EDIT)) {
				scBuff.append("isServChEditable = true;");
			}
			if (hasPrivilege(request, WebConstants.PRIV_SYS_SECURITY_USER_MYIDTRAVEL)) {
				scBuff.append("setMyIDCarrier = true;");
			}
			if (hasPrivilege(request, WebConstants.PRIV_SYS_SECURITY_USER_FFP)) {
				scBuff.append("isFFPMaintainable = true;");
			} else {
				scBuff.append("isFFPMaintainable = false;");
			}
			setAttribInRequest(request, PARAM_SERV_CH_JS, scBuff.toString());
			try {

				if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {

					if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
						checkPrivilege(request, WebConstants.PRIV_SYS_SECURITY_USER_EDIT);
						setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
					} else {
						checkPrivilege(request, WebConstants.PRIV_SYS_SECURITY_USER_ADD);
						setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
					}

					Properties props = getProperties(request);
					String strAgent = props.getProperty(PARAM_TRAVEL_AGENT).split(",")[0];
					int validity = checkValidStationAgent(props.getProperty(PARAM_LOGIN_ID), strAgent,
							props.getProperty(PARAM_ASSIGN_ROLES), request);

					switch (validity) {
					// case 1:
					case 2:
						// Agent is invalid
						strFormData = fillEnteredFormData(request);
						strFormData += " var saveSuccess = 3;"; // Agent invalid
						// error
						// notification
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
						saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.inactive", userLanguage),
								WebConstants.MSG_ERROR);
						break;
					case 3:
						// GSA V2
						strFormData = fillEnteredFormData(request);
						strFormData += " var saveSuccess = 3;";
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
						saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.user.agent.change.restricted", userLanguage),
								WebConstants.MSG_ERROR);
						break;
					case 4:
						// not a TA
						strFormData = fillEnteredFormData(request);
						strFormData += " var saveSuccess = 3;";
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
						saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.roleInvalid", userLanguage),
								WebConstants.MSG_ERROR);
						break;
					}
					if (validity == 0) {
						// Station and Agent of user were validated successfuly
						saveData(props, request);
						if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
							saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_UPDATE_SUCCESS, userLanguage),
									WebConstants.MSG_SUCCESS);

							setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
						} else {
							saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS, userLanguage),
									WebConstants.MSG_SUCCESS);
							setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
						}
						strFormData = "var saveSuccess = 1; ";
						strFormData += "var arrFormData = new Array();";
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
					} else {
						if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
							setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
						} else {
							setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
						}

					}

				}
			} catch (ModuleException moduleException) {
				log.error("Exception in UserRequestHandler.execute() [origin module=" + moduleException.getModuleDesc()
						+ "]", moduleException);
				if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
					saveMessage(request, xbeConfig.getMessage("cc.user.form.id.dupkey", userLanguage),
							WebConstants.MSG_ERROR);
				} else {
					saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
				}
				if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
				} else {
					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
				}
				// isExceptionOccured = true;
				setExceptionOccured(request, true);

				try {
					strFormData = fillEnteredFormData(request);
				} catch (ModuleException me) {
					log.error("Exception in UserRequestHandler.execute() [origin module=" + me.getModuleDesc() + "]",
							me);
					saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
				}

				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			} catch (Exception exception) {
				log.error("Exception in UserRequestHandler.execute()", exception);
				if (exception instanceof RuntimeException) {
					forward = S2Constants.Result.ERROR;
					JavaScriptGenerator.setServerError(request, exception.getMessage(), "", "");
				}
				setExceptionOccured(request, true);
				if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
				} else {
					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
				}
				try {
					strFormData = fillEnteredFormData(request);
				} catch (ModuleException me) {
					log.error("Exception in UserRequestHandler.execute() [origin module=" + me.getModuleDesc() + "]",
							me);
					saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
				}

				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			}

			try {
				if (strHdnMode != null && strHdnMode.equalsIgnoreCase(WebConstants.ACTION_DELETE)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_SECURITY_USER_DELETE);

					String strUserId = request.getParameter(PARAM_LOGIN_ID);
					if (strUserId != null && strUserId.length() > 0) {
						strUserId = defaultAirlineCode + strUserId;
					}
					if (strUserId != null) {
						User existingUser = ModuleServiceLocator.getSecurityBD().getUser(strUserId);
						if (existingUser == null)
							ModuleServiceLocator.getSecurityBD().removeUser(strUserId);
						else {
							String strVersion = request.getParameter(PARAM_VERSION);
							if (strVersion != null && !"".equals(strVersion)) {
								existingUser.setVersion(Long.parseLong(strVersion));
								ModuleServiceLocator.getSecurityBD().removeUser(existingUser);
							} else {
								ModuleServiceLocator.getSecurityBD().removeUser(strUserId);
							}
						}

						saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS, userLanguage),
								WebConstants.MSG_SUCCESS);
						setExceptionOccured(request, false);

					}
				}

			} catch (ModuleException moduleException) {
				log.error("Exception in UserRequestHandler.execute() [origin module=" + moduleException.getModuleDesc()
						+ "]", moduleException);
				setExceptionOccured(request, true);
				if (moduleException.getMessageString()
						.equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
					saveMessage(request, xbeConfig.getMessage("cc.airadmin.childrecord", userLanguage),
							WebConstants.MSG_ERROR);
				} else {
					saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
				}
				if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
				} else {
					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
				}
			} catch (Exception exception) {
				log.error("Exception in UserRequestHandler.execute()", exception);
				setExceptionOccured(request, true);
				if (exception instanceof RuntimeException) {
					forward = S2Constants.Result.ERROR;
					JavaScriptGenerator.setServerError(request, exception.getMessage(), "", "");
				}
				if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
				} else {
					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
				}
			}

			try {
				if (strHdnMode != null && strHdnMode.equalsIgnoreCase(WebConstants.ACTION_EDIT_FFP)) {
					String strUserId = request.getParameter(PARAM_LOGIN_ID);
					if (strUserId != null && strUserId.length() > 0) {
						strUserId = defaultAirlineCode + strUserId;
					}
					String ffpNumber = request.getParameter(PARAM_FFP_NUMBER);
					if (ffpNumber == null) {
						forward = S2Constants.Result.ERROR;
						return forward;
					} else if (ffpNumber.length() > 15) {
						forward = S2Constants.Result.ERROR;
						return forward;
					}
					if (strUserId != null) {
						User existingUser = ModuleServiceLocator.getSecurityBD().getUser(strUserId);
						if (checkFfpAccesability(request, existingUser)) {
							if (existingUser != null) {
								existingUser.setFfpNumber(ffpNumber.trim());
								existingUser.setPassword(null);
								existingUser.setLccPublishStatus(Util.LCC_TOBE_REPUBLISHED);

								ModuleServiceLocator.getSecurityBD().saveUser(existingUser);
								if (AppSysParamsUtil.isLCCDataSyncEnabled()) {
									User savedUser = ModuleServiceLocator.getSecurityBD()
											.getUser(existingUser.getUserId());
									ModuleServiceLocator.getCommonServiceBD().publishSpecificUserDataUpdate(savedUser);
								}
								saveMessage(request,
										xbeConfig.getMessage(WebConstants.KEY_FFP_EDIT_SUCCESS, userLanguage),
										WebConstants.MSG_SUCCESS);
								setExceptionOccured(request, false);
							}

						}

					}
				}
			} catch (ModuleException moduleException) {
				setExceptionOccured(request, true);
				if (moduleException.getExceptionCode().equals(WebConstants.KEY_FFP_EIDT_NOTAUTHORIZED)) {
					saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_FFP_EIDT_NOTAUTHORIZED, userLanguage),
							WebConstants.MSG_ERROR);
				} else {
					log.error("Exception in UserRequestHandler.execute() [origin module=" + moduleException
							.getModuleDesc() + "]", moduleException);
					saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
				}
			} catch (Exception exception) {
				log.error("Exception in UserRequestHandler.execute()", exception);
				setExceptionOccured(request, true);
				if (exception instanceof RuntimeException) {
					forward = S2Constants.Result.ERROR;
					JavaScriptGenerator.setServerError(request, exception.getMessage(), "", "");
				}
				return forward;
			}
			;

			try {
				if (strHdnMode != null && strHdnMode.equalsIgnoreCase(WebConstants.ACTION_RESET)) {
					String strUserId = request.getParameter(PARAM_LOGIN_ID);
					if (strUserId != null && strUserId.length() > 0) {
						strUserId = defaultAirlineCode + strUserId;
					}
					String strpwd = request.getParameter(PARAM_PASSWORD);

					if (strUserId != null) {
						User existingUser = ModuleServiceLocator.getSecurityBD().getUser(strUserId);
						if (checkAccesability(request, existingUser)) {
							if (existingUser != null) {
								existingUser.setPassword(strpwd.trim());
								existingUser.setPasswordReseted("Y");
								existingUser.setLccPublishStatus(Util.LCC_TOBE_REPUBLISHED);

								ModuleServiceLocator.getSecurityBD().saveUser(existingUser);
								if (AppSysParamsUtil.isLCCDataSyncEnabled()) {
									User savedUser = ModuleServiceLocator.getSecurityBD()
											.getUser(existingUser.getUserId());
									ModuleServiceLocator.getCommonServiceBD().publishSpecificUserDataUpdate(savedUser);
								}
								saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_RESET_SUCCESS, userLanguage),
										WebConstants.MSG_SUCCESS);
								setExceptionOccured(request, false);
							}

						}

					}
				}

			} catch (ModuleException moduleException) {

				setExceptionOccured(request, true);
				if (moduleException.getExceptionCode().equals(WebConstants.KEY_RESET_NOTAUTHORIZED)) {
					saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_RESET_NOTAUTHORIZED, userLanguage),
							WebConstants.MSG_ERROR);
				} else if (moduleException.getExceptionCode().equals(WebConstants.KEY_MAX_ADMINUSERS)) {
					saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_MAX_ADMINUSERS, userLanguage),
							WebConstants.MSG_ERROR);
				} else {
					log.error("Exception in UserRequestHandler.execute() [origin module=" + moduleException
							.getModuleDesc() + "]", moduleException);
					saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
				}
				if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
				} else {
					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
				}
			} catch (Exception exception) {
				log.error("Exception in UserRequestHandler.execute()", exception);
				setExceptionOccured(request, true);
				if (exception instanceof RuntimeException) {
					forward = S2Constants.Result.ERROR;
					JavaScriptGenerator.setServerError(request, exception.getMessage(), "", "");
				}
				if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
				} else {
					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
				}
			}

			try {
				audit(request, TasksUtil.MASTER_VIEW_USERS);
				setDisplayData(request);
			} catch (ModuleException e) {
				log.error("Exception in UserRequestHandler.execute() [origin module=" + e.getModuleDesc() + "]", e);
				saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			}
		}

		return forward;
	}

	public static String loadAgentList(HttpServletRequest request, String station) throws ModuleException {
		int intAdminAcc = 0;
		int intNormalAcc = 0;
		int intAcc = 0;
		Principal principal = request.getUserPrincipal();
		User user = ModuleServiceLocator.getSecurityBD().getUser(principal.getName());

		intAdminAcc = getAdminUsersAccesability(request, user);
		intNormalAcc = getNormalUsersAccesability(request, user);
		if (!user.getPrivitedgeIDs().contains(WebConstants.PRIV_SYS_SECURITY_USER_AUDIT)) {

			intAcc = getAccountAccessLevel(intAdminAcc, intNormalAcc);

		} else {

			if (isGSAV2Enabled) {
				if (intAdminAcc == 0 && intNormalAcc == 0) {
					intAcc = 2;
				} else {
					intAcc = getAccountAccessLevel(intAdminAcc, intNormalAcc);
				}
			} else {
				intAcc = 1;
			}

		}
		return getAgentList(request, user, intAcc, station);
	}
	/**
	 * Saves the User Data
	 *
	 * @param props   the Property file Containg User Request values
	 * @param request the HttpServletRequest
	 * @throws ModuleException the ModuleException
	 */
	private static void saveData(Properties props, HttpServletRequest request) throws ModuleException {

		User user = null;
		String strVersion = props.getProperty(PARAM_VERSION);
		String strHdnAction = request.getParameter("hdnAction");

		if (user == null) {
			Set<Role> setAssignRoles = new HashSet<Role>();
			String strAssignRoleValues = props.getProperty(PARAM_ASSIGN_ROLES);
			String strCarrierCodes = props.getProperty(PARAM_CARRIER_CODES);
			User currentUser = null;
			boolean existinAdminUser = false;
			Collection<String> chColtn = new HashSet<String>();
			chColtn.add(Constants.BOTH_CHANNELS);
			chColtn.add(Constants.INTERNAL_CHANNEL);
			chColtn.add(Constants.EXTERNAL_CHANNEL);
			Collection<Role> colRoles = ModuleServiceLocator.getSecurityBD().getRolesByServiceChannels(chColtn);
			user = new User();

			user.setUserId(props.getProperty(PARAM_LOGIN_ID));
			user.setDisplayName(props.getProperty(PARAM_FIRST_NAME));
			user.setEmailId(props.getProperty(PARAM_EMAIL));
			user.setThemeCode(props.getProperty(PARAM_USER_THEME));
			if (props.getProperty(PARAM_PASSWORD).equals("") || strHdnAction.equals(WebConstants.ACTION_EDIT))
				user.setPassword(null);
			else
				user.setPassword(props.getProperty(PARAM_PASSWORD).trim());
			user.setStatus(props.getProperty(PARAM_STATUS));
			user.setAgentCode(props.getProperty(PARAM_TRAVEL_AGENT).split(",")[0]);
			user.setDefaultCarrierCode(props.getProperty(PARAM_DEF_CARRIER));
			Collection<Role> existingRoles = null;
			Principal principal = request.getUserPrincipal();
			User existingUser = ModuleServiceLocator.getSecurityBD().getUserBasicDetails(principal.getName());

			if (strHdnAction.equals(WebConstants.ACTION_EDIT)) {
				user.setLccPublishStatus(Util.LCC_TOBE_REPUBLISHED);
			} else {
				user.setLccPublishStatus(Util.LCC_TOBE_PUBLISHED);
			}

			if (props.getProperty(PARAM_APPLY_ROLE_CHANGE).equals("true")) {
				user.setAssignRoleChangesToOtherUsers(true);
			}

			currentUser = ModuleServiceLocator.getSecurityBD().getUser(props.getProperty(PARAM_LOGIN_ID));
			if (currentUser != null) {
				existingRoles = currentUser.getRoles();
				if (!currentUser.getAdminUserCreateStatus().equals("N") || !currentUser.getNormalUserCreateStatus()
						.equals("N")) {
					existinAdminUser = true;
				}
				user.setPasswordReseted(currentUser.getPasswordReseted() != null ?
						currentUser.getPasswordReseted() :
						User.PASSWORD_RESETTED_NO);
				user.setServiceChannel(currentUser.getServiceChannel());
				if (currentUser.getAirlineCode() != null) {
					user.setAirlineCode(currentUser.getAirlineCode());
				}
				user.setPasswordExpiryDate(currentUser.getPasswordExpiryDate());
			} else {
				user.setPasswordReseted(User.PASSWORD_RESETTED_YES);
			}
			if (user.getAirlineCode() == null) {
				user.setAirlineCode(AppSysParamsUtil.getDefaultAirlineIdentifierCode());
			}

			if (existingRoles != null) {
				String strAgCode = existingUser.getAgentCode();
				String agentTypes[] = { strAgCode };
				Collection defCol = SelectListGenerator.createVisibleRoles(agentTypes); // the visible roles
				// Check for already exists invisible roles and insert them

				for (Role existingRole : existingRoles) {
					if (!isRoleContain(existingRole, defCol)) {
						// System user assigned role found. Retain it.
						setAssignRoles.add(existingRole);
						break;
					}
				}
			}

			if (hasPrivilege(request, "sys.security.user.roles")) {
				if (strAssignRoleValues != null && strAssignRoleValues.length() > 0) {
					String strAgCode = props.getProperty(PARAM_TRAVEL_AGENT).split(",")[0];
					String agentTypes[] = { strAgCode };
					Collection assiCol = SelectListGenerator.createDefaultRoles(agentTypes); // to do assignable roles

					if (!isAssignRolesContain(strAssignRoleValues.split(","), assiCol)) {
						throw new ModuleException("xbe.user.role.notassignable");
					}

					for (Role role : colRoles) {
						if (isRoleContain(role, assiCol)) {
							StringTokenizer strTok = new StringTokenizer(strAssignRoleValues, ",");

							while (strTok.hasMoreTokens()) {
								if (role.getRoleId().equals(strTok.nextToken())) {
									setAssignRoles.add(role);
									break;
								}
							}
						}
					}
				}

			} else {
				if (strHdnAction.equals(WebConstants.ACTION_ADD)) {
					String strAgCode = props.getProperty(PARAM_TRAVEL_AGENT).split(",")[0];
					String agentTypes[] = { strAgCode };
					Collection visiCol = SelectListGenerator.createVisibleRoles(agentTypes); // the visible roles
					Collection assiCol = SelectListGenerator.createDefaultRoles(agentTypes); // to do assignable roles
					Role role = null;
					Iterator<Role> roleIter = colRoles.iterator();
					while (roleIter.hasNext()) {
						role = (Role) roleIter.next();
						if (isRoleContain(role, visiCol) && isRoleContain(role, assiCol)) {
							setAssignRoles.add(role);
						}
					}

				} else {
					if (currentUser != null) {
						/**
						 * M. Naeem Akhtar 28-OCT-2010 * Add User's old roles while updating an existing User
						 * AARESAA-5101
						 ***/
						setAssignRoles.addAll(currentUser.getRoles());
					}

				}

			}

			Set<String> carrierCodes = new HashSet<String>();
			if (strCarrierCodes != null && strCarrierCodes.length() > 0) {
				StringTokenizer strTok = new StringTokenizer(strCarrierCodes, ",");
				while (strTok.hasMoreTokens()) {
					carrierCodes.add(strTok.nextToken());
				}
			}
			user.setCarriers(carrierCodes);

			if (strVersion != null && !"".equals(strVersion)) {
				user.setVersion(Long.parseLong(strVersion));
			}

			if (props.getProperty(PARAM_ADMIN_ANY).equals("ANY")) {
				user.setAdminUserCreateStatus("A");
			} else if (props.getProperty(PARAM_ADMIN_OWN).equals("OWNER") && props.getProperty(PARAM_ADMIN_REPORTING)
					.equals("REPORTING")) {
				user.setAdminUserCreateStatus("B");
			} else if (props.getProperty(PARAM_ADMIN_OWN).equals("OWNER")) {
				user.setAdminUserCreateStatus("O");
			} else if (props.getProperty(PARAM_ADMIN_REPORTING).equals("REPORTING")) {
				user.setAdminUserCreateStatus("R");
			} else {
				user.setAdminUserCreateStatus("N");
			}

			if (props.getProperty(PARAM_NORM_ANY).equals("ANY")) {
				user.setNormalUserCreateStatus("A");
			} else if (props.getProperty(PARAM_NORM_OWN).equals("OWNER") && props.getProperty(PARAM_NORM_REPORTING)
					.equals("REPORTING")) {
				user.setNormalUserCreateStatus("B");
			} else if (props.getProperty(PARAM_NORM_OWN).equals("OWNER")) {
				user.setNormalUserCreateStatus("O");
			} else if (props.getProperty(PARAM_NORM_REPORTING).equals("REPORTING")) {
				user.setNormalUserCreateStatus("R");
			} else {
				user.setNormalUserCreateStatus("N");
			}
			if (!existinAdminUser && (!user.getNormalUserCreateStatus().equals("N") || !user.getAdminUserCreateStatus()
					.equals("N"))) {
				String strAgCode = props.getProperty(PARAM_TRAVEL_AGENT).split(",")[0];
				String agentTypes[] = { strAgCode };
				int adminCount = SelectListGenerator.getAdminUseCount(agentTypes);
				int maxCount = 0;
				String strCount = ModuleServiceLocator.getGlobalConfig()
						.getBizParam(SystemParamKeys.MAX_ADMIN_USER_COUNT);
				if (strCount != null && !strCount.equals("")) {
					maxCount = new Integer(strCount).intValue();
				}
				if (adminCount >= maxCount) {
					throw new ModuleException(WebConstants.KEY_MAX_ADMINUSERS);
				}

				Role defRole = ModuleServiceLocator.getSecurityBD()
						.getRole(AppSysParamsUtil.getDefaultAirlineIdentifierCode() + Role.UNEDITABLE_ROLE_ID3);
				if (defRole != null)
					setAssignRoles.add(defRole);
			}
			if (existinAdminUser && (!user.getNormalUserCreateStatus().equals("N") || !user.getAdminUserCreateStatus()
					.equals("N"))) {
				String strAgCode = props.getProperty(PARAM_TRAVEL_AGENT).split(",")[0];
				String agentTypes[] = { strAgCode };
				int adminCount = SelectListGenerator.getAdminUseCount(agentTypes);
				int maxCount = 0;
				String strCount = ModuleServiceLocator.getGlobalConfig()
						.getBizParam(SystemParamKeys.MAX_ADMIN_USER_COUNT);
				if (strCount != null && !strCount.equals("")) {
					maxCount = new Integer(strCount).intValue();
				}
				if (adminCount >= maxCount && user.STATUS_ACTIVE.equalsIgnoreCase(user.getStatus())) {
					throw new ModuleException(WebConstants.KEY_MAX_ADMINUSERS);
				}
			} else if (existinAdminUser && user.getNormalUserCreateStatus().equals("N") && user
					.getAdminUserCreateStatus().equals("N")) {
				Role defRole = ModuleServiceLocator.getSecurityBD()
						.getRole(AppSysParamsUtil.getDefaultAirlineIdentifierCode() + Role.UNEDITABLE_ROLE_ID3);
				Role asgRole = null;
				if (defRole != null)

					if (setAssignRoles != null && setAssignRoles.size() > 0) {
						Iterator aiter = setAssignRoles.iterator();
						while (aiter.hasNext()) {
							asgRole = (Role) aiter.next();
							if (asgRole.getRoleId().equals(defRole.getRoleId())) {
								setAssignRoles.remove(asgRole);
								break;
							}
						}
					}

			}

			// adding dry exclusive privilege to newly created users
			String strAgCode = props.getProperty(PARAM_TRAVEL_AGENT).split(",")[0];
			String agentTypes[] = { strAgCode };
			Collection assignCol = SelectListGenerator.createDefaultRoles(agentTypes);
			Collection<Role> colDryExclusiveRoles = ModuleServiceLocator.getSecurityBD()
					.getDryExcludeRolesByServiceChannels(chColtn);
			Collection<Role> unaddedDryExclusiveRoles = new ArrayList<Role>();
			if (colDryExclusiveRoles != null && !colDryExclusiveRoles.isEmpty()) {
				for (Role dryDole : colDryExclusiveRoles) {
					if (isRoleContain(dryDole, assignCol)) {
						boolean found = false;
						for (Role addedRoles : setAssignRoles) {
							if (addedRoles.getRoleId().equals(dryDole.getRoleId())) {
								found = true;
								break;
							}
						}
						if (!found) {
							unaddedDryExclusiveRoles.add(dryDole);
						}
					}
				}
			}

			setAssignRoles.addAll(unaddedDryExclusiveRoles);
			user.setRoles(setAssignRoles);
			user.setFfpNumber(props.getProperty(PARAM_FFP_NUMBER));

			String myIdCarrier = props.getProperty(PARAM_MYID_CARRIER);
			if (hasPrivilege(request, WebConstants.PRIV_SYS_SECURITY_USER_MYIDTRAVEL) && myIdCarrier.length() > 0) {
				user.setMyIDCarrierCode(myIdCarrier.toUpperCase());
			}

			String strServChannel = props.getProperty(PARAM_SERVICE_CHANNEL);
			/** assign service channel if user has privilege. otherwise set the existin value */
			if (hasPrivilege(request, "sys.security.role.servicechannel.edit") && strServChannel != null
					&& !strServChannel.equals("")) {
				user.setServiceChannel(strServChannel);
			}
			
			user.setMaxAdultAllowed(props.getProperty(PARAM_MAX_ADULT_ALLOWED).equals("")? null :  new Integer(props.getProperty(PARAM_MAX_ADULT_ALLOWED)));
			
		}
		
		validateLoggedUserUserCreationAccessLevel(request.getUserPrincipal(),  user);

		ModuleServiceLocator.getSecurityBD().saveUser(user);
		try {
			if (AppSysParamsUtil.isLCCDataSyncEnabled()) {
				User savedUser = ModuleServiceLocator.getSecurityBD().getUser(user.getUserId());
				ModuleServiceLocator.getCommonServiceBD().publishSpecificUserDataUpdate(savedUser);
			}
		} catch (Exception exp) {
			log.error("Error in LCC USER SYNC operation");
		}
		user = null;
	}

	private static boolean isRoleContain(Role chkRole, Collection visible) throws ModuleException {

		if (chkRole == null || visible == null || visible.size() == 0) {
			return false;
		} else {
			String chkRoleId = chkRole.getRoleId();
			for (int cvl = 0; cvl < visible.size(); cvl++) {
				Iterator vite = visible.iterator();
				while (vite.hasNext()) {
					String[] vDefs = (String[]) vite.next();
					if (chkRoleId.equals(vDefs[0])) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private static boolean isAssignRolesContain(String[] assgdRole, Collection visible) throws ModuleException {

		boolean blnFound = false;
		if (assgdRole == null || visible == null || visible.size() == 0) {
			return false;
		} else {

			for (int al = 0; al < assgdRole.length; al++) {
				String chkRoleId = assgdRole[al];
				blnFound = false;
				for (int cvl = 0; cvl < visible.size(); cvl++) {
					Iterator vite = visible.iterator();
					while (vite.hasNext()) {
						String[] vDefs = (String[]) vite.next();
						if (chkRoleId.equals(vDefs[0])) {
							blnFound = true;
						}
					}
				}
				if (!blnFound)
					return false;
			}

		}
		return blnFound;
	}

	/**
	 * Sets the Display Data For User Page
	 *
	 * @param request the HttpServletRequest
	 * @throws ModuleException the ModuleException
	 */
	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		Principal principal = request.getUserPrincipal();
		User user = ModuleServiceLocator.getSecurityBD().getUser(principal.getName());
		int intAdminAcc = 0;
		int intNormalAcc = 0;
		int intAcc = 0;

		intAdminAcc = getAdminUsersAccesability(request, user);
		intNormalAcc = getNormalUsersAccesability(request, user);
		if (!user.getPrivitedgeIDs().contains(WebConstants.PRIV_SYS_SECURITY_USER_AUDIT)) {

			intAcc = getAccountAccessLevel(intAdminAcc, intNormalAcc);

		} else {

			if (isGSAV2Enabled) {
				if (intAdminAcc == 0 && intNormalAcc == 0) {
					intAdminAcc = 2;
					intNormalAcc = 2;
					intAcc = 2;
				} else {
					intAcc = getAccountAccessLevel(intAdminAcc, intNormalAcc);
				}
			} else {
				intAdminAcc = 1;
				intNormalAcc = 1;
				intAcc = 1;
			}
			request.setAttribute("reqUseAddAdminStat", "adminInt = " + intAdminAcc + ";");
			request.setAttribute("reqUseAddNormalStat", "normalInt = " + intNormalAcc + ";");

		}

		setClientErrors(request);
		setStationHtml(request, user.getAgentCode(), intAcc);
		setRoleHtml(request, user);
		setUserRowHtml(request, user.getAgentCode(), intAdminAcc, intNormalAcc);
		setAgentList(request, user, intAcc);
		setServiceChannelsHtml(request);
		setDryUserEnable(request);
		setRequestHtml(request);
		setAirlineCodeList(request);
		setCarrierCodeList(request, user, intAcc);
		setCriierAgent(request);
		setUserThemesHtml(request);
		setLCCStatus(request);
		setEnableGSAV2(request);
		userAgentType(request);

		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));
		request.setAttribute(WebConstants.REQ_DEFAULT_AIRLINE_CODE, AppSysParamsUtil.getDefaultAirlineIdentifierCode());
		setCarrierPrefixStatus(request);
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		if (userPrincipal != null) {
			request.setAttribute(WebConstants.REQ_IS_REMOTE_USER, "false");
		}
	}

	private static void setCarrierPrefixStatus(HttpServletRequest request) {
		boolean isAllowAirlineCarrierPrefixForAgentsAndUsers = AppSysParamsUtil
				.isAllowAirlineCarrierPrefixForAgentsAndUsers();
		request.setAttribute(WebConstants.REQ_ALLOW_CARRIER_PREFIX, isAllowAirlineCarrierPrefixForAgentsAndUsers);
	}

	private static void setLCCStatus(HttpServletRequest request) {
		boolean lccEnbled = AppSysParamsUtil.isLCCConnectivityEnabled();
		request.setAttribute("reqLccEnabled", lccEnbled);
	}

	private static void setCriierAgent(HttpServletRequest request) throws ModuleException {
		String strCrrAgentType = AppSysParamsUtil.getCarrierAgent();
		request.setAttribute("reqCarrAgent", strCrrAgentType);
	}

	private static int getAdminUsersAccesability(HttpServletRequest req, User user) {
		int intAdminAcc = 0;
		String adminStatus = user.getAdminUserCreateStatus();

		if (adminStatus.equals("A")) {
			intAdminAcc = 1;
		} else if (adminStatus.equals("B")) {
			intAdminAcc = 2;
		} else if (adminStatus.equals("R")) {
			intAdminAcc = 3;
		} else if (adminStatus.equals("O")) {
			intAdminAcc = 4;
		}
		req.setAttribute("reqUseAddAdminStat", "adminInt = " + intAdminAcc + ";");

		return intAdminAcc;
	}

	private static int getNormalUsersAccesability(HttpServletRequest req, User user) {

		int intNormalAcc = 0;
		String normalStatus = user.getNormalUserCreateStatus();

		if (normalStatus.equals("A")) {
			intNormalAcc = 1;
		} else if (normalStatus.equals("B")) {
			intNormalAcc = 2;
		} else if (normalStatus.equals("R")) {
			intNormalAcc = 3;
		} else if (normalStatus.equals("O")) {
			intNormalAcc = 4;
		}
		req.setAttribute("reqUseAddNormalStat", "normalInt = " + intNormalAcc + ";");
		return intNormalAcc;
	}

	/**
	 * Set the ServiceChannels to the Request
	 *
	 * @param request the HttpServletRequest
	 * @throws ModuleException the ModuelException
	 */
	private static void setServiceChannelsHtml(HttpServletRequest request) throws ModuleException {
		String strSrvChs = SelectListGenerator.createServiceChannels();
		request.setAttribute(WebConstants.REQ_HTML_SERVICE_CHANNELS, strSrvChs);
	}

	/**
	 * Set the dry user enable state to the request
	 *
	 * @param request the HttpServletRequest
	 * @throws ModuleException the ModuelException
	 */
	private static void setDryUserEnable(HttpServletRequest request) throws ModuleException {
		UserHTMLGenerator htmlGen = new UserHTMLGenerator(getProperties(request));
		String strDryUserEnable = htmlGen.getDryUserEnable(request);
		request.setAttribute(WebConstants.REQ_HTML_DRY_USER_ENABLE, strDryUserEnable);
	}

	/**
	 * Sets the Agents Array to the Request
	 *
	 * @param request the HttpServletRequest
	 * @throws ModuleException the ModuleException
	 */
	private static void setAgentList(HttpServletRequest request, User user, int accLevel) throws ModuleException {
		String strStation = StringUtil.getNotNullString(request.getParameter(PARAM_SEARCHAIRPORT));
		if (!strStation.isEmpty()) {
			request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, getAgentList(request, user, accLevel, strStation));
		}

	}

	
	/**
	 * Sets the Agents Array to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static String getAgentList(HttpServletRequest request, User user, int accLevel, String station) throws ModuleException {
		String strAgentGSAHtml = "";
		List<String[]> gsas = null;
		String[] gsaAgents;
		StringBuilder gsb = new StringBuilder();
		if (station.equals("ALL")) {
			if (accLevel == 1) {
				gsas = (List<String[]>) SelectListGenerator.createAgentsWithTypeFilteredByAirline();
			} else if (accLevel > 0) {

				String agentCode = user.getAgentCode();
				String[] agents = new String[1];
				agents[0] = agentCode;

				if (isGSAV2Enabled) {
					gsas = (List<String[]>) SelectListGenerator.createAgentForLoggedUserLevelOfRights(agentCode, 2);
				} else {
					// with new GSA restructure, GSA and other agents we load all the reporting agents where the root
					// node of the hierarchy is the current Agent, so GSA and TA agents have same query now
					if (accLevel == 2 || accLevel == 3) {
						gsas = (List<String[]>) SelectListGenerator.createTasFromGSA(agents); // for gsa

					} else if (accLevel == 4) {
						gsas = (List<String[]>) SelectListGenerator.createTADescription(agents); // owner agent
					}
				}

			}
		} else {
			if (accLevel == 1) {
				gsas = (List<String[]>) SelectListGenerator.createAgentsWithTypeFilteredByAirlineAndStation(station);
			} else if (accLevel > 0) {

				String agentCode = user.getAgentCode();
				String[] agents = new String[1];
				agents[0] = agentCode;

				if (isGSAV2Enabled) {
					gsas = (List<String[]>) SelectListGenerator.createAgentForLoggedUserLevelOfRightsByStation(agentCode, 2,
							station);
				} else {
					// with new GSA restructure, GSA and other agents we load all the reporting agents where the root
					// node of the hierarchy is the current Agent, so GSA and TA agents have same query now
					if (accLevel == 2 || accLevel == 3) {
						gsas = (List<String[]>) SelectListGenerator.createStationTasFromGSA(agents, station); // for gsa

					} else if (accLevel == 4) {
						gsas = (List<String[]>) SelectListGenerator.createStationTADescription(agents, station); // owner
																													// agent
					}
				}

			}
		}

		if (gsas != null && !gsas.isEmpty()) {
			Iterator<String[]> iter = gsas.iterator();
			int i = 0;
			while (iter.hasNext()) {
				gsaAgents = (String[]) iter.next();
				if (gsaAgents != null) {
					if (!isGSAV2Enabled && accLevel == 2) {
						if (i == 0) {
							gsb.append("<option value='" + gsaAgents[0] + ",GSA'>" + gsaAgents[1] + "</option>");
						}
					}
					if (gsaAgents[2] != null) {
						gsb.append("<option value='" + gsaAgents[2] + "," + gsaAgents[4] + "'>" + gsaAgents[3]
								+ "</option>");
					}
					i++;
				}

			}
		}
		strAgentGSAHtml = gsb.toString();

		return strAgentGSAHtml;

	}

	/**
	 * Sets the Carrier code list
	 *
	 * @param request
	 * @throws ModuleException
	 */
	private static void setCarrierCodeList(HttpServletRequest request, User user, int accLevel) throws ModuleException {

		String strCarrierCodesSelHTML = "";
		if (accLevel == 1) {
			strCarrierCodesSelHTML = SelectListGenerator.getSystemCarrierCodes();
		} else if (accLevel > 0) {
			StringBuilder csb = new StringBuilder();
			String strValue = "";
			Set<String> crrCode = user.getCarriers();
			if (crrCode != null && !crrCode.isEmpty()) {
				Iterator<String> cIt = crrCode.iterator();
				while (cIt.hasNext()) {
					strValue = (String) cIt.next();
					csb.append("<option value='" + strValue + "'>" + strValue + "</option>");
				}
			}
			strCarrierCodesSelHTML = csb.toString();
		}
		request.setAttribute(WebConstants.REQ_CARRIER_SELECT_LIST, strCarrierCodesSelHTML);

		String[] arrCarrirers = strCarrierCodesSelHTML.split("</option>");
		request.setAttribute(WebConstants.REQ_NO_OF_CARRIERS, arrCarrirers.length);
	}

	/**
	 * Sets the Airline code list
	 *
	 * @param request
	 * @throws ModuleException
	 */
	private static void setAirlineCodeList(HttpServletRequest request) throws ModuleException {

		String strAirLineList = "";
		strAirLineList = SelectListGenerator.getAirlineCodeList();
		request.setAttribute(WebConstants.REQ_AIRLINE_SELECT_LIST, strAirLineList);
	}

	/**
	 * Sets the Role Array to the Request
	 *
	 * @param request the HttpServletRequest
	 * @throws ModuleException the ModuleException
	 */
	private static void setRoleHtml(HttpServletRequest request, User user) throws ModuleException {
		String strRoleList = "";
		if (hasPrivilege(request, "sys.security.user.roles")) {
			strRoleList = JavaScriptGenerator.createRoleByAgent(user.getAgentCode());
		} else {
			StringBuffer jsb = new StringBuffer("var arrGroup1 = new Array();");
			jsb.append("arrGroup1[0] = '' ;");
			jsb.append("var arrData = new Array();");
			jsb.append("var roles = new Array();");
			jsb.append("arrData[0] = roles; arrData[1] = new Array();");
			jsb.append("var arrGroup2 = new Array();");
			jsb.append("var arrData2 = new Array();");
			jsb.append("arrData2[0] = new Array();");
			jsb.append("var ls = new Listbox2('lstRoles', 'lstAssignedRoles', 'spn1');");
			jsb.append("blnRoleValidate= true; ");
			jsb.append("ls.group1 = arrData[0];" + "ls.group2 = arrGroup2;");
			jsb.append("ls.height = '165px'; ls.width = '170px';");
			jsb.append("ls.headingLeft = '&nbsp;&nbsp;Roles';");
			jsb.append("ls.headingRight = '&nbsp;&nbsp;Assigned Roles';");
			jsb.append("if(document.getElementById('spn1'))");
			jsb.append("ls.drawListBox();");
			strRoleList = jsb.toString();
		}
		request.setAttribute(WebConstants.REQ_HTML_USER_ROLE_DATA, strRoleList);

	}

	/**
	 * Sets the User Client validations to the Request
	 *
	 * @param request the HttpServletRequest
	 * @throws ModuleException the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {

		String strClientErrors = UserHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null)
			strClientErrors = "";
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

	}

	/**
	 * Sets the Station array to the request
	 *
	 * @param request the HttpServletRequest
	 * @throws ModuleException the ModuleException
	 */
	private static void setStationHtml(HttpServletRequest request, String gsaCode, int accLevel)
			throws ModuleException {

		String strAirportList = "";
		if (accLevel == 1) {
			strAirportList = SelectListGenerator.createStationCodeList();
		} else if (accLevel == 2 || accLevel == 3) {
			strAirportList = SelectListGenerator.createGSAStations(gsaCode);
		} else if (accLevel == 4) {
			strAirportList = SelectListGenerator.createOwnerStations(gsaCode);
		}
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strAirportList);

	}

	/**
	 * Sets User Grid Array to request
	 *
	 * @param request the HttpServletRequest
	 * @throws ModuleException the ModuleException
	 */
	private static void setUserRowHtml(HttpServletRequest request, String gsaCode, int adminLevel, int normLevel)
			throws ModuleException {

		UserHTMLGenerator htmlGen = new UserHTMLGenerator(getProperties(request));
		String strHtml = htmlGen.getUserRowHtml(request, gsaCode, adminLevel, normLevel);
		request.setAttribute(WebConstants.REQ_HTML_USER_DATA, strHtml);
	}

	/**
	 * Sets the User Request To a Property File
	 *
	 * @param request the HttpServletRequest
	 * @return Properties the File Containg User Request Values
	 */
	private static Properties getProperties(HttpServletRequest request) {

		Properties props = new Properties();

		String strVersion = request.getParameter(PARAM_VERSION);
		String strLoginId = request.getParameter(PARAM_LOGIN_ID);
		if (strLoginId != null && strLoginId.length() > 0) {
			strLoginId = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode() + strLoginId;
		}
		String strPassword = request.getParameter(PARAM_PASSWORD);
		if (strPassword == null || strPassword.equals(""))
			strPassword = request.getParameter(PARAM_PREVPASSWORD);
		String strFirstName = request.getParameter(PARAM_FIRST_NAME);
		String strEmail = request.getParameter(PARAM_EMAIL);
		String arrAssignRoles = request.getParameter(PARAM_ASSIGN_ROLES);
		String strStatus = request.getParameter(PARAM_STATUS);
		String strMode = request.getParameter(PARAM_MODE);
		String strTravelAgentCode = request.getParameter(PARAM_TRAVEL_AGENT);
		String strhdnAction = request.getParameter(PARAM_HDNACTION);
		String strHdneditable = request.getParameter(PARAM_EDITABLE);
		String strRecNo = request.getParameter(PARAM_RECNO);
		String strSearchData = request.getParameter(PARAM_SEARCHDATA);
		String strGridData = request.getParameter(PARAM_GRIDDATA);
		String strSearchID = request.getParameter(PARAM_SEARCHID);
		String strSearchAirline = request.getParameter(PARAM_SEARCHAIRLINE);
		String strSearchName = request.getParameter(PARAM_SEARCHNAME);
		String strSearchPort = request.getParameter(PARAM_SEARCHAIRPORT);
		String strSearchAgent = request.getParameter(PARAM_SEARCHAGENT);
		String strCarrierCodes = request.getParameter(PARAM_CARRIER_CODES);
		String strDefCarrier = request.getParameter(PARAM_DEF_CARRIER);
		String strServiceChannel = request.getParameter(PARAM_SERVICE_CHANNEL);
		String strUserThemeCode = request.getParameter(PARAM_USER_THEME);

		String strAdminAny = request.getParameter(PARAM_ADMIN_ANY);
		String strAdminOwn = request.getParameter(PARAM_ADMIN_OWN);
		String strAdminReorting = request.getParameter(PARAM_ADMIN_REPORTING);
		String strNormAny = request.getParameter(PARAM_NORM_ANY);
		String strNormOwn = request.getParameter(PARAM_NORM_OWN);
		String strNormReporting = request.getParameter(PARAM_NORM_REPORTING);
		String strDryUserEnable = request.getParameter(PARAM_DRY_USER_ENABLE);
		String ffpNumber = request.getParameter(PARAM_FFP_NUMBER);
		String applyRoleChange = request.getParameter(PARAM_APPLY_ROLE_CHANGE);
		String myIDCarrier = request.getParameter(PARAM_MYID_CARRIER);

		props.setProperty(PARAM_VERSION, StringUtil.getNotNullString(strVersion));
		props.setProperty(PARAM_LOGIN_ID, StringUtil.getNotNullString(strLoginId).toUpperCase());
		props.setProperty(PARAM_PASSWORD, StringUtil.getNotNullString(strPassword));
		props.setProperty(PARAM_FIRST_NAME, StringUtil.getNotNullString(strFirstName));
		props.setProperty(PARAM_EMAIL, StringUtil.getNotNullString(strEmail));
		props.setProperty(PARAM_ASSIGN_ROLES, StringUtil.getNotNullString(arrAssignRoles));
		props.setProperty(PARAM_HDNACTION, StringUtil.getNotNullString(strhdnAction));
		props.setProperty(PARAM_EDITABLE, StringUtil.getNotNullString(strHdneditable));
		props.setProperty(PARAM_SEARCHDATA, StringUtil.getNotNullString(strSearchData));
		props.setProperty(PARAM_GRIDDATA, StringUtil.getNotNullString(strGridData));
		props.setProperty(PARAM_SEARCHID, StringUtil.getNotNullString(strSearchID));
		props.setProperty(PARAM_SEARCHAIRLINE, StringUtil.getNotNullString(strSearchAirline));
		props.setProperty(PARAM_SEARCHNAME, StringUtil.getNotNullString(strSearchName));
		props.setProperty(PARAM_SEARCHAIRPORT, StringUtil.getNotNullString(strSearchPort));
		props.setProperty(PARAM_SEARCHAGENT, StringUtil.getNotNullString(strSearchAgent));
		if (strCarrierCodes == null || strCarrierCodes.trim().equals("")) {
			strCarrierCodes = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		}
		props.setProperty(PARAM_CARRIER_CODES, StringUtil.getNotNullString(strCarrierCodes));
		props.setProperty(PARAM_RECNO, (strRecNo == null) ? "1" : strRecNo);
		if (strDefCarrier == null || strDefCarrier.trim().equals("")) {
			strDefCarrier = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		}
		props.setProperty(PARAM_DEF_CARRIER, StringUtil.getNotNullString(strDefCarrier));

		props.setProperty(PARAM_ADMIN_ANY, StringUtil.getNotNullString(strAdminAny));
		props.setProperty(PARAM_ADMIN_OWN, StringUtil.getNotNullString(strAdminOwn));
		props.setProperty(PARAM_ADMIN_REPORTING, StringUtil.getNotNullString(strAdminReorting));
		props.setProperty(PARAM_NORM_ANY, StringUtil.getNotNullString(strNormAny));
		props.setProperty(PARAM_NORM_OWN, StringUtil.getNotNullString(strNormOwn));
		props.setProperty(PARAM_NORM_REPORTING, StringUtil.getNotNullString(strNormReporting));
		props.setProperty(PARAM_SERVICE_CHANNEL,
				(strServiceChannel == null) ? Constants.INTERNAL_CHANNEL : strServiceChannel);
		props.setProperty(PARAM_USER_THEME, (strUserThemeCode == null) ? User.THEME_CODE_DEFAULT : strUserThemeCode);
		props.setProperty(PARAM_DRY_USER_ENABLE, StringUtil.getNotNullString(strDryUserEnable));
		props.setProperty(PARAM_FFP_NUMBER, StringUtil.getNotNullString(ffpNumber));

		if (strStatus != null && strStatus.equals("Active"))
			props.setProperty(PARAM_STATUS, User.STATUS_ACTIVE);
		else
			props.setProperty(PARAM_STATUS, User.STATUS_INVALID);
		props.setProperty(PARAM_MODE, StringUtil.getNotNullString(strMode));
		props.setProperty(PARAM_TRAVEL_AGENT,
				((strTravelAgentCode == null) || (strTravelAgentCode.trim().toUpperCase().equals("SELECT"))) ?
						" " :
						strTravelAgentCode);
		props.setProperty(PARAM_SEARCHSTATUS, StringUtil.getNotNullString(request.getParameter(PARAM_SEARCHSTATUS)));

		if (applyRoleChange != null && applyRoleChange.equals("ROLECHANGE")) {
			props.setProperty(PARAM_APPLY_ROLE_CHANGE, "true");
		} else {
			props.setProperty(PARAM_APPLY_ROLE_CHANGE, "false");
		}
		props.setProperty(PARAM_MYID_CARRIER, StringUtil.getNotNullString(myIDCarrier));
		props.setProperty(PARAM_MAX_ADULT_ALLOWED, StringUtil.getNotNullString(request.getParameter(PARAM_MAX_ADULT_ALLOWED)));
		return props;
	}

	/**
	 * Sets the Form Data to be Used in User Page
	 *
	 * @param request     the HttpServletRequest
	 * @param strFormData
	 * @return
	 * @throws ModuleException
	 */
	private static String fillEnteredFormData(HttpServletRequest request) throws ModuleException {
		Properties props = getProperties(request);

		StringBuffer sbf = new StringBuffer("var arrFormData = new Array();");
		sbf.append("arrFormData[0] = new Array();");
		sbf.append("arrFormData[0][1] = '" + props.getProperty(PARAM_LOGIN_ID) + "';");
		if (props.getProperty(PARAM_PASSWORD).equals("")) {
			sbf.append("arrFormData[0][2] = '" + props.getProperty(PARAM_PREVPASSWORD) + "';");
			sbf.append(" var prePWDChanged = 'N';");
		} else {
			sbf.append("arrFormData[0][2] = '" + props.getProperty(PARAM_PASSWORD) + "';");
			sbf.append(" var prePWDChanged = 'Y';");
		}
		sbf.append("arrFormData[0][3] = '" + props.getProperty(PARAM_FIRST_NAME) + "';");
		sbf.append("arrFormData[0][4] = ' ';");
		sbf.append("arrFormData[0][5] = '" + props.getProperty(PARAM_EMAIL) + "';");
		sbf.append("arrFormData[0][6] = '&nbsp';");
		sbf.append("arrFormData[0][7] = ' ';");
		sbf.append("arrFormData[0][8] = '" + props.getProperty(PARAM_TRAVEL_AGENT) + "';");
		sbf.append("arrFormData[0][9] = '" + props.getProperty(PARAM_STATUS) + "';");
		sbf.append("arrFormData[0][10] = '" + props.getProperty(PARAM_ASSIGN_ROLES) + "';");
		sbf.append("arrFormData[0][11] = '" + props.getProperty(PARAM_VERSION) + "';");
		sbf.append("arrFormData[0][12] = '" + props.getProperty(PARAM_DEF_CARRIER) + "';");
		sbf.append("arrFormData[0][13] = '" + props.getProperty(PARAM_CARRIER_CODES) + "';");
		if (request.getParameter("chkAdmin") != null) {
			sbf.append("arrFormData[0][14] = '" + request.getParameter("chkAdmin").trim() + "';");
		} else {
			sbf.append("arrFormData[0][14] = '';");
		}

		sbf.append("arrFormData[0][15] = '" + props.getProperty(PARAM_ADMIN_ANY) + "';");
		sbf.append("arrFormData[0][16] = '" + props.getProperty(PARAM_ADMIN_OWN) + "';");
		sbf.append("arrFormData[0][17] = '" + props.getProperty(PARAM_ADMIN_REPORTING) + "';");
		sbf.append("arrFormData[0][18] = '" + props.getProperty(PARAM_NORM_ANY) + "';");
		sbf.append("arrFormData[0][19] = '" + props.getProperty(PARAM_NORM_OWN) + "';");
		sbf.append("arrFormData[0][20] = '" + props.getProperty(PARAM_NORM_REPORTING) + "';");
		sbf.append("arrFormData[0][21] = '" + props.getProperty(PARAM_SERVICE_CHANNEL) + "';");
		sbf.append("arrFormData[0][22] = '" + props.getProperty(PARAM_USER_THEME) + "';");
		sbf.append("arrFormData[0][23] = '" + props.getProperty(PARAM_FFP_NUMBER) + "';");
		sbf.append("arrFormData[0][24] = '" + props.getProperty(PARAM_MYID_CARRIER) + "';");
		sbf.append(" var preVersion = '" + props.getProperty(PARAM_VERSION) + "';");
		sbf.append(" var saveSuccess = 2;"); // 0-Not Applicable, 1-Success,
		// 2-Fail
		sbf.append(" var preAction = '" + props.getProperty(PARAM_HDNACTION) + "';");
		sbf.append(" var preEditable = '" + props.getProperty(PARAM_EDITABLE) + "';");

		return sbf.toString();
	}

	/**
	 * Sets Secure Context to the User request
	 *
	 * @param request the HttpServletRequest
	 */
	private static void setRequestHtml(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();

		sb.append("request['contextPath']='" + BasicRH.getContextPath(request) + "';");
		sb.append("request['secureUrl']='" + BasicRH.getSecureUrl(request) + "';");
		sb.append("request['userId']='" + request.getRemoteUser() + "';");
		sb.append("request['protocolStatus']='" + BasicRH.isProtocolStatusEqual() + "';");
		request.setAttribute(WebConstants.REQ_HTML_REQUEST, sb.toString());

	}

	/**
	 * Method to close the User page
	 *
	 * @param request the HttpServletRequest
	 */
	private static void setClose(HttpServletRequest request) {
		String strHtml = "";
		String strPath = BasicRH.getContextPath(request);
		strHtml += "window.location.replace('" + strPath + "/private/showFile!dummyUser.action'); sPage = true";
		request.setAttribute(WebConstants.REQ_HTML_LOGIN_UID, strHtml);

	}

	private static boolean checkFfpAccesability(HttpServletRequest request, User user) throws ModuleException {

		if (hasPrivilege(request, WebConstants.PRIV_SYS_SECURITY_USER_FFP)) {
			return true;
		} else {
			throw new ModuleException(WebConstants.KEY_FFP_EIDT_NOTAUTHORIZED);
		}

	}

	private static boolean checkAccesability(HttpServletRequest request, User user) throws ModuleException {
		Principal principal = request.getUserPrincipal();
		User loggeduser = ModuleServiceLocator.getSecurityBD().getUserBasicDetails(principal.getName());

		if (hasPrivilege(request, "sys.security.user.reset.anyusers.passwd")) {
			return true;
		} else if (hasPrivilege(request, "sys.security.user.reset.rptusers.passwd") && isReportingAgent(
				user.getAgentCode(), loggeduser.getAgentCode())) {
			return true;
		} else if (hasPrivilege(request, "sys.security.user.reset.ownusers.passwd") && user.getAgentCode()
				.equals(loggeduser.getAgentCode())) {
			return true;
		} else {
			throw new ModuleException(WebConstants.KEY_RESET_NOTAUTHORIZED);
		}

	}

	private static boolean isReportingAgent(String agentCode, String gsaCde) throws ModuleException {

		boolean isReporting = false;
		Collection<Agent> agCol = ModuleServiceLocator.getTravelAgentBD().getReportingAgentsForGSA(gsaCde);
		if (agCol != null) {
			Iterator<Agent> iter = agCol.iterator();
			Agent tmpAgent = null;
			while (iter.hasNext()) {
				tmpAgent = (Agent) iter.next();
				if (tmpAgent.getAgentCode().equals(agentCode)) {
					isReporting = true;
					break;
				}
			}
		}
		return isReporting;
	}

	/**
	 * Set the User Theme List to the Request
	 *
	 * @param request the HttpServletRequest
	 * @throws ModuleException the ModuelException
	 */
	private static void setUserThemesHtml(HttpServletRequest request) throws ModuleException {
		String strSrvChs = SelectListGenerator.createUserThemeList(User.THEME_CODE_DEFAULT);
		request.setAttribute(WebConstants.REQ_HTML_USER_THEME_LIST, strSrvChs);
	}
	
	private static int getAccountAccessLevel(int intAdminAcc,int intNormalAcc){
		int intAcc= 0;
		if (intAdminAcc == 1 || intNormalAcc == 1) {
			intAcc = 1;
		} else if (intAdminAcc == 2 || intNormalAcc == 2) {
			intAcc = 2;
		} else if (intAdminAcc == 3 || intNormalAcc == 3) {
			if (intAdminAcc == 4 || intNormalAcc == 4) {
				intAcc = 2;
			} else {
				intAcc = 3;
			}

		} else if (intAdminAcc == 4 || intNormalAcc == 4) {
			intAcc = 4;
		}
		return intAcc;
	}
	
	private static void validateLoggedUserUserCreationAccessLevel(Principal principal, User user) throws ModuleException {
		User loggedInUser = ModuleServiceLocator.getSecurityBD().getUser(principal.getName());

		if (!isValidUserCreateOperation(loggedInUser.getAdminUserCreateStatus(), user.getAdminUserCreateStatus(),
				loggedInUser.getAgentCode(), user.getAgentCode())) {
			throw new ModuleException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		}

		if (!isValidUserCreateOperation(loggedInUser.getNormalUserCreateStatus(), user.getNormalUserCreateStatus(),
				loggedInUser.getAgentCode(), user.getAgentCode())) {
			throw new ModuleException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		}
	}
	
	private static boolean isValidUserCreateOperation(String loggedInUserLevel, String userLevel,
			String loggedUserAgentCode, String agentCode) {
		boolean isUserCreateOperationValid = true;
		if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(userLevel)
				&& !"N".equalsIgnoreCase(userLevel)) {
			if ("N".equalsIgnoreCase(loggedInUserLevel)) {
				// Logged user cannot create Admin User
				isUserCreateOperationValid = false;

			} else if ("O".equalsIgnoreCase(loggedInUserLevel)) {
				if (!"O".equalsIgnoreCase(userLevel)) {
					// Logged user can create only Own Agents
					isUserCreateOperationValid = false;
				} else if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(loggedUserAgentCode)
						&& !loggedUserAgentCode.equalsIgnoreCase(agentCode)) {
					// Logged user can create only Own Agents
					isUserCreateOperationValid = false;
				}

			} else if ("R".equalsIgnoreCase(loggedInUserLevel)) {
				if (!"R".equalsIgnoreCase(userLevel)) {
					// Logged user can create only Reporting Agents
					isUserCreateOperationValid = false;
				} else if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(loggedUserAgentCode)
						&& loggedUserAgentCode.equalsIgnoreCase(agentCode)) {
					// Logged user can create only Reporting Agents
					isUserCreateOperationValid = false;
				}

			}
		}
		return isUserCreateOperationValid;
	}

	private static void setEnableGSAV2(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_GSA_V2, AppSysParamsUtil.isGSAStructureVersion2Enabled());
	}

	private static void userAgentType(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_USER_AGENT_TYPE,
				((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode());
	}

}
