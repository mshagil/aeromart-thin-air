package com.isa.thinair.xbe.core.web.action.reporting;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.reporting.TaxDetailsReportRH;

/**
 * Struts action class for Tax Details Report
 * 
 * @author asiri
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.REPORTING_TAX_DETAILS_JSP),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowTaxDetailsReportAction extends BaseRequestResponseAwareAction {
	public String execute() throws Exception {
		return TaxDetailsReportRH.execute(request, response);
	}
}