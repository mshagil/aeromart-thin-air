package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * Action to update passenger name information
 * 
 * @author Jagath
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class UpdatePaxNamesAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(UpdatePaxNamesAction.class);

	private String pnr;
	private boolean success = true;
	private String messageTxt;
	private String reservationResponse;
	private String paxNameDetails;
	private boolean groupPNR;
	private String reasonForAllowBlPax;

    public String getReservationResponse() {
        return reservationResponse;
    }

    public void setReservationResponse(String reservationResponse) {
        this.reservationResponse = reservationResponse;
    }

    private String version;
	public String execute() {

		String strForward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			if (paxNameDetails != null) {
				Map<Integer, NameDTO> changedPaxNamesMap = ReservationBeanUtil.getNameChangedPaxMap(paxNameDetails);

				boolean skipDuplicateNameCheck = BasicRH.hasPrivilege(request,
						PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_DUPLI_NAME_SKIP);
				long pnrVersion = 0; 
				if(!groupPNR){
					pnrVersion = Long.parseLong(version);
				}
				
				ModuleServiceLocator.getAirproxyReservationBD().updatePaxNames(changedPaxNamesMap, pnr, pnrVersion,
						AppIndicatorEnum.APP_XBE.toString(), skipDuplicateNameCheck, getTrackInfo(), groupPNR ,reasonForAllowBlPax );
			}
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return strForward;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPaxNameDetails() {
		return paxNameDetails;
	}

	public void setPaxNameDetails(String paxNameDetails) {
		this.paxNameDetails = paxNameDetails;
	}
	
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessageTxt() {
        return messageTxt;
    }

    public void setMessageTxt(String messageTxt) {
        this.messageTxt = messageTxt;
    }

	public boolean isGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getReasonForAllowBlPax() {
		return reasonForAllowBlPax;
	}

	public void setReasonForAllowBlPax(String reasonForAllowBlPax) {
		this.reasonForAllowBlPax = reasonForAllowBlPax;
	}    

}
