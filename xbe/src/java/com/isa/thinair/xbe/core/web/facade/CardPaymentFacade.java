package com.isa.thinair.xbe.core.web.facade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.xbe.api.dto.CurrencyInfoTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.util.PaymentRHUtil;

public class CardPaymentFacade {

	public static Object[] getPaymentGatewayValues(String paxTnxId, ExchangeRateProxy exchangeRateProxy, boolean isOwnTnx)
			throws ModuleException {
		Object[] paymentGatewayValues = new Object[5];

		String paymentGatewayName = ModuleServiceLocator.getPaymentBrokerBD().getPaymentGatewayNameForCCTransaction(
				Integer.valueOf(paxTnxId), isOwnTnx);

		if (paymentGatewayName == null) {
			throw new ModuleException("paymentbroker.refund.missingcardpayment");
		}
		Integer ipgId = new Integer(paymentGatewayName.split("_")[0]);
		String currency = paymentGatewayName.split("_")[1];

		CurrencyInfoTO cardPayCurrencyInfoTO = PaymentRHUtil.getCurrencyInfo(currency, exchangeRateProxy);
		IPGIdentificationParamsDTO ipgIdparamsDTO = PaymentRHUtil.validateAndPrepareIPGIdentificationParamsDTO(
				cardPayCurrencyInfoTO, ipgId);
		Properties props = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdparamsDTO);
		// IPGPaymentOptionDTO paymentOptionDTO = (IPGPaymentOptionDTO) ModuleServiceLocator.getPaymentBrokerBD()
		// .getPaymentGateways(ipgId).get(0);
		List<IPGPaymentOptionDTO> paymentOptionDTOList = (List<IPGPaymentOptionDTO>)ModuleServiceLocator.getPaymentBrokerBD().getPaymentGatewaysForAccountTab(ipgId);

		if (paymentOptionDTOList.size() == 0) {
			throw new ModuleException("paymentbroker.refund.operation.not.supported");
		}

		IPGPaymentOptionDTO paymentOptionDTO = paymentOptionDTOList.get(0);

		paymentGatewayValues[0] = ipgId;
		paymentGatewayValues[1] = paymentOptionDTO.getDescription();
		paymentGatewayValues[2] = currency;
		paymentGatewayValues[3] = cardPayCurrencyInfoTO;
		paymentGatewayValues[4] = props;

		return paymentGatewayValues;
	}

	public static CardPaymentInfo getCardDetails(Integer txnId, boolean isOwnTnx) throws ModuleException {
		ReservationBD reservationBD = ModuleServiceLocator.getReservationBD();

		Collection<Integer> colTnxId = new ArrayList<Integer>();
		colTnxId.add(txnId);
		Map mapCreditCardDetail = reservationBD.getCreditCardInfo(colTnxId, isOwnTnx);
		CardPaymentInfo cardPaymentInfo = (CardPaymentInfo) mapCreditCardDetail.get(txnId);

		return cardPaymentInfo;
	}

}
