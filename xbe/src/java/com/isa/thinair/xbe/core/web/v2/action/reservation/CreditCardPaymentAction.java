package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ServiceTaxCCParamsDTO;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.CurrencyInfoTO;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.facade.CardPaymentFacade;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.xbe.core.web.v2.util.StringUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class CreditCardPaymentAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(CreditCardPaymentAction.class);
	private String ccCharges;
	private String totalPayAmountWithccCharges;
	private String totalPayAmountWithccChargesSelcur;
	private PaymentTO payment;
	private boolean success = true;
	private String messageTxt;
	private CurrencyInfoTO currencyInfo;
	private String hdnIPGID;
	private String ipgDesc;
	private String currencyCode;
	private String cardType;
	private String cardNumber;
	private String cardHolderName;
	private boolean showCardDetails;
	private String tempPayId;
	private String referenceNo;
	private String cardExpiryDate;
	private String cardCVV;
	private String ccdID;
	private boolean allowAnyCardRefund;
	private boolean vouchersRedeemed;

	// For input
	private String hdnMode;
	private String selPax;
	private String selAuthId;
	private String selRefundAmount;
	private String groupPNR;

	// private String selectedFareType;
	private String pmtdate;
	private String selectedCurrency;
	private String sendEmail;
	private String paymentCarrier;
	private String selectedFlightListSize;
	private String isFullRefundOperation;

	private boolean onHoldBooking = false;

	/** params for apply service tax **/

	private FlightSearchDTO searchParams;
	private String selectedFlightList;
	private String pnr;
	private String paxState;
	private String paxCountryCode;
	private boolean paxTaxRegistered;
	private FlightSearchDTO fareQuoteParams;
	private String resContactInfo;
	private String flightRPHList;
	private String flightSegmentToList;
	private String paxWiseAnci;
	private String nameChangeFlightSegmentList;
	private String resPaxs;
	private boolean addSegment;

	public String execute() {
		String strReturn = S2Constants.Result.SUCCESS;
		boolean isGroupPnr = ReservationUtil.isGroupPnr(groupPNR);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			if (hdnMode != null && hdnMode.equals(WebConstants.COMMAND_REFUND)) {
				// Credit card refunds can be made only for the airline which accepted
				// the credit card payments.
				if (!paymentCarrier.equals(AppSysParamsUtil.getDefaultCarrierCode())) {
					throw new ModuleException("paymentbroker.refund.operation.not.supported");
				}
				XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
						S2Constants.Session_Data.XBE_SES_RESDATA);

				ReservationProcessParams processParams = new ReservationProcessParams(request, resInfo != null
						? resInfo.getReservationStatus()
						: null, null, true, false, null, null, false, false);

				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(getApplicableExchangeDate());
				if (isGroupPnr) {
					Map<String, String> carrierWiseRefNo = StringUtil.carrierWiseSplite(referenceNo, "|", "=");
					setGroupResRefundData(groupPNR, selPax, selAuthId, selRefundAmount, carrierWiseRefNo, processParams,
							exchangeRateProxy);
				} else {
					setOwnRefundData(referenceNo, processParams, exchangeRateProxy);
				}
			} else {
				setDisplayData(request);
			}
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}
		return strReturn;
	}

	private Date getApplicableExchangeDate() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date applicableExRateTimestamp = sdf.parse(pmtdate);

		return applicableExRateTimestamp;
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private void setDisplayData(HttpServletRequest request) throws ModuleException, ParseException,
			org.json.simple.parser.ParseException, Exception {
		Integer ipgId = null;

		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);

		if (request.getParameter(WebConstants.REQ_HDN_IPG_ID) != null) {
			ipgId = new Integer(request.getParameter(WebConstants.REQ_HDN_IPG_ID));
		} else {
			ipgId = new Integer(hdnIPGID);
		}
		IPGPaymentOptionDTO ipgPaymentOptionDTO = new IPGPaymentOptionDTO();
		ipgPaymentOptionDTO.setSelCurrency(selectedCurrency);
		ipgPaymentOptionDTO.setModuleCode("XBE");
		ipgPaymentOptionDTO.setPaymentGateway(ipgId);
		List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getPaymentGateways(ipgPaymentOptionDTO);
		String ipgCurrencyCode = pgwList.get(0).getSelCurrency();
		// String ipgCurrencyCode = ((IPGPaymentOptionDTO)(ModuleServiceLocator.getPaymentBrokerBD()
		// .getPaymentGateways(ipgId).toArray()[0])).getBaseCurrency();
		currencyInfo = PaymentUtil.getCurrencyInfo(ipgCurrencyCode, new Date());
		// TODO add creditCard details to populate

		BigDecimal totalPaymentAmountExcludingCCCharge = PaymentUtil.getTotalPaymentAmountWithoutTransactionFee(request, payment);
		BigDecimal ccChargeAmount = BigDecimal.ZERO;

		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
				S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		ServiceTaxCCParamsDTO serviceTaxCCParamsDTO = new ServiceTaxCCParamsDTO(searchParams, fareQuoteParams, paxWiseAnci, resPaxs,
				resContactInfo, paxState, paxCountryCode, paxTaxRegistered, flightRPHList, selectedFlightList,
				flightSegmentToList, nameChangeFlightSegmentList, addSegment, groupPNR);

		// [0] - CC Charge , [1] - totalPaymentAmountExcludingCCCharge
		BigDecimal[] amounts = calculateCCChargeAndApply(totalPaymentAmountExcludingCCCharge, bookingShoppingCart, resInfo,
				serviceTaxCCParamsDTO);
		ccChargeAmount = amounts[0];
		totalPaymentAmountExcludingCCCharge = amounts[1];

		BigDecimal totalPayAmount = AccelAeroCalculator.add(totalPaymentAmountExcludingCCCharge, ccChargeAmount);

		ccCharges = AccelAeroCalculator.formatAsDecimal(ccChargeAmount);
		totalPayAmountWithccCharges = AccelAeroCalculator.formatAsDecimal(totalPayAmount);

		totalPayAmountWithccChargesSelcur = AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
				getSelCurrencyMultiplyingExchangeRate(currencyInfo.getCurrencyCode()), totalPayAmount));
		sendEmail = request.getParameter("chkEmail");

	}
	
	private Set<Integer> getPayablePassengerSequence() {

		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);	
		Set<Integer> paxSequences = new HashSet();
		Map<Integer, BigDecimal> passengerPayments = resInfo.getPaxCreditMap();
		
		if (passengerPayments != null) {
			passengerPayments.forEach((key,value) -> {
					if (AccelAeroCalculator.isGreaterThan(value, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						paxSequences.add(key);
					}
			});		
			
		}	
		
		return paxSequences;
	}

	private BigDecimal[] calculateCCChargeAndApply(BigDecimal totalPaymentAmountExcludingCCCharge,
			BookingShoppingCart bookingShoppingCart, XBEReservationInfoDTO resInfo, ServiceTaxCCParamsDTO serviceTaxCCParamsDTO)
			throws ParseException, ModuleException, org.json.simple.parser.ParseException, Exception {
		// [0] - CC Charge , [1] - totalPaymentAmountExcludingCCCharge
		
		BigDecimal[] amounts = new BigDecimal[2];

		BigDecimal ccChargeAmount = BigDecimal.ZERO;

		String reservationStatus = resInfo.getReservationStatus();
		boolean isPaymentCarrierSame = resInfo.getMarketingAirlineCode() == null ? true : resInfo.getMarketingAirlineCode()
				.equals(AppSysParamsUtil.getDefaultCarrierCode());
		boolean isOnHold = ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationStatus);
		boolean isConfirmed = ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservationStatus);
		
		int payableAdultPaxCount = bookingShoppingCart.getPayablePaxCount();
		int payableInfantPaxCount = 0;
		
		if ("".equals(pnr)) {
			payableInfantPaxCount = bookingShoppingCart.getPayableInfantPaxCount();
		}

		if (!AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
			ccChargeAmount = getCCChargeAmount(totalPaymentAmountExcludingCCCharge);

			BigDecimal totalServiceTaxAmount = ReservationUtil.calculateServiceTaxForTransactionFee(bookingShoppingCart, ccChargeAmount,
					resInfo.getTransactionId(), serviceTaxCCParamsDTO, getTrackInfo(), EXTERNAL_CHARGES.CREDIT_CARD, payableAdultPaxCount,
					payableInfantPaxCount, getPayablePassengerSequence(), false);
			ccChargeAmount = AccelAeroCalculator.add(ccChargeAmount, totalServiceTaxAmount);
		}

		if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
				&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, RequestHandlerUtil.getAgentCode(request)) && !isConfirmed) {

			if (bookingShoppingCart.getSelectedExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD) != null) {
				ccChargeAmount = bookingShoppingCart.getSelectedExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD).getAmount();
				BigDecimal totalServiceTaxAmount = ReservationUtil.calculateServiceTaxForTransactionFee(bookingShoppingCart, ccChargeAmount,
						resInfo.getTransactionId(), serviceTaxCCParamsDTO, getTrackInfo(), EXTERNAL_CHARGES.CREDIT_CARD, payableAdultPaxCount,
						payableInfantPaxCount, getPayablePassengerSequence(), false);
				ccChargeAmount = AccelAeroCalculator.add(ccChargeAmount, totalServiceTaxAmount);

				if (bookingShoppingCart.getSelectedExternalCharge(EXTERNAL_CHARGES.JN_OTHER) != null
						&& bookingShoppingCart.getSelectedExternalCharge(EXTERNAL_CHARGES.JN_OTHER).getAmount()
								.compareTo(BigDecimal.ZERO) > 0) {
					BigDecimal ccCharge = bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.CREDIT_CARD)
							.getAmount();
					BigDecimal jnOther = bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.JN_OTHER)
							.getAmount();

					bookingShoppingCart.getSelectedExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD).setAmount(ccCharge);
					bookingShoppingCart.getSelectedExternalCharge(EXTERNAL_CHARGES.JN_OTHER).setAmount(jnOther);
					if (!isConfirmed) {
						ccChargeAmount = bookingShoppingCart.getSelectedExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD).getAmount()
								.add(jnOther);
						totalPaymentAmountExcludingCCCharge = totalPaymentAmountExcludingCCCharge.subtract(jnOther);
					}
				}

				// when it is dry/interline
				if (resInfo.getMarketingAirlineCode() != null) {
					// isForceConfirmed is used to identify modify booking
					if ((isOnHold && isPaymentCarrierSame) || (isOnHold && !isPaymentCarrierSame) || isConfirmed) {
						ccChargeAmount = BigDecimal.ZERO;
					}
				} else {
					if (isOnHold) {
						ccChargeAmount = ReservationUtil.addTaxToServiceCharge(bookingShoppingCart, ccChargeAmount);
					} else if (isConfirmed) { // modify booking
						ccChargeAmount = BigDecimal.ZERO;
					}
				}

			} else {
				ccChargeAmount = ReservationUtil.addTaxToServiceCharge(bookingShoppingCart, ccChargeAmount);
			}
		} else {
			ccChargeAmount = ReservationUtil.addTaxToServiceCharge(bookingShoppingCart, ccChargeAmount);
		}
		
		amounts[0] = ccChargeAmount;
		amounts[1] = totalPaymentAmountExcludingCCCharge;

		return amounts;
	}

	private void
			setOwnRefundData(String referenceNo, ReservationProcessParams processParams, ExchangeRateProxy exchangeRateProxy)
					throws ModuleException, ParseException {
		Object[] pgObj = CardPaymentFacade.getPaymentGatewayValues(referenceNo, exchangeRateProxy, true);
		this.currencyCode = AppSysParamsUtil.getBaseCurrency();
		this.currencyInfo = (CurrencyInfoTO) pgObj[3];
		this.totalPayAmountWithccCharges = AccelAeroCalculator.formatAsDecimal(new BigDecimal(selRefundAmount));
		this.totalPayAmountWithccChargesSelcur = AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(new BigDecimal(
				selRefundAmount), currencyInfo.getCurrencyExchangeRate()));
		CardPaymentInfo cardPaymentInfo = CardPaymentFacade.getCardDetails(Integer.valueOf(referenceNo), true);
		this.cardHolderName = cardPaymentInfo.getName();
		this.cardNumber = cardPaymentInfo.getNoLastDigits();
		this.cardType = Integer.toString(cardPaymentInfo.getType());
		this.cardCVV = cardPaymentInfo.getSecurityCode();
		this.allowAnyCardRefund = processParams.isAllowAnyCreditCardRefund();
		String eDate = PlatformUtiltiies.nullHandler(cardPaymentInfo.getEDate());
		if (eDate.length() > 0) {
			this.cardExpiryDate = eDate.substring(2, 4) + eDate.substring(0, 2);
		} else {
			this.cardExpiryDate = "";
		}
		this.showCardDetails = Boolean.valueOf(((Properties) pgObj[4]).getProperty("refundWithoutCardDetails"));
		this.hdnIPGID = ((Integer) pgObj[0]).toString();
		this.tempPayId = Integer.toString(cardPaymentInfo.getTemporyPaymentId());
		this.ccdID = cardPaymentInfo.getCcdId().toString();

	}

	private void setGroupResRefundData(String groupPnr, String selectedPax, String selAuthId, String selRefundAmount,
			Map<String, String> carrierViseRefNo, ReservationProcessParams processParams, ExchangeRateProxy exchangeRateProxy)
			throws ModuleException, ParseException {

		if (carrierViseRefNo.keySet().contains(AppSysParamsUtil.getDefaultCarrierCode())) {
			// we have group reservation with own carrier CC payments.
			setOwnRefundData(carrierViseRefNo.get(AppSysParamsUtil.getDefaultCarrierCode()), processParams, exchangeRateProxy);
		} else {
			// CC payment for only other carrier payments
			carrierViseRefNo.remove(AppSysParamsUtil.getDefaultCarrierCode());
			String refNo = carrierViseRefNo.values().iterator().next(); // get one of the ExternalPaxTnx tnx id as the
																		// refNo
			Object[] pgObj = CardPaymentFacade.getPaymentGatewayValues(refNo, exchangeRateProxy, false);
			this.currencyInfo = (CurrencyInfoTO) pgObj[3];
			this.currencyCode = AppSysParamsUtil.getBaseCurrency();
			this.totalPayAmountWithccCharges = AccelAeroCalculator.formatAsDecimal(new BigDecimal(selRefundAmount));
			this.totalPayAmountWithccChargesSelcur = AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
					new BigDecimal(selRefundAmount), currencyInfo.getCurrencyExchangeRate()));
			CardPaymentInfo cardPaymentInfo = CardPaymentFacade.getCardDetails(Integer.valueOf(refNo), false);
			this.cardHolderName = cardPaymentInfo.getName();
			this.cardNumber = cardPaymentInfo.getNoLastDigits();
			this.cardType = Integer.toString(cardPaymentInfo.getType());
			this.cardCVV = cardPaymentInfo.getSecurityCode();
			this.allowAnyCardRefund = processParams.isAllowAnyCreditCardRefund();
			String eDate = PlatformUtiltiies.nullHandler(cardPaymentInfo.getEDate());
			if (eDate.length() > 0) {
				this.cardExpiryDate = eDate.substring(2, 4) + eDate.substring(0, 2);
			} else {
				this.cardExpiryDate = "";
			}
			this.showCardDetails = Boolean.valueOf(((Properties) pgObj[4]).getProperty("refundWithoutCardDetails"));
			this.hdnIPGID = ((Integer) pgObj[0]).toString();
			this.tempPayId = Integer.toString(cardPaymentInfo.getTemporyPaymentId());
			this.ccdID = cardPaymentInfo.getCcdId().toString();// this.ccdID = "";

			Collection<String[]> cardTypes = SelectListGenerator.createCreditCardTypesForPg(new Integer(hdnIPGID));
			for (String[] strings : cardTypes) {
				String cType = strings[0];
				String cardTypeDesc = strings[1];
				if (cType.equals(this.cardType)) {
					this.ipgDesc = cardTypeDesc;
					break;
				}
			}
		}
	}

//	private BigDecimal getTotalPaymentAmountExcludingCCCharge() {
//		BigDecimal totWOcc;
//		if ("FORCE".equals(payment.getMode())) {
//			totWOcc = new BigDecimal(payment.getAmount());
//		} else {
//			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
//					S2Constants.Session_Data.LCC_SES_BOOKING_CART);
//			if (bookingShoppingCart.getTotalPayable().compareTo(BigDecimal.ZERO) == 0) {
//				totWOcc = new BigDecimal(payment.getAmount());
//			} else if (bookingShoppingCart.getTotalPayable().compareTo(BigDecimal.ZERO) < 0) {
//				totWOcc = new BigDecimal(payment.getAmount());
//			} else if (bookingShoppingCart.getTotalPayable().compareTo(new BigDecimal(payment.getAmount())) > 0) {
//				totWOcc = new BigDecimal(payment.getAmount());
//			} else {
//				totWOcc = bookingShoppingCart.getTotalPayable();
//			}
//
//			if (bookingShoppingCart.getPayByVoucherInfo() != null) {
//				BigDecimal toPay = bookingShoppingCart.getPayByVoucherInfo().getRedeemedTotal();
//				if (totWOcc.compareTo(toPay) > -1) {
//					totWOcc = AccelAeroCalculator.subtract(totWOcc, toPay);
//				} else {
//					totWOcc = BigDecimal.ZERO;
//				}
//			}
//
//		}
//		return totWOcc;
//	}

	private BigDecimal getSelCurrencyMultiplyingExchangeRate(String currency) throws ModuleException {

		CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
				.getCurrencyExchangeRate(currency, ApplicationEngine.XBE);
		return currencyExchangeRate.getMultiplyingExchangeRate();
	}

	private BigDecimal getCCChargeAmount(BigDecimal totalPaymentAmountExcludingCCCharge) throws ModuleException {
		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
				S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		ExternalChgDTO externalChgDTO = bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.CREDIT_CARD);
		externalChgDTO = (ExternalChgDTO) externalChgDTO.clone();

		if (externalChgDTO.getAmount() == null || BigDecimal.ZERO.equals(externalChgDTO.getAmount())) {
			if (externalChgDTO.isRatioValueInPercentage()) {
				externalChgDTO.calculateAmount(totalPaymentAmountExcludingCCCharge);
			} else {
				externalChgDTO
						.calculateAmount(bookingShoppingCart.getPayablePaxCount(), Integer.parseInt(selectedFlightListSize));
			}
		}

		// Updating the booking shopping cart
		bookingShoppingCart.addSelectedExternalCharge(externalChgDTO);

		return externalChgDTO.getAmount();
	}

	public String getCcCharges() {
		return ccCharges;
	}

	public PaymentTO getPayment() {
		return payment;
	}

	public void setPayment(PaymentTO payment) {
		this.payment = payment;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setSelPax(String selPax) {
		this.selPax = selPax;
	}

	public void setSelAuthId(String selAuthId) {
		this.selAuthId = selAuthId;
	}

	public void setTotalPayAmountWithccCharges(String totalPayAmountWithccCharges) {
		this.totalPayAmountWithccCharges = totalPayAmountWithccCharges;
	}

	public void setSelRefundAmount(String selRefundAmount) {
		this.selRefundAmount = selRefundAmount;
	}

	public void setTotalPayAmountWithccChargesSelcur(String totalPayAmountWithccChargesSelcur) {
		this.totalPayAmountWithccChargesSelcur = totalPayAmountWithccChargesSelcur;
	}

	public void setShowCardDetails(boolean showCardDetails) {
		this.showCardDetails = showCardDetails;
	}

	// getters
	public boolean isShowCardDetails() {
		return showCardDetails;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public CurrencyInfoTO getCurrencyInfo() {
		return currencyInfo;
	}

	public String getTotalPayAmountWithccCharges() {
		return totalPayAmountWithccCharges;
	}

	public String getTotalPayAmountWithccChargesSelcur() {
		return totalPayAmountWithccChargesSelcur;
	}

	public String getHdnIPGID() {
		return hdnIPGID;
	}

	public String getIpgDesc() {
		return ipgDesc;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public String getCardType() {
		return cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public String getTempPayId() {
		return tempPayId;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public void setCardExpiryDate(String cardExpiryDate) {
		this.cardExpiryDate = cardExpiryDate;
	}

	public void setCardCVV(String cardCVV) {
		this.cardCVV = cardCVV;
	}

	public void setPmtdate(String pmtdate) {
		this.pmtdate = pmtdate;
	}

	public String getCardExpiryDate() {
		return cardExpiryDate;
	}

	public String getCardCVV() {
		return cardCVV;
	}

	public String getCcdID() {
		return ccdID;
	}

	public void setCcdID(String ccdID) {
		this.ccdID = ccdID;
	}

	/**
	 * @return the allowAnyCardRefund
	 */
	public boolean isAllowAnyCardRefund() {
		return allowAnyCardRefund;
	}

	/**
	 * @param allowAnyCardRefund
	 *            the allowAnyCardRefund to set
	 */
	public void setAllowAnyCardRefund(boolean allowAnyCardRefund) {
		this.allowAnyCardRefund = allowAnyCardRefund;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	public String getSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(String sendEmail) {
		this.sendEmail = sendEmail;
	}

	public String getSelectedFlightListSize() {
		return selectedFlightListSize;
	}

	public void setSelectedFlightListSize(String selectedFlightListSize) {
		this.selectedFlightListSize = selectedFlightListSize;
	}

	/**
	 * @param paymentCarrier
	 *            the paymentCarrier to set
	 */
	public void setPaymentCarrier(String paymentCarrier) {
		this.paymentCarrier = paymentCarrier;
	}

	/**
	 * @return the isFullRefundOperation
	 */
	public String getIsFullRefundOperation() {
		return isFullRefundOperation;
	}

	/**
	 * @param isFullRefundOperation
	 *            the isFullRefundOperation to set
	 */
	public void setIsFullRefundOperation(String isFullRefundOperation) {
		this.isFullRefundOperation = isFullRefundOperation;
	}

	public boolean isVouchersRedeemed() {
		return vouchersRedeemed;
	}

	public void setVouchersRedeemed(boolean vouchersRedeemed) {
		this.vouchersRedeemed = vouchersRedeemed;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setPaxState(String paxState) {
		this.paxState = paxState;
	}

	public void setPaxCountryCode(String paxCountryCode) {
		this.paxCountryCode = paxCountryCode;
	}

	public void setPaxTaxRegistered(boolean paxTaxRegistered) {
		this.paxTaxRegistered = paxTaxRegistered;
	}

	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public String getResContactInfo() {
		return resContactInfo;
	}

	public void setResContactInfo(String resContactInfo) {
		this.resContactInfo = resContactInfo;
	}

	public String getFlightRPHList() {
		return flightRPHList;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public String getFlightSegmentToList() {
		return flightSegmentToList;
	}

	public void setFlightSegmentToList(String flightSegmentToList) {
		this.flightSegmentToList = flightSegmentToList;
	}

	public String getPaxWiseAnci() {
		return paxWiseAnci;
	}

	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public String getNameChangeFlightSegmentList() {
		return nameChangeFlightSegmentList;
	}

	public void setNameChangeFlightSegmentList(String nameChangeFlightSegmentList) {
		this.nameChangeFlightSegmentList = nameChangeFlightSegmentList;
	}

	public String getResPaxs() {
		return resPaxs;
	}

	public void setResPaxs(String resPaxs) {
		this.resPaxs = resPaxs;
	}

	public boolean isAddSegment() {
		return addSegment;
	}

	public void setAddSegment(boolean addSegment) {
		this.addSegment = addSegment;
	}

}