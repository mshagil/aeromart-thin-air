/**
 * 
 */
package com.isa.thinair.xbe.core.web.action.requestmanagement;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * @author suneth
 *
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.RequestManagement.REQUEST_HISTORY),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })

public class ShowRequestHistoryAction extends BaseRequestAwareAction{

	private int requestId;
	
	public String execute() {
		
		return S2Constants.Result.SUCCESS;
		
	}

	/**
	 * @return the requestId
	 */
	public int getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
	
	
	
}