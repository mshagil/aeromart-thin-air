package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.Constants.ReservationFlow;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.FltSegBuilder;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "", params = { "excludeProperties",
		"currencyExchangeRate\\.currency, countryCurrExchangeRate\\.currency" })
public class ModifyAnciGSTCalculationAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ModifyAnciGSTCalculationAction.class);

	private boolean success = true;

	private String messageTxt;

	private String pnr;

	private String groupPNR;

	private String insurableFltRefNumbers;

	private String modifySegment;

	private String flightRPHList;

	private String paxWiseAnci;

	private String insurances;

	private CurrencyExchangeRate currencyExchangeRate;

	private String modifedSegmentsCount;

	private CurrencyExchangeRate countryCurrExchangeRate;

	private BigDecimal totalServiceTaxAmountForAncillary = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalServiceTaxAmountForFare = AccelAeroCalculator.getDefaultBigDecimalZero();

	public String execute() {

		@SuppressWarnings("unused")
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			SYSTEM system = SYSTEM.INT;
			if (!isGroupPNR) {
				system = SYSTEM.AA;
				groupPNR = pnr;
			}

			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

			CommonReservationContactInfo contactInfo = ReservationUtil
					.transformJsonContactInfo(request.getParameter("resContactInfo"));

			UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
			String strCurrency = up.getAgentCurrencyCode();
			if (strCurrency == null || strCurrency.equals("")) {
				strCurrency = AppSysParamsUtil.getBaseCurrency();
			}
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(strCurrency, ApplicationEngine.XBE);
			this.countryCurrExchangeRate = PaymentUtil.getBSPCountryCurrencyExchgRate(request);

			FltSegBuilder fltSegBuilder = new FltSegBuilder(flightRPHList);
			List<FlightSegmentTO> flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
			SortUtil.sortFlightSegByDepDate(flightSegmentTOs);

			List<LCCInsuranceQuotationDTO> insuranceQuotations = AncillaryJSONUtil.getSelectedInsuranceQuotations(insurances,
					flightSegmentTOs, insurableFltRefNumbers);

			if (insuranceQuotations != null && !insuranceQuotations.isEmpty()) {

				BigDecimal insuranceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCInsuranceQuotationDTO insuranceQuotation : insuranceQuotations) {
					insuranceAmount = AccelAeroCalculator.add(insuranceAmount, insuranceQuotation.getQuotedTotalPremiumAmount());
				}

				ExternalChgDTO insChgDTO = new ExternalChgDTO();
				insChgDTO.setAmount(insuranceAmount);
				insChgDTO.setExternalChargesEnum(EXTERNAL_CHARGES.INSURANCE);

				bookingShoppingCart.addSelectedExternalCharge(insChgDTO);
				bookingShoppingCart.setInsuranceQuotes(insuranceQuotations);

			} else {
				bookingShoppingCart.removeSelectedExternalCharge(EXTERNAL_CHARGES.INSURANCE);
				bookingShoppingCart.setInsuranceQuotes(null);
			}

			List<ReservationPaxTO> paxList = AncillaryJSONUtil.extractReservationPax(paxWiseAnci, insuranceQuotations,
					ApplicationEngine.XBE, flightSegmentTOs, null, null);

			SSRServicesUtil.checkMedicalSsrInAllSegments(paxList, getFlightSegmentsWithoutBusSegments(flightSegmentTOs),
					ReservationInternalConstants.SsrTypes.MEDA);

			SSRServicesUtil.setSegmentSeqForSSR(paxList, flightSegmentTOs, null);
			bookingShoppingCart.setPaxList(paxList);

			BookingUtil.applyAncillaryTax(bookingShoppingCart, flightSegmentTOs.get(0), null);

			Map<String, BigDecimal> serviceTaxAmountMap = BookingUtil.applyServiceTaxAnciModifyDisplay(
					bookingShoppingCart.getPaxList(), flightSegmentTOs, contactInfo, getTrackInfo(), system, ReservationFlow.MODIFY);

			setTotalServiceTaxAmountForAncillary(
					serviceTaxAmountMap.get(ReservationInternalConstants.ServiceTaxIdentificationConstant.ANCI));
			setTotalServiceTaxAmountForFare(
					serviceTaxAmountMap.get(ReservationInternalConstants.ServiceTaxIdentificationConstant.FARE));

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error("Generic exception caught.", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private List<FlightSegmentTO> getFlightSegmentsWithoutBusSegments(List<FlightSegmentTO> flightSegmentTOs)
			throws ModuleException {
		List<FlightSegmentTO> actualFlightSegmentList = new ArrayList<>();
		for (Iterator<FlightSegmentTO> flightSegmentListIterator = flightSegmentTOs.iterator(); flightSegmentListIterator
				.hasNext();) {
			FlightSegmentTO flightSegment = flightSegmentListIterator.next();
			if (!ModuleServiceLocator.getAirportBD().isBusSegment(flightSegment.getSegmentCode())) {
				actualFlightSegmentList.add(flightSegment);
			}
		}
		return actualFlightSegmentList;
	}

	@Override
	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = super.getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		trackInfoDTO.setPaymentAgent(((UserPrincipal) request.getUserPrincipal()).getAgentCode());
		return trackInfoDTO;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getInsurableFltRefNumbers() {
		return insurableFltRefNumbers;
	}

	public void setInsurableFltRefNumbers(String insurableFltRefNumbers) {
		this.insurableFltRefNumbers = insurableFltRefNumbers;
	}

	public String getModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(String modifySegment) {
		this.modifySegment = modifySegment;
	}

	public String getFlightRPHList() {
		return flightRPHList;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public String getPaxWiseAnci() {
		return paxWiseAnci;
	}

	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public String getInsurances() {
		return insurances;
	}

	public void setInsurances(String insurances) {
		this.insurances = insurances;
	}

	public CurrencyExchangeRate getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	public void setCurrencyExchangeRate(CurrencyExchangeRate currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public String getModifedSegmentsCount() {
		return modifedSegmentsCount;
	}

	public void setModifedSegmentsCount(String modifedSegmentsCount) {
		this.modifedSegmentsCount = modifedSegmentsCount;
	}

	public CurrencyExchangeRate getCountryCurrExchangeRate() {
		return countryCurrExchangeRate;
	}

	public void setCountryCurrExchangeRate(CurrencyExchangeRate countryCurrExchangeRate) {
		this.countryCurrExchangeRate = countryCurrExchangeRate;
	}

	public BigDecimal getTotalServiceTaxAmountForAncillary() {
		return totalServiceTaxAmountForAncillary;
	}

	public void setTotalServiceTaxAmountForAncillary(BigDecimal totalServiceTaxAmountForAncillary) {
		this.totalServiceTaxAmountForAncillary = totalServiceTaxAmountForAncillary;
	}

	public BigDecimal getTotalServiceTaxAmountForFare() {
		return totalServiceTaxAmountForFare;
	}

	public void setTotalServiceTaxAmountForFare(BigDecimal totalServiceTaxAmountForFare) {
		this.totalServiceTaxAmountForFare = totalServiceTaxAmountForFare;
	}

}
