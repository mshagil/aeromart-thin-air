package com.isa.thinair.xbe.core.web.action.palcal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.palcal.SendPALCALRequestHandler;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.PnlAdl.PRINT_PNL_ADL),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowPrintPALCALAction extends BaseRequestAwareAction {
	private static final Log log = LogFactory.getLog(ShowPrintPALCALAction.class);

	public String execute() {
		log.debug("Inside the ShowSendPALCALAction.execute()...");
		return SendPALCALRequestHandler.execute(request);
	}
}
