package com.isa.thinair.xbe.core.web.v2.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

public class FltSegBuilder {
	private boolean isReturn = false;
	private List<FlightSegmentTO> selectedFlightSegments = null;
	private SYSTEM system = null;
	private String fromAirport = null;
	private String toAirport = null;
	private String classOfService = null;

	public FltSegBuilder(String jsonSegmentStr) throws ParseException, XBERuntimeException {
		this(jsonSegmentStr, false);
	}

	public FltSegBuilder(String jsonSegmentStr, boolean skipDerivations) throws ParseException, XBERuntimeException {
		this(jsonSegmentStr, skipDerivations, false, false);
	}

	public FltSegBuilder(String jsonSegmentStr, boolean skipDerivations, boolean validateFltSeg, boolean isForInsAvailability)
			throws ParseException, XBERuntimeException {
		List<FlightSegmentTO> fsTOList = new ArrayList<FlightSegmentTO>();
		Boolean isReturn = new Boolean(false);
		String strSystem = null;
		FlightSegmentTO fs = null;
		IFlightSegment ls = null;

		if (jsonSegmentStr != null && !"".equals(jsonSegmentStr) && !"[]".equals(jsonSegmentStr)) {
			
			Map<String, String> busAirCarrierCodes = null;
			try {
				busAirCarrierCodes = ModuleServiceLocator.getFlightServiceBD().getBusAirCarrierCodes();
			} catch (ModuleException e) {
				e.printStackTrace();
			}
			
			JSONParser parser = new JSONParser();
			JSONArray jSegments = (JSONArray) parser.parse(jsonSegmentStr);
			Iterator<?> iterator = jSegments.iterator();
			while (iterator.hasNext()) {
				JSONObject jSeg = (JSONObject) iterator.next();
				if (jSeg.isEmpty())
					continue;
				Date dtime = parseJsonDate(jSeg.get("departureDate").toString());
				Date atime = parseJsonDate(jSeg.get("arrivalDate").toString());
				Date dZulu = parseJsonDate(jSeg.get("departureDateZulu").toString());
				Date aZulu = parseJsonDate(jSeg.get("arrivalDateZulu").toString());
				String segCode = jSeg.get("segmentCode").toString();
				String flightRefNo = jSeg.get("flightSegmentRefNumber").toString();
				if (flightRefNo != null && flightRefNo.indexOf("#") != -1) {
					flightRefNo = flightRefNo.split("#")[0];
				}

				String flightNo = jSeg.get("flightNo").toString();
				String carrierCode = jSeg.get("carrierCode").toString();
				String cabinClass = jSeg.get("cabinClassCode").toString();
				String logicalCabinClass = jSeg.get("logicalCabinClass").toString();
				strSystem = jSeg.get("system").toString();
				String strStaus = jSeg.get("status").toString();
				String strRetFlag = jSeg.get("returnFlag").toString();
				String bookingType = jSeg.get("bookingType").toString();
				String segmentSeq = jSeg.get("segmentSeq").toString();
				
				boolean flownSegment = jSeg.get("flownSegment").toString().equals("true");

				String pnrSegId = null;
				if (BeanUtils.nullHandler(jSeg.get("pnrSegID")).length() > 0) {
					pnrSegId = BeanUtils.nullHandler(jSeg.get("pnrSegID"));
				}

				String baggageONDGroupId = null;
				if (BeanUtils.nullHandler(jSeg.get("baggageONDGroupId")).length() > 0) {
					baggageONDGroupId = BeanUtils.nullHandler(jSeg.get("baggageONDGroupId"));
				}

				Boolean returnFlag = false;
				if ("Y".equals(strRetFlag)) {
					returnFlag = true;
				}

				int ondSequence = 0;
				String bookingClass = null;

				if (jSeg.get("fareTO") != null) {
					ondSequence = Integer.parseInt(((JSONObject) jSeg.get("fareTO")).get("ondSequence").toString());
					bookingClass = ((JSONObject) jSeg.get("fareTO")).get("bookingClassCode").toString();
				} else if (jSeg.get("journeySequence") != null) {
					ondSequence = Integer.parseInt(jSeg.get("journeySequence").toString()) - 1;
				}
				
				String csOcCarrierCode = null;
				if (jSeg.get("csOcCarrierCode") != null){
					csOcCarrierCode = jSeg.get("csOcCarrierCode").toString();
				}

				FlightSegmentTO fltSeg = new FlightSegmentTO();
				fltSeg.setArrivalDateTime(atime);
				fltSeg.setDepartureDateTime(dtime);
				fltSeg.setArrivalDateTimeZulu(aZulu);
				fltSeg.setDepartureDateTimeZulu(dZulu);
				fltSeg.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRefNo));
				fltSeg.setCabinClassCode(cabinClass);
				fltSeg.setLogicalCabinClassCode(logicalCabinClass);
				fltSeg.setFlightNumber(flightNo);
				fltSeg.setFlightRefNumber(flightRefNo);
				
				if (busAirCarrierCodes != null && busAirCarrierCodes.get(carrierCode) != null) {//AEROMART-3141
					fltSeg.setOperatingAirline(busAirCarrierCodes.get(carrierCode));
				} else {
					fltSeg.setOperatingAirline(carrierCode);
				}
				
				fltSeg.setSegmentCode(segCode);
				fltSeg.setReturnFlag(returnFlag);
				fltSeg.setBookingType(bookingType);
				fltSeg.setPnrSegId(pnrSegId);
				fltSeg.setBaggageONDGroupId(baggageONDGroupId);
				fltSeg.setForInsuranceAvailability(isForInsAvailability);
				fltSeg.setOndSequence(ondSequence);
				fltSeg.setCsOcCarrierCode(csOcCarrierCode);
				fltSeg.setFlownSegment(flownSegment);
				fltSeg.setBookingClass(bookingClass);

				if (jSeg.get("domesticFlight") != null && !"".equals(jSeg.get("domesticFlight"))) {
					fltSeg.setDomesticFlight(Boolean.parseBoolean(jSeg.get("domesticFlight").toString()));
				}

				if (segmentSeq != null && !"".equals(segmentSeq)) {
					fltSeg.setSegmentSequence(new Integer(segmentSeq));
				}

				if (strStaus == null || strStaus.equals("CNF")) {
					fsTOList.add(fltSeg);
				}

				/*
				 * if(returnFlag){ isReturn = true; }else{ if(fs == null ||
				 * fs.getDepartureDateTime().compareTo(fltSeg.getDepartureDateTime()) > 0){ fs = fltSeg; } if(ls == null
				 * || ls.getDepartureDateTime().compareTo(fltSeg.getDepartureDateTime()) < 0){ ls = fltSeg; } }
				 */

				// TODO: Move getOutboundInboundList method from AncillaryDTOUtil to a generic Util class

			}
			Object[] outboundInboundFlifghts = AncillaryDTOUtil.getOutboundInboundList(fsTOList);
			List<FlightSegmentTO> outboundFlightSegmentTOs = (List<FlightSegmentTO>) outboundInboundFlifghts[0];
			List<FlightSegmentTO> inboundFlightSegmentTOs = (List<FlightSegmentTO>) outboundInboundFlifghts[1];

			if (outboundFlightSegmentTOs.size() > 0) {
				fs = outboundFlightSegmentTOs.get(0);
				ls = outboundFlightSegmentTOs.get(outboundFlightSegmentTOs.size() - 1);
			}

			IFlightSegment inboundFlightSegmentTO = inboundFlightSegmentTOs.size() == 0 ? null : inboundFlightSegmentTOs
					.get(inboundFlightSegmentTOs.size() - 1);
			if (inboundFlightSegmentTO != null) {
				// ls = inboundFlightSegmentTOs.get(inboundFlightSegmentTOs.size()-1);
				isReturn = true;
			}
			if (validateFltSeg && fs == null) {
				throw new XBERuntimeException(WebConstants.NO_OUTBOUND_FOUND);
			}
		}
		this.selectedFlightSegments = fsTOList;
		if (fsTOList.size() > 0 && !skipDerivations) {
			this.isReturn = isReturn;
			this.system = SYSTEM.getEnum(strSystem);
			this.fromAirport = fs.getSegmentCode().split("/")[0];
			String[] segments = ls.getSegmentCode().split("/");
			this.toAirport = segments[segments.length - 1];
			this.classOfService = fs.getCabinClassCode();
		}
	}

	private Date parseJsonDate(String jsonDate) {
		Date d = null;
		if (jsonDate == null || "".equals(jsonDate)) {
			return d;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			d = sdf.parse(jsonDate);
		} catch (java.text.ParseException e) {
			d = null;
		}
		return d;
	}

	public boolean isReturn() {
		return this.isReturn;
	}

	public List<FlightSegmentTO> getSelectedFlightSegments() {
		return this.selectedFlightSegments;
	}

	public SYSTEM getSystem() {
		return this.system;
	}

	public String getFromAirport() {
		return fromAirport;
	}

	public String getToAirport() {
		return toAirport;
	}

	public String getClassOfService() {
		return classOfService;
	}
}
