package com.isa.thinair.xbe.core.web.handler.system;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.config.XBEModuleConfig;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.system.LoginHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class ShowLoginRH extends BasicRH {

	private static Log log = LogFactory.getLog(ShowLoginRH.class);

	public static String execute(HttpServletRequest request) {

		String forward = S2Constants.Result.SHOW_LOGIN;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if(userLanguage == null || userLanguage.isEmpty()){
			userLanguage = WebConstants.DEFAULT_LANGUAGE;
		}
		try {
			setDisplayData(request);
		} catch (RuntimeException re) {
			forward = S2Constants.Result.ERROR;
			String msg = BasicRH.getErrorMessage(re,userLanguage);
			re.printStackTrace();
			log.error(msg, re);
			JavaScriptGenerator.setServerError(request, msg, "top", "");
		}

		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) {
		setRequestHtml(request);
		setClientErrors(request);
		setCarrierName(request);
		setCarrierList(request);
		// code merged to fixed a issue related to this
		setXBETracking(request);
		setAccountLockStatus(request);
	}
	
	private static void setAccountLockStatus(HttpServletRequest request) {
		if (AppSysParamsUtil.isEnableAccountLock()) {
			String message = (String)request.getAttribute("lockedMessage");
			if (message != null) {
				request.setAttribute(WebConstants.REQ_ERROR_SERVER_MESSAGES, "Your account has been locked until " + message + " GMT");
			}
		}		
	}

	private static void setClientErrors(HttpServletRequest request) {
		String strClientErrors = LoginHTMLGenerator.getClientErrors(request);
		BasicRH.saveClientErrors(request, strClientErrors);
	}

	private static void setRequestHtml(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();
		XBEModuleConfig config = XBEModuleUtils.getConfig();

		String port = config.getPort();
		String sslPort = config.getSslPort();
		String secureLoginUrl = BasicRH.getSecureUrl(request);
		boolean isAllowCarrierPrefix = AppSysParamsUtil.isAllowAirlineCarrierPrefixForAgentsAndUsers();

		sb.append("request['secureLoginUrl']='" + secureLoginUrl + "';");

		sb.append("request['port']='" + port + "';");

		sb.append("request['sslPort']='" + sslPort + "';");

		sb.append("request['isAllowCarrierPrefix']='" + isAllowCarrierPrefix + "';");
		
		sb.append("request['useSecureLogin']=" + config.isUseSecureLogin() + ";");

		request.setAttribute(WebConstants.REQ_HTML_REQUEST_DATA, sb.toString());

	}

	private static void setCarrierName(HttpServletRequest request) {
		String strCarrName = AppSysParamsUtil.getDefaultCarrierName();
		String strDefaultAirlineID = AppSysParamsUtil.getDefaultAirlineIdentifierCode();
		request.setAttribute(WebConstants.RES_DEFAULT_CARRIER_NAME, strCarrName);
		request.setAttribute(WebConstants.RES_DEFAULT_AIRLINE_ID_CODE, strDefaultAirlineID);
	}

	private static void setCarrierList(HttpServletRequest request) {

		try {
			String defaultCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();

			// Put default carrier into list
			Map<String, String> carrierList = new HashMap<String, String>();
			carrierList.put(AppSysParamsUtil.getCarrierDesc(defaultCarrierCode), defaultCarrierCode);

			// Fetch all dryEnabled carriers
			for (String[] carrierCode : SelectListGenerator.createCarrierListWithCode()) {
				carrierList.put(carrierCode[0], carrierCode[1]);
			}

			// Add carriers to list
			String strCarrierOptions = "";
			for (Entry<String, String> carrierEntry : carrierList.entrySet()) {
				String carrierName = carrierEntry.getKey();
				String carrierCode = carrierEntry.getValue();
				if (carrierCode.compareTo(defaultCarrierCode) == 0) {
					strCarrierOptions += "<option value=\"" + carrierCode + "\" SELECTED >" + carrierName + "</option>\n";
				} else {
					strCarrierOptions += "<option value=\"" + carrierCode + "\">" + carrierName + "</option>\n";
				}
			}
			request.setAttribute("reqTrakkingEnebled", AppSysParamsUtil.isXBETrackingEnable());
			request.setAttribute("reqCarrierOptions", strCarrierOptions);

		} catch (ModuleException e) {
			log.error(e);
		}

	}

	// code merged to fixed a issue related to this
	private static void setXBETracking(HttpServletRequest request) {
		try {
			request.setAttribute("reqXBETrackingEnabled", AppSysParamsUtil.isXBETrackingEnable());
		} catch (Exception e) {
			log.error(e);
		}
	}
}
