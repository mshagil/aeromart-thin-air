package com.isa.thinair.xbe.core.web.action.checkin;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.checkin.PFSProcessingRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Checkin.CHECKIN_PFSDETAILS_JSP),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowPFSDetailsProcessingAction extends BaseRequestResponseAwareAction {

	public String execute() {
		return PFSProcessingRH.execute(request, response);
	}

}
