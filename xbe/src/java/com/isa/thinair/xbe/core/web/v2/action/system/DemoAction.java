package com.isa.thinair.xbe.core.web.v2.action.system;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * @author Thushara Fernando
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({
		@Result(name=S2Constants.Jsp.Reservation.V2_MULTYCITY_SEARCH, value=S2Constants.Jsp.Reservation.V2_MULTYCITY_SEARCH),
		@Result(name=S2Constants.Jsp.Reservation.RESERVATION2, value=S2Constants.Jsp.Reservation.RESERVATION2),
		@Result(name=S2Constants.Jsp.Reservation.RESERVATION3, value=S2Constants.Jsp.Reservation.RESERVATION3)
})

public class DemoAction extends BaseRequestAwareAction {
	public String loadMulticiti() {
		return S2Constants.Jsp.Reservation.V2_MULTYCITY_SEARCH;
	}
	
	public String loadavailability() {
		String fwd = S2Constants.Jsp.Reservation.RESERVATION2;
		String action = request.getParameter("hdnAction");
    	if (action != null && action.equals("ADVANCEDSEARCH2")) {
    		fwd =  S2Constants.Jsp.Reservation.RESERVATION2;
    	} else if (action != null && action.equals("ADVANCEDSEARCH3")){
    		fwd =  S2Constants.Jsp.Reservation.RESERVATION3;
    	}
    	return fwd;
	}
}
