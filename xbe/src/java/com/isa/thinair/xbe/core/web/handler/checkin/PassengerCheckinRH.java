package com.isa.thinair.xbe.core.web.handler.checkin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airreservation.api.dto.PassengerCheckinDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author Charith
 * 
 */
public class PassengerCheckinRH extends BasicRH {

	private static Log log = LogFactory.getLog(PassengerCheckinRH.class);

	/**
	 * Execute Method for Passenger Checkin Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String forward = S2Constants.Result.SUCCESS;
		String strMode = request.getParameter("hdnMode");
		String viewOnly = "0";

		try {
			String hdnSelMode = (String) request.getParameter("hdnPPFSIds");
			ArrayList<Integer> ppfsIntIds = new ArrayList<Integer>();
			if (hdnSelMode != null && !"".equals(hdnSelMode)) {
				String[] ppfsIds = hdnSelMode.split(",");
				for (String str : ppfsIds) {
					ppfsIntIds.add(new Integer(str));
				}
			}

			if (strMode != null && strMode.equals("CHECKIN") && ppfsIntIds.size() > 0) {
				ModuleServiceLocator.getReservationQueryBD().updatePaxCheckinStatus(ppfsIntIds,
						ReservationInternalConstants.PassengerCheckinStatus.CHECKED_IN);
			} else if (strMode != null && strMode.equals("UNCHECKIN") && ppfsIntIds.size() > 0) {
				ModuleServiceLocator.getReservationQueryBD().updatePaxCheckinStatus(ppfsIntIds,
						ReservationInternalConstants.PassengerCheckinStatus.TO_BE_CHECKED_IN);
			} else if (strMode != null && strMode.equals("VIEWONLY")) {
				viewOnly = "1";
			}
			request.setAttribute(WebConstants.REQ_VIEW_ONLY, viewOnly);
			setDisplayData(request);

		} catch (ModuleException moduleException) {
			log.error(
					"Exception in PassengerCheckinRHandler.execute - setDisplayData(request) [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception e) {
			log.error("Exception in PassengerCheckinRHandler.execute - setDisplayData(request)", e);
			if (e instanceof RuntimeException) {
				forward = S2Constants.Result.ERROR;
				JavaScriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		Collection<PassengerCheckinDTO> colPassengers = new ArrayList<PassengerCheckinDTO>();
		String strFlightId = request.getParameter("hdnFlightId");
		String hdnSelMode = request.getParameter("hdnSelMode");
		String airportCode = request.getParameter("hdnAirportCode");

		if (strFlightId != null && !strFlightId.trim().equals("")) {
			int fltId = Integer.parseInt(strFlightId);
			if (hdnSelMode != null && !"".equals(hdnSelMode)) {
				if (hdnSelMode.equals("CHECKEDIN")) {
					colPassengers = ModuleServiceLocator.getReservationQueryBD().getCheckinPassengers(fltId, airportCode,
							ReservationInternalConstants.PassengerCheckinStatus.CHECKED_IN, false, false);
				}
				if (hdnSelMode.equals("TOBECHECKEDIN")) {
					colPassengers = ModuleServiceLocator.getReservationQueryBD().getCheckinPassengers(fltId, airportCode,
							ReservationInternalConstants.PassengerCheckinStatus.TO_BE_CHECKED_IN, false, false);
				}
				if (hdnSelMode.equals("STANDBY")) {
					colPassengers = ModuleServiceLocator.getReservationQueryBD().getCheckinPassengers(fltId, airportCode, null,
							true, false);
				}
				if (hdnSelMode.equals("GOSHOW")) {
					colPassengers = ModuleServiceLocator.getReservationQueryBD().getCheckinPassengers(fltId, airportCode, null,
							false, true);
				}
			} else { // Retrieve all passengers list
				colPassengers = ModuleServiceLocator.getReservationQueryBD().getCheckinPassengers(fltId, airportCode, null,
						false, false);
				setTotals(request, colPassengers);
			}
			setFlightDetails(request);
		}
		setDisplayCheckinPassengers(request, colPassengers);
	}

	/**
	 * Sets the Pax Totals to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param colPassengers
	 *            the Collection of Checkin passenger DTOs
	 */
	private static void setTotals(HttpServletRequest request, Collection colPassengers) throws ModuleException {
		int totalNoOfRecords = 0;
		int totalNoOfCheckedIn = 0;
		int totalNoOfTobeCheckedIn = 0;
		int totalNoOfStandby = 0;
		int totalNoOfGoShow = 0;
		int maleCount = 0;
		int femaleCount = 0;
		int otherCount = 0;
		int childCount = 0;
		int infantCount = 0;

		String strPassengerType = null;
		String strPassengerTitle = null;
		String strCheckinStatus = null;
		String strBcType = null;
		String strPpfsStatus = null;
		PassengerCheckinDTO passengerCheckinDTO = null;
		String[] cosCount = getCOSWiseCount(request, colPassengers);

		Iterator iterator = colPassengers.iterator();
		while (iterator.hasNext()) {
			passengerCheckinDTO = (PassengerCheckinDTO) iterator.next();
			strPassengerType = PlatformUtiltiies.nullHandler(passengerCheckinDTO.getPassengerType());
			strPassengerTitle = PlatformUtiltiies.nullHandler(passengerCheckinDTO.getPassengerTitle());
			strCheckinStatus = PlatformUtiltiies.nullHandler(passengerCheckinDTO.getPaxCheckinStatus());
			strBcType = PlatformUtiltiies.nullHandler(passengerCheckinDTO.getBcType());
			strPpfsStatus = PlatformUtiltiies.nullHandler(passengerCheckinDTO.getPpfsStatus());

			if (strPassengerType.equals(ReservationInternalConstants.PassengerType.ADULT)) {
				if (strPassengerTitle.equals(ReservationInternalConstants.PassengerTitle.MR)
						|| strPassengerTitle.equals(ReservationInternalConstants.PassengerTitle.MASTER)) {
					maleCount++;
				} else if (strPassengerTitle.equals(ReservationInternalConstants.PassengerTitle.MS)
						|| strPassengerTitle.equals(ReservationInternalConstants.PassengerTitle.MRS)
						|| strPassengerTitle.equals(ReservationInternalConstants.PassengerTitle.MISS)) {
					femaleCount++;
				} else {
					otherCount++;
				}
			} else if (strPassengerType.equals(ReservationInternalConstants.PassengerType.CHILD)) {
				childCount++;
			} else if (strPassengerType.equals(ReservationInternalConstants.PassengerType.INFANT)) {
				infantCount++;
			} else {
				otherCount++;
			}

			if (strCheckinStatus.equals(ReservationInternalConstants.PassengerCheckinStatus.CHECKED_IN)
					&& !strPassengerType.equals(ReservationInternalConstants.PassengerType.INFANT)) {
				totalNoOfCheckedIn++;
			} else if (!strPassengerType.equals(ReservationInternalConstants.PassengerType.INFANT)) {
				totalNoOfTobeCheckedIn++;
			}

			if (strBcType.equals(BookingClass.BookingClassType.STANDBY)) {
				totalNoOfStandby++;
			}

			if (strPpfsStatus.equals(ReservationInternalConstants.PaxFareSegmentTypes.GO_SHORE)) {
				totalNoOfGoShow++;
			}

		}
		totalNoOfRecords = maleCount + femaleCount + childCount + otherCount;
		if (cosCount == null) {
			cosCount = new String[2];
			cosCount[0] = passengerCheckinDTO == null ? "" : passengerCheckinDTO.getClassOfService() + "," + maleCount + ","
					+ femaleCount + "," + childCount + "," + infantCount + "," + otherCount + "#";
			cosCount[1] = passengerCheckinDTO == null ? "" : passengerCheckinDTO.getClassOfService() + "," + totalNoOfRecords
					+ "," + totalNoOfStandby + "," + totalNoOfCheckedIn + "," + totalNoOfGoShow + "," + totalNoOfTobeCheckedIn
					+ "#";
		}

		request.getSession().setAttribute(WebConstants.REQ_TOTAL_RECORDS, new Integer(totalNoOfRecords));
		request.getSession().setAttribute(WebConstants.SES_COS, cosCount[0]);
		request.getSession().setAttribute(WebConstants.SES_PAX_LOAD, cosCount[1]);
	}

	/**
	 * Sets the Display data From the Joined Collection
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param manifest
	 *            the Joined Collection of Manifest Data
	 */
	private static void setDisplayCheckinPassengers(HttpServletRequest request, Collection checkinPassengers) {

		int i = 0;
		Iterator ite = null;
		StringBuffer sb = new StringBuffer();
		PassengerCheckinDTO passengerCheckinDTO = null;

		if (checkinPassengers != null) {
			ite = checkinPassengers.iterator();
			while (ite.hasNext()) {
				passengerCheckinDTO = (PassengerCheckinDTO) ite.next();
				sb.append("arrCheckinPassengers[" + i + "] = new Array();");
				sb.append("arrCheckinPassengers[" + i + "][0] = '" + i + "';");
				sb.append("arrCheckinPassengers[" + i + "][1] = '" + passengerCheckinDTO.getPnr() + "';");
				if (passengerCheckinDTO.getInfantPax() != null && !passengerCheckinDTO.getInfantPax().equals("")) {
					sb.append("arrCheckinPassengers[" + i + "][2] = '" + passengerCheckinDTO.getPassengerName() + "  INFANT("
							+ passengerCheckinDTO.getInfantPax() + ")';");
				} else {
					sb.append("arrCheckinPassengers[" + i + "][2] = '" + passengerCheckinDTO.getPassengerName() + "';");
				}
				sb.append("arrCheckinPassengers[" + i + "][3] = '" + passengerCheckinDTO.getOrigin() + "';");
				sb.append("arrCheckinPassengers[" + i + "][4] = '" + passengerCheckinDTO.getSegmentCode() + "';");
				sb.append("arrCheckinPassengers["
						+ i
						+ "][5] = '"
						+ (passengerCheckinDTO.getPaxCheckinStatus() == null ? "&nbsp;" : passengerCheckinDTO
								.getPaxCheckinStatus()) + "';");
				sb.append("arrCheckinPassengers[" + i + "][6] = '" + passengerCheckinDTO.getPassengerType() + "';");
				sb.append("arrCheckinPassengers[" + i + "][7] = '"
						+ (passengerCheckinDTO.getClassOfService() == null ? "&nbsp;" : passengerCheckinDTO.getClassOfService())
						+ "';");
				sb.append("arrCheckinPassengers[" + i + "][8] = '"
						+ (passengerCheckinDTO.getBookingClass() == null ? "&nbsp;" : passengerCheckinDTO.getBookingClass())
						+ "';");
				sb.append("arrCheckinPassengers[" + i + "][9] = '"
						+ (passengerCheckinDTO.getSeatCode() == null ? "&nbsp;" : passengerCheckinDTO.getSeatCode()) + "';");
				sb.append("arrCheckinPassengers[" + i + "][10] = '"
						+ (passengerCheckinDTO.getSsrText() == null ? "&nbsp;" : passengerCheckinDTO.getSsrText()) + "';");
				sb.append("arrCheckinPassengers[" + i + "][12] = '" + passengerCheckinDTO.getPpfs_id().intValue() + "';");
				if (passengerCheckinDTO.isUnpaid()) {
					sb.append("arrCheckinPassengers[" + i + "][13] = 'No';");
				} else {
					sb.append("arrCheckinPassengers[" + i + "][13] = 'Yes';");
				}
				i++;
			}
		}
		request.setAttribute(WebConstants.CHECKIN_PASSENGER_DATA, sb.toString());
	}

	private static void setFlightDetails(HttpServletRequest request) {
		String strFlightId = request.getParameter("hdnFlightId");
		String airportCode = request.getParameter("hdnAirportCode");
		String modleCapacities = "";
		AircraftCabinCapacity ccCapacity = null;

		int fltId = Integer.parseInt(strFlightId);
		try {
			Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(fltId);
			AircraftModel aircraftModel = ModuleServiceLocator.getAircraftServiceBD().getAircraftModel(flight.getModelNumber());
			if (aircraftModel != null) {
				modleCapacities = aircraftModel.getModelNumber();
				Set ccCapacities = aircraftModel.getAircraftCabinCapacitys();
				if (ccCapacities != null) {
					Iterator ccCapacitiesIt = ccCapacities.iterator();
					while (ccCapacitiesIt.hasNext()) {
						ccCapacity = (AircraftCabinCapacity) ccCapacitiesIt.next();
						modleCapacities += " " + ccCapacity.getCabinClassCode() + "-" + ccCapacity.getSeatCapacity();
					}
				}
			}

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy HH:mm");
			FlightSegement segement = ModuleServiceLocator.getFlightServiceBD().getFlightSegmentForAirport(fltId,
					airportCode + "%");
			request.setAttribute(WebConstants.REQ_FLT_NUMBER, flight.getFlightNumber());
			request.setAttribute(WebConstants.REQ_FLT_ID, flight.getFlightId());
			request.setAttribute(WebConstants.REQ_FLT_DATE_TIME, formatter.format(segement.getEstTimeDepatureLocal()));
			request.setAttribute(WebConstants.REQ_FLT_STATUS, flight.getStatus());
			request.setAttribute(WebConstants.REQ_FLT_CHECKIN_STATUS, segement.getCheckinStatus());
			request.setAttribute(WebConstants.REQ_AIRPORT_CODE, airportCode);
			request.setAttribute(WebConstants.PARAM_FLIGHT_SEG_ID, segement.getFltSegId().intValue());
			request.setAttribute(WebConstants.REQ_MODEL_CAPACITIES, modleCapacities);
		} catch (ModuleException moduleException) {
			log.error("Exception in PassengerCheckinRH.execute - load flight [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static String[] getCOSWiseCount(HttpServletRequest request, Collection colPassengers) throws ModuleException {
		String strFlightId = request.getParameter("hdnFlightId");
		if (strFlightId != null && !strFlightId.trim().equals("")) {
			int fltId = Integer.parseInt(strFlightId);
			Collection cos = ModuleServiceLocator.getReservationQueryBD().getCOSForFlight(fltId);

			if (cos.size() == 1)
				return null; // Only one COS no need to iterate

			HashMap<String, int[]> cosCount = new HashMap<String, int[]>();
			HashMap<String, int[]> cosStatusCount = new HashMap<String, int[]>();// TODO
			int[] countArray = new int[5];

			Iterator<String> iterator = cos.iterator();
			while (iterator.hasNext()) {
				cosCount.put(iterator.next(), countArray);
			}

			String strPassengerType = null;
			String strPassengerTitle = null;
			String strCheckinStatus = null;
			String strBcType = null;
			String strCos = null;
			PassengerCheckinDTO passengerCheckinDTO = null;
			Iterator paxIterator = colPassengers.iterator();

			while (iterator.hasNext()) {
				passengerCheckinDTO = (PassengerCheckinDTO) paxIterator.next();
				strPassengerType = PlatformUtiltiies.nullHandler(passengerCheckinDTO.getPassengerType());
				strPassengerTitle = PlatformUtiltiies.nullHandler(passengerCheckinDTO.getPassengerTitle());
				strCheckinStatus = PlatformUtiltiies.nullHandler(passengerCheckinDTO.getPaxCheckinStatus());
				strBcType = PlatformUtiltiies.nullHandler(passengerCheckinDTO.getBcType());
				strCos = PlatformUtiltiies.nullHandler(passengerCheckinDTO.getClassOfService());

				if (strPassengerType.equals(ReservationInternalConstants.PassengerType.ADULT)) {
					if (strPassengerTitle.equals(ReservationInternalConstants.PassengerTitle.MR)
							|| strPassengerTitle.equals(ReservationInternalConstants.PassengerTitle.MASTER)) {
						cosCount.get(strCos)[0]++;
					} else if (strPassengerTitle.equals(ReservationInternalConstants.PassengerTitle.MS)
							|| strPassengerTitle.equals(ReservationInternalConstants.PassengerTitle.MRS)
							|| strPassengerTitle.equals(ReservationInternalConstants.PassengerTitle.MISS)) {
						cosCount.get(strCos)[1]++;
					} else {
						cosCount.get(strCos)[4]++;
					}
				} else if (strPassengerType.equals(ReservationInternalConstants.PassengerType.CHILD)) {
					cosCount.get(strCos)[2]++;
				} else if (strPassengerType.equals(ReservationInternalConstants.PassengerType.INFANT)) {
					cosCount.get(strCos)[3]++;
				} else {
					cosCount.get(strCos)[4]++;
				}
			}

			iterator = cos.iterator();
			String oneCOSCount = null;
			while (iterator.hasNext()) {
				strCos = iterator.next();
				String lineCount = strCos;
				for (int i = 0; i < 5; i++) {
					lineCount += "," + cosCount.get(strCos)[i];
				}
				lineCount += "#";
			}
			String[] returnArray = { oneCOSCount, oneCOSCount };
			return returnArray;
		}
		return null;
	}

}
