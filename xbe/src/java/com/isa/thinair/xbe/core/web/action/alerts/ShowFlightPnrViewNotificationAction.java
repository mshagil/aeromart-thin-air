package com.isa.thinair.xbe.core.web.action.alerts;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.alerts.FlightPnrViewNotificationRequestHandler;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

/**
 * Created by hasika on 6/11/18.
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.GET_XBE_DATA, value = S2Constants.Jsp.Search.GET_XBE_DATA),
        @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Alert.SHOW_FLIGHT_PNR_NOTIFICATION),
        @Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowFlightPnrViewNotificationAction extends BaseRequestAwareAction {

    public String execute() {
        return FlightPnrViewNotificationRequestHandler.execute(request);
    }

}
