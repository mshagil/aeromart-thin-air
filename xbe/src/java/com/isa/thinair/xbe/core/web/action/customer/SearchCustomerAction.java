package com.isa.thinair.xbe.core.web.action.customer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.CustomerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })

public class SearchCustomerAction extends BaseRequestAwareAction {
	
	private static Log log = LogFactory.getLog(SearchCustomerAction.class);

	private String searchEmail;

	private int page;

	private int records;

	private int total;

	private ArrayList<Map<String, Object>> customerList;

	public String execute(){
		String success = S2Constants.Result.SUCCESS;
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		boolean accessToGlobalSearch= BasicRH.hasPrivilege(request, PrivilegesKeys.XBECustomerPrivilageKeys.XBE_CUSTOMER_FULL_SEARCH_ACCESS);
		try {
			int start = (page - 1) * 20;
			Page<Customer> customerPage ;//= customerDelegate.getCustomerByPartialEmail(searchEmail, start, 20);
			if(accessToGlobalSearch){
				customerPage = customerDelegate.getCustomerByPartialEmail(searchEmail, start, 20);
			}else{
				customerPage = customerDelegate.getCustomerByEmail(searchEmail, start, 20);
			}

			setCustomerList(CustomerUtil.getCustomerDTOList((List<Customer>) customerPage.getPageData()));
			this.setRecords(customerPage.getTotalNoOfRecords());
			this.total = customerPage.getTotalNoOfRecords() / 20;
			int mod = customerPage.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
		} catch (ModuleException e) {
			log.error("SearchCustomerAction ==> ", e);
		}

		return success;
	}

	/**
	 * @return the searchEmail
	 */
	public String getSearchEmail() {
		return searchEmail;
	}

	/**
	 * @param searchEmail
	 *            the searchEmail to set
	 */
	public void setSearchEmail(String searchEmail) {
		this.searchEmail = searchEmail;
	}

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the customerList
	 */
	public ArrayList<Map<String, Object>> getCustomerList() {
		return customerList;
	}

	/**
	 * @param customerList
	 *            the customerList to set
	 */
	public void setCustomerList(ArrayList<Map<String, Object>> customerList) {
		this.customerList = customerList;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

}
