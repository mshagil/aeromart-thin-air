// $Id$
package com.isa.thinair.xbe.core.web.handler.reservation;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class SaveXBEDataRH extends BasicRH {

	private static Log log = LogFactory.getLog(SaveXBEDataRH.class);

	/**
	 * 
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.GET_XBE_DATA;
		String responseString = "";
		Collection colCommands = getCommands(request);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			// Chk if User has the privilege to alter without paying

			String forceConfirm = request.getParameter(WebConstants.PARAM_FORCE_CONFIRM);
			if ("true".equals(forceConfirm)) {
				BasicRH.checkPrivilege(request, PriviledgeConstants.ALT_RES_NO_PAY);
			}

			if (colCommands.contains(WebConstants.COMMAND_CHANGE_PASSWORD)) {
				responseString = changePassword(request);
			} else if (colCommands.contains(WebConstants.COMMAND_GET_PASSWORD)) {
				responseString = getUserPassword(request);

			}

		} catch (Exception ex) {
			forward = S2Constants.Result.ERROR;
			String msg = BasicRH.getErrorMessage(ex,userLanguage);
			boolean blnRetry = true;
			log.error(ex.getMessage() + ":" + msg, ex);
			if (blnRetry) {
				responseString = getErrorResponse("code", "desc", msg);
			} else {
				// user cannot continue
				responseString = getErrorResponse("code0", "desc", msg);
			}

		}

		request.setAttribute(WebConstants.REQ_RESPONSE_STRING, responseString);
		return forward;
	}

	/**
	 * Gets the user password
	 * 
	 * @param props
	 *            the Property file Containg User Request values
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static String getUserPassword(HttpServletRequest request) throws ModuleException {
		String responseString = "";
		try {

			CryptoServiceBD cryptoServiceBD = (CryptoServiceBD) ModuleServiceLocator.getCryptoServiceBD();

			String strLoginId = request.getParameter("txtLoginId");
			if (strLoginId != null && strLoginId != "") {
				strLoginId = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode() + strLoginId;
			}
			String decryptPassword = "";

			if (strLoginId != null) {

				User user = ModuleServiceLocator.getSecurityBD().getUser(strLoginId);

				String encyptPswd = user.getPassword();

				decryptPassword = cryptoServiceBD.decrypt(encyptPswd);

				request.getSession().setAttribute("reqResponseString", decryptPassword);

				responseString = "true";
				responseString = "pswd = '" + decryptPassword + "';";

			}
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), WebConstants.MSG_ERROR);
		}
		return responseString;
	}

	private static String changePassword(HttpServletRequest request) throws ModuleException {
		String PARAM_OLD_PWD = "pwdOld";

		String PARAM_NEW_PWD = "pwdNew";

		String PARAM_PWD_RESET = "pwdReset";

		boolean isReseted = false;
		String reseted = "";
		reseted = request.getParameter(PARAM_PWD_RESET);
		if (reseted != null && reseted.equals("true")) {
			isReseted = true;
		}
		String responseString = "";

		String userId = request.getRemoteUser();
		String oldPassword = request.getParameter(PARAM_OLD_PWD);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if (authenticateUser(userId, oldPassword)) {
			String newPassword = request.getParameter(PARAM_NEW_PWD);
			if (AppSysParamsUtil.isEnablePasswordHistory() && AirSecurityUtil.getSecurityBD().isPasswordUsed(userId, newPassword)) {				
				responseString = getFailureResponse(XBEConfig.getServerMessage("passwordchange.used.password", userLanguage, null));
			} else {
				String newSessionId = null;
				changePassword(userId, newPassword, isReseted);
				// request.getSession().invalidate();
				newSessionId = request.getSession().getId();
				responseString = getSucccessResponse(XBEConfig.getServerMessage("passwordchange.operation.success", userLanguage, null), newSessionId);
			}
		} else {
			responseString = getFailureResponse(XBEConfig.getServerMessage("passwordchange.invalid.user", userLanguage, null));
		}

		return responseString;
	}

	private static String getErrorResponse(String codeParamName, String descParamName, String messageString) {
		return "aMessage['" + codeParamName + "']='ERROR';aMessage['" + descParamName + "']='" + messageString + "';";
	}

	/**
	 * gets the success response
	 * 
	 * @param messageString
	 * @param newSessionId
	 * @return
	 */
	private static String getSucccessResponse(String messageString, String other) {
		return "aMessage['code']='SUCCESS';aMessage['desc']='" + messageString + "';aMessage['other']='" + other + "';";

	}

	/**
	 * gets the failure response
	 * 
	 * @param messageString
	 * @return
	 */
	private static String getFailureResponse(String messageString) {
		return "aMessage['code']='ERROR';aMessage['desc']='" + messageString + "';";

	}

	private static boolean authenticateUser(String userId, String password) throws ModuleException {
		SecurityBD securityBD = ModuleServiceLocator.getSecurityBD();
		User user = securityBD.authenticate(userId, password);
		return (user != null);
	}

	private static void changePassword(String userId, String newPassword, boolean isReseted) throws ModuleException {
		SecurityBD securityBD = ModuleServiceLocator.getSecurityBD();
		User user = securityBD.getUser(userId);
		if (isReseted) {
			user.setPasswordReseted("N");
		}
		user.setPassword(newPassword);
		user.setLccPublishStatus(Util.LCC_TOBE_REPUBLISHED);

		securityBD.saveUser(user);
		if (AppSysParamsUtil.isLCCDataSyncEnabled()) {
			User savedUser = ModuleServiceLocator.getSecurityBD().getUser(user.getUserId());
			ModuleServiceLocator.getCommonServiceBD().publishSpecificUserDataUpdate(savedUser);
		}
	}

}
