package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class MinFareCalculationAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(MinFareCalculationAction.class);

	private boolean success = true;

	private String messageTxt;

	private CalendarSearchRS calendarRS;

	private String fromAirport = null;
	private String toAirport = null;
	private String departureDate = null;
	private String toDate = null;
	private String adultCount = null;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean isSuccess) {
		success = isSuccess;
	}

	private String childCount = null;
	private String infantCount = null;
	private String selectedCurrency = null;
	private String classOfService = null;

	public String execute() {
		String result = S2Constants.Result.SUCCESS;
		CalendarSearchRQ calendarSearchRQ = new CalendarSearchRQ();

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		// AvailRQ.getAvailPreferences().setTravelAgentCode(userPrincipal.getAgentCode());

		try {
			OriginDestinationInformationTO ond = calendarSearchRQ.addNewOriginDestinationInformation();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date d = sdf.parse(departureDate);
			ond.setOrigin(fromAirport);
			ond.setDestination(toAirport);
			ond.setDepartureDateTimeStart(d);
			ond.setDepartureDateTimeEnd(getEndOfDay(sdf.parse(toDate)));
			ond.setPreferredClassOfService(classOfService);

			int adt = Integer.parseInt(adultCount);
			int chd = Integer.parseInt(childCount);
			int inf = Integer.parseInt(infantCount);

			AvailPreferencesTO availPref = calendarSearchRQ.getAvailPreferences();
			availPref.setAppIndicator(ApplicationEngine.XBE);
			availPref.setPreferredCurrency(selectedCurrency);

			TravelerInfoSummaryTO travellerInfo = calendarSearchRQ.getTravelerInfoSummary();
			if (adt > 0) {
				PassengerTypeQuantityTO paxType = travellerInfo.addNewPassengerTypeQuantityTO();
				paxType.setPassengerType(PaxTypeTO.ADULT);
				paxType.setQuantity(adt);
			}
			if (chd > 0) {
				PassengerTypeQuantityTO paxType = travellerInfo.addNewPassengerTypeQuantityTO();
				paxType.setPassengerType(PaxTypeTO.CHILD);
				paxType.setQuantity(chd);
			}
			if (inf > 0) {
				PassengerTypeQuantityTO paxType = travellerInfo.addNewPassengerTypeQuantityTO();
				paxType.setPassengerType(PaxTypeTO.INFANT);
				paxType.setQuantity(inf);
			}

			calendarSearchRQ.getAvailPreferences().setTravelAgentCode(userPrincipal.getAgentCode());
			calendarSearchRQ.getAvailPreferences().setPosAirportCode(userPrincipal.getAgentStation());

			FlightInventoryResBD f = ModuleServiceLocator.getFlightInventoryReservationBD();
			this.calendarRS = f.searchCalendarFares(calendarSearchRQ);
		} catch (ModuleException e) {
			e.printStackTrace();
			success = false;
			log.error(e);
			messageTxt = e.getMessage();
		} catch (Exception e) {
			String h = e.getMessage();
			e.printStackTrace();
			success = false;
			log.error(h);
			messageTxt = "Unkwn error";
		}

		return result;
	}
	
	private Date getEndOfDay(Date date) {
		LocalDateTime localDateTime = dateToLocalDateTime(date);
		LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
		return localDateTimeToDate(endOfDay);
	}

	private Date localDateTimeToDate(LocalDateTime startOfDay) {
		return Date.from(startOfDay.atZone(ZoneId.systemDefault()).toInstant());
	}

	private LocalDateTime dateToLocalDateTime(Date date) {
		return LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.systemDefault());
	}

	public String getClassOfService() {
		return classOfService;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}

	public String getFromAirport() {
		return fromAirport;
	}

	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	public String getToAirport() {
		return toAirport;
	}

	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}

	public String getChildCount() {
		return childCount;
	}

	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}

	public String getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(String infantCount) {
		this.infantCount = infantCount;
	}

	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	public CalendarSearchRS getCalendarRS() {
		return calendarRS;
	}

	public void setCalendarRS(CalendarSearchRS calendarRS) {
		this.calendarRS = calendarRS;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}
}
