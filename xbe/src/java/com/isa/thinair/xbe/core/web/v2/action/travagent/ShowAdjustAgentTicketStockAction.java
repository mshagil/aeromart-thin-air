package com.isa.thinair.xbe.core.web.v2.action.travagent;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTicketStockTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.util.DataUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

/**
 * @author eric
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowAdjustAgentTicketStockAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowAdjustAgentTicketStockAction.class);

	private String message;
	private String msgType;

	private static final int PAGE_LENGTH = 10;

	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;
	private String agentCode;

	private AgentTicketStockTO agentTicketStockTo;

	@SuppressWarnings("unchecked")
	public String execute() {
		try {

			if (request.getParameter("agentCode") != null) {
				agentCode = request.getParameter("agentCode").trim();
			}

			int startIndex = (page - 1) * PAGE_LENGTH;

			Page pagedReocrds = ModuleServiceLocator.getTravelAgentBD().getAgentTicketStocks(agentCode, startIndex, PAGE_LENGTH);

			this.rows = (Collection<Map<String, Object>>) DataUtil.createGridData(pagedReocrds.getStartPosition(),
					"agentTicketStockTo", pagedReocrds.getPageData());

			this.page = getCurrentPageNo(pagedReocrds.getStartPosition());
			this.total = getTotalNoOfPages(pagedReocrds.getTotalNoOfRecords());
			this.records = pagedReocrds.getTotalNoOfRecords();
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (Exception e) {
			log.error(" Exception occured while laoding AgentTicketStock ");
			this.message = e.getMessage();
			this.msgType = WebConstants.MSG_ERROR;
		}
		return S2Constants.Result.SUCCESS;

	}

	public String addEditTicketStock() {
		try {
			ModuleServiceLocator.getTravelAgentBD().saveAgentTicketStock(agentTicketStockTo);
			this.message = WebConstants.KEY_SAVE_SUCCESS;
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException me) {
			log.error(" Exception occured while saving TicketStock ", me);
			this.message = me.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception e) {
			log.error(" Exception occured while saving TicketStock ", e);
			this.message = e.getMessage();
			this.msgType = WebConstants.MSG_ERROR;
		} finally {
			agentTicketStockTo = null;
		}
		return S2Constants.Result.SUCCESS;

	}

	private int getCurrentPageNo(int startPosition) {
		int currentPageNo = 1;
		if (PAGE_LENGTH != 0) {
			currentPageNo = startPosition / PAGE_LENGTH + 1;
		}
		return currentPageNo;
	}

	private int getTotalNoOfPages(int totoalNoOfRecords) {
		int totalNoOfPages = 1;
		if (PAGE_LENGTH != 0) {
			totalNoOfPages = (int) totoalNoOfRecords / PAGE_LENGTH;
			if (totoalNoOfRecords % PAGE_LENGTH > 0)
				totalNoOfPages++;
		}
		return totalNoOfPages;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;

	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public AgentTicketStockTO getAgentTicketStockTo() {
		return agentTicketStockTo;
	}

	public void setAgentTicketStockTo(AgentTicketStockTO agentTicketStockTo) {
		this.agentTicketStockTo = agentTicketStockTo;
	}

}
