package com.isa.thinair.xbe.core.web.handler.flight;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author Thushara,Janaka
 * 
 */
public class FlightDetailRH extends BasicRH {

	private static Log log = LogFactory.getLog(FlightDetailRH.class);

	/**
	 * Execute Method to Display the Manifest
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SUCCESS;

		try {

			// Sets manifest options
			FlightManifestOptionsDTO manifestOptions = setManifestOptions(request);

			// Generate Flight Manifest Content
			if (manifestOptions.isEmail()) {
				// Boolean status =
				// ModuleServiceLocator.getReservationQueryBD().sendFlightManifestAsEmail(manifestOptions);
				Boolean status = ModuleServiceLocator.getAirProxyFlightServiceBD().sendFlightManifestAsEmail(manifestOptions,
						TrackInfoUtil.getBasicTrackInfo(request));
				setEmailStatus(request, status);
				forward = S2Constants.Result.ACTION_FORWARD_SUCCESS_AJAX;

			} else {

				// String manifestHTML =
				// ModuleServiceLocator.getReservationQueryBD().viewFlightManifest(manifestOptions);
				String manifestHTML = ModuleServiceLocator.getAirProxyFlightServiceBD().viewFlightManifest(manifestOptions,
						TrackInfoUtil.getBasicTrackInfo(request));
				request.setAttribute(WebConstants.MANIFEST_HTML_RESPONSE, manifestHTML);

			}

		} catch (ModuleException moduleException) {
			log.error(
					"Exception in FlightRequestHandler.execute - setDisplayData(request) [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			request.setAttribute("returnData", "var strMsg = '" + moduleException.getMessageString() + "';");
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception e) {
			log.error("Exception in FlightRequestHandler.execute - setDisplayData(request)", e);
			if (e instanceof RuntimeException) {
				forward = S2Constants.Result.ERROR;
				JavaScriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}

		return forward;
	}

	private static FlightManifestOptionsDTO setManifestOptions(HttpServletRequest request) throws ModuleException {

		FlightManifestOptionsDTO manifestOptions = new FlightManifestOptionsDTO();
		manifestOptions.setUserPrincipal((UserPrincipal) request.getUserPrincipal());

		String strFlightID = request.getParameter(WebConstants.REQ_MANIFEST_FLIGHT_ID);

		// General options
		manifestOptions.setFlightId(Integer.parseInt(strFlightID));
		manifestOptions.setFlightNo(request.getParameter(WebConstants.REQ_MANIFEST_FLIGHT_NO));
		manifestOptions.setFlightDate(request.getParameter(WebConstants.REQ_MANIFEST_FLIGHT_DATE));
		manifestOptions.setRoute(request.getParameter(WebConstants.REQ_MANIFEST_FLIGHT_ROUTE));
		manifestOptions.setConfirmedReservation(true);
		manifestOptions.setSortOption(request.getParameter(WebConstants.REQ_MANIFEST_SORT_OPTION));
		manifestOptions.setPrivilegedToShowAllBookings(BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_SHOW_ALL_BOOKINGS_IN_FLIGHT_MANIFEST));

		String offlineReportParams = AppSysParamsUtil.getEnableOfflineReportParams();
		String[] reportParams = offlineReportParams.split(",");

		try {
			if (reportParams[0].equals("Y")) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
				Date reportFromDate = DateUtil.parseDate(manifestOptions.getFlightDate(), "dd/MM/yyyy");
				int noOfLiveReportDays = Integer.parseInt(reportParams[1]);

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_YEAR, -1 * noOfLiveReportDays);
				Date validReportFromDate = dateFormat.parse(dateFormat.format(cal.getTime()));
				if (validReportFromDate.compareTo(reportFromDate) > 0) {
					manifestOptions.setDataSourceMode(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				} else {
					manifestOptions.setDataSourceMode(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				}
			}
		} catch (Exception e) {
			log.error(e);
		}

		if (request.getParameter(WebConstants.REQ_MANIFEST_COS) != null
				&& !"".equals(request.getParameter(WebConstants.REQ_MANIFEST_COS))) {
			String cosVal = request.getParameter(WebConstants.REQ_MANIFEST_COS);
			String[] cosArr = cosVal.split("-");

			if (cosArr.length > 1) {
				manifestOptions.setLogicalCabinClass(cosArr[0]);
				manifestOptions.setCabinClass(cosArr[1]);
			} else {
				manifestOptions.setLogicalCabinClass("All");
				manifestOptions.setCabinClass(cosArr[0]);
			}

			String filterType = request.getParameter(WebConstants.REQ_MANIFEST_COS_TYPE);

			if (FlightManifestOptionsDTO.GROUP_BY_CABIN_CLASS.equals(filterType)) {
				manifestOptions.setFilterByCabinClass(true);
			} else if (FlightManifestOptionsDTO.GROUP_BY_LOGICAL_CABIN_CLASS.equals(filterType)) {
				manifestOptions.setFilterByLogicalCabinClass(true);
			}
		}

		manifestOptions.setPassportStatus(request.getParameter(WebConstants.REQ_PASSPORT_STATUS));
		manifestOptions.setDocsStatus(request.getParameter(WebConstants.REQ_DOCS_STATUS));

		String strMode = request.getParameter(WebConstants.REQ_MANIFEST_MODE);

		// Email
		if (FlightManifestOptionsDTO.EMAIL.equalsIgnoreCase(strMode)) {
			manifestOptions.setEmail(true);
			manifestOptions.setEmailId(request.getParameter(WebConstants.REQ_MANIFEST_EMAIL_ID));
		} else if (FlightManifestOptionsDTO.DOWNLOAD.equalsIgnoreCase(strMode)) {
			manifestOptions.setDownload(true);
		}else{
			manifestOptions.setLoadByPnr(AppSysParamsUtil.isAllowLoadReservationFromManifest());
		}

		String strConnectionMode = request.getParameter(WebConstants.REQ_MANIFEST_CONNECTION_MODE);

		// Connections
		if (FlightManifestOptionsDTO.INWARD_CONNECTIONS.equals(strConnectionMode)) {

			manifestOptions.setCheckInwardConnections(true);
			manifestOptions.setInwardConnectionOrigin(request.getParameter(WebConstants.REQ_MANIFEST_INWARD_CONN_ORIGIN));
			manifestOptions
					.setInwardBoardingAirport(request.getParameter(WebConstants.REQ_MANIFEST_INWARD_CONN_BOARDING_AIRPORT));
			manifestOptions.setInwardFlightNo(request.getParameter(WebConstants.REQ_MANIFEST_INWARD_CONN_FLIGHT));
			manifestOptions.setMaxInwardConnectionTime(request.getParameter(WebConstants.REQ_MANIFEST_INWARD_CONN_LIMIT));

		} else if (FlightManifestOptionsDTO.OUTWARD_CONNECTIONS.equals(strConnectionMode)) {

			manifestOptions.setCheckOutwardConnections(true);
			manifestOptions.setOutwardConnectionDestination(request.getParameter(WebConstants.REQ_MANIFEST_OUTWARD_CONN_DEST));
			manifestOptions.setOutwardBoardingAirport(request
					.getParameter(WebConstants.REQ_MANIFEST_OUTWARD_CONN_BOARDING_AIRPORT));
			manifestOptions.setOutwardFlightNo(request.getParameter(WebConstants.REQ_MANIFEST_OUTWARD_CONN_FLIGHT));
			manifestOptions.setMaxOutwardConnectionTime(request.getParameter(WebConstants.REQ_MANIFEST_OUTWARD_CONN_LIMIT));

		} else if (FlightManifestOptionsDTO.BOTH_CONNECTIONS.equals(strConnectionMode)) {
			manifestOptions.setInwardConnectionOrigin(request.getParameter(WebConstants.REQ_MANIFEST_INWARD_CONN_ORIGIN));
			manifestOptions
					.setInwardBoardingAirport(request.getParameter(WebConstants.REQ_MANIFEST_INWARD_CONN_BOARDING_AIRPORT));
			manifestOptions.setInwardFlightNo(request.getParameter(WebConstants.REQ_MANIFEST_INWARD_CONN_FLIGHT));
			manifestOptions.setMaxInwardConnectionTime(request.getParameter(WebConstants.REQ_MANIFEST_INWARD_CONN_LIMIT));
			manifestOptions.setOutwardConnectionDestination(request.getParameter(WebConstants.REQ_MANIFEST_OUTWARD_CONN_DEST));
			manifestOptions.setOutwardBoardingAirport(request
					.getParameter(WebConstants.REQ_MANIFEST_OUTWARD_CONN_BOARDING_AIRPORT));
			manifestOptions.setOutwardFlightNo(request.getParameter(WebConstants.REQ_MANIFEST_OUTWARD_CONN_FLIGHT));
			manifestOptions.setMaxOutwardConnectionTime(request.getParameter(WebConstants.REQ_MANIFEST_OUTWARD_CONN_LIMIT));

			manifestOptions.setCheckInwardConnections(true);
			manifestOptions.setCheckOutwardConnections(true);

		} else {
			manifestOptions.setCheckInwardConnections(false);
			manifestOptions.setCheckOutwardConnections(false);
		}

		String strViewOption = request.getParameter(WebConstants.REQ_MANIFEST_VIEW_OPTION);
		String strSummaryMode = request.getParameter(WebConstants.REQ_MANIFEST_SUMMARY_MODE);
		String strSummaryPosition = request.getParameter(WebConstants.REQ_MANIFEST_SUMMARY_POSITION);

		// Summary options
		if (FlightManifestOptionsDTO.SUMMARY_OPTIONS_SEGMENTWISE.equals(strSummaryMode)) {
			manifestOptions.setSummaryMode(strSummaryMode);
		} else if (FlightManifestOptionsDTO.SUMMARY_OPTIONS_INOUT.equals(strSummaryMode)) {
			manifestOptions.setSummaryMode(strSummaryMode);
		} else {
			manifestOptions.setSummaryMode(FlightManifestOptionsDTO.SUMMARY_OPTIONS_SEGMENTWISE);
		}

		// View options
		if (FlightManifestOptionsDTO.VIEW_OPTIONS_DETAILED.equals(strViewOption)) {
			manifestOptions.setViewOption(strViewOption);
		} else if (FlightManifestOptionsDTO.VIEW_OPTIONS_SUMMARY.equals(strViewOption)) {
			manifestOptions.setViewOption(strViewOption);
		} else {
			manifestOptions.setViewOption(FlightManifestOptionsDTO.VIEW_OPTIONS_DETAILED);
		}

		if (FlightManifestOptionsDTO.SUMMARY_POSITION_TOP.equals(strSummaryPosition)) {
			manifestOptions.setSummaryPosition(strSummaryPosition);
		} else if (FlightManifestOptionsDTO.SUMMARY_POSITION_BOTTOM.equals(strSummaryPosition)) {
			manifestOptions.setSummaryPosition(strSummaryPosition);
		} else {
			manifestOptions.setSummaryPosition(FlightManifestOptionsDTO.SUMMARY_POSITION_TOP);
		}

		// Other manifest options to be used at generating view
		manifestOptions.setShowAirportServices(AppSysParamsUtil.isAirportServicesEnabled());
		manifestOptions.setShowCreditCardDetails(AppSysParamsUtil.sendCCDetailsWithPNL());
		manifestOptions.setShowSeatMap(AppSysParamsUtil.isShowSeatMap());
		manifestOptions.setShowMeal(AppSysParamsUtil.isShowMeals());
		manifestOptions.setShowBaggage(AppSysParamsUtil.isShowBaggages());
		manifestOptions.setShowPaxIndentification(AppSysParamsUtil.showPassportInItinerary());
		manifestOptions.setShowSsr(true);

		return manifestOptions;
	}

	private static void setEmailStatus(HttpServletRequest request, Boolean status) {

		String strReturnData = "returnData";

		if (status) {
			request.setAttribute(strReturnData, "var emailStatus='success';");
		} else {
			request.setAttribute(strReturnData, "var emailStatus='error';");
		}
	}
}
