package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.constants.WebConstants.ReportFormatType;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class FlownPassengerListReportRH extends BasicRH {
	private static Log log = LogFactory.getLog(FlownPassengerListReportRH.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;

		setAttribInRequest(request, "displayAgencyMode", "-1");
		setAttribInRequest(request, "currentAgentCode", "");
		try {
			setDisplayData(request);
		} catch (Exception e) {
			log.error("FlownPassengerListReportRH SETDISPLAYDATA() FAILED " + e.getMessage());
		}

		try {
			if (strHdnMode != null
					&& (strHdnMode.equals(WebConstants.ACTION_VIEW) || strHdnMode.equals(WebConstants.ACTION_VIEW_SUMMARY))) {
				setReportView(request, response);
				return null;
			} else {
				log.error("FlownPassengerListReportRH setReportView not selected");
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		}
		return forward;

	}

	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setAirportList(request);
		setDisplayAgencyMode(request);
		setAgentTypes(request);
		setGSAMultiSelectList(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setDisplayAgencyMode(HttpServletRequest request) {
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges.get("rpt.res.fpl.all") != null) {
			// Show both the controls
			setAttribInRequest(request, "displayAgencyMode", "1");
		} else if (mapPrivileges.get("rpt.res.fpl.gsa") != null) {
			setAttribInRequest(request, "displayAgencyMode", "2");
		} else if (mapPrivileges.get("rpt.res.fpl") != null) {
			setAttribInRequest(request, "displayAgencyMode", "0");
		} else {
			setAttribInRequest(request, "displayAgencyMode", "-1");
		}
	}

	private static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (getAttribInRequest(request, "displayAgencyMode").equals("-1")) {
			// No privilege to access the reports
			throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			// No Agent Population Controls or Agents list box are displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
			} else {
				// No privilege to access the reports
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("1")) {
			// Both Agent Population Controls and Agents list box are displayed
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			// Only Agents list box is displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
				if (currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					// Agent is a GSA
					strList = ReportsHTMLGenerator.createTAsOfGSAMultiSelect(currentAgent.getAgentCode(), true);
				} else {
					// Agent is not a GSA
					// Only select the current agent
					setAttribInRequest(request, "displayAgencyMode", "0");
				}
			} else {
				// No privilege to access the reports
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS,
				"displayAgencyMode = " + getAttribInRequest(request, "displayAgencyMode") + ";");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	private static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_FLOWN_PASSENGER_LIST, strATList);
	}

	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		String id = "UC_REPM_088";
		String reportTemplate = "";

		String viewMode = request.getParameter("hdnMode");
		if (("VIEW").equals(viewMode)) {
			reportTemplate = "FlownPassengerListReport.jasper";
		} else {
			reportTemplate = "FlownPassengerSummaryReport.jasper";
		}

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");

		String agents = "";
		if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			agents = (String) getAttribInRequest(request, "currentAgentCode");
		} else {
			agents = request.getParameter("hdnAgents");
		}

		String bookFrmDate = request.getParameter("txtBookedFromDate");
		String bookToDate = request.getParameter("txtBookedToDate");
		String paxSegStatus = request.getParameter("selPaxSegStatus");

		String loggedUserAgentCode = RequestHandlerUtil.getAgentCode(request);

		String reportType = request.getParameter("radReportOption");
		String reportNumFormat = request.getParameter("radRptNumFormat");
		String selSegmentOnD = request.getParameter("hdnSegments");
		String flighttNumber = request.getParameter("txtFlightNumber");

		ArrayList<String> selectedAgents = new ArrayList<String>();
		if (!agents.trim().isEmpty()) {
			String agentList[] = agents.split(",");
			for (String agent : agentList) {
				selectedAgents.add(agent);
			}
		}
		ArrayList<String> lstSegments = new ArrayList<String>();

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria searchCriteria = new ReportsSearchCriteria();
			Map<String, Object> parameters = new HashMap<String, Object>();
			if (isNotEmptyOrNull(toDate)) {
				searchCriteria.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}
			if (isNotEmptyOrNull(toDate)) {
				searchCriteria.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + "  23:59:59");
			}

			if (isNotEmptyOrNull(selSegmentOnD)) {
				String[] arrSegCodes = selSegmentOnD.split(",");
				for (String segCode : arrSegCodes) {
					lstSegments.add(segCode);
				}
				searchCriteria.setSegmentCodes(lstSegments);
			}

			if (isNotEmptyOrNull(flighttNumber)) {
				searchCriteria.setFlightNumber(flighttNumber);
			}

			if (isNotEmptyOrNull(paxSegStatus)) {
				searchCriteria.setStatus(paxSegStatus);
			}

			if (isNotEmptyOrNull(bookFrmDate) && isNotEmptyOrNull(bookToDate)) {
				searchCriteria.setDateRange2From(ReportsHTMLGenerator.convertDate(bookFrmDate) + " 00:00:00");
				searchCriteria.setDateRange2To(ReportsHTMLGenerator.convertDate(bookToDate) + "  23:59:59");
			}

			if (selectedAgents.size() > 0 && isNotEmptyOrNull(loggedUserAgentCode)) {
				selectedAgents.add(loggedUserAgentCode);
				searchCriteria.setAgents(selectedAgents);
			}

			searchCriteria.setInterlineCode(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE));

			ResultSet resultSet = null;

			resultSet = ModuleServiceLocator.getDataExtractionBD().getFlownPassengerListData(searchCriteria, viewMode);

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);

			// To provide Report Format Options
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			String strLogo = AppSysParamsUtil.getReportLogo(false);
			String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true);
			String reportNameStr = "FlownPassengerListReport";
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			if (reportType.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
						reportsRootDir, response);

			} else if (reportType.trim().equals("PDF")) {
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

			} else if (reportType.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportNameStr));
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);

			} else if (reportType.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=FlownPassengerList.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}

		} catch (Exception e) {
			log.error("setReportView() method is failed :" + e.getMessage());

		}

	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}

}
