package com.isa.thinair.xbe.core.web.v2.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * String Utility
 * 
 * @author Thushara
 * @since Dec 06, 2009
 */
public class StringUtil {

	public static String toInitCap(String str) {
		String intCap = str;
		if (str != null && !"".equals(str.trim())) {
			intCap = str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
		}
		return intCap;
	}

	public static String getNotNullString(Object obj) {
		String strVal = "";
		if (obj != null)
			strVal = obj.toString();
		return strVal;
	}

	protected static String replaceNewLineForJSON(String str) {
		String strVal = "";
		if (str != null && str.length() > 0) {
			strVal = str.replace("\n", "<br>");
		}
		return strVal;
	}

	/**
	 * Split G9-343|E5-600 type sting in to a carrier wise map
	 * 
	 * @param data
	 * @param carrierSplitter
	 * @param valueSplitter
	 * @return
	 */
	public static Map<String, String> carrierWiseSplite(String data, String carrierSplitter, String valueSplitter) {
		String[] perCarrier = StringUtils.split(data, carrierSplitter);
		Map<String, String> carrierViseRefNo = new HashMap<String, String>();
		for (String carrierViseVal : perCarrier) {
			String[] temp = StringUtils.split(carrierViseVal, valueSplitter);
			carrierViseRefNo.put(temp[0], temp[1]);
		}
		return carrierViseRefNo;
	}
	
	/**
	 * Check the String contain a value that convertible to an integer.
	 * This method is faster than using Integer.parseInt and capturing the exception.
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c <= '/' || c >= ':') {
				return false;
			}
		}
		return true;
	}
}