/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduler.api.service;

import com.isa.thinair.commons.api.exception.ModuleException;

import java.util.Map;

/**
 * @author Byorn
 */
public interface SchedulerBD {

	public static final String SERVICE_NAME = "SchedulerService";

	public void startScheduler() throws ModuleException;

	public void addJob(String jobDetailBeanName, String jobName, String jobGroupName, String cronExpression)
			throws ModuleException;

	public void removeJob(String jobName, String jobGroupName) throws ModuleException;

	public void stopScheduler() throws ModuleException;

	public String getStatusOfScheduler() throws ModuleException;

	public void scheduleJob(Map<String, String> jobDataMap) throws ModuleException;

	public boolean wasJobScheduled(String jobName, String jobGroup) throws ModuleException;

}
