/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.scheduler.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author byorn
 * @isa.module.config-bean
 */
public class SchedulerConfig extends DefaultModuleConfig {

	private String username;
	private String password;
	private String errorNotifyingEmail;
	private Integer minGapInMinitesBWJobs;
	private Integer jobSearchDurationInHrs;
	private Integer failedJobSearchBackDurationInHrs;

	public SchedulerConfig() {
		super();

	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getErrorNotifyingEmail() {
		return errorNotifyingEmail;
	}

	public void setErrorNotifyingEmail(String errorNotifyingEmail) {
		this.errorNotifyingEmail = errorNotifyingEmail;
	}

	public Integer getMinGapInMinitesBWJobs() {
		return minGapInMinitesBWJobs;
	}

	public void setMinGapInMinitesBWJobs(Integer minGapInMinitesBWJobs) {
		this.minGapInMinitesBWJobs = minGapInMinitesBWJobs;
	}

	public Integer getJobSearchDurationInHrs() {
		return jobSearchDurationInHrs;
	}

	public void setJobSearchDurationInHrs(Integer jobSearchDurationInHrs) {
		this.jobSearchDurationInHrs = jobSearchDurationInHrs;
	}

	public Integer getFailedJobSearchBackDurationInHrs() {
		return failedJobSearchBackDurationInHrs;
	}

	public void setFailedJobSearchBackDurationInHrs(Integer failedJobSearchBackDurationInHrs) {
		this.failedJobSearchBackDurationInHrs = failedJobSearchBackDurationInHrs;
	}

}
