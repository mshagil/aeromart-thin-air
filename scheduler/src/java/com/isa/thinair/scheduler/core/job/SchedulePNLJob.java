package com.isa.thinair.scheduler.core.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.scheduler.api.JobDetail;

public class SchedulePNLJob implements Job {
	public static final String PROP_FlightId = "prop.pnl.flightid";
	public static final String PROP_TransmissionTime = "prop.pnl.transmissiontime";

	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {
		// TODO Auto-generated method stub
		JobDetail jobDetail = (JobDetail) jobExcContext.getJobDetail();
		System.out.println("Executing " + jobDetail.getJobDataMap().getString(com.isa.thinair.scheduler.api.Job.PROP_JOB_GROUP)
				+ " ........" + this.PROP_FlightId);

	}

}
