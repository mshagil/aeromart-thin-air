/*
 * Created on Jul 8, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.scheduler.core.listeners;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.SchedulerListener;
import org.quartz.Trigger;
import org.quartz.TriggerKey;

/**
 * @author byorn
 * 
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style -
 *         Code Templates
 */
public class ASchedulerListener implements SchedulerListener {

	private String name;

	private Log log = LogFactory.getLog(getClass());

	public ASchedulerListener(String name) {
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void jobScheduled(Trigger arg0) {
		// TODO Auto-generated method stub

	}

	public void jobUnscheduled(String arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	public void triggerFinalized(Trigger arg0) {
		// TODO Auto-generated method stub

	}

	public void triggersPaused(String arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	public void triggersResumed(String arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	public void jobsPaused(String arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	public void jobsResumed(String arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	public void schedulerError(String arg0, SchedulerException se) {

		if (se != null) {
			log.error(arg0, se);

		}

	}

	public void schedulerShutdown() {
		// TODO Auto-generated method stub

	}

	@Override
	public void jobAdded(JobDetail arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void jobDeleted(JobKey arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void jobPaused(JobKey arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void jobResumed(JobKey arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void jobUnscheduled(TriggerKey arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void jobsPaused(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void jobsResumed(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void schedulerInStandbyMode() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void schedulerShuttingdown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void schedulerStarted() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void schedulerStarting() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void schedulingDataCleared() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void triggerPaused(TriggerKey arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void triggerResumed(TriggerKey arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void triggersPaused(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void triggersResumed(String arg0) {
		// TODO Auto-generated method stub
		
	}

}
