package com.isa.thinair.airpricing.api.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 * @hibernate.class table = "T_FARE_RULE_PAX_FEES"
 */
public class FareRulePaxFees extends Tracking {
	private static final long serialVersionUID = -5143678825606956579L;

	private Long fareRulePaxFeeID;

	private FareRule fareRule;

	private long startCutOver;

	private long endCutOver;

	private String feeType;

	private String adultAmountSpecifiedAs;

	private double adultAmount;

	private double adultMinimumAmountThresold;

	private double adultMaximumAmountThresold;

	private String childAmountSpecifiedAs;

	private double childAmount;

	private double childMinimumAmountThresold;

	private double childMaximumAmountThresold;

	private String infantAmountSpecifiedAs;

	private double infantAmount;

	private double infantMinimumAmountThresold;

	private double infantMaximumAmountThresold;

	/**
	 * @return the fareRulePaxFeeID
	 * @hibernate.id column = "FARE_RULE_PAX_FEES_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value ="S_FARE_RULE_PAX_FEES"
	 */
	public Long getFareRulePaxFeeID() {
		return fareRulePaxFeeID;
	}

	public void setFareRulePaxFeeID(Long fareRulePaxFeeID) {
		this.fareRulePaxFeeID = fareRulePaxFeeID;
	}

	/**
	 * @return the fareRule
	 * @hibernate.many-to-one column="FARE_RULE_ID" class="com.isa.thinair.airpricing.api.model.FareRule"
	 */
	public FareRule getFareRule() {
		return fareRule;
	}

	public void setFareRule(FareRule fareRule) {
		this.fareRule = fareRule;
	}

	/**
	 * @return Returns the startCutOver.
	 * @hibernate.property column = "START_CUTOVER"
	 */
	public long getStartCutOver() {
		return startCutOver;
	}

	public void setStartCutOver(long startCutOver) {
		this.startCutOver = startCutOver;
	}

	/**
	 * @return Returns the endCutOver.
	 * @hibernate.property column = "END_CUTOVER"
	 */
	public long getEndCutOver() {
		return endCutOver;
	}

	public void setEndCutOver(long endCutOver) {
		this.endCutOver = endCutOver;
	}

	/**
	 * @return Returns the feeType.
	 * @hibernate.property column = "FEE_TYPE"
	 */
	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	/**
	 * @return Returns the adultAmountSpecifiedAs.
	 * @hibernate.property column = "ADULT_AMOUNT_SPECIFIED_AS"
	 */
	public String getAdultAmountSpecifiedAs() {
		return adultAmountSpecifiedAs;
	}

	public void setAdultAmountSpecifiedAs(String adultAmountSpecifiedAs) {
		this.adultAmountSpecifiedAs = adultAmountSpecifiedAs;
	}

	/**
	 * @return Returns the adultAmountSpecifiedAs.
	 * @hibernate.property column = "ADULT_AMOUNT"
	 */
	public double getAdultAmount() {
		return adultAmount;
	}

	public void setAdultAmount(double adultAmount) {
		this.adultAmount = adultAmount;
	}

	/**
	 * @return Returns the adultMinimumAmountThresold.
	 * @hibernate.property column = "ADULT_MIN_AMOUNT_THRESHOLD"
	 */
	public double getAdultMinimumAmountThresold() {
		return adultMinimumAmountThresold;
	}

	public void setAdultMinimumAmountThresold(double adultMinimumAmountThresold) {
		this.adultMinimumAmountThresold = adultMinimumAmountThresold;
	}

	/**
	 * @return Returns the adultMiximumAmountThresold.
	 * @hibernate.property column = "ADULT_MAX_AMOUNT_THRESHOLD"
	 */
	public double getAdultMaximumAmountThresold() {
		return adultMaximumAmountThresold;
	}

	public void setAdultMaximumAmountThresold(double adultMaximumAmountThresold) {
		this.adultMaximumAmountThresold = adultMaximumAmountThresold;
	}

	/**
	 * @return Returns the childAmountSpecifiedAs.
	 * @hibernate.property column = "CHILD_AMOUNT_SPECIFIED_AS"
	 */
	public String getChildAmountSpecifiedAs() {
		return childAmountSpecifiedAs;
	}

	public void setChildAmountSpecifiedAs(String childAmountSpecifiedAs) {
		this.childAmountSpecifiedAs = childAmountSpecifiedAs;
	}

	/**
	 * @return Returns the childAmount.
	 * @hibernate.property column = "CHILD_AMOUNT"
	 */
	public double getChildAmount() {
		return childAmount;
	}

	public void setChildAmount(double childAmount) {
		this.childAmount = childAmount;
	}

	/**
	 * @return Returns the childMinimumAmountThresold.
	 * @hibernate.property column = "CHILD_MIN_AMOUNT_THRESHOLD"
	 */
	public double getChildMinimumAmountThresold() {
		return childMinimumAmountThresold;
	}

	public void setChildMinimumAmountThresold(double childMinimumAmountThresold) {
		this.childMinimumAmountThresold = childMinimumAmountThresold;
	}

	/**
	 * @return Returns the childMiximumAmountThresold.
	 * @hibernate.property column = "CHILD_MAX_AMOUNT_THRESHOLD"
	 */
	public double getChildMaximumAmountThresold() {
		return childMaximumAmountThresold;
	}

	public void setChildMaximumAmountThresold(double childMiximumAmountThresold) {
		this.childMaximumAmountThresold = childMiximumAmountThresold;
	}

	/**
	 * @return Returns the infantAmountSpecifiedAs.
	 * @hibernate.property column = "INF_AMOUNT_SPECIFIED_AS"
	 */
	public String getInfantAmountSpecifiedAs() {
		return infantAmountSpecifiedAs;
	}

	public void setInfantAmountSpecifiedAs(String infantAmountSpecifiedAs) {
		this.infantAmountSpecifiedAs = infantAmountSpecifiedAs;
	}

	/**
	 * @return Returns the infantAmount.
	 * @hibernate.property column = "INF_AMOUNT"
	 */
	public double getInfantAmount() {
		return infantAmount;
	}

	public void setInfantAmount(double infantAmount) {
		this.infantAmount = infantAmount;
	}

	/**
	 * @return Returns the infantMinimumAmountThresold.
	 * @hibernate.property column = "INF_MIN_AMOUNT_THRESHOLD"
	 */
	public double getInfantMinimumAmountThresold() {
		return infantMinimumAmountThresold;
	}

	public void setInfantMinimumAmountThresold(double infantMinimumAmountThresold) {
		this.infantMinimumAmountThresold = infantMinimumAmountThresold;
	}

	/**
	 * @return Returns the infantMiximumAmountThresold.
	 * @hibernate.property column = "INF_MAX_AMOUNT_THRESHOLD"
	 */
	public double getInfantMaximumAmountThresold() {
		return infantMaximumAmountThresold;
	}

	public void setInfantMaximumAmountThresold(double infantMiximumAmountThresold) {
		this.infantMaximumAmountThresold = infantMiximumAmountThresold;
	}

	/**
	 * Overrides hashcode to support lazy loading
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getFareRulePaxFeeID()).toHashCode();
	}

	/**
	 * Overides the equals
	 * 
	 * @param o
	 * @return
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof FareRulePaxFees) {
			FareRulePaxFees castObject = (FareRulePaxFees) o;
			if (castObject.getFareRulePaxFeeID() == null || this.getFareRulePaxFeeID() == null) {
				return false;
			} else if (castObject.getFareRulePaxFeeID() == this.getFareRulePaxFeeID()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

}
