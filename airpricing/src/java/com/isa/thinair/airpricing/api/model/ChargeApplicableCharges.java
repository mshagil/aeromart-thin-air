package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;

/**
 * @author Manjula
 * 
 * @hibernate.class table = "T_CHARGE_APPLICABLE_CHARGES"
 */
public class ChargeApplicableCharges implements Serializable{
	
	private static final long serialVersionUID = 5012554443147523773L;

	private Integer chargeApplicableId;

	private String chargeCode;

	private String applicableChargeCode;

	private String applyStatus;

	/**
	 * @hibernate.id column = "CHARGE_APPLICABLE_CHARGES_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CHARGE_APPLICABLE_CHARGES"
	 * @return
	 */
	public Integer getChargeApplicableId() {
		return chargeApplicableId;
	}

	public void setChargeApplicableId(Integer chargeApplicableId) {
		this.chargeApplicableId = chargeApplicableId;
	}

	/**
	 * @hibernate.property column = "CHARGE_CODE"
	 * @return
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeApplicableCode) {
		this.chargeCode = chargeApplicableCode;
	}

	/**
	 * @hibernate.property column = "APPLICABLE_CHARGE_CODE"
	 * @return
	 */
	public String getApplicableChargeCode() {
		return applicableChargeCode;
	}

	public void setApplicableChargeCode(String applicableChargeCode) {
		this.applicableChargeCode = applicableChargeCode;
	}

	/**
	 * @hibernate.property column = "APPLY_STATUS"
	 * @return
	 */
	public String getApplyStatus() {
		return applyStatus;
	}

	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}

}
