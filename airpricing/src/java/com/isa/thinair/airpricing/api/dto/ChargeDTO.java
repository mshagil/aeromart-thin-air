package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;

import com.isa.thinair.airpricing.api.model.Charge;

public class ChargeDTO implements Serializable {

	private static final long serialVersionUID = 2145401787320596821L;
	private String chargeCode;
	private Charge charge;

	public Charge getCharge() {
		return charge;
	}

	public void setCharge(Charge charge) {
		this.charge = charge;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

}
