package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.HashCodeBuilder;


/**
 * @hibernate.class table = "BG_T_OND_TEMPLATE_CHANNEL"
 */
public class BaggageTemplateSalesChannel implements Serializable {

	private Integer id;
	private ONDBaggageTemplate baggageTemplate;
	private String salesChannelCode;
	
	/**
	 * @hibernate.id column = "OND_TEMPLATE_CHANNEL_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "BG_S_OND_TEMPLATE_CHANNEL"
	 */
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @hibernate.many-to-one column="OND_CHARGE_TEMPLATE_ID" class="com.isa.thinair.airpricing.api.model.ONDBaggageTemplate"
	 */
	public ONDBaggageTemplate getBaggageTemplate() {
		return baggageTemplate;
	}
	public void setBaggageTemplate(ONDBaggageTemplate baggageTemplate) {
		this.baggageTemplate = baggageTemplate;
	}
	
	/**
	 * @hibernate.property column = "SALES_CHANNEL"
	 */
	public String getSalesChannelCode() {
		return salesChannelCode;
	}
	public void setSalesChannelCode(String salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}
	
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BaggageTemplateSalesChannel salesChannel = (BaggageTemplateSalesChannel) o;

		if (salesChannelCode != null ? !salesChannelCode.equals(salesChannel.salesChannelCode) : salesChannel.salesChannelCode != null) return false;

		return true;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getSalesChannelCode()).toHashCode();
	}
	
}
