/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.airpricing.api.criteria.BaggageTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ChargeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ChargeTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.MealTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ONDChargeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ONDCodeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.SSRChargesSearchCriteria;
import com.isa.thinair.airpricing.api.dto.BaggageTemplateDTO;
import com.isa.thinair.airpricing.api.dto.BaggageTemplateTo;
import com.isa.thinair.airpricing.api.dto.CCChargeTO;
import com.isa.thinair.airpricing.api.dto.MealTemplateDTO;
import com.isa.thinair.airpricing.api.dto.ONDBaggageTemplateDTO;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.dto.QuotedONDChargesDTO;
import com.isa.thinair.airpricing.api.dto.SSRChargeDTO;
import com.isa.thinair.airpricing.api.dto.SSRChargeSearchDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airpricing.api.model.BaggageCharge;
import com.isa.thinair.airpricing.api.model.BaggageTemplate;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.MCO;
import com.isa.thinair.airpricing.api.model.MCOServices;
import com.isa.thinair.airpricing.api.model.MealCharge;
import com.isa.thinair.airpricing.api.model.MealTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggageChargeTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.airpricing.api.model.PaymentGatewayWiseCharges;
import com.isa.thinair.airpricing.api.model.ReportChargeGroup;
import com.isa.thinair.airpricing.api.model.SSRCharge;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.SSRChargesDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Byorn
 */
public interface ChargeBD {

	public static final String SERVICE_NAME = "ChargeService";

	/** Charges **/
	public Page getCharges(ChargeSearchCriteria chargeSearchCriteria, int startIndex, int pageSize, List<String> orderbyList)
			throws ModuleException;

	public Charge getCharge(ChargeSearchCriteria chargeSearchCriteria) throws ModuleException;

	public Charge getCharge(String chargeCode) throws ModuleException;

	public void createCharge(Charge charge) throws ModuleException;

	public void updateCharge(Charge charge) throws ModuleException;

	public void deleteCharge(String chargeCode) throws ModuleException;

	public void deleteCharge(Charge charge) throws ModuleException;

	public void deleteCharge(String chargeCode, long version) throws ModuleException;

	public Collection<Charge> getGlobalCharges() throws ModuleException;

	/**
	 * Will return a HashMap [key: INS value: QuotedChargeTO] Assumption: For a give charge group there will be only one
	 * active chargerate
	 */
	public HashMap<String, QuotedChargeDTO> quoteUniqueChargeForGroups(Collection<String> chargeGroups) throws ModuleException;

	/** Method to Search OND Charges **/
	public Page getONDCharges(ONDChargeSearchCriteria criteria, int startIndex, int pageSize, List<String> orderbyList)
			throws ModuleException;

	public HashMap<String, HashMap<String, QuotedChargeDTO>> quoteCharges(Collection<QuoteChargesCriteria> criteriaCollection)
			throws ModuleException;

	public HashMap<String, QuotedChargeDTO> rebuildQuoteCharges(Collection<Integer> chargeRateIds, QuoteChargesCriteria criteria)
			throws ModuleException;

	/**
	 * 
	 * @param criteriaCollection
	 *            Collection QuoteChargesCriteria
	 * @return
	 * @throws ModuleException
	 */
	public QuotedONDChargesDTO quoteONDCharges(Collection<QuoteChargesCriteria> criteriaCollection) throws ModuleException;

	public Map<String, String> getChargeGroups() throws ModuleException;

	/**
	 * Return charge charge with rate applicable for the specified effective date.
	 * 
	 * @param chargeCode
	 * @param effectiveDate
	 * @param fareSummaryDTO
	 *            basis for charge value when charge is specified as a percentage
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public QuotedChargeDTO getCharge(String chargeCode, Date effectiveDate, FareSummaryDTO fareSummaryDTO,
			Integer operationalMode, boolean checkSalesValidity) throws ModuleException;

	/**
	 * @param chargeCode
	 * @param effectiveDate
	 * @param fareSummaryDTO
	 * @param operationlaMode
	 * @return
	 * @throws ModuleException
	 */
	public List<QuotedChargeDTO> getCharges(String chargeCode, Date effectiveDate, FareSummaryDTO fareSummaryDTO,
			Integer operationalMode) throws ModuleException;

	/**
	 * Get ChargeTemplate
	 * 
	 * @param templateId
	 * @return ChargeTemplate
	 */
	public ChargeTemplate getChargeTemplate(int templateId) throws ModuleException;

	/**
	 * Get ChargeTemplates for criteria
	 * 
	 * @param criteria
	 * @param startRec
	 * @param noRecs
	 * @return
	 * @throws ModuleException
	 */
	public Page<ChargeTemplate> getChargeTemplates(ChargeTemplateSearchCriteria criteria, int startRec, int noRecs)
			throws ModuleException;

	public Page<ChargeTemplate> getSeatCharges(ChargeTemplateSearchCriteria criteria, int startRec, int noRecs)
			throws ModuleException;

	/**
	 * @param save
	 *            new ONDs
	 * @throws ModuleException
	 */
	public void saveONDs(Set<String> newOnds) throws ModuleException;

	public boolean isONDsExists(Set<String> ondList) throws ModuleException;

	public void saveSeatChargeTemplate(ChargeTemplate chargeTemplate, Collection<SeatDTO> seatChargeDTO, boolean isDefAmountChanged) throws ModuleException;

	/**
	 * Save Charge Template
	 * 
	 * @param chargeTemplate
	 * @param flitghtSegID
	 *            TODO
	 * @param modelNo
	 *            TODO
	 */
	public void saveTemplate(ChargeTemplate chargeTemplate, Integer flitghtSegID, String modelNo) throws ModuleException;

	public ChargeTemplate getTemplateId(String templateCode);

	public Collection<ChargeTemplate> getChargeTemplates(ChargeTemplateSearchCriteria criteria) throws ModuleException;

	public void deleteTemplate(int chargeTemplate) throws ModuleException;

	public ChargeTemplate copyChargeTemplate(int srcTemplateID, String newTemplateCode, Collection<SeatCharge> seatCharges)
			throws ModuleException;

	public Collection<SeatCharge> getSeatCharges(Collection<Integer> colAMSChaegeIDs) throws ModuleException;

	public Page<MealTemplate> getMealTemplates(MealTemplateSearchCriteria criteria, int startRec, int noRecs)
			throws ModuleException;

	public void saveMealTemplate(MealTemplateDTO mealTemplateDTO, Integer newChargeId) throws ModuleException;
	
	public void saveMealTemplate(MealTemplate mealTemplate, Integer newChargeId) throws ModuleException;
	
	public void deleteMealTemplate(MealTemplateDTO mealTemplateDTO) throws ModuleException;

	public Collection<MealTemplate> getMealTemplates(Integer mealId) throws ModuleException;

	public MealTemplate getMealTemplate(int tempalteID) throws ModuleException;

	public Collection<MealCharge> getMealCharges(Collection<Integer> soldMeals) throws ModuleException;

	public void reCalcChargesInLocalCurr(Date date) throws ModuleException;

	public void updateStatusForChargesInLocalCurr(Collection<CurrencyExchangeRate> currExchangeList, String status)
			throws ModuleException;

	/**
	 * @return Map<airportCode,Map<ssrChargeCode, SSRChargeDTO>>
	 * @param ssrChargeSearchDTO
	 * @return
	 * @throws ModuleException
	 */
	public HashMap<String, Collection<SSRChargeDTO>> getSSRCharges(SSRChargeSearchDTO ssrChargeSearchDTO) throws ModuleException;

	public BaggageTemplate getBaggageTemplate(int tempalteID) throws ModuleException;

	public Page getBaggageTemplates(BaggageTemplateSearchCriteria criteria, int startRec, int noRecs) throws ModuleException;

	public void saveBaggageTemplate(BaggageTemplateDTO baggageTemplateDTO, Integer newChargeId) throws ModuleException;
	
	public void saveBaggageTemplate(BaggageTemplate baggageTemplate, Integer newChargeId) throws ModuleException;
	
	public void deleteBaggageTemplate(BaggageTemplateDTO baggageTemplateDTO) throws ModuleException;

	public Page getONDBaggageTemplates(BaggageTemplateSearchCriteria criteria, int startRec, int noRecs) throws ModuleException;

	public ONDBaggageTemplate getONDBaggageTemplate(int templateId) throws ModuleException;

	public void saveONDBaggageTemplate(ONDBaggageTemplateDTO baggageTemplateDTO, Integer newChargeId) throws ModuleException;

	public void deleteONDBaggageTemplate(ONDBaggageTemplateDTO baggageTemplateDTO) throws ModuleException;

	@Deprecated
	public Page getONDCodes(ONDCodeSearchCriteria criteria, int startRec, int noRecs) throws ModuleException;

	public Page searchBaggageTemplates(ONDCodeSearchCriteria criteria, int startRec, int noRecs) throws ModuleException;

	public void saveONDCodes(ONDBaggageChargeTemplate baggageChargeTemplate, Integer newChargeId) throws ModuleException;

	public void saveBaggageTemplate(BaggageTemplateTo baggageTemplateTo) throws ModuleException;

	public void deleteONDCodes(ONDBaggageChargeTemplate baggageChargeTemplate) throws ModuleException;

	public boolean isONDCodeExist(String ondCode, int templateId) throws ModuleException;

	public boolean isActiveONDCodeExistForBaggageTemplate(int templateId, Date fromDate, Date toDate) throws ModuleException;

	public Collection<BaggageTemplate> getBaggageTemplatesCollect(Integer baggageId) throws ModuleException;

	public Collection<BaggageCharge> getBaggageCharges(Collection<Integer> soldBaggages) throws ModuleException;

	// SSR Charges
	public Page<SSRCharge> getSSRCharges(SSRChargesSearchCriteria criteria, int startRec, int noRecs) throws ModuleException;

	public void saveSSRCharges(SSRChargesDTO ssrChargesDTO,String cos) throws ModuleException;

	public SSRCharge getSSRCharges(int chargeId) throws ModuleException;

	public SSRCharge getSSRChargesBySSRId(int ssrId) throws ModuleException;

	public void deleteSSRCharges(SSRCharge ssrCharge) throws ModuleException;

	public boolean checkSSRChargeAlreadyDefined(int ssrId, String ssrChargeCode, int chargeId, ArrayList<String> ccList,
			ArrayList<String> lccList) throws ModuleException;

	// JIRA 6929
	public String getReportingChargeGroupCodeByChargeRateID(Integer chargeRateId) throws ModuleException;

	public void saveReportChargeGroup(ReportChargeGroup reportChargeGroup) throws ModuleException;

	public void deleteReportChargeGroup(ReportChargeGroup reportChargeGroup) throws ModuleException;

	public ReportChargeGroup getReportChargeGroupByRptChargeGroupCode(String reportChargeGroupCode) throws ModuleException;

	public void chargeUpdateOnExchangeRateChange() throws ModuleException;

	public List<Charge> getExcludableCharges() throws ModuleException;

	public boolean isValidChargeExclusion(List<String> excludingCharges) throws ModuleException;

	public boolean isValidChargeExists(String chargeCode, String ondCode) throws ModuleException;

	public Map<Integer, Map<CommonsConstants.BaggageCriterion, List<String>>> getOverlappedTemplates(ONDBaggageTemplate baggageTemplate);
	
	public PaymentGatewayWiseCharges getPaymentGatewayWiseCharge(int paymentGatewayId);
	
	public Page<CCChargeTO> getPaymentGatewayWiseCharge(int paymentGatewayId, int i, List<ModuleCriterion> criterion);

	public void savePaymentGatewayWiseCharges(PaymentGatewayWiseCharges paymentGatewayWiseCharges) throws ModuleException;
	
	public PaymentGatewayWiseCharges getUniquePaymentGatewayWiseCharge(int chargeId);
	
	public List<MCOServices> getMCOServices() throws ModuleException;

	public void saveMCO(MCO mco) throws ModuleException;

	public String getPrintMCOText(String mcoNumber, String agentCode) throws ModuleException;

	public String getNextMCOSQID() throws ModuleException;
	
	public void issueMCO(MCO mco, PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO) throws ModuleException;

	public void voidMCO(MCO mco, PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO) throws ModuleException;

	public MCO getMCO(String mcoNumber) throws ModuleException;
	
	public HashMap<String, Object> getMCORecord(String mcoNumber, String agentCode) throws CommonsDataAccessException;
	
	public void emailMCO(String mcoNumber, String agentCode) throws ModuleException;

	public List<HashMap<String, Object>> getMCORecords(String mcoNumber, Integer pnrPaxID) throws ModuleException;

	public boolean hasMCORecords(Integer pnrPaxID) throws ModuleException;

	public ServiceTaxQuoteForTicketingRevenueRS quoteServiceTaxForTicketingRevenue(
			ServiceTaxQuoteForTicketingRevenueRQ serviceTaxQuoteForTktRevRQ) throws ModuleException;

	public ServiceTaxQuoteForNonTicketingRevenueRS quoteServiceTaxForNonTicketingRevenue(
			ServiceTaxQuoteForNonTicketingRevenueRQ serviceTaxQuoteForNonTktRevRQ) throws ModuleException;

	public List<String> getAllActiveServiceTaxChargeCodes() throws ModuleException;

	public Collection<Charge> getCharges(ChargeSearchCriteria chargeSearchCriteria) throws ModuleException;

	public Map<String,Boolean> getChargeCodeWiseRefundability(List<String> chargeCodes) throws ModuleException;
	
	public Boolean isEnableLMSCreditBlock(int tempTransactionId);

	public void updateMealTemplates(Meal meal) throws ModuleException;
	
}
