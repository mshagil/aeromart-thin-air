/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:12
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @hibernate.class table = "T_FARE_RULE"
 */
public class FareRule extends Tracking {

	private static final long serialVersionUID = 6141852604444719470L;

	public static char Return_Flight_Yes = 'Y';
	public static char Return_Flight_No = 'N';
	public static char Refundable_No = 'N';
	public static String Status_Active = "ACT";
	public static String Status_Inactive = "INA";
	public static int NO_OF_PAX_COUNT = 3;
	public static String Print_Exp_Date_Yes = "Y";
	public static String Print_Exp_Date_No = "N";
	public static char Open_Return_Yes = 'Y';
	public static char Open_Return_No = 'N';
	public static final String CHARGE_TYPE_V = "V"; // TODO refactor use from
													// AirPricingCustomConstants
	public static final String CHARGE_TYPE_PF = "PF";
	public static final String CHARGE_TYPE_PFS = "PFS";
	public static final char FEE_CUTOVER_YES = 'Y';

	public static final char FEE_CUTOVER_NO = 'N';
	public static final char HALFRT_YES = 'Y';
	public static final char HALFRT_NO = 'N';

	private String fareRuleCode;

	private Integer advanceBookingDays;

	private String status;

	private char returnFlag = Open_Return_No;

	private Long minimumStayoverMins;

	private Long minimumStayoverMonths;

	private Long maximumStayoverMins;

	private Long maximumStayoverMonths;

	private Date outboundDepatureTimeFrom;

	private Date outboundDepatureTimeTo;

	private Date inboundDepatureTimeFrom;

	private Date inboundDepatureTimeTo;

	private OutboundDepartureFrequency outboundDepatureFreqency;

	private InboundDepartureFrequency inboundDepatureFrequency;

	private double modificationChargeAmount;

	private String modificationChargeType;

	private double cancellationChargeAmount;

	private String cancellationChargeType;

	private String rulesComments;

	private String fareRuleDescription;

	private String fareRulesIdentifier;

	private String fareBasisCode;

	private Set<Integer> visibleChannelIds;

	private Set<String> visibileAgentCodes;

	private Integer fareRuleId;

	private String paxCategoryCode;

	private String fareCategoryCode;

	private Collection<FareRulePax> paxDetails;

	private String internationalModifyBufferTime;

	private String domesticModifyBufferTime;

	private String printExpDate = Print_Exp_Date_No;

	private char OpenReturnFlag = Open_Return_No;

	private Long openRetConfPeriod;

	private Long openRenConfPeriodMonths;

	private Set<Integer> flexiCodes;

	private Double minModificationAmount;

	private Double maximumModificationAmount;

	private Double minCancellationAmount;

	private Double maximumCancellationAmount;

	/* fare rule modifications and load factor */
	private String modifyByDate;

	private String modifyByRoute;

	private Integer loadFactorMin;

	private Integer loadFactorMax;

	private Integer fareDiscountMin;

	private Integer fareDiscountMax;

	private Collection<FareRuleFee> fees;

	private String agentInstructions;

	private char feeTwlHrsCutOverApplied = FEE_CUTOVER_NO;

	private char halfRTFlag = HALFRT_NO;

	private Double modChargeAmountInLocal = 0.0d;

	private Double cnxChargeAmountInLocal = 0.0d;

	private String localCurrencyCode;

	private boolean localCurrencySelected = false;

	private String agentCommissionType;

	private Double agentCommissionAmount;

	private Double agentCommissionAmountInLocal;

	private Double nameChangeChargeAmount = 0.0d;

	private Double nameChangeChargeAmountLocal = 0.0d;

	private String nameChangeChargeType;

	private Double minNCCAmount;

	private Double maxNCCAmount;

	private Set<String> pointOfSale;

	private Boolean applicableToAllCountries = Boolean.FALSE;

	private Set<FareRuleComment> fareRuleComments;
		
	private Character bulkTicket;
	
	private Integer minBulkTicketSeatCount;

	public FareRule() {
		this.modificationChargeType = CHARGE_TYPE_V;
		this.cancellationChargeType = CHARGE_TYPE_V;
	}

	/**
	 * @return Returns the fareRuleId.
	 * @hibernate.id column = "FARE_RULE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FARE_RULE"
	 */
	public Integer getFareRuleId() {
		return fareRuleId;
	}

	/**
	 * @param fareRuleId
	 *            The fareRuleId to set.
	 */
	public void setFareRuleId(Integer fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	/**
	 * @return Returns the fareRuleCode.
	 * @hibernate.property column = "FARE_RULE_CODE"
	 */
	public String getFareRuleCode() {
		return fareRuleCode;
	}

	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	/**
	 * @return Returns the advanceBookingDays.
	 * @hibernate.property column = "ADVANCE_BOOKING_DAYS"
	 */
	public Integer getAdvanceBookingDays() {
		return advanceBookingDays;
	}

	/**
	 * @param advanceBookingDays
	 *            The advanceBookingDays to set.
	 */
	public void setAdvanceBookingDays(Integer advanceBookingDays) {
		this.advanceBookingDays = advanceBookingDays;
	}

	/**
	 * @return Returns the cancellationChargeAmount.
	 * @hibernate.property column = "CANCELLATION_CHARGE_AMOUNT"
	 */
	public double getCancellationChargeAmount() {
		return cancellationChargeAmount;
	}

	/**
	 * @param cancellationChargeAmount
	 *            The cancellationChargeAmount to set.
	 */
	public void setCancellationChargeAmount(double cancellationChargeAmount) {
		this.cancellationChargeAmount = cancellationChargeAmount;
	}

	/**
	 * @return Returns the fareBasisCode.
	 * @hibernate.property column = "FARE_BASIS_CODE"
	 */
	public String getFareBasisCode() {
		if (fareBasisCode == null || "".equals(fareBasisCode)) {
			fareBasisCode = generateFareBasisCode();
		}

		return fareBasisCode;
	}

	/**
	 * @param fareBasisCode
	 *            The fareBasisCode to set.
	 */
	public void setFareBasisCode(String fareBasisCode) {

		this.fareBasisCode = fareBasisCode;
	}

	private String generateFareBasisCode() {
		String code = "";

		String standardOrspecial = fareRuleCode != null ? "S" : "P";
		String returnOrOneway = returnFlag == Return_Flight_Yes ? "R" : "O";
		String weekdayweekendOrfullweek = returnFlag == Return_Flight_Yes ? decideCodeForReturnFlight()
				: decideCodeForOneWayFlight();

		String daysInAdvance = advanceBookingDays != null ? String.valueOf(advanceBookingDays.intValue()) : "";

		String refundableOrNonRefundable = "";

		if (this.paxDetails != null && !this.paxDetails.isEmpty()) {
			Map<String, Boolean> hmPaxRefundable = new HashMap<String, Boolean>();
			Iterator<FareRulePax> itr = this.paxDetails.iterator();
			while (itr.hasNext()) {
				FareRulePax pax = (FareRulePax) itr.next();
				if (pax != null) {
					hmPaxRefundable.put(pax.getPaxType(), pax.getRefundableFares());
				}
			}

			boolean isRefundable = false;

			if (hmPaxRefundable.containsKey(PaxTypeTO.ADULT)) {
				isRefundable = (Boolean) hmPaxRefundable.get(PaxTypeTO.ADULT);
			} else if (hmPaxRefundable.containsKey(PaxTypeTO.CHILD)) {
				isRefundable = (Boolean) hmPaxRefundable.get(PaxTypeTO.CHILD);
			} else if (hmPaxRefundable.containsKey(PaxTypeTO.INFANT)) {
				// This scenario wills never occur because, always Infant/s do not fly alone.
				isRefundable = (Boolean) hmPaxRefundable.get(PaxTypeTO.INFANT);
			}

			refundableOrNonRefundable = (isRefundable ? "R" : "NR");
		}

		code = standardOrspecial + returnOrOneway + weekdayweekendOrfullweek + daysInAdvance + refundableOrNonRefundable;
		return code;
	}

	private String decideCodeForReturnFlight() {
		String code = "";
		boolean isweekend = false;
		boolean isweekday = false;
		boolean isfullweek = false;

		if (inboundDepatureFrequency != null && outboundDepatureFreqency != null) {
			if (inboundDepatureFrequency.getDay0() == 1 || inboundDepatureFrequency.getDay6() == 1
					|| outboundDepatureFreqency.getDay0() == 1 || outboundDepatureFreqency.getDay6() == 1) {
				isweekend = true;
			}

			if (inboundDepatureFrequency.getDay1() == 1 || inboundDepatureFrequency.getDay2() == 1
					|| inboundDepatureFrequency.getDay3() == 1 || inboundDepatureFrequency.getDay4() == 1
					|| inboundDepatureFrequency.getDay5() == 1 || outboundDepatureFreqency.getDay1() == 1
					|| outboundDepatureFreqency.getDay2() == 1 || outboundDepatureFreqency.getDay3() == 1
					|| outboundDepatureFreqency.getDay4() == 1 || outboundDepatureFreqency.getDay5() == 1) {
				isweekday = true;
			}

			if (inboundDepatureFrequency.getDay0() == 1 && inboundDepatureFrequency.getDay1() == 1
					&& inboundDepatureFrequency.getDay2() == 1 && inboundDepatureFrequency.getDay3() == 1
					&& inboundDepatureFrequency.getDay4() == 1 && inboundDepatureFrequency.getDay5() == 1
					&& inboundDepatureFrequency.getDay6() == 1 && outboundDepatureFreqency.getDay0() == 1
					&& outboundDepatureFreqency.getDay1() == 1 && outboundDepatureFreqency.getDay2() == 1
					&& outboundDepatureFreqency.getDay3() == 1 && outboundDepatureFreqency.getDay4() == 1
					&& outboundDepatureFreqency.getDay5() == 1 && outboundDepatureFreqency.getDay6() == 1) {
				isfullweek = true;
			}
		}
		if (isfullweek) {
			code = "WK";
			return code;
		}
		if (!(isweekday && isweekend)) {
			if (isweekday) {
				code = "WD";
				return code;
			}
			if (isweekend) {
				code = "WE";
				return code;
			}
		}

		return code;

	}

	private String decideCodeForOneWayFlight() {
		String code = "";
		boolean isweekend = false;
		boolean isweekday = false;
		boolean isfullweek = false;

		if (outboundDepatureFreqency != null) {

			if (outboundDepatureFreqency.getDay0() == 1 || outboundDepatureFreqency.getDay6() == 1) {
				isweekend = true;
			}

			if (outboundDepatureFreqency.getDay1() == 1 || outboundDepatureFreqency.getDay2() == 1
					|| outboundDepatureFreqency.getDay3() == 1 || outboundDepatureFreqency.getDay4() == 1
					|| outboundDepatureFreqency.getDay5() == 1) {
				isweekday = true;
			}

			if (outboundDepatureFreqency.getDay0() == 1 && outboundDepatureFreqency.getDay1() == 1
					&& outboundDepatureFreqency.getDay2() == 1 && outboundDepatureFreqency.getDay3() == 1
					&& outboundDepatureFreqency.getDay4() == 1 && outboundDepatureFreqency.getDay5() == 1
					&& outboundDepatureFreqency.getDay6() == 1) {
				isfullweek = true;
			}
		}
		if (isfullweek) {
			code = "WK";
			return code;
		}
		if (!(isweekday && isweekend)) {
			if (isweekday) {
				code = "WD";
				return code;
			}
			if (isweekend) {
				code = "WE";
				return code;
			}
		}

		return code;

	}

	/**
	 * @return Returns the fareRuleDescription.
	 * @hibernate.property column = "FARE_RULE_DESCRIPTION"
	 */
	public String getFareRuleDescription() {
		return fareRuleDescription;
	}

	/**
	 * @param fareRuleDescription
	 *            The fareRuleDescription to set.
	 */
	public void setFareRuleDescription(String fareRuleDescription) {
		this.fareRuleDescription = fareRuleDescription;
	}

	/**
	 * @return Returns the fareRulesIdentifier.
	 * @hibernate.property column = "FARE_RULE_IDENTIFIER"
	 */
	public String getFareRulesIdentifier() {
		return fareRulesIdentifier;
	}

	/**
	 * @param fareRulesIdentifier
	 *            The fareRulesIdentifier to set.
	 */
	public void setFareRulesIdentifier(String fareRulesIdentifier) {
		this.fareRulesIdentifier = fareRulesIdentifier;
	}

	/**
	 * @return Returns the inboundDepatureFrequency.
	 * @hibernate.component
	 */
	public InboundDepartureFrequency getInboundDepatureFrequency() {
		return inboundDepatureFrequency;
	}

	/**
	 * @param inboundDepatureFrequency
	 *            The inboundDepatureFrequency to set.
	 */
	public void setInboundDepatureFrequency(InboundDepartureFrequency inboundDepatureFrequency) {
		this.inboundDepatureFrequency = inboundDepatureFrequency;
	}

	/**
	 * @return Returns the inboundDepatureTimeFrom.
	 * @hibernate.property column = "ARRIVAL_TIME_FROM"
	 */
	public Date getInboundDepatureTimeFrom() {
		return inboundDepatureTimeFrom;
	}

	/**
	 * @param inboundDepatureTimeFrom
	 *            The inboundDepatureTimeFrom to set.
	 */
	public void setInboundDepatureTimeFrom(Date inboundDepatureTimeFrom) {
		this.inboundDepatureTimeFrom = inboundDepatureTimeFrom;
	}

	/**
	 * @return Returns the inboundDepatureTimeTo.
	 * @hibernate.property column = "ARRIVAL_TIME_TO"
	 */
	public Date getInboundDepatureTimeTo() {
		return inboundDepatureTimeTo;
	}

	/**
	 * @param inboundDepatureTimeTo
	 *            The inboundDepatureTimeTo to set.
	 */
	public void setInboundDepatureTimeTo(Date inboundDepatureTimeTo) {
		this.inboundDepatureTimeTo = inboundDepatureTimeTo;
	}

	/**
	 * @return Returns the maximumStayoverDays.
	 * @hibernate.property column = "MAXIMUM_STAY_OVER_MINS"
	 */
	public Long getMaximumStayoverMins() {
		return maximumStayoverMins;
	}

	/**
	 * @param maximumStayoverDays
	 *            The maximumStayoverDays to set.
	 */
	public void setMaximumStayoverMins(Long maximumStayoverDays) {
		this.maximumStayoverMins = maximumStayoverDays;
	}

	/**
	 * @return Returns the minimumStayoverDays.
	 * @hibernate.property column = "MINIMUM_STAY_OVER_MINS"
	 */
	public Long getMinimumStayoverMins() {
		return minimumStayoverMins;
	}

	/**
	 * @param minimumStayoverDays
	 *            The minimumStayoverDays to set.
	 */
	public void setMinimumStayoverMins(Long minimumStayoverDays) {
		this.minimumStayoverMins = minimumStayoverDays;
	}

	/**
	 * @return Returns the modificationChargeAmount.
	 * @hibernate.property column = "MODIFICATION_CHARGE_AMOUNT"
	 */
	public double getModificationChargeAmount() {
		return modificationChargeAmount;
	}

	/**
	 * @param modificationChargeAmount
	 *            The modificationChargeAmount to set.
	 */
	public void setModificationChargeAmount(double modificationChargeAmount) {
		this.modificationChargeAmount = modificationChargeAmount;
	}

	/**
	 * @return Returns the outboundDepatureFreqency.
	 * @hibernate.component
	 */
	public OutboundDepartureFrequency getOutboundDepatureFreqency() {
		return outboundDepatureFreqency;
	}

	/**
	 * @param outboundDepatureFreqency
	 *            The outboundDepatureFreqency to set.
	 */
	public void setOutboundDepatureFreqency(OutboundDepartureFrequency outboundDepatureFreqency) {
		this.outboundDepatureFreqency = outboundDepatureFreqency;
	}

	/**
	 * @return Returns the outboundDepatureTimeFrom.
	 * @hibernate.property column = "DEPARTURE_TIME_FROM"
	 */
	public Date getOutboundDepatureTimeFrom() {
		return outboundDepatureTimeFrom;
	}

	/**
	 * @param outboundDepatureTimeFrom
	 *            The outboundDepatureTimeFrom to set.
	 */
	public void setOutboundDepatureTimeFrom(Date outboundDepatureTimeFrom) {
		this.outboundDepatureTimeFrom = outboundDepatureTimeFrom;
	}

	/**
	 * @return Returns the outboundDepatureTimeTo.
	 * @hibernate.property column = "DEPARTURE_TIME_TO"
	 */
	public Date getOutboundDepatureTimeTo() {
		return outboundDepatureTimeTo;
	}

	/**
	 * @param outboundDepatureTimeTo
	 *            The outboundDepatureTimeTo to set.
	 */
	public void setOutboundDepatureTimeTo(Date outboundDepatureTimeTo) {
		this.outboundDepatureTimeTo = outboundDepatureTimeTo;
	}

	/**
	 * @return Returns the returnFlag.
	 * @hibernate.property column = "RETURN_FLAG"
	 */
	public char getReturnFlag() {
		return returnFlag;
	}

	/**
	 * @param returnFlag
	 *            The returnFlag to set.
	 */
	public void setReturnFlag(char returnFlag) {
		this.returnFlag = returnFlag;
	}

	/**
	 * @return Returns the rulesComments.
	 * @hibernate.property column = "RULES_COMMENTS"
	 */
	public String getRulesComments() {
		return rulesComments;
	}

	/**
	 * @param rulesComments
	 *            The rulesComments to set.
	 */
	public void setRulesComments(String rulesComments) {
		this.rulesComments = rulesComments;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the visibileAgentCodes.
	 * 
	 * @hibernate.set table="T_FARE_AGENT" cascade="all"
	 * @hibernate.collection-key column="FARE_RULE_ID"
	 * @hibernate.collection-element type="string" column="AGENT_CODE"
	 */
	public Set<String> getVisibileAgentCodes() {
		return visibileAgentCodes;
	}

	/**
	 * @param visibileAgentCodes
	 *            The visibileAgentCodes to set.
	 */
	public void setVisibileAgentCodes(Set<String> visibileAgentCodes) {
		this.visibileAgentCodes = visibileAgentCodes;
	}

	/**
	 * @return Returns the visibleChannelIds.
	 * 
	 * @hibernate.set table="T_FARE_VISIBILITY" cascade="all"
	 * @hibernate.collection-key column="FARE_RULE_ID"
	 * @hibernate.collection-element type="integer" column="SALES_CHANNEL_CODE"
	 * 
	 */
	public Set<Integer> getVisibleChannelIds() {
		return visibleChannelIds;
	}

	/**
	 * @param visibleChannelIds
	 *            The visibleChannelIds to set.
	 */
	public void setVisibleChannelIds(Set<Integer> visibleChannelIds) {
		this.visibleChannelIds = visibleChannelIds;
	}

	/**
	 * @return Returns the paxCategoryCode.
	 * @hibernate.property column = "pax_category_code"
	 */
	public String getPaxCategoryCode() {
		return paxCategoryCode;
	}

	/**
	 * @param paxCategoryCode
	 *            The paxCategoryCode to set.
	 */
	public void setPaxCategoryCode(String paxCategoryCode) {
		this.paxCategoryCode = paxCategoryCode;
	}

	/**
	 * @return Returns the fareCategoryCode.
	 * @hibernate.property column = "fare_cat_id"
	 */
	public String getFareCategoryCode() {
		return fareCategoryCode;
	}

	/**
	 * @param fareCategoryCode
	 *            The fareCategoryCode to set.
	 */
	public void setFareCategoryCode(String fareCategoryCode) {
		this.fareCategoryCode = fareCategoryCode;
	}

	/**
	 * @return Returns the paxDetails. as Collection of FareRulePax objects
	 * 
	 * @hibernate.set lazy="false" cascade="all" inverse="true"
	 * 
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.FareRulePax"
	 * 
	 * @hibernate.collection-key column="FARE_RULE_ID"
	 */
	public Collection<FareRulePax> getPaxDetails() {
		return paxDetails;
	}

	/**
	 * @param paxDetails
	 *            The paxDetails to set.
	 */
	public void setPaxDetails(Collection<FareRulePax> paxDetails) {
		this.paxDetails = paxDetails;
	}

	/**
	 * Add paxDetails
	 * 
	 * @param paxDetails
	 *            The paxDetails to set.
	 */
	public void addPaxDetails(FareRulePax pax) {
		if (this.getPaxDetails() == null) {
			this.setPaxDetails(new HashSet<FareRulePax>());
		}
		pax.setFareRule(this);
		this.getPaxDetails().add(pax);
	}

	/**
	 * @return Returns the internationalModifyBufferTime.
	 * @hibernate.property column = "MODIFY_BUFFER_TIME"
	 */
	public String getInternationalModifyBufferTime() {
		return internationalModifyBufferTime;
	}

	/**
	 * @param internationalModifyBufferTime
	 *            The internationalmmodifyBufferTime to set.
	 */
	public void setInternationalModifyBufferTime(String internationalModifyBufferTime) {
		this.internationalModifyBufferTime = internationalModifyBufferTime;
	}

	/**
	 * @return the domesticModifyBufferTime
	 * @hibernate.property column = "DOMESTIC_MODIFY_BUFFER_TIME"
	 */
	public String getDomesticModifyBufferTime() {
		return domesticModifyBufferTime;
	}

	/**
	 * @param domesticModifyBufferTime
	 *            the domesticModifyBufferTime to set
	 */
	public void setDomesticModifyBufferTime(String domesticModifyBufferTime) {
		this.domesticModifyBufferTime = domesticModifyBufferTime;
	}

	/**
	 * @return Returns the printExpDate.
	 * @hibernate.property column = "PRINT_EXPIRE_DATE"
	 */
	public String getStrPrintExpDate() {
		return printExpDate;
	}

	/**
	 * Get Print Exprire Date
	 * 
	 * @return
	 */
	public boolean getPrintExpDate() {
		if (Print_Exp_Date_Yes.equals(getStrPrintExpDate())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param printExpDate
	 *            The printExpDate to set.
	 */
	private void setStrPrintExpDate(String printExpDate) {
		this.printExpDate = printExpDate;
	}

	/**
	 * @param printExpDate
	 *            The printExpDate to set.
	 */
	public void setPrintExpDate(boolean printExpDate) {
		if (printExpDate) {
			this.setStrPrintExpDate(Print_Exp_Date_Yes);
		} else {
			this.setStrPrintExpDate(Print_Exp_Date_No);
		}
	}

	/**
	 * @return Returns the openRetConfPeriod.
	 * @hibernate.property column = "OPENRT_CONF_PERIOD_MINS"
	 */
	public Long getOpenReturnConfPeriod() {
		return openRetConfPeriod;
	}

	/**
	 * @param openrtConfPeriod
	 *            The openReturnConfPeriod to set.
	 */
	public void setOpenReturnConfPeriod(Long openReturnConfPeriod) {
		this.openRetConfPeriod = openReturnConfPeriod;
	}

	/**
	 * Get Open Return Flag
	 * 
	 * @return
	 */
	public boolean getOPenRetrun() {
		if (Open_Return_Yes == getOpenReturnFlag()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "IS_OPEN_RETURN"
	 */
	private char getOpenReturnFlag() {
		return OpenReturnFlag;
	}

	/**
	 * @param OpenReturnFlag
	 *            The printExpDate to set.
	 */
	public void setOpenReturn(boolean isOpenRT) {
		if (isOpenRT) {
			this.setOpenReturnFlag(Open_Return_Yes);
		} else {
			this.setOpenReturnFlag(Open_Return_No);
		}
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	private void setOpenReturnFlag(char openRtFlag) {
		this.OpenReturnFlag = openRtFlag;
	}

	/**
	 * @return the minimumStayoverMonths
	 * @hibernate.property column = "MINIMUM_STAY_OVER_MONTHS"
	 */
	public Long getMinimumStayoverMonths() {
		return minimumStayoverMonths;
	}

	/**
	 * @param minimumStayoverMonths
	 *            the minimumStayoverMonths to set
	 */
	public void setMinimumStayoverMonths(Long minimumStayoverMonths) {
		this.minimumStayoverMonths = minimumStayoverMonths;
	}

	/**
	 * @return the maximumStayoverMonths
	 * @hibernate.property column = "MAXIMUM_STAY_OVER_MONTHS"
	 */
	public Long getMaximumStayoverMonths() {
		return maximumStayoverMonths;
	}

	/**
	 * @param maximumStayoverMonths
	 *            the maximumStayoverMonths to set
	 */
	public void setMaximumStayoverMonths(Long maximumStayoverMonths) {
		this.maximumStayoverMonths = maximumStayoverMonths;
	}

	/**
	 * @return the openRenConfPeriodMonths
	 * @hibernate.property column = "OPENRT_CONF_STAY_OVER_MONTHS"
	 */
	public Long getOpenRenConfPeriodMonths() {
		return openRenConfPeriodMonths;
	}

	/**
	 * @param openRenConfPeriodMonths
	 *            the openRenConfPeriodMonths to set
	 */
	public void setOpenRenConfPeriodMonths(Long openRenConfPeriodMonths) {
		this.openRenConfPeriodMonths = openRenConfPeriodMonths;
	}

	/**
	 * @hibernate.set table="T_FARE_RULE_FLEXI_RULE" cascade="all"
	 * @hibernate.collection-key column="FARE_RULE_ID"
	 * @hibernate.collection-element type="integer" column="FLEXI_RULE_ID"
	 */
	public Set<Integer> getFlexiCodes() {
		return flexiCodes;
	}

	/**
	 * @param flexiCodes
	 *            The flexiCodes to set.
	 */
	public void setFlexiCodes(Set<Integer> flexiCodes) {
		this.flexiCodes = flexiCodes;
	}

	/**
	 * @return whether the modification charge is defined as percentage or value
	 * @hibernate.property column = "MODIFICATION_CHARGE_TYPE"
	 */
	public String getModificationChargeType() {
		return modificationChargeType;
	}

	public void setModificationChargeType(String modificationChargeType) {
		this.modificationChargeType = modificationChargeType;
	}

	/**
	 * @return whether the cancellation charge is defined as percentage or value
	 * @hibernate.property column = "CANCELLATION_CHARGE_TYPE"
	 */
	public String getCancellationChargeType() {
		return cancellationChargeType;
	}

	public void setCancellationChargeType(String cancellationChargeType) {
		this.cancellationChargeType = cancellationChargeType;
	}

	/**
	 * @return
	 * @hibernate.property column = "MIN_MODIFICATION_VALUE"
	 */
	public Double getMinModificationAmount() {
		return minModificationAmount;
	}

	/**
	 * @param minModificationAmount
	 *            the minModificationAmount to set
	 */
	public void setMinModificationAmount(Double minModificationAmount) {
		this.minModificationAmount = minModificationAmount;
	}

	/**
	 * @return
	 * @hibernate.property column = "MAX_MODIFICATION_VALUE"
	 */
	public Double getMaximumModificationAmount() {
		return maximumModificationAmount;
	}

	/**
	 * @param maximumModificationAmount
	 *            the maximumModificationAmount to set
	 */
	public void setMaximumModificationAmount(Double maximumModificationAmount) {
		this.maximumModificationAmount = maximumModificationAmount;
	}

	/**
	 * @return
	 * @hibernate.property column = "MIN_CANCELLATION_VALUE"
	 */
	public Double getMinCancellationAmount() {
		return minCancellationAmount;
	}

	/**
	 * @param minCancellationAmount
	 *            the minCancellationAmount to set
	 */
	public void setMinCancellationAmount(Double minCancellationAmount) {
		this.minCancellationAmount = minCancellationAmount;
	}

	/**
	 * @return
	 * @hibernate.property column = "MAX_CANCELLATION_VALUE"
	 */
	public Double getMaximumCancellationAmount() {
		return maximumCancellationAmount;
	}

	/**
	 * @param maximumCancellationAmount
	 *            the maximumCancellationAmount to set
	 */
	public void setMaximumCancellationAmount(Double maximumCancellationAmount) {
		this.maximumCancellationAmount = maximumCancellationAmount;
	}

	/**
	 * @return
	 * @hibernate.property column = "MODIFY_DATE_CHANGE"
	 */
	public String getModifyByDate() {
		return modifyByDate;
	}

	/**
	 * @param modifyByDate
	 *            the modifyByDate to set
	 */
	public void setModifyByDate(String modifyByDate) {
		this.modifyByDate = modifyByDate;
	}

	/**
	 * @return
	 * @hibernate.property column = "MODIFY_ROUTE_CHANGE"
	 */
	public String getModifyByRoute() {
		return modifyByRoute;
	}

	/**
	 * @param modifyByRoute
	 *            the modifyByRoute to set
	 */
	public void setModifyByRoute(String modifyByRoute) {
		this.modifyByRoute = modifyByRoute;
	}

	/**
	 * @return
	 * @hibernate.property column = "LOAD_FACTOR_MIN"
	 */
	public Integer getLoadFactorMin() {
		return loadFactorMin;
	}

	/**
	 * @param loadFactorMin
	 *            the loadFactorMin to set
	 */
	public void setLoadFactorMin(Integer loadFactorMin) {
		this.loadFactorMin = loadFactorMin;
	}

	/**
	 * @return
	 * @hibernate.property column = "LOAD_FACTOR_MAX"
	 */
	public Integer getLoadFactorMax() {
		return loadFactorMax;
	}

	/**
	 * @param loadFactorMax
	 *            the loadFactorMax to set
	 */
	public void setLoadFactorMax(Integer loadFactorMax) {
		this.loadFactorMax = loadFactorMax;
	}

	/**
	 * @return
	 * @hibernate.property column = "FARE_DISCOUNT_MIN"
	 */
	public Integer getFareDiscountMin() {
		return fareDiscountMin;
	}

	public void setFareDiscountMin(Integer fareDiscountMin) {
		this.fareDiscountMin = fareDiscountMin;
	}

	/**
	 * @return
	 * @hibernate.property column = "FARE_DISCOUNT_MAX"
	 */
	public Integer getFareDiscountMax() {
		return fareDiscountMax;
	}

	public void setFareDiscountMax(Integer fareDiscountMax) {
		this.fareDiscountMax = fareDiscountMax;
	}

	/**
	 * @return the fees
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" order-by="CHARGE_TYPE" inverse="true"
	 * @hibernate.collection-key column="FARE_RULE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.FareRuleFee"
	 */
	public Collection<FareRuleFee> getFees() {
		return fees;
	}

	/**
	 * @param fees
	 *            the fees to set
	 */
	public void setFees(Collection<FareRuleFee> fees) {
		this.fees = fees;
	}

	/**
	 * Adds a passenger
	 * 
	 * @param reservationPax
	 */
	public void addFee(FareRuleFee fareRuleFee) {
		if (this.getFees() == null) {
			this.setFees(new HashSet<FareRuleFee>());
		}

		fareRuleFee.setFareRule(this);
		this.getFees().add(fareRuleFee);
	}

	/**
	 * Remove a passenger
	 * 
	 * @param reservationPax
	 */
	public void removeFee(FareRuleFee fareRuleFee) {
		if (this.getFees() != null) {
			fareRuleFee.setFareRule(null);
			this.getFees().remove(fareRuleFee);
		}
	}

	/**
	 * Remove a passenger
	 * 
	 * @param reservationPax
	 */
	public void removeAllFee() {
		if (this.getFees() != null) {
			Collection<FareRuleFee> removeSet = new HashSet<FareRuleFee>();
			for (Iterator<FareRuleFee> iterator = this.getFees().iterator(); iterator.hasNext();) {
				FareRuleFee fareRuleFee = (FareRuleFee) iterator.next();
				fareRuleFee.setFareRule(null);
				removeSet.add(fareRuleFee);
			}

			this.getFees().removeAll(removeSet);
		}
	}

	/**
	 * @return the agentInstructions
	 * @hibernate.property column = "AGENT_INSTRUCTIONS"
	 */
	public String getAgentInstructions() {
		return agentInstructions;
	}

	/**
	 * @param agentInstructions
	 *            the agentInstructions to set
	 */
	public void setAgentInstructions(String agentInstructions) {
		this.agentInstructions = agentInstructions;
	}

	/**
	 * @return the feeTwlHrsCutOverApplied
	 * @hibernate.property column = "FEE_TWL_HRS_CUTOVER_APPLIED"
	 */
	public char getFeeTwlHrsCutOverApplied() {
		return feeTwlHrsCutOverApplied;
	}

	/**
	 * @param feeTwlHrsCutOverApplied
	 *            the feeTwlHrsCutOverApplied to set
	 */
	public void setFeeTwlHrsCutOverApplied(char feeTwlHrsCutOverApplied) {
		this.feeTwlHrsCutOverApplied = feeTwlHrsCutOverApplied;
	}

	/**
	 * @return Returns the returnFlag.
	 * @hibernate.property column = "HALFRT_FLAG"
	 */
	private char getHalfReturnFlag() {
		return halfRTFlag;
	}

	/**
	 * @param returnFlag
	 *            The returnFlag to set.
	 */
	private void setHalfReturnFlag(char halfRTFlag) {
		this.halfRTFlag = halfRTFlag;
	}

	public boolean getHalfReturn() {
		return (HALFRT_YES == getHalfReturnFlag());
	}

	public void setHalfReturn(boolean halfReturn) {
		if (halfReturn) {
			setHalfReturnFlag(HALFRT_YES);
		} else {
			setHalfReturnFlag(HALFRT_NO);
		}
	}

	/**
	 * @return the modChargeAmountInLocal
	 * @hibernate.property column = "MOD_CHARGE_AMOUNT_IN_LOCAL"
	 */
	public Double getModChargeAmountInLocal() {
		return modChargeAmountInLocal;
	}

	public void setModChargeAmountInLocal(Double modChargeAmountInLocal) {
		this.modChargeAmountInLocal = modChargeAmountInLocal;
	}

	/**
	 * @return the cnxChargeAmountInLocal
	 * @hibernate.property column = "CNX_CHARGE_AMOUNT_IN_LOCAL"
	 */
	public Double getCnxChargeAmountInLocal() {
		return cnxChargeAmountInLocal;
	}

	public void setCnxChargeAmountInLocal(Double cnxChargeAmountInLocal) {
		this.cnxChargeAmountInLocal = cnxChargeAmountInLocal;
	}

	/**
	 * @return the localCurrencyCode
	 * @hibernate.property column = "CURRENCY_CODE"
	 */
	public String getLocalCurrencyCode() {
		return localCurrencyCode;
	}

	public void setLocalCurrencyCode(String localCurrencyCode) {
		this.localCurrencyCode = localCurrencyCode;
	}

	/**
	 * @return Returns true if local Currency Selected
	 */
	public boolean isLocalCurrencySelected() {
		return localCurrencySelected;
	}

	/**
	 * @param localCurrencySelected
	 *            The boolean to set if local currency is selected.
	 */
	public void setLocalCurrencySelected(boolean localCurrencySelected) {
		this.localCurrencySelected = localCurrencySelected;
	}

	/**
	 * @hibernate.property column = "AGENT_COMMISSION_TYPE"
	 */
	public String getAgentCommissionType() {
		return agentCommissionType;
	}

	public void setAgentCommissionType(String agentCommissionType) {
		this.agentCommissionType = agentCommissionType;
	}

	/**
	 * @hibernate.property column = "AGENT_COMMISSION_AMOUNT"
	 */
	public Double getAgentCommissionAmount() {
		return agentCommissionAmount;
	}

	public void setAgentCommissionAmount(Double agentCommissionAmount) {
		this.agentCommissionAmount = agentCommissionAmount;
	}

	/**
	 * @hibernate.property column = "AGENT_COMMISSION_AMOUNT_LOCAL"
	 */
	public Double getAgentCommissionAmountInLocal() {
		return agentCommissionAmountInLocal;
	}

	public void setAgentCommissionAmountInLocal(Double agentCommissionAmountInLocal) {
		this.agentCommissionAmountInLocal = agentCommissionAmountInLocal;
	}

	/**
	 * @hibernate.property column = "NAMECHANGE_CHARGE_AMOUNT"
	 */
	public Double getNameChangeChargeAmount() {
		return nameChangeChargeAmount;
	}

	public void setNameChangeChargeAmount(Double nameChangeChargeAmount) {
		this.nameChangeChargeAmount = nameChangeChargeAmount;
	}

	/**
	 * @hibernate.property column = "NAMECHANGE_CHG_AMOUNT_IN_LOCAL"
	 */
	public Double getNameChangeChargeAmountLocal() {
		return nameChangeChargeAmountLocal;
	}

	public void setNameChangeChargeAmountLocal(Double nameChangeChargeAmountLocal) {
		this.nameChangeChargeAmountLocal = nameChangeChargeAmountLocal;
	}

	/**
	 * @hibernate.property column = "NAMECHANGE_CHARGE_TYPE"
	 */
	public String getNameChangeChargeType() {
		return nameChangeChargeType;
	}

	public void setNameChangeChargeType(String nameChangeChargeType) {
		this.nameChangeChargeType = nameChangeChargeType;
	}

	/**
	 * @hibernate.property column = "MIN_NAMECHANGE_VALUE"
	 */
	public Double getMinNCCAmount() {
		return minNCCAmount;
	}

	public void setMinNCCAmount(Double minNCCAmount) {
		this.minNCCAmount = minNCCAmount;
	}

	/**
	 * @hibernate.property column = "MAX_NAMECHANGE_VALUE"
	 */
	public Double getMaxNCCAmount() {
		return maxNCCAmount;
	}

	public void setMaxNCCAmount(Double maxNCCAmount) {
		this.maxNCCAmount = maxNCCAmount;
	}

	/**
	 * @return the pointOfSale
	 * @hibernate.set table="T_FARE_RULE_POS" cascade="all"
	 * @hibernate.collection-key column="FARE_RULE_ID"
	 * @hibernate.collection-element type="string" column="COUNTRY_CODE"
	 */
	public Set<String> getPointOfSale() {
		return pointOfSale;
	}

	/**
	 * @param pointOfSale
	 *            the pointOfSale to set
	 */
	public void setPointOfSale(Set<String> pointOfSale) {
		this.pointOfSale = pointOfSale;
	}

	/**
	 * @return the applicableToAllCountries
	 * @hibernate.property column = "APPLICABLE_TO_ALL_COUNTRIES" type = "yes_no"
	 */
	public Boolean getApplicableToAllCountries() {
		return applicableToAllCountries;
	}

	/**
	 * @param applicableToAllCountries
	 *            the applicableToAllCountries to set
	 */
	public void setApplicableToAllCountries(Boolean applicableToAllCountries) {
		if (applicableToAllCountries == null) {
			applicableToAllCountries = Boolean.FALSE;
		}
		this.applicableToAllCountries = applicableToAllCountries;
	}

	/**
	 * @return the fareRuleComments
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="FARE_RULE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.FareRuleComment"
	 */
	public Set<FareRuleComment> getFareRuleComments() {
		return fareRuleComments;
	}

	public void setFareRuleComments(Set<FareRuleComment> fareRuleComments) {
		this.fareRuleComments = fareRuleComments;
	}

	public void addFareRuleComment(FareRuleComment fareRuleComment) {
		if (fareRuleComments == null) {
			this.setFareRuleComments(new HashSet<FareRuleComment>());
		}
		fareRuleComments.add(fareRuleComment);
	}

	public void removeFareRuleComment(FareRuleComment fareRuleComment) {
		if (this.getFareRuleComments() != null) {
			fareRuleComment.setFareRule(null);
			this.getFareRuleComments().remove(fareRuleComment);
		}
	}

	/**
	 * @return Returns the returnFlag.
	 * @hibernate.property column = "BULK_TICKET_FLAG"
	 */
	public Character getBulkTicket() {
		return bulkTicket;
	}

	/**
	 * @param bulkTicket the bulkTicket to set
	 */
	public void setBulkTicket(Character bulkTicket) {
		this.bulkTicket = bulkTicket;
	}

	/**
	 * @return Returns the returnFlag.
	 * @hibernate.property column = "MIN_BULK_TICKET_SEAT_COUNT"
	 */
	public Integer getMinBulkTicketSeatCount() {
		return minBulkTicketSeatCount;
	}

	/**
	 * @param minBulkTicketSeatCount the minBulkTicketSeatCount to set
	 */
	public void setMinBulkTicketSeatCount(Integer minBulkTicketSeatCount) {
		this.minBulkTicketSeatCount = minBulkTicketSeatCount;
	}
}
