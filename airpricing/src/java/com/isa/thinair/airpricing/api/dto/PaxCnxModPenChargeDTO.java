package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class PaxCnxModPenChargeDTO implements Serializable {
	
	private static final long serialVersionUID = -872742184195676821L;
	
	private Integer paxSequence;
	
	private String paxType = PaxTypeTO.ADULT;
	
	private List<CnxModPenChargeDTO> cnxModPenChargeDTOs;
	
	private boolean totalAmountIncludingServiceTax;

	public Integer getPaxSequence() {
		return paxSequence;
	}

	public List<CnxModPenChargeDTO> getCnxModPenChargeDTOs() {
		return cnxModPenChargeDTOs;
	}

	public boolean isTotalAmountIncludingServiceTax() {
		return totalAmountIncludingServiceTax;
	}

	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	public void setCnxModPenChargeDTOs(List<CnxModPenChargeDTO> cnxModPenChargeDTOs) {
		this.cnxModPenChargeDTOs = cnxModPenChargeDTOs;
	}

	public void setTotalAmountIncludingServiceTax(boolean totalAmountIncludingServiceTax) {
		this.totalAmountIncludingServiceTax = totalAmountIncludingServiceTax;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

}
