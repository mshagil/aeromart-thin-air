package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class SSRChargeDTO implements Serializable, Cloneable {

	private static final long serialVersionUID = 9103532222448945399L;
	private Integer sSRChargeId;
	private String sSRChargeCode;
	private String description;
	private String oNDCode;
	private Integer sSRId;
	private String sSRCode;
	private BigDecimal chargeAmount;
	private Integer sSRSubCategoryId;
	private int adultCount;
	private int childCount;
	private int infantCount;

	private BigDecimal[] adultCharges;
	private BigDecimal[] childCharges;
	private BigDecimal[] infantCharges;

	private boolean applyForArrival;
	private boolean applyForDeparture;
	private boolean applyForTransit;
	private boolean applyForSegment;

	private boolean applyForAdult;
	private boolean applyForChild;
	private boolean applyForInfant;

	private Integer minimumTransitTime;
	private Integer maximumTransitTime;

	private Integer minimumTotalPax;
	private Integer maximumTotalPax;
	private Integer minimumAdultPax;
	private Integer maximumAdultPax;
	private Integer minimumChildPax;
	private Integer maximumChildPax;
	private Integer minimumInfantPax;
	private Integer maximumInfantPax;

	private String classOfService;

	private Integer attachSegmentCode;

	private boolean isOneway;

	private Integer segmentSeq;

	public Integer getSSRChargeId() {
		return this.sSRChargeId;
	}

	public void setSSRChargeId(Integer chargeId) {
		this.sSRChargeId = chargeId;
	}

	public String getSSRChargeCode() {
		return sSRChargeCode;
	}

	public void setSSRChargeCode(String sSRChargeCode) {
		this.sSRChargeCode = sSRChargeCode;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSSRId() {
		return sSRId;
	}

	public void setSSRId(Integer ssrId) {
		this.sSRId = ssrId;
	}

	public String getSSRCode() {
		return this.sSRCode;
	}

	public void setSSRCode(String code) {
		this.sSRCode = code;
	}

	public BigDecimal getChargeAmount() {
		return this.chargeAmount;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public Integer getSSRSubCategoryId() {
		return this.sSRSubCategoryId;
	}

	public void setSSRSubCategoryId(Integer subCategoryId) {
		this.sSRSubCategoryId = subCategoryId;
	}

	public String getONDCode() {
		return oNDCode;
	}

	public void setONDCode(String code) {
		oNDCode = code;
	}

	public boolean isApplyForSegment() {
		return this.applyForSegment;
	}

	public void setApplyForSegment(boolean applyForSegment) {
		this.applyForSegment = applyForSegment;
	}

	public boolean isApplyForArrival() {
		return applyForArrival;
	}

	public void setApplyForArrival(boolean applyOnArrival) {
		this.applyForArrival = applyOnArrival;
	}

	public boolean isApplyForDeparture() {
		return applyForDeparture;
	}

	public void setApplyForDeparture(boolean applyOnDeparture) {
		this.applyForDeparture = applyOnDeparture;
	}

	public boolean isApplyForTransit() {
		return applyForTransit;
	}

	public void setApplyForTransit(boolean applyOnTransit) {
		this.applyForTransit = applyOnTransit;
	}

	public boolean isApplyForAdult() {
		return this.applyForAdult;
	}

	public void setApplyForAdult(boolean applyForAdult) {
		this.applyForAdult = applyForAdult;
	}

	public boolean isApplyForChild() {
		return this.applyForChild;
	}

	public void setApplyForChild(boolean applyForChild) {
		this.applyForChild = applyForChild;
	}

	public boolean isApplyForInfant() {
		return this.applyForInfant;
	}

	public void setApplyForInfant(boolean applyForInfant) {
		this.applyForInfant = applyForInfant;
	}

	public String getClassOfService() {
		return this.classOfService;
	}

	public Integer getMinimumTransitTime() {
		return this.minimumTransitTime;
	}

	public void setMinimumTransitTime(Integer minimumTransitTime) {
		this.minimumTransitTime = minimumTransitTime;
	}

	public Integer getMaximumTransitTime() {
		return this.maximumTransitTime;
	}

	public void setMaximumTransitTime(Integer maximumTransitTime) {
		this.maximumTransitTime = maximumTransitTime;
	}

	public Integer getMinimumTotalPax() {
		return this.minimumTotalPax;
	}

	public void setMinimumTotalPax(Integer minimunTotalPax) {
		this.minimumTotalPax = minimunTotalPax;
	}

	public Integer getMaximumTotalPax() {
		return this.maximumTotalPax;
	}

	public void setMaximumTotalPax(Integer maximunTotalPax) {
		this.maximumTotalPax = maximunTotalPax;
	}

	public Integer getMinimumAdultPax() {
		return this.minimumAdultPax;
	}

	public void setMinimumAdultPax(Integer minimunAdultPax) {
		this.minimumAdultPax = minimunAdultPax;
	}

	public Integer getMaximumAdultPax() {
		return this.maximumAdultPax;
	}

	public void setMaximumAdultPax(Integer maximunAdultPax) {
		this.maximumAdultPax = maximunAdultPax;
	}

	public Integer getMinimumChildPax() {
		return this.minimumChildPax;
	}

	public void setMinimumChildPax(Integer minimunChildPax) {
		this.minimumChildPax = minimunChildPax;
	}

	public Integer getMaximumChildPax() {
		return this.maximumChildPax;
	}

	public void setMaximumChildPax(Integer maximunChildPax) {
		this.maximumChildPax = maximunChildPax;
	}

	public Integer getMinimumInfantPax() {
		return this.minimumInfantPax;
	}

	public void setMinimumInfantPax(Integer minimunInfantPax) {
		this.minimumInfantPax = minimunInfantPax;
	}

	public Integer getMaximumInfantPax() {
		return this.maximumInfantPax;
	}

	public void setMaximumInfantPax(Integer maximunInfantPax) {
		this.maximumInfantPax = maximunInfantPax;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}

	public int getAdultCount() {
		return this.adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return this.childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getInfantCount() {
		return this.infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public BigDecimal[] getAdultCharges() {
		return this.adultCharges;
	}

	public void setAdultCharges(BigDecimal[] adultCharges) {
		this.adultCharges = adultCharges;
	}

	public BigDecimal[] getChildCharges() {
		return this.childCharges;
	}

	public void setChildCharges(BigDecimal[] childCharges) {
		this.childCharges = childCharges;
	}

	public BigDecimal[] getInfantCharges() {
		return this.infantCharges;
	}

	public void setInfantCharges(BigDecimal[] infantCharges) {
		this.infantCharges = infantCharges;
	}

	/**
	 * @return Returns the attachSegmentCode.
	 */
	public Integer getAttachSegmentCode() {
		return attachSegmentCode;
	}

	/**
	 * @param attachSegmentCode
	 *            The attachSegmentCode to set.
	 */
	public void setAttachSegmentCode(Integer attachSegmentCode) {
		this.attachSegmentCode = attachSegmentCode;
	}

	@Override
	public SSRChargeDTO clone() throws CloneNotSupportedException {
		return (SSRChargeDTO) super.clone();
	}

	/**
	 * @return Returns the isOneway.
	 */
	public boolean isOneway() {
		return isOneway;
	}

	/**
	 * @param isOneway
	 *            The isOneway to set.
	 */
	public void setOneway(boolean isOneway) {
		this.isOneway = isOneway;
	}

	/**
	 * @return Returns the segmentSeq.
	 */
	public Integer getSegmentSeq() {
		return segmentSeq;
	}

	/**
	 * @param segmentSeq
	 *            The segmentSeq to set.
	 */
	public void setSegmentSeq(Integer segmentSeq) {
		this.segmentSeq = segmentSeq;
	}
}
