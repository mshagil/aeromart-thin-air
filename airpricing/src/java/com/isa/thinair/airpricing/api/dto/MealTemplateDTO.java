package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Date;

public class MealTemplateDTO implements Serializable {

	/**
	 * @author subash
	 */
	private static final long serialVersionUID = 1L;

	private Integer templateId;
	private String templateCode;
	private String description;
	private String status;
	private String mealCharges;
	private String createdBy;
	private Date createdDate;
	private long version;
	private String localCurrCode;
	private String multiRestrictions;

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMealCharges() {
		return mealCharges;
	}

	public void setMealCharges(String mealCharges) {
		this.mealCharges = mealCharges;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getLocalCurrCode() {
		return localCurrCode;
	}

	public void setLocalCurrCode(String localCurrCode) {
		this.localCurrCode = localCurrCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the multiRestrictions
	 */
	public String getMultiRestrictions() {
		return multiRestrictions;
	}

	/**
	 * @param multiRestrictions the multiRestrictions to set
	 */
	public void setMultiRestrictions(String multiRestrictions) {
		this.multiRestrictions = multiRestrictions;
	}
	
	
}
