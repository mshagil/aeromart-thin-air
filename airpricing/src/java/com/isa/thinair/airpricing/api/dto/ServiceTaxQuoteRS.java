package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;

public class ServiceTaxQuoteRS implements Serializable {
	
	private static final long serialVersionUID = -872792189195676821L;
	
	private String serviceTaxDepositeStateCode;
	
	private String serviceTaxDepositeCountryCode;
	
	public String getServiceTaxDepositeStateCode() {
		return serviceTaxDepositeStateCode;
	}

	public String getServiceTaxDepositeCountryCode() {
		return serviceTaxDepositeCountryCode;
	}

	public void setServiceTaxDepositeStateCode(String serviceTaxDepositeStateCode) {
		this.serviceTaxDepositeStateCode = serviceTaxDepositeStateCode;
	}

	public void setServiceTaxDepositeCountryCode(String serviceTaxDepositeCountryCode) {
		this.serviceTaxDepositeCountryCode = serviceTaxDepositeCountryCode;
	}
}
