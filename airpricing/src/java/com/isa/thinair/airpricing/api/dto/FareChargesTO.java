package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;

/**
 * Transfer Object to hold fare modification / cancellation related attributes
 * 
 * @author dumindaG
 */
public class FareChargesTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String modificationChargeType;

	private String cancellationChargeType;

	private BigDecimal minModificationAmount;

	private BigDecimal maximumModificationAmount;

	private BigDecimal minCancellationAmount;

	private BigDecimal maximumCancellationAmount;

	private String nameChangeChargeType;

	private BigDecimal maxNameChangeChargeAmount;

	private BigDecimal minNameChangeChargeAmount;

	public FareChargesTO(String modificationChargeType, String cancellationChargeType, String nameChangeChargeType,
			BigDecimal minModificationAmount, BigDecimal maximumModificationAmount, BigDecimal minCancellationAmount,
			BigDecimal maximumCancellationAmount, BigDecimal maxNameChangeChargeAmount, BigDecimal minNameChangeChargeAmount) {
		this.modificationChargeType = modificationChargeType;
		this.cancellationChargeType = cancellationChargeType;
		this.nameChangeChargeType = nameChangeChargeType;
		this.minModificationAmount = minModificationAmount;
		this.maximumModificationAmount = maximumModificationAmount;
		this.minCancellationAmount = minCancellationAmount;
		this.maximumCancellationAmount = maximumCancellationAmount;
		this.minNameChangeChargeAmount = minNameChangeChargeAmount;
		this.maxNameChangeChargeAmount = maxNameChangeChargeAmount;

	}

	public FareChargesTO() {

	}

	public boolean isPercentageTypeCharge(boolean isCancelation) {
		if (isCancelation) {
			return !AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(cancellationChargeType);
		}
		return !AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(modificationChargeType);

	}

	/**
	 * @return the modificationChargeType
	 */
	public String getModificationChargeType() {
		return modificationChargeType;
	}

	/**
	 * @param modificationChargeType
	 *            the modificationChargeType to set
	 */
	public void setModificationChargeType(String modificationChargeType) {
		this.modificationChargeType = modificationChargeType;
	}

	/**
	 * @return the cancellationChargeType
	 */
	public String getCancellationChargeType() {
		return cancellationChargeType;
	}

	/**
	 * @param cancellationChargeType
	 *            the cancellationChargeType to set
	 */
	public void setCancellationChargeType(String cancellationChargeType) {
		this.cancellationChargeType = cancellationChargeType;
	}

	/**
	 * @return the minModificationAmount
	 */
	public BigDecimal getMinModificationAmount() {
		if (minModificationAmount == null)
			return new BigDecimal(0);
		return minModificationAmount;
	}

	/**
	 * @return the maximumModificationAmount
	 */
	public BigDecimal getMaximumModificationAmount() {
		if (maximumModificationAmount == null)
			return new BigDecimal(0);
		return maximumModificationAmount;
	}

	/**
	 * @return the minCancellationAmount
	 */
	public BigDecimal getMinCancellationAmount() {
		if (minCancellationAmount == null)
			return new BigDecimal(0);
		return minCancellationAmount;
	}

	/**
	 * @return the maximumCancellationAmount
	 */
	public BigDecimal getMaximumCancellationAmount() {
		if (maximumCancellationAmount == null)
			return new BigDecimal(0);
		return maximumCancellationAmount;
	}

	/**
	 * @param minModificationAmount
	 *            the minModificationAmount to set
	 */
	public void setMinModificationAmount(BigDecimal minModificationAmount) {
		this.minModificationAmount = minModificationAmount;
	}

	/**
	 * @param maximumModificationAmount
	 *            the maximumModificationAmount to set
	 */
	public void setMaximumModificationAmount(BigDecimal maximumModificationAmount) {
		this.maximumModificationAmount = maximumModificationAmount;
	}

	/**
	 * @param minCancellationAmount
	 *            the minCancellationAmount to set
	 */
	public void setMinCancellationAmount(BigDecimal minCancellationAmount) {
		this.minCancellationAmount = minCancellationAmount;
	}

	/**
	 * @param maximumCancellationAmount
	 *            the maximumCancellationAmount to set
	 */
	public void setMaximumCancellationAmount(BigDecimal maximumCancellationAmount) {
		this.maximumCancellationAmount = maximumCancellationAmount;
	}

	public String getNameChangeChargeType() {
		return nameChangeChargeType;
	}

	public void setNameChangeChargeType(String nameChangeChargeType) {
		this.nameChangeChargeType = nameChangeChargeType;
	}

	public BigDecimal getMaxNameChangeChargeAmount() {
		return maxNameChangeChargeAmount;
	}

	public void setMaxNameChangeChargeAmount(BigDecimal maxNameChangeChargeAmount) {
		this.maxNameChangeChargeAmount = maxNameChangeChargeAmount;
	}

	public BigDecimal getMinNameChangeChargeAmount() {
		return minNameChangeChargeAmount;
	}

	public void setMinNameChangeChargeAmount(BigDecimal minNameChangeChargeAmount) {
		this.minNameChangeChargeAmount = minNameChangeChargeAmount;
	}

}
