package com.isa.thinair.airpricing.api.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author kasun

 * @hibernate.class table = "BG_T_OND_BAGG_CHARGE_TEMPLATE"
 */
public class ONDBaggageChargeTemplate extends Persistent {

	private static final long serialVersionUID = 1L;

	private Integer ondBaggageChargeTemplateId;

	private String ondCode;

	private ONDBaggageTemplate ondBaggageTemplate;

	/**
	 * @hibernate.id column = "OND_BAGG_CHARGE_TEMPLATE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "BG_S_OND_BAGG_CHARGE_TEMPLATE"
	 * @return the ondBaggageChargeTemplateId
	 */
	public Integer getOndBaggageChargeTemplateId() {
		return ondBaggageChargeTemplateId;
	}

	/**
	 * @param ondBaggageChargeTemplateId
	 *            the ondBaggageChargeTemplateId to set
	 */
	public void setOndBaggageChargeTemplateId(Integer ondBaggageChargeTemplateId) {
		this.ondBaggageChargeTemplateId = ondBaggageChargeTemplateId;
	}

	/**
	 * 
	 * 
	 * @hibernate.many-to-one column="OND_CHARGE_TEMPLATE_ID"
	 *                        class="com.isa.thinair.airpricing.api.model.ONDBaggageTemplate" lazy="false"
	 * @return the ondBaggageTemplate
	 */
	public ONDBaggageTemplate getOndBaggageTemplate() {
		return ondBaggageTemplate;
	}

	/**
	 * @param ondBaggageTemplate
	 *            the ondBaggageTemplate to set
	 */
	public void setOndBaggageTemplate(ONDBaggageTemplate ondBaggageTemplate) {
		this.ondBaggageTemplate = ondBaggageTemplate;
	}

	/**
	 * @param ondCode the ondCode to set
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return the ondCode
	 * 
	 * @hibernate.property column = "OND_BAGGAGE_OND_CODE"
	 */
	 
	public String getOndCode() {
		return ondCode;
	}


	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ONDBaggageChargeTemplate that = (ONDBaggageChargeTemplate) o;
		if (ondCode != null ? !ondCode.equals(that.ondCode) : that.ondCode != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(getOndCode()).toHashCode();
	}
}
