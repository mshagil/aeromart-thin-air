package com.isa.thinair.airpricing.api.criteria;

import java.io.Serializable;
import java.util.Date;

public class FlexiRuleSearchCriteria implements Serializable {

	private static final long serialVersionUID = -8518053966586120854L;
	private String flexiCode;
	private String fareRuleCode;
	private Date depDateFrom;
	private Date depDateTo;
	private Date salesDateFrom;
	private Date salesDateTo;
	private String status;

	/**
	 * @return Returns the flexiCode.
	 */
	public String getFlexiCode() {
		return flexiCode;
	}

	/**
	 * @param flexiCode
	 *            The flexiCode to set.
	 */
	public void setFlexiCode(String flexiCode) {
		this.flexiCode = flexiCode;
	}

	/**
	 * @return Returns the fareRuleCode.
	 */
	public String getFareRuleCode() {
		return fareRuleCode;
	}

	/**
	 * @param fareRuleCode
	 *            The fareRuleCode to set.
	 */
	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	/**
	 * @return Returns the depDateFrom.
	 */
	public Date getDepDateFrom() {
		return depDateFrom;
	}

	/**
	 * @param depDateFrom
	 *            The depDateFrom to set.
	 */
	public void setDepDateFrom(Date depDateFrom) {
		this.depDateFrom = depDateFrom;
	}

	/**
	 * @return Returns the depDateTo.
	 */
	public Date getDepDateTo() {
		return depDateTo;
	}

	/**
	 * @param depDateTo
	 *            The depDateTo to set.
	 */
	public void setDepDateTo(Date depDateTo) {
		this.depDateTo = depDateTo;
	}

	/**
	 * @return Returns the salesDateFrom.
	 */
	public Date getSalesDateFrom() {
		return salesDateFrom;
	}

	/**
	 * @param salesDateFrom
	 *            The salesDateFrom to set.
	 */
	public void setSalesDateFrom(Date salesDateFrom) {
		this.salesDateFrom = salesDateFrom;
	}

	/**
	 * @return Returns the salesDateTo.
	 */
	public Date getSalesDateTo() {
		return salesDateTo;
	}

	/**
	 * @param salesDateTo
	 *            The salesDateTo to set.
	 */
	public void setSalesDateTo(Date salesDateTo) {
		this.salesDateTo = salesDateTo;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}