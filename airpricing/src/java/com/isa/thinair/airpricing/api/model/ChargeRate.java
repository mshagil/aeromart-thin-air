/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:13:19
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateJourneyType;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_CHARGE_RATE"
 * 
 *                  ChargeRate is the entity class to repesent a ChargeRate model
 * 
 */
public class ChargeRate extends Tracking {

	// public static final String VALUE_PERCENTAGE_FLAG_V = "V";

	// public static final String VALUE_PERCENTAGE_FLAG_P = "P";

	private static final long serialVersionUID = -186175187654784925L;

	public static final String CHARGE_BASIS_V = "V";

	public static final String CHARGE_BASIS_PF = "PF";

	public static final String CHARGE_BASIS_PTF = "PTF";

	public static final String CHARGE_BASIS_PS = "PS";
	
	/** Percentage of fare + surcharges */
	public static final String CHARGE_BASIS_PFS = "PFS";
	
	/** Percentage of total fare + total surcharges */
	public static final String CHARGE_BASIS_PTFS = "PTFS";
	
	/** Percentage of total fare + total surcharges + Taxes */
	public static final String CHARGE_BASIS_PTFST = "PTFST";

	public static final String Status_Active = "ACT";

	public static final String Status_InActive = "INA";

	private String chargeCode;

	private int chargeRateCode;

	private Date chargeEffectiveFromDate;

	private Date chargeEffectiveToDate;

	private String valuePercentageFlag;

	private Double chargeValuePercentage;

	private Double valueInLocalCurrency;

	private String status;

	private String currencyCode;

	private BigDecimal chargeValueMin;

	private BigDecimal chargeValueMax;

	private String chargeBasis;

	private int operationType = ChargeRateOperationType.ANY;

	private int journeyType = ChargeRateJourneyType.ANY;

	private String cabinClassCode = null;

	private BigDecimal breakPoint;

	private BigDecimal boundryValue;

	private Date salesFromDate;

	private Date saleslToDate;

	/**
	 * returns the chargeCode
	 * 
	 * @return Returns the chargeCode.
	 * 
	 * @hibernate.property column = "CHARGE_CODE"
	 * 
	 */
	public String getChargeCode() {

		return chargeCode;
	}

	/**
	 * sets the chargeCode
	 * 
	 * @param chargeCode
	 *            The chargeCode to set.
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * returns the chargeEffectiveFromDate
	 * 
	 * @return Returns the chargeEffectiveFromDate.
	 * @hibernate.id column = "CHARGE_RATE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CHARGE_RATE"
	 */
	public int getChargeRateCode() {
		return chargeRateCode;
	}

	/**
	 * sets the chargeEffectiveFromDate
	 * 
	 * @param chargeEffectiveFromDate
	 *            The chargeEffectiveFromDate to set.
	 */
	public void setChargeRateCode(int chargeRateCode) {
		this.chargeRateCode = chargeRateCode;
	}

	/**
	 * returns the chargeEffectiveFromDate
	 * 
	 * @return Returns the chargeEffectiveFromDate.
	 * 
	 * @hibernate.property column = "CHARGE_EFFECTIVE_FROM_DATE"
	 */
	public Date getChargeEffectiveFromDate() {
		return chargeEffectiveFromDate;
	}

	/**
	 * sets the chargeEffectiveFromDate
	 * 
	 * @param chargeEffectiveFromDate
	 *            The chargeEffectiveFromDate to set.
	 */
	public void setChargeEffectiveFromDate(Date chargeEffectiveFromDate) {
		this.chargeEffectiveFromDate = chargeEffectiveFromDate;
	}

	/**
	 * returns the chargeEffectiveToDate
	 * 
	 * @return Returns the chargeEffectiveToDate.
	 * 
	 * @hibernate.property column = "CHARGE_EFFECTIVE_TO_DATE"
	 */
	public Date getChargeEffectiveToDate() {
		return chargeEffectiveToDate;
	}

	/**
	 * sets the chargeEffectiveToDate
	 * 
	 * @param chargeEffectiveToDate
	 *            The chargeEffectiveToDate to set.
	 */
	public void setChargeEffectiveToDate(Date chargeEffectiveToDate) {
		this.chargeEffectiveToDate = chargeEffectiveToDate;
	}

	/**
	 * returns the valuePercentageFlag
	 * 
	 * @return Returns the valuePercentageFlag.
	 * 
	 * @hibernate.property column = "VALUE_PERCENTAGE_FLAG"
	 */
	public String getValuePercentageFlag() {
		return valuePercentageFlag;
	}

	/**
	 * sets the valuePercentageFlag
	 * 
	 * @param valuePercentageFlag
	 *            The valuePercentageFlag to set.
	 */
	public void setValuePercentageFlag(String valuePercentageFlag) {
		this.valuePercentageFlag = valuePercentageFlag;
	}

	/**
	 * returns the chargeValuePercentage
	 * 
	 * @return Returns the chargeValuePercentage.
	 * 
	 * @hibernate.property column = "CHARGE_VALUE_PERCENTAGE"
	 */
	public Double getChargeValuePercentage() {
		return chargeValuePercentage;
	}

	/**
	 * sets the chargeValuePercentage
	 * 
	 * @param chargeValuePercentage
	 *            The chargeValuePercentage to set.
	 */
	public void setChargeValuePercentage(Double chargeValuePercentage) {
		this.chargeValuePercentage = chargeValuePercentage;
	}

	/**
	 * returns the valueInLocalCurrency
	 * 
	 * @return Returns the valueInLocalCurrency.
	 * 
	 * @hibernate.property column = "VALUE_IN_LOCAL_CURRENCY"
	 */
	public Double getValueInLocalCurrency() {
		return valueInLocalCurrency;
	}

	/**
	 * sets the valueInLocalCurrency
	 * 
	 * @param valueInLocalCurrency
	 *            The valueInLocalCurrency to set.
	 */
	public void setValueInLocalCurrency(Double valueInLocalCurrency) {
		this.valueInLocalCurrency = valueInLocalCurrency;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the currencyCode.
	 * @hibernate.property column = "CURRENCY_CODE"
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            The currencyCode to set.
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return Returns the chargeValueMin.
	 * @hibernate.property column = "CHARGE_VALUE_MIN"
	 */
	public BigDecimal getChargeValueMin() {
		return chargeValueMin;
	}

	/**
	 * @param chargeValMin
	 *            The chargeValueMin to set.
	 */
	public void setChargeValueMin(BigDecimal chargeValueMin) {
		this.chargeValueMin = chargeValueMin;
	}

	/**
	 * @return Returns the chargeValueMax.
	 * @hibernate.property column = "CHARGE_VALUE_MAX"
	 */
	public BigDecimal getChargeValueMax() {
		return chargeValueMax;
	}

	/**
	 * @param chargeValMax
	 *            The chargeValueMax to set.
	 */
	public void setChargeValueMax(BigDecimal chargeValueMax) {
		this.chargeValueMax = chargeValueMax;
	}

	/**
	 * @return Returns the chargeBasis.
	 * @hibernate.property column = "CHARGE_BASIS"
	 */
	public String getChargeBasis() {
		return chargeBasis;
	}

	/**
	 * @param chargeBasis
	 *            The chargeBasis to set.
	 */
	public void setChargeBasis(String chargeBasis) {
		this.chargeBasis = chargeBasis;
	}

	/**
	 * @return appliesTo.
	 * @hibernate.property column ="APPLIES_TO"
	 */
	public int getOperationType() {
		return operationType;
	}

	/**
	 * @param appliesTo
	 */
	public void setOperationType(int operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return
	 * @hibernate.property column ="RATE_JOURNEY_TYPE"
	 */
	public int getJourneyType() {
		return journeyType;
	}

	/**
	 * @param journeyType
	 */
	public void setJourneyType(int journeyType) {
		this.journeyType = journeyType;
	}

	/**
	 * @return
	 * @hibernate.property column ="CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return : The break point.
	 * @hibernate.property column ="BREAK_POINT"
	 */
	public BigDecimal getBreakPoint() {
		return breakPoint;
	}

	/**
	 * @param breakPoint
	 *            : The break point to set.
	 */
	public void setBreakPoint(BigDecimal breakPoint) {
		this.breakPoint = breakPoint;
	}

	/**
	 * @return : The boundry value.
	 * @hibernate.property column ="BOUNDRY_VALUE"
	 */
	public BigDecimal getBoundryValue() {
		return boundryValue;
	}

	/**
	 * @param boundryValue
	 *            : The boundry value to set.
	 */
	public void setBoundryValue(BigDecimal boundryValue) {
		this.boundryValue = boundryValue;
	}

	/**
	 * @return
	 * @hibernate.property column ="SALES_VALID_FROM"
	 */
	public Date getSalesFromDate() {
		return salesFromDate;
	}

	public void setSalesFromDate(Date salesFromDate) {
		this.salesFromDate = salesFromDate;
	}

	/**
	 * @return
	 * @hibernate.property column ="SALES_VALID_TO"
	 */
	public Date getSaleslToDate() {
		return saleslToDate;
	}

	public void setSaleslToDate(Date saleslToDate) {
		this.saleslToDate = saleslToDate;
	}
}
