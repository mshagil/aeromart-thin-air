package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class CnxModPenServiceTaxDTO implements Serializable {

	private static final long serialVersionUID = -872732184195676821L;

	private List<ServiceTaxDTO> cnxModPenServiceTaxes;

	private BigDecimal effectiveCnxModPenChargeAmount;

	public List<ServiceTaxDTO> getCnxModPenServiceTaxes() {
		return cnxModPenServiceTaxes;
	}

	public BigDecimal getEffectiveCnxModPenChargeAmount() {
		return effectiveCnxModPenChargeAmount;
	}

	public void setCnxModPenServiceTaxes(List<ServiceTaxDTO> cnxModPenServiceTaxes) {
		this.cnxModPenServiceTaxes = cnxModPenServiceTaxes;
	}

	public void setEffectiveCnxModPenChargeAmount(BigDecimal effectiveCnxModPenChargeAmount) {
		this.effectiveCnxModPenChargeAmount = effectiveCnxModPenChargeAmount;
	}

	public BigDecimal getTotalTaxAmount() {
		BigDecimal totalTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (ServiceTaxDTO tax : getCnxModPenServiceTaxes()) {
			totalTaxAmount = AccelAeroCalculator.add(totalTaxAmount, tax.getAmount());
		}
		return totalTaxAmount;
	}
}
