package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;

public class FlexiRuleDTO implements Serializable {

	private static final long serialVersionUID = -1592589255809832989L;

	public static final String VALUE_PERCENTAGE_FLAG_P = "P";

	private int fareRuleId;

	private int flexiRuleId;

	private String flexiCode;

	private String description;

	private String journeyType;

	private String chargeCode;

	private int flexiRuleRateId;

	private HashMap<String,Double> effectiveChargeAmountsMap;

	private HashMap<String, Double> chargesMap;

	private HashMap<String, String> chargeTypesMap;

	private HashMap<String, String> percentageBasisMap;

	private Collection<FlexiRuleFlexibilityDTO> flexibilitiesList;

	private String ruleComments;

	public FlexiRuleDTO() {

	}

	public int getFlexiRuleId() {
		return this.flexiRuleId;
	}

	public void setFlexiRuleId(int flexiRuleId) {
		this.flexiRuleId = flexiRuleId;
	}

	public int getFareRuleId() {
		return this.fareRuleId;
	}

	public void setFareRuleId(int fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	public String getFlexiCode() {
		return this.flexiCode;
	}

	public void setFlexiCode(String flexiCode) {
		this.flexiCode = flexiCode;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setJourneyType(String journeyType) {
		this.journeyType = journeyType;
	}

	public String getJourneyType() {
		return this.journeyType;
	}

	public boolean isForReturnJourney() {
		return "RT".equals(journeyType);
	}

	public String getChargeCode() {
		return this.chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public int getFlexiRuleRateId() {
		return this.flexiRuleRateId;
	}

	public void setFlexiRuleRateId(int flexiRuleRateId) {
		this.flexiRuleRateId = flexiRuleRateId;
	}

	public String getRuleComments() {
		return ruleComments;
	}

	public void setRuleComments(String ruleComments) {
		this.ruleComments = ruleComments;
	}

	public HashMap<String,Double> getCharges() {
		return this.chargesMap;
	}

	public Double getCharge(String paxType) {
		if (this.chargesMap != null)
			return this.chargesMap.get(paxType);
		else
			return new Double(0);
	}

	public void addCharge(String paxType, Double charge) {
		if (this.chargesMap == null) {
			this.chargesMap = new HashMap<String, Double>();
		}
		this.chargesMap.put(paxType, charge);
	}

	public HashMap<String, String> getChargeTypes() {
		return this.chargeTypesMap;
	}

	public String getChargeType(String paxType) {
		if (this.chargeTypesMap != null)
			return this.chargeTypesMap.get(paxType);
		else
			return null;
	}

	public void addChargeType(String paxType, String chargeType) {
		if (this.chargeTypesMap == null) {
			this.chargeTypesMap = new HashMap<String, String>();
		}
		this.chargeTypesMap.put(paxType, chargeType);
	}

	public HashMap<String, String> getPercentageBasis() {
		return this.percentageBasisMap;
	}

	public String getPercentageBasis(String paxType) {
		if (this.percentageBasisMap != null)
			return this.percentageBasisMap.get(paxType);
		else
			return null;
	}

	public void addPercentageBasis(String paxType, String percentageBasis) {
		if (this.percentageBasisMap == null) {
			this.percentageBasisMap = new HashMap<String, String>();
		}
		this.percentageBasisMap.put(paxType, percentageBasis);
	}

	public Collection<FlexiRuleFlexibilityDTO> getAvailableFlexibilities() {
		return this.flexibilitiesList;
	}

	public void setAvailableFlexibilities(Collection<FlexiRuleFlexibilityDTO> flexibilitiesList) {
		this.flexibilitiesList = flexibilitiesList;
	}

	public void addToFlexibilitiesList(FlexiRuleFlexibilityDTO flexiRuleFlexibilityDTO) {
		if (this.flexibilitiesList == null) {
			this.flexibilitiesList = new ArrayList<FlexiRuleFlexibilityDTO>();
		}
		this.flexibilitiesList.add(flexiRuleFlexibilityDTO);
	}

	public void setEffectiveChargeAmount(String paxType, Double effectiveChargAmount) {
		if (this.effectiveChargeAmountsMap == null) {
			this.effectiveChargeAmountsMap = new HashMap<String,Double>();
		}
		this.effectiveChargeAmountsMap.put(paxType, effectiveChargAmount);
	}

	public Double getEffectiveChargeAmount(String paxType) {
		if (this.effectiveChargeAmountsMap == null || !this.effectiveChargeAmountsMap.containsKey(paxType)) {
			throw new ModuleRuntimeException("module.runtime.error");
		}
		return (Double) this.effectiveChargeAmountsMap.get(paxType);
	}

	public HashMap<String,Double> getEffectiveChargeAmountMap() {
		return this.effectiveChargeAmountsMap;
	}

	public FlexiRuleDTO clone() {
		FlexiRuleDTO clone = new FlexiRuleDTO();
		clone.setFareRuleId(this.getFareRuleId());
		clone.setFlexiRuleId(this.getFlexiRuleId());
		clone.setFlexiCode(this.getFlexiCode());
		clone.setFlexiRuleRateId(this.getFlexiRuleRateId());
		clone.setChargeCode(this.getChargeCode());
		clone.setDescription(this.getDescription());
		clone.setJourneyType(this.getJourneyType());
		clone.setRuleComments(this.getRuleComments());
		
		
		for (FlexiRuleFlexibilityDTO flexiRuleFlexibilityDTO : this.getAvailableFlexibilities()) {
			clone.addToFlexibilitiesList(flexiRuleFlexibilityDTO.clone());
		}
		
		if (this.getCharges() != null) {
			for (Entry<String, Double> entry : this.getCharges().entrySet()) {
				clone.addCharge(entry.getKey(), entry.getValue());
			}
		}

		if (this.getChargeTypes() != null) {
			for (Entry<String, String> entry : this.getChargeTypes().entrySet()) {
				clone.addChargeType(entry.getKey(), entry.getValue());
			}
		}

		if (this.getPercentageBasis() != null) {
			for (Entry<String, String> entry : this.getPercentageBasis().entrySet()) {
				clone.addPercentageBasis(entry.getKey(), entry.getValue());
			}
		}

		if (this.getPercentageBasis() != null) {
			for (Entry<String, String> entry : this.getPercentageBasis().entrySet()) {
				clone.addPercentageBasis(entry.getKey(), entry.getValue());
			}
		}

		if (this.getEffectiveChargeAmountMap() != null) {
			for (Entry<String, Double> entry : this.getEffectiveChargeAmountMap().entrySet()) {
				clone.setEffectiveChargeAmount(entry.getKey(), entry.getValue());
			}
		}
		
		return clone;
	}

	@Override
	public String toString() {
		return "FlexiRuleDTO [chargeCode=" + chargeCode + ", fareRuleId=" + fareRuleId + ", flexiCode=" + flexiCode
				+ ", flexiRuleId=" + flexiRuleId + ", flexiRuleRateId=" + flexiRuleRateId + "]";
	}

}
