/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this chargeCategoryCode in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:48
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : chargeCategoryCode Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_CHARGE_CATEGORY"
 * 
 *                  ChargeCategory is the entity class to repesent a ChargeCategory model
 * 
 */
public class ChargeCategory extends Persistent {

	private static final long serialVersionUID = -4028044806174493304L;

	private String chargeCategoryCode;

	private String chargeCategoryDescription;

	/**
	 * @return Returns the chargeCategoryCode.
	 * @hibernate.id column = "CHARGE_CATEGORY_CODE" generator-class = "assigned"
	 * 
	 */
	public String getChargeCategoryCode() {
		return chargeCategoryCode;
	}

	public void setChargeCategoryCode(String chargeCategoryCode) {
		this.chargeCategoryCode = chargeCategoryCode;
	}

	/**
	 * returns the chargeCategoryDesc
	 * 
	 * @return Returns the chargeCategoryDesc.
	 * 
	 * @hibernate.property column = "CHARGE_CATEGORY_DESC"
	 */
	public String getDescription() {
		return chargeCategoryDescription;
	}

	/**
	 * sets the chargeCategoryDesc
	 * 
	 * @param chargeCategoryDesc
	 *            The chargeCategoryDesc to set.
	 */
	public void setDescription(String chargeCategoryDescription) {
		this.chargeCategoryDescription = chargeCategoryDescription;
	}

}
