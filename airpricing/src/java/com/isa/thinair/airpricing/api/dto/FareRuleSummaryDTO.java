package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;

/** Transfer Object to store the master fare rule **/
/** This object is accessed through the AgentFareRulesDTO **/
public class FareRuleSummaryDTO implements Serializable {

	private static final long serialVersionUID = 242776099336838077L;
	private String fareRuleCode;
	private Integer fareRuleId;
	private String description;
	private String fareBasisCode;
	private boolean linkedToNonExpiredFare;
	private String status;

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the fareBasisCode.
	 */
	public String getFareBasisCode() {
		return fareBasisCode;
	}

	/**
	 * @param fareBasisCode
	 *            The fareBasisCode to set.
	 */
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	/**
	 * @return Returns the linkedToNonExpiredFare.
	 */
	public boolean isLinkedToNonExpiredFare() {
		return linkedToNonExpiredFare;
	}

	/**
	 * @param linkedToNonExpiredFare
	 *            The linkedToNonExpiredFare to set.
	 */
	public void setLinkedToNonExpiredFare(boolean linkedToNonExpiredFare) {
		this.linkedToNonExpiredFare = linkedToNonExpiredFare;
	}

	/**
	 * @return Returns the fareRuleCode.
	 */
	public String getFareRuleCode() {
		return fareRuleCode;
	}

	/**
	 * @param fareRuleCode
	 *            The fareRuleCode to set.
	 */
	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the fareRuleId.
	 */
	public Integer getFareRuleId() {
		return fareRuleId;
	}

	/**
	 * @param fareRuleId
	 *            The fareRuleId to set.
	 */
	public void setFareRuleId(Integer fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

}
