package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Collection;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

public class SSRChargeSearchDTO implements Serializable, Cloneable {
	private static final long serialVersionUID = -5009516706544851514L;

	private String fromAirpot;
	private String toAirpot;
	// XBE/IBE...
	private AppIndicatorEnum appIndicator;
	private Collection<FlightSegmentDTO> flightSegments;
	private int adultCount;
	private int childCount;
	private int infantCount;

	public String getFromAirpot() {
		return this.fromAirpot;
	}

	public void setFromAirpot(String fromAirpot) {
		this.fromAirpot = fromAirpot;
	}

	public String getToAirpot() {
		return this.toAirpot;
	}

	public void setToAirpot(String toAirpot) {
		this.toAirpot = toAirpot;
	}

	public AppIndicatorEnum getAppIndicator() {
		return this.appIndicator;
	}

	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	public Collection<FlightSegmentDTO> getFlightSegments() {
		return this.flightSegments;
	}

	public void setFlightSegments(Collection<FlightSegmentDTO> flightSegments) {
		this.flightSegments = flightSegments;
	}

	public int getAdultCount() {
		return this.adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return this.childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getInfantCount() {
		return this.infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

}
