package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * @hibernate.class table = "BG_T_OND_TEMPLATE_FLIGHT"
 */
public class BaggageTemplateFlight implements Serializable {

	private Integer id;
	private ONDBaggageTemplate baggageTemplate;
	private String flightNumber;

	/**
	 * @hibernate.id column = "OND_TEMPLATE_FLIGHT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "BG_S_OND_TEMPLATE_FLIGHT"
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @hibernate.many-to-one column="OND_CHARGE_TEMPLATE_ID" class="com.isa.thinair.airpricing.api.model.ONDBaggageTemplate"
	 */
	public ONDBaggageTemplate getBaggageTemplate() {
		return baggageTemplate;
	}

	public void setBaggageTemplate(ONDBaggageTemplate baggageTemplate) {
		this.baggageTemplate = baggageTemplate;
	}

	/**
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BaggageTemplateFlight flight = (BaggageTemplateFlight) o;

		if (flightNumber != null ? !flightNumber.equals(flight.flightNumber) : flight.flightNumber != null)
			return false;

		return true;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getFlightNumber()).toHashCode();
	}
}
