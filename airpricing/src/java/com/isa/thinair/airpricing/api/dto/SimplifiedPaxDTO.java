package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;

import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class SimplifiedPaxDTO implements Serializable {
	
	private static final long serialVersionUID = -872822184195676821L;
	
	private String paxType = PaxTypeTO.ADULT;
	
	private Integer paxSequence;
	
	private boolean totalAmountIncludingServiceTax;

	public String getPaxType() {
		return paxType;
	}

	public Integer getPaxSequence() {
		return paxSequence;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	public boolean isTotalAmountIncludingServiceTax() {
		return totalAmountIncludingServiceTax;
	}

	public void setTotalAmountIncludingServiceTax(boolean totalAmountIncludingServiceTax) {
		this.totalAmountIncludingServiceTax = totalAmountIncludingServiceTax;
	}
}
