/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:13:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_CHARGE_SYSTEM_DEFAULT"
 * 
 *                  ChargeSystemDefault is the entity class to repesent a ChargeSystemDefault model
 * 
 */
public class DefaultCharge extends Tracking {

	private static final long serialVersionUID = -1919467624112240075L;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	private String sysChargeCode;

	private String description;

	private int amount;

	private String status;

	/**
	 * returns the sysChargeCode
	 * 
	 * @return Returns the sysChargeCode.
	 * 
	 * @hibernate.id column = "SYS_CHARGE_CODE" generator-class = "assigned"
	 */
	public String getSysChargeCode() {
		return sysChargeCode;
	}

	/**
	 * sets the sysChargeCode
	 * 
	 * @param sysChargeCode
	 *            The sysChargeCode to set.
	 */
	public void setSysChargeCode(String sysChargeCode) {
		this.sysChargeCode = sysChargeCode;
	}

	/**
	 * returns the description
	 * 
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets the description
	 * 
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * returns the amount
	 * 
	 * @return Returns the amount.
	 * 
	 * @hibernate.property column = "AMOUNT"
	 */
	public int getAmount() {
		return amount;
	}

	/**
	 * sets the amount
	 * 
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
