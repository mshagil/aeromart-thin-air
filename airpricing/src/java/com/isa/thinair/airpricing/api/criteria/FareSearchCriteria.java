/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:12
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.criteria;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class FareSearchCriteria implements Serializable {

	private static final long serialVersionUID = -8330543511314045607L;
	private String originAirportCode;
	private String destinationAirportCode;
	private List<String> viaAirportCodes;
	private String fareBasisCode;
	private String cabinClassCode;
	private String logicalCabinClass;
	private String status;
	private boolean showAllONDCombinations;
	private Date fromDate;
	private Date toDate;
	private boolean searchForSalesPeriod;
	private String bookingClassCode;
	private String fareRuleCode;
	private boolean isExactCombination;
	private String sortingField;
	private String sortingOder;

	/**
	 * @return Returns the destinationAirportCode.
	 */
	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	/**
	 * @param destinationAirportCode
	 *            The destinationAirportCode to set.
	 */
	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	/**
	 * @return Returns the fareBasisCode.
	 */
	public String getFareBasisCode() {
		return fareBasisCode;
	}

	/**
	 * @param fareBasisCode
	 *            The fareBasisCode to set.
	 */
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	/**
	 * @return Returns the fromDate.
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate
	 *            The fromDate to set.
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return Returns the isExactCombination.
	 */
	public boolean isExactCombination() {
		return isExactCombination;
	}

	/**
	 * @param isExactCombination
	 *            The isExactCombination to set.
	 */
	public void setExactCombination(boolean isExactCombination) {
		this.isExactCombination = isExactCombination;
	}

	/**
	 * @return Returns the originAirportCode.
	 */
	public String getOriginAirportCode() {
		return originAirportCode;
	}

	/**
	 * @param originAirportCode
	 *            The originAirportCode to set.
	 */
	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

	/**
	 * @return Returns the showAllONDCombinations.
	 */
	public boolean isShowAllONDCombinations() {
		return showAllONDCombinations;
	}

	/**
	 * @param showAllONDCombinations
	 *            The showAllONDCombinations to set.
	 */
	public void setShowAllONDCombinations(boolean showAllONDCombinations) {
		this.showAllONDCombinations = showAllONDCombinations;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the toDate.
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate
	 *            The toDate to set.
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return Returns the viaAirportCodes.
	 */
	public List<String> getViaAirportCodes() {
		return viaAirportCodes;
	}

	/**
	 * @param viaAirportCodes
	 *            The viaAirportCodes to set.
	 */
	public void setViaAirportCodes(List<String> viaAirportCodes) {
		this.viaAirportCodes = viaAirportCodes;
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the logicalCabinClass
	 */
	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	/**
	 * @param logicalCabinClass
	 *            the logicalCabinClass to set
	 */
	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	/**
	 * @return Returns the bookingClassCode.
	 */
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	/**
	 * @param bookingClassCode
	 *            The bookingClassCode to set.
	 */
	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	/**
	 * @return the searchForSalesPeriod
	 */
	public boolean isSearchForSalesPeriod() {
		return searchForSalesPeriod;
	}

	/**
	 * @param searchForSalesPeriod
	 *            the searchForSalesPeriod to set
	 */
	public void setSearchForSalesPeriod(boolean searchForSalesPeriod) {
		this.searchForSalesPeriod = searchForSalesPeriod;
	}

	/**
	 * @return the fareRuleCode
	 */
	public String getFareRuleCode() {
		return fareRuleCode;
	}

	/**
	 * @param fareRuleCode
	 *            the fareRuleCode to set
	 */
	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	/**
	 * @return the sortingField
	 */
	public String getSortingField() {
		return sortingField;
	}

	/**
	 * @param sortingField
	 *            the sortingField to set
	 */
	public void setSortingField(String sortingField) {
		this.sortingField = sortingField;
	}

	/**
	 * @return the sortingOder
	 */
	public String getSortingOder() {
		return sortingOder;
	}

	/**
	 * @param sortingOder
	 *            the sortingOder to set
	 */
	public void setSortingOder(String sortingOder) {
		this.sortingOder = sortingOder;
	}

}
