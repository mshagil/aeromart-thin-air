package com.isa.thinair.airpricing.api.criteria;

import java.io.Serializable;

public class DefaultAnciTemplSearchCriteria implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7074570983476942625L;

	private String origin;
	
	private String depature;
	
	private String anciType;
	
	private String status;

	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @param origin the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return the depature
	 */
	public String getDepature() {
		return depature;
	}

	/**
	 * @param depature the depature to set
	 */
	public void setDepature(String depature) {
		this.depature = depature;
	}

	/**
	 * @return the anciType
	 */
	public String getAnciType() {
		return anciType;
	}

	/**
	 * @param anciType the anciType to set
	 */
	public void setAnciType(String anciType) {
		this.anciType = anciType;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	

}
