package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Date;

public class SimplifiedFlightSegmentDTO implements Serializable, Comparable<SimplifiedFlightSegmentDTO> {
	
	private static final long serialVersionUID = -872812184195676821L;
	
	private Date arrivalDateTimeZulu;
	
	private Date departureDateTimeZulu;
	
	private String flightRefNumber;
	
	private String logicalCabinClassCode;
	
	private Integer ondSequence;
	
	private String operatingAirline;
	
	private String segmentCode;
	
	private boolean returnFlag;
	
	private Integer segmentSequence;

	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	public String getFlightRefNumber() {
		return flightRefNumber;
	}

	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	public String getOperatingAirline() {
		return operatingAirline;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setArrivalDateTimeZulu(Date arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}

	public void setFlightRefNumber(String flightRefNumber) {
		this.flightRefNumber = flightRefNumber;
	}

	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	public void setOperatingAirline(String operatingAirline) {
		this.operatingAirline = operatingAirline;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}
	
	@Override
	public int compareTo(SimplifiedFlightSegmentDTO flightSegmentDTO) {
		int result = -2;// unknown
		if (departureDateTimeZulu != null && flightSegmentDTO.getDepartureDateTimeZulu() != null) {
			result = departureDateTimeZulu.compareTo(flightSegmentDTO.getDepartureDateTimeZulu());
		}
		return result;
	}

	public Integer getOndSequence() {
		return ondSequence;
	}

	public Integer getSegmentSequence() {
		return segmentSequence;
	}

	public void setOndSequence(Integer ondSequence) {
		this.ondSequence = ondSequence;
	}

	public void setSegmentSequence(Integer segmentSequence) {
		this.segmentSequence = segmentSequence;
	}
}
