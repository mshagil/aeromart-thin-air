/**
 * 
 */
package com.isa.thinair.airpricing.api.model;

import java.math.BigDecimal;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @hibernate.class table = "ML_T_MEAL_CHARGE" MealCharge is the entity class to repesent a MealCharge
 * 
 */
public class MealCharge extends Persistent {

	private static final long serialVersionUID = 5253015299702892762L;
	
	private Integer chargeId;

	private MealTemplate template;

	private Integer mealId;

	private BigDecimal amount;

	private String mealName;

	private Integer allocatedMeal;

	private String status;

	private String cabinClass;

	private String logicalCCCode;
	
	private BigDecimal localCurrencyAmount;
	
	private Integer popularity;

	public int hashCode() {
		return new HashCodeBuilder().append(getChargeId()).toHashCode();
	}

	/**
	 * @hibernate.id column = "MEAL_CHARGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "ML_S_MEAL_CHARGE"
	 * 
	 * @return Returns the chargeId.
	 */
	public Integer getChargeId() {
		return chargeId;
	}

	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}

	/**
	 * @return Returns the templateCode.
	 * @hibernate.many-to-one column = "MEAL_TEMPLATE_ID" class= "com.isa.thinair.airpricing.api.model.MealTemplate"
	 */
	public MealTemplate getTemplateId() {
		return template;
	}

	public void setTemplateId(MealTemplate template) {
		this.template = template;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "ALLOCATED_MEALS"
	 */
	public Integer getAllocatedMeal() {
		return allocatedMeal;
	}

	public void setAllocatedMeal(Integer allocatedMeal) {
		this.allocatedMeal = allocatedMeal;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "MEAL_ID"
	 */
	public Integer getMealId() {
		return mealId;
	}

	public void setMealId(Integer mealId) {
		this.mealId = mealId;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	/**
	 * @return the cabinClass
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/***
	 * 
	 * @return logicalCCCode
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}
	
	/**
	 * @return the localCurrencyAmount
	 * @hibernate.property column = "LOCAL_CURR_AMOUNT"
	 */
	public BigDecimal getLocalCurrencyAmount() {
		return localCurrencyAmount;
	}

	public void setLocalCurrencyAmount(BigDecimal localCurrencyAmount) {
		this.localCurrencyAmount = localCurrencyAmount;
	}

	/**
	 * @return the popularity
	 * @hibernate.property column = "POPULARITY"
	 */
	public Integer getPopularity() {
		return popularity;
	}

	public void setPopularity(Integer popularity) {
		this.popularity = popularity;
	}
	
	

}
