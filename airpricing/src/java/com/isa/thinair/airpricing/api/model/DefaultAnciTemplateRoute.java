package com.isa.thinair.airpricing.api.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @hibernate.class table = "T_DEFAULT_ANCI_TEMPLATE_ROUTE"
 * 
 */
public class DefaultAnciTemplateRoute extends Persistent {

	private static final long serialVersionUID = 56088868315860645L;

	private int defaultAnciTemplateRouteId;

	private DefaultAnciTemplate template;

	private String ondCode;

	public int hashCode() {
		return new HashCodeBuilder().append(getDefaultAnciTemplateRouteId()).toHashCode();
	}

	/**
	 * @return the defaultAnciTemplateRouteId
	 * @hibernate.id column = "DEFAULT_ANCI_TEMPLATE_ROUTE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_DEFAULT_ANCI_TEMPLATE_ROUTE"
	 */
	public int getDefaultAnciTemplateRouteId() {
		return defaultAnciTemplateRouteId;
	}

	/**
	 * @param defaultAnciTemplateRouteId
	 *            the defaultAnciTemplateRouteId to set
	 */
	public void setDefaultAnciTemplateRouteId(int defaultAnciTemplateRouteId) {
		this.defaultAnciTemplateRouteId = defaultAnciTemplateRouteId;
	}

	/**
	 * @return the defaultAnciTemplateId
	 * @hibernate.many-to-one column = "DEFAULT_ANCI_TEMPLATE_ID" class=
	 *                        "com.isa.thinair.airpricing.api.model.DefaultAnciTemplate"
	 */
	public DefaultAnciTemplate getTemplate() {
		return template;
	}

	/**
	 * @param template
	 *            the template to set
	 */
	public void setTemplate(DefaultAnciTemplate template) {
		this.template = template;
	}

	/**
	 * @return the ondCode
	 * @hibernate.property column = "OND_CODE"
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 *            the ondCode to set
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	@Override
	public boolean equals(Object o) {

		if (!(o instanceof DefaultAnciTemplateRoute)) {
			return false;
		}

		DefaultAnciTemplateRoute castObject = (DefaultAnciTemplateRoute) o;
		if (castObject.getDefaultAnciTemplateRouteId() == this.getDefaultAnciTemplateRouteId()
				&& castObject.getOndCode().equals(this.getOndCode())) {
			return true;
		} else {
			return false;
		}
	}

}
