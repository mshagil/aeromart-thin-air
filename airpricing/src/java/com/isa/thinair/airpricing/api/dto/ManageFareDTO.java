package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * DTO Used when Retrieving Fares
 * 
 * @author Byorn
 * 
 */
public class ManageFareDTO implements Serializable {

	private static final long serialVersionUID = 4391162856345030761L;
	private int fareId;
	private int fareRuleId;
	private String fareBasisCode;
	private Date validFrom;
	private Date validTo;
	private double fareAmount;
	private String bookingClass;
	private String classOfService;
	private String returnFlag;
	private String fixedAllocation;
	private String standardCode;
	private String ondCode;
	private Set<Integer> channelVisibilities;
	private String status;
	private String refundableFlag;
	private Date salesValidFrom;
	private Date salesValidTo;
	private Date returnValidFrom;
	private Date returnValidTo;
	private String masterFareStatus;
	private String masterFareRefCode;
	private Integer masterFareID;
	private Integer masterFarePercentage;
	private String modToSameFare;
	private String paxCategoryCode;
	private String currencyCode;

	private int version;

	/**
	 * @return Returns the bookingClass.
	 */
	public String getBookingClass() {
		return bookingClass;
	}

	/**
	 * @param bookingClass
	 *            The bookingClass to set.
	 */
	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	/**
	 * @return Returns the channelVisibilities.
	 */
	public Set<Integer> getChannelVisibilities() {
		return channelVisibilities;
	}

	/**
	 * @param channelVisibilities
	 *            The channelVisibilities to set.
	 */
	public void setChannelVisibilities(Set<Integer> channelVisibilities) {
		this.channelVisibilities = channelVisibilities;
	}

	/**
	 * @return Returns the classOfService.
	 */
	public String getClassOfService() {
		return classOfService;
	}

	/**
	 * @param classOfService
	 *            The classOfService to set.
	 */
	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}

	/**
	 * @return Returns the fareAmount.
	 */
	public double getFareAmount() {
		return fareAmount;
	}

	/**
	 * @param fareAmount
	 *            The fareAmount to set.
	 */
	public void setFareAmount(double fareAmount) {
		this.fareAmount = fareAmount;
	}

	/**
	 * @return Returns the fareBasisCode.
	 */
	public String getFareBasisCode() {
		return fareBasisCode;
	}

	/**
	 * @param fareBasisCode
	 *            The fareBasisCode to set.
	 */
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	/**
	 * @return Returns the fareId.
	 */
	public int getFareId() {
		return fareId;
	}

	/**
	 * @param fareId
	 *            The fareId to set.
	 */
	public void setFareId(int fareId) {
		this.fareId = fareId;
	}

	/**
	 * @return Returns the fareRuleId.
	 */
	public int getFareRuleId() {
		return fareRuleId;
	}

	/**
	 * @param fareRuleId
	 *            The fareRuleId to set.
	 */
	public void setFareRuleId(int fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	/**
	 * @return Returns the fixedAllocation.
	 */
	public String getFixedAllocation() {
		return fixedAllocation;
	}

	/**
	 * @param fixedAllocation
	 *            The fixedAllocation to set.
	 */
	public void setFixedAllocation(String fixedAllocation) {
		this.fixedAllocation = fixedAllocation;
	}

	/**
	 * @return Returns the ondCode.
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 *            The ondCode to set.
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return Returns the refundableFlag.
	 */
	public String getRefundableFlag() {
		return refundableFlag;
	}

	/**
	 * @param refundableFlag
	 *            The refundableFlag to set.
	 */
	public void setRefundableFlag(String refundableFlag) {
		this.refundableFlag = refundableFlag;
	}

	/**
	 * @return Returns the returnFlag.
	 */
	public String getReturnFlag() {
		return returnFlag;
	}

	/**
	 * @param returnFlag
	 *            The returnFlag to set.
	 */
	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the validFrom.
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            The validFrom to set.
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return Returns the validTo.
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            The validTo to set.
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return Returns the version.
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            The version to set.
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/**
	 * @return Returns the standardCode.
	 */
	public String getStandardCode() {
		return standardCode;
	}

	/**
	 * @param standardCode
	 *            The standardCode to set.
	 */
	public void setStandardCode(String standardCode) {
		this.standardCode = standardCode;
	}

	/**
	 * @return the salesValidFrom
	 */
	public Date getSalesValidFrom() {
		return salesValidFrom;
	}

	/**
	 * @param salesValidFrom
	 *            the salesValidFrom to set
	 */
	public void setSalesValidFrom(Date salesValidFrom) {
		this.salesValidFrom = salesValidFrom;
	}

	/**
	 * @return the salesValidTo
	 */
	public Date getSalesValidTo() {
		return salesValidTo;
	}

	/**
	 * @param salesValidTo
	 *            the salesValidTo to set
	 */
	public void setSalesValidTo(Date salesValidTo) {
		this.salesValidTo = salesValidTo;
	}

	/**
	 * @return Returns the returnValidFrom.
	 */
	public Date getReturnValidFrom() {
		return returnValidFrom;
	}

	/**
	 * @param returnValidFrom
	 *            The returnValidFrom to set.
	 */
	public void setReturnValidFrom(Date returnValidFrom) {
		this.returnValidFrom = returnValidFrom;
	}

	/**
	 * @return Returns the returnValidTo.
	 */
	public Date getReturnValidTo() {
		return returnValidTo;
	}

	/**
	 * @param validTo
	 *            The validTo to set.
	 */
	public void setReturnValidTo(Date returnValidTo) {
		this.returnValidTo = returnValidTo;
	}

	public String getMasterFareStatus() {
		return masterFareStatus;
	}

	public void setMasterFareStatus(String masterFareStatus) {
		this.masterFareStatus = masterFareStatus;
	}

	public String getMasterFareRefCode() {
		return masterFareRefCode;
	}

	public void setMasterFareRefCode(String masterFareRefCode) {
		this.masterFareRefCode = masterFareRefCode;
	}

	public Integer getMasterFareID() {
		return masterFareID;
	}

	public void setMasterFareID(Integer masterFareID) {
		this.masterFareID = masterFareID;
	}

	public Integer getMasterFarePercentage() {
		return masterFarePercentage;
	}

	public void setMasterFarePercentage(Integer masterFarePercentage) {
		this.masterFarePercentage = masterFarePercentage;
	}

	public String getModToSameFare() {
		return modToSameFare;
	}

	public void setModToSameFare(String modToSameFare) {
		this.modToSameFare = modToSameFare;
	}

	public String getPaxCategoryCode() {
		return paxCategoryCode;
	}

	public void setPaxCategoryCode(String paxCategoryCode) {
		this.paxCategoryCode = paxCategoryCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

}
