package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ServiceTaxQuoteForTicketingRevenueRS extends ServiceTaxQuoteRS implements Serializable {
	
	private static final long serialVersionUID = -872792184195676821L;
	
	private Map<String, List<ServiceTaxDTO>> paxTypeWiseServiceTaxes;
	
	private Map<Integer, List<ServiceTaxDTO>> paxWiseServiceTaxes;
	
	public Map<String, List<ServiceTaxDTO>> getPaxTypeWiseServiceTaxes() {
		return paxTypeWiseServiceTaxes;
	}

	public Map<Integer, List<ServiceTaxDTO>> getPaxWiseServiceTaxes() {
		return paxWiseServiceTaxes;
	}

	public void setPaxTypeWiseServiceTaxes(Map<String, List<ServiceTaxDTO>> paxTypeWiseServiceTaxes) {
		this.paxTypeWiseServiceTaxes = paxTypeWiseServiceTaxes;
	}

	public void setPaxWiseServiceTaxes(Map<Integer, List<ServiceTaxDTO>> paxWiseServiceTaxes) {
		this.paxWiseServiceTaxes = paxWiseServiceTaxes;
	}	
}
