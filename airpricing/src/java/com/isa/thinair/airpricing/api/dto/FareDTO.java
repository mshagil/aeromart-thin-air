package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Collection;

import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.Proration;

/**
 * DTO Used when Creating a New FAre
 * 
 * @author Byorn
 * 
 */
public class FareDTO implements Serializable {

	private static final long serialVersionUID = 1653428719778478146L;
	private String fareRuleCode;
	private Fare fare;
	private Collection<Proration> prorations;
	// TODO Remove
	private FareRule fareRule;

	private int fareRuleID;

	/**
	 * @return Returns the fare.
	 */
	public Fare getFare() {
		return fare;
	}

	/**
	 * @param fare
	 *            The fare to set.
	 */
	public void setFare(Fare fare) {
		this.fare = fare;
	}

	/**
	 * @return Returns the fareRuleCode.
	 */
	public String getFareRuleCode() {
		return fareRuleCode;
	}

	/**
	 * @param fareRuleCode
	 *            The fareRuleCode to set.
	 */
	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	/**
	 * @return Returns the fareRule.
	 */
	public FareRule getFareRule() {
		return fareRule;
	}

	/**
	 * @param fareRule
	 *            The fareRule to set.
	 */
	public void setFareRule(FareRule fareRule) {
		this.fareRule = fareRule;
	}

	public int getFareRuleID() {
		return fareRuleID;
	}

	public void setFareRuleID(int fareRuleID) {
		this.fareRuleID = fareRuleID;
	}

	public Collection<Proration> getProrations() {
		return prorations;
	}

	public void setProrations(Collection<Proration> prorations) {
		this.prorations = prorations;
	}

	
}