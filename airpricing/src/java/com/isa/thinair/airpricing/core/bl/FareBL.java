/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * Created on Jul 13, 2005 17:12:34
 *
 * $Id$
 *
 * ===============================================================================
 */

package com.isa.thinair.airpricing.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.dto.FareRuleFeeTO;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.OriginDestination;
import com.isa.thinair.airpricing.api.model.Proration;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.util.MasterAndLinkFareUtil;
import com.isa.thinair.airpricing.core.audit.AuditAirpricing;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareProrationDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.dao.ONDDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.airpricing.core.util.SplitFareBySalesUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.HubAirportTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * Class to implement the bussiness logic related to Fares
 * 
 * @author Byorn
 */
public class FareBL {

	private final Log log = LogFactory.getLog(getClass());

	private static final boolean UPDATE_MODE = true;

	private static final boolean SAVE_MODE = false;

	private static final String YES = "Y";

	private final FareRuleDAO fareruledao;

	private final FareRuleJdbcDAO fareRuleJdbcDao;

	private final FareDAO faredao;

	private final ONDDAO onddao;

	private final FareJdbcDAO fareJdbcDAO;

	private final FareProrationDAO faeProrationDao;

	/**
	 * CONSTRUCTOR for looking up releavent dao's
	 * 
	 */
	public FareBL() {
		fareRuleJdbcDao = (FareRuleJdbcDAO) AirpricingUtils.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.AGENT_FARE_RULE);
		fareruledao = (FareRuleDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_RULE_DAO);
		faredao = (FareDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_DAO);
		onddao = (ONDDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.OND_DAO);
		fareJdbcDAO = (FareJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.MANAGE_FARES);
		faeProrationDao = (FareProrationDAO) AirpricingUtils.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FARE_PRORATION_DAO);
	}

	/**
	 * 
	 * ChargeDAO
	 * 
	 * @return
	 */
	public ChargeDAO getChargeDAO() {
		return (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);
	}

	/**
	 * MEthod to GET a fare for the fare ID FareDTO
	 * 
	 * @param fareId
	 * @return
	 * @throws ModuleException
	 */
	public FareDTO getFare(int fareId) throws ModuleException {
		try {
			if (log.isDebugEnabled()) {
				log.debug("Getting a FARE and its Relavent FAre Rule for FareID:" + fareId);
			}

			FareDTO fareDTO = null;

			// get the fare and set it to the faredto

			Fare fare = faredao.getFare(fareId);
			if (fare != null) {
				fareDTO = new FareDTO();
				fareDTO.setFare(fare);

				// get the farerule and set it to the faredto.
				FareRule fareRule = fareruledao.getFareRule(fare.getFareRuleId());
				fareDTO.setFareRule(fareRule);

				fareDTO.setProrations(fare.getProrations());

				if (log.isDebugEnabled()) {
					log.debug("Getting Fare Rule Success" + BeanUtils.nullHandler(fareRule));
				}

				fareDTO.setFareRuleCode(fareRule.getFareRuleCode() != null ? fareRule.getFareRuleCode() : null);
			}
			return fareDTO;
		} catch (CommonsDataAccessException e) {
			log.error("ERROR getting Fare" + fareId);
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	public Collection<FareDTO> getFares(Collection<Integer> fareIds) throws ModuleException {
		try {
			if (log.isDebugEnabled()) {
				log.debug("Getting a FARE and its Relavent FAre Rule for FareIDs:" + fareIds);
			}

			Collection<FareDTO> fareDTOs = new ArrayList<FareDTO>();
			Map<Integer, Fare> faresMap = faredao.getFares(fareIds);
			Map<Integer, FareRule> fareRulesMap = faredao.getFareRules(fareIds);

			for (Integer fareId : fareIds) {
				FareDTO fareDTO = new FareDTO();
				fareDTO.setFare(faresMap.get(fareId));
				FareRule fareRule = fareRulesMap.get(fareId);
				fareDTO.setFareRule(fareRule);
				fareDTO.setFareRuleCode(fareRule.getFareRuleCode() != null ? fareRule.getFareRuleCode() : null);
				fareDTOs.add(fareDTO);
			}
			return fareDTOs;
		} catch (CommonsDataAccessException exception) {
			log.error("ERROR getting Fare" + fareIds, exception);
			throw new ModuleException(exception, exception.getExceptionCode(), "airpricing.desc");
		}
	}

	private static String isShowFareDefInDepAirPCurr() {
		String showFareDefByDepCountryCurrency = CommonsServices.getGlobalConfig().getBizParam(
				SystemParamKeys.SHOW_FARE_DEF_BY_DEP_COUNTRY_CURRENCY);
		return (showFareDefByDepCountryCurrency != null && showFareDefByDepCountryCurrency.equalsIgnoreCase("Y")) ? "Y" : "N";
	}

	/**
	 * BL method when saving a Fare void
	 * 
	 * @param fare
	 * @throws CommonsDataAccessException
	 *             , CommonsBusinessException
	 */
	public void saveFare(FareDTO fareDTO, UserPrincipal userPrincipal) throws ModuleException {

		String ondCode = fareDTO.getFare().getOndCode();

		log.debug("Saving a Fare");

		if (log.isDebugEnabled()) {
			log.debug("Saving a Fare:" + BeanUtils.nullHandler(fareDTO.getFare()) + "Fare Rule :"
					+ BeanUtils.nullHandler(fareDTO.getFareRule()) + "   fr code :"
					+ BeanUtils.nullHandler(fareDTO.getFareRuleCode()));
		}

		Fare fare = fareDTO.getFare();

		FareRule fareRuleInFareRuleTable = null;
		boolean retrunFlag = false;
		if (fareDTO.getFareRuleCode() != null) {
			fareRuleInFareRuleTable = getFareRule(BeanUtils.nullHandler(fareDTO.getFareRuleCode()));
			if (fareRuleInFareRuleTable != null) {
				retrunFlag = YES.equals(fareRuleInFareRuleTable.getReturnFlag());
			}
		} else {
			retrunFlag = YES.equals(fareDTO.getFareRule().getReturnFlag());
		}

		// check if fare rule, booking code, ond combination exists for time period
		checkIsFareLegal(fareDTO, SAVE_MODE, retrunFlag);
		log.debug("Fare is Legal");

		if (isShowFareDefInDepAirPCurr().equalsIgnoreCase("Y") && fare != null && fare.isLocalCurrencySelected()) {
			BigDecimal localAdFareAmount = new BigDecimal(fare.getFareAmountInLocal().toString());
			BigDecimal localChFareAmount = new BigDecimal(fare.getChildFareInLocal().toString());
			BigDecimal localInfFareAmount = new BigDecimal(fare.getInfantFareInLocal().toString());
			BigDecimal adultFareAmountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal childFareAmountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal infantFareAmountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(fare.getLocalCurrencyCode());

			if (currExRate != null) {
				BigDecimal exchangeRate = currExRate.getExrateBaseToCurNumber();// getMultiplyingExchangeRate();
				// Currency currency = currExRate.getCurrency();
				adultFareAmountInBase = AccelAeroRounderPolicy.convertAndRound(localAdFareAmount, exchangeRate, null, null);

				if (fare.getChildFareType() != null && fare.getChildFareType().equalsIgnoreCase("V")) {
					childFareAmountInBase = AccelAeroRounderPolicy.convertAndRound(localChFareAmount, exchangeRate, null, null);
				} else {
					// If it is sent as a percentage it should be saved as it is
					childFareAmountInBase = localChFareAmount;
				}
				if (fare.getInfantFareType() != null && fare.getInfantFareType().equalsIgnoreCase("V")) {
					infantFareAmountInBase = AccelAeroRounderPolicy.convertAndRound(localInfFareAmount, exchangeRate, null, null);
				} else {
					infantFareAmountInBase = localInfFareAmount;
				}

			}

			fare.setFareAmount(adultFareAmountInBase.doubleValue());
			fare.setChildFare(childFareAmountInBase.doubleValue());
			fare.setInfantFare(infantFareAmountInBase.doubleValue());
		}

		// check if user is saving a with FR or ADHOC

		if (fareDTO.getFareRuleCode() != null) {

			log.debug("fare is linked to a MFR");
			// check to see if the FR code already exists in the farerule table
			// Note-> Only one MFRCode can be in the farerule table
			// therefore if the MFRCode exists in the FareRule table dont save
			// the fareRule but take that fareRuleID and put it in Fare's
			// fareRuleID
			if (log.isDebugEnabled()) {
				log.debug("checking if FR Code: " + BeanUtils.nullHandler(fareDTO.getFareRuleCode())
						+ " exists in the fareRuleTable");
			}

			fareRuleInFareRuleTable = getFareRule(BeanUtils.nullHandler(fareDTO.getFareRuleCode()));

			BookingClass bookingClass = getBookingClassBD().getBookingClass(BeanUtils.nullHandler(fare.getBookingCode()));

			checkInboundDepartureValidityDatesForFareRule(fareRuleInFareRuleTable, bookingClass, fareDTO.getFare());
			checkFareRuleBookingClassValidity(fareRuleInFareRuleTable, bookingClass, ondCode);
			fare.setFareRuleId(fareRuleInFareRuleTable.getFareRuleId());
			log.debug("Fare's fare Rule ID will be set with an existing fare Rule id as MFR already exists in Fare Rule Table");

			// checkFareRuleBookingClassValidity(fareRuleInFareRuleTable, bookingClass)fareRuleInFareRuleTable
			saveNewONDCode(BeanUtils.nullHandler(fare.getOndCode()));

			if (fare.getFareId() != null) {
				// used to remove removed prorations in modify flow
				removeDeletedProrations(fareDTO.getProrations(), fare.getFareId());
				updateSalesEffectiveDates(fare.getFareId(), fare);
			}
			setProrations(fareDTO.getProrations(), fare, false);
			faredao.saveFare(fare);

			AuditAirpricing.doAudit(fare, userPrincipal, true);
			log.debug("saving fare success");
		}

		// FareDTO MFRCode was NULL and FR Not NUlL condition
		/** If the Code Comes here, it means User has Selected an Ad-Hoc FareRule * */
		else {

			saveOrUpdateADHOCFareRule(fareDTO, userPrincipal);
			AuditAirpricing.doAudit(fareDTO.getFareRule(), userPrincipal, true);
			AuditAirpricing.doAudit(fareDTO.getFare(), userPrincipal, true);
		}
		log.debug("END OF Save Fare BL");
	}

	/**
	 * BL Method when updating a Fare. void
	 * 
	 * @param skipDuplicateFareValidation
	 * @param fare
	 * @param skipSplitFare
	 * @throws
	 */
	public void updateFare(FareDTO fareDTO, UserPrincipal userPrincipal, boolean skipSplitFare,
			boolean skipDuplicateFareValidation) throws ModuleException {

		String ondCode = fareDTO.getFare().getOndCode();
		Integer originalFareId = fareDTO.getFare().getFareId();

		log.debug("updating a fare");

		if (log.isDebugEnabled()) {
			log.debug("Updating a Fare:" + BeanUtils.nullHandler(fareDTO.getFare()) + "Fare Rule :"
					+ BeanUtils.nullHandler(fareDTO.getFareRule()) + " fr code :"
					+ BeanUtils.nullHandler(fareDTO.getFareRuleCode()));
		}

		FareRule fareRuleInFareRuleTable = null;
		boolean retrunFlag = false;
		boolean isSplittedFare = false;
		if (fareDTO.getFareRuleCode() != null) {
			fareRuleInFareRuleTable = getFareRule(BeanUtils.nullHandler(fareDTO.getFareRuleCode()));
			if (fareRuleInFareRuleTable != null) {
				retrunFlag = YES.equals(fareRuleInFareRuleTable.getReturnFlag());
			}
		} else {
			retrunFlag = YES.equals(fareDTO.getFareRule().getReturnFlag());
		}

		if (!skipDuplicateFareValidation) {
			checkIsFareLegal(fareDTO, UPDATE_MODE, retrunFlag);
		}

		log.debug("Fare is legal");

		// TODO:::::: MUST DELETE THE ADHOC IF THE MFR IS CHANGED TO ADHOC.
		// get the fare for the fare ID
		// Fare fare = faredao.getFare(fareDTO.getFare().getFareId());
		// ///////////////////////////////////////////////
		Fare fare = fareDTO.getFare();

		if (!skipSplitFare) {
			Integer oldFareId = fare.getFareId();
			boolean fareAssignedForPnr = fareJdbcDAO.isFareAssignedForPnr(oldFareId);

			boolean fareSplitEnabled = AppSysParamsUtil.isSplitFareSalesValidityEnabled();

			if (fareAssignedForPnr && CalendarUtil.getCurrentSystemTimeInZulu().after(fare.getSalesEffectiveTo())) {
				throw new ModuleException("airpricing.fare.already.expired");
			}

			if (fareAssignedForPnr && fareSplitEnabled) {
				splitFareBySalesValidity(fare, skipDuplicateFareValidation, userPrincipal);
				isSplittedFare = true;
			}
		}

		if (isShowFareDefInDepAirPCurr().equalsIgnoreCase("Y") && fare != null && fare.isLocalCurrencySelected()) {
			BigDecimal localAdFareAmount = new BigDecimal(fare.getFareAmountInLocal().toString());
			BigDecimal localChFareAmount = new BigDecimal(fare.getChildFareInLocal().toString());
			BigDecimal localInfFareAmount = new BigDecimal(fare.getInfantFareInLocal().toString());
			BigDecimal adultFareAmountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal childFareAmountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal infantFareAmountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(fare.getLocalCurrencyCode());

			if (currExRate != null) {
				BigDecimal exchangeRate = currExRate.getExrateBaseToCurNumber();// getMultiplyingExchangeRate();
				// Currency currency = currExRate.getCurrency();
				adultFareAmountInBase = AccelAeroRounderPolicy.convertAndRound(localAdFareAmount, exchangeRate, null, null);

				if (fare.getChildFareType() != null && fare.getChildFareType().equalsIgnoreCase("V")) {
					childFareAmountInBase = AccelAeroRounderPolicy.convertAndRound(localChFareAmount, exchangeRate, null, null);
				} else {
					// If it is sent as a percentage it should be saved as it is
					childFareAmountInBase = localChFareAmount;
				}
				if (fare.getInfantFareType() != null && fare.getInfantFareType().equalsIgnoreCase("V")) {
					infantFareAmountInBase = AccelAeroRounderPolicy.convertAndRound(localInfFareAmount, exchangeRate, null, null);
				} else {
					infantFareAmountInBase = localInfFareAmount;
				}

			}

			fare.setFareAmount(adultFareAmountInBase.doubleValue());
			fare.setChildFare(childFareAmountInBase.doubleValue());
			fare.setInfantFare(infantFareAmountInBase.doubleValue());
		}

		// check if ADHOC or FR
		if (fareDTO.getFareRuleCode() != null) {

			log.debug("fare is linked to FR");
			/**
			 * If the FareRuleCode exists in the FareRule Table then Dont Update the FareRule Table *
			 */
			fareRuleInFareRuleTable = getFareRule(fareDTO.getFareRuleCode());

			if (log.isDebugEnabled()) {
				log.debug("Checking if Fare Rule exists in Fare Rule table with MFR Code :"
						+ BeanUtils.nullHandler(fareDTO.getFareRuleCode()));
			}

			BookingClass bookingClass = getBookingClassBD().getBookingClass(BeanUtils.nullHandler(fare.getBookingCode()));

			checkInboundDepartureValidityDatesForFareRule(fareRuleInFareRuleTable, bookingClass, fareDTO.getFare());
			checkFareRuleBookingClassValidity(fareRuleInFareRuleTable, bookingClass, ondCode);
			fare.setFareRuleId(fareRuleInFareRuleTable.getFareRuleId());
			log.debug("Already Existing FareRules ID is set to the FAres Fare Rule ID");
			// }

			// check if ond code was changed.

			saveNewONDCode(fareDTO.getFare().getOndCode());

			/** Save the Fare **/

			if (fare.getFareId() != null) {
				// used to remove removed prorations in modify flow
				removeDeletedProrations(fareDTO.getProrations(), fare.getFareId());
				updateSalesEffectiveDates(originalFareId, fare);
			}
			setProrations(fareDTO.getProrations(), fare, isSplittedFare);
			faredao.saveFare(fare);
			// save proration
			AuditAirpricing.doAudit(fare, userPrincipal);

			if (fare.getFareId() != null) {
				fareDTO.getFare().setFareId(fare.getFareId());
			}

			log.debug("success saving fare");
		}
		/** If the Code Comes here, it means User has Selected an Ad-Hoc FareRule * */
		else {
			saveOrUpdateADHOCFareRule(fareDTO, userPrincipal);
			AuditAirpricing.doAudit(fareDTO.getFareRule(), userPrincipal, false);
			AuditAirpricing.doAudit(fareDTO.getFare(), userPrincipal, false);
		}
		log.debug("END OF Update Fare BL");
	}

	/**
	 * Sets prorations to fare object
	 * 
	 * @param prorations
	 * @param fare
	 * @param isSplittedFare
	 */
	private void setProrations(Collection<Proration> prorations, Fare fare, boolean isSplittedFare) {
		if (prorations != null && prorations.size() > 0) {
			for (Proration proration : prorations) {
				proration.setFare(fare);
				if (isSplittedFare) {
					proration.setProrationId(null);
				}
			}
			fare.setProrations(prorations);
		} else {
			fare.setProrations(null);
		}

	}

	/**
	 * Remove prorations which are not relevant.
	 */
	private void removeDeletedProrations(Collection<Proration> newProrations, int fareId) {
		if (fareId > 0) {
			Fare savedFare = faredao.getFare(fareId);

			List<Proration> removeList = new ArrayList<Proration>();
			Collection<Proration> savedProrations = savedFare.getProrations();
			if (newProrations != null && newProrations.size() > 0) {

				for (Proration tempSavedProration : savedProrations) {
					boolean isExist = false;
					for (Proration tempNewProration : newProrations) {

						if (tempSavedProration.getProrationId().equals(tempNewProration.getProrationId())) {
							isExist = true;

							break;
						}
					}
					if (!isExist) {
						removeList.add(tempSavedProration);
					}
				}

			} else {
				removeList.addAll(savedProrations);
			}

			if (removeList.size() > 0) {
				faeProrationDao.deleteAllProrations(removeList);
			}
		}
	}

	/**
	 * Updates non critical FareRule details. No other operation like splitting will occur. Currently updates Agent
	 * Instructions and Comment.
	 * 
	 * Normally when an update occurs a new FareRule will be created and it's ID assigned to Fare.fareRuleID. When using
	 * this method it will update the existing FareRule object.
	 * 
	 * @param fareDTO
	 *            DTO object containing the Fare and FareRule objects.
	 * @throws ModuleException
	 *             If any of the underlying operations throws an exception.
	 */
	public void updateNonCriticalFareDetails(FareDTO fareDTO) throws ModuleException {
		// Load the fare freshly from database to get the fareRuleID
		Fare fare = faredao.getFare(fareDTO.getFare().getFareId());

		// Load a fresh instance to make sure that only the changes we want will be saved
		FareRule freshRule = fareruledao.getFareRule(fare.getFareRuleId());
		freshRule.setAgentInstructions(fareDTO.getFareRule().getAgentInstructions());
		freshRule.setRulesComments(fareDTO.getFareRule().getRulesComments());

		fareruledao.saveFareRule(freshRule);
	}

	/*
	 * private void saveProrations(FareDTO fareDTO,Fare fare) throws ModuleException {
	 * if(fareDTO.getProrations()!=null){ for(Proration proration:fareDTO.getProrations()){ proration.setFare(fare); }
	 * }else{ fare.setProrations(null); }
	 * 
	 * 
	 * }
	 */

	/**
	 * Split the fare based on the sales validity period. Fare passed in as argument will be saved as a new fare
	 * effective from today and existing fare's sales validity period will be truncated to today
	 * 
	 * If existing fare is a linked fare (fare that sales validity ends today), link to master will be removed If
	 * existing fare is a master fare (fare that sales validity ends today), all the master fare flags (code and type)
	 * will be removed
	 * 
	 * @param fare
	 * @param skipValidation
	 *            TODO
	 * @throws ModuleException
	 */
	private void splitFareBySalesValidity(Fare fare, boolean skipValidation, UserPrincipal userPrincipal) throws ModuleException {

		Date currentDate = new Date();
		Fare existFareObj = faredao.getFare(fare.getFareId());

		boolean isSalesEndToDay = CalendarUtil.isSameDay(currentDate, existFareObj.getSalesEffectiveTo());

		boolean isSalesEffectiveNotExpired = CalendarUtil.isGreaterThan(fare.getSalesEffectiveTo(), currentDate);
		boolean splitFare = true;

		if ((!AppSysParamsUtil.isSplitFareImmediateEffectEnable() && isSalesEndToDay) || !isSalesEffectiveNotExpired) {
			splitFare = false;

			if (!skipValidation) {
				throw new ModuleException("modify.fare.sales.validity.invalid");
			} else {
				log.error("WARNING: Fare Split skipped {fare-id,sales-end,sales-effective-not-expired} - " + fare.getFareId()
						+ "," + isSalesEndToDay + "," + isSalesEffectiveNotExpired);
			}

		}

		if (splitFare) {
			FareRule newFareRule = SplitFareBySalesUtil.copyExistingFareRuleAsAdhoc(existFareObj.getFareRuleId());

			SplitFareBySalesUtil.splitFares(existFareObj.getFareId(), newFareRule.getFareRuleId(), currentDate, fare, true,
					userPrincipal);
		}

	}

	/**
	 * Method to saveOrUpdateADHOCFareRule void
	 * 
	 * @param fareDTO
	 * @throws ModuleException
	 */
	private void saveOrUpdateADHOCFareRule(FareDTO fareDTO, UserPrincipal userPrincipal) throws ModuleException {
		String ondCode = fareDTO.getFare().getOndCode();
		log.debug("Saving or updating ADHOC Fare Rule");
		if (log.isDebugEnabled()) {
			log.debug("saving/updating ADHOC fare rule" + BeanUtils.nullHandler(fareDTO.getFareRule()));
		}
		/**
		 * when saving an adhoc farerule check the DTO structure.. it should meet the following *
		 */
		if (fareDTO.getFareRuleCode() == null && fareDTO.getFareRule() != null) {

			if (log.isDebugEnabled()) {
				log.debug("Getting the Booking Class for booking code : "
						+ BeanUtils.nullHandler(fareDTO.getFare().getBookingCode()));
			}

			BookingClass bookingClass = getBookingClassBD().getBookingClass(
					BeanUtils.nullHandler(fareDTO.getFare().getBookingCode()));

			log.debug("Getting booking class success");

			if (log.isDebugEnabled()) {
				log.debug("Check FR BC Validity :" + BeanUtils.nullHandler(fareDTO.getFareRule()) + " booking class : "
						+ BeanUtils.nullHandler(bookingClass));
			}

			checkInboundDepartureValidityDatesForFareRule(fareDTO.getFareRule(), bookingClass, fareDTO.getFare());
			// checkAllocationTypeValidity(fareDTO.getFareRule(),bookingClass,fareDTO.)
			checkFareRuleBookingClassValidity(fareDTO.getFareRule(), bookingClass, ondCode);

			saveNewONDCode(fareDTO.getFare().getOndCode());

			fareDTO.getFareRule().setFareRuleCode(fareDTO.getFareRuleCode() == null ? null : fareDTO.getFareRuleCode());

			if (fareDTO.getFareRule().getPaxDetails().size() < FareRule.NO_OF_PAX_COUNT) {
				throw new ModuleException("invalid.fare.rule.pax.count");
			}

			if (fareDTO.getFareRule().isLocalCurrencySelected()) {
				PricingUtils.setFareRuleChargesInSelectedCurr(fareDTO.getFareRule());
			}
            
			FareRule fareRule = fareDTO.getFareRule();
			if(fareRule.getVisibileAgentCodes() != null && !fareRule.getVisibileAgentCodes().isEmpty()){
				fareRule = PricingUtils.clearAgentsListIfVisibilityIsNotAgent(fareDTO.getFareRule());	
			}
			
			fareRule = PricingUtils.clearPOSListIfVisibilityNotWeb(fareRule);
		    			
			fareruledao.saveFareRule(fareRule);

			log.debug("saving fare rule success");

			if (log.isDebugEnabled()) {
				log.debug("set the new fare rule id :" + fareDTO.getFareRule().getFareRuleId() + " to the fare "
						+ BeanUtils.nullHandler(fareDTO.getFare()));
			}

			if (log.isDebugEnabled()) {
				log.debug("Fare Rules ID :" + fareDTO.getFareRule().getFareRuleId() + " is set to Fare");
			}

			fareDTO.getFare().setFareRuleId(fareDTO.getFareRule().getFareRuleId());
			Fare fare = fareDTO.getFare();

			if (fare.getFareId() != null && fare.getFareId() != 0) {
				// used to remove removed prorations in modify flow
				removeDeletedProrations(fareDTO.getProrations(), fare.getFareId());
				updateSalesEffectiveDates(fare.getFareId(), fare);
			}
			setProrations(fareDTO.getProrations(), fare, false);
			faredao.saveFare(fare);

			log.debug("Saving fare success");
		}
	}

	/**
	 * Check if FareRule with exists with the farerulecode in the farerule table. boolean
	 * 
	 * @param fareRuleCode
	 * @return
	 * @throws ModuleException
	 */
	private FareRule getFareRule(String fareRuleCode) throws ModuleException {
		try {
			if (log.isDebugEnabled()) {
				log.debug("getFareRule " + fareRuleCode);
			}
			FareRule fareRule = fareruledao.getFareRule(fareRuleCode);
			return fareRule;
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode(), cde.getModuleCode());
		}

	}

	/**
	 * BL Method to See if Fare Rule, Booking Code and OND Combination is duplicated. boolean
	 * 
	 * @param fare
	 * @return
	 * @throws CommonsDataAccessException
	 */
	private void checkIsFareLegal(FareDTO fareDTO, boolean updatemode, boolean retrunFlag) throws ModuleException {

		String fareRuleCode = fareDTO.getFareRuleCode();
		Integer fareId = fareDTO.getFare().getFareId();
		Date effectiveFromDate = fareDTO.getFare().getEffectiveFromDate();
		Date effectiveToDate = fareDTO.getFare().getEffectiveToDate();
		Date salesEffectiveFromDate = fareDTO.getFare().getSalesEffectiveFrom();
		Date salesEffectiveToDate = fareDTO.getFare().getSalesEffectiveTo();

		Date returnEffectiveFromDate = fareDTO.getFare().getReturnEffectiveFromDate();
		Date returnEffectiveToDate = fareDTO.getFare().getReturnEffectiveToDate();

		String bookingCode = fareDTO.getFare().getBookingCode();
		String ondCode = fareDTO.getFare().getOndCode();

		if (log.isDebugEnabled()) {
			log.debug("Checking if Fare is Legal "
					+ "Fare Rule Code :"
					+ BeanUtils.nullHandler(fareDTO.getFareRuleCode() + "Fare :" + BeanUtils.nullHandler(fareDTO.getFare())
							+ "Fare Rule :" + BeanUtils.nullHandler(fareDTO.getFareRule())));

		}

		boolean status = false;
		if (updatemode && fareId != null) {
			status = faredao.isFareLegal(fareRuleCode, bookingCode, ondCode, new Integer(fareId), effectiveFromDate,
					effectiveToDate, salesEffectiveFromDate, salesEffectiveToDate, returnEffectiveFromDate,
					returnEffectiveToDate, retrunFlag);
		} else {
			status = faredao.isFareLegal(fareDTO.getFareRuleCode(), bookingCode, ondCode, fareDTO.getFare()
					.getEffectiveFromDate(), fareDTO.getFare().getEffectiveToDate(), salesEffectiveFromDate,
					salesEffectiveToDate, returnEffectiveFromDate, returnEffectiveToDate, retrunFlag);
		}

		if (log.isDebugEnabled()) {
			log.debug("Is Fare Legal ? :" + status);
		}
		/**
		 * If FareRule-BookingCode-OND Combination Exists Must check if it exists for the same period.
		 * 
		 */

		if (status == false) {
			log.error("Fare Is Not Legal");

			throw new ModuleException("airpricing.fare.illegal", "airpricing.desc");
		}
	}

	/**
	 * Checks booking class, fare rule combination validity for allocation type
	 * 
	 * @param fareRule
	 * @param bookingClass
	 * @param ondCode
	 * @throws ModuleException
	 */
	private void checkAllocationTypeValidity(FareRule fareRule, BookingClass bookingClass, String ondCode) throws ModuleException {

		if (fareRule.getReturnFlag() == FareRule.Return_Flight_No) {// oneway fare

			// FIXME need to define the Hubs in configurations file and match against
			// Ondcodes not involving a Hub is considered as a segment
			if (ondCode.length() == 7 || (ondCode.length() > 7 && !isConnectionOnd(ondCode))) {// oneway, segment fare
				if (bookingClass.getAllocationType().equals(BookingClass.AllocationType.RETURN)
						|| bookingClass.getAllocationType().equals(BookingClass.AllocationType.CONNECTION)) {
					throw new ModuleException("airpricing.bc.alloctype.invalid", "airpricing.desc");
				}
			} else if (ondCode.length() > 7) {// oneway, connection fare
				if (bookingClass.getAllocationType().equals(BookingClass.AllocationType.RETURN)
						|| bookingClass.getAllocationType().equals(BookingClass.AllocationType.SEGMENT)) {
					throw new ModuleException("airpricing.bc.alloctype.invalid", "airpricing.desc");
				}
			}
		}

		if (fareRule.getReturnFlag() == FareRule.Return_Flight_Yes) {// return fare
			if (bookingClass.getAllocationType().equals(BookingClass.AllocationType.SEGMENT)
					|| bookingClass.getAllocationType().equals(BookingClass.AllocationType.CONNECTION)) {
				throw new ModuleException("airpricing.bc.alloctype.invalid", "airpricing.desc");
			}
		}
	}

	private boolean isConnectionOnd(String ondCode) {
		boolean isConnectionOnd = false;
		// FIXME have to be removed comment block
		// String hub = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
		// {(hub != null && !"".equals(hub))? hub : "SHJ"};
		Collection<HubAirportTO> hubsList = CommonsServices.getGlobalConfig().getHubsMap().values();
		Set<String> airportCodeSet = new HashSet<String>();

		for (HubAirportTO hubAirportTO : hubsList) {
			HubAirportTO airportTO = hubAirportTO;
			airportCodeSet.add(airportTO.getAirportCode());
		}

		Object[] hubs = airportCodeSet.toArray();

		for (Object hub : hubs) {
			if (ondCode.indexOf("/" + hub.toString() + "/") != -1) {
				isConnectionOnd = true;
				break;
			}
		}

		return isConnectionOnd;
	}

	/**
	 * BL Method to check if if booking class is fixed, farerule visibilities should be only agent. boolean
	 * 
	 * @param fareRule
	 * @param bookingClass
	 * @return
	 */
	private void checkFareRuleBookingClassValidity(FareRule fareRule, BookingClass bookingClass, String ondCode)
			throws ModuleException {

		checkAllocationTypeValidity(fareRule, bookingClass, ondCode);

		if (log.isDebugEnabled()) {
			log.debug("BYORN: FareRule :" + fareRule.toString() + " Booking Class :" + bookingClass.toString());

		}
		log.debug("Checking Fare Rule and Booking Class Validity");
		// String agentChannelCode =
		// (String)AirpricingUtils.getAirpricingConfig().getSalesChannelCodesMap().get("TravelAgent");
		// String allChannelCode = (String) AirpricingUtils.getAirpricingConfig().getSalesChannelCodesMap().get("All");

		int agentChannelCode = SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY);
		// int allChannelCode = SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY);

		if (bookingClass.getFixedFlag() == true) {

			Set<Integer> codes = fareRule.getVisibleChannelIds();
			Iterator<Integer> iter = codes.iterator();
			while (iter.hasNext()) {
				int channelId = Integer.parseInt(iter.next().toString());
				if (log.isDebugEnabled()) {
					log.debug("BYORN: Channel Id:" + channelId);

				}
				if (!((channelId == agentChannelCode))) {
					log.error("For Fixed BookingClass Fare Rule Visible Channels is NOT Agent ERROR");
					throw new ModuleException("airpricing.fare.bcfr.invalid", "airpricing.desc");
				}
			}
		}

	}

	/**
	 * BL Method to sava a new OrigingDestination. void
	 * 
	 * @param ondCode
	 */
	private void saveNewONDCode(String ondCode) throws ModuleException {
		log.debug("Saving new OND Code");
		if (onddao.getOND(ondCode) == null) {
			OriginDestination originDestination = new OriginDestination();
			originDestination.setONDCode(ondCode);
			originDestination.setOriginAirportCode(ondCode.substring(0, 3));
			int dstntnIndx = ondCode.length() - 3;
			originDestination.setDestinationAirportCode(ondCode.substring(dstntnIndx));
			try {
				onddao.saveOND(originDestination);
			} catch (CommonsDataAccessException dataAccessException) {
				throw new ModuleException(dataAccessException, dataAccessException.getExceptionCode());
			}
		}

	}

	/**
	 * BL Method used to Update a collection of fare objects. void
	 * 
	 * @param fares
	 * @param skipSplitFare
	 * @param skipDuplicateFareValidation
	 * @throws ModuleException
	 */
	public void updateFares(Collection<Fare> fares, UserPrincipal userPrincipal, boolean skipSplitFare,
			boolean skipDuplicateFareValidation) throws ModuleException {
		Iterator<Fare> iterator = fares.iterator();

		while (iterator.hasNext()) {
			Fare fare = iterator.next();

			FareDTO fareDTO = getFare(fare.getFareId());
			Integer fareId = fare.getFareId();
			if (fareDTO != null) {
				updateSalesEffectiveDates(fareId, fare);
				fareDTO.setFare(fare);
				updateFare(fareDTO, userPrincipal, skipSplitFare, skipDuplicateFareValidation);
				/*
				 * fareDTO.setFare(fare); checkIsFareLegal(fareDTO, UPDATE_MODE); try { faredao.saveFare(fare); } catch
				 * (CommonsDataAccessException commonsDataAccessException) { throw new
				 * ModuleException(commonsDataAccessException, commonsDataAccessException.getExceptionCode(),
				 * "airpricing.desc");
				 * 
				 * }
				 */
			} else {
				log.error("FARE was not found for fare ID " + fare.getFareId());
			}
			if (fare.getFareDefType().equals(Fare.FARE_DEF_TYPE_MASTER)) {
				updateLinkFares(fare, fareId, userPrincipal, skipDuplicateFareValidation);
			}
		}

	}

	/**
	 * BL Method used to get Effective Agents Count Values for each booking code void
	 * 
	 * @param bookingCodes
	 * @param ondCode
	 * @param depatureDate
	 * @throws ModuleException
	 */
	public HashMap<String, Integer> getEffectiveAgentsCount(Collection<String> bookingCodes, String ondCode, Date depatureDate)
			throws ModuleException {
		log.debug("get Effective Agents Count");

		if (log.isDebugEnabled()) {
			log.debug("Booking Codes" + bookingCodes.toString() + " ond code: " + BeanUtils.nullHandler(ondCode)
					+ " departureDate :" + BeanUtils.nullHandler(depatureDate));
		}

		Collection<Object[]> fareRuleList = faredao.getEffectiveFaresWithFareRule(bookingCodes, ondCode, depatureDate);

		HashMap<String, Integer> hashMap = new HashMap<String, Integer>();

		String bookingCode = "";
		Iterator<String> bookingCodesIterator = bookingCodes.iterator();
		while (bookingCodesIterator.hasNext()) {
			bookingCode = bookingCodesIterator.next();
			Integer agentCount = getAgentCount(bookingCode, fareRuleList);
			hashMap.put(bookingCode, agentCount);
		}
		return hashMap;

	}

	/**
	 * BL Method to get Agent Count. Integer
	 * 
	 * @param bookingCode
	 * @param fareRuleList
	 * @return
	 * @throws ModuleException
	 */
	private Integer getAgentCount(String bookingCode, Collection<Object[]> fareRuleList) throws ModuleException {

		int count = 0;
		Iterator<Object[]> iterator = fareRuleList.iterator();
		while (iterator.hasNext()) {
			Object[] pair = iterator.next();
			String bCode = pair[0].toString();
			FareRule fareRule = (FareRule) pair[1];

			if (bookingCode.equals(bCode)) {
				count = fareRule.getVisibileAgentCodes().size();
			}

		}
		return new Integer(count);

	}

	/**
	 * Return Map of chargeTO
	 * 
	 * @param Collection
	 *            of chargeRateIds
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, ChargeTO> getChageRateById(Collection<Integer> chargeRateIds) throws ModuleException {
		return getChargeDAO().getChageRateById(chargeRateIds);
	}

	/**
	 * Return refundable states
	 * 
	 * @param fareIds
	 * @param chargeRateIds
	 * @param prefferedLanguage
	 * @return
	 * @throws ModuleException
	 */
	public FaresAndChargesTO getRefundableStatuses(Collection<Integer> fareIds, Collection<Integer> chargeRateIds,
			String prefferedLanguage) throws ModuleException {
		HashMap<Integer, FareTO> fareTOsMap = new HashMap<Integer, FareTO>();
		HashMap<Integer, ChargeTO> chargeTOsMap = new HashMap<Integer, ChargeTO>();
		HashMap<Integer, Collection<FareRuleFeeTO>> fareRuleFeeMap = new HashMap<Integer, Collection<FareRuleFeeTO>>();

		Collection<FareTO> fareTOs = null;
		Collection<ChargeTO> chargeTOs = null;
		ChargeTO chargeTO;
		Collection<FareRuleFeeTO> colFareRuleFee = null;

		try {
			if (fareIds != null && fareIds.size() > 0) {
				fareTOs = fareRuleJdbcDao.getRefundableFares(fareIds, prefferedLanguage);
				Collection<Integer> fareRuleIds = new HashSet<Integer>();

				for (FareTO fareTO : fareTOs) {
					fareRuleIds.add(fareTO.getFareRuleID());
				}

				colFareRuleFee = fareRuleJdbcDao.getFareRuleFees(fareRuleIds);
			}

			chargeTOs = getChargeDAO().getRefundableCharges(chargeRateIds);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

		if (fareTOs != null) {
			Iterator<FareTO> fareTosIter = fareTOs.iterator();
			while (fareTosIter.hasNext()) {
				FareTO fareTO = fareTosIter.next();
				fareTOsMap.put(new Integer(fareTO.getFareId()), fareTO);
			}
		}

		if (chargeTOs != null) {
			Iterator<ChargeTO> chargeTosIter = chargeTOs.iterator();
			while (chargeTosIter.hasNext()) {
				chargeTO = chargeTosIter.next();
				chargeTOsMap.put(new Integer(chargeTO.getChargeRateId()), chargeTO);
			}
		}

		if (colFareRuleFee != null) {
			Iterator<FareRuleFeeTO> itFareRuleFee = colFareRuleFee.iterator();
			while (itFareRuleFee.hasNext()) {
				FareRuleFeeTO fareRuleFeeTO = itFareRuleFee.next();

				Collection<FareRuleFeeTO> colFareRuleFeeTO = fareRuleFeeMap.get(new Integer(fareRuleFeeTO.getFareRuleId()));

				if (colFareRuleFeeTO != null) {
					colFareRuleFeeTO.add(fareRuleFeeTO);
				} else {
					colFareRuleFeeTO = new ArrayList<FareRuleFeeTO>();
					colFareRuleFeeTO.add(fareRuleFeeTO);
					fareRuleFeeMap.put(new Integer(fareRuleFeeTO.getFareRuleId()), colFareRuleFeeTO);
				}
			}
		}

		FaresAndChargesTO faresAndChargesTO = new FaresAndChargesTO();
		faresAndChargesTO.setChargeTOs(chargeTOsMap);
		faresAndChargesTO.setFareTOs(fareTOsMap);
		faresAndChargesTO.setFareRuleFeeTOs(fareRuleFeeMap);

		return faresAndChargesTO;
	}

	/**
	 * Return refundable states
	 * 
	 * @param fareRuleIds
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, FareSummaryDTO> getPaxDetails(Collection<Integer> fareRuleIds) throws ModuleException {
		return fareRuleJdbcDao.getPaxDetails(fareRuleIds);
	}

	/**
	 * 
	 * BookingClassBD
	 * 
	 * @return
	 */
	private BookingClassBD getBookingClassBD() {
		return AirpricingUtils.getBookingClassBD();
	}

	public Collection<Charge> getActiveCharges(Collection<String> colChargeGrpCodes) {
		return getChargeDAO().getActiveCharges(colChargeGrpCodes);
	}

	/**
	 * Retrieve Currencies and and exRates that are currently active and was updated.
	 * <p>
	 * The exRate is not used for any calculations but its extracted for a validation that is not in place yet.
	 * </P>
	 * 
	 * @throws ModuleException
	 */
	public void reCalcBaseFareForLocalCurr(Collection<CurrencyExchangeRate> currExchangeList) throws ModuleException {
		Date date = new Date();
		if (currExchangeList != null && !currExchangeList.isEmpty()) {
			Iterator<CurrencyExchangeRate> itrCurrencyList = currExchangeList.iterator();
			CurrencyExchangeRate currExRate;
			while (itrCurrencyList.hasNext()) {
				currExRate = itrCurrencyList.next();
				reCalculateFares(currExRate.getCurrency().getCurrencyCode(), date);
			}
		}
	}

	/**
	 * Re-calculate Base fare, when conversion rates changed.
	 * 
	 * @param toDate
	 * @throws ModuleException
	 */
	private void reCalculateFares(String currencySpecifiedIn, Date date) throws ModuleException {

		Collection<Fare> colFaresWithLocalCurr = faredao.getFaresWithLocalCurrency(date);

		if (colFaresWithLocalCurr != null && colFaresWithLocalCurr.size() > 0) {
			Iterator<Fare> itr = colFaresWithLocalCurr.iterator();
			Collection<Fare> faresCol = new ArrayList<Fare>();
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

			while (itr.hasNext()) {
				Fare fare = itr.next();
				if (fare != null && fare.getLocalCurrencyCode().equalsIgnoreCase(currencySpecifiedIn)) {
					if (fare.getFareAmountInLocal() != null) {
						double fareInLocalCurr = fare.getFareAmountInLocal().doubleValue();

						BigDecimal localFareAmt = AccelAeroCalculator.parseBigDecimal(fareInLocalCurr);
						String currencyCode = fare.getLocalCurrencyCode();
						CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(currencyCode);

						if (currExRate != null) {
							BigDecimal exchangeRate = currExRate.getMultiplyingExchangeRate();
							Currency currency = currExRate.getCurrency();
							BigDecimal baseFareAmtInBaseCurr = AccelAeroRounderPolicy.convertAndRound(localFareAmt, exchangeRate,
									currency.getBoundryValue(), currency.getBreakPoint());
							fare.setFareAmount(baseFareAmtInBaseCurr.doubleValue());
							faresCol.add(fare);
						}
					}
				}

			}
			if (faresCol != null && faresCol.size() > 0) {
				faredao.saveOrUpdateFare(faresCol);
			}
		}

	}

	/**
	 * Update currecny exRate FARES_IN_LOCAL_CUR_UPD_STATUS to Success \ Fail
	 * 
	 * @param colCurrency
	 * @param status
	 * @throws ModuleException
	 */
	public void updateStatusForFaresInLocalCurr(Collection<CurrencyExchangeRate> colCurrExchangeRates, String status)
			throws ModuleException {
		if (colCurrExchangeRates != null && status != null && !colCurrExchangeRates.isEmpty()) {
			Iterator<CurrencyExchangeRate> itrCurrencyList = colCurrExchangeRates.iterator();
			CurrencyExchangeRate currExRate;
			while (itrCurrencyList.hasNext()) {
				currExRate = itrCurrencyList.next();
				currExRate.setFaresInLocalCurrUpdateStatus(status);
			}

			AirpricingUtils.getCommonMasterBD().saveOrUpdateCurrencyExRate(colCurrExchangeRates);
		}
	}

	private void checkInboundDepartureValidityDatesForFareRule(Object objFareRule, BookingClass bookingClass, Fare fare)
			throws ModuleException {
		char returnFare = FareRule.Return_Flight_No;

		if (objFareRule instanceof FareRule) {
			returnFare = ((FareRule) objFareRule).getReturnFlag();
		}

		if (returnFare == FareRule.Return_Flight_Yes) {// return fare
			if (bookingClass.getAllocationType().equals(BookingClass.AllocationType.COMBINED)
					|| bookingClass.getAllocationType().equals(BookingClass.AllocationType.RETURN)) {
				if (fare.getReturnEffectiveFromDate() == null || "".equals(fare.getReturnEffectiveFromDate())
						|| fare.getReturnEffectiveToDate() == null || "".equals(fare.getReturnEffectiveToDate())) {
					throw new ModuleException("airpricing.fare.bcfr.ret.retdepvalidityrequired", "airpricing.desc");
				}
			}
		} else {// Segment or connection fares - set return departure validity to null
			fare.setReturnEffectiveFromDate(null);
			fare.setReturnEffectiveToDate(null);
		}
	}

	public Collection<FareRule> getFareRules(String fareBasisCode) {
		return fareruledao.getFareRuleByBasisCode(fareBasisCode);
	}

	public Map<Integer, FareSummaryDTO> getLocalFares(Collection<Integer> fareIds) {
		return fareRuleJdbcDao.getLocalFares(fareIds);
	}

	public Map<Integer, String> getMasterFaresForOnD(String ondCode) {
		return faredao.getMasterFares(ondCode);
	}

	public boolean isMasterFareAlreadyLinked(Integer masterFareID) {
		return faredao.isMasterFareLinked(masterFareID);
	}

	public boolean isMasterFareRefCodeAlreayUsed(String masterFareRefCode) {
		return faredao.isMasterFareRefCodeAlreayUsed(masterFareRefCode);
	}

	public Collection<Fare> getLinkFares(Integer masterFareID) {
		return faredao.getLinkFares(masterFareID);
	}

	public void
			updateLinkFares(Fare masterFare, Integer fareId, UserPrincipal userPrincipal, boolean skipDuplicateFareValidation)
					throws ModuleException {

		Collection<Fare> linkFares = faredao.getLinkFares(fareId);

		boolean fareSplitEnabled = AppSysParamsUtil.isSplitFareSalesValidityEnabled();

		if (linkFares != null && linkFares.size() > 0) {
			for (Fare linkFare : linkFares) {
				boolean fareAssignedForPnr = fareJdbcDAO.isFareAssignedForPnr(linkFare.getFareId());

				if (fareAssignedForPnr && fareSplitEnabled) {
					splitFareBySalesValidity(linkFare, skipDuplicateFareValidation, userPrincipal);
					faredao.saveFare(linkFare);
				}

				MasterAndLinkFareUtil.updateFieldsOfLinkFare(linkFare, masterFare);
				MasterAndLinkFareUtil.adjustChargesAndUpdateForLinkFare(linkFare, masterFare);
			}
			updateFares(linkFares, userPrincipal, true, skipDuplicateFareValidation);
		}

	}

	/**
	 * Update Fares Automatically upon the Exchange Rate Change 1.Retrive the updated Currency Exchange List 2.Retrive
	 * the fares, and update the fare amount and finally update the Currency
	 * 
	 * @param userPrincipal
	 * @throws ModuleException
	 */
	public void fareUpdateOnExchangeRateChange(UserPrincipal userPrincipal) throws ModuleException {

		ArrayList<CurrencyExchangeRate> updatedCurrencyExchangeList;
		ArrayList<Fare> applicableFareList;
		BigDecimal newFareAmount;
		BigDecimal newChildFareAmount;
		BigDecimal newInfantFareAmount;
		try {
			if (AppSysParamsUtil.isShowFareDefInDepAirPCurr()) {
				updatedCurrencyExchangeList = (ArrayList<CurrencyExchangeRate>) AirpricingUtils.getCurrencyDAO().getExchangeRatesForAutomatedUpdate(CalendarUtil.getCurrentSystemTimeInZulu(), InternalConstants.FARE_UPDATED_FOR_EXRATE_CHNG);
				//		.getExchangeRatesForAutomatedFareUpdate(CalendarUtil.getCurrentSystemTimeInZulu());
				if (updatedCurrencyExchangeList.isEmpty()) {
					log.debug("No Currency Exchange Rates are Available for Automatic Fare Update");
				} else {
					for (CurrencyExchangeRate currencyExchangeRate : updatedCurrencyExchangeList) {
						applicableFareList = (ArrayList<Fare>) faredao
								.getApplicableFaresForUpdateOnExchangeRateChange(currencyExchangeRate.getCurrency()
										.getCurrencyCode());

						Iterator<Fare> applicableFareItr = applicableFareList.iterator();

						while (applicableFareItr.hasNext()) {
							Fare fare = applicableFareItr.next();

							// for (Fare fare : applicableFareList) {
							Date currentDate = new Date();
							boolean isSalesEndToDay = CalendarUtil.isSameDay(currentDate, fare.getSalesEffectiveTo());

							boolean isSalesEffectiveNotExpired = CalendarUtil.isGreaterThan(fare.getSalesEffectiveTo(),
									currentDate);

							if ((!AppSysParamsUtil.isSplitFareImmediateEffectEnable() && isSalesEndToDay)
									|| !isSalesEffectiveNotExpired) {
								applicableFareItr.remove();
								continue;
							}

							newFareAmount = AccelAeroRounderPolicy.convertAndRound(AccelAeroCalculator.parseBigDecimal(fare
									.getFareAmountInLocal()), currencyExchangeRate.getExrateBaseToCurNumber(),
									currencyExchangeRate.getCurrency().getBoundryValue(), currencyExchangeRate.getCurrency()
											.getBreakPoint());
							fare.setFareAmount(newFareAmount.doubleValue());
							if (fare.getChildFareType() != null && fare.getChildFareType().equalsIgnoreCase("V")) {
								newChildFareAmount = AccelAeroRounderPolicy.convertAndRound(AccelAeroCalculator
										.parseBigDecimal(fare.getChildFareInLocal()), currencyExchangeRate
										.getExrateBaseToCurNumber(), currencyExchangeRate.getCurrency().getBoundryValue(),
										currencyExchangeRate.getCurrency().getBreakPoint());
								fare.setChildFare(newChildFareAmount.doubleValue());
							}
							if (fare.getInfantFareType() != null && fare.getInfantFareType().equalsIgnoreCase("V")) {
								newInfantFareAmount = AccelAeroRounderPolicy.convertAndRound(AccelAeroCalculator
										.parseBigDecimal(fare.getInfantFareInLocal()), currencyExchangeRate
										.getExrateBaseToCurNumber(), currencyExchangeRate.getCurrency().getBoundryValue(),
										currencyExchangeRate.getCurrency().getBreakPoint());
								fare.setInfantFare(newInfantFareAmount.doubleValue());
							}

						}

						// faredao.saveOrUpdateFare(applicableFareList);
						updateFares(applicableFareList, userPrincipal, false, true);
						// update fare-rule not defined in fare
						updateFareRuleOnExchangeRateChange(currencyExchangeRate.getCurrency().getCurrencyCode(), userPrincipal);

						// update fare-rule defined in fare and fare sales validity expired

						currencyExchangeRate.setFaresUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_ENABLED);
					}
					AirpricingUtils.getCurrencyDAO().saveOrUpdateCurrencyExRates(updatedCurrencyExchangeList);
				}
			}
		} catch (CommonsDataAccessException e) {
			log.error("ERROR-Auto Updating Fares upon Exchnage Rate Change Failed");
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}

	private void updateFareRuleOnExchangeRateChange(String currencyCode, UserPrincipal userPrincipal) throws ModuleException {

		ArrayList<FareRule> fareRuleList = (ArrayList<FareRule>) fareruledao
				.getApplicableFareRulesForExchangeRateChange(currencyCode);

		if (fareRuleList != null && fareRuleList.size() > 0) {

			Iterator<FareRule> fareRuleListItr = fareRuleList.iterator();

			while (fareRuleListItr.hasNext()) {
				FareRule fareRule = fareRuleListItr.next();
				Integer fareRuleId = fareRule.getFareRuleId();

				Collection<Fare> faresCollect = faredao.getFaresByFareRuleId(fareRuleId);

				if (AppSysParamsUtil.isSplitFareSalesValidityEnabled()) {

					boolean isFareRuleAssociatedWithPnr = fareJdbcDAO.isFareRuleAssociatedWithPnr(fareRuleId);

					FareRule adhocFareRule = null;

					if (isFareRuleAssociatedWithPnr && faresCollect != null && faresCollect.size() > 0) {

						adhocFareRule = SplitFareBySalesUtil.copyExistingFareRuleAsAdhoc(fareRuleId);

						Iterator<Fare> faresItr = faresCollect.iterator();
						Date currentDate = new Date();

						while (faresItr.hasNext()) {
							Fare fare = faresItr.next();
							boolean fareAssignedForPnr = fareJdbcDAO.isFareAssignedForPnr(fare.getFareId());

							boolean isSalesEffectiveNotExpired = CalendarUtil.isGreaterThan(fare.getSalesEffectiveTo(),
									new Date());

							boolean isSalesEndToDay = CalendarUtil.isSameDay(currentDate, fare.getSalesEffectiveTo());

							if (fareAssignedForPnr && isSalesEffectiveNotExpired && !isSalesEndToDay) {
								SplitFareBySalesUtil.splitFares(fare.getFareId(), adhocFareRule.getFareRuleId(), currentDate,
										null, true, userPrincipal);
							} else if (fareAssignedForPnr) {
								SplitFareBySalesUtil.splitFares(fare.getFareId(), adhocFareRule.getFareRuleId(), currentDate,
										null, false, userPrincipal);
							}

						}

					}
				}

				PricingUtils.setFareRuleChargesInSelectedCurr(fareRule);
				fareruledao.saveFareRule(fareRule);
			}
		}

	}

	private void updateSalesEffectiveDates(int existingFareId, Fare updatedFare) {
		/**
		 * If no sales effective date modification is done from the UI we should set those dates from the values in db
		 * for include the validity time with relevant minutes.
		 * 
		 */
		if (AppSysParamsUtil.isSplitFareImmediateEffectEnable()) {
			return;
		}

		Fare existingFare = faredao.getFare(existingFareId);
		if (CalendarUtil.isSameDay(existingFare.getSalesEffectiveFrom(), updatedFare.getSalesEffectiveFrom())) {
			updatedFare.setSalesEffectiveFrom(existingFare.getSalesEffectiveFrom());
		}

		if (CalendarUtil.isSameDay(existingFare.getSalesEffectiveTo(), updatedFare.getSalesEffectiveTo())) {
			updatedFare.setSalesEffectiveTo(existingFare.getSalesEffectiveTo());
		}
	}
}