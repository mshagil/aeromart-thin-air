package com.isa.thinair.airpricing.core.remoting.ejb;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airpricing.api.criteria.DefaultAnciTemplSearchCriteria;
import com.isa.thinair.airpricing.api.dto.DefaultAnciTemplateTO;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplate;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplateRoute;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.audit.AuditAirpricing;
import com.isa.thinair.airpricing.core.persistence.dao.CommonAncillaryDAO;
import com.isa.thinair.airpricing.core.service.bd.CommonAncillaryServiceDeligateImpl;
import com.isa.thinair.airpricing.core.service.bd.CommonAncillaryServiceLocalDeligateImpl;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;

@Stateless
@RemoteBinding(jndiBinding = "CommonAncillaryService.remote")
@LocalBinding(jndiBinding = "CommonAncillaryService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class CommonAncillaryServiceBean extends PlatformBaseSessionBean implements CommonAncillaryServiceDeligateImpl,
		CommonAncillaryServiceLocalDeligateImpl {

	private static Log log = LogFactory.getLog(CommonAncillaryServiceBean.class);

	@Override
	public Page<DefaultAnciTemplate> getAllDefaultAnciTemplate() throws ModuleException {
		try {
			return getCommonAncillaryDAO().getdefaultAnciTemaplates(0, 10);

		} catch (CommonsDataAccessException e) {
			log.error("DefaultAnciTemplate list retrieve fails", e);
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveDefaultAnciTemplate(DefaultAnciTemplate template) throws ModuleException {
		try {
			template = (DefaultAnciTemplate) setUserDetails(template);
			getCommonAncillaryDAO().saveDefaultAnciTemplate(template);
			doAuditDefaultAnciTemplate(template, false);

		} catch (CommonsDataAccessException e) {
			log.error("DefaultAnciTemplate list save fails", e);
			this.sessionContext.setRollbackOnly();
			if (e.getExceptionCode().equals("module.saveconstraint.childerror")) {
				log.error("DefaultAnciTemplate list retrieve fails", e);
				throw new ModuleException("module.saveconstraint.childerror",
						MessagesUtil.getMessage("module.saveconstraint.childerror"));
			}
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private CommonAncillaryDAO getCommonAncillaryDAO() {
		return (CommonAncillaryDAO) AirpricingUtils.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.COMMON_ANCILLARY_DAO);
	}

	@Override
	public Page<DefaultAnciTemplateTO> searchDefAnciTemplate(DefaultAnciTemplSearchCriteria criteria, int startRec, int noRecs)
			throws ModuleException {
		try {
			return getCommonAncillaryDAO().searchDefAnciTemplate(criteria, startRec, noRecs);

		} catch (CommonsDataAccessException e) {
			log.error("DefaultAnciTemplate list retrieve fails", e);
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airpricing.desc");
		}

	}

	private void doAuditDefaultAnciTemplate(DefaultAnciTemplate template, boolean fromDelete) throws ModuleException {
		StringBuilder sb = new StringBuilder();

		if(template.getAssignedONDs()!=null){
			for (DefaultAnciTemplateRoute ond : template.getAssignedONDs()) {
				sb.append(ond.getOndCode());
				sb.append(",");
			}
		}
		String taskCode;
		if (template.getVersion() < 0) {
			taskCode = TasksUtil.MASTER_ROUTE_WISE_DEF_ANCI_TEMPLATE_CREATE;
		}else if(fromDelete){
			taskCode = TasksUtil.MASTER_ROUTE_WISE_DEF_ANCI_TEMPLATE_DELETE;
		} else {
			taskCode = TasksUtil.MASTER_ROUTE_WISE_DEF_ANCI_TEMPLATE_UPDATE;
		}
		AuditAirpricing.doAuditRouteWiseDefaultAnciTemplate(template.getDefaultAnciTemplateId(), template.getAncillaryType(),
				template.getAnciTemplateId(), sb.toString(), template.getStatus(), taskCode, getUserPrincipal());
	}

	@Override
	public void checkDuplicateDefaultAnciTemplateForSameRoute(DefaultAnciTemplateTO templateTO) throws ModuleException {

		try {
			boolean isDuplicate = getCommonAncillaryDAO().checkDuplicateDefaultAnciTemplateForSameRoute(templateTO);

			if (isDuplicate) {
				throw new ModuleException("module.duplicate.key", MessagesUtil.getMessage("module.duplicate.key"));
			}

		} catch (CommonsDataAccessException e) {
			log.error("DefaultAnciTemplate list retrieve fails", e);
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public void checkTemplateAttachedToRouteWiseDefAnciTempl(DefaultAnciTemplate.ANCI_TEMPLATES anciType, int templateId)
			throws ModuleException {
		try {
			boolean isAttachedToDefAnciTempl = getCommonAncillaryDAO().checkTemplateAttachedToRouteWiseDefAnciTempl(anciType,
					templateId);

			if (isAttachedToDefAnciTempl) {
				throw new ModuleException("airpricing.anci.template.linked.to.routewise.def.templ", "airpricing.desc");
			}

		} catch (CommonsDataAccessException e) {
			log.error("DefaultAnciTemplate list retrieve fails", e);
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airpricing.desc");
		}

	}

	@Override
	public Map<String, Integer> getDefaultAnciTemplateForFlight(String ondCode, String flightModel) throws ModuleException {
		try {

			return getCommonAncillaryDAO().getDefaultAnciTemplateForFlight(ondCode, flightModel);

		} catch (CommonsDataAccessException e) {
			log.error("getting Default anci templates for flight getting failed");
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airpricing.desc");
		}

	}

	@Override
	public void deleteDefaultAnciTemplate(DefaultAnciTemplate template) throws ModuleException {
		try {
			doAuditDefaultAnciTemplate(template,true);
			getCommonAncillaryDAO().deleteDefaultAnciTemplate(template);

		} catch (CommonsDataAccessException e) {
			log.error("Deleting Default anci templates getting failed");
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airpricing.desc");
		}
	}
}
