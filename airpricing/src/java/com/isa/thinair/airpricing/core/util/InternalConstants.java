/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.core.util;

/**
 * Class to keep constants related to AirPricing. The Constants defined in this class should only use within the module
 * 
 * @author Byorn
 */
public abstract class InternalConstants {

	public static String MODULE_NAME = "airpricing";
	
	public static final String FARE_UPDATED_FOR_EXRATE_CHNG = "exRate.faresUpdatedForExchangeRateChange";
	public static final String CHARGE_UPDATED_FOR_EXRATE_CHNG = "exRate.chargesUpdatedForExchangeRateChange";
	public static final String ANCICHARGE_UPDATED_FOR_EXRATE_CHNG = "exRate.anciChargesUpdatedForExchangeRateChange";
	
	/** For Looking-up the DAOs **/
	public static interface DAOProxyNames {

		public static String CHARGE_DAO = "ChargeDAOProxy";
		public static String HANDLINGCHARGE_DAO = "HandlingChargeDAOProxy";
		public static String FARE_DAO = "FareDAOProxy";
		public static String FARE_RULE_DAO = "FareRuleDAOProxy";
		public static String OND_DAO = "ONDDAOProxy";
		public static String PAYMENTGATEWAY_WISE_CHARGE_DAO = "PaymentGatewayWiseChargesDAOProxy";
		public static String MANAGE_FARES = "fareDAO";
		public static String AGENT_FARE_RULE = "fareruleDAO";
		public static String CHARGE_JDBC_DAO = "chargeJdbcDAO";
		public static String OND_CHARGE_JDBC_DAO = "ondChargeJdbcDAO";
		public static String SSR_CHARGE_JDBC_DAO = "ssrChargeJdbcDAO";
		public static String FLEXI_RULE_DAO = "FlexiRuleDAOProxy";
		public static String FLEXI_RULE_JDBC_DAO = "FlexiRuleJdbcDAO";
		public static String FARE_PRORATION_DAO = "FareProrationDAOProxy";
		public static String COMMON_ANCILLARY_DAO = "CommonAncillaryDAOProxy";
	}

	/** Charges Column Names to pass in OrderBy Lists **/
	/** Front End Developer can use these when passing column names for orderby lists **/
	public static interface ChargesColumnNames {

		public static String CHARGE_CODE = "charge.chargeCode";
		public static String CHARGE_DESCRIPTION = "charge.chargeDescription";
		public static String CHARGE_CATEGORY_VALUE = "charge.chargeCategoryValue";
		public static String APPLIES_TO_INFANTS = "charge.appliesToInfants";
		public static String REFUNDABLE_CHARGE_FLAG = "charge.refundableChargeFlag";
		public static String STATUS = "charge.status";
		public static String CHARGE_CATEGORY_CODE = "charge.chargeCategoryCode";
		public static String CHARGE_GROUP_CODE = "charge.chargeGroupCode";

	}

	/** Charge Rates Column Names to pass in OrderBy Lists **/
	public static interface ChargeRatesColumnNames {

		public static String CHARGE_RATE_CODE = "chargerates.chargeRateCode";
		public static String CHARGE_EFFECTIVE_FROM_DATE = "chargerates.chargeEffectiveFromDate";
		public static String CHARGE_EFFECTIVE_TO_DATE = "chargerates.chargeEffectiveToDate";
		public static String CHARGE_VALUE_PERCENTAGE = "chargerates.chargeValuePercentage";
		public static String VALUE_IN_LOCAL_CURRENCY = "chargerates.valueInLocalCurrency";
		public static String CHARGE_VALUE_MIN = "chargerates.chargeValueMin";
		public static String CHARGE_VALUE_MAX = "chargerates.chargeValueMax";
		public static String CHARGE_BASIS = "chargerates.chargeBasis";
		public static String STATUS = "chargerates.status";

	}

	/** Handling charges is maintaind by HibernateQueryParser therefore the alias name is not necessary **/
	public static interface HandlingChargesColumnNames {

		public static String HANDLING_CHARGE_ID = "handlingChargId";
		public static String HANDLING_CHARGE_AMOUNT = "handlingChargeAmount";
		public static String STATUS = "status";
		public static String AIRPORT_CODE = "airportCode";
		public static String VALID_SEGMENT_CODE = "validSegmentCode";
	}

	/** For Charges Applicapable for OND Query **/
	public static interface ONDChargesColumnNames {

		public static String OND_CODE = "ond.ONDCode";
		public static String ORIGIN_AIRPORT_CODE = "ond.originAirportCode";
		public static String DESTINATION_AIRPORT_CODE = "ond.destinationAirportCode";

	}

}
