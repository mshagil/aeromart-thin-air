package com.isa.thinair.airpricing.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airpricing.api.dto.CCChargeTO;
import com.isa.thinair.airpricing.api.model.PaymentGatewayWiseCharges;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface PaymentGatewayWiseChargesDAO {
	
	public PaymentGatewayWiseCharges getPaymentGatewayWiseCharges(int gatewayId);
	
	public PaymentGatewayWiseCharges getUniquePaymentGatewayWiseCharge(int chargeId);
	
	public void savePaymentGatewayWiseCharges(PaymentGatewayWiseCharges paymentGatewayWiseCharges) throws ModuleException;
	
	public void delete(PaymentGatewayWiseCharges paymentGatewayWiseCharges);

	public Page<CCChargeTO> getPaymentGatewayWiseCharges(int startrec, int numofrecs, List<ModuleCriterion> criteria);
}
