package com.isa.thinair.airpricing.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.FareRuleComment;
import com.isa.thinair.airpricing.api.model.FareRuleFee;
import com.isa.thinair.airpricing.api.model.FareRulePax;
import com.isa.thinair.airpricing.api.model.Proration;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.audit.AuditAirpricing;
import com.isa.thinair.airpricing.core.persistence.dao.FareDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * 
 * @author m.Rikaz
 * 
 */

public class SplitFareBySalesUtil {

	private static Log log = LogFactory.getLog(SplitFareBySalesUtil.class);
	
	public static FareRule copyExistingFareRuleAsAdhoc(Integer fareRuleId) throws ModuleException {
		try {

			FareRule oldFareRule = getFareRuleDAO().getFareRule(fareRuleId);
			FareRule existFareRuleObj = oldFareRule;
			Collection<FareRulePax> setPaxDetails = existFareRuleObj.getPaxDetails();
			if (setPaxDetails != null) {
				existFareRuleObj.setPaxDetails(null);
				Iterator<FareRulePax> iterPaxDetails = setPaxDetails.iterator();
				while (iterPaxDetails.hasNext()) {
					FareRulePax mDef = (FareRulePax) iterPaxDetails.next();
					FareRulePax def = new FareRulePax();
					def.setApplyCNXCharges(mDef.getApplyCNXCharges());
					def.setApplyFares(mDef.getApplyFares());
					def.setApplyModCharges(mDef.getApplyModCharges());
					def.setCreatedBy(mDef.getCreatedBy());
					def.setCreatedDate(new Date());
					def.setModifiedBy(mDef.getModifiedBy());
					def.setModifiedDate(new Date());
					def.setPaxType(mDef.getPaxType());
					def.setRefundableFares(mDef.getRefundableFares());
					def.setNoShowChrge(mDef.getNoShowChrge());
					def.setNoShowChgBasis(mDef.getNoShowChgBasis());
					def.setNoShowChgBreakPoint(mDef.getNoShowChgBreakPoint());
					def.setNoShowChgBoundary(mDef.getNoShowChgBoundary());
					def.setNoShowChrgeInLocal(mDef.getNoShowChrgeInLocal());
					existFareRuleObj.addPaxDetails(def);
				}
			}

			if (existFareRuleObj.getFees() != null && existFareRuleObj.getFees().size() > 0) {

				Collection<FareRuleFee> setFareRuleFees = existFareRuleObj.getFees();
				existFareRuleObj.setFees(null);
				Iterator<FareRuleFee> itrFareRuleFees = setFareRuleFees.iterator();

				while (itrFareRuleFees.hasNext()) {
					FareRuleFee existFareRuleFee = (FareRuleFee) itrFareRuleFees.next();

					FareRuleFee fareRuleFee = new FareRuleFee();

					fareRuleFee.setChargeAmount(existFareRuleFee.getChargeAmount());
					fareRuleFee.setChargeAmountInLocal(existFareRuleFee.getChargeAmountInLocal());
					fareRuleFee.setChargeAmountType(existFareRuleFee.getChargeAmountType());
					fareRuleFee.setChargeType(existFareRuleFee.getChargeType());
					fareRuleFee.setCompareAgainst(existFareRuleFee.getCompareAgainst());
					fareRuleFee.setMaximumPerChargeAmt(existFareRuleFee.getMaximumPerChargeAmt());
					fareRuleFee.setMinimumPerChargeAmt(existFareRuleFee.getMinimumPerChargeAmt());
					fareRuleFee.setStatus(existFareRuleFee.getStatus());
					fareRuleFee.setTimeType(existFareRuleFee.getTimeType());
					fareRuleFee.setTimeValue(existFareRuleFee.getTimeValue());

					existFareRuleObj.addFee(fareRuleFee);
				}
			} else {
				existFareRuleObj.setFees(null);
			}

			Set<Integer> setVisibleChannelIds = existFareRuleObj.getVisibleChannelIds();
			if (setVisibleChannelIds != null) {
				Set<Integer> visibChannels = new HashSet<Integer>();
				Iterator<Integer> iterVisibChannels = setVisibleChannelIds.iterator();
				while (iterVisibChannels.hasNext()) {
					visibChannels.add(iterVisibChannels.next());
				}

				existFareRuleObj.setVisibleChannelIds(visibChannels);

			}
			Set<String> setAgentCodes = existFareRuleObj.getVisibileAgentCodes();
			if (setAgentCodes != null) {
				Set<String> agentCodes = new HashSet<String>();
				Iterator<String> iterAgentCode = setAgentCodes.iterator();
				while (iterAgentCode.hasNext()) {
					agentCodes.add(iterAgentCode.next());
				}
				existFareRuleObj.setVisibileAgentCodes(agentCodes);
			}

			Set<String> existingPOS = existFareRuleObj.getPointOfSale();
			if (existingPOS != null) {
				Set<String> newPOS = new HashSet<String>();
				for (String pos : existingPOS) {
					newPOS.add(pos);
				}
				existFareRuleObj.setPointOfSale(newPOS);
			}

			Set<Integer> flexiCodes = existFareRuleObj.getFlexiCodes();
			if (flexiCodes != null) {
				Set<Integer> flexiCodesNew = new HashSet<Integer>();
				Iterator<Integer> flexiCodeIte = flexiCodes.iterator();
				while (flexiCodeIte.hasNext()) {
					flexiCodesNew.add(flexiCodeIte.next());
				}
				existFareRuleObj.setFlexiCodes(flexiCodesNew);
			}
			// }

			Set<FareRuleComment> fareRuleComments = existFareRuleObj.getFareRuleComments();
			if (fareRuleComments != null) {
				Set<FareRuleComment> fareRuleCommentsNew = null;
				if (fareRuleComments.size() > 0) {
					fareRuleCommentsNew = new HashSet<>();
					for (FareRuleComment currentFareRuleComment : fareRuleComments) {
						FareRuleComment newFRComment = new FareRuleComment();
						newFRComment.setComments(currentFareRuleComment.getComments());
						newFRComment.setLanguageCode(currentFareRuleComment.getLanguageCode());
						newFRComment.setFareRule(existFareRuleObj);
						fareRuleCommentsNew.add(newFRComment);
					}
				}
				existFareRuleObj.setFareRuleComments(fareRuleCommentsNew);

			}

			existFareRuleObj.setVersion(-1);
			existFareRuleObj.setFareRuleId(0);
			existFareRuleObj.setFareRuleCode(null);
			existFareRuleObj.setFareRuleComments(null);

			getFareRuleDAO().saveFareRule(existFareRuleObj);

			return existFareRuleObj;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error :: splitting fares " + e.getCause());
			throw new ModuleException("airpricing.fare.split.sales.validity", "airpricing.desc");
		}
	}

	/**
	 * splitFares
	 * 
	 * @param oldFareId
	 * @param adhocFareRuleId
	 * @param currentDate
	 * @param updatedFareObj
	 *            required on fare modification flow
	 * @param splitFare
	 *            denotes whether to split or not by sales validity
	 * @return
	 */
	public static void splitFares(Integer oldFareId, Integer adhocFareRuleId, Date date, Fare updatedFareObj, boolean splitFare,
			UserPrincipal userPrincipal) throws ModuleException {

		try {
			boolean saveNewFareObj = false;
			Fare exstOndFare = getFareDAO().getFare(oldFareId);
			boolean fareSplitImmediateEffectEnabled = AppSysParamsUtil.isSplitFareImmediateEffectEnable();
			int cutofTimeInMinutes = AppSysParamsUtil.getSplitFareImmediateEnableCutofTime();
			Date currentZuluTime = CalendarUtil.getCurrentSystemTimeInZulu();

			if (splitFare) {
				if (updatedFareObj != null) {
					updatedFareObj.setVersion(-1);
					updatedFareObj.setFareId(0);
					if (fareSplitImmediateEffectEnabled) {
						updatedFareObj
								.setSalesEffectiveFrom(CalendarUtil.add(currentZuluTime, 0, 0, 0, 0, cutofTimeInMinutes, 0));
					} else {
						updatedFareObj
								.setSalesEffectiveFrom(CalendarUtil.getStartTimeOfDate(CalendarUtil.addDateVarience(date, 1)));
					}
					Collection<Proration> prorations = new ArrayList<Proration>();
					prorations = updatedFareObj.getProrations();
					updatedFareObj.setProrations(prorations);
				} else {
					updatedFareObj = new Fare();

					copyFareAttributes(exstOndFare, updatedFareObj);

					updatedFareObj.setVersion(-1);
					updatedFareObj.setFareId(0);

					if (fareSplitImmediateEffectEnabled) {
						updatedFareObj
								.setSalesEffectiveFrom(CalendarUtil.add(currentZuluTime, 0, 0, 0, 0, cutofTimeInMinutes, 0));
					} else {
						updatedFareObj
								.setSalesEffectiveFrom(CalendarUtil.getStartTimeOfDate(CalendarUtil.addDateVarience(date, 1)));
					}

					saveNewFareObj = true;
				}

				boolean isSalesEffectiveNotExpired = CalendarUtil.isGreaterThan(exstOndFare.getSalesEffectiveTo(), date);
				// Set sales validity end date as current date only if sales validity not expired
				if (isSalesEffectiveNotExpired || fareSplitImmediateEffectEnabled) {
					if (fareSplitImmediateEffectEnabled) {
						exstOndFare.setSalesEffectiveTo(CalendarUtil.add(currentZuluTime, 0, 0, 0, 0, cutofTimeInMinutes, -1));
					} else {
						exstOndFare.setSalesEffectiveTo(CalendarUtil.getEndTimeOfDate(date));
					}
				}
			}

			// removing the link fare flags for the fare that sales validity ends today
			if (Fare.FARE_DEF_TYPE_LINK.equals(exstOndFare.getFareDefType())) {
				exstOndFare.setFareDefType(Fare.FARE_DEF_TYPE_NORMAL);
				exstOndFare.setMasterFareID(null);
			}

			// removing the master fare flags for the fare that sales validity ends today
			if (Fare.FARE_DEF_TYPE_MASTER.equals(exstOndFare.getFareDefType())) {
				exstOndFare.setMasterFareRefCode(null);
				exstOndFare.setMasterFarePercentage(null);
				exstOndFare.setFareDefType(Fare.FARE_DEF_TYPE_NORMAL);
			}

			exstOndFare.setFareRuleId(adhocFareRuleId);

			if (splitFare) {
				AuditAirpricing.doAudit(exstOndFare, userPrincipal);
			}

			getFareDAO().saveFare(exstOndFare);

			if (saveNewFareObj) {
				getFareDAO().saveFare(updatedFareObj);
				AuditAirpricing.doAudit(updatedFareObj, userPrincipal, true);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error :: splitting fares " + e.getCause());
			throw new ModuleException("airpricing.fare.split.sales.validity", "airpricing.desc");
		}
	}

	private static FareRuleDAO getFareRuleDAO() {
		return (FareRuleDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_RULE_DAO);
	}

	private static FareDAO getFareDAO() {
		return (FareDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_DAO);
	}

	private static void copyFareAttributes(Fare fromFare, Fare toFare) {

		toFare.setBookingCode(fromFare.getBookingCode());
		toFare.setChildFare(fromFare.getChildFare());
		toFare.setChildFareInLocal(fromFare.getChildFareInLocal());
		toFare.setChildFareType(fromFare.getChildFareType());
		toFare.setEffectiveFromDate(fromFare.getEffectiveFromDate());
		toFare.setEffectiveToDate(fromFare.getEffectiveToDate());
		toFare.setFareAmount(fromFare.getFareAmount());
		toFare.setFareAmountInLocal(fromFare.getFareAmountInLocal());
		toFare.setFareDefType(fromFare.getFareDefType());
		toFare.setFareId(fromFare.getFareId());
		toFare.setFareRuleId(fromFare.getFareRuleId());
		toFare.setInfantFare(fromFare.getInfantFare());
		toFare.setInfantFareInLocal(fromFare.getInfantFareInLocal());
		toFare.setInfantFareType(fromFare.getInfantFareType());
		toFare.setLocalCurrencyCode(fromFare.getLocalCurrencyCode());
		toFare.setLocalCurrencySelected(fromFare.isLocalCurrencySelected());
		toFare.setMasterFareID(fromFare.getMasterFareID());
		toFare.setMasterFarePercentage(fromFare.getMasterFarePercentage());
		toFare.setMasterFareRefCode(fromFare.getMasterFareRefCode());
		toFare.setOndCode(fromFare.getOndCode());
		toFare.setReturnEffectiveFromDate(fromFare.getReturnEffectiveFromDate());
		toFare.setReturnEffectiveToDate(fromFare.getReturnEffectiveToDate());
		toFare.setSalesEffectiveFrom(fromFare.getSalesEffectiveFrom());
		toFare.setSalesEffectiveTo(fromFare.getSalesEffectiveTo());
		toFare.setStatus(fromFare.getStatus());
		toFare.setUploaded(fromFare.isUploaded());
		toFare.setVersion(fromFare.getVersion());
		toFare.setModifyToSameFare(fromFare.getModifyToSameFare());

		if (fromFare.getProrations() != null && fromFare.getProrations().size() > 0) {
			Collection<Proration> prorations = new LinkedHashSet<Proration>();
			for (Proration proration : fromFare.getProrations()) {
				Proration newProration = copyProration(proration);
				newProration.setFare(toFare);
				prorations.add(newProration);
			}
			toFare.setProrations(prorations);
		}
	}

	private static Proration copyProration(Proration proration) {
		Proration newProration = new Proration();
		newProration.setProration(proration.getProration());
		newProration.setSegmentCode(proration.getSegmentCode());
		newProration.setVersion(-1);
		return newProration;
	}

}
