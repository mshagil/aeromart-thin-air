package com.isa.thinair.airpricing.core.persistence.hibernate;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;

import com.isa.thinair.airpricing.api.criteria.FlexiRuleSearchCriteria;
import com.isa.thinair.airpricing.api.model.FlexiRule;
import com.isa.thinair.airpricing.core.persistence.dao.FlexiRuleDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * BHive: FlexiRule module
 * 
 * @author Dhanya
 */

public class FlexiRuleDAOImpl extends PlatformBaseHibernateDaoSupport implements FlexiRuleDAO {

	private Log log = LogFactory.getLog(FlexiRuleDAOImpl.class);

	/**
	 * method to save a flexiRule
	 */
	public void saveFlexiRule(FlexiRule flexiRule) {
		if (log.isDebugEnabled()) {
			log.debug("saveFlexiRule for id" + flexiRule.getFlexiCode());
		}
		hibernateSaveOrUpdate(flexiRule);

	}

	/**
	 * method to remove a flexiRule for a given flexiRuleId
	 */
	public void removeFlexiRule(int flexiRuleId) {

		if (log.isDebugEnabled()) {
			log.debug("removeCharge for id" + flexiRuleId);
		}
		Object flexiRule = load(FlexiRule.class, flexiRuleId);
		delete(flexiRule);

	}

	/**
	 * method to get Flexi Rules with paging and criteria using HQL joins.
	 * 
	 * @return Page
	 * @param FlexiRuleSearchCriteria
	 * @param startIndex
	 * @param pageSize
	 * @param orderbyfields
	 */
	public Page getFlexiRules(FlexiRuleSearchCriteria flexiRuleSearchCriteria, int startIndex, int pageSize, List<String> orderbyfields)
			throws ModuleException {

		String s[] = getHQLForFlexiRules(flexiRuleSearchCriteria, orderbyfields);
		String hql = s[0];
		String hqlCount = s[1];

		Query query = getSession().createQuery(hql).setFirstResult(startIndex).setMaxResults(pageSize);
		Query queryCount = getSession().createQuery(hqlCount);
		try {
			if (flexiRuleSearchCriteria.getFlexiCode() != null) {
				query.setParameter("flexicode", flexiRuleSearchCriteria.getFlexiCode());
				queryCount.setParameter("flexicode", flexiRuleSearchCriteria.getFlexiCode());
			}
			if (flexiRuleSearchCriteria.getDepDateFrom() != null) {
				query.setParameter("depStartDate", flexiRuleSearchCriteria.getDepDateFrom());
				queryCount.setParameter("depStartDate", flexiRuleSearchCriteria.getDepDateFrom());
			}
			if (flexiRuleSearchCriteria.getDepDateTo() != null) {
				query.setParameter("depEndDate", flexiRuleSearchCriteria.getDepDateTo());
				queryCount.setParameter("depEndDate", flexiRuleSearchCriteria.getDepDateTo());
			}
			if (flexiRuleSearchCriteria.getSalesDateFrom() != null) {
				query.setParameter("salesStartDate", flexiRuleSearchCriteria.getSalesDateFrom());
				queryCount.setParameter("salesStartDate", flexiRuleSearchCriteria.getSalesDateFrom());
			}
			if (flexiRuleSearchCriteria.getSalesDateTo() != null) {
				query.setParameter("salesEndDate", flexiRuleSearchCriteria.getSalesDateTo());
				queryCount.setParameter("salesEndDate", flexiRuleSearchCriteria.getSalesDateTo());
			}

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
		log.debug("Page getFlexiRules with criteria");

		return new Page(queryCount.list().size(), startIndex, startIndex + pageSize, query.list());
	}

	private String[] getHQLForFlexiRules(FlexiRuleSearchCriteria flexiRuleSearchCriteria, List<String> orderbyfields) {
		String hqlCount = "select distinct flexi.flexiCode from FlexiRule flexi left join flexi.flexiRuleRates flexirates";

		String hql = "select distinct flexi from FlexiRule flexi left join flexi.flexiRuleRates flexirates";

		String criteriaHql = "";

		hql = hql + criteriaHql;
		hqlCount = hqlCount + criteriaHql;

		String strClause = "";
		/** check for order by list **/

		/** If flexi Code has been selected in the search criteria **/
		if (flexiRuleSearchCriteria.getFlexiCode() != null) {
			strClause = criteriaHql.equals("") ? " WHERE " : " AND";
			criteriaHql = criteriaHql + strClause + "flexi.flexiCode=:flexicode";
		}

		if (flexiRuleSearchCriteria.getDepDateFrom() != null && flexiRuleSearchCriteria.getDepDateTo() != null) {
			strClause = criteriaHql.equals("") ? " WHERE " : " AND";
			criteriaHql = criteriaHql + strClause
					+ " (flexirates.depDateFrom>=:depStartDate AND flexirates.depDateTo <= :depEndDate )";
		} else if (flexiRuleSearchCriteria.getDepDateFrom() != null && flexiRuleSearchCriteria.getDepDateTo() == null) {
			strClause = criteriaHql.equals("") ? " WHERE " : " AND";
			criteriaHql = criteriaHql + strClause + " flexirates.depDateFrom>=:depStartDate ";
		} else if (flexiRuleSearchCriteria.getDepDateFrom() == null && flexiRuleSearchCriteria.getDepDateTo() != null) {
			strClause = criteriaHql.equals("") ? " WHERE " : " AND";
			criteriaHql = criteriaHql + strClause + " flexirates.depDateTo <= :depEndDate ";
		}

		if (flexiRuleSearchCriteria.getSalesDateFrom() != null && flexiRuleSearchCriteria.getSalesDateTo() != null) {
			strClause = criteriaHql.equals("") ? " WHERE " : " AND";
			criteriaHql = criteriaHql + strClause
					+ " (flexirates.salesDateFrom>=:salesStartDate AND flexirates.salesDateTo <= :salesEndDate )";
		} else if (flexiRuleSearchCriteria.getSalesDateFrom() != null && flexiRuleSearchCriteria.getSalesDateTo() == null) {
			strClause = criteriaHql.equals("") ? " WHERE " : " AND";
			criteriaHql = criteriaHql + strClause + " flexirates.salesDateFrom>=:salesStartDate";
		} else if (flexiRuleSearchCriteria.getSalesDateFrom() == null && flexiRuleSearchCriteria.getSalesDateTo() != null) {
			strClause = criteriaHql.equals("") ? " WHERE " : " AND";
			criteriaHql = criteriaHql + strClause + " flexirates.salesDateTo <= :salesEndDate )";
		}

		if (orderbyfields != null) {
			String orderbyHql = "";
			Iterator<String> iter = orderbyfields.iterator();
			while (iter.hasNext()) {
				if (orderbyHql.equals(""))
					orderbyHql = " order by " + iter.next().toString();
				else
					orderbyHql = orderbyHql + ", " + iter.next().toString();
			}
			hql = hql + " " + orderbyHql + " asc";
		}

		return new String[] { hql, hqlCount };
	}

}
