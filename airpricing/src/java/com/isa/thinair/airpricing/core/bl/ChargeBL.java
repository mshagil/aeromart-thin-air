/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airpricing.core.bl;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.ChargeQuoteUtils;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airmaster.api.dto.AirCraftModelSeatsDTO;
import com.isa.thinair.airmaster.api.dto.StationDTO;
import com.isa.thinair.airmaster.api.model.AirCraftModelSeats;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.ChargeDTO;
import com.isa.thinair.airpricing.api.dto.CnxModPenChargeDTO;
import com.isa.thinair.airpricing.api.dto.CnxModPenServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.PaxCnxModPenChargeDTO;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.SimplifiedChargeDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedFlightSegmentDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedPaxDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.MCO;
import com.isa.thinair.airpricing.api.model.MCOServices;
import com.isa.thinair.airpricing.api.model.OriginDestination;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airpricing.api.util.LocalCurrencyUtil;
import com.isa.thinair.airpricing.core.audit.AuditAirpricing;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.dao.ONDDAO;
import com.isa.thinair.airpricing.core.persistence.dao.PaymentGatewayWiseChargesDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.airpricing.core.util.SplitChargeBySalesUtil;
import com.isa.thinair.airproxy.api.utils.DateUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Class to implement the bussiness logic related to Airpricing
 * 
 * @author Byorn
 */

public class ChargeBL {

	private final Log log = LogFactory.getLog(getClass());

	private final ChargeDAO chargedao;

	private final ONDDAO onddao;

	private final PaymentGatewayWiseChargesDAO paymentGatewayWiseChargesDAO;

	private final ChargeJdbcDAO chargejdbcdao;

	public final static int SERIAL_NO_MAX_LENGTH = 8;

	public ChargeBL() {

		chargedao = (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);
		onddao = (ONDDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.OND_DAO);
		chargejdbcdao = getChargeJdbcDAO();
		paymentGatewayWiseChargesDAO = (PaymentGatewayWiseChargesDAO) AirpricingUtils.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.PAYMENTGATEWAY_WISE_CHARGE_DAO);
	}

	public ChargeTemplate copyChargeTemplate(int srcTemplateID, String newTemplateCode, Collection<SeatCharge> seatCharges)
			throws ModuleException {
		// DefaultServiceResponse res = new DefaultServiceResponse(false);
		ChargeTemplate chargeTemplate = chargedao.getChargeTemplate(srcTemplateID);

		ChargeTemplate newChargeTemplate = new ChargeTemplate();
		newChargeTemplate.setTemplateCode(newTemplateCode);
		newChargeTemplate.setDefaultChargeAmount(chargeTemplate.getDefaultChargeAmount());
		newChargeTemplate.setDescription(chargeTemplate.getDescription());
		newChargeTemplate.setModelNo(chargeTemplate.getModelNo());
		newChargeTemplate.setStatus(chargeTemplate.getStatus());
		newChargeTemplate.setUserNote(chargeTemplate.getUserNote());

		Set<SeatCharge> newSeatCharges = new HashSet<SeatCharge>();
		Iterator<SeatCharge> Itr = seatCharges.iterator();
		while (Itr.hasNext()) {
			SeatCharge newSeatCharge = Itr.next();

			SeatCharge seatCharge = new SeatCharge();

			seatCharge.setChargeAmount(newSeatCharge.getChargeAmount());
			seatCharge.setSeatId(newSeatCharge.getSeatId());
			seatCharge.setStatus(newSeatCharge.getStatus());
			seatCharge.setTemplate(newChargeTemplate);
			newSeatCharges.add(seatCharge);
		}
		newChargeTemplate.setSeatCharges(newSeatCharges);
		chargedao.saveTemplate(newChargeTemplate);
		return newChargeTemplate;
	}

	public void saveSeatChargeTemplate(ChargeTemplate chargeTemplate, Collection<SeatDTO> seatChgDTO, boolean isDefAmountChanged, UserPrincipal userPrincipal) throws ModuleException {
		try {
			Set<SeatCharge> seatCharges = new HashSet<SeatCharge>();
			ChargeTemplate addhoc =null;
			Map<Integer, String> newSeatCharges = new HashMap<Integer, String>();
			SeatCharge seatChg = null;
			for (SeatDTO seatDTO : seatChgDTO) {
				seatChg = new SeatCharge();
				seatChg.setChargeId(seatDTO.getChargeID());
				seatChg.setSeatId(seatDTO.getSeatID());
				seatChg.setLocalCurrencyAmount(seatDTO.getChargeAmount());
				seatChg.setChargeAmount(LocalCurrencyUtil.amountInBaseCurrency(chargeTemplate.getChargeLocalCurrencyCode(),
						seatDTO.getChargeAmount())); // changed to local curr
				seatChg.setStatus(seatDTO.getStatus());
				seatChg.setVersion(seatDTO.getVersion());
				if (seatDTO.isEditedSeat()) {
					newSeatCharges.put(seatDTO.getSeatID(), seatDTO.getChargeAmount() + "_" + seatDTO.getStatus());
				}
				seatChg.setTemplate(chargeTemplate);
				seatCharges.add(seatChg);
			}
			chargeTemplate.setSeatCharges(seatCharges);

			if (chargeTemplate.getSeatCharges() == null || chargeTemplate.getSeatCharges().isEmpty()) {
				
				if(chargeTemplate.getVersion()<0){ //save
					setDefaultCharges(chargeTemplate);
				}else{ // edit	
					ChargeTemplate chrgTempBeforeEdit = chargedao.getChargeTemplate(chargeTemplate.getTemplateId());
					seatCharges  = chrgTempBeforeEdit.getSeatCharges();
					if(isDefAmountChanged){
						addhoc = new ChargeTemplate();
						
						addhoc.setModelNo(chargeTemplate.getModelNo());
						addhoc.setTemplateCode(chargeTemplate.getTemplateCode()+"_"+DateUtil.formatDate(new Date(), "HH:mm:ss"));
						addhoc.setDescription(chargeTemplate.getDescription());
						addhoc.setDefaultChargeAmount(chrgTempBeforeEdit.getDefaultChargeAmount()); // set previous def amount
						addhoc.setUserNote(chargeTemplate.getUserNote());
						addhoc.setStatus(chargeTemplate.getStatus());
						addhoc.setChargeLocalCurrencyCode(chargeTemplate.getChargeLocalCurrencyCode());
								
						
						for(SeatCharge seatCharge :seatCharges){
							seatCharge.setTemplate(addhoc);
						}	
						setDefaultCharges(chargeTemplate);// create new template with new def amount
						addhoc.setSeatCharges(seatCharges);
					}else{// edit mode not change default amount
						chargeTemplate.setSeatCharges(seatCharges);
					}					
				}
			}
			
			if(addhoc != null){
				addhoc =(ChargeTemplate) PricingUtils.setUserDetails(addhoc, userPrincipal);
				AuditAirpricing.doAuditSeatMapAddOrModifyTemplate(chargeTemplate,userPrincipal);
				chargedao.saveTemplate(addhoc);
			}
			chargedao.saveTemplate(chargeTemplate);
			
			Collection<Integer> colSegIds = chargejdbcdao.getSegmentIdsForChargeTemplate(chargeTemplate.getTemplateId());

			SeatMapBD oSeatMapBD = AirpricingUtils.getSeatMapBD();

			if (chargeTemplate.getVersion() != 0 && colSegIds.size() > 0) {
				ServiceResponce sr = oSeatMapBD.assignFlightSeatCharge(colSegIds, newSeatCharges);
			}

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}
	
	private void setDefaultCharges(ChargeTemplate chargeTemplate) throws ModuleException{
		try{
		AirCraftModelSeatsDTO airCraftModelSeatsDTO = getAirCraftBD().getAirCraftModelSeats(chargeTemplate.getModelNo());
		chargeTemplate.setSeatCharges(getDefaultSeatChargeForModel(airCraftModelSeatsDTO.getAirCraftModelSeats(),
			chargeTemplate));
		
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@SuppressWarnings("unused")
	public void saveTemplate(ChargeTemplate chargeTemplate, Integer flightSegId, String modelNo) throws ModuleException {
		try {
			Boolean isNew = false;
			if (chargeTemplate.getSeatCharges() == null || chargeTemplate.getSeatCharges().isEmpty()) {
				// If no charges assigned, set the default charges
				AirCraftModelSeatsDTO airCraftModelSeatsDTO = getAirCraftBD().getAirCraftModelSeats(chargeTemplate.getModelNo());
				chargeTemplate.setSeatCharges(getDefaultSeatChargeForModel(airCraftModelSeatsDTO.getAirCraftModelSeats(),
						chargeTemplate));
				isNew = true;

			}
			Collection<Integer> colSegIds = new ArrayList<Integer>();

			if (flightSegId != null) {
				colSegIds.add(flightSegId);
			} else {
				colSegIds = chargejdbcdao.getSegmentIdsForChargeTemplate(chargeTemplate.getTemplateId());
			}

			SeatMapBD oSeatMapBD = AirpricingUtils.getSeatMapBD();

			// remove calling from create
			if (!isNew && colSegIds.size() > 0) {
				ServiceResponce sr = oSeatMapBD.assignFlightSeatCharges(colSegIds, chargeTemplate.getTemplateId(), modelNo,
						chargeTemplate);

			}

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	public ChargeTemplate getTemplateId(String templateCode) {
		return chargedao.getTemplateId(templateCode);
	}

	/**
	 * BL Method when creating an Charge (1) If any New OND Exists it will be saved to the Database prior to saving the
	 * CHARGE object. void
	 * 
	 * @param charge
	 * @throws ModuleException
	 */
	public void createCharge(Charge charge) throws ModuleException {
		saveNewONDs(charge.getOndCodes());

		try {
			chargedao.removeChargePos(charge.getChargeCode());
			chargedao.removeChargeAgents(charge.getChargeCode());
			chargedao.removeApplicableCharges(charge.getChargeCode());
			String baseCurrency = AppSysParamsUtil.getBaseCurrency();
			Set<ChargeRate> chargeRates = new HashSet<ChargeRate>();

			if (charge != null) {

				Collection<ChargeRate> chargeRatesCol = charge.getChargeRates();
				if (chargeRatesCol != null && chargeRatesCol.size() > 0) {
					Iterator<ChargeRate> itr = chargeRatesCol.iterator();
					ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
					while (itr.hasNext()) {
						ChargeRate chargeRate = itr.next();
						if (chargeRate != null) {
							String currencyCode = chargeRate.getCurrencyCode();
							if (currencyCode != null && !currencyCode.equals(baseCurrency)) {
								if (chargeRate.getValueInLocalCurrency() != null) {
									BigDecimal valueInLocalCurrency = new BigDecimal(chargeRate.getValueInLocalCurrency()
											.doubleValue());

									BigDecimal valueInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
									CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(currencyCode);
									if (currExRate != null) {
										BigDecimal exchangeRate = currExRate.getExrateBaseToCurNumber();
										// Currency currency = currExRate.getCurrency();
										if (AppSysParamsUtil.isRoundupChargesWhenCalculatedInBaseCurrency()) {
											valueInBase = AccelAeroRounderPolicy.convertAndRoundToNearestCeilingInt(
													valueInLocalCurrency, exchangeRate, null, null);
										} else {
											valueInBase = AccelAeroRounderPolicy.convertAndRound(valueInLocalCurrency,
													exchangeRate, null, null);
										}
									}

									chargeRate.setChargeValuePercentage((valueInBase.doubleValue()));

								} else if (chargeRate.getChargeValuePercentage() != null) {
									Double definedValueInBase = chargeRate.getChargeValuePercentage();
									if (chargeRate.getChargeBasis().equals(ChargeRate.CHARGE_BASIS_V)) {
										chargeRate.setValueInLocalCurrency(definedValueInBase);
										BigDecimal valueInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
										CurrencyExchangeRate currExRate = AirpricingUtils.getCommonMasterBD().getExchangeRate(
												currencyCode, new Date());
										BigDecimal valueInLocalCurrency = new BigDecimal(chargeRate.getValueInLocalCurrency()
												.doubleValue());
										if (currExRate != null) {
											BigDecimal exchangeRate = currExRate.getExrateBaseToCurNumber();
											valueInBase = AccelAeroRounderPolicy.convertAndRound(valueInLocalCurrency,
													exchangeRate, null, null);
										}
										chargeRate.setChargeValuePercentage((valueInBase.doubleValue()));
									}
								}
							}
							chargeRates.add(chargeRate);
						}
					}
				}
			}

			if (chargeRates != null && chargeRates.size() > 0) {
				charge.setChargeRates(chargeRates);
			}
			chargedao.saveCharge(charge);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}

	/**
	 * BL Method when updating a Charge (1) When Updating too, if a new ond code updated it will be checked if it exists
	 * in the OND Master Table and *** if not it will be saved. void
	 * 
	 * @param charge
	 * @throws ModuleException
	 */
	public void updateCharge(Charge charge) throws ModuleException {
		saveNewONDs(charge.getOndCodes());

		try {
			chargedao.removeChargePos(charge.getChargeCode());
			chargedao.removeChargeAgents(charge.getChargeCode());
			chargedao.saveCharge(charge);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * Private Method that will check for any new ONDCode that has been entered. * If a new OND code exists it will be
	 * saved to the MasterTable. void
	 * 
	 * @param s
	 * @throws ModuleException
	 */
	public void saveNewONDs(Set<String> newOnds) throws ModuleException {

		if (newOnds != null) {
			/**
			 * See what ONDCodes exist in the database...getting the existing codes to the list *
			 */
			List<String> existingOnds = onddao.getONDsForCodes(newOnds);

			/**
			 * If the exisitingOnds have all the Onds the given from the front-end then do nothing, else find out witch
			 * ones need to be saved newly *
			 */
			if (!existingOnds.containsAll(newOnds)) {
				Iterator<String> newOndsIter = newOnds.iterator();
				while (newOndsIter.hasNext()) {
					String ondCode = newOndsIter.next().toString();
					/**
					 * seeing if the "front-end selected" for each ONDCode if it exists in the existingList of ONDS *
					 */
					/**
					 * if it does not exist check the next ondCode, else save it to the ONDTable *
					 */
					if (!existingOnds.contains(ondCode)) {
						OriginDestination originDestination = new OriginDestination();
						originDestination.setONDCode(ondCode);

						String origin = ondCode.substring(0, 3);
						int dstntnIndx = ondCode.length() - 3;
						String destination = ondCode.substring(dstntnIndx);

						if (!origin.equals("***")) {
							originDestination.setOriginAirportCode(origin);
						} else {
							originDestination.setOriginAirportCode(null);
						}
						if (!destination.equals("***")) {
							originDestination.setDestinationAirportCode(destination);
						} else {
							originDestination.setDestinationAirportCode(null);
						}

						try {
							onddao.saveOND(originDestination);
						} catch (CommonsDataAccessException e) {
							throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
						}

					}
				}
			}
		}
	}

	private Set<SeatCharge> getDefaultSeatChargeForModel(Collection<AirCraftModelSeats> seats, ChargeTemplate chargeTemplate)
			throws ModuleException {
		Set<SeatCharge> seatCharges = new HashSet<SeatCharge>();
		Iterator<AirCraftModelSeats> Itr = seats.iterator();
		while (Itr.hasNext()) {
			AirCraftModelSeats airCraftModelSeats = Itr.next();
			SeatCharge seatCharge = new SeatCharge();
			seatCharge.setLocalCurrencyAmount(chargeTemplate.getDefaultChargeAmount());
			seatCharge.setChargeAmount(LocalCurrencyUtil.amountInBaseCurrency(chargeTemplate.getChargeLocalCurrencyCode(),
					chargeTemplate.getDefaultChargeAmount()));
			seatCharge.setSeatId(airCraftModelSeats.getSeatID());
			seatCharge.setStatus(airCraftModelSeats.getStatus());
			seatCharge.setTemplate(chargeTemplate);
			seatCharges.add(seatCharge);
		}
		return seatCharges;
	}

	private ChargeJdbcDAO getChargeJdbcDAO() {
		return (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
	}

	private AircraftBD getAirCraftBD() {
		return AirpricingUtils.getAirCraftBD();
	}

	/**
	 * Retrieve Currencies and and exRates that are currently active and was updated.
	 * <p>
	 * The exRate is not used for any calculations but its extracted for a validation that is not in place yet.
	 * </P>
	 * 
	 * @throws ModuleException
	 */
	public void reCalcChargesInLocalCurr(Collection<CurrencyExchangeRate> currExchangeList) throws ModuleException {
		Date date = new Date();
		if (currExchangeList != null && !currExchangeList.isEmpty()) {
			Iterator<CurrencyExchangeRate> itrCurrencyList = currExchangeList.iterator();
			CurrencyExchangeRate currExRate;
			while (itrCurrencyList.hasNext()) {
				currExRate = itrCurrencyList.next();
				reCalculateCharges(currExRate.getCurrency().getCurrencyCode(), date);
			}
		}
	}

	/**
	 * Adjust Credit Limit of agents that uses local currency when the currency exchange rate is updated <br>
	 * author : dumindag
	 * 
	 * @param currencySpecifiedIn
	 * @param login
	 * @throws ModuleException
	 */
	private void reCalculateCharges(String currencySpecifiedIn, Date date) throws ModuleException {

		String baseCurrency = AppSysParamsUtil.getBaseCurrency();
		Collection<ChargeRate> chargeRatesCol = chargedao.getChargeRatesWithoutLocalCurrency(date, baseCurrency);

		Set<ChargeRate> chargeRates = new HashSet<ChargeRate>();
		Charge charge = new Charge();
		try {

			if (chargeRatesCol != null && chargeRatesCol.size() > 0) {
				Iterator<ChargeRate> itr = chargeRatesCol.iterator();
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				while (itr.hasNext()) {
					ChargeRate chargeRate = itr.next();

					if (chargeRate != null) {
						String currencyCode = chargeRate.getCurrencyCode();
						if (currencyCode != null && currencyCode.equals(currencySpecifiedIn)) {
							String chargeCode = chargeRate.getChargeCode();
							charge = chargedao.getCharge(chargeCode);
							if (chargeRate.getValueInLocalCurrency() != null) {
								BigDecimal valueInLocalCurrency = new BigDecimal(chargeRate.getValueInLocalCurrency()
										.doubleValue());

								BigDecimal valueInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
								CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(currencyCode);
								if (currExRate != null) {
									BigDecimal exchangeRate = currExRate.getExrateBaseToCurNumber();
									valueInBase = AccelAeroRounderPolicy.convertAndRound(valueInLocalCurrency, exchangeRate,
											null, null);
								}

								chargeRate.setValueInLocalCurrency(valueInBase.doubleValue());
								chargeRates.add(chargeRate);
							}
						}
					}
				}
			}

			if (charge != null && charge.getChargeRates() != null && charge.getChargeRates().size() > 0) {
				if (chargeRates != null && chargeRates.size() > 0) {
					charge.setChargeRates(chargeRates);
				}
				chargedao.saveCharge(charge);
			}

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}

	/**
	 * Update currecny exRate CHG_IN_LOCAL_CUR_UPD_STATUS to Success \ Fail
	 * 
	 * @param colCurrency
	 * @param status
	 * @throws ModuleException
	 */
	public void updateStatusForChargesInLocalCurr(Collection<CurrencyExchangeRate> colCurrExchangeRates, String status)
			throws ModuleException {
		if (colCurrExchangeRates != null && status != null && !colCurrExchangeRates.isEmpty()) {
			Iterator<CurrencyExchangeRate> itrCurrencyList = colCurrExchangeRates.iterator();
			CurrencyExchangeRate currExRate;
			while (itrCurrencyList.hasNext()) {
				currExRate = itrCurrencyList.next();
				currExRate.setChargesInLocalCurrUpdateStatus(status);
			}

			AirpricingUtils.getCommonMasterBD().saveOrUpdateCurrencyExRate(colCurrExchangeRates);
		}
	}

	/**
	 * Update Charges Automatically upon the Exchange Rate Change 1.Retrive the updated Currency Exchange List 2.Retrive
	 * the charges, and update the charge amount and finally update the Currency
	 * 
	 * @param userPrincipal
	 * @throws ModuleException
	 */
	public void chargeUpdateOnExchangeRateChange(UserPrincipal userPrincipal) throws ModuleException {

		ArrayList<CurrencyExchangeRate> updatedCurrencyExchangeList;
		ArrayList<Charge> applicableChargeList;
		BigDecimal newChargeAmount;

		try {
			if (AppSysParamsUtil.isUpdateTaxWithROEChanges()) {
				updatedCurrencyExchangeList = (ArrayList<CurrencyExchangeRate>) AirpricingUtils.getCurrencyDAO().getExchangeRatesForAutomatedUpdate(CalendarUtil.getCurrentSystemTimeInZulu(), InternalConstants.CHARGE_UPDATED_FOR_EXRATE_CHNG);
					//	.getExchangeRatesForAutomatedChargeUpdate(CalendarUtil.getCurrentSystemTimeInZulu());
				if (updatedCurrencyExchangeList.isEmpty()) {
					log.debug("No Currency Exchange Rates are Available for Automatic Charge Update");
				} else {
					for (CurrencyExchangeRate currencyExchangeRate : updatedCurrencyExchangeList) {
						applicableChargeList = (ArrayList<Charge>) chargedao
								.getApplicableChargesForUpdateOnExchangeRateChange(currencyExchangeRate.getCurrency()
										.getCurrencyCode());
						for (Charge charge : applicableChargeList) {
							for (ChargeRate chargeRate : charge.getChargeRates()) {
								if (!chargeRate.getStatus().equals(ChargeRate.Status_Active)
										|| CalendarUtil.isSameDay(new Date(), chargeRate.getSaleslToDate())
										|| !CalendarUtil.isGreaterThan(chargeRate.getSaleslToDate(), new Date())
										|| !chargeRate.getCurrencyCode().equals(
												currencyExchangeRate.getCurrency().getCurrencyCode())) {
									continue;
								}

								newChargeAmount = AccelAeroRounderPolicy.convertAndRound(AccelAeroCalculator
										.parseBigDecimal(chargeRate.getValueInLocalCurrency()), currencyExchangeRate
										.getExrateBaseToCurNumber(), currencyExchangeRate.getCurrency().getBoundryValue(),
										currencyExchangeRate.getCurrency().getBreakPoint());
								chargeRate.setChargeValuePercentage(newChargeAmount.doubleValue());
							}
						}
						updateCharges(applicableChargeList, userPrincipal);
						currencyExchangeRate.setChargesUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_ENABLED);
					}
					AirpricingUtils.getCurrencyDAO().saveOrUpdateCurrencyExRates(updatedCurrencyExchangeList);
				}
			}
		} catch (CommonsDataAccessException e) {
			log.error("ERROR-Auto Updating Charges upon Exchnage Rate Change Failed");
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * BL Method used to Update a collection of Charge objects. void
	 * 
	 * @param charges
	 * @param skipSplitCharge
	 * @param skipDuplicateChargeValidation
	 * @throws ModuleException
	 */
	public void updateCharges(Collection<Charge> charges, UserPrincipal userPrincipal) throws ModuleException {
		Iterator<Charge> iterator = charges.iterator();

		while (iterator.hasNext()) {
			Charge charge = iterator.next();

			ChargeDTO chargeDTO = getCharge(charge.getChargeCode());
			if (chargeDTO != null) {
				chargeDTO.setCharge(charge);
				updateCharge(chargeDTO, userPrincipal);

			} else {
				log.error("Charge was not found for Charge Code " + charge.getChargeCode());
			}
		}
	}

	/**
	 * MEthod to GET a chargeRate for the fare ID FareDTO
	 * 
	 * @param chargeRateId
	 * @return
	 * @throws ModuleException
	 */
	public ChargeDTO getCharge(String chargeCode) throws ModuleException {
		try {
			if (log.isDebugEnabled()) {
				log.debug("Getting a Charge for chargeCode:" + chargeCode);
			}

			ChargeDTO chargeDTO = null;
			// get the charge and set it to the chargedto
			Charge charge = chargedao.getCharge(chargeCode);
			if (charge != null) {
				chargeDTO = new ChargeDTO();
				chargeDTO.setCharge(charge);

				if (log.isDebugEnabled()) {
					log.debug("Getting Charge Success" + BeanUtils.nullHandler(charge));
				}
			}
			return chargeDTO;
		} catch (CommonsDataAccessException e) {
			log.error("ERROR getting Charge" + chargeCode);
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * BL Method when updating a Charge. void
	 * 
	 * @param Charge
	 * @throws
	 */
	public void updateCharge(ChargeDTO chargeDTO, UserPrincipal userPrincipal) throws ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("Updating a Charge:" + BeanUtils.nullHandler(chargeDTO.getCharge()));
		}

		Charge charge = chargeDTO.getCharge();
		splitChargeRatesBySalesValidity(charge);

		/** Save the Charge **/
		// createCharge(charge);
		chargedao.saveCharge(charge);
		AuditAirpricing.doAudit(charge, userPrincipal);

		log.debug("success saving Charge");

		log.debug("END OF Update Charge BL");
	}

	/**
	 * Split the charge based on the sales validity period. charge passed in as argument will be saved as a new charge
	 * effective from today and existing charge's sales validity period will be truncated to today
	 * 
	 * @param charge
	 * @throws ModuleException
	 */
	private void splitChargeRatesBySalesValidity(Charge charge) throws ModuleException {

		Date currentDate = new Date();
		Charge existChargeObj = chargedao.getCharge(charge.getChargeCode());
		ArrayList<ChargeRate> updatedChargeRateList = new ArrayList<ChargeRate>();
		ChargeRate updatedChargeRate;

		for (ChargeRate chargeRate : charge.getChargeRates()) {
			for (ChargeRate existChargeRate : existChargeObj.getChargeRates()) {
				if ((chargeRate.getChargeRateCode() == existChargeRate.getChargeRateCode())
						&& (chargeRate.getChargeValuePercentage() != existChargeRate.getChargeValuePercentage())) {

					boolean isSalesEndToDay = CalendarUtil.isSameDay(currentDate, existChargeRate.getSaleslToDate());
					boolean isSalesEffectiveNotExpired = CalendarUtil.isGreaterThan(existChargeRate.getSaleslToDate(),
							currentDate);
					boolean chargeAssignedForPnr = chargejdbcdao.isChargeAssignedForPnr(chargeRate.getChargeRateCode());
					boolean splitChargeRate = true;

					if (isSalesEndToDay || !isSalesEffectiveNotExpired || !chargeAssignedForPnr) {
						splitChargeRate = false;
					}
					if (splitChargeRate) {
						chargeRate
								.setSalesFromDate(CalendarUtil.getStartTimeOfDate(CalendarUtil.addDateVarience(currentDate, 1)));
						chargeRate.setChargeEffectiveFromDate(CalendarUtil.getStartTimeOfDate(CalendarUtil.addDateVarience(
								currentDate, 1)));
						chargeRate.setChargeRateCode(0);
						chargeRate.setVersion(-1);

						existChargeRate.setSaleslToDate(CalendarUtil.getEndTimeOfDate(currentDate));
						updatedChargeRate = new ChargeRate();
						SplitChargeBySalesUtil.copyChargeRateAttributes(existChargeRate, updatedChargeRate);
						updatedChargeRateList.add(updatedChargeRate);
					}
				}
			}
		}
		charge.getChargeRates().addAll(updatedChargeRateList);
	}

	public boolean isValidChargeExclusion(List<String> excludingCharges) throws ModuleException {

		if (excludingCharges != null && !excludingCharges.isEmpty()) {
			List<Charge> allExcludableCharges = chargedao.getExcludableCharges();
			if (allExcludableCharges == null || allExcludableCharges.isEmpty()) {
				throw new ModuleException("airPricing.invalid.charge.exclusion", "airpricing.desc");
			}

			List<String> allExclubleChgCodes = new ArrayList<String>();
			for (Charge charge : allExcludableCharges) {
				allExclubleChgCodes.add(charge.getChargeCode());
			}

			for (String exclChgCode : excludingCharges) {
				if (!allExclubleChgCodes.contains(exclChgCode)) {
					throw new ModuleException("airPricing.invalid.charge.exclusion", "airpricing.desc");
				}
			}

			return true;
		}

		return true;
	}

	public List<MCOServices> getMCOServices() throws ModuleException {
		return chargedao.getMCOServices();
	}
	
	public void saveMCO(MCO mco) throws ModuleException{
		chargedao.saveMCO(mco);
	}

	public String getPrintMCOText(String mcoNumber, String userID) throws ModuleException {

		String htmlContent = "";
		HashMap<String, Object> mcoDataMap = getMCODataMap(mcoNumber, userID);


		try {
			String templateName = "email/mco_print.xml.vm";
			StringWriter writer = new StringWriter();

			new TemplateEngine().writeTemplate(mcoDataMap, templateName, writer);

			htmlContent += writer.toString();

		} catch (Exception ex) {
			log.error("Error creating template", ex);
		}
		return htmlContent;
	}

	private HashMap<String, Object> getMCODataMap(String mcoNumber, String agentCode) throws ModuleException {
		HashMap<String, Object> mcoDataMap = chargedao.getMCORecord(mcoNumber, agentCode);
		
		if (mcoDataMap.isEmpty()) {
			throw new ModuleException("airpricing.empty.mco.results");
		}

		mcoDataMap.put("logoImageName", "LogoAni" + AppSysParamsUtil.getDefaultCarrierCode() + ".gif");
		mcoDataMap.put("ibeURL", CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
		mcoDataMap.put("subject", "Miscellaneous Charge Order");
		return mcoDataMap;
	}

	public void emailMCO(String mcoNumber, String agentCode) throws ModuleException {

		try {
			MessagingServiceBD messagingServiceBD = AirpricingUtils.getMessagingServiceBD();

			List<UserMessaging> messages = new ArrayList<UserMessaging>();
			HashMap<String, Object> emailDataMap = getMCODataMap(mcoNumber, agentCode);
			UserMessaging mcoOwner = new UserMessaging();
			mcoOwner.setFirstName((String) emailDataMap.get("firstName"));
			mcoOwner.setLastName((String) emailDataMap.get("lastName"));
			mcoOwner.setToAddres((String) emailDataMap.get("email"));

			messages.add(mcoOwner);

			Topic topic = new Topic();
			topic.setTopicParams(emailDataMap);
			topic.setLocale(null);
			topic.setTopicName("mco_email");
			topic.setAttachMessageBody(false);

			MessageProfile messageProfile = new MessageProfile();
			messageProfile.setUserMessagings(messages);
			messageProfile.setTopic(topic);

			messagingServiceBD.sendMessage(messageProfile);

		} catch (Exception ex) {
			throw new ModuleException("");
		}

	}
	
	public String getGeneratedMcoNumber() throws ModuleException {
		String airlineCode = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.BSP_TICKETING_AIRLINE_CODE);
		String formCode = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.MCO_FORM_CODE);
		String serialNo = getNextMCOSequenceNo();
		StringBuilder sb = new StringBuilder();
		sb.append(airlineCode);
		sb.append(formCode);
		sb.append(formatSerialNumber(serialNo));
		return sb.toString();

	}

	private static String formatSerialNumber(String serialNo) {
		while (serialNo.length() < SERIAL_NO_MAX_LENGTH) {
			serialNo = "0" + serialNo;
		}
		return serialNo;
	}

	private String getNextMCOSequenceNo() throws ModuleException {
		String nextSequence = chargedao.getNextMCONumberSQID();
		return nextSequence;

	}
	
	public ServiceTaxQuoteForTicketingRevenueRS quoteServiceTaxForTicketingRevenue(
			ServiceTaxQuoteForTicketingRevenueRQ serviceTaxQuoteForTktRevRQ) throws ModuleException {

		ServiceTaxQuoteForTicketingRevenueRS serviceTaxQuoteForTktRevRS = new ServiceTaxQuoteForTicketingRevenueRS();
		if ((serviceTaxQuoteForTktRevRQ.getPaxTypeWiseCharges() != null
				&& !serviceTaxQuoteForTktRevRQ.getPaxTypeWiseCharges().isEmpty()
				&& serviceTaxQuoteForTktRevRQ.getSimplifiedFlightSegmentDTOs() != null && !serviceTaxQuoteForTktRevRQ
				.getSimplifiedFlightSegmentDTOs().isEmpty())
				|| (serviceTaxQuoteForTktRevRQ.getPaxWiseExternalCharges() != null && !serviceTaxQuoteForTktRevRQ
						.getPaxWiseExternalCharges().isEmpty())) {

			Collection<QuoteChargesCriteria> criteriaCollection = generateCriteriaCollectionForTicketingRevenue(serviceTaxQuoteForTktRevRQ);

			HashMap<String, HashMap<String, QuotedChargeDTO>> ondWiseQuotedCharges = getChargeJdbcDAO().quoteCharges(
					criteriaCollection);

			boolean serviceTaxApplied = false;
			if (ondWiseQuotedCharges != null && ondWiseQuotedCharges.size() > 0 && ondWiseQuotedCharges.values() != null) {
				for (HashMap<String, QuotedChargeDTO> quotedCharges : ondWiseQuotedCharges.values()) {
					if (quotedCharges.values() != null && !quotedCharges.values().isEmpty()) {
						serviceTaxApplied = true;
						break;
					}
				}
			}

			if (serviceTaxApplied) {
				Collection<QuotedChargeDTO> quotedServiceTaxesForFirstOnd = null;
				Collection<String> quotedServiceTaxCodesForFirstOnd = null;
				for (Entry<String, HashMap<String, QuotedChargeDTO>> ondWiseQuotedChargesEntry : ondWiseQuotedCharges.entrySet()) {
					quotedServiceTaxesForFirstOnd = ondWiseQuotedChargesEntry.getValue().values();
					quotedServiceTaxCodesForFirstOnd = ondWiseQuotedChargesEntry.getValue().keySet();
					break;
				}

				HashSet<String> reservationChargeCodes = new HashSet<String>();
				if (serviceTaxQuoteForTktRevRQ.getPaxTypeWiseCharges() != null
						&& !serviceTaxQuoteForTktRevRQ.getPaxTypeWiseCharges().isEmpty()) {
					for (Entry<String, List<SimplifiedChargeDTO>> paxTypeWiseCharges : serviceTaxQuoteForTktRevRQ
							.getPaxTypeWiseCharges().entrySet()) {
						List<SimplifiedChargeDTO> chargesList = paxTypeWiseCharges.getValue();
						for (SimplifiedChargeDTO charge : chargesList) {
							if (charge.getChargeCode() != null) {
								reservationChargeCodes.add(charge.getChargeCode());
							}
						}
					}
				}
				if (serviceTaxQuoteForTktRevRQ.getPaxWiseExternalCharges() != null
						&& !serviceTaxQuoteForTktRevRQ.getPaxWiseExternalCharges().isEmpty()) {
					for (Entry<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> paxWiseCharges : serviceTaxQuoteForTktRevRQ
							.getPaxWiseExternalCharges().entrySet()) {
						List<SimplifiedChargeDTO> chargesList = paxWiseCharges.getValue();
						for (SimplifiedChargeDTO charge : chargesList) {
							if (charge.getChargeCode() != null) {
								reservationChargeCodes.add(charge.getChargeCode());
							}
						}
					}
				}
				Map<String, List<String>> serviceTaxWiseExcludableChargeCodes = null;
				if (quotedServiceTaxCodesForFirstOnd != null && !quotedServiceTaxCodesForFirstOnd.isEmpty()
						&& reservationChargeCodes != null && !reservationChargeCodes.isEmpty()) {
					serviceTaxWiseExcludableChargeCodes = getChargeJdbcDAO().getServiceTaxWiseExcludableChargeCodes(
							quotedServiceTaxCodesForFirstOnd, reservationChargeCodes);
				}

				Map<Integer, Map<String, String>> ondSeqSegFlightRefNumberMap = new HashMap<Integer, Map<String, String>>();
				Map<Integer, Boolean> ondSeqReturnFlagMap = new HashMap<Integer, Boolean>();
				Map<String, Boolean> flightRefReturnFlagMap = new HashMap<String, Boolean>();
				for (SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO : serviceTaxQuoteForTktRevRQ
						.getSimplifiedFlightSegmentDTOs()) {
					if (simplifiedFlightSegmentDTO.getOndSequence() != null) {
						ondSeqReturnFlagMap.put(simplifiedFlightSegmentDTO.getOndSequence(),
								simplifiedFlightSegmentDTO.isReturnFlag());
						if (ondSeqSegFlightRefNumberMap.get(simplifiedFlightSegmentDTO.getOndSequence()) != null) {
							ondSeqSegFlightRefNumberMap.get(simplifiedFlightSegmentDTO.getOndSequence()).put(
									simplifiedFlightSegmentDTO.getSegmentCode(), simplifiedFlightSegmentDTO.getFlightRefNumber());
						} else {
							Map<String, String> segFlightRefNumberMap = new HashMap<String, String>();
							segFlightRefNumberMap.put(simplifiedFlightSegmentDTO.getSegmentCode(),
									simplifiedFlightSegmentDTO.getFlightRefNumber());
							ondSeqSegFlightRefNumberMap.put(simplifiedFlightSegmentDTO.getOndSequence(), segFlightRefNumberMap);
						}
					}
					if (simplifiedFlightSegmentDTO.getFlightRefNumber() != null) {
						flightRefReturnFlagMap.put(simplifiedFlightSegmentDTO.getFlightRefNumber(),
								simplifiedFlightSegmentDTO.isReturnFlag());
					}
				}

				String serviceTaxDepositeStateCode = null;
				String serviceTaxDepositeCountryCode = null;
				for (QuoteChargesCriteria criteria : criteriaCollection) {
					serviceTaxDepositeStateCode = criteria.getServiceTaxDepositeStateCode();
					serviceTaxDepositeCountryCode = criteria.getServiceTaxDepositeCountryCode();
					break;
				}

				boolean returnJourneyNotTaxable = false;
				if (AppSysParamsUtil.isConsiderSegmentReturnFlagInGSTCalculation()
						&& !serviceTaxQuoteForTktRevRQ.isPaxTaxRegistered() && serviceTaxDepositeCountryCode != null
						&& serviceTaxDepositeCountryCode.equals(serviceTaxQuoteForTktRevRQ.getPaxCountryCode())) {
					returnJourneyNotTaxable = true;
				}

				if (serviceTaxQuoteForTktRevRQ.getPaxTypeWiseCharges() != null) {
					Map<String, List<ServiceTaxDTO>> paxTypeWiseServiceTaxes = new HashMap<String, List<ServiceTaxDTO>>();
					for (Entry<String, List<SimplifiedChargeDTO>> paxTypeWiseCharges : serviceTaxQuoteForTktRevRQ
							.getPaxTypeWiseCharges().entrySet()) {
						for (QuotedChargeDTO quotedServiceTaxForFirstOnd : quotedServiceTaxesForFirstOnd) {
							Map<String, ServiceTaxDTO> flightRefWiseServiceTaxes = new HashMap<String, ServiceTaxDTO>();
							for (SimplifiedChargeDTO charge : paxTypeWiseCharges.getValue()) {
								ServiceTaxDTO serviceTaxDTO = null;
								String flightRefNumber = getFlightRefNumber(charge, ondSeqSegFlightRefNumberMap);
								if (flightRefWiseServiceTaxes.get(flightRefNumber) != null) {
									serviceTaxDTO = flightRefWiseServiceTaxes.get(flightRefNumber);
								} else {
									serviceTaxDTO = new ServiceTaxDTO();
									serviceTaxDTO.setChargeCode(quotedServiceTaxForFirstOnd.getChargeCode());
									serviceTaxDTO.setChargeRateId(quotedServiceTaxForFirstOnd.getChargeRateId());
									serviceTaxDTO.setChargeGroupCode(quotedServiceTaxForFirstOnd.getChargeGroupCode());
									serviceTaxDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
									serviceTaxDTO.setFlightRefNumber(flightRefNumber);
								}

								if ((!returnJourneyNotTaxable || !isReturnSegment(charge, ondSeqReturnFlagMap,
										flightRefReturnFlagMap))
										&& ChargeQuoteUtils.isChgApplicableForPaxType(quotedServiceTaxForFirstOnd,
												paxTypeWiseCharges.getKey())
										&& (serviceTaxWiseExcludableChargeCodes == null
												|| serviceTaxWiseExcludableChargeCodes.get(quotedServiceTaxForFirstOnd
														.getChargeCode()) == null || !serviceTaxWiseExcludableChargeCodes.get(
												quotedServiceTaxForFirstOnd.getChargeCode()).contains(charge.getChargeCode()))) {

									if (quotedServiceTaxForFirstOnd.isChargeValueInPercentage()) {
										serviceTaxDTO.setAmount(AccelAeroCalculator.add(serviceTaxDTO.getAmount(),
												setEffectiveChargeAmount(quotedServiceTaxForFirstOnd, charge.getAmount())));
									} else {
										serviceTaxDTO.setAmount(AccelAeroCalculator.add(serviceTaxDTO.getAmount(),
												AccelAeroCalculator.parseBigDecimal(quotedServiceTaxForFirstOnd
														.getChargeValuePercentage())));
									}
									serviceTaxDTO.setTaxableAmount(AccelAeroCalculator.add(serviceTaxDTO.getTaxableAmount(),
											charge.getAmount()));
								} else {
									serviceTaxDTO.setNonTaxableAmount(AccelAeroCalculator.add(
											serviceTaxDTO.getNonTaxableAmount(), charge.getAmount()));
								}								
								flightRefWiseServiceTaxes.put(flightRefNumber, serviceTaxDTO);
							}
							for (ServiceTaxDTO finalServiceTaxDTO : flightRefWiseServiceTaxes.values()) {
								finalServiceTaxDTO.setAmount(roundingChargeAmount(quotedServiceTaxForFirstOnd,
										finalServiceTaxDTO.getAmount()));
							}
							if (paxTypeWiseServiceTaxes.get(paxTypeWiseCharges.getKey()) != null) {
								paxTypeWiseServiceTaxes.get(paxTypeWiseCharges.getKey()).addAll(
										flightRefWiseServiceTaxes.values());
							} else {
								paxTypeWiseServiceTaxes.put(paxTypeWiseCharges.getKey(),
										new ArrayList(flightRefWiseServiceTaxes.values()));
							}
						}
					}
					serviceTaxQuoteForTktRevRS.setPaxTypeWiseServiceTaxes(paxTypeWiseServiceTaxes);
				}

				if (serviceTaxQuoteForTktRevRQ.getPaxWiseExternalCharges() != null) {
					Double totalChargePercentage = 0d;
					Double totalChargeValue = 0d;
					for (QuotedChargeDTO quotedServiceTaxForFirstOnd : quotedServiceTaxesForFirstOnd) {
						if (quotedServiceTaxForFirstOnd.isChargeValueInPercentage()) {
							totalChargePercentage += quotedServiceTaxForFirstOnd.getChargeValuePercentage();
						} else {
							totalChargeValue += quotedServiceTaxForFirstOnd.getChargeValuePercentage();
						}
					}
					Map<Integer, List<ServiceTaxDTO>> paxWiseServiceTaxes = new HashMap<Integer, List<ServiceTaxDTO>>();
					for (Entry<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> paxWiseCharges : serviceTaxQuoteForTktRevRQ
							.getPaxWiseExternalCharges().entrySet()) {
						BigDecimal serviceTaxTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
						for (QuotedChargeDTO quotedServiceTaxForFirstOnd : quotedServiceTaxesForFirstOnd) {
							Map<String, ServiceTaxDTO> flightRefWiseServiceTaxes = new HashMap<String, ServiceTaxDTO>();
							for (SimplifiedChargeDTO charge : paxWiseCharges.getValue()) {
								ServiceTaxDTO serviceTaxDTO = null;
								String flightRefNumber = getFlightRefNumber(charge, ondSeqSegFlightRefNumberMap);
								if (flightRefWiseServiceTaxes.get(flightRefNumber) != null) {
									serviceTaxDTO = flightRefWiseServiceTaxes.get(flightRefNumber);
								} else {
									serviceTaxDTO = new ServiceTaxDTO();
									serviceTaxDTO.setChargeCode(quotedServiceTaxForFirstOnd.getChargeCode());
									serviceTaxDTO.setChargeGroupCode(quotedServiceTaxForFirstOnd.getChargeGroupCode());
									serviceTaxDTO.setChargeRateId(quotedServiceTaxForFirstOnd.getChargeRateId());
									serviceTaxDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
									serviceTaxDTO.setFlightRefNumber(flightRefNumber);
								}

								if ((!returnJourneyNotTaxable || !isReturnSegment(charge, ondSeqReturnFlagMap,
										flightRefReturnFlagMap))
										&& ChargeQuoteUtils.isChgApplicableForPaxType(quotedServiceTaxForFirstOnd, paxWiseCharges
												.getKey().getPaxType())
										&& (serviceTaxWiseExcludableChargeCodes == null
												|| serviceTaxWiseExcludableChargeCodes.get(quotedServiceTaxForFirstOnd
														.getChargeCode()) == null || !serviceTaxWiseExcludableChargeCodes.get(
												quotedServiceTaxForFirstOnd.getChargeCode()).contains(charge.getChargeCode()))) {

									serviceTaxDTO.setTaxableAmount(AccelAeroCalculator.add(serviceTaxDTO.getTaxableAmount(),
											charge.getAmount()));
								} else {
									serviceTaxDTO.setNonTaxableAmount(AccelAeroCalculator.add(
											serviceTaxDTO.getNonTaxableAmount(), charge.getAmount()));
								}
								flightRefWiseServiceTaxes.put(flightRefNumber, serviceTaxDTO);
							}
							for (ServiceTaxDTO finalServiceTaxDTO : flightRefWiseServiceTaxes.values()) {
								if (paxWiseCharges.getKey().isTotalAmountIncludingServiceTax()) {
									if (quotedServiceTaxForFirstOnd.isChargeValueInPercentage()) {
										BigDecimal taxablePaxTotalCharge = AccelAeroCalculator.divide(finalServiceTaxDTO
												.getTaxableAmount(), new BigDecimal(1 + (totalChargePercentage / 100d)));
										finalServiceTaxDTO.setAmount(setEffectiveChargeAmount(quotedServiceTaxForFirstOnd,
												taxablePaxTotalCharge));
									} else {
										if (finalServiceTaxDTO.getTaxableAmount().doubleValue() > totalChargeValue) {
											BigDecimal taxablePaxTotalCharge = AccelAeroCalculator.subtract(
													finalServiceTaxDTO.getTaxableAmount(),
													AccelAeroCalculator.parseBigDecimal(totalChargeValue));
											finalServiceTaxDTO.setAmount(AccelAeroCalculator
													.parseBigDecimal(quotedServiceTaxForFirstOnd.getChargeValuePercentage()));
										} else {
											finalServiceTaxDTO.setTaxableAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
											finalServiceTaxDTO.setNonTaxableAmount(AccelAeroCalculator.add(
													finalServiceTaxDTO.getNonTaxableAmount(),
													finalServiceTaxDTO.getTaxableAmount()));
										}
									}
								} else {
									if (quotedServiceTaxForFirstOnd.isChargeValueInPercentage()) {
										finalServiceTaxDTO.setAmount(setEffectiveChargeAmount(quotedServiceTaxForFirstOnd, finalServiceTaxDTO.getTaxableAmount()));
									} else {
										finalServiceTaxDTO.setAmount(AccelAeroCalculator.parseBigDecimal(quotedServiceTaxForFirstOnd
														.getChargeValuePercentage()));
									}
								}
								finalServiceTaxDTO.setAmount(
										roundingChargeAmount(quotedServiceTaxForFirstOnd, finalServiceTaxDTO.getAmount()));
								serviceTaxTotal = AccelAeroCalculator.add(serviceTaxTotal, finalServiceTaxDTO.getAmount());
							}
							if (paxWiseServiceTaxes.get(paxWiseCharges.getKey().getPaxSequence()) != null) {
								paxWiseServiceTaxes.get(paxWiseCharges.getKey().getPaxSequence()).addAll(
										flightRefWiseServiceTaxes.values());
							} else {
								paxWiseServiceTaxes.put(paxWiseCharges.getKey().getPaxSequence(), new ArrayList(
										flightRefWiseServiceTaxes.values()));
							}
						}
						if (paxWiseCharges.getKey().isTotalAmountIncludingServiceTax()
								&& paxWiseServiceTaxes.get(paxWiseCharges.getKey().getPaxSequence()) != null) {
							BigDecimal taxableAmount = null;
							for (ServiceTaxDTO serviceTaxDTO : paxWiseServiceTaxes
									.get(paxWiseCharges.getKey().getPaxSequence())) {
								if (taxableAmount == null) {
									taxableAmount = AccelAeroCalculator
											.subtract(serviceTaxDTO.getTaxableAmount(), serviceTaxTotal);
								}
								serviceTaxDTO.setTaxableAmount(taxableAmount);
							}
						}

					}
					serviceTaxQuoteForTktRevRS.setPaxWiseServiceTaxes(paxWiseServiceTaxes);
				}
				serviceTaxQuoteForTktRevRS.setServiceTaxDepositeStateCode(serviceTaxDepositeStateCode);
				serviceTaxQuoteForTktRevRS.setServiceTaxDepositeCountryCode(serviceTaxDepositeCountryCode);
			}
		}
		return serviceTaxQuoteForTktRevRS;
	}
	
	private String getFlightRefNumber(SimplifiedChargeDTO charge, Map<Integer, Map<String, String>> ondSeqSegFlightRefNumberMap) {
		String flightRefNumber = null;
		if (charge.getFlightRefNumber() != null) {
			flightRefNumber = charge.getFlightRefNumber();
		} else {
			Map<String, String> segFlightRefMap = ondSeqSegFlightRefNumberMap.get(charge.getOndSequence());
			if(segFlightRefMap != null && !segFlightRefMap.isEmpty()){
				flightRefNumber = segFlightRefMap.get(charge.getSegmentCode());
				if (flightRefNumber == null && charge.getSegmentCode() != null && charge.getSegmentCode().length() > 7
						&& ondSeqSegFlightRefNumberMap.get(charge.getOndSequence()).size() > 0) {
					flightRefNumber = ondSeqSegFlightRefNumberMap.get(charge.getOndSequence()).get(
							charge.getSegmentCode().substring(0, 7));
				}
			}
		}
		return flightRefNumber;
	}

	private boolean isReturnSegment(SimplifiedChargeDTO charge, Map<Integer, Boolean> ondSeqReturnFlagMap,
			Map<String, Boolean> flightRefReturnFlagMap) {
		if (charge.getOndSequence() != null) {
			return ondSeqReturnFlagMap.get(charge.getOndSequence());
		} else if (charge.getFlightRefNumber() != null) {
			return flightRefReturnFlagMap.get(charge.getFlightRefNumber());
		}
		return false;
	}

	private BigDecimal setEffectiveChargeAmount(QuotedChargeDTO quotedChargeDTO, BigDecimal chargeAmount) {
		Double chargePercentage = quotedChargeDTO.getChargeValuePercentage();
		if (chargeAmount != null) {
			Double calculatedEffectiveVal = new Double(chargeAmount.doubleValue() * (chargePercentage / 100d));
			return new BigDecimal(calculatedEffectiveVal);
		}
		return null;
	}
	
	private BigDecimal roundingChargeAmount(QuotedChargeDTO quotedChargeDTO, BigDecimal chargeAmount) {
		Double calculatedEffectiveVal = null;
		BigDecimal bgCalculatedEffectiveVal = null;
		if (chargeAmount != null) {
			calculatedEffectiveVal = chargeAmount.doubleValue();
		}
		
		// break point calculations
		boolean chargeRoundingApplied = false;
		if (quotedChargeDTO.isChargeRateRoundingApplicable()) {
			calculatedEffectiveVal = AccelAeroRounderPolicy.getRoundedValue(BigDecimal.valueOf(calculatedEffectiveVal),
					quotedChargeDTO.getBoundryValue(), quotedChargeDTO.getBreakPoint()).doubleValue();
			chargeRoundingApplied = true;
		}

		if (calculatedEffectiveVal != null) {
			String strRoundEnable = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_ENABLE);
			if (!chargeRoundingApplied && strRoundEnable != null && strRoundEnable.equals("Y") && calculatedEffectiveVal != null) {
				BigDecimal bgeffectiveChargeAmount = AccelAeroRounderPolicy.getRoundedValue(AccelAeroCalculator
						.parseBigDecimal(calculatedEffectiveVal),
						new BigDecimal(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_ROUND)),
						new BigDecimal(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_BREAKPOINT)));
				calculatedEffectiveVal = bgeffectiveChargeAmount.doubleValue();
			}
			if (AppSysParamsUtil.isRoundupChargesWhenCalculatedInBaseCurrency()) {
				bgCalculatedEffectiveVal = new BigDecimal(calculatedEffectiveVal).setScale(0, RoundingMode.UP);
			} else {
				bgCalculatedEffectiveVal = AccelAeroCalculator.parseBigDecimal(calculatedEffectiveVal.doubleValue());
			}
		}
		return bgCalculatedEffectiveVal;
	}
	
	private BigDecimal setEffectiveChargeAmountWithRounding(QuotedChargeDTO quotedChargeDTO, BigDecimal chargeAmount) {
		Double chargePercentage = quotedChargeDTO.getChargeValuePercentage();
		Double calculatedEffectiveVal = null;
		BigDecimal bgCalculatedEffectiveVal = null;
		if (chargeAmount != null) {
			calculatedEffectiveVal = new Double(chargeAmount.doubleValue() * (chargePercentage / 100d));
		}

		// break point calculations
		boolean chargeRoundingApplied = false;
		if (quotedChargeDTO.isChargeRateRoundingApplicable()) {
			calculatedEffectiveVal = AccelAeroRounderPolicy.getRoundedValue(BigDecimal.valueOf(calculatedEffectiveVal),
					quotedChargeDTO.getBoundryValue(), quotedChargeDTO.getBreakPoint()).doubleValue();
			chargeRoundingApplied = true;
		}

		if (calculatedEffectiveVal != null) {
			String strRoundEnable = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_ENABLE);
			if (!chargeRoundingApplied && strRoundEnable != null && strRoundEnable.equals("Y") && calculatedEffectiveVal != null) {
				BigDecimal bgeffectiveChargeAmount = AccelAeroRounderPolicy.getRoundedValue(AccelAeroCalculator
						.parseBigDecimal(calculatedEffectiveVal),
						new BigDecimal(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_ROUND)),
						new BigDecimal(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_BREAKPOINT)));
				calculatedEffectiveVal = bgeffectiveChargeAmount.doubleValue();
			}
			if (AppSysParamsUtil.isRoundupChargesWhenCalculatedInBaseCurrency()) {
				bgCalculatedEffectiveVal = new BigDecimal(calculatedEffectiveVal).setScale(0, RoundingMode.UP);
			} else {
				bgCalculatedEffectiveVal = AccelAeroCalculator.parseBigDecimal(calculatedEffectiveVal.doubleValue());
			}
		}
		return bgCalculatedEffectiveVal;
	}

	private Collection<QuoteChargesCriteria> generateCriteriaCollectionForTicketingRevenue(
			ServiceTaxQuoteForTicketingRevenueRQ serviceTaxQuoteForTktRevRQ) {
		String pointOfEmbarkationCountry = null;
		String pointOfEmbarkationState = null;
		String serviceTaxPlaceOfSupplyStateCode = null;
		String serviceTaxPlaceOfSupplyCountryCode = null;

		Collection<QuoteChargesCriteria> criteriaCollection = new ArrayList<QuoteChargesCriteria>();
		List<SimplifiedFlightSegmentDTO> simplifiedFlightSegmentDTOs = serviceTaxQuoteForTktRevRQ
				.getSimplifiedFlightSegmentDTOs();
		Collections.sort(simplifiedFlightSegmentDTOs);
		for (SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO : simplifiedFlightSegmentDTOs) {
			if (AppSysParamsUtil.getDefaultCarrierCode().equals(simplifiedFlightSegmentDTO.getOperatingAirline())) {
				QuoteChargesCriteria criterion = new QuoteChargesCriteria();
				criterion.setServiceTaxQuote(true);
				String firstSegment = simplifiedFlightSegmentDTO.getSegmentCode();
				criterion.setOnd(firstSegment);
				if (firstSegment != null) {
					String[] values = firstSegment.split("/");
					String pointOfEmbarkationAirport = values[0];
					if (pointOfEmbarkationAirport != null) {
						Object[] pointOfEmbarkationInfo = CommonsServices.getGlobalConfig().retrieveAirportInfo(
								pointOfEmbarkationAirport);
						pointOfEmbarkationCountry = pointOfEmbarkationInfo[3].toString();
						if (pointOfEmbarkationInfo[5] != null) {
							pointOfEmbarkationState = pointOfEmbarkationInfo[5].toString();
						}
					}
				}
				if (CalendarUtil.isLessThan(simplifiedFlightSegmentDTO.getDepartureDateTimeZulu(),
						CalendarUtil.getCurrentSystemTimeInZulu())) {
					criterion.setDepatureDate(CalendarUtil.getCurrentSystemTimeInZulu());
				} else {
					criterion.setDepatureDate(simplifiedFlightSegmentDTO.getDepartureDateTimeZulu());
				}
				criterion.setCabinClassCode(simplifiedFlightSegmentDTO.getLogicalCabinClassCode());
				criterion.setChargeQuoteDate(new Date());
				criterion.setChannelId(serviceTaxQuoteForTktRevRQ.getChannelCode());
				criteriaCollection.add(criterion);
				break;
			}
		}
		serviceTaxPlaceOfSupplyStateCode = deriveServiceTaxPlaceOfSupplyStateCodeForTkt(serviceTaxQuoteForTktRevRQ.getPaxState(),
				pointOfEmbarkationState, serviceTaxQuoteForTktRevRQ.isPaxTaxRegistered());
		serviceTaxPlaceOfSupplyCountryCode = deriveServiceTaxPlaceOfSupplyCountryCodeForTkt(
				serviceTaxQuoteForTktRevRQ.getPaxCountryCode(), pointOfEmbarkationCountry,
				serviceTaxQuoteForTktRevRQ.isPaxTaxRegistered());
		for (QuoteChargesCriteria criterion : criteriaCollection) {
			criterion.setServiceTaxDepositeStateCode(pointOfEmbarkationState);
			criterion.setServiceTaxDepositeCountryCode(pointOfEmbarkationCountry);
			criterion.setServiceTaxPlaceOfSupplyStateCode(serviceTaxPlaceOfSupplyStateCode);
			criterion.setServiceTaxPlaceOfSupplyCountryCode(serviceTaxPlaceOfSupplyCountryCode);
		}
		return criteriaCollection;
	}

	private String deriveServiceTaxPlaceOfSupplyStateCodeForTkt(String paxState, String pointOfEmbarkationState,
			boolean paxTaxRegistered) {
		String placeOfSupplyStateCode = null;
		if (pointOfEmbarkationState != null) {
			if (paxTaxRegistered) {
				placeOfSupplyStateCode = paxState;
			} else {
				placeOfSupplyStateCode = pointOfEmbarkationState;
			}
		}
		return placeOfSupplyStateCode;
	}

	private String deriveServiceTaxPlaceOfSupplyCountryCodeForTkt(String paxCountry, String pointOfEmbarkationCountry,
			boolean paxTaxRegistered) {
		String placeOfSupplyCountryCode = null;
		if (pointOfEmbarkationCountry != null) {
			if (paxTaxRegistered) {
				placeOfSupplyCountryCode = paxCountry;
			} else {
				placeOfSupplyCountryCode = pointOfEmbarkationCountry;
			}
		}
		return placeOfSupplyCountryCode;
	}

	public ServiceTaxQuoteForNonTicketingRevenueRS quoteServiceTaxForNonTicketingRevenue(
			ServiceTaxQuoteForNonTicketingRevenueRQ serviceTaxQuoteForNonTktRevRQ) throws ModuleException {
		ServiceTaxQuoteForNonTicketingRevenueRS serviceTaxQuoteForNonTktRevRS = new ServiceTaxQuoteForNonTicketingRevenueRS();

		if (serviceTaxQuoteForNonTktRevRQ.getPaxCnxModPenCharges() != null
				&& !serviceTaxQuoteForNonTktRevRQ.getPaxCnxModPenCharges().isEmpty()) {
			Collection<QuoteChargesCriteria> criteriaCollection = generateCriteriaCollectionForNonTicketingRevenue(serviceTaxQuoteForNonTktRevRQ);

			HashMap<String, HashMap<String, QuotedChargeDTO>> ondWiseQuotedCharges = getChargeJdbcDAO().quoteCharges(
					criteriaCollection);

			boolean serviceTaxApplied = false;
			if (ondWiseQuotedCharges != null && ondWiseQuotedCharges.size() > 0 && ondWiseQuotedCharges.values() != null) {
				for (HashMap<String, QuotedChargeDTO> quotedCharges : ondWiseQuotedCharges.values()) {
					if (quotedCharges.values() != null && !quotedCharges.values().isEmpty()) {
						serviceTaxApplied = true;
						break;
					}
				}
			}

			if (serviceTaxApplied) {
				HashMap<String, QuotedChargeDTO> quotedServiceTaxesForFirstOnd = null;
				for (Entry<String, HashMap<String, QuotedChargeDTO>> ondWiseQuotedChargesEntry : ondWiseQuotedCharges.entrySet()) {
					quotedServiceTaxesForFirstOnd = ondWiseQuotedChargesEntry.getValue();
					break;
				}

				for (QuoteChargesCriteria criteria : criteriaCollection) {
					serviceTaxQuoteForNonTktRevRS.setServiceTaxDepositeStateCode(criteria.getServiceTaxDepositeStateCode());
					serviceTaxQuoteForNonTktRevRS.setServiceTaxDepositeCountryCode(criteria.getServiceTaxDepositeCountryCode());
					break;
				}

				if (serviceTaxQuoteForNonTktRevRQ.getPaxCnxModPenCharges() != null) {
					Map<Integer, CnxModPenServiceTaxDTO> paxWiseCnxModPenServiceTaxes = new HashMap<Integer, CnxModPenServiceTaxDTO>();
					for (PaxCnxModPenChargeDTO paxCnxModPenCharge : serviceTaxQuoteForNonTktRevRQ.getPaxCnxModPenCharges()) {
						List<ServiceTaxDTO> cnxModPenServiceTaxes = new ArrayList<>();
						BigDecimal paxTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
						for (CnxModPenChargeDTO charge : paxCnxModPenCharge.getCnxModPenChargeDTOs()) {
							paxTotalCharge = AccelAeroCalculator.add(paxTotalCharge, charge.getAmount());
						}
						BigDecimal taxablePaxTotalCharge = paxTotalCharge;
						Double chargePercentage = 0d;
						Double chargeValue = 0d;
						for (QuotedChargeDTO quotedServiceTaxForFirstOnd : quotedServiceTaxesForFirstOnd.values()) {
							ServiceTaxDTO serviceTaxDTO = new ServiceTaxDTO();
							serviceTaxDTO.setChargeCode(quotedServiceTaxForFirstOnd.getChargeCode());
							serviceTaxDTO.setChargeRateId(quotedServiceTaxForFirstOnd.getChargeRateId());
							serviceTaxDTO.setChargeGroupCode(quotedServiceTaxForFirstOnd.getChargeGroupCode());
							serviceTaxDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
							if (ChargeQuoteUtils.isChgApplicableForPaxType(quotedServiceTaxForFirstOnd,
									paxCnxModPenCharge.getPaxType())) {
								if (paxCnxModPenCharge.isTotalAmountIncludingServiceTax()) {
									if (quotedServiceTaxForFirstOnd.isChargeValueInPercentage()) {
										chargePercentage += quotedServiceTaxForFirstOnd.getChargeValuePercentage();
									} else {
										chargeValue += quotedServiceTaxForFirstOnd.getChargeValuePercentage();
									}
								} else {
									if (quotedServiceTaxForFirstOnd.isChargeValueInPercentage()) {
										serviceTaxDTO.setAmount(setEffectiveChargeAmountWithRounding(quotedServiceTaxForFirstOnd,
												paxTotalCharge));
									} else {
										serviceTaxDTO.setAmount(AccelAeroCalculator.parseBigDecimal(quotedServiceTaxForFirstOnd
												.getChargeValuePercentage()));
									}
									serviceTaxDTO.setTaxableAmount(paxTotalCharge);
								}
							} else {
								serviceTaxDTO.setNonTaxableAmount(paxTotalCharge);
							}
							cnxModPenServiceTaxes.add(serviceTaxDTO);
						}
						if (chargePercentage.doubleValue() != 0) {
							taxablePaxTotalCharge = AccelAeroCalculator.divide(paxTotalCharge,
									AccelAeroCalculator.parseBigDecimal(1 + (chargePercentage / 100d)));
							BigDecimal totalTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
							for (ServiceTaxDTO serviceTaxDTO : cnxModPenServiceTaxes) {
								serviceTaxDTO.setAmount(setEffectiveChargeAmountWithRounding(
										quotedServiceTaxesForFirstOnd.get(serviceTaxDTO.getChargeCode()), taxablePaxTotalCharge));
								totalTaxAmount = AccelAeroCalculator.add(totalTaxAmount, serviceTaxDTO.getAmount());
							}
							taxablePaxTotalCharge = AccelAeroCalculator.subtract(paxTotalCharge, totalTaxAmount);
							for (ServiceTaxDTO serviceTaxDTO : cnxModPenServiceTaxes) {
								serviceTaxDTO.setTaxableAmount(taxablePaxTotalCharge);
							}							
						}
						if (chargeValue.doubleValue() != 0) {
							if (paxTotalCharge.doubleValue() > chargeValue) {
								paxTotalCharge = AccelAeroCalculator.subtract(paxTotalCharge,
										AccelAeroCalculator.parseBigDecimal(chargeValue));
								for (ServiceTaxDTO serviceTaxDTO : cnxModPenServiceTaxes) {
									serviceTaxDTO.setAmount(AccelAeroCalculator.parseBigDecimal(quotedServiceTaxesForFirstOnd
											.get(serviceTaxDTO.getChargeCode()).getChargeValuePercentage()));
									serviceTaxDTO.setTaxableAmount(paxTotalCharge);
								}
							} else {
								for (ServiceTaxDTO serviceTaxDTO : cnxModPenServiceTaxes) {
									serviceTaxDTO.setNonTaxableAmount(paxTotalCharge);
								}
							}
						}
						CnxModPenServiceTaxDTO cnxModPenServiceTaxDTO = new CnxModPenServiceTaxDTO();
						cnxModPenServiceTaxDTO.setCnxModPenServiceTaxes(cnxModPenServiceTaxes);
						cnxModPenServiceTaxDTO.setEffectiveCnxModPenChargeAmount(taxablePaxTotalCharge);
						paxWiseCnxModPenServiceTaxes.put(paxCnxModPenCharge.getPaxSequence(), cnxModPenServiceTaxDTO);
					}
					serviceTaxQuoteForNonTktRevRS.setPaxWiseCnxModPenServiceTaxes(paxWiseCnxModPenServiceTaxes);
				}
			}
		}
		return serviceTaxQuoteForNonTktRevRS;
	}
	
	private Collection<QuoteChargesCriteria> generateCriteriaCollectionForNonTicketingRevenue(
			ServiceTaxQuoteForNonTicketingRevenueRQ serviceTaxQuoteForNonTktRevRQ) throws ModuleException {
		String modifyingAgentCountryCode = null;
		String modifyingAgentStateCode = null;
		String serviceTaxPlaceOfSupplyStateCode = null;
		String serviceTaxPlaceOfSupplyCountryCode = null;

		Collection<QuoteChargesCriteria> criteriaCollection = new ArrayList<QuoteChargesCriteria>();
		String modifyingAgentStation = serviceTaxQuoteForNonTktRevRQ.getModifyingAgentStation();
		if (modifyingAgentStation != null) {
			StationDTO stationDTO = AirpricingUtils.getCommonMasterBD().getStationInfo(modifyingAgentStation);
			if (stationDTO != null) {
				modifyingAgentCountryCode = stationDTO.getCountryCode();
				modifyingAgentStateCode = stationDTO.getStateCode();
				
				QuoteChargesCriteria criterion = new QuoteChargesCriteria();
				criterion.setServiceTaxQuote(true);
				criterion.setServiceTaxQuoteForNonTktRev(true);
				criterion.setOnd(modifyingAgentCountryCode + "/" + modifyingAgentCountryCode);
				criterion.setDepatureDate(new Date());
				criterion.setChargeQuoteDate(new Date());
				criterion.setChannelId(serviceTaxQuoteForNonTktRevRQ.getChannelCode());

				serviceTaxPlaceOfSupplyStateCode = deriveServiceTaxPlaceOfSupplyStateCodeForNonTkt(serviceTaxQuoteForNonTktRevRQ.getPaxState(),
						modifyingAgentStateCode);
				serviceTaxPlaceOfSupplyCountryCode = deriveServiceTaxPlaceOfSupplyCountryCodeForNonTkt(
						serviceTaxQuoteForNonTktRevRQ.getPaxCountryCode(), modifyingAgentCountryCode);

				criterion.setServiceTaxDepositeStateCode(modifyingAgentStateCode);
				criterion.setServiceTaxDepositeCountryCode(modifyingAgentCountryCode);
				criterion.setServiceTaxPlaceOfSupplyStateCode(serviceTaxPlaceOfSupplyStateCode);
				criterion.setServiceTaxPlaceOfSupplyCountryCode(serviceTaxPlaceOfSupplyCountryCode);
				criteriaCollection.add(criterion);
			}
		}
		return criteriaCollection;
	}
	
	private String deriveServiceTaxPlaceOfSupplyStateCodeForNonTkt(String paxState, String modifyingAgentStateCode) {
		String placeOfSupplyStateCode = null;
		if (paxState != null) {
			placeOfSupplyStateCode = paxState;
		} else {
			placeOfSupplyStateCode = modifyingAgentStateCode;
		}
		return placeOfSupplyStateCode;
	}

	private String deriveServiceTaxPlaceOfSupplyCountryCodeForNonTkt(String paxCountry, String modifyingAgentCountryCode) {
		String placeOfSupplyCountryCode = null;
		if (paxCountry != null) {
			placeOfSupplyCountryCode = paxCountry;
		} else {
			placeOfSupplyCountryCode = modifyingAgentCountryCode;
		}
		return placeOfSupplyCountryCode;
	}
	public void updateSeatChargeBaseCurrAmountOnLocalCurrExchangeRtUpdate(CurrencyExchangeRate currencyExchangeRate,
			UserPrincipal userPrincipal) throws ModuleException {

		try {
			BigDecimal newChargeAmount;
			BigDecimal exRate, boundaryValue, breakPointValue;
			ArrayList<ChargeTemplate> affectedchrgTMPList;

			affectedchrgTMPList = (ArrayList<ChargeTemplate>) chargedao.getAffectedChargeTemplates(currencyExchangeRate
					.getCurrency().getCurrencyCode());
			exRate = currencyExchangeRate.getExrateCurToBaseNumber();
			boundaryValue = currencyExchangeRate.getCurrency().getBoundryValue();
			breakPointValue = currencyExchangeRate.getCurrency().getBreakPoint();
			for (ChargeTemplate chrgTemp : affectedchrgTMPList) {
				for (SeatCharge seatChrg : chrgTemp.getSeatCharges()) {
					if (!seatChrg.getStatus().equals(AirPricingCustomConstants.STATUS_ACTIVE)) {
						newChargeAmount = AccelAeroRounderPolicy.getRoundedValue(
								AccelAeroCalculator.multiply(seatChrg.getLocalCurrencyAmount(), exRate), boundaryValue,
								breakPointValue);
						seatChrg.setChargeAmount(newChargeAmount);
					}
				}
			}
			updateSeatCharges(affectedchrgTMPList, userPrincipal);

		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private void updateSeatCharges(ArrayList<ChargeTemplate> chrgTMPList, UserPrincipal userPrincipal) throws ModuleException {
		Iterator<ChargeTemplate> iterator = chrgTMPList.iterator();
		try {
			while (iterator.hasNext()) {
				ChargeTemplate chrgTemp = iterator.next();
				chrgTemp = (ChargeTemplate) PricingUtils.setUserDetails(chrgTemp, userPrincipal);
				chargedao.saveTemplate(chrgTemp);
				AuditAirpricing.doAuditSeatMapAddOrModifyTemplate(chrgTemp, userPrincipal);
			}
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

}
