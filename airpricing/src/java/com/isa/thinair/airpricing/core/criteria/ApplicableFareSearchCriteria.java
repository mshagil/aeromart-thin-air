package com.isa.thinair.airpricing.core.criteria;

import java.util.Date;
import java.util.Set;

public class ApplicableFareSearchCriteria {
	private Set<String> ondCodes;
	private Date from;
	private Date to;
	private String bookingType;
	private Set<Integer> salesChannels;
	private Set<String> paxTypes;
	private String agentCode;

	public Date getFromDate() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getToDate() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public Set<Integer> getSalesChannels() {
		return salesChannels;
	}

	public void setSalesChannels(Set<Integer> salesChannels) {
		this.salesChannels = salesChannels;
	}

	public Set<String> getPaxTypes() {
		return paxTypes;
	}

	public void setPaxTypes(Set<String> paxTypes) {
		this.paxTypes = paxTypes;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public void setOndCodes(Set<String> ondCodes) {
		this.ondCodes = ondCodes;
	}

	public Set<String> getOndCodes() {
		return ondCodes;
	}

}
