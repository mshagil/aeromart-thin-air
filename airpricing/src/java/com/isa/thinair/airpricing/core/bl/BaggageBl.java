package com.isa.thinair.airpricing.core.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.baggage.ONDBaggageDTO;
import com.isa.thinair.airinventory.api.dto.baggage.ReservationBaggageTo;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airpricing.api.criteria.ONDCodeSearchCriteria;
import com.isa.thinair.airpricing.api.dto.BaggageTemplateTo;
import com.isa.thinair.airpricing.api.model.ONDBaggageChargeTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeJdbcDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class BaggageBl implements IBaggageBl {

	public void saveBaggage(ONDBaggageTemplate baggage) {
		ChargeDAO chargeDAO = (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);
		chargeDAO.saveONDBaggageTemplate(baggage);
	}

	public Page<BaggageTemplateTo> searchBaggage(ONDCodeSearchCriteria criteria, int startRec, int noOfRecs) {

		return null;
	}

	public void validateBaggageRules(ONDBaggageTemplate baggageTemplate) throws ModuleException {
		Map<Integer, Map<CommonsConstants.BaggageCriterion, List<String>>> overlappedTemplates;
		List<String> baggageCriterionList = AppSysParamsUtil.getBaggageSelectionCriterionByPriority();
		ChargeJdbcDAO chargeJdbcDAO = (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
		boolean ignore;

		overlappedTemplates = chargeJdbcDAO.getOverlappedTemplates(baggageTemplate);
		List<Integer> overlappedTemplateIds = new ArrayList<Integer>();

		for (int possibleOverlapId : overlappedTemplates.keySet()) {
			ignore = true;
			if(overlappedTemplates.get(possibleOverlapId).keySet().size() == baggageCriterionList.size()) {
				for(List<String> matchedParams : overlappedTemplates.get(possibleOverlapId).values()) {
					if (!matchedParams.isEmpty()) {
						ignore = false;
						break;
					}
				}

				if(!ignore) {
					overlappedTemplateIds.add(possibleOverlapId);
				}
			}
		}

		if (!overlappedTemplateIds.isEmpty()) {
			ModuleException moduleException = new ModuleException("um.inventory.logic.baggage.template.overlap");
			moduleException.setExceptionDetails(overlappedTemplateIds.toString());
			throw moduleException;
		}

	}

	private BaggageHandler getBaggageHandler(BaggageCriterion baggageCriterion) {

		switch (baggageCriterion) {
		case BOOKING_CLASS:
			return new BookingClassBaggageHandler();
		case AGENT:
			return new AgentBaggageHandler();
		case FLIGHT_NUMBER:
			return new FlightNumberBaggageHandler();
		case OND:
			return new OndBaggageHandler();
		default:
			return null;
		}
	}

	// ---------------------------------------------------------

	private enum BaggageCriterion {
		BOOKING_CLASS, AGENT, FLIGHT_NUMBER, OND

	}

	private abstract class BaggageHandler {

		public void buildBaggages(Map<Integer, BaggageTemplateTo> baggageTemplates) {
		}

		public ReservationBaggageTo selectBaggage(ONDBaggageDTO baggageDTO) {
			return new ReservationBaggageTo();
		}

		public void validateBaggageRules(ONDBaggageTemplate baggageTemplate) throws ModuleException {
		}
	}

	private class BookingClassBaggageHandler extends BaggageHandler {

		public void validateBaggageRules(ONDBaggageTemplate baggageTemplate) throws ModuleException {
			ChargeJdbcDAO chargeJdbcDAO = (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
			boolean rulesViolated = chargeJdbcDAO.overlapsBaggageTemplatesBookingClasses(baggageTemplate);

			if(rulesViolated) {
				throw new ModuleException("um.inventory.logic.baggage.template.overlap.bookingclasses");
			}
		}
	}

	private class AgentBaggageHandler extends BaggageHandler {
		public void validateBaggageRules(ONDBaggageTemplate baggageTemplate) throws ModuleException {
			ChargeJdbcDAO chargeJdbcDAO = (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
			boolean rulesViolated = chargeJdbcDAO.overlapsBaggageTemplatesAgents(baggageTemplate);

			if(rulesViolated) {
				throw new ModuleException("um.inventory.logic.baggage.template.overlap.agents");
			}
		}
	}

	private class FlightNumberBaggageHandler extends BaggageHandler {

		public void validateBaggageRules(ONDBaggageTemplate baggageTemplate) throws ModuleException {
			ChargeJdbcDAO chargeJdbcDAO = (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
			boolean rulesViolated = chargeJdbcDAO.overlapsBaggageTemplatesFlights(baggageTemplate);

			if(rulesViolated) {
				throw new ModuleException("um.inventory.logic.baggage.template.overlap.flights");
			}
		}

	}

	private class OndBaggageHandler extends BaggageHandler {

		public void validateBaggageRules(ONDBaggageTemplate baggageTemplate) throws ModuleException {
			ChargeBD chargeBD = AirInventoryModuleUtils.getChargeBD();
			boolean rulesViolated = false;

			if (baggageTemplate.getOnds() != null) {
				for (ONDBaggageChargeTemplate ond : baggageTemplate.getOnds()) {
					rulesViolated = chargeBD.isONDCodeExist(ond.getOndCode(), baggageTemplate.getTemplateId());
					if (rulesViolated) {
						throw new ModuleException("um.inventory.logic.baggage.template.overlap.onds");
					}
				}
			}
		}
	}
}
