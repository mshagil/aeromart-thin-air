package com.isa.thinair.airpricing.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airpricing.api.service.AnciChargeLocalCurrBD;

/**
 * @author subash 
 */

@Remote
public interface AnciChargeLocalCurrBDImpl extends AnciChargeLocalCurrBD {

}
