package com.isa.thinair.airpricing.core.persistence.jdbc.chargejdbcdao;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.seatavailability.ChargeQuoteUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTOBuilder;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author MN
 * @see OndFareDTOBuilder.getQuoteChargesCriteria
 */
public class ChargeQuoteResultSetExtractor implements ResultSetExtractor {

	private final QuoteChargesCriteria _criteria;

	public ChargeQuoteResultSetExtractor(QuoteChargesCriteria criteria) {
		this._criteria = criteria;
	}

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		HashMap<String, QuotedChargeDTO> quotedChargeDTOs = new HashMap<String, QuotedChargeDTO>();

		Double adultOnDFare = null;
		Double childOnDFare = null;
		Double infantOnDFare = null;

		Double adultTotalJourneyFare = null;
		Double childTotalJourneyFare = null;
		Double infantTotalJourneyFare = null;

		if (_criteria.getFareSummaryDTO() != null) {
			adultOnDFare = _criteria.getFareSummaryDTO().getFareAmount(PaxTypeTO.ADULT);
			childOnDFare = _criteria.getFareSummaryDTO().getFareAmount(PaxTypeTO.CHILD);
			infantOnDFare = _criteria.getFareSummaryDTO().getFareAmount(PaxTypeTO.INFANT);
		}

		if (_criteria.getTotalJourneyFare() != null) {
			adultTotalJourneyFare = (Double) _criteria.getTotalJourneyFare().get(PaxTypeTO.ADULT);
			childTotalJourneyFare = (Double) _criteria.getTotalJourneyFare().get(PaxTypeTO.CHILD);
			infantTotalJourneyFare = (Double) _criteria.getTotalJourneyFare().get(PaxTypeTO.INFANT);

			if (AppSysParamsUtil.isRoundupChildAndInfantFaresWhenCalculatingFromAdultFare()
					&& AppSysParamsUtil.getBaseCurrency().equals(_criteria.getFareSummaryDTO().getCurrenyCode())) {
				adultTotalJourneyFare = new BigDecimal(adultTotalJourneyFare).setScale(0, RoundingMode.UP).doubleValue();
				childTotalJourneyFare = new BigDecimal(childTotalJourneyFare).setScale(0, RoundingMode.UP).doubleValue();
				infantTotalJourneyFare = new BigDecimal(infantTotalJourneyFare).setScale(0, RoundingMode.UP).doubleValue();
			}
		}

		List<QuotedChargeDTO> chargesPFS = new ArrayList<QuotedChargeDTO>();

		double surAdult = 0d;
		double surChild = 0d;
		double surInfant = 0d;

		while (rs.next()) {
			QuotedChargeDTO quotedChargeDTO = new QuotedChargeDTO();
			String chargeCode = rs.getString("charge_code");
			quotedChargeDTO.setChargeCode(chargeCode);
			quotedChargeDTO.setChargeDescription(rs.getString("charge_description"));
			quotedChargeDTO.setChargeGroupCode(rs.getString("charge_group_code"));
			quotedChargeDTO.setChargeCategoryCode(rs.getString("charge_category_code"));
			quotedChargeDTO.setMinFlightSearchGap(rs.getInt("min_flight_search_gap"));
			quotedChargeDTO.setApplicableTo(rs.getInt("applies_to"));

			quotedChargeDTO.setSegApplicable(rs.getString("seg_applicable"));
			quotedChargeDTO.setJourneyType(rs.getString("journey_type"));
			quotedChargeDTO.setEffectiveFromDate(rs.getTimestamp("charge_effective_from_date"));
			quotedChargeDTO.setEffectiveToDate(rs.getTimestamp("charge_effective_to_date"));

			quotedChargeDTO.setChargeRateId(rs.getInt("charge_rate_id"));
			String isChargePercentage = rs.getString("value_percentage_flag");// should remove when removing charge
																				// value percentage
			String chargeBasis = rs.getString("charge_basis");
			// double chargeValue = rs.getDouble("value_in_local_currency");
			quotedChargeDTO.setValueInLocalCurrency(rs.getDouble("value_in_local_currency"));
			quotedChargeDTO.setLocalCurrencyCode(rs.getString("local_currency_code"));
			double chargePercentage = rs.getDouble("charge_value_percentage");

			quotedChargeDTO.setChargeValueInPercentage(isChargePercentage != null
					&& isChargePercentage.equals(ChargeRate.CHARGE_BASIS_V) ? false : true);

			quotedChargeDTO.setChargeValuePercentage(chargePercentage);
			int refundable = rs.getInt("refundable_charge_flag");
			quotedChargeDTO.setRefundable(refundable == 1 ? true : false);

			Double effectiveChargeAmount = null;
			// Added new two fields to make upper and lower bound to the charge. If calculated value exceeds the
			// boundary it will get those values
			quotedChargeDTO.setChargeValueMin(rs.getBigDecimal("charge_value_min"));
			quotedChargeDTO.setChargeValueMax(rs.getBigDecimal("charge_value_max"));

			quotedChargeDTO.setBreakPoint(rs.getBigDecimal("break_point"));
			quotedChargeDTO.setBoundryValue(rs.getBigDecimal("boundry_value"));

			// Added new field for calculate the charge. Charge will be calculate base on this value
			quotedChargeDTO.setChargeBasis(chargeBasis);

			// JIRA 6929
			quotedChargeDTO.setReportingChargeGroupCode(rs.getString("rpt_charge_group_code"));

			if (ChargeGroups.TAX.equals(quotedChargeDTO.getChargeGroupCode()) && quotedChargeDTO.isChargeValueInPercentage()
					&& ChargeRate.CHARGE_BASIS_PFS.equals(quotedChargeDTO.getChargeBasis())
					&& _criteria.getFareSummaryDTO() != null) {
				chargesPFS.add(quotedChargeDTO);
			} else {
				if (quotedChargeDTO.getChargeBasis() != null
						&& ((ChargeRate.CHARGE_BASIS_PF.equals(quotedChargeDTO.getChargeBasis()) && _criteria.getFareSummaryDTO() != null) || (ChargeRate.CHARGE_BASIS_PTF
								.equals(quotedChargeDTO.getChargeBasis()) && _criteria.getTotalJourneyFare() != null))) {

					ChargeQuoteUtils.setEffectiveChargeAmount(quotedChargeDTO, PaxTypeTO.ADULT, chargePercentage, adultOnDFare,
							adultTotalJourneyFare, null, null);

					ChargeQuoteUtils.setEffectiveChargeAmount(quotedChargeDTO, PaxTypeTO.CHILD, chargePercentage, childOnDFare,
							childTotalJourneyFare, null, null);

					ChargeQuoteUtils.setEffectiveChargeAmount(quotedChargeDTO, PaxTypeTO.INFANT, chargePercentage, infantOnDFare,
							infantTotalJourneyFare, null, null);
				} else {
					effectiveChargeAmount = new Double(chargePercentage);
					quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.ADULT, effectiveChargeAmount.doubleValue());
					quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.CHILD, effectiveChargeAmount.doubleValue());
					quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.INFANT, effectiveChargeAmount.doubleValue());
				}
			}

			if (ChargeGroups.SURCHARGE.equals(quotedChargeDTO.getChargeGroupCode())) {
				surAdult = surAdult + quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT);
				surChild = surChild + quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD);
				surInfant = surInfant + quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT);
			}

			String isBundledFare = rs.getString("bundled_fare_charge");
			quotedChargeDTO.setBundledFareCharge(isBundledFare != null && "Y".equals(isBundledFare));

			quotedChargeDTOs.put(chargeCode, quotedChargeDTO);
		}

		if (chargesPFS.size() > 0) {
			for (QuotedChargeDTO chg : chargesPFS) {
				ChargeQuoteUtils.setEffectiveChargeAmount(chg, PaxTypeTO.ADULT, chg.getChargeValuePercentage(), adultOnDFare,
						adultTotalJourneyFare, surAdult, null);
				ChargeQuoteUtils.setEffectiveChargeAmount(chg, PaxTypeTO.CHILD, chg.getChargeValuePercentage(), childOnDFare,
						childTotalJourneyFare, surChild, null);
				ChargeQuoteUtils.setEffectiveChargeAmount(chg, PaxTypeTO.INFANT, chg.getChargeValuePercentage(), infantOnDFare,
						infantTotalJourneyFare, surInfant, null);
			}
		}

		return quotedChargeDTOs;
	}
}
