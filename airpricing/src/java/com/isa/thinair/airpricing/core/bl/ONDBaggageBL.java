/**
 * 
 */
package com.isa.thinair.airpricing.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airpricing.api.dto.ONDBaggageTemplateDTO;
import com.isa.thinair.airpricing.api.model.ONDBaggageCharge;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airpricing.api.util.LocalCurrencyUtil;
import com.isa.thinair.airpricing.api.util.SplitUtil;
import com.isa.thinair.airpricing.core.audit.AuditAirpricing;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeJdbcDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.SeatFactorConditionApplyFor;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;

/**
 * @author kasun
 * 
 */
public class ONDBaggageBL {

	private final Log log = LogFactory.getLog(ONDBaggageBL.class);

	private ChargeDAO chargedao;

	private ChargeJdbcDAO chargejdbcdao;

	public ONDBaggageBL() {

		chargedao = (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);

		chargejdbcdao = getChargeJdbcDAO();

	}

	@SuppressWarnings("unused")
	public void saveTemplate(ONDBaggageTemplate baggageTemplate, Integer chargeid) throws ModuleException {
		try {

			chargedao.saveONDBaggageTemplate(baggageTemplate);

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private ChargeJdbcDAO getChargeJdbcDAO() {
		return (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
	}

	public ONDBaggageTemplate createTemplate(ONDBaggageTemplateDTO ondBgDto) throws ModuleException {

		try {
			ONDBaggageTemplate baggageTemplate = new ONDBaggageTemplate();
			baggageTemplate.setDescription(ondBgDto.getDescription().trim());
			baggageTemplate.setTemplateCode(ondBgDto.getTemplateCode().toUpperCase().trim());

			if (ondBgDto.getTemplateId() != null) {
				baggageTemplate.setTemplateId(ondBgDto.getTemplateId());
			}

			baggageTemplate.setVersion(ondBgDto.getVersion());
			baggageTemplate.setCreatedBy(ondBgDto.getCreatedBy());
			baggageTemplate.setCreatedDate(ondBgDto.getCreatedDate());

			if (ondBgDto.getFromDateTmp() != null) {
				baggageTemplate.setFromDate(ondBgDto.getFromDateTmp());
			}
			if (ondBgDto.getToDateTmp() != null) {
				baggageTemplate.setToDate(ondBgDto.getToDateTmp());
			}

			if (ondBgDto.getStatus() != null) {
				baggageTemplate.setStatus(ondBgDto.getStatus());
			} else {
				baggageTemplate.setStatus(Baggage.STATUS_INACTIVE);
			}

			Set<ONDBaggageCharge> bgCharges = new HashSet<ONDBaggageCharge>();

			if (ondBgDto.getBaggageCharges() != null && !ondBgDto.getBaggageCharges().equals("")) {
				String[] arrChages = ondBgDto.getBaggageCharges().split("~");
				String[] strChages = null;

				Map<String, Boolean> defaultBGGStatusForCabinClass = new HashMap<String, Boolean>();
				Map<String, Boolean> defaultBGGStatusForLogicalCabinClass = new HashMap<String, Boolean>();

				for (int i = 0; i < arrChages.length; i++) {

					if (arrChages[i] != null && !arrChages[i].equals("")) {
						strChages = arrChages[i].split(",");

						ONDBaggageCharge baggageChg = new ONDBaggageCharge();

						baggageChg.setBaggageName(strChages[1]);

						if (strChages[2] != null && !strChages[2].trim().equals(""))
							baggageChg.setChargeId(new Integer(strChages[2]));

						baggageChg.setTemplate(baggageTemplate);

						if (strChages[4] != null && !strChages[4].trim().equals(""))
							baggageChg.setBaggageId(new Integer(strChages[4]));

						baggageChg.setLocalCurrencyAmount(new BigDecimal(strChages[5]));// charges defined in local
																						// currency

						baggageChg.setAmount(LocalCurrencyUtil.amountInBaseCurrency(ondBgDto.getLocalCurrCode(),
								baggageChg.getLocalCurrencyAmount()));

						if (strChages[6] != null && !strChages[6].trim().equals(""))
							baggageChg.setAllocatedPieces(new Integer(strChages[6]));

						baggageChg.setVersion(new Long(strChages[7]));
						baggageChg.setStatus(strChages[8]);

						String[] splittedCabinClassAndLogicalCabinClass = SplitUtil
								.getSplittedCabinClassAndLogicalCabinClass(strChages[9]);
						baggageChg.setLogicalCCCode(splittedCabinClassAndLogicalCabinClass[1]);
						baggageChg.setCabinClass(splittedCabinClassAndLogicalCabinClass[0]);
						if (strChages[10] != null && strChages[10].equalsIgnoreCase("Y")) {

							if (baggageChg.getLogicalCCCode() != null) {
								if (defaultBGGStatusForLogicalCabinClass.isEmpty()
										|| defaultBGGStatusForLogicalCabinClass.get(baggageChg.getLogicalCCCode()) == null) {
									defaultBGGStatusForLogicalCabinClass.put(baggageChg.getLogicalCCCode(), true);
									baggageChg.setDefaultBaggageFlag("Y");
								}

							} else if (baggageChg.getCabinClass() != null) {
								if (defaultBGGStatusForCabinClass.isEmpty()
										|| defaultBGGStatusForCabinClass.get(baggageChg.getCabinClass()) == null) {
									defaultBGGStatusForCabinClass.put(baggageChg.getCabinClass(), true);
									baggageChg.setDefaultBaggageFlag("Y");
								}
							}
						} else {
							baggageChg.setDefaultBaggageFlag("N");
						}

						baggageChg.setSeatFactor(new Double(strChages[11]));
						baggageChg.setTotalWeightLimit(new BigDecimal(strChages[12]));
						baggageChg.setSeatFactorLimitationApplyFor(getBaggageSeatFactorApplyFor(strChages[13]));

						bgCharges.add(baggageChg);

					}
				}

			}

			baggageTemplate.setBaggageCharges(bgCharges);
			baggageTemplate.setChargeLocalCurrencyCode(ondBgDto.getLocalCurrCode());
			return baggageTemplate;
		} catch (ModuleException e) {
			log.error("ERROR-Auto Updating Charges upon Exchnage Rate Change Failed");
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private static int getBaggageSeatFactorApplyFor(String seatFactorDescription) {
		if (SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getDescription().equals(seatFactorDescription)) {
			return SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getCondition();
		} else if (CommonsConstants.SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getDescription().equals(
				seatFactorDescription)) {
			return SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getCondition();
		} else if (SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getDescription().equals(seatFactorDescription)) {
			return SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getCondition();
		}
		// return default restriction level
		return CommonsConstants.SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getCondition();
	}

	public void updateONDBaggageChargeBaseCurrAmountOnLocalCurrExchangeRtUpdate(CurrencyExchangeRate currencyExchangeRate,
			UserPrincipal userPrincipal) throws ModuleException {

		try {
			ArrayList<ONDBaggageTemplate> affectedONDBgTMPList;
			BigDecimal newChargeAmount;
			BigDecimal exRate, boundaryValue, breakPointValue;

			affectedONDBgTMPList = (ArrayList<ONDBaggageTemplate>) chargedao.getAffectedONDBaggageTemplates(currencyExchangeRate
					.getCurrency().getCurrencyCode());
			exRate = currencyExchangeRate.getExrateCurToBaseNumber();
			boundaryValue = currencyExchangeRate.getCurrency().getBoundryValue();
			breakPointValue = currencyExchangeRate.getCurrency().getBreakPoint();
			for (ONDBaggageTemplate ondBgTemp : affectedONDBgTMPList) {
				for (ONDBaggageCharge ondBgChrg : ondBgTemp.getBaggageCharges()) {
					if (!ondBgChrg.getStatus().equals(AirPricingCustomConstants.STATUS_ACTIVE)) {
						continue;
					}
					newChargeAmount = AccelAeroRounderPolicy.getRoundedValue(
							AccelAeroCalculator.multiply(ondBgChrg.getLocalCurrencyAmount(), exRate), boundaryValue,
							breakPointValue);
					ondBgChrg.setAmount(newChargeAmount);
				}

			}
			updateONDBaggageTemplate(affectedONDBgTMPList, userPrincipal);

		} catch (ModuleException e) {
			log.error("ERROR-Auto Updating Charges upon Exchnage Rate Change Failed");
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}

	private void updateONDBaggageTemplate(ArrayList<ONDBaggageTemplate> ondBgTMPList, UserPrincipal userPrincipal)
			throws ModuleException {
		Iterator<ONDBaggageTemplate> iterator = ondBgTMPList.iterator();
		try {
			while (iterator.hasNext()) {
				ONDBaggageTemplate ondBgTemplate = iterator.next();
				ondBgTemplate = (ONDBaggageTemplate) PricingUtils.setUserDetails(ondBgTemplate, userPrincipal);
				chargedao.saveONDBaggageTemplate(ondBgTemplate);
				AuditAirpricing.doAuditONDBaggageAddOrModifyTemplate(ondBgTemplate, userPrincipal);
			}
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}

}
