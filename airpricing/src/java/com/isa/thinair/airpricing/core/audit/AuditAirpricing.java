/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airpricing.core.audit;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.model.BaggageCharge;
import com.isa.thinair.airpricing.api.model.BaggageTemplate;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeApplicableCharges;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.FareRulePax;
import com.isa.thinair.airpricing.api.model.MealCharge;
import com.isa.thinair.airpricing.api.model.MealTemplate;
import com.isa.thinair.airpricing.api.model.MultiSelectCategoryRestriction;
import com.isa.thinair.airpricing.api.model.ONDBaggageCharge;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.airpricing.api.model.SSRCharge;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * Class to implement the bussiness logic related to the Flight
 * 
 * @author Byorn
 */
public abstract class AuditAirpricing {

	public static final String FARE_RULE = "FARERULE";
	public static final String CHARGE = "CHARGE";
	public static final String FARE = "FARE";
	public static final int MINUTES_PER_DAY = 1440;
	
	/**sales channel **/
	public enum channelId {
	  All("ALL") ,XBE("XBE") ,IBE("IBE") ,API("API") ;
		private final String status;

		private channelId(String status) {
			this.status = status;
		}
		public String getChannelId() {
			return status;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditSeatMapAddOrModifyTemplate(ChargeTemplate chargeTemplate,UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();
		
		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template code", chargeTemplate.getTemplateCode());
		contents.put("default_price", chargeTemplate.getDefaultChargeAmount());
		contents.put("status", chargeTemplate.getStatus());
		contents.put("currencyCode",chargeTemplate.getChargeLocalCurrencyCode());
		
		if (chargeTemplate.getVersion() < 0) {
			audit.setTaskCode(String.valueOf(TasksUtil.SEATMAP_ADD_TEMPLATE));
		}else{
			audit.setTaskCode(String.valueOf(TasksUtil.SEATMAP_MODIFY_TEMPLATE));
		}
		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditSeatMapDeleteTemplate(String templateCode, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template code", templateCode);

		audit.setTaskCode(String.valueOf(TasksUtil.SEATMAP_DELETE_TEMPLATE));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditMealAddOrModifyTemplate(MealTemplate mealTemplate, UserPrincipal principal) throws ModuleException {
		
		StringBuilder sb = new StringBuilder();
		for (MealCharge mlChg : mealTemplate.getMealCharges()) {
			//sb.append(mlChg.getMealName());
			sb.append((mlChg.getMealName()!=null && !mlChg.getMealName().isEmpty())? mlChg.getMealName() : mlChg.getMealId());
			sb.append(" - ");
			sb.append(mlChg.getAmount());
			sb.append("("+ mlChg.getLocalCurrencyAmount()+")");// local currency amount in brackets
			sb.append(" ");
		}
		StringBuilder sbr = new StringBuilder();
		for(MultiSelectCategoryRestriction restriction : mealTemplate.getCategoryRestrictions()){
			sbr.append(restriction.getMealCategoryId());
			sbr.append(" - ");
		}
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}
		
		StringBuilder sbp = new StringBuilder();
		for (MealCharge mlChg : mealTemplate.getMealCharges()) {			
			sbp.append((mlChg.getMealId()));
			sbp.append(" - ");
			sbp.append(mlChg.getPopularity());			
			sbp.append(" ");
		}
		

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template code", mealTemplate.getTemplateCode());
		contents.put("meal_price", sb.toString());
		contents.put("status", mealTemplate.getStatus());
		contents.put("currencyCode",mealTemplate.getChargeLocalCurrencyCode());
		contents.put("restrict_cat",sbr.toString());
		contents.put("popularity",sbp.toString());
		
		if (mealTemplate.getVersion() < 0) {
			audit.setTaskCode(String.valueOf(TasksUtil.MEAL_ADD_TEMPLATE));
		} else {
			audit.setTaskCode(String.valueOf(TasksUtil.MEAL_MODIFY_TEMPLATE));
		}
		
		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditMealDeleteTemplate(String templateCode, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template code", templateCode);

		audit.setTaskCode(String.valueOf(TasksUtil.MEAL_DELETE_TEMPLATE));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditBaggageDeleteTemplate(String templateCode, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template code", templateCode);

		audit.setTaskCode(String.valueOf(TasksUtil.BAGGAGE_DELETE_TEMPLATE));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditONDBaggageDeleteTemplate(String templateCode, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template code", templateCode);

		audit.setTaskCode(String.valueOf(TasksUtil.OND_BAGGAGE_DELETE_TEMPLATE));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditONDCodesDelete(String templateCode, String ondCode, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template code", templateCode);
		contents.put("ond_code", ondCode);

		audit.setTaskCode(String.valueOf(TasksUtil.OND_CODE_DELETE));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAudit(String code, String type, UserPrincipal principal) throws ModuleException {

		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		if (type.equals(FARE_RULE)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.FARES_DELETE_FARE_RULES));
		}
		if (type.equals(CHARGE)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.FARES_DELETE_CHARGE_CODES));
		}

		AirpricingUtils.getAuditorBD().audit(audit, contents);

	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditRouteWiseDefaultAnciTemplate(int tamplateId, String templateType, int anciTemplId,
			String assignedRoutes, String status, String taskCode, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}
		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template id", tamplateId);
		contents.put("status", status);
		contents.put("anci type", templateType);
		contents.put("template code", anciTemplId);
		contents.put("assigned routes", assignedRoutes);

		audit.setTaskCode(String.valueOf(taskCode));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	/**
	 * NOTE: there are cases where some objects have a sequence number. when saving newly the new number needs to get
	 * audited but the version will also get update. so my doAudit method will do and 'update' audit without 'save' so
	 * before parsing to the doAudit method i am changing the version and reseting it back as i think its safer.
	 */
	public static void doAudit(Persistent persistant, UserPrincipal principal, boolean isSaveMode) throws ModuleException {
		// get the current version
		long versn = persistant.getVersion();

		// if save mode change the version to -1 so that the below doAudit will know if to save or update

		if (isSaveMode) {
			persistant.setVersion(-1);
			doAudit(persistant, principal);
		} else {
			doAudit(persistant, principal);
		}
		// keep the version in its previous state after auditing.
		persistant.setVersion(versn);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAudit(Persistent persistant, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		if (persistant instanceof Charge) {
			Charge charge = (Charge) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.FARES_ADD_CHARGE_CODES));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.FARES_EDIT_CHARGE_CODES));
			}

			contents.put("code", BeanUtils.nullHandler(charge.getChargeCode()));
			contents.put("description", BeanUtils.nullHandler(charge.getChargeDescription()));
			contents.put("status", BeanUtils.nullHandler(charge.getStatus()));
						
			if (charge.getChannelId() ==-1) {
				contents.put("sales channel",channelId.All.getChannelId());
			}else if (charge.getChannelId() == 3) {
				contents.put("sales channel",channelId.XBE.getChannelId());
			}else if (charge.getChannelId() == 4) {
				contents.put("sales channel",channelId.IBE.getChannelId());
			}else if (charge.getChannelId() == 12) {
				contents.put("sales channel",channelId.API.getChannelId());
			}
						
			if (charge.getChargeRates() != null) {
				Iterator chargeRateIter = charge.getChargeRates().iterator();
				int i = 0;
				while (chargeRateIter.hasNext()) {
					ChargeRate chargeRate = (ChargeRate) chargeRateIter.next();
					contents.put("chrg Rt Code " + i, String.valueOf(chargeRate.getChargeRateCode()));
					contents.put("effect from" + i, BeanUtils.nullHandler(chargeRate.getChargeEffectiveFromDate()));
					contents.put("effect to" + i, BeanUtils.nullHandler(chargeRate.getChargeEffectiveToDate()));
					contents.put("value" + i, BeanUtils.nullHandler(chargeRate.getValueInLocalCurrency()));
					contents.put("chrg Rt Status" + i,BeanUtils.nullHandler(chargeRate.getStatus()));
					i = i + 1;
				}
			} else {
				contents.put("charge rate", "none");
			}
			// contents.put(contents, charge.get
			if (charge.getApplicableCharges() != null) {
				Iterator applicableChargesIter = charge.getApplicableCharges().iterator();
				int i = 0;
				while (applicableChargesIter.hasNext()) {
					ChargeApplicableCharges applicableCharges = (ChargeApplicableCharges) applicableChargesIter.next();
					contents.put(" Applicable Charge Code " + i, String.valueOf(applicableCharges.getApplicableChargeCode()));
					contents.put(" Applicable Charge Status" + i, BeanUtils.nullHandler(applicableCharges.getApplyStatus()));
					i = i + 1;
				}
			} else {
				contents.put("Applicable Charges", "none");
			}
			
			if (charge.getChargeCategoryCode().equals("SERVICE_TAX")) {
				if (charge.getCountryCode() != null) {
					contents.put("charge service tax country", charge.getCountryCode());
				}
				if (charge.getStateId() != null) {
					contents.put("charge service tax StateId", charge.getStateId());
				}
				if (charge.getInterState() != null) {
					contents.put("charge service tax interState", charge.getInterState());
				}
			} else {
				contents.put("Service Tax Selected", "false");
			}
			
		}

		if (persistant instanceof Fare) {
			Fare obj = (Fare) persistant;
			FareDTO existingFare = null;

			if (persistant.getVersion() < 0) {
				if (obj.isUploaded())
					audit.setTaskCode(String.valueOf(TasksUtil.FARES_UPLOAD_ADD));
				else
					audit.setTaskCode(String.valueOf(TasksUtil.FARES_ADD_FARE));
			} else {
				existingFare = AirpricingUtils.getFareBD().getFare(obj.getFareId());
				if (obj.isUploaded())
					audit.setTaskCode(String.valueOf(TasksUtil.FARES_UPLOAD_UPDATE));
				else
					audit.setTaskCode(String.valueOf(TasksUtil.FARES_EDIT_FARE));
			}

			contents.put("fare id", String.valueOf(obj.getFareId()));
			contents.put("booking code", BeanUtils.nullHandler(obj.getBookingCode()));
			contents.put("status", BeanUtils.nullHandler(obj.getStatus()));
			contents.put("effective from", BeanUtils.nullHandler(obj.getEffectiveFromDate()));
			contents.put("effective to", BeanUtils.nullHandler(obj.getEffectiveToDate()));
			contents.put("ond code", BeanUtils.nullHandler(obj.getOndCode()));
			contents.put("fare amount", String.valueOf(obj.getFareAmount()));
			contents.put("fare rule id", String.valueOf(obj.getFareRuleId()));
			contents.put("child fare type", BeanUtils.nullHandler(obj.getChildFareType()));
			contents.put("child fare value", String.valueOf(obj.getChildFare()));
			contents.put("infant fare type", BeanUtils.nullHandler(obj.getInfantFareType()));
			contents.put("infant fare value", String.valueOf(obj.getInfantFare()));

			if (existingFare != null) {
				try {
					contents.put("changedContent", populateChangedContent(existingFare.getFare(), obj));
				} catch (ParseException e) {
					e.printStackTrace();
				}

			}

		}

		if (persistant instanceof FareRule) {
			FareRule obj = (FareRule) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.FARES_ADD_FARE_RULES));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.FARES_EDIT_FARE_RULES));
			}

			contents.put("fare rule id", String.valueOf(obj.getFareRuleId()));
			contents.put("fr code", BeanUtils.nullHandler(obj.getFareRuleCode()));
			contents.put("des", BeanUtils.nullHandler(obj.getFareRuleDescription()));
			int advBkTime = (obj.getAdvanceBookingDays() == null) ? 0 : obj.getAdvanceBookingDays();
			String[] advBKTimeDDHHMM = PricingUtils.getAdvBkDDHHMM(advBkTime);
			contents.put("adv bking days(Days:Hours:Minutes)",
					BeanUtils.nullHandler(advBKTimeDDHHMM[0] + ":" + advBKTimeDDHHMM[1] + ":" + advBKTimeDDHHMM[2]));
			contents.put("fare basis code", BeanUtils.nullHandler(obj.getFareBasisCode()));

			Long maximumStayover = (obj.getMaximumStayoverMins() == null) ? 0 : obj.getMaximumStayoverMins();
			String[] maxStayOverTimeDDHHMM = PricingUtils.getStayOverDDHHMM(maximumStayover);
			String maximumStayoverMonths = (obj.getMaximumStayoverMonths() == null) ? "0" : String.valueOf(obj
					.getMaximumStayoverMonths());
			contents.put(
					"Max stay over(Months:Days:Hours:Minutes)",
					BeanUtils.nullHandler(maximumStayoverMonths + ":" + maxStayOverTimeDDHHMM[0] + ":" + maxStayOverTimeDDHHMM[1]
							+ ":" + maxStayOverTimeDDHHMM[2]));

			Long minimumStayover = (obj.getMinimumStayoverMins() == null) ? 0 : obj.getMinimumStayoverMins();
			String[] minStayOverTimeDDHHMM = PricingUtils.getStayOverDDHHMM(minimumStayover);
			String minimumStayoverMonths = (obj.getMinimumStayoverMonths() == null) ? "0" : String.valueOf(obj
					.getMinimumStayoverMonths());
			contents.put(
					"Min stay over(Months:Days:Hours:Minutes)",
					BeanUtils.nullHandler(minimumStayoverMonths + ":" + minStayOverTimeDDHHMM[0] + ":" + minStayOverTimeDDHHMM[1]
							+ ":" + minStayOverTimeDDHHMM[2]));

			contents.put("outbound dep time from", BeanUtils.nullHandler(obj.getOutboundDepatureTimeFrom()));
			contents.put("outbound dep time to", BeanUtils.nullHandler(obj.getOutboundDepatureTimeTo()));
			contents.put("inbound dep time from", BeanUtils.nullHandler(obj.getInboundDepatureTimeFrom()));
			contents.put("inbound dep time to", BeanUtils.nullHandler(obj.getInboundDepatureTimeTo()));
			contents.put("status", BeanUtils.nullHandler(obj.getStatus()));

			if (obj.getVisibileAgentCodes() != null) {
				Set set = obj.getVisibileAgentCodes();
				Iterator iterator = set.iterator();
				int i = 0;
				while (iterator.hasNext()) {
					contents.put("agent code" + i, iterator.next());
					i = i + 1;
				}
			}

			if (obj.getVisibleChannelIds() != null) {
				Set setCns = obj.getVisibleChannelIds();
				Iterator iteratorCns = setCns.iterator();
				int i = 0;
				while (iteratorCns.hasNext()) {

					contents.put("channel id" + i, iteratorCns.next());
					i = i + 1;
				}
			}

			contents.put("cnx chrg amnt", String.valueOf(obj.getCancellationChargeAmount()));
			contents.put("mod chrg amnt", String.valueOf(obj.getModificationChargeAmount()));
			contents.put("return ?", String.valueOf(obj.getReturnFlag()));
			Collection col = obj.getPaxDetails();
			Iterator Itr = col.iterator();
			HashMap hmPaxRefundable = new HashMap();
			while (Itr.hasNext()) {
				FareRulePax oFareRulePax = (FareRulePax) Itr.next();
				hmPaxRefundable.put(oFareRulePax.getPaxType(), String.valueOf(oFareRulePax.getRefundableFares()));
			}
			contents.put(
					"refundable (AD/CH/IN) ?",
					hmPaxRefundable.get(PaxTypeTO.ADULT) + "/" + hmPaxRefundable.get(PaxTypeTO.CHILD) + "/"
							+ hmPaxRefundable.get(PaxTypeTO.INFANT));

		}

		if (persistant instanceof FareRule) {
			FareRule obj = (FareRule) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.FARES_ADD_FARE_RULES));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.FARES_EDIT_FARE_RULES));
			}

			contents.put("fare rule id", String.valueOf(obj.getFareRuleId()));
			contents.put("fr code", BeanUtils.nullHandler(obj.getFareRuleCode()));
			contents.put("des", BeanUtils.nullHandler(obj.getFareRuleDescription()));
			contents.put("adv bking days", BeanUtils.nullHandler(obj.getAdvanceBookingDays()));
			contents.put("fare basis code", BeanUtils.nullHandler(obj.getFareBasisCode()));

			Long maximumStayover = (obj.getMaximumStayoverMins() == null) ? 0 : obj.getMaximumStayoverMins();
			String[] maxStayOverTimeDDHHMM = PricingUtils.getStayOverDDHHMM(maximumStayover);
			String maximumStayoverMonths = (obj.getMaximumStayoverMonths() == null) ? "0" : String.valueOf(obj
					.getMaximumStayoverMonths());
			contents.put(
					"Max stay over(Months:Days:Hours:Minutes)",
					BeanUtils.nullHandler(maximumStayoverMonths + ":" + maxStayOverTimeDDHHMM[0] + ":" + maxStayOverTimeDDHHMM[1]
							+ ":" + maxStayOverTimeDDHHMM[2]));

			Long minimumStayover = (obj.getMinimumStayoverMins() == null) ? 0 : obj.getMinimumStayoverMins();
			String[] minStayOverTimeDDHHMM = PricingUtils.getStayOverDDHHMM(minimumStayover);
			String minimumStayoverMonths = (obj.getMinimumStayoverMonths() == null) ? "0" : String.valueOf(obj
					.getMinimumStayoverMonths());
			contents.put(
					"Min stay over(Months:Days:Hours:Minutes)",
					BeanUtils.nullHandler(minimumStayoverMonths + ":" + minStayOverTimeDDHHMM[0] + ":" + minStayOverTimeDDHHMM[1]
							+ ":" + minStayOverTimeDDHHMM[2]));

			contents.put("outbound dep time from", BeanUtils.nullHandler(obj.getOutboundDepatureTimeFrom()));
			contents.put("outbound dep time to", BeanUtils.nullHandler(obj.getOutboundDepatureTimeTo()));
			contents.put("inbound dep time from", BeanUtils.nullHandler(obj.getInboundDepatureTimeFrom()));
			contents.put("inbound dep time to", BeanUtils.nullHandler(obj.getInboundDepatureTimeTo()));
			contents.put("status", BeanUtils.nullHandler(obj.getStatus()));

			if (obj.getVisibileAgentCodes() != null) {
				Set set = obj.getVisibileAgentCodes();
				Iterator iterator = set.iterator();
				int i = 0;
				while (iterator.hasNext()) {
					contents.put("agent code" + i, iterator.next());
					i = i + 1;
				}
			}

			if (obj.getVisibleChannelIds() != null) {
				Set setCns = obj.getVisibleChannelIds();
				Iterator iteratorCns = setCns.iterator();
				int i = 0;
				while (iteratorCns.hasNext()) {

					contents.put("channel id" + i, iteratorCns.next());
					i = i + 1;
				}
			}

			// contents.put("cnx chrg amnt", String.valueOf(obj.getCancellationChargeAmount()));
			// contents.put("mod chrg amnt", String.valueOf(obj.getModificationChargeAmount()));
			contents.put("return ?", String.valueOf(obj.getReturnFlag()));
			if (obj.getPaxDetails() != null) {
				Collection col = obj.getPaxDetails();
				Iterator Itr = col.iterator();
				HashMap hmPaxRefundable = new HashMap();
				while (Itr.hasNext()) {
					FareRulePax oFareRulePax = (FareRulePax) Itr.next();
					hmPaxRefundable.put(oFareRulePax.getPaxType(), String.valueOf(oFareRulePax.getRefundableFares()));
				}
				contents.put(
						"refundable (AD/CH/IN) ?",
						hmPaxRefundable.get(PaxTypeTO.ADULT) + "/" + hmPaxRefundable.get(PaxTypeTO.CHILD) + "/"
								+ hmPaxRefundable.get(PaxTypeTO.INFANT));
			}

		}

		AirpricingUtils.getAuditorBD().audit(audit, contents);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditBaggageAddOrModifyTemplate(BaggageTemplate baggageTemplate, UserPrincipal principal) throws ModuleException {
		
		StringBuilder sb = new StringBuilder();
		for (BaggageCharge bgChg : baggageTemplate.getBaggageCharges()) {
			 
			//sb.append(bgChg.getBaggageName());
			sb.append((bgChg.getBaggageName()!=null && !bgChg.getBaggageName().isEmpty())? bgChg.getBaggageName() : bgChg.getBaggageId());
			sb.append(" - ");
			sb.append(bgChg.getAmount());
			sb.append("("+bgChg.getLocalCurrencyAmount()+")");// local currency amount in brackets
			sb.append(" ");
		}
		
		Audit audit = new Audit();
		
		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template code", baggageTemplate.getTemplateCode());
		contents.put("baggage_price", sb.toString());
		contents.put("status", baggageTemplate.getStatus());
		contents.put("currencyCode", baggageTemplate.getChargeLocalCurrencyCode());
		
		if (baggageTemplate.getVersion() < 0) {
			audit.setTaskCode(String.valueOf(TasksUtil.BAGGAGE_ADD_TEMPLATE));
		} else {
			audit.setTaskCode(String.valueOf(TasksUtil.BAGGAGE_MODIFY_TEMPLATE));
		}
		

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditONDBaggageAddOrModifyTemplate(ONDBaggageTemplate baggageTemplate, UserPrincipal principal) throws ModuleException {
		
		Audit audit = new Audit();
		
		StringBuilder sb = new StringBuilder();
		for (ONDBaggageCharge bgChg : baggageTemplate.getBaggageCharges()) {
			
			//sb.append(bgChg.getBaggageName());
			sb.append((bgChg.getBaggageName()!=null && !bgChg.getBaggageName().isEmpty())? bgChg.getBaggageName() : bgChg.getBaggageId());
			sb.append(" - ");
			sb.append(bgChg.getAmount());
			sb.append("("+bgChg.getLocalCurrencyAmount()+")");// local currency amount in brackets
			sb.append(" ");
		}
		
		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template code", baggageTemplate.getTemplateCode());
		contents.put("baggage_price", sb.toString());
		contents.put("status", baggageTemplate.getStatus());
		contents.put("currencyCode", baggageTemplate.getChargeLocalCurrencyCode());
		
		if (baggageTemplate.getVersion() < 0) {
			audit.setTaskCode(String.valueOf(TasksUtil.OND_BAGGAGE_ADD_TEMPLATE));
		}else{
			audit.setTaskCode(String.valueOf(TasksUtil.OND_BAGGAGE_MODIFY_TEMPLATE));
		}
		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditONDCodeAdd(String templateCode, String strOndCode, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template code", templateCode);
		contents.put("ondCode", strOndCode);

		audit.setTaskCode(String.valueOf(TasksUtil.OND_CODE_ADD));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditONDCodeModify(String templateCode, String ondCode, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template code", templateCode);
		contents.put("ondCode", ondCode);

		audit.setTaskCode(String.valueOf(TasksUtil.OND_CODE_MODIFY));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	// SSR Charges audit
	/**
	 * auditFor 1-add,2-modify
	 * */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditAddModifySSRCharge(SSRCharge ssrCharge, UserPrincipal principal) throws ModuleException {
		
		StringBuilder sb = new StringBuilder();
		sb.append(ssrCharge.getSsrId());
		sb.append("-");
		sb.append(ssrCharge.getApplicablityType());
		sb.append("-");
		sb.append(ssrCharge.getAdultAmount());
		sb.append("("+ssrCharge.getLocalCurrAdultAmount()+")");
		sb.append("-");
		sb.append(ssrCharge.getChildAmount());
		sb.append("("+ssrCharge.getLocalCurrChildAmount()+")");
		sb.append("-");
		sb.append(ssrCharge.getInfantAmount());
		sb.append("("+ssrCharge.getLocalCurrInfantAmount()+")");
		sb.append("-");
		sb.append(ssrCharge.getReservationAmount());
		sb.append("("+ssrCharge.getLocalCurrReservationAmount()+")"); // local currency amount in brackets
		sb.append(" ");
		
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("charge code", ssrCharge.getSsrChargeCode());
		contents.put("ssr_price", sb.toString());
		contents.put("status", ssrCharge.getStatus());
		contents.put("currencyCode", ssrCharge.getchargeLocalCurrencyCode());

		if (ssrCharge.getVersion() < 0) {
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_SSR_CHARGE));
		} else {
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_MODIFY_SSR_CHARGE));
		}

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditDeleteSSRCharge(String chargeCode, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("charge code", chargeCode);

		audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_SSR_CHARGE));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	public static String populateChangedContent(Fare existingFare, Fare modifiedFare) throws ParseException {

		String changedContent = "";
		String separator = " ";

		if (existingFare.getFareAmountInLocal() != null && modifiedFare.getFareAmountInLocal() != null
				&& Double.compare(existingFare.getFareAmountInLocal(), modifiedFare.getFareAmountInLocal()) != 0) {
			changedContent += "fare amount changed from " + blankHandler(existingFare.getFareAmountInLocal()) + " to "
					+ blankHandler(modifiedFare.getFareAmountInLocal()) + separator;
		} else if (Double.compare(existingFare.getFareAmount(), modifiedFare.getFareAmount()) != 0) {
			changedContent += "fare amount changed from " + blankHandler(existingFare.getFareAmount()) + " to "
					+ blankHandler(modifiedFare.getFareAmount()) + separator;
		}

		if (!((existingFare.getEffectiveFromDate() != null) ? CalendarUtil.formatForSQL(existingFare.getEffectiveFromDate(),
				CalendarUtil.PATTERN4).toString() : "").equals(((modifiedFare.getEffectiveFromDate() != null) ? CalendarUtil
				.formatForSQL(modifiedFare.getEffectiveFromDate(), CalendarUtil.PATTERN4).toString() : ""))) {

			changedContent += "effective from date changed from "
					+ ((existingFare.getEffectiveFromDate() != null) ? CalendarUtil.formatForSQL(
							existingFare.getEffectiveFromDate(), CalendarUtil.PATTERN4).toString() : "")
					+ " to "
					+ ((modifiedFare.getEffectiveFromDate() != null) ? CalendarUtil.formatForSQL(
							modifiedFare.getEffectiveFromDate(), CalendarUtil.PATTERN4).toString() : "") + separator;
		}

		if (!((existingFare.getEffectiveToDate() != null) ? CalendarUtil.formatForSQL(existingFare.getEffectiveToDate(),
				CalendarUtil.PATTERN4).toString() : "").equals(((modifiedFare.getEffectiveToDate() != null) ? CalendarUtil
				.formatForSQL(modifiedFare.getEffectiveToDate(), CalendarUtil.PATTERN4).toString() : ""))) {

			changedContent += "effective to date changed from "
					+ ((existingFare.getEffectiveToDate() != null) ? CalendarUtil.formatForSQL(existingFare.getEffectiveToDate(),
							CalendarUtil.PATTERN4).toString() : "")
					+ " to "
					+ ((modifiedFare.getEffectiveToDate() != null) ? CalendarUtil.formatForSQL(modifiedFare.getEffectiveToDate(),
							CalendarUtil.PATTERN4).toString() : "") + separator;
		}

		if (!((existingFare.getReturnEffectiveFromDate() != null) ? CalendarUtil.formatForSQL(
				existingFare.getReturnEffectiveFromDate(), CalendarUtil.PATTERN4).toString() : "").equals(((modifiedFare
				.getReturnEffectiveFromDate() != null) ? CalendarUtil.formatForSQL(modifiedFare.getReturnEffectiveFromDate(),
				CalendarUtil.PATTERN4).toString() : ""))) {

			changedContent += "return effective from date changed from "
					+ ((existingFare.getReturnEffectiveFromDate() != null) ? CalendarUtil.formatForSQL(
							existingFare.getReturnEffectiveFromDate(), CalendarUtil.PATTERN4).toString() : "")
					+ " to "
					+ ((modifiedFare.getReturnEffectiveFromDate() != null) ? CalendarUtil.formatForSQL(
							modifiedFare.getReturnEffectiveFromDate(), CalendarUtil.PATTERN4).toString() : "") + separator;
		}

		if (!((existingFare.getReturnEffectiveToDate() != null) ? CalendarUtil.formatForSQL(
				existingFare.getReturnEffectiveToDate(), CalendarUtil.PATTERN4).toString() : "").equals(((modifiedFare
				.getReturnEffectiveToDate() != null) ? CalendarUtil.formatForSQL(modifiedFare.getReturnEffectiveToDate(),
				CalendarUtil.PATTERN4).toString() : ""))) {

			changedContent += "return effective to date changed from "
					+ ((existingFare.getReturnEffectiveToDate() != null) ? CalendarUtil.formatForSQL(
							existingFare.getReturnEffectiveToDate(), CalendarUtil.PATTERN4).toString() : "")
					+ " to "
					+ ((modifiedFare.getReturnEffectiveToDate() != null) ? CalendarUtil.formatForSQL(
							modifiedFare.getReturnEffectiveToDate(), CalendarUtil.PATTERN4).toString() : "") + separator;
		}

		if (!((existingFare.getSalesEffectiveFrom() != null) ? CalendarUtil.formatForSQL(existingFare.getSalesEffectiveFrom(),
				CalendarUtil.PATTERN4).toString() : "").equals(((modifiedFare.getSalesEffectiveFrom() != null) ? CalendarUtil
				.formatForSQL(modifiedFare.getSalesEffectiveFrom(), CalendarUtil.PATTERN4).toString() : ""))) {

			changedContent += "sales effective from date changed from "
					+ ((existingFare.getSalesEffectiveFrom() != null) ? CalendarUtil.formatForSQL(
							existingFare.getSalesEffectiveFrom(), CalendarUtil.PATTERN4).toString() : "")
					+ " to "
					+ ((modifiedFare.getSalesEffectiveFrom() != null) ? CalendarUtil.formatForSQL(
							modifiedFare.getSalesEffectiveFrom(), CalendarUtil.PATTERN4).toString() : "") + separator;
		}

		if (!((existingFare.getSalesEffectiveTo() != null) ? CalendarUtil.formatForSQL(existingFare.getSalesEffectiveTo(),
				CalendarUtil.PATTERN4).toString() : "").equals(((modifiedFare.getSalesEffectiveTo() != null) ? CalendarUtil
				.formatForSQL(modifiedFare.getSalesEffectiveTo(), CalendarUtil.PATTERN4).toString() : ""))) {

			changedContent += "sales effective to date changed from "
					+ ((existingFare.getSalesEffectiveTo() != null) ? CalendarUtil.formatForSQL(
							existingFare.getSalesEffectiveTo(), CalendarUtil.PATTERN4).toString() : "")
					+ " to "
					+ ((modifiedFare.getSalesEffectiveTo() != null) ? CalendarUtil.formatForSQL(
							modifiedFare.getSalesEffectiveTo(), CalendarUtil.PATTERN4).toString() : "") + separator;
		}

		if (!PlatformUtiltiies.nullHandler(existingFare.getFareRuleId()).equals(
				PlatformUtiltiies.nullHandler(modifiedFare.getFareRuleId()))) {
			changedContent += "fare rule id changed from " + blankHandler(existingFare.getFareRuleId()) + " to "
					+ blankHandler(modifiedFare.getFareRuleId()) + separator;
		}

		if (!PlatformUtiltiies.nullHandler(existingFare.getBookingCode()).equals(
				PlatformUtiltiies.nullHandler(modifiedFare.getBookingCode()))) {
			changedContent += "booking code changed from " + blankHandler(existingFare.getBookingCode()) + " to "
					+ blankHandler(modifiedFare.getBookingCode()) + separator;
		}

		if (!PlatformUtiltiies.nullHandler(existingFare.getMasterFareID()).equals(
				PlatformUtiltiies.nullHandler(modifiedFare.getMasterFareID()))) {
			changedContent += "master fare id changed from " + blankHandler(existingFare.getMasterFareID()) + " to "
					+ blankHandler(modifiedFare.getMasterFareID()) + separator;
		}

		if (!PlatformUtiltiies.nullHandler(existingFare.getMasterFarePercentage()).equals(
				PlatformUtiltiies.nullHandler(modifiedFare.getMasterFarePercentage()))) {
			changedContent += "master fare percentage changed from " + blankHandler(existingFare.getMasterFarePercentage())
					+ " to " + blankHandler(modifiedFare.getMasterFarePercentage()) + separator;
		}

		if (!PlatformUtiltiies.nullHandler(existingFare.getMasterFareRefCode()).equals(
				PlatformUtiltiies.nullHandler(modifiedFare.getMasterFareRefCode()))) {
			changedContent += "master fare ref code changed from " + blankHandler(existingFare.getMasterFareRefCode()) + " to "
					+ blankHandler(modifiedFare.getMasterFareRefCode()) + separator;
		}

		if (!PlatformUtiltiies.nullHandler(existingFare.getMasterFareRefCode()).equals(
				PlatformUtiltiies.nullHandler(modifiedFare.getMasterFareRefCode()))) {
			changedContent += "master fare ref code changed from " + blankHandler(existingFare.getMasterFareRefCode()) + " to "
					+ blankHandler(modifiedFare.getMasterFareRefCode()) + separator;
		}

		if (!PlatformUtiltiies.nullHandler(existingFare.getChildFare()).equals(
				PlatformUtiltiies.nullHandler(modifiedFare.getChildFare()))) {
			changedContent += "child fare changed from " + blankHandler(existingFare.getChildFare()) + " to "
					+ blankHandler(modifiedFare.getChildFare()) + separator;
		}

		if (!PlatformUtiltiies.nullHandler(existingFare.getChildFareType()).equals(
				PlatformUtiltiies.nullHandler(modifiedFare.getChildFareType()))) {
			changedContent += "child fare type changed from" + blankHandler(existingFare.getChildFareType()) + " to "
					+ blankHandler(modifiedFare.getChildFareType()) + separator;
		}

		if (!PlatformUtiltiies.nullHandler(existingFare.getInfantFare()).equals(
				PlatformUtiltiies.nullHandler(modifiedFare.getInfantFare()))) {
			changedContent += "infant fare changed from " + blankHandler(existingFare.getInfantFare()) + " to "
					+ blankHandler(modifiedFare.getInfantFare()) + separator;
		}

		if (!PlatformUtiltiies.nullHandler(existingFare.getInfantFareType()).equals(
				PlatformUtiltiies.nullHandler(modifiedFare.getInfantFareType()))) {
			changedContent += "infant fare type changed from " + blankHandler(existingFare.getInfantFareType()) + " to "
					+ blankHandler(modifiedFare.getInfantFareType()) + separator;
		}

		return changedContent;
	}

	private static String blankHandler(Object object) {
		if (object == null) {
			return "none";
		} else {
			if ("".equals(object.toString().trim())) {
				return "none";
			} else {
				return object.toString().trim();
			}
		}
	}

}
