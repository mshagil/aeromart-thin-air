package com.isa.thinair.airpricing.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airpricing.api.service.CommonAncillaryServiceBD;

@Local
public interface CommonAncillaryServiceLocalDeligateImpl extends CommonAncillaryServiceBD {

}
