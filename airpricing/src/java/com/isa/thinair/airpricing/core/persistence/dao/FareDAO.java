/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.dao;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;

/**
 * BHive Commons: FareRule module
 * 
 * @author Byorn
 */
public interface FareDAO {

	/** Interface method to save a Fare **/
	public void saveFare(Fare fare);

	/** Interface method to save a collection of Fares **/
	public void saveOrUpdateFare(Collection<Fare> fares);

	/** Interface method to Check if FareRule, Booking Class, OND Combination Exists for the time period **/
	public boolean isFareLegal(String fareRuleCode, String bookingCode, String ONDCode, Date efctFrom, Date efctTo,
			Date salesEfctFrom, Date salesEfctTo, Date returnFromDate, Date returnToDate, boolean retrunFlag);

	/**
	 * Interface method to Check if FareRule, Booking Class, OND Combination Exists , and time period except in the
	 * specifiied fareID
	 **/
	public boolean isFareLegal(String fareRuleCode, String bookingCode, String ONDCode, Integer fareID, Date efctFrom,
			Date efctTo, Date salesEfctFrom, Date salesEfctTo, Date returnFromDate, Date returnToDate, boolean retrunFlag);

	/**
	 * Interface method to get fares if FareRule, Booking Class, OND Combination Exists for the time period and get
	 * records if any
	 **/
	public Collection<Object[]> getFare(String fareRuleCode, String bookingCode, String ONDCode, Date efctFrom, Date efctTo,
			Date salesEfctFrom, Date salesEfctTo, Date returnFromDate, Date returnToDate);

	/** Interface method to get a Fare **/
	public Fare getFare(int fareID);

	public Map<Integer, Fare> getFares(Collection<Integer> fareIds);

	/** Interface method to delete a Fare **/
	public void deleteFare(int fareId);

	/** Interface method to get Effective Fares **/
	public Collection<Fare> getEffectiveFares(Collection<String> bookingCodes, String ondCode, Date depatureDate);

	/** Interface method to get Effective Fares with FareRUle **/
	public Collection<Object[]> getEffectiveFaresWithFareRule(Collection<String> bookingCodes, String ondCode, Date depatureDate);

	/**
	 * Return fare rules linked to specified fares.
	 * 
	 * @param fareIds
	 * @return
	 */
	public HashMap<Integer, FareRule> getFareRules(Collection<Integer> fareIds);

	/** Interface method to get Fares With Local Currency **/
	public Collection<Fare> getFaresWithLocalCurrency(Date executionDate);

	public HashMap<Integer, String> getMasterFares(String ondCode);

	public boolean isMasterFareLinked(Integer masterFareID);

	public boolean isMasterFareRefCodeAlreayUsed(String masterFareRefCode);

	public Collection<Fare> getLinkFares(Integer masterFareID);

	public List<Fare> getApplicableFaresForUpdateOnExchangeRateChange(String currencyCode);

	public Collection<Fare> getFaresByFareRuleId(Integer fareRuleId);
}
