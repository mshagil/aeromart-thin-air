package com.isa.thinair.airpricing.core.remoting.ejb;

import java.util.ArrayList;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.bl.BaggageBusinessLayer;
import com.isa.thinair.airpricing.core.bl.ChargeBL;
import com.isa.thinair.airpricing.core.bl.MealBL;
import com.isa.thinair.airpricing.core.bl.ONDBaggageBL;
import com.isa.thinair.airpricing.core.bl.SSRChargeBL;
import com.isa.thinair.airpricing.core.service.bd.AnciChargeLocalCurrBDImpl;
import com.isa.thinair.airpricing.core.service.bd.AnciChargeLocalCurrBDLocalImpl;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * 
 * @author subash
 */

@Stateless
@RemoteBinding(jndiBinding = "AnciChargeLocalCurrService.remote")
@LocalBinding(jndiBinding = "AnciChargeLocalCurrService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class AnciChargeLocalCurrServiceBean extends PlatformBaseSessionBean implements AnciChargeLocalCurrBDImpl,
		AnciChargeLocalCurrBDLocalImpl {

	private final Log log = LogFactory.getLog(getClass());

	private ArrayList<CurrencyExchangeRate> updatedCurrencyExchangeList;

	@Override
	public void baseCurrAmountUpdateOnLocalCurrExchangeRtUpdate() throws ModuleException {
		try {
			updatedCurrencyExchangeList = (ArrayList<CurrencyExchangeRate>) AirpricingUtils.getCurrencyDAO().getExchangeRatesForAutomatedUpdate(CalendarUtil.getCurrentSystemTimeInZulu(), InternalConstants.ANCICHARGE_UPDATED_FOR_EXRATE_CHNG);
				//	.getExchangeRatesForAutomatedAnciChargeUpdate(CalendarUtil.getCurrentSystemTimeInZulu());
			if (updatedCurrencyExchangeList.isEmpty()) {

				log.debug("No Currency Exchange Rates are Available for Automatic Charge Update");

			} else {
				for (CurrencyExchangeRate currencyExchangeRate : updatedCurrencyExchangeList) {
					try{
						updateONDBaggageChargeBaseCurrAmount(currencyExchangeRate);
						updateBaggageChargeBaseCurrAmount(currencyExchangeRate);
						updateMealChargeBaseCurrAmount(currencyExchangeRate);
						updateSSRChargeBaseCurrAmount(currencyExchangeRate);
						updateSeatChargeBaseCurrAmount(currencyExchangeRate);
						
						currencyExchangeRate.setAnciChargesUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_ENABLED);
						
					}catch(Exception e){
						log.error("Error occur at update anci template amount at exchange rate change :" + e.getCause());
						currencyExchangeRate.setAnciChargesUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);
					}					
				}
				AirpricingUtils.getCurrencyDAO().saveOrUpdateCurrencyExRates(updatedCurrencyExchangeList);
			}
		} catch (Exception e) {
			log.error("Error occur at update anci template amount at exchange rate change :" + e.getCause());
		}

	}

	private void updateONDBaggageChargeBaseCurrAmount(CurrencyExchangeRate currencyExchangeRate) throws ModuleException {
		try {
			new ONDBaggageBL().updateONDBaggageChargeBaseCurrAmountOnLocalCurrExchangeRtUpdate(currencyExchangeRate,
					getUserPrincipal());
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private void updateBaggageChargeBaseCurrAmount(CurrencyExchangeRate currencyExchangeRate) throws ModuleException {
		try {
			new BaggageBusinessLayer().updateBaggageChargeBaseCurrAmountOnLocalCurrExchangeRtUpdate(currencyExchangeRate,
					getUserPrincipal());
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private void updateMealChargeBaseCurrAmount(CurrencyExchangeRate currencyExchangeRate) throws ModuleException {
		try {
			new MealBL().updateMealChargeBaseCurrAmountOnLocalCurrExchangeRtUpdate(currencyExchangeRate,
					getUserPrincipal());
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private void updateSSRChargeBaseCurrAmount(CurrencyExchangeRate currencyExchangeRate) throws ModuleException {
		try {
			new SSRChargeBL().updateSSRChargeBaseCurrAmountOnLocalCurrExchangeRtUpdate(currencyExchangeRate,
					getUserPrincipal());
		} catch (ModuleException e) {
			throw new ModuleException(e, "airpricing.desc");
		}
	}

	private void updateSeatChargeBaseCurrAmount(CurrencyExchangeRate currencyExchangeRate) throws ModuleException {
		try {
			new ChargeBL().updateSeatChargeBaseCurrAmountOnLocalCurrExchangeRtUpdate(currencyExchangeRate,
					getUserPrincipal());
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

}
