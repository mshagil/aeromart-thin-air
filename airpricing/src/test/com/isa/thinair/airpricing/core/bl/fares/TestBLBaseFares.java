/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.bl.fares;

import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.core.persistence.dao.FareDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Byorn
 */
public class TestBLBaseFares extends PlatformTestCase {
	
	
	
	public FareRuleDAO fareRuleDAO;
	public FareDAO fareDAO;
	public FareJdbcDAO fareJdbcDAO;
	
	public TestBLBaseFares() {
		super();
		fareRuleDAO = (FareRuleDAO)AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_RULE_DAO);
		fareDAO = (FareDAO)AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_DAO);
		fareJdbcDAO = (FareJdbcDAO)AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.MANAGE_FARES);		
	}
    
    public FareBD getFareService()
    { 
//        LookupService lookup = LookupServiceFactory.getInstance();
//        IModule airpricingModule = lookup.getModule("airpricing");
//        System.out.println("Lookup successful...");
//        FareBD delegate = null;
//        try {
//            delegate = (FareBD)airpricingModule.getServiceBD("fare.service.remote");
//            System.out.println("Delegate creation successful...");
//            return delegate;
//            
//        } catch (Exception e) {
//            System.out.println("Exception in creating delegate");
//            e.printStackTrace();
//        }
//        return delegate;
    	return null;
    }
	
		
}
	