/*
 * Created on Jul 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.airpricing.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import java.util.List;


import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

import com.isa.thinair.platform.api.util.PlatformTestCase;


//import com.isa.thinair.airpricing.api.dto.ChargeDTO;
import com.isa.thinair.airpricing.api.criteria.ChargeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ONDChargeSearchCriteria;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airpricing.api.model.HandlingCharge;
import com.isa.thinair.airpricing.api.model.OriginDestination;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.persistence.dao.HandlingChargeDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;




/**
 * @author byorn
 *
 */

public class TestChargeServiceRemote extends PlatformTestCase{
	protected void setUp() throws Exception{
		super.setUp();
	}
	
	
	/**
	 * Test method for saving a charge.
	 * Note: Charge rate id need not be given, also the charge code will appliy when setting the charge rates charge code.
	 * @throws ModuleException 
	 */
	public void testSaveCharges() throws ModuleException
	{
		Charge c = new Charge();
        //FIXME
		//c.setAppliesToInfants(1);
		c.setChargeCategoryCode("Global");
		c.setChargeGroupCode("AA1");
		c.setChargeDescription("AAAAAA");
		c.setChargeCode("AA8");
		c.setStatus(c.STATUS_ACTIVE);
	
		
			ChargeRate cr = new ChargeRate();
			cr.setChargeCode("AA8");
			cr.setChargeEffectiveFromDate(new Date());
			cr.setChargeEffectiveToDate(new Date());
			cr.setChargeRateCode(2);
			cr.setChargeValuePercentage(null);
			cr.setValueInLocalCurrency(new Double(12.0));
			cr.setValuePercentageFlag("V");
			cr.setStatus(cr.Status_Active);
			Set s = new HashSet();
			s.add(cr);
		
		c.setChargeRates(s);
		
		try{
            getChargeService().createCharge(c);    
        }
		catch(ModuleException e)
        {
        System.out.println(e.getExceptionCode() + " " + e.getMessage());    
        }
		
        getChargeService().deleteCharge("AA8");
		
		
		ChargeDAO dao = (ChargeDAO)AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);
		assertNull(dao.getCharge("AA8"));
		
	}
	
	
	/**
	 * Test method to get All the ONDCharges with paging, and passing a criteria, and order by list.
	 * @throws Exception
	 */
	public void testGetONDCharges() throws Exception
	{
		 	/** create the criteria **/
			ONDChargeSearchCriteria searchCriteria = new ONDChargeSearchCriteria();
		   
			/**                     **/
			
            List l1 = new ArrayList();
          
            l1.add("ond.ONDCode");
            
            Page p = getChargeService().getONDCharges(searchCriteria,0,10,l1);
			Collection list = p.getPageData();
			assertTrue("No records found", list.size()>0);
			
			Iterator iter = list.iterator();
			
			while(iter.hasNext())
			   {
				   OriginDestination originDestination = (OriginDestination) iter.next();
				   System.out.println("OND CODE: " + originDestination.getONDCode());
				   System.out.println("\n origin: " + originDestination.getOriginAirportCode());
				   System.out.println("\n destination: " + originDestination.getDestinationAirportCode());
				   System.out.println("\n ******Charges For the OND*******");
				   
				   Set s = originDestination.getCharges();
				   Iterator iter1 = s.iterator();
				   while(iter1.hasNext())
				   {
					   Charge c = (Charge)iter1.next();
					   System.out.println("\n Charge Code" + c.getChargeCode());
					   System.out.println("\n Charge description" + c.getChargeDescription());
					   //System.out.println("\n Applies to infants" + c.getAppliesToInfants());
					   System.out.println("\n charge categorycode" + c.getChargeCategoryCode());
					   System.out.println("\n charge groupcode" + c.getChargeGroupCode());
					   System.out.println("\n ******Charges Rate sFor the Charge*******");
					   Set s1 = c.getChargeRates();
					   Iterator iter2 = s1.iterator();
					   while(iter2.hasNext())
					   {
						   ChargeRate cr = (ChargeRate)iter2.next();  
						   System.out.println("\n chargeRate code " + cr.getChargeRateCode());
						   System.out.println("\n charge effective from date :" + cr.getChargeEffectiveFromDate().toString());
						   System.out.println("\n charge effective to date :" + cr.getChargeEffectiveToDate().toString());
						   System.out.println("\n value in local currency :" + cr.getValueInLocalCurrency());
						   System.out.println("\n value in Status :" + cr.getStatus());
					   }
				   }
			   }
		   
			
	}

	
	
	
	/**
	 * Test method to get All the charges with paging, and passing a criteria, and order by list.
	 * @throws Exception
	 */
	public void testGetCharges() throws Exception
	{
		 	/** create the criteria **/
			List criteriaList = new ArrayList();
			
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
			criterion.setFieldName("chargeCategoryCode");
				
			List values = new ArrayList();
			values.add("AA1");
			criterion.setValue(values);
			
			criteriaList.add(criterion);
			/**                     **/
			Page p = getChargeService().getCharges(new ChargeSearchCriteria(), 1, 100, null);
			Collection list = p.getPageData();
			assertTrue("No Records Found", list.size()>0);	
	}
	
	

	/**
	 * Test method for testing of getting handling charges , passing a crietira 
	 * @throws Exception
	 */
	
	public void testGetHandlingCharges() throws Exception
	{
		
		/** create the criteria **/
		List criteriaList = new ArrayList();
		ModuleCriterion criterion = new ModuleCriterion();
		criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
		criterion.setFieldName("airportCode");
			
		List values = new ArrayList();
		values.add("AA1");
		criterion.setValue(values);
		
		criteriaList.add(criterion);
		/**                     **/
		Page p = getChargeService().getHandlingCharges(criteriaList,0,100,null);
		Collection list = p.getPageData();
		assertTrue("No Records Found", list.size()>0);	
	}
	

	/**
	 * test for saving a handling charge
	 * @throws Exception
	 */
	/*public void testSaveHandlingCharges() throws Exception
	{
		
		HandlingCharge c = new HandlingCharge();
		c.setAirportCode("AA2");
		c.setStatus(c.STATUS_ACTIVE);
		c.setValidSegmentCode("AA1/AA2");
		c.setHandlingChargeAmount(123.23);
		c.setHandlingChargId(13);
		getChargeService().createHandlingCharge(c);
		getChargeService().deleteHandlingCharge(13);
		HandlingChargeDAO dao = (HandlingChargeDAO)AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.HANDLINGCHARGE_DAO);
		assertNull(dao.getHandlingCharge(13));
	}*/
	
	
	/* getCharge Service **/
	private ChargeBD getChargeService()
	{ 
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule airpricingModule = lookup.getModule("airpricing");
		System.out.println("Lookup successful...");
		ChargeBD delegate = null;
		try {
			delegate = (ChargeBD)airpricingModule.getServiceBD("charge.service.remote");
			System.out.println("Delegate creation successful...");
			return delegate;
			
		} catch (Exception e) {
			System.out.println("Exception in creating delegate");
			e.printStackTrace();
		}
		return delegate;
	}
	
	
}
