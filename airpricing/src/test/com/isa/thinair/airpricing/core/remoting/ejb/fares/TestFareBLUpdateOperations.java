/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.remoting.ejb.fares;

import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.bl.FareBL;
import com.isa.thinair.airpricing.core.persistence.dao.FareDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Byorn
 */
public class TestFareBLUpdateOperations extends TestBaseFaresTest {
	
	
	/**
	 * Need to Test 
	 * (1) Saving a FareRule (Copy of MasterFareRule to FareRule Table) -- RESULT: should save succesfully
	 * (2) Save a AD-HOC FareRule -- RESULT: should save successfully and masterfarerulecode column should be null.
	 * (3) Save a new FareRule with a masterFareRuleCode that already exists in FareRule Table. RESULT (Exception should occur)
	 * (4) Save a fare with a ONDFRBC exisitng combination RESULT : Exception should occur
	 * (5) Save a fare with existing FR and BC combination RESULT : Exception should occur
	 * (6) Update a fare 
	 **/
	
	FareRuleDAO fareRuleDAO;
	FareDAO fareDAO;
	FareJdbcDAO fareJdbcDAO;
	
	public TestFareBLUpdateOperations() {
		super();
		fareRuleDAO = (FareRuleDAO)AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_RULE_DAO);
		fareDAO = (FareDAO)AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_DAO);
		fareJdbcDAO = (FareJdbcDAO)AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.MANAGE_FARES);		
	}

	protected void setUp() throws Exception{
		super.setUp();
	}

    /**
     * Operation:   Test for checking when Ammending a Fare that is tied to a MasterFareRule.
     *              The MAsterFareRule details will be edited too. 
     * 
     *  
     * Result:      Fare will be updated
     *              In FareRule Table a record will be saved as an ADHOC. (i.e MFRCode = "")
     *              Fare should be tied to the new ADHOC and not to the Prev FareRuleID
     * 
     * NOTE :       FareDTO.masterFareRuleCode=null
     *              FareDTO.getFareRule.setVersion=-1
     * 
     *                  
     */
    public void testUpdateFare_And_ChangeMasterFareRule() throws Exception
    {
                
            /** fareID 19009 is currently linked to AA9 in fareRule 19009 **/
            /** changing it to link to AA10 **/
            
        
            FareDTO fareDTO = getFareService().getFare(19009);
            assertNotNull("could not retrive fare 19009", fareDTO);
            
            Fare fare = fareDTO.getFare();
            fare.setFareAmount(999);
            
            //changing to link to mfr10
            fareDTO.setMasterFareRuleCode("AA10");
            fareDTO.setFare(fare);
            fareDTO.setFareRule(null);
            
            try{
                    getFareService().updateFare(fareDTO);
        
            }
            catch(ModuleException e)
            {
                    System.out.println(e.getExceptionCode());
            }
    
            
            FareDTO fareDTO2 = getFareService().getFare(19009);
            assertEquals(999,999,fareDTO2.getFare().getFareAmount());
            assertEquals("AA10",fareDTO2.getFareRule().getMasterFareRuleCode());
            assertNotNull("Fare Rule was not saved correctly",fareRuleDAO.getFareRule(fare.getFareRuleId()));
            assertEquals("AA10",fareRuleDAO.getFareRule(fare.getFareRuleId()).getMasterFareRuleCode());
    }
    
    public void testUpdateFare_And_ChangeMasterFareRule_Which_Exists() throws Exception
    {
                
            /** fareID 19009 is currently linked to AA9 in fareRule 19009 **/
            /** changing it to link to AA10 **/
            
        
            FareDTO fareDTO = getFareService().getFare(19008);
            assertNotNull("could not retrive fare 19008", fareDTO);
            
            Fare fare = fareDTO.getFare();
            fare.setFareAmount(999);
            
            //changing to link to mfr02
            fareDTO.setMasterFareRuleCode("AA2");
            fareDTO.setFare(fare);
            fareDTO.setFareRule(null);
            
            try{
                    getFareService().updateFare(fareDTO);
        
            }
            catch(ModuleException e)
            {
                    System.out.println(e.getExceptionCode());
            }
    
            
            FareDTO fareDTO2 = getFareService().getFare(19008);
            int fareRuleIdinFAreRuleTable = fareRuleDAO.getFareRule("AA2").getFareRuleId();
            assertEquals(fareDTO2.getFareRule().getFareRuleId(), fareRuleIdinFAreRuleTable);
            
    }
    
    
    /**
     * Operation:   Ammending a Fare that is tied to a ADHOC FareRule
     *              Editing the Adhoc FareRule              
     * 
     * Result:      Fare will be updated
     *              In FareRule Table a the farerule record will be updated. (MFRCode = "")
     *         
     * Note:        FareDTO.masterFAreRuleCode=null
     * 
     */
    public void testUpdateAdhocFare_AndEdit_AdhocFareRule() throws Exception
    {
        
            
            FareDTO fareDTO = getFareService().getFare(19017);
            assertNotNull("failed retirieveing fare for 19017",fareDTO);
            
            Fare fare = fareDTO.getFare();
            
            FareRule fareRule = fareDTO.getFareRule();
            fareRule.setFareRuleDescription("testingtesting");
            
            fareDTO.setFare(fare);
            fareDTO.setFareRule(fareRule);
            fareDTO.setMasterFareRuleCode(null);
        
        getFareService().updateFare(fareDTO);
        
      
      
        
        FareDTO fareDTO2 = getFareService().getFare(19017);
        
        //fare rule id's must not change
        assertEquals("fare rule id's have changed", fareDTO.getFare().getFareRuleId(),fareDTO2.getFare().getFareRuleId());
        assertEquals("fare rule id's have changed", fareDTO.getFareRule().getFareRuleId(),fareDTO2.getFareRule().getFareRuleId());
        
        assertEquals("fare rule was not updated well", fareDTO.getFareRule().getFareRuleDescription(), "testingtesting");
    }
    /**
     * Test Update Fare With AD-HOC FareRule
     * void
     * @throws Exception
     */
    public void testUpdateAdhocFare_To_Link_To_NEW_MasterFareRule() throws Exception
    {
        
        FareDTO fareDTO = getFareService().getFare(19008);
        
        assertNotNull("failed retirieveing fare for 19008",fareDTO);
        fareDTO.setMasterFareRuleCode("AA11");
        assertNull(fareRuleDAO.getFareRule("AA11"));
        fareDTO.setFareRule(null);
        try{
        
            getFareService().updateFare(fareDTO);
        }
        catch(ModuleException e)
        {
            System.out.println(e.getExceptionCode() + " " + e.getMessageString() ); 
        }
        
        assertEquals(19008, fareDTO.getFare().getFareId());
        FareDTO fareDTO2 = getFareService().getFare(19008);
        assertEquals("AA11", fareDTO2.getFareRule().getMasterFareRuleCode());
        assertNotNull(fareRuleDAO.getFareRule("AA11"));
    }

    /**
     * Test Update Fare With AD-HOC FareRule
     * void
     * @throws Exception
     */
    public void testUpdateAdhocFare_To_Link_To_EXISTING_MasterFareRule() throws Exception
    {
        
        FareDTO fareDTO = getFareService().getFare(19019);
        int prevFareRuleId = fareDTO.getFare().getFareRuleId();
        assertNotNull("failed retirieveing fare for 19019",fareDTO);
        fareDTO.setMasterFareRuleCode("AA7");
        fareDTO.setFareRule(null);
        assertNotNull(fareRuleDAO.getFareRule("AA7"));
        getFareService().updateFare(fareDTO);
        
        assertEquals(19019, fareDTO.getFare().getFareId());
        FareDTO fareDTO2 = getFareService().getFare(19019);
        
        assertEquals("AA7", fareDTO2.getFareRule().getMasterFareRuleCode());
        assertNotSame(new Integer(prevFareRuleId),new Integer(fareDTO2.getFare().getFareRuleId()));
        assertEquals(fareDTO.getFare().getFareRuleId(), fareDTO2.getFare().getFareRuleId());
        
        
    }
    
    /**
     * 
     * void
     * @throws Exception
     */
    public void testUpdateMFRFare_To_Link_To_ADHOC() throws Exception
    {
        
        FareDTO fareDTO = getFareService().getFare(19005);
        assertNotNull("failed retirieveing fare for 19005",fareDTO);
        FareRule fareRule = fareDTO.getFareRule();
        fareRule.setVersion(-1);
        fareDTO.setFareRule(fareRule);
        fareDTO.setMasterFareRuleCode(null);
        
        int prevFareId = fareDTO.getFare().getFareId();
        int prevFareRuleId = fareDTO.getFare().getFareRuleId();
            
        
        
        getFareService().updateFare(fareDTO);
        
        
        FareDTO fareDTO2 = getFareService().getFare(19019);
        
        assertNull(fareDTO2.getFareRule().getMasterFareRuleCode());
        
        assertNotSame(new Integer(prevFareRuleId),new Integer(fareDTO2.getFare().getFareRuleId()));
        assertEquals(prevFareId, fareDTO2.getFare().getFareRuleId());
        
        
    }

		
}
	