/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.remoting.ejb.fares;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.InboundDepartureFrequency;
import com.isa.thinair.airpricing.api.model.MasterFareRule;
import com.isa.thinair.airpricing.api.model.OutboundDepartureFrequency;
import com.isa.thinair.airpricing.core.bl.FareBL;



/**
 * @author Byorn
 */
public class TestFareBLSaveOperations extends TestBaseFaresTest {
	
	public void testUpdateMasterFAreRule() throws Exception
	{
		MasterFareRule masterFareRule = fareRuleDAO.getMasterFareRule("AA1");
		masterFareRule.setCancellationChargeAmount(432.99);
//		getFareRuleService().createMasterFareRule(masterFareRule);
	}
	
//	/**
//	 * 
//	 * Creating a New Fare with  a MasterFareRule Code
//	 * void
//	 * @throws Exception
//	 */
//	public void testSaveFareWithNewMasterFareRule() throws Exception
//	{
//		int generatedFAreId=0;
//        FareDTO fareDTO = new FareDTO();
//		fareDTO.setMasterFareRuleCode("AA25");
//		
//		Fare fare = new Fare();
//		fare.setFareId(19026);
//        fare.setBookingCode("AA1");
//		fare.setFareAmount(232.22);
//		
//        GregorianCalendar calendar = new GregorianCalendar(2000, GregorianCalendar.FEBRUARY, 12);
//        GregorianCalendar calendar1 = new GregorianCalendar(2000, GregorianCalendar.MARCH, 12);
//        
//        fare.setEffectiveFromDate(calendar.getTime());
//		fare.setEffectiveToDate(calendar1.getTime());
//		
//        fare.setOndCode("AA1/AA2");
//		fare.setStatus(fare.STATUS_ACTIVE);
//		
//        fareDTO.setFare(fare);
//		try{
//		getFareService().saveFare(fareDTO);
//		
//        generatedFAreId = fare.getFareId();}
//		catch(ModuleException e)
//		{ System.out.println(e.getExceptionCode()); }
//        
//        Fare fare2 = fareDAO.getFare(generatedFAreId);
//        assertNotNull("fare didnt save successfully",fare2);
//         FareRule fareRule = fareRuleDAO.getFareRule(generatedFAreId);
//        assertNotNull("fareRule was not saved successfully", fareRule);
//        		
// 	}
//	
//    /**
//     * 
//     * Creating a New Fare with  a Existing MasterFareRule Code in FareRule Table
//     * void
//     * @throws Exception
//     * 
//     */
//    public void testSaveFareWithExistingMasterFareRuleInFareRuleTable() throws Exception
//    {
//        FareDTO fareDTO = new FareDTO();
//        fareDTO.setMasterFareRuleCode("AA25");
//        
//        Fare fare = new Fare();
//        fare.setFareId(19027);
//        fare.setBookingCode("AA2");
//        fare.setFareAmount(232.22);
//        
//        GregorianCalendar calendar = new GregorianCalendar(2000, GregorianCalendar.FEBRUARY, 12);
//        GregorianCalendar calendar1 = new GregorianCalendar(2000, GregorianCalendar.MARCH, 12);
//        
//        fare.setEffectiveFromDate(calendar.getTime());
//        fare.setEffectiveToDate(calendar1.getTime());
//        
//        fare.setOndCode("AA1/AA2");
//        fare.setStatus(fare.STATUS_ACTIVE);
//        
//        fareDTO.setFare(fare);
//        try{
//        getFareService().saveFare(fareDTO);}
//        
//        catch(ModuleException e)
//        { System.out.println(e.getExceptionCode()); }
//        
//        Fare fare2 = fareDAO.getFare(fare.getFareId());
//        assertNotNull("fare didnt save successfully",fare2);
//        
//        FareRule fareRule = fareRuleDAO.getFareRule(fare.getFareId());
//        assertNotNull("fareRule was not saved successfully", fareRule);
//        
//        assertEquals("Fares FareRule Id is incorrect", fare2.getFareRuleId(), fareRule.getFareRuleId());
//    }
//    
//    /**
//	 * Test BL Method when user is saving with a MasterFareRule Code
//	 * void
//	 * @throws Exception
//	 */
//	public void testSaveFareWithADHOCFareRule() throws Exception
//	{
//		FareDTO fareDTO = new FareDTO();
//		//fareDTO.setMasterFareRuleCode("AAA");
//		
//		Fare fare = new Fare();
//		fare.setBookingCode("eeeee");
//		fare.setFareAmount(232.22);
//		fare.setEffectiveFromDate(new Date());
//		fare.setEffectiveToDate(new Date());
//		fare.setOndCode("CMB/SHJ");
//		fare.setStatus(fare.STATUS_ACTIVE);
//		fareDTO.setFare(fare);
//
//		   FareRule fareRule = new FareRule();
//		   fareRule.setAdvanceBookingDays(new Integer(1));
//		   fareRule.setCancellationChargeAmount(12.11);
//		   fareRule.setFareBasisCode("Test");
//		     fareRule.setFareRuleDescription("TX Description");
//		   fareRule.setFareRulesIdentifier("11D");
//		   fareRule.setInboundDepatureFrequency(new InboundDepartureFrequency());
//		   fareRule.setOutboundDepatureFreqency(new OutboundDepartureFrequency());
//		   fareRule.setOutboundDepatureTimeFrom(new Date());
//		   fareRule.setInboundDepatureTimeFrom(new Date());
//		   fareRule.setMaximumStayoverDays(new Integer(12));
//		   fareRule.setMinimumStayoverDays(new Integer(12));
//		   fareRule.setModificationChargeAmount(12.12);
//		   fareRule.setRefundableFlag(FareRule.Refundable_Yes);
//		   fareRule.setStatus(FareRule.Status_Active);
//		   fareRule.setReturnFlag(fareRule.Return_Flight_Yes);
//           
//           Set visibleChannels = new HashSet();
//		   visibleChannels.add(new Integer(1));
//		   fareRule.setVisibleChannelIds(visibleChannels);
//
//		   Set visibleAgentIds = new HashSet();
//		   visibleAgentIds.add("PRICAG1");
//		   fareRule.setVisibileAgentCodes(visibleAgentIds);	
//		
//		fareDTO.setFareRule(fareRule);
//		
//		try{
//		getFareService().saveFare(fareDTO);}
//		catch(ModuleException e)
//		{
//		System.out.println(e.getExceptionCode());
//        }
//
//        Fare fare2 = fareDAO.getFare(fare.getFareId());
//        assertNotNull("fare didnt save successfully",fare2);
//        
//        FareRule fareRule2 = fareRuleDAO.getFareRule(fare.getFareId());
//        assertNotNull("fareRule was not saved successfully", fareRule2);
//        
//        assertNull("Adhoc Fare Rule's MasterFareRule code was  not null", fareRule2.getMasterFareRuleCode());
//		
// 	}
  
	


	

		
}
	