/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.hibernate;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

import com.isa.thinair.airpricing.api.model.InboundDepartureFrequency;
import com.isa.thinair.airpricing.api.model.MasterFareRule;
import com.isa.thinair.airpricing.api.model.MasterFareRulePax;
import com.isa.thinair.airpricing.api.model.OutboundDepartureFrequency;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Byorn
 *
 */
public class TestFareRuleDAOImpl extends PlatformTestCase {
	
	protected void setUp() throws Exception{
		super.setUp();
	}

   /** 
    * Test Method to SAVE  a MASTER FARE RULE
    * void
    * @throws Exception
    */
	public void testSaveMasterFareRule() throws Exception { 
	   
	   MasterFareRule masterFareRule = new MasterFareRule();
	   masterFareRule.setAdvanceBookingDays(new Integer(1));
	   masterFareRule.setCancellationChargeAmount(12.11);
	   masterFareRule.setFareBasisCode("Test");
	   masterFareRule.setFareRuleCode("A19"); 
	   masterFareRule.setFareRuleDescription("TestFR Description");
	   masterFareRule.setFareRulesIdentifier("11D");
	   masterFareRule.setInboundDepatureFrequency(new InboundDepartureFrequency());
	   masterFareRule.setOutboundDepatureFreqency(new OutboundDepartureFrequency());
	   masterFareRule.setOutboundDepatureTimeFrom(new Date());
	   masterFareRule.setInboundDepatureTimeFrom(new Date());
	   //masterFareRule.setMaximumStayoverMins(new Integer(12));
	   //masterFareRule.setMinimumStayoverMins(new Integer(12));
	   masterFareRule.setPaxCategoryCode("L");
	   masterFareRule.setFareCategoryCode("N");
	   

	   masterFareRule.setModificationChargeAmount(12.12);
//	   masterFareRule.setRefundableFlag(MasterFareRule.Refundable_Yes);
//	   masterFareRule.setStatus(MasterFareRule.Status_Active);
//	   masterFareRule.setReturnFlag(MasterFareRule.Return_Flight_Yes);
	   Set visibleChannels = new HashSet();
	   
       //must be data from the master table
       visibleChannels.add(new Integer(1));
	   
       masterFareRule.setVisibleChannelIds(visibleChannels);
	   
	   Set visibleAgentIds = new HashSet();
	   visibleAgentIds.add("GAN5");
	   masterFareRule.setVisibileAgentCodes(visibleAgentIds);
	   
	   //Add PAX Details
	   MasterFareRulePax oAdult = new MasterFareRulePax();
	   MasterFareRulePax oChild = new MasterFareRulePax();
	   MasterFareRulePax oInfant = new MasterFareRulePax();
	   
//	   oAdult.setApplyCNXCharge('Y');
//	   oAdult.setApplyFare('Y');
//	   oAdult.setApplyModCharge('Y');
//	   oAdult.setMasterFareRuleCode(masterFareRule.getFareRuleCode());
//	   oAdult.setPaxType("AD");
//	   oAdult.setRefundableFare('Y');
//	   
//	   oChild.setApplyCNXCharge('Y');
//	   oChild.setApplyFare('Y');
//	   oChild.setApplyModCharge('Y');
//	   oChild.setMasterFareRuleCode(masterFareRule.getFareRuleCode());
//	   oChild.setPaxType("CH");
//	   oChild.setRefundableFare('Y');
//	   
//	   oInfant.setApplyCNXCharge('Y');
//	   oInfant.setApplyFare('Y');
//	   oInfant.setApplyModCharge('Y');
//	   oInfant.setMasterFareRuleCode(masterFareRule.getFareRuleCode());
//	   oInfant.setPaxType("IN");
//	   oInfant.setRefundableFare('Y');
	   
	   Collection colPaxDetails = new HashSet();
	   colPaxDetails.add(oAdult);
	   colPaxDetails.add(oChild);
	   colPaxDetails.add(oInfant);
	   //------------------------------------------
	   
	   //masterFareRule.setPaxDetails(colPaxDetails);
	   
	   
	   
	   getFareRuleDAO().saveMasterFareRule(masterFareRule);
	   assertFalse(false);
//	   assertNotNull("AA7 MFR Code was not saved", getFareRuleDAO().getMasterFareRule("AA7"));
//	   getFareRuleDAO().deleteMasterFareRule("AA7");
//	   assertNull("AA7 MFR Code was not deleted",getFareRuleDAO().getMasterFareRule("AA7"));
   }
   
//	 /** 
//	    * Test Method to SAVE  a MASTER FARE RULE
//	    * void
//	    * @throws Exception
//	    */
//		public void testSaveFareRule() throws Exception {
//		   FareRule fareRule = new FareRule();
//		   fareRule.setFareRuleId(9022);
//           fareRule.setAdvanceBookingDays(new Integer(1));
//		   fareRule.setCancellationChargeAmount(12.11);
//		   fareRule.setFareBasisCode("Test");
//		   //fareRule.setFareRuleCode("TestFR");
//		   fareRule.setFareRuleDescription("TX Description");
//		   fareRule.setFareRulesIdentifier("11D");
//		   fareRule.setInboundDepatureFrequency(new InboundDepartureFrequency());
//		   fareRule.setOutboundDepatureFreqency(new OutboundDepartureFrequency());
//		   fareRule.setOutboundDepatureTimeFrom(new Date());
//		   fareRule.setInboundDepatureTimeFrom(new Date());
//		   fareRule.setMaximumStayoverDays(new Integer(12));
//		   fareRule.setMinimumStayoverDays(new Integer(12));
//		   fareRule.setModificationChargeAmount(12.12);
//		   fareRule.setRefundableFlag(FareRule.Refundable_Yes);
//		   fareRule.setStatus(FareRule.Status_Active);
//		   fareRule.setMasterFareRuleCode("AA1");
//		   Set visibleChannels = new HashSet();
//		   visibleChannels.add(new Integer(4));
//		   fareRule.setVisibleChannelIds(visibleChannels);
//		   
//		   Set visibleAgentIds = new HashSet();
//		   visibleAgentIds.add("AA1");
//		   fareRule.setVisibileAgentCodes(visibleAgentIds);
//		   
//		   getFareRuleDAO().saveFareRule(fareRule);
//		   assertNotNull("Fare Rule 9022 was not saved",getFareRuleDAO().getFareRule(9022));
//		   getFareRuleDAO().deleteFareRule(new Integer(9022));
//		   assertNull("Fare Rule 9022 was not deleted",getFareRuleDAO().getFareRule(9022));
//           //assertNull(getFareRuleDAO().getMasterFareRule("TestFR"));
//	   }
//	
//	/**
//    * Test Method to Get a MasterFareRule Paged DATA
//    * void
//    * @throws Exception
//    */
//   public void testGetMasterFareRulePaging() throws Exception
//   {
//	   List l = new ArrayList();
//	   ModuleCriterion moduleCriterion = new ModuleCriterion();
//	   l.add(moduleCriterion);
//	   
//	   Page p = getFareRuleDAO().getMasterFareRules(null,0,10,new ArrayList());
//	   System.out.println("TOTAL RECORDS: " + p.getTotalNoOfRecords());
//	   Collection c = p.getPageData();
//	   assertTrue("No records found", c.size()>0 );   
//   }
//   
//   /**
//    * Test Method to get a List of channels
//    * void
//    */
//   public void testGetChannels(){
//	   Collection channels = new ArrayList();
//	   channels.add(new Integer(1));
//	   channels.add(new Integer(2));
//	   Collection cs = getFareRuleDAO().getChannels(channels);
//	   assertEquals(2, cs.size());
//   }
//	   
//   
//	/**
//	 * Test method to get a Fare Rule given a MFR Code
//	 * void
//	 */
//   public void testGetFareRuleForMasterFareRuleCode()
//	{
//		assertEquals("AA1", getFareRuleDAO().getFareRule("AA1").getMasterFareRuleCode());
//		
//		
//		
//		
//	}
//	
	private FareRuleDAO getFareRuleDAO(){
	   return (FareRuleDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_RULE_DAO);
    }
   
	
	
	
}
		
	