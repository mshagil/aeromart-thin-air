/** MASTER DATA **/
INSERT INTO T_VALID_SEGMENT
(SEGMENT_CODE,DESCRIPTION,ORIGIN,DESTINATION,VERSION)
VALUES
('AA1/AA2','AA1AA2','AA1','AA2',0);

/**************/
/** CHARGES **/
/*************/

/** 2 CHARGES **/
INSERT INTO T_CHARGE
(CHARGE_CODE,CHARGE_DESCRIPTION,CHARGE_GROUP_CODE,CHARGE_CATEGORY_CODE,CHARGE_CATEGORY_VALUE,APPLIES_TO_INFANTS,REFUNDABLE_CHARGE_FLAG,STATUS,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,VERSION)
VALUES
('AA1','CODE1','AA1','AA1','12','1','1','ACT',NULL,NULL,NULL,NULL,0);

INSERT INTO T_CHARGE
(CHARGE_CODE,CHARGE_DESCRIPTION,CHARGE_GROUP_CODE,CHARGE_CATEGORY_CODE,CHARGE_CATEGORY_VALUE,APPLIES_TO_INFANTS,REFUNDABLE_CHARGE_FLAG,STATUS,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,VERSION)
VALUES
('AA2','CODE2','AA2','AA2','12','1','1','ACT',NULL,NULL,NULL,NULL,0);


/** 2 CHARGE RATES FOR CHARGE AA1 **/
INSERT INTO  t_charge_rate
            (charge_rate_id, charge_code, charge_effective_from_date,
             charge_effective_to_date, value_percentage_flag,
             charge_value_percentage, value_in_local_currency, created_by,
             created_date, modified_by, modified_date, VERSION, status
            )
VALUES (19001, 'AA1', '1-jan-2000',
             '3-mar-2000', 'P',
             33.22, 23.22, null, 
             null, null, null,0,'ACT'
            );

INSERT INTO  t_charge_rate
            (charge_rate_id, charge_code, charge_effective_from_date,
             charge_effective_to_date, value_percentage_flag,
             charge_value_percentage, value_in_local_currency, created_by,
             created_date, modified_by, modified_date, VERSION, status
            )
VALUES (19002, 'AA1', '3-mar-2000',
             '6-jun-2001', 'P',
             33.22, 23.22, null, 
             null, null, null,0,'ACT'
            );


/** 2 CHARGE RATES FOR CHARGE AA2 **/
INSERT INTO  t_charge_rate
            (charge_rate_id, charge_code, charge_effective_from_date,
             charge_effective_to_date, value_percentage_flag,
             charge_value_percentage, value_in_local_currency, created_by,
             created_date, modified_by, modified_date, VERSION, status
            )
VALUES (19003, 'AA2', '1-jan-2000',
             '3-mar-2000', 'P',
             33.22, 23.22, null, 
             null, null, null,0,'ACT'
            );

INSERT INTO  t_charge_rate
            (charge_rate_id, charge_code, charge_effective_from_date,
             charge_effective_to_date, value_percentage_flag,
             charge_value_percentage, value_in_local_currency, created_by,
             created_date, modified_by, modified_date, VERSION, status
            )
VALUES (19004, 'AA2', '3-mar-2000',
             '6-jun-2000', 'P',
             33.22, 23.22, null, 
             null, null, null,0,'ACT'
            );
/*************END OF CHARGES *****************************/


/*****************/
/* OND CHARGES */
/****************/

/** 3 ONDs **/
/* I */
INSERT INTO T_OND
(OND_CODE,ORIGIN,DESTINATION,VERSION)
VALUES
('AA1/AA2', 'AA1', 'AA2', 0); 

/* II */
INSERT INTO T_OND
(OND_CODE,ORIGIN,DESTINATION,VERSION)
VALUES
('AA1/AA2/AA3', 'AA1', 'AA3', 0); 

/* III */
INSERT INTO T_OND
(OND_CODE,ORIGIN,DESTINATION,VERSION)
VALUES
('AA1/AA2/AA4/AA3', 'AA1', 'AA3', 0); 



INSERT INTO  t_ond_charge
(charge_code, ond_code)
VALUES ('AA1', 'AA1/AA2');

INSERT INTO  t_ond_charge
(charge_code, ond_code)
VALUES ('AA1', 'AA1/AA2/AA3');
/******end of ond charges***/


/************************/
/* MASTER DATA FOR      */ 
/* FARES AND FARE RULES */
/***********************/


/** 3 booking codes **/
/** 1 **/
INSERT INTO T_BOOKING_CLASS
(BOOKING_CODE, BOOKING_CODE_DESCRIPTION, CABIN_CLASS_CODE, STANDARD_CODE, NEST_RANK, REMARKS, STATUS, 
CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,VERSION, FIXED_FLAG, PAX_TYPE)
VALUES
('AA1', 'standard booking code','I','Y',501,'remark','ACT',
NULL,NULL,NULL,NULL,0, 'Y','ADULT');

/** II **/
INSERT INTO T_BOOKING_CLASS
(BOOKING_CODE, BOOKING_CODE_DESCRIPTION, CABIN_CLASS_CODE, STANDARD_CODE, NEST_RANK, REMARKS, STATUS, 
CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,VERSION, FIXED_FLAG, PAX_TYPE)
VALUES
('AA2', 'standard booking code','J','Y',501,'remark','ACT',
NULL,NULL,NULL,NULL,0,'N','ADULT');

/** III **/
INSERT INTO T_BOOKING_CLASS
(BOOKING_CODE, BOOKING_CODE_DESCRIPTION, CABIN_CLASS_CODE, STANDARD_CODE, NEST_RANK, REMARKS, STATUS, 
CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,VERSION, FIXED_FLAG, PAX_TYPE)
VALUES
('AA3', 'standard booking code','Y','Y',501,'remark','ACT',
NULL,NULL,NULL,NULL,0, 'N', 'ADULT');


/** **************************************************************/
/******	*************3 	AGENTS **********************************/
/*******************AA1 , AA2  AA3 ***************************/

INSERT INTO  t_agent_type
       (agent_type_code, agent_type_description, VERSION
       )
VALUES ('AA1', 'Agent Type1',0);

INSERT   INTO  t_agent
           (billing_email, handling_charge_amount, agent_code,
            agent_type_code, agent_name, address_line_1, address_line_2,
            address_city, address_state_province, postal_code,
            account_code, station_code, territory_code, telephone, fax,
            email_id, agent_iata_number, credit_limit,
            bank_guarantee_cash_advance, status, created_by,
            created_date, modified_by, modified_date, VERSION,
            report_to_gsa
           )
VALUES ('b@yahoo.com', 23.22, 'AA1',
         'AA1', 'Agent name', 'address_line_1', 'address_line_2',
             'address_city', 'address_state_provce', 'posde',
             'account', 'AA1', 'AA1', 'telepne', 'fax',
             'email_id', 'agent_iata', 344.33,
             456.33, 'ACT', null,
             null, null, null, 0,
             'Y'
            );
            
INSERT   INTO  t_agent
           (billing_email, handling_charge_amount, agent_code,
            agent_type_code, agent_name, address_line_1, address_line_2,
            address_city, address_state_province, postal_code,
            account_code, station_code, territory_code, telephone, fax,
            email_id, agent_iata_number, credit_limit,
            bank_guarantee_cash_advance, status, created_by,
            created_date, modified_by, modified_date, VERSION,
            report_to_gsa
           )
VALUES ('b@yahoo.com', 23.22, 'AA2',
         'AA1', 'Agent name', 'address_line_1', 'address_line_2',
             'address_city', 'address_state_provce', 'posde',
             'account', 'AA1', 'AA1', 'telepne', 'fax',
             'email_id', 'agent_iata', 344.33,
             456.33, 'ACT', null,
             null, null, null, 0,
             'Y'
            );

INSERT   INTO  t_agent
           (billing_email, handling_charge_amount, agent_code,
            agent_type_code, agent_name, address_line_1, address_line_2,
            address_city, address_state_province, postal_code,
            account_code, station_code, territory_code, telephone, fax,
            email_id, agent_iata_number, credit_limit,
            bank_guarantee_cash_advance, status, created_by,
            created_date, modified_by, modified_date, VERSION,
            report_to_gsa
           )
VALUES ('b@yahoo.com', 23.22, 'AA3',
         'AA1', 'Agent name', 'address_line_1', 'address_line_2',
             'address_city', 'address_state_provce', 'posde',
             'account', 'AA1', 'AA1', 'telepne', 'fax',
             'email_id', 'agent_iata', 344.33,
             456.33, 'ACT', null,
             null, null, null, 0,
             'Y'
            );
            
            


/** 20 MFR's inserted **/

/*(I) */
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, minimum_stay_over_mins,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA1', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );
            
   /*(II) */
            INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             maximum_stay_over_days, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA2', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );
/*(III) */
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA3', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/*(IV) */
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA4', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/*(V) */
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA5', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/*(V1) */
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA6', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/*(V11) */
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA7', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/*(V111) */
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA8', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/*(1x) */
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA9', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/*(x) */
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA10', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );


/*(x1) */
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA11', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );


/** XII **/
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA12', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/** XIII **/
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA13', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/** XIV **/
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA14', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/** XV **/
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA15', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/** XVI**/
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA16', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/** XVII**/
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA17', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/** XVIII **/
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA18', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/** XIX **/
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA19', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

/** XX **/
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA20', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );
            
  /** XXi **/
  INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA21', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

  /** XXii **/
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA22', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );
            
   /** XXiii **/
 INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA23', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

   /** XXiv **/
INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA24', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );

   /** XXiv **/
 INSERT INTO  t_master_fare_rule
            (master_fare_rule_code, fare_rule_description, status,
             advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS,
             MAXIMUM_STAY_OVER_MINS, modification_charge_amount,
             cancellation_charge_amount, valid_days_of_week_a0,
             valid_days_of_week_a1, valid_days_of_week_a2,
             valid_days_of_week_a3, valid_days_of_week_a4,
             valid_days_of_week_a5, valid_days_of_week_a6,
             valid_days_of_week_d0, valid_days_of_week_d1,
             valid_days_of_week_d2, valid_days_of_week_d3,
             valid_days_of_week_d4, valid_days_of_week_d5,
             valid_days_of_week_d6, departure_time_from,
             departure_time_to, arrival_time_from, arrival_time_to,
             rules_comments, fare_rule_identifier, created_by,
             created_date, modified_by, modified_date, VERSION,
             fare_basis_code, refundable_flag
            )
     VALUES ('AA25', 'fare_rule_description', 'ACT',
             11,'Y',11,
             11,12.22,
             22.11,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,0,
             0,'1-nov-98',
             '1-dec-99','1-nov-98','1-nov-99',
             'rules_comments',null,null,
             '1-nov-98',null,null,0,
             '12','Y'
            );
            


/** 20 Fare Rules **/
/** 10 Fare Rules Linked to 10 MFRS 
		NOTE: A SINGLE MFR CANT APPEAR TWICE IN THIS TABLE 
		NOTE : AA11-AA20 MFR IS NOT APPEARING IN THE FR TABLE FOR TESTS PURPOSES**/

/** 1)  **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, 
             valid_days_of_week_d1, valid_days_of_week_d2,
             valid_days_of_week_d3, valid_days_of_week_d4,
             valid_days_of_week_d5, valid_days_of_week_d6,
             departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,
             created_by, created_date, modified_by, modified_date,
             VERSION, fare_basis_code, refundable_flag,
             master_fare_rule_code, fare_rule_id, fare_rule_code,
             fare_rule_description, status, advance_booking_days,
             return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,
             valid_days_of_week_a0, valid_days_of_week_a1,
             valid_days_of_week_a2, valid_days_of_week_a3,
             valid_days_of_week_a4, valid_days_of_week_a5
            )
     VALUES (0, 0, 
             0, 0,
             0, 0,
             0, 0,
             '1-dec-2000', '1-dec-2001', '1-dec-2000',
             '1-dec-2000', 'rules_comments', null,
             null, null, null, null,
             0, 'test', 'Y',
             'AA1', 19001, null,
             'fare_rule_description', 'ACT', 11,
             'Y', 11, 11,
             11.11, 11.11,
             0, 0,
             0, 0,
             0, 0
            );


/** II)  MFR AA2 **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', 'AA2', 19002, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );
     

/** III)  MFR AA3 **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', 'AA3', 19003, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** IV)  MFR AA4 **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', 'AA4', 19004, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** V)  MFR AA5 **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', 'AA5', 19005, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** VI)  MFR AA6 **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', 'AA6', 19006, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** VII)  MFR AA7 **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', 'AA7', 19007, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** VIII)  MFR AA8 **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', 'AA8', 19008, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** IX)  MFR AA9 **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', 'AA9', 19009, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** X)  MFR AA10 **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', 'AA10', 19010, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** XI)  ADHOC  **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', null, 19011, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );
     
/** X11 ADHOC **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', null, 19012, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );


/** X111 ADHOC **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', null, 19013, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );
     
/** X1v ADHOC **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', null, 19014, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** xv ADHOC **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', null, 19015, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** xvi ADHOC **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', null, 19016, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** xvii ADHOC **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', null, 19017, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** xviii ADHOC **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', null, 19018, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** xix ADHOC **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', null, 19019, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );

/** xx ADHOC **/
INSERT  INTO  t_fare_rule (valid_days_of_week_a6, valid_days_of_week_d0, valid_days_of_week_d1, valid_days_of_week_d2, valid_days_of_week_d3, valid_days_of_week_d4, valid_days_of_week_d5, valid_days_of_week_d6, departure_time_from, departure_time_to, arrival_time_from,
             arrival_time_to, rules_comments, fare_rule_identifier,  created_by, created_date, modified_by, modified_date,  VERSION, fare_basis_code, refundable_flag,  master_fare_rule_code, fare_rule_id, fare_rule_code,  fare_rule_description, status, advance_booking_days, return_flag, MINIMUM_STAY_OVER_MINS, MAXIMUM_STAY_OVER_MINS,
             modification_charge_amount, cancellation_charge_amount,  valid_days_of_week_a0, valid_days_of_week_a1,  valid_days_of_week_a2, valid_days_of_week_a3, valid_days_of_week_a4, valid_days_of_week_a5   )
     VALUES (0, 0, 0, 0, 0, 0, 0, 0, '1-dec-2000', '1-dec-2001', '1-dec-2000', '1-dec-2000', 'rules_comments', null, null, null, null, null, 0, 'test', 'Y', null, 19020, null, 'fare_rule_description', 'ACT', 11, 'Y', 11, 11, 11.11, 11.11,0, 0, 0, 0, 0, 0 );


/** 25 OND FARES INSERTED **/
/** NOTE : No of rows in the fare rule table cant be greater than the number of rows in the t_ond_fare  **/
/**THE FIRST 5 FARES ARE LINKED TO 5 FARE RULES WHICH ARE MFRS **/
/** THE NEXT 5 FARES ARE LINKED TO 5 FARES RULES WHICH ARE ADHOCS **/
INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19001, '1-jan-2000', '3-mar-2000', 234.22,'ACT','AA1/AA2',0,19001,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19002, '11-jan-2000', '13-mar-2000', 234.22,'ACT','AA1/AA2',0,19002,'AA2');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19003, '21-jan-2000', '23-mar-2000', 234.22,'ACT','AA1/AA2',0,19003,'AA3');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19004, '3-mar-2000', '6-jun-2000', 234.22,'ACT','AA1/AA2/AA3',0,19004,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19005, '13-mar-2000', '16-jun-2000', 234.22,'ACT','AA1/AA2/AA3',0,19005,'AA2');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19006, '23-mar-2000', '23-jun-2000', 234.22,'ACT','AA1/AA2/AA4/AA3',0,19006,'AA3');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19007, '6-jun-2000', '10-oct-2000', 234.22,'ACT','AA1/AA2/AA4/AA3',0,19007,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19008, '16-jun-2000', '20-oct-2000', 234.22,'ACT','AA1/AA2',0,19008,'AA2');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19009, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19009,'AA3');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19010, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19010,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19011, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19011,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19012, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19012,'AA1');


INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19013, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19013,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19014, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19014,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19015, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19015,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19016, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19016,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19017, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19017,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19018, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19018,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19019, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19019,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19020, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19020,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19021, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19001,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19022, '26-jun-2000', '30-oct-2000', 234.22,'ACT','AA1/AA2',0,19002,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19023, '26-jun-2000', '30-oct-2000', 234.22,'INA','AA1/AA2',0,19003,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19024, '26-jun-2000', '30-oct-2000', 234.22,'INA','AA1/AA2',0,19004,'AA1');

INSERT INTO T_OND_FARE 
(FARE_ID,EFFECTIVE_FROM_DATE,EFFECTIVE_TO_DATE,FARE_AMOUNT,STATUS,OND_CODE,VERSION,FARE_RULE_ID,BOOKING_CODE)
VALUES
(19025, '26-jun-2000', '30-oct-2000', 234.22,'INA','AA1/AA2',0,19005,'AA1');

/** end of inserting 10 ond fares **********************/

/***************************************************/
/** MASTER FARE RULES AND AGENTS AND VISIBILITIES **/
/***************************************************/				

				/** DEPENDANT: SALES_CHANNEL DATA 
                 1	All
                 2	Web
                 3	Air Arabia (Internal)
                 4	GSA & Travel Agent  **/

/***************************************************/

			/***************************************/
			/* 25 MASTER FARE RULES WITH VISIBILITIES */
			/***************************************/

			/** NOTE IMPORTANT : ALL THE MASTERFARERULES HAVE TO HAVE ATLEAST ONE SALES CHANNEL CODE **/			
			/* FIRST 10 MFRS WILL HAVE ALL THE VISIBILITIES .I.E : sales_channel_code = 1 */
			/* NEXT 5 MFRS WILL HAVE  sales_channel_code = 2 */
			/* NEXT 5 MFRS WILL HAVE  sales_channel_code = 3 */
			/* NEXT 5 MFRS WILL HAVE  sales_channel_code = 4 */
			
			/* NOTE : THERE ARE 10 GSA & TRAVEL AGENTS ...ALL LINKED TO A MASTER FARE RULE    
					  THEY ARE AA1-AA5 AND AA16-AA20					   	**/
			/***********************************************************************************/

			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (1, 'AA1');
			
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (1, 'AA2');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (1, 'AA3');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (1, 'AA4');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (1, 'AA5');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (1, 'AA6');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (1, 'AA7');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (1, 'AA8');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (1, 'AA9');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (1, 'AA10');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (2, 'AA11');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (2, 'AA12');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (2, 'AA13');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (2, 'AA14');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (2, 'AA15');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (3, 'AA16');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (3, 'AA17');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (3, 'AA18');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (3, 'AA19');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (3, 'AA20');
			  
			  	INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (4, 'AA21');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (4, 'AA22');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (4, 'AA23');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (4, 'AA24');
			INSERT INTO  t_master_fare_visibility
			      (sales_channel_code, master_fare_rule_code
			       )
			     VALUES (4, 'AA25');
			   
			     

		
			/***************************************/
			/* MASTER FARE RULES WITH AGENT		   */
			/***************************************/
			/** AA1-AA5 & AA16-AA20 **/

				/** Agent AA1 has 10 MFR **/
				/** Agent AA2 has 5 MFR **/
				/** Agent AA3 has 5 MFR **/

				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA1', 'AA1');
				
				
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA1', 'AA2');
			
			
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA1', 'AA3');
				
				
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA1', 'AA4');
			
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA1', 'AA5');
				
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA1', 'AA6');
				
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA1', 'AA7');
				
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA1', 'AA8');
				
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA1', 'AA9');
				
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA1', 'AA10');
				
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA2', 'AA11');
				
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA2', 'AA12');
				
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA2', 'AA13');

				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA2', 'AA14');

				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA2', 'AA15');

				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA3', 'AA16');

				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA3', 'AA17');
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA3', 'AA18');
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA3', 'AA19');
				INSERT  INTO  t_master_fare_agent
				            (agent_code, master_fare_rule_code
				            )
				VALUES     ('AA3', 'AA20');


/***************************************************/
/**        FARE RULES AND AGENTS AND VISIBILITIES **/
/***************************************************/		
			
			/***************************************/
			/* FARE RULES WITH VISIBILITIES 	   */
			/***************************************/

			/** NOTE : IN FARES I HAVE ENTERD 5 FARES WITH FARERULES LINKD TO 5 MFRS **/
			/** ONLY FOR THOSE 5 MFRS I WILL ENTER DATA FOR VISIBILITIY AND AGENTS **/
			/** 19001 TO 19005 fare rule id's are MFR1 to MFR5 which are lined to Sales channel code 1 **/
			
			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (1, 19001);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (1, 19002);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (1, 19003);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (1, 19004);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (1, 19005);
		     
		     INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (1, 19006);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (1, 19007);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (1, 19008);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (1, 19009);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (1, 19010);
		     
		     	INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (2, 19011);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (2, 19012);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (2, 19013);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (2, 19014);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (2, 19015);
		     
		     INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (3, 19016);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (3, 19017);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (3, 19018);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (3, 19019);

			INSERT  INTO  t_fare_visibility
		     (sales_channel_code, fare_rule_id)
		     VALUES (3, 19020);
     

			/***************************************/
			/* FARE RULES WITH AGENTS     	 	   */
			/***************************************/

			/** agents fare rules **/
			/** Agent AA1 has 3 **/

				INSERT  INTO  t_fare_agent
				        (agent_code, fare_rule_id
				         )
				VALUES ('AA1', 19001);
			
				INSERT  INTO  t_fare_agent
				        (agent_code, fare_rule_id
				         )
				VALUES ('AA1', 19002);
			
				INSERT  INTO  t_fare_agent
				        (agent_code, fare_rule_id
				         )
				VALUES ('AA1', 19003);
				
			
				INSERT  INTO  t_fare_agent
				        (agent_code, fare_rule_id
				         )
				VALUES ('AA2', 19004);
			
			
				INSERT  INTO  t_fare_agent
				        (agent_code, fare_rule_id
				         )
				VALUES ('AA2', 19005);
				
					INSERT  INTO  t_fare_agent
				        (agent_code, fare_rule_id
				         )
				VALUES ('AA2', 19006);
			
				INSERT  INTO  t_fare_agent
				        (agent_code, fare_rule_id
				         )
				VALUES ('AA3', 19007);
			
				INSERT  INTO  t_fare_agent
				        (agent_code, fare_rule_id
				         )
				VALUES ('AA3', 19008);
				
			
				INSERT  INTO  t_fare_agent
				        (agent_code, fare_rule_id
				         )
				VALUES ('AA3', 19009);
			
			
				INSERT  INTO  t_fare_agent
				        (agent_code, fare_rule_id
				         )
				VALUES ('AA3', 19010);



